<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NachDetails extends Model
{
    protected $table = 'nach_details';
    protected $fillable = ['nach_status',];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
