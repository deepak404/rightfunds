<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ekyc extends Model
{
	protected $table = 'ekyc';
    
    public function user(){
    	$this->belongsTo('App\User');
    }
}
