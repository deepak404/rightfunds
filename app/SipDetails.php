<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SipDetails extends Model
{
    protected $table = 'sip_details';

    public function sip(){
    	return $this->belongsTo('App\Sip');
    }

    public function portfolioDetails(){
    	return $this->hasOne('App\portfolioDetails');
    }
    
}
