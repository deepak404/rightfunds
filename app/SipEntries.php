<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SipEntries extends Model
{
   	public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function sip()
    {
    	return $this->belongsTo('App\Sip');
    }
}
