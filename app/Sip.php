<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sip extends Model
{

    public function sipDetails(){
    	return $this->hasMany('App\SipDetails');
    }

    public function investmentDetails()
    {
    	return $this->belongsTo('App\InvestmentDetails', 'port_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function sipEntries()
    {
        return $this->hasMany('App\SipEntries');
    }
}
