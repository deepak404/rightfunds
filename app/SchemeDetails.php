<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeDetails extends Model
{
    protected $table = 'scheme_details';

    public function portfolioDetails(){

    	return $this->hasMany('App\PortfolioDetails');	
    }

    public function withdrawDetails(){

    	return $this->hasMany('App\WithdrawDetails');	
    }
}
