<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NomineeDetails extends Model
{
    protected $table = "nominee_details";

    protected $fillable = ['nomi_type'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
