<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        //removed the pan from previous version
        //'name', 'email', 'password','mobile','pan','acc_type','p_check','kyc_check' , 'otp', 'otp_check', 'otp_start_time'

        'name', 'email', 'password','mobile','acc_type','p_check','kyc_check' , 'otp', 'otp_check', 'otp_start_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'otp'
    ];


    public function personalDetails()
    {
        return $this->hasOne('App\PersonalDetails');
    }

    public function bankDetails()
    {
        return $this->hasMany('App\BankDetails');
    }

    public function corporateDetails()
    {
        return $this->hasOne('App\corporateDetails');
    }


    public function directors(){
        return $this->hasMany('App\directors');
    }


    public function portfolioComp(){
        return $this->hasMany('App\portfolioComp');
    }
	
	public function nomineeDetails(){
        return $this->hasMany('App\NomineeDetails');
    }

    public function investmentDetails(){
        return $this->hasMany('App\InvestmentDetails');
    }

    public function schemeDetails(){
        return $this->hasMany('App\SchemeDetails');
    }

    public function portfolioDetails(){
        return $this->hasMany('App\PortfolioDetails');
    }

    public function WithdrawDetails(){
        return $this->hasMany('App\WithdrawDetails');
    }
	
	public function sip(){
        return $this->hasMany('App\Sip');
    }

    public function nachDetails()
    {
        return $this->hasOne('App\NachDetails');
    }

    public function ekyc()
    {
        return $this->hasOne('App\ekyc');
    }

    public function notifications(){
        return $this->hasMany('App\Notifications');
    }
}
