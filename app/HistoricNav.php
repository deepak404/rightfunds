<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricNav extends Model
{
    protected $table = 'historic_navs';
}
