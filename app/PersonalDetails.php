<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalDetails extends Model
{
    protected $table = "personal_details";


     public function user()
    {
        return $this->belongsTo('App\User');
    }
}
