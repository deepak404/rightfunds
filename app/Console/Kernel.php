<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
           Commands\GetNavValues::class,
           Commands\GetBseIndex::class,
           Commands\BseFutureOrders::class,
           Commands\UpdateBsePayment::class,
           //Commands\MySqlDump::class,
           Commands\SipUpdates::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('get:nav')->daily(00.10);
        $schedule->command('get:bse')->daily();
        $schedule->command('update:sip')->daily();
        $schedule->command('get:bsefuture')->daily(10.30);
        $schedule->command('get:bsepayment')->daily(00.10);
        //$schedule->command('db:mysqldump')->daily();
        //$schedule->command('get:nav')->everyMinute();
    }
}
