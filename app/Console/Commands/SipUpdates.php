<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sip;
use App\SchemeDetails;
use Carbon\Carbon;
use App\SipDetails;
use App\InvestmentDetails;
use App\PortfolioDetails;

class SipUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:sip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and make auto entry for recuring sips';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sips = Sip::all();
        $scheme_details = SchemeDetails::all()->pluck('bse_scheme_code','scheme_code');
        foreach($sips as $sip) {
            $sip_start_date = new Carbon($sip->start_date);
            $current_date = carbon::now();
            $current_sip_date = new Carbon($current_date->year.'-'.$current_date->month.'-'.$sip_start_date->day);
            //$sip_start_date->addMonths(1);

            $diff_in_days = $current_sip_date->diffInDays($current_date);
            if($diff_in_days > 0 && $diff_in_days <= 7) {

                $current_month = $current_date->month;
                $current_year = $current_date->year;
                $sip_id = $sip->id;

                $sip_details = SipDetails::all()
                    ->where('sip_id', $sip_id)
                    ->where('month', $current_month)
                    ->where('year', $current_year);
                // dd($sip_details);
                    // dd(count($sip_details));
                if(count($sip_details) == 0) {

                    // dd($sip_id);
                    $user = $sip->user;
                    //$investment_detail = $sip->investmentDetails;
                    $portfolio_details = $sip->sipEntries;
                    $new_investment_details = new InvestmentDetails();
                    // dd($sip, $sip_id, $current_month, $current_year);
                    $new_investment_details->portfolio_type = $this->sip_port_type[$sip->port_type];
                    $new_investment_details->investment_amount = $sip->inv_amount;
                    $new_investment_details->investment_date = $current_sip_date->toDateString();
                    $new_investment_details->investment_type = 2;
                    $new_investment_details->investment_status = 0;
                    $new_investment_details->sip_id = $sip_id;
                    $new_investment_save = $user->investmentDetails()->save($new_investment_details);


                    if($new_investment_save) {
                        foreach($portfolio_details as $portfolio_detail) {
                            $scheme_code_sip = SipEntries::where('sip_id',$sip->id)->value('scheme_code');
                            $port_type_sip = SchemeDetails::where('scheme_code',$scheme_code_sip)->value('scheme_type');

                            $new_portfolio_detail = new PortfolioDetails();
                            $new_portfolio_detail->investment_id = $new_investment_save->id;
                            $new_portfolio_detail->scheme_code = $portfolio_detail->scheme_code;
                            $new_portfolio_detail->bse_scheme_code = $scheme_details[$portfolio_detail->scheme_code];
                            $new_portfolio_detail->folio_number = '';
                            $new_portfolio_detail->portfolio_status = 0;
                            $new_portfolio_detail->sip_reg_no = $portfolio_detail->sip_reg_no;
                            $new_portfolio_detail->initial_amount_invested = $portfolio_detail->amount;
                            // $new_portfolio_detail->portfolio_type = $sip->port_type;
                            $new_portfolio_detail->portfolio_type = $port_type_sip;
                            $new_portfolio_detail->amount_invested = $portfolio_detail->amount;
                            $new_portfolio_detail->investment_date = $current_sip_date->toDateString();
                            $new_portfolio_detail->units_held = '';
                            $new_portfolio_detail->sip_id = $sip_id;
                            $user->portfolioDetails()->save($new_portfolio_detail);
                        }

                        $sipdetails = new SipDetails();
                        $sipdetails->month = $current_month;
                        $sipdetails->year = $current_year;

                        $sip->sipDetails()->save($sipdetails);
                    }
                }
            }
        }
    }
}
