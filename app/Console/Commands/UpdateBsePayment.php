<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PortfolioDetails;
use App\InvestmentDetails;
use Carbon\Carbon;
use SoapClient;
use SoapHeader;

class UpdateBsePayment extends Command
{
    protected $response_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:bsepayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inv_array = [];
        $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
                'soap_version' => SOAP_1_2, // !!!!!!!
                'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
              ));

        $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword',true);
        $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

        $client->__setSoapHeaders([$actionHeader,$toHeader]);

            $login_params = array(
              'UserId' => '1245601',
              'MemberId' => '12456',
              'Password' => 'Pwm$2017',
              'PassKey' => '1234569870',

            );

          $result = $client->getPassword($login_params);
          $result = $result->getPasswordResult;
          //dd($result);

          $response = explode('|', $result);

          // dd($response);
          $pwd_response =  $response[0];
          $this->response_key = $response[1];


            $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
                'soap_version' => SOAP_1_2, // !!!!!!!
                'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
              ));

            $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI',true);
            $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

            $client->__setSoapHeaders([$actionHeader,$toHeader]);


            $portfolio_details = PortfolioDetails::where('portfolio_status',0)->where('bse_order_status',0)->get();

            //dd($portfolio_details);
            foreach ($portfolio_details as $portfolio) {
                //echo $portfolio->bse_order_no."-".$portfolio->user->personalDetails->pan."<br>";
                // echo $portfolio->id."<br>";
                $params = array(
                  'Flag' => '11',
                  'UserId' => '1245601',
                  'EncryptedPassword' => $this->response_key,
                  'param' => $portfolio->user->personalDetails->pan.'|'.$portfolio->bse_order_no.'|BSEMF',

                );

                $result = $client->MFAPI($params);
                $result = explode('|', $result->MFAPIResult);

                $payment_res = explode('(', $result[1]);
                //echo $portfolio->user->personalDetails->pan." -- ".$portfolio->bse_order_no." -- ".trim($payment_res[0])."<br>";

                if (trim($payment_res[0]) == "APPROVED") {
                    // echo "Inside Approved";
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'apr']);
                }else if (trim($payment_res[0]) == "REJECTED") {
                    // reject the order.
                    // echo "Inside rejected";
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'rej','portfolio_status'=>3]);
                    $inv_array[] = $portfolio->investment_id;
                }else if (trim($payment_res[0]) == "AWAITING FOR FUNDS CONFIRMATION") {
                    // Change the Payment Status column to AWC
                    // echo "inside afc";
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'afc']);
                    $inv_array[] = $portfolio->investment_id;
                }else if (trim($payment_res[0]) == "PAYMENT NOT INITIATED FOR GIVEN ORDER") {
                    // Chnage the Payment STatus to PNI
                    // echo "Inside pni";

                    // if it stays "PNI" for more than 24 hours Cancel it off
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'pni']);
                    $inv_array[] = $portfolio->investment_id;
                }else{

                }
                //echo ;
            }


            $update_pni_port = PortfolioDetails::where('created_at', '<', Carbon::now()->subDay())->where('bse_payment_status','pni')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

            $update_ac = PortfolioDetails::where('created_at', '<', Carbon::now()->subDays(7))->where('bse_payment_status','afc')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);


            foreach ($inv_array as $inv) {

                $can_port_count = PortfolioDetails::where('investment_id',$inv)->where('portfolio_status','!=',3)->count();
                //echo $can_port_count."<br>";

                if ($can_port_count == 0) {
                    InvestmentDetails::where('id',$inv)->update(['investment_status'=>3]);
                }
            }
    }
}
