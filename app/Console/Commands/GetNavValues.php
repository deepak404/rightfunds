<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DailyNav;
use App\HistoricNav;
use Carbon\Carbon;
use App\SchemeDetails;

class GetNavValues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:nav';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the daily Nav value of the funds from the AMFII Website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        
        /*$scheme_code contains the scheme code of the 20 funds. First 5 are Debt funds, Followed by 4 balanced fund, and 11 Equity funds*/
        $week_old = Carbon::now()->subDays(8)->toDateString();
        //echo $week_old;
        DailyNav::where('date','<', $week_old)->delete();


        // // Increasing the Maximum execution time and setting it to default value at End of the function 
        
        //  /*$scheme_code contains the scheme code of the 20 funds. First 5 are Debt funds, Followed by 4 balanced fund, and 11 Equity funds*/

        $scheme_code = SchemeDetails::all()->pluck('scheme_code')->toArray();

        // //$scheme_code = array("106212","112423","114239","128053","111803","133805","133926",  "104685","100122","118191","131666","100081","100520","118102","100349","112090" ,"105758","103360","113177","101672","104481","107745","103196","101979");


        // //dd($db,$scheme_code);

        //set_time_limit(300);
        
        $count = 0;
        // $file = file("http://portal.amfiindia.com/spages//NAV0.txt");
        $file = file("https://www.amfiindia.com/spages/NAVAll.txt");

        //dd($file);
        $today = Carbon::now();
        $today = $today->toDateString();

        $total_scheme_count = count($scheme_code);
        //dd($total_scheme_count);

        foreach ($scheme_code as $scheme) {
            $key = array_search($scheme, $file);
            //echo $file[$key]."<br>";
            $line_exp = explode(";", $file[$key]);                    
            $date = date("Y-m-d",strtotime($line_exp[5]));
            \Log::info($date.' : '.$line_exp[0].' : '.$line_exp[4]);
            $store_nav = new DailyNav();
            $store_nav->scheme_code = $line_exp[0];
            $store_nav->nav_value = $line_exp[4];
            $store_nav->date = $date;
            $store_nav->save();

            //echo $line_exp[0]."<br>";

            //Storing in historic_navs

            $historic_navs = new HistoricNav();
            $historic_navs->scheme_code = $line_exp[0];
            $historic_navs->scheme_name = $line_exp[3];
            $historic_navs->date = $date;
            $historic_navs->nav = $line_exp[4];
            $historic_navs->save();
        }

        //set_time_limit(60);

         //dd(HistoricNav::where('date','2017-10-06')->count());

         //$this->deleteSchemeDetails('112496');

   
    }


    
}
