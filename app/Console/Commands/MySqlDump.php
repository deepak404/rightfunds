<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MySqlDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
 protected $signature = 'db:mysqldump';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the mysqldump utility';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $dbhost = 'localhost:3036';
       $dbuser = 'root';
       $dbpass = '';
       $dbname = 'rightfund';
       
       $backup_file = $dbname . date("Y-m-d-H-i-s") . '.gz';
       $command = "mysqldump --opt -h $dbhost -u $dbuser -p $dbpass ". "test_db | gzip > $backup_file";
       
       system($command);
    }
}