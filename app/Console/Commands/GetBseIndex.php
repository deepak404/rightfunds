<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\historic_bse;
use App\smallcap;

class GetBseIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:bse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get BSE sensex and small cap index value daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sensex = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=16');

        foreach ($sensex as $value) {

                $array = explode("@",$value);
                $date_split = explode("|", $array[9]);
                $mydate = date('Y-m-d', strtotime($date_split[0]));
                // dd($array)
                $save = historic_bse::insert(['date' => $mydate, 
                                                 'opening_index' => $array[2],
                                                 'high' => $array[3],
                                                 'low' => $array[4],
                                                 'closing_index' => $array[5]
                                            ]);
        }



        $smallcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=82');

        foreach ($smallcap_data as $value) {

                $array = explode("@",$value);
                $date_split = explode("|", $array[9]);
                $mydate = date('Y-m-d', strtotime($date_split[0]));
                $save = smallcap::insert(['date' => $mydate, 
                                                 'open' => $array[2],
                                                 'high' => $array[3],
                                                 'low' => $array[4],
                                                 'close' => $array[5]
                                            ]);


        }
    }
}
