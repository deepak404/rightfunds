<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\InvestmentDetails;
use App\SchemeDetails;
use App\PortfolioDetails;

class BseFutureOrders extends Command
{

     protected $pass_key = 'abcdef7895';
     protected $password = 'Pwm$2017';
     protected $response_key;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:bsefuture';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To Execute all the future orders from rightfunds platform on BSE';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bse_order_response = array();

        $future_investment = InvestmentDetails::where('investment_type','1')->where('investment_date',date('Y-m-d'))->where('investment_status',0)->get();

        foreach ($future_investment as $investment) {
            $portfolio_details = $investment->portfolioDetails;

            $bse_date = date('Y-m-d');
            $bse_date = explode('-', $bse_date);
            $bse_date = $bse_date[0].$bse_date[1].$bse_date[2];
            $time = explode('.', microtime());
            $time = substr($time[1], 0 ,6);

            $unique_ref_no = $bse_date."12456".$time;
            $int_ref_no = $unique_ref_no;


            foreach ($portfolio_details as $portfolio) {
                $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$portfolio['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                if (count($user_folio) == 0) {
                    $folio_number = ''; 
                }else{
                    $folio_number = $user_folio['folio_number'];

                }

                $this->getPassword();
                                                        

                $transact_mode = 'NEW';
                $buy_sell = 'P';

                if ($folio_number == '') {
                    $buy_sell_type = 'FRESH'; //FRESH OR ADDITIONAL WILL COME HERE; 
                }else{
                    $buy_sell_type = 'ADDITIONAL'; //FRESH OR ADDITIONAL WILL COME HERE;
                }


                $bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$portfolio['scheme_code']],$buy_sell,$buy_sell_type,$portfolio['amount'],$folio_number,$int_ref_no,$investment_id);

            }//portfolio details foreach ends 
        }// future investment array ends


        foreach ($bse_order_response as $key => $value) {
            $date = date('d/m/Y h:i:s A', strtotime($date));
            PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $date]);
        } //foreach ends
    }


    public function getPassword(){


        $soap_request = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
           <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
           <wsa:To>http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc</wsa:To>
           </soap:Header>
           
           <soap:Body>
              <bses:getPassword>
                 <!--Optional:-->
                 <bses:UserId>1245601</bses:UserId>
                 <!--Optional:-->
                 <bses:Password>'.$this->password.'</bses:Password>
                 <!--Optional:-->
                 <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
              </bses:getPassword>
           </soap:Body>
        </soap:Envelope>';

        //echo "hello";


        $result = $this->makeSoapCall($soap_request);
        // var_dump($result);
        // die();
        $result = explode('http://bsestarmf.in/MFOrderEntry/getPasswordResponse', $result);
        $response = explode('|', $result[1]);

        //var_dump($response);
        //echo $response[1];

        ///preg_match('/<getPasswordResult>-(.*?)-<getPasswordResult>/', $result, $pass_response);


       // dd($pass_response);

        $pwd_response =  $response[0];
        $this->response_key = $response[1];

        //echo $this->response_key;

      }

      

      public function placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code,$buy_sell,$buy_sell_type,$inv_amount,$folio_number,$int_ref_no,$investment_id){

        //dd($this->response_key);



        /*
              Order Response will be in this order. Kindly don't go through the BSE web file documentation.
              That will only confuse you. 

              The order response will be in this format.

              TRANSACTION_CODE|UNIQUE REFERENCE NUMBER|BSE ORDER NO|USER ID|MEMBER ID|CLIENT CODE|BSE REMARKS|SUCCESS FLAG

              UNIQUE REFERENCE NUMBER = $unique_ref_no //this should be unique for each order. This will be tracking for the order.
              BSE ORDER NO //This is generated by BSE
              TRANSACTION CODE // NEW||MOD||CXL -- New order || Modification || Cancel.
              SUCCESS FLAG // 0 == ORDER SUCCESS and 1 = ORDER FAILURE *What a Brilliant thing by BSE* 

        */

        $response_key = explode('</getPasswordResult></getPasswordResponse></s:Body></s:Envelope>', $this->response_key);



        $soap_request = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
           <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
           <wsa:To>http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc</wsa:To>
           </soap:Header>
       <soap:Body>
          <bses:orderEntryParam>
             <!--Optional:-->
             <bses:TransCode>'.$transact_mode.'</bses:TransCode>
             <!--Optional:-->
             <bses:TransNo>'.$unique_ref_no.'</bses:TransNo>
             <!--Optional:-->
             <bses:OrderId></bses:OrderId>
             <!--Optional:-->
             <bses:UserID>1245601</bses:UserID>
             <!--Optional:-->
             <bses:MemberId>12456</bses:MemberId>
             <!--Optional:-->
             <bses:ClientCode>'.$pan.'</bses:ClientCode>
             <!--Optional:-->
             <bses:SchemeCd>'.$bse_scheme_code.'</bses:SchemeCd>
             <!--Optional:-->
             <bses:BuySell>'.$buy_sell.'</bses:BuySell>
             <!--Optional:-->
             <bses:BuySellType>'.$buy_sell_type.'</bses:BuySellType>
             <!--Optional:-->
             <bses:DPTxn>P</bses:DPTxn>
             <!--Optional:-->
             <bses:OrderVal>'.$inv_amount.'</bses:OrderVal>
             <!--Optional:-->
             <bses:Qty></bses:Qty>
             <!--Optional:-->
             <bses:AllRedeem>N</bses:AllRedeem>
             <!--Optional:-->
             <bses:FolioNo>'.$folio_number.'</bses:FolioNo>
             <!--Optional:-->
             <bses:Remarks></bses:Remarks>
             <!--Optional:-->
             <bses:KYCStatus>Y</bses:KYCStatus>
             <!--Optional:-->
             <bses:RefNo>'.$int_ref_no.'</bses:RefNo>
             <!--Optional:-->
             <bses:SubBrCode></bses:SubBrCode>
             <!--Optional:-->
             <bses:EUIN>E173580</bses:EUIN>
             <!--Optional:-->
             <bses:EUINVal>N</bses:EUINVal>
             <!--Optional:-->
             <bses:MinRedeem>N</bses:MinRedeem>
             <!--Optional:-->
             <bses:DPC>N</bses:DPC>
             <!--Optional:-->
             <bses:IPAdd></bses:IPAdd>
             <!--Optional:-->
             <bses:Password>'.$response_key[0].'</bses:Password>
             <!--Optional:-->
             <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
             <!--Optional:-->
             <bses:Parma1></bses:Parma1>
             <!--Optional:-->
             <bses:Param2></bses:Param2>
             <!--Optional:-->
             <bses:Param3></bses:Param3>
          </bses:orderEntryParam>
       </soap:Body>
    </soap:Envelope>';

    //dd($soap_request);

        $result = $this->makeSoapCall($soap_request);
        //echo $result;



        $response = explode('http://bsestarmf.in/MFOrderEntry/orderEntryParamResponse', $result);
        $response = explode('|', $response[1]);

        $trans_code = explode('</a:Action></s:Header><s:Body><orderEntryParamResponse xmlns="http://bsestarmf.in/"><orderEntryParamResult>', $response[0]);
        $order_response = explode('</orderEntryParamResult></orderEntryParamResponse></s:Body></s:Envelope>', $response[7]);

        $order_response = array(
            'trans_code' => $trans_code[1],
            'unique_ref_no' => $response[1],
            'order_no' =>$response[2],
            'client_code' => $response[5],
            'remarks' =>$response[6],
            'order_status' => $order_response[0],
            'inv_id' => $investment_id,
          );

        $this->place_order_count++;
        return $order_response;
        

      }

      public function makeSoapCall($soap_request){

        //dd($soap_request);
        $header = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
            "Accept: application/soap+xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: ".strlen($soap_request),
        );
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL,"http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc?singleWsdl" );
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do, CURLOPT_POST,           true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
        $result = curl_exec($soap_do);
        //dd($result);
        return $result;
      }
}
