<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioDetails extends Model
{
     protected $table = "portfolio_details";


    public function investmentDetails()
    {
        return $this->belongsTo('App\InvestmentDetails','investment_id');
    }

    public function schemeDetails()
    {
    	return $this->belongsTo('App\SchemeDetails');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function sip(){
    	return $this->belongsTo('App\Sip');
    }

    public function sipDetails(){
        return $this->belongsTo('App\SipDetails');
    }

}
