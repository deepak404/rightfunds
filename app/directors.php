<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directors extends Model
{
	protected $table = 'directors';
	
    public function user(){
    	return $this->hasMany('App\User');
    }
}
