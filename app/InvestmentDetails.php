<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentDetails extends Model
{
	protected $table='investment_details';
    protected $fillable = [
        'user_id','pan','investment_amount','investment_status','investment_date','payment_type'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function portfolioDetails()
    {
    	return $this->hasMany('App\PortfolioDetails','investment_id');
    }

    public function sip()
    {
    	return $this->hasOne('App\Sip', 'port_id');
    }
}
