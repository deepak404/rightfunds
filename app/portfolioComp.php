<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class portfolioComp extends Model
{
    protected $table = "portfolio_comps";

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
