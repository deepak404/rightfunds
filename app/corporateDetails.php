<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorporateDetails extends Model
{	
	 protected $table = "corporate_details";

     public function user()
    {
        return $this->belongsTo('App\User');
    }
}
