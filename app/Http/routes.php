<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	$user = Auth::user();
    if (!$user) {
    	return view('welcome');
    } else {
    	if($user->role == 1) {
    		return redirect('/admin/overview');
    	} else {
    		return redirect('/home');
    	}
    }
});





Route::get('/register', function() {
	return view('users.version2.register');
});

Route::get('/media-kit', function() {
	return view('media-kit');
});


// Route::get('/create_user',[
// 	'uses' => 'adminController@createUser',
// 	'as' => 'createUser',
// ]);

Route::get('/add_bank_code',[
	'uses' => 'adminController@addBankCode',
	'as' => 'addBankCode',
]);


/*Route::post('/get_sip_calculated', [
	'uses' => 'acctStatementController@sipCalculator',
	'as' => 'sipCalculator',
]);*/



Route::get('/check_payment',[
	'uses' => 'acctStatementController@checkPayment',
	'as' => 'getBsePassword',
]);


// Route::get('/get_bse_pass',[
// 	'uses' => 'fundController@getPassword',
// 	'as' => 'getBsePassword',
// ]);


// Route::get('/show_future',[
// 	'uses' => 'userController@showFutureOrders',
// 	'as' => 'showFutureOrders',
// ]);

Route::get('/blog', function(){
	return view('blog.index');
});


Route::get('/blog/why_should_i_invest_my_money', function() {
    return view('blog.article1');
});

Route::get('/blog/introduction_to_mutual_funds', function() {
    return view('blog.article2');
});

Route::get('/blog/how_do_i_save_on_taxes_with_mutual_funds', function() {
    return view('blog.article3');
});

Route::get('/blog/how_to_make_returns_like_warren_buffet', function() {
    return view('blog.article4');
});

Route::get('/blog/budget_for_the_2018_19_fiscal_year', function() {
    return view('blog.article5');
});

Route::get('/blog/what_is_systematic_investment_plan', function() {
    return view('blog.article6');
});

Route::get('/blog/benchmark_indices_vs_mutual_funds', function() {
    return view('blog.article7');
});


Route::post('/send_otp',[
	'uses' => 'userController@sendOtp',
	'as' => 'sendOtp',
]);

Route::post('/send_contact_us_email',[
	'uses' => 'userController@sendContactUsEmail',
	'as' => 'sendContactUsEmail',
]);

Route::post('/verify_otp',[
	'uses' => 'userController@verifyOtp',
	'as' => 'verifyOtp',
]);

Route::get('/get_onetime_investment_date',[
	'uses' => 'userController@getOnetimeInvestmentDate',
	'as' => 'getOnetimeInvestmentDate',
]);


Route::get('/get_remaining_inv',[
	'uses' => 'fundController@checkRemainingInvestments',
	'as' => 'getOnetimeInvestmentDate',
]);


Route::get('/privacy_policy', function() {
	return view('privacy_policy');
});


Route::get('/faq', function() {
	return view('faq');
});

/*Route::get('/txtrobot', function() {
	return view('robots');
});

Route::get('/mapsite', function() {
	return 'sitemap.xml';
});*/

Route::get('/about-us', function() {
	return view('about_us');
});

Route::get('/terms', function() {
	return view('terms');
});

Route::get('/contact-us', function() {
	return view('contact_us');
});



Route::get('/login', function() {
	return view('users.login');
});


/*Route::get('/preference',[
	'uses' => 'userController@preference',
	'as' => 'preference',
]);
*/

Route::get('/test',[
	'uses' => 'testController@test',
	]);


Route::get('/testnach',[
	'uses' => 'testController@testnach',
	]);

Route::post('/getsipreturn',[
		'uses' => 'adminController@getSipReturn',
		'as' => 'getSipReturn',
	]);




/*Middleware Group Starts*/

Route::group(['middleware' => ['auth','user']], function () {


	Route::get('/sign-up', [
	'uses' => 'userController@signUp',
	]);


	
	Route::get('/account_activation', [
	'uses' => 'userController@activateAccount',
	'as'=> 'accountActivation'
	]);


	Route::post('/personal_details',[
		'uses' => 'userController@personalDetails',
		'as' => 'personalDetails',
	]);

	Route::post('/get_active_user_details',[
		'uses' => 'userController@getActiveDetails',
		'as' => 'getActiveDetails',
	]);

	Route::post('/activate_bank_form',[
		'uses' => 'userController@saveNewBankDetails',
		'as' => 'saveNewBankDetails',
	]);

	Route::post('/user_documents',[
		'uses' => 'userController@userDocument',
		'as' => 'userDocument',
	]);


	Route::post('/change_image',[
		'uses' => 'userController@storeProfileImage',
		'as' => 'storeProfileImage',
	]);

	Route::post('/corporate_details',[
		'uses' => 'userController@corporateDetails',
		'as' => 'corporateDetails',
	]);

	Route::post('/add_director',[
		'uses' => 'userController@addDirector',
		'as' => 'addDirector',
	]);

	Route::get('/check_director',[
		'uses' => 'userController@checkDirector',
		'as' => 'checkDirector',
	]);

	Route::post('/remove_director',[
		'uses' => 'userController@removeDirector',
		'as' => 'removeDirector',
	]);

	Route::post('/reg_check',[
		'uses' => 'userController@regCheck',
		'as' => 'regCheck',
	]);


	Route::get('/investment_history/{period?}',[
		'uses' => 'acctStatementController@showInvestmentHistory',
		'as' => 'investmentHistory ',
	]);

	// Route::post('/filter_investment_history',[
	// 	'uses' => 'acctStatementController@filterInvestmentHistory',
	// 	'as' => 'filterInvestmentHistory ',
	// ]);

	Route::get('/withdraw_funds',[
		'uses' => 'acctStatementController@showWithdrawFunds',
		'as' => 'withdrawFunds',
	]);

	Route::post('/withdraw_user_funds',[
		'uses' => 'acctStatementController@withdrawFunds',
		'as' => 'withdrawUserFunds',
	]);

	Route::get('/tax_saving',[
		'uses' => 'acctStatementController@showTaxSaving',
		'as' => 'taxSaving',
	]);

	Route::get('/portfolio_details',[
		'uses' => 'acctStatementController@showPortfolioDetails',
		'as' => 'portfolioDetails',
	]);

	Route::post('/get_investment_history',[
		'uses' => 'acctStatementController@getInvestmentHistory',
		'as' => 'getInvestmentHistory',
	]);


	Route::get('/home',[

		'uses' => 'userController@showHome',
		'as' => 'homePage',

	]);  
	
	Route::get('/preference',[
		'uses' => 'userController@preference',
		'as' => 'preference',
	]);

	Route::post('/change_password',[
		'uses' => 'userController@changePassword'
	]);
	
	Route::post('/save_user_details',[
		'uses' => 'userController@saveUserDetails',
		'as' => 'saveUserDetails',
	]);

	Route::post('/save_bank_details',[
		'uses' => 'userController@saveBankDetails',
		'as' => 'saveBankDetails',
	]);

	Route::post('/save_nominee1_details',[
		'uses' => 'userController@saveNominee1Details',
		'as' => 'saveNominee1Details',
	]);

	Route::post('/save_nominee2_details',[
		'uses' => 'userController@saveNominee2Details',
		'as' => 'saveNominee2Details',
	]);

	Route::post('/save_nominee3_details',[
		'uses' => 'userController@saveNominee3Details',
		'as' => 'saveNominee3Details',
	]);


	Route::get('/send_email',[
		'uses' => 'userController@SendEmail'
	]);

	Route::post('/save_goal',[
		'uses' => 'userController@saveGoal',
		'as' => 'saveGoal',
	]);
	

	Route::get('/get_user_portfolio_document',[
		'uses' => 'acctStatementController@getUserPortfolioDocument',
		'as' => 'getUserPortfolioDocument',
	]);

	Route::post('/save_notify_mode',[
		'uses' => 'fundController@saveNotifyMode',
		'as' => 'saveNotifyMode',
	]);

	// Route::get('/ekyc',[
	// 	'uses' => 'userController@ekyc',
	// 	'as' => 'ekyc'
	// ]);

	Route::get('/ekyc',[
		'uses' => 'userController@ekyc1',
		'as' => 'ekyc1'
	]);

	Route::post('/upload_video_kyc',[
		'uses' => 'acctStatementController@uploadKycVideo',
		'as' => 'uploadKycVideo',
	]);

	Route::post('/submit_video_kyc',[
		'uses' => 'acctStatementController@saveKycVideo',
		'as' => 'saveKycVideo',
	]);

	Route::post('/get_investment_details',[
		'uses' => 'acctStatementController@getInvestmentDetails',
		'as' => 'getInvestmentDetails',
	]);

	Route::post('/cancel_investment',[
		'uses' => 'acctStatementController@cancelInvestment',
		'as' => 'cancelInvestment',
	]);

	
	Route::get('/mandate_status',[
		'uses' => 'userController@getMandateStatus',
		'as' => 'mandate_status'
	]);

	Route::get('/make_payment/{id}',[
		'uses' => 'BseController@PaymentGatewayIntegration',
		'as' => 'getBsePassword',
	]);

	Route::get('/payment_response/{id}',[
		'uses' => 'BseController@paymentResponse',
		'as' => 'paymentResponse',
	]);

	Route::get('/handle_payment',[
		'uses' => 'userController@handlePayment',
		'as' => 'handlePayment',
	]);



});

/*Route::group(['middleware' => ['auth','user']],function(){

	Route::get('/fund_selector_d',[
		'uses' => 'userController@showFundSelector',
		'as' => 'fundSelector',
	]);


});*/


Route::get('/testnoti',[

	'uses' => 'adminController@getNoti',
]);





/*Middleware Group ends*/

/*KYC Middleware starts*/
Route::group(['middleware' => ['auth','user','kyc']], function () {

	Route::get('/fund_selector',[
		'uses' => 'userController@showFundSelector',
		'as' => 'fundSelector',
	]);

	Route::get('/fund_selector1',[
		'uses' => 'userController@showFundSelector1',
		'as' => 'fundSelector',
	]);

	Route::post('/showFunds',[
		'uses' => 'fundController@showFunds',
		'as' => 'showFunds',
	]);

	Route::post('/showSchemeDetails',[
		'uses' => 'fundController@showSchemeDetails',
		'as' => 'showSchemeDetails',
	]);

	Route::post('/showPendingOrders',[
		'uses' => 'fundController@showPendingOrders',
		'as' => 'showPendingOrders',
	]);

	Route::post('/showCustomFunds',[
		'uses' => 'fundController@showCustomFunds',
		'as' => 'showCustomFunds',
	]);

	Route::post('/investCustomFunds',[
		'uses' => 'fundController@investCustomFunds',
		'as' => 'investCustomFunds',
	]);

	Route::post('/investFunds',[
		'uses' => 'fundController@investFunds',
		'as' => 'investFunds',
	]);

	Route::post('/get_scheme_performance_details',[
		'uses' => 'acctStatementController@getSchemePerformanceDetails',
		'as' => 'getSchemePerformanceDetails',
	]);


	Route::get('/get_mandate_link',[
		'uses' => 'userController@getMandateLink',
	]);

	
});
/*KYC Middleware ends*/

Route::group(['prefix'=>'admin' , 'middleware'=>['auth','admin']],function(){

		Route::post('/add_mandate_link',[
			'uses' => 'adminController@addMandateLink',
		]);

		Route::post('/nach_complete',[
			'uses' => 'adminController@nachComplete'
		]);

		Route::post('/update_priority_info',[
			'uses' => 'adminController@updatePriorityInfo',
		]);

		Route::post('/update_bank_info',[
			'uses' => 'adminController@updateBankInfo',
		]);

		Route::post('/update_nominee_info',[
			'uses' => 'adminController@updateNomineeInfo',
		]);

		Route::post('/update_address_info',[
			'uses' => 'adminController@updateAddressInfo',
		]);

		Route::post('/update_personal_info',[
			'uses' => 'adminController@updatePersonalInfo',
		]);

		Route::get('/overview',[
			'uses' => 'adminController@adminOverview',
			'as' => 'adminOverview',
		]); 

		Route::get('/amc',[
			'uses' => 'adminController@adminAmc',
			'as' => 'adminAmc',
		]); 

		Route::get('/kyc',[
			'uses' => 'adminController@adminKyc',
			'as' => 'adminKyc',
		]); 

		Route::post('/showUserKyc',[
			'uses' => 'adminController@showUserKyc',
			'as' => 'showUserKyc',
		]); 

		Route::post('/file_upload',[
			'uses' => 'adminController@fileUpload',
		]);

		Route::get('/nach/{nach_status?}',[
			'uses' => 'adminController@newNach',
			'as' => 'adminNewNach',
		]);

		Route::get('/nach1',[
			'uses' => 'adminController@newNach',
			'as' => 'adminNewNach',
		]);

		Route::post('/get_user_nach_details',[
			'uses' => 'adminController@getUserNachDetails',
			'as' => 'adminNewNach',
		]);

		Route::post('/update_user_nach_details',[
			'uses' => 'adminController@updateUserNachDetails',
			'as' => 'adminUpdateNach',
		]);

		

		Route::get('/pending_orders',[
			'uses' => 'adminController@newpendingOrders',
			'as' => 'pendingOrders',
		]);

		Route::get('/sip_orders',[
			'uses' => 'adminController@pendingSipOrders',
			'as' => 'pendingSipOrders',
		]);

		Route::get('/pending_orders1',[
			'uses' => 'adminController@newPendingOrders',
			'as' => 'pendingOrders',
		]);

		Route::get('/export_pending_orders',[
			'uses' => 'adminController@exportPendingOrders',
			'as' => 'exportpendingOrders',
		]);

		Route::get('/portfolio_details/{id}',[
			'uses' => 'adminController@portfolioDetails',
			'as' => 'portfolioDetails',
		]);

		Route::post('/update_portfolio_details',[
			'uses' => 'adminController@updatePortfolioDetails',
			'as' => 'updatePortfolioDetails',
		]);

		Route::get('/tax_saving/{id}',[
			'uses' => 'adminController@taxSaving',
			'as' => 'taxSaving',
		]);

		Route::get('/pending_details/{type}/id/{id}',[
			'uses' => 'adminController@pendingDetails',
			'as' => 'pendingDetails',
		]);

		Route::post('/updateBseDate',[
			'uses' => 'adminController@updateBseDate',
			'as' => 'updateBseDate',
		]);

		Route::post('/updateSipRegDate',[
			'uses' => 'adminController@updateSipRegDate',
			'as' => 'updateSipRegDate',
		]);

		Route::post('/updateSipRegNo',[
			'uses' => 'adminController@updateSipRegNo',
			'as' => 'updateSipRegNo',
		]);

		Route::post('/updateSipBseDate',[
			'uses' => 'adminController@updateSipBseDate',
			'as' => 'updateSipBseDate',
		]);

		Route::post('/checkSipReg',[
			'uses' => 'adminController@checkSipReg',
			'as' => 'checkSipReg',
		]);

		Route::post('/completePendingOrders',[
			'uses' => 'adminController@completePendingOrders',
			'as' => 'completePendingOrders',
		]);

		Route::get('/inv_history/{id}',[
			'uses' => 'adminController@investmentHistory',
			'as' => 'investmentHistory',
		]);


		Route::get('/find',[
			'uses' => 'adminController@userSearch',
		]);

		Route::get('/find_nach',[
			'uses' => 'adminController@findNach',
		]);

		Route::get('/order_history',[
			'uses' => 'adminController@orderHistory',
		]);

		Route::get('/all_sip',[
			'uses' => 'adminController@getAllSip',
		]);

		Route::post('/save_nach',[
			'uses' => 'adminController@saveNach',
			'as' => 'saveNach',
		]);

		Route::post('/save_pending_orders',[
			'uses' => 'adminController@savePendingOrders',
			'as' => 'savePendingOrders',
		]);

		Route::post('/manual_withdraw',[
			'uses' => 'adminController@manualWithdraw',
			'as' => 'manualWithdraw',
		]);

		Route::get('/order_history',[
			'uses' => 'adminController@orderHistory',
			'as' => 'orderHistory',
		]);

		Route::get('/order_history1',[
			'uses' => 'adminController@newOrderHistory',
			'as' => 'newOrderHistory',
		]);


		Route::post('/revoke_orders',[
			'uses' => 'adminController@revokeOrders',
			'as' => 'revokeOrders',
		]);



		Route::get('/modify_details/{type}/id/{id}',[
			'uses' => 'adminController@modifyDetails',
			'as' => 'modifyDetails',
		]);

		Route::get('/view_details/{type}/id/{id}',[
			'uses' => 'adminController@viewDetails',
			'as' => 'viewDetails',
		]);

		Route::post('/modify_investment',[
			'uses' => 'adminController@saveModifyOrders',
			'as' => 'saveModifyOrders',
		]);


		Route::post('/cancel_pending_order',[
			'uses' => 'adminController@cancelPendingOrder',
			'as' => 'cancelPendingOrder',
		]);

		Route::post('/get_ind_user_order_history',[
			'uses' => 'adminController@getIndUserOrderHistory',
			'as' => 'getIndUserOrderHistory',
		]);

		Route::get('/get_user_investment_performance',[
			'uses' => 'adminController@getNewUserInvestmentPerformance',
			'as' => 'getNewUserInvestmentPerformance',
		]);

		Route::post('/get_ind_user_investment_performance',[
			'uses' => 'adminController@getIndUserInvestmentPerformance',
			'as' => 'getIndUserInvestmentPerformance',
		]);

		Route::post('/get_ind_user_portfolio_performance',[
			'uses' => 'adminController@getIndUserPortfolioPerformance',
			'as' => 'getIndUserPortfolioPerformance',
		]);

		Route::post('/get_user_pending_orders',[
			'uses' => 'adminController@getIndUserPendingOrders',
			'as' => 'getIndUserPendingOrders',
		]);

		Route::get('/manage_schemes',[
			'uses' => 'adminController@manageSchemes',
			'as' => 'manageSchemes',
		]);

		Route::post('/add_scheme',[
			'uses' => 'adminController@addScheme',
			'as' => 'addScheme',
		]);

		Route::post('/get_update_scheme_data',[
			'uses' => 'adminController@getUpdateSchemeData',
			'as' => 'getUpdateSchemeData',
		]);

		Route::post('/update_scheme_data',[
			'uses' => 'adminController@updateSchemeDetails',
			'as' => 'updateSchemeDetails',
		]);

		Route::post('/update_allotment_status',[
			'uses' => 'adminController@updateAllotmentStatus',
			'as' => 'updateAllotmentStatus',
		]);

		Route::post('/update_sip_allotment_status',[
			'uses' => 'adminController@updateSipAllotmentStatus',
			'as' => 'updateSipAllotmentStatus',
		]);

		Route::post('/update_redemption_status',[
			'uses' => 'adminController@updateRedemptionStatus',
			'as' => 'updateRedemptionStatus',
		]);

		Route::post('/add_manual_portfolio',[
			'uses' => 'adminController@addManualPortfolio',
			'as' => 'addManualPortfolio',
		]);

		Route::get('/super_login/{id}',[
			'uses' => 'adminController@superLogin',
			'as' => 'superLogin',
		]);

		Route::get('/get_nav',[

			'uses' => 'NavController@getNav',

			]);

		Route::get('/not_found',function(){
			return view('errors.404');
		});

		Route::get('/get_ifsc',[
			'uses' => 'userController@getIfsc',
			'as' => 'ifsc',
		]);

		Route::post('/bpo_status_update','adminController@bpoUpdate');

		Route::post('/filter_list','adminController@filterBystatus');

		// Route::get('/inv_cg','adminController@investmenStatus');

		Route::get('/switch','adminController@changeDate');
});




Route::auth();
