<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PHPExcel_Calculation_Financial;
use App\Http\Controllers\userController;
use App\Http\Requests;
use App\PersonalDetails;
use App\CorporateDetails;
use App\User;
use App\Directors;
use App\portfolioComp;
use App\DailyNav;
use App\NomineeDetails;
use App\HistoricNav;
use App\InvestmentDetails;
use App\PortfolioDetails;
use App\SchemeDetails;
use App\WithdrawDetails;
use App\NavDetails;
use App\NachDetails;
use Carbon\Carbon;
use App\ekyc;
use Excel;
use SoapClient;
use SoapHeader;

class acctStatementController extends Controller
{

    protected $pass_key = 'abcdef7895';
    protected $password = 'Pwm$2017';
    protected $response_key;
    
    public function showInvestmentHistory($period = null)
    {
        if($period == null){
            $period = 6;    
        }

        // dd($period);

        $total_investment_details = $this->getInvestmentHistoryDetails($period, 'data');Log::info($total_investment_details);
        $inv_date = array();
        //dd($total_investment_details);  
            foreach ($total_investment_details as $key => $values) {
                //$values[]['portfolio_type'] = $key;
                foreach ($values as $value) {

                    
                    if (array_key_exists($value['investment_date'], $inv_date)) {

                        $inv_date[$value['investment_date']][] = $value;
                    }else{
                        
                        $inv_date[$value['investment_date']][] = $value;                      
                    }
                }
            }


            unset($inv_date[""]);
            //dd($inv_date);
             $temp = array();
             foreach($inv_date as $key=>$value){
                 $temp[strtotime($key)] = $value;
             }
             krsort($temp);

             $inv_date = array();
             foreach($temp as $key=>$value){
                  $inv_date[date("d-m-Y",$key)] = $value;
             }
             //dd($inv_date);
            //dd($inv_date);
            //$investment_details[] = $inv_date;
    	return view('users.version2.investment-history')->with('investment_details',$inv_date,'period',$period);
    }//showInvestmentHistory() function ends


    public function showWithdrawFunds(){
    	$portfolio_investment_details = array();
        $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
        $current_date = new Carbon();
        $current_date->subDay();
        $current_date = $current_date->format('Y-m-d');
        $scheme_code_name = $scheme_details->toArray();
        //$nav_code_value = $nav_details->toArray();
        $user= \Auth::user();
        $portfolio_types = array('debt', 'bal', 'eq', 'ts');
        $total_investment_current_value = 0;
        $ts_year_eligibility = Carbon::now()->subDays(1095)->toDateString();

        $withdraw_details = WithdrawDetails::where('user_id',\Auth::user()->id)->where('withdraw_status','0')->get();
        //dd($pending_withdraws);
        $pending_withdraws = array();
        //dd(count($withdraw_details));

        foreach ($withdraw_details as $withdraw) {
            //$pending_withdraws[$withdraw->scheme_code] = $withdraw->withdraw_amount;

            if (array_key_exists($withdraw->scheme_code, $pending_withdraws)) {
                $pending_withdraws[$withdraw->scheme_code]['amount'] += $withdraw->withdraw_amount;
            }else{
                $pending_withdraws[$withdraw->scheme_code]['amount'] = $withdraw->withdraw_amount;
            }
        }

        //dd($pending_withdraws);

        //processing all active funds to create withdraw options
        foreach ($portfolio_types as $portfolio_type) {

            //checking for tax saver eligibilty for withdrawl
            if ($portfolio_type == "ts") {
                
                $portfolio_details = $user->portfolioDetails()
                ->where('investment_date','<', $ts_year_eligibility)
                ->where('portfolio_type', $portfolio_type)
                ->where('portfolio_status', 1)
                ->get();
                $portfolio_details = $portfolio_details->groupBy('scheme_code');
            } else {
                $portfolio_details = $user->portfolioDetails
                ->where('portfolio_type', $portfolio_type)
                ->where('portfolio_status', 1)
                ->groupBy('scheme_code')
                ->all();if($portfolio_type == "debt"){}
            }
            $total_portfolio_current_value = 0;
            foreach($portfolio_details as $code => $portfolio_detail) {
                $nav_code_value = NavDetails::where('scheme_code',$code)->orderBy('date', 'desc')->first();
                $units_held = $portfolio_detail->sum('units_held');
                $nav_value = $nav_code_value->nav_value;
                $current_value = round(($units_held * $nav_value),2);

                //checking for tax saver portfolio details eligibility
                $portfolio_investment_details[$portfolio_type][]=array(
                    'scheme_code' => $code,
                    'scheme_name' => $scheme_code_name[$code],
                    'current_value' => $current_value,
                );
                $total_portfolio_current_value += $current_value;
            }
            $total_investment_current_value += $total_portfolio_current_value;
        }
        if($total_investment_current_value > 0){
            $portfolio_investment_details['total_current_value'] = $total_investment_current_value;
        }

        $portfolio_details = $portfolio_investment_details;

        return view('users.version2.withdraw-details')->with(compact('portfolio_details','pending_withdraws'));
        //return view('users.withdraw_funds')->with('portfolio_details',$portfolio_investment_details);
    }//showWithdrawFunds() function ends

    public function showTaxSaving(){
    	return view('users.version2.tax-saving');
    }//showTaxSaving() function ends

    //function to prepare user portfolio details
    public function showPortfolioDetails() 
    {
        $portfolio_investment_details = $this->userPortfolioDetails($response_type = "data");
    	return view('users.version2.portfolio-details')->with('portfolio_details',$portfolio_investment_details);
    }//showPortfolioDetails() function ends

    public function getInvestmentHistory(Request $request)
    {
        $response_type = 'document';
        $period = 6;
        $request = $request->all();
        if(array_key_exists('investment_period', $request)) {
            $period = $request['investment_period'];
        }
        if(array_key_exists('response_type', $request)) {
            $response_type = $request['response_type'];
        }
        
        $total_investment_details = $this->getInvestmentHistoryDetails($period, $response_type);
        if($response_type == 'data') {
            return response()->json(['msg' => 'success', 'response' => $total_investment_details]);
        } else {
            $file_content = Excel::create('Investment Summary', function($excel) use ($total_investment_details) {
                $excel->sheet('Investment Summary', function($sheet) use ($total_investment_details)
                {
                    $sheet->fromArray($total_investment_details);
                });
            })->download('xlsx');
        }
        
    }

    public function getInvestmentHistoryDetails($period, $response_type)
    {
        $total_investment_details = array();
        $investment_period;


        //getting Investment period time as 12 months , 24 months and the default is 6 Months
        switch ($period) {
            case '12': {
                $investment_period = Carbon::now()->subMonths(12)->toDateString();
                break;
            }
            case '3': {
                $investment_period = Carbon::now()->subMonths(3)->toDateString();
                break;
            }

            case '6': {
                $investment_period = Carbon::now()->subMonths(6)->toDateString();
                break;
            }

            case '1': {
                $investment_period = Carbon::now()->subMonths(1)->toDateString();
                break;
            }

            default: {
                $investment_period = Carbon::now()->subMonths(6)->toDateString();
                break;
            }
        }Log::info($investment_period);
        $user= \Auth::user();
        $scheme_types = array('One Time', 'Future Investment', 'Monthly Investment');
        $scheme_status = array('Processing', 'Success', 'Failed','Cancelled');
        $total_investment_amount = 0;
        $total_units_held = 0;
        $toal_investment_current_value = 0;
        $investment_details = $user->investmentDetails()
            ->where('investment_date', '>', $investment_period)
            ->orderBy('investment_date','asc')
            ->get();

        //dd($investment_details);
        $total_debt_investment = 0;
        $total_equity_investment = 0;
        $total_balanced_investment = 0;
        $total_ts_investment = 0;
        //$document_response =

        //adding investment details of the user
        foreach($investment_details as $investment_detail) {

            //getting portfolio details of the particular investment and storing it in $portfolio_details
            $portfolio_details = $investment_detail->portfolioDetails->all();
            $debt_investment = 0;
            $equity_investment = 0;
            $balanced_investment = 0;
            $ts_investment = 0;


            //below foreach is to find out the total amount invested in particular portfolio type and adding it to respective variable

            foreach($portfolio_details as $portfolio_detail) {
                if($portfolio_detail->portfolio_type == 'debt'){
                    $debt_investment += $portfolio_detail->amount_invested;
                } elseif($portfolio_detail->portfolio_type == 'eq') {
                    $equity_investment += $portfolio_detail->amount_invested;
                } elseif($portfolio_detail->portfolio_type == 'bal') {
                    $balanced_investment += $portfolio_detail->amount_invested;
                } elseif($portfolio_detail->portfolio_type == 'ts') {
                    $ts_investment += $portfolio_detail->amount_invested;
                }

            } // $portfolio_details foreach ends


            $investment_date = '';

            //below if is to convert date format of the particular investment
            if($investment_detail->investment_date != NULL && $investment_detail->investment_date != '') {
                $investment_date = date('d-m-Y', strtotime($investment_detail->investment_date));
            }


            /*
                Getting all the investment if the investment amount in each portfolio is greater than 0 and storing it accordingly.
                
            */

            
            if($debt_investment > 0){
                if($response_type == 'data') {
                    $total_investment_details['debt'][] = array(
                        'investment_id' => $investment_detail->id,
                        'investment_date' => $investment_date,
                        'investment_amount' => $debt_investment,
                        'investment_type' => $scheme_types[$investment_detail->investment_type],
                        'investment_status' => $scheme_status[$investment_detail->investment_status],
                        'portfolio_type' => 'Debt'
                    );
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $investment_date,
                        'Invested Amount' => $debt_investment,
                        'Transaction Type' => $scheme_types[$investment_detail->investment_type],
                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
                    );
                }
            }


            if($equity_investment > 0){
                if($response_type == 'data') {

                    $total_investment_details['eq'][] = array(
                        'investment_id' => $investment_detail->id,
                        'investment_date' => $investment_date,
                        'investment_amount' => $equity_investment,
                        'investment_type' => $scheme_types[$investment_detail->investment_type],
                        'investment_status' => $scheme_status[$investment_detail->investment_status],
                        'portfolio_type' => 'Equity'
                    );
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $investment_date,
                        'Invested Amount' => $equity_investment,
                        'Transaction Type' => $scheme_types[$investment_detail->investment_type],
                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
                    );
                }
            }
            if($balanced_investment > 0){
                if($response_type == 'data') {
                    $total_investment_details['bal'][] = array(
                        'investment_id' => $investment_detail->id,
                        'investment_date' => $investment_date,
                        'investment_amount' => $balanced_investment,
                        'investment_type' => $scheme_types[$investment_detail->investment_type],
                        'investment_status' => $scheme_status[$investment_detail->investment_status],
                        'portfolio_type' => 'Balanced'
                    );
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $investment_date,
                        'Invested Amount' => $balanced_investment,
                        'Transaction Type' => $scheme_types[$investment_detail->investment_type],
                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
                    );
                }
            }
            if($ts_investment > 0){
                if($response_type == 'data') {
                    $total_investment_details['ts'][] = array(
                        'investment_id' => $investment_detail->id,
                        'investment_date' => $investment_date,
                        'investment_amount' => $ts_investment,
                        'investment_type' => $scheme_types[$investment_detail->investment_type],
                        'investment_status' => $scheme_status[$investment_detail->investment_status],
                        'portfolio_type' => 'Tax Saver'
                    );
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $investment_date,
                        'Invested Amount' => $ts_investment,
                        'Transaction Type' => $scheme_types[$investment_detail->investment_type],
                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
                    );
                }
            }


            // Getting Grand total of investments in each portfolio and storing it 
                
            $total_debt_investment += $debt_investment;
            $total_equity_investment += $equity_investment;
            $total_balanced_investment += $balanced_investment;
            $total_ts_investment += $ts_investment;

        } // Investment details foreach ends

        //Getting all the withdraw details of the user and storing it in the variable

        $withdraw_details = $user->withdrawDetails->where('withdraw_date', '>', $investment_period)->groupBy('withdraw_group_id')->all();Log::info(gettype($withdraw_details));
        if(!(empty($withdraw_details))) {

            //processing each withdraw 
            foreach($withdraw_details as $withdraw_detail) {Log::info($withdraw_detail);
                $debt_investment = 0;
                $equity_investment = 0;
                $balanced_investment = 0;
                $ts_investment = 0;
                $withdraw_status = 1;
                $withdraw_date = '';
                foreach($withdraw_detail as $withdraw) {
                    if($withdraw->status == 0) {
                        $withdraw_status = 0;
                    }
                    $withdraw_date = '';
                    if($withdraw->withdraw_date != NULL && $withdraw->withdraw_date != '') {
                        $withdraw_date = date('d-m-Y',strtotime($withdraw->withdraw_date));
                    }
                }

                if($response_type == 'data') {
                    if($withdraw->portfolio_type == 'debt'){
                        $total_investment_details['debt'][] = array(
                            'investment_date' => $withdraw_date,
                            'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                            'investment_type' => 'Withdraw',
                            'investment_status' => $scheme_status[$withdraw_status]
                        );
                    }
                    if($withdraw->portfolio_type == 'eq'){
                        $total_investment_details['eq'][] = array(
                            'investment_date' => $withdraw_date,
                            'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                            'investment_type' => 'Withdraw',
                            'investment_status' => $scheme_status[$withdraw_status]
                        );
                    }
                    if($balanced_investment > 0){
                        $total_investment_details['bal'][] = array(
                        'investment_date' => $withdraw_date,
                        'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                        'investment_type' => 'Withdraw',
                        'investment_status' => $scheme_status[$withdraw_status]
                        );
                    }
                    if($ts_investment > 0){
                        $total_investment_details['ts'][] = array(
                        'investment_date' => $withdraw_date,
                        'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                        'investment_type' => 'Withdraw',
                        'investment_status' => $scheme_status[$withdraw_status]
                        );
                    }
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $withdraw_date,
                        'Invested Amount' => $withdraw_detail->sum('withdraw_amount'),
                        'Transaction Type' => 'Withdraw',
                        'Investment Status' => $scheme_status[$withdraw_status]
                    );
                }

            } // Withdraw details foreach ends
        } // !empty($withdraw_details) ends
        if($response_type == 'data') {
            if($total_debt_investment > 0) {
                $total_investment_details['debt']['total_investment'] = $total_debt_investment;
            }
            if($total_equity_investment > 0) {
                $total_investment_details['eq']['total_investment'] = $total_equity_investment;
            }
            if($total_balanced_investment > 0) {
                $total_investment_details['bal']['total_investment'] = $total_balanced_investment;
            }
            if($total_ts_investment > 0) {
                $total_investment_details['ts']['total_investment'] = $total_ts_investment;
            }
        }
        return $total_investment_details;
    }

    //function to process withdrawl of funds
    public function withdrawFunds(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'withdraw_funds' => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'failure','status' => 'Withdraw failed. Please check and try again']);
        } else {

            $pan = \Auth::user()->PersonalDetails->pan;
            $bse_scheme_code = SchemeDetails::pluck('bse_scheme_code','scheme_code');

            $withdraw_request = $request['withdraw_funds'];
            $withdraw_funds = explode('|', $withdraw_request);
            $scheme_details = SchemeDetails::get()->pluck('scheme_type','scheme_code');
            $scheme_details = $scheme_details->toArray();
            $withdraw_status = array();
            $withdrawn_amount = 0;
            $withdraw_group_id = '';
            $withdraw_code_amount = array();

            //dd($withdraw_funds);
            foreach($withdraw_funds as $withdraw_fund) {
                $withdrawl_funds = explode('/', $withdraw_fund);
                if(array_key_exists(0, $withdrawl_funds) && array_key_exists(1, $withdrawl_funds)) {
                   if(strip_tags($withdrawl_funds[0]) != '' && strip_tags($withdrawl_funds[1]) != '' && strip_tags($withdrawl_funds[2]) != '') {
                        //$withdraw_code_amount[$withdrawl_funds[0]] = $withdrawl_funds[1];
                        $withdraw_code_amount[$withdrawl_funds[0]] = array(
                            'amount' => $withdrawl_funds[1],
                            'f_nf' => $withdrawl_funds[2]
                            );
                   }
                }   
            }

            //dd($withdraw_code_amount);

            $withdraw_details = WithdrawDetails::where('user_id',\Auth::user()->id)->where('withdraw_status','0')->get();
            //dd($pending_withdraws);
            $pending_withdraws = array();
            //dd(count($withdraw_details));

            //dd($withdraw_code_amount);


            /*Below foreach is to loop through all the current authenticated uset's pending withdrawal and compare it with 
            the current value of the scheme*/

            foreach ($withdraw_details as $withdraw) {
                /*
                    The below code is to check whether the withdrawal amount is greater than the available withdraw.
                    The available withdraw is calculated from pending withdraws and current value of the particular scheme.
                */

                if (array_key_exists($withdraw->scheme_code, $withdraw_code_amount)) {

                    //Below is to calulate the current value of the scheme.

                    $nav_code_value = NavDetails::where('scheme_code',$withdraw->scheme_code)->orderBy('date', 'desc')->first();
                    $units_held = \Auth::user()->portfolioDetails()->where('scheme_code',$withdraw->scheme_code)->where('portfolio_status','1')->sum('units_held');
                    $nav_value = $nav_code_value->nav_value;
                    $current_value = round(($units_held * $nav_value),2);
                    
                    
                    if (array_key_exists($withdraw->scheme_code, $pending_withdraws)) {
                        $pending_withdraws[$withdraw->scheme_code]['amount'] += $withdraw->withdraw_amount;
                    }else{
                        $pending_withdraws[$withdraw->scheme_code]['amount'] = $withdraw->withdraw_amount;
                    }

                    $pending_withdrawal_value = $pending_withdraws[$withdraw->scheme_code]['amount'];

                    $available_withdraw = $current_value - $pending_withdrawal_value;

                    //if withdrawal amount is greater than available withdraw then return with error.

                    //dd($withdraw_code_amount[$withdraw->scheme_code]['amount'],$available_withdraw);

                    if ($withdraw_code_amount[$withdraw->scheme_code]['amount'] > $available_withdraw) {


                        $scheme_name = SchemeDetails::where('scheme_code',$withdraw->scheme_code)->pluck('scheme_name');
                        return response()->json(['msg'=>'failure', 'status' => 'The withdrawal amount is greater than the available value in '.$scheme_name[0]]) ;
                    }else{
                        //echo "okay bhaai";
                    }
                }
            }

            //die();


            $user = \Auth::user();
            $withdraw_group_id = WithdrawDetails::max('withdraw_group_id');
            //dd($withdraw_code_amount,$withdraw_group_id);
            $withdraw_group_id++;


            // Below $amount is a array that contains withdrawal amount and full withdrawal or not details

            foreach($withdraw_code_amount as $code => $amount) {
               
                $total_units_held = $user->portfolioDetails()
                    ->where('scheme_code', $code)
                    ->where('portfolio_status', 1)
                    ->sum('units_held');
                
                $nav_code_value = NavDetails::where('scheme_code',$code)->orderBy('date', 'desc')->first();
                $total_current_value = $nav_code_value->nav_value * $total_units_held;

                //print_r($amount);
                //die();

                if(round($total_current_value,2) < $amount['amount']) {
                    $withdraw_status[$code] = 0;
                } else {
                   

                    $date = date('Y-m-d');
                    $date = explode('-', $date);
                    $date = $date[0].$date[1].$date[2];
                    $time = explode('.', microtime());
                    $time = substr($time[1], 0 ,6);
                    $unique_ref_no = $date."12456".$time;
                    $int_ref_no = $unique_ref_no;


                    //dd($amount['amount'],round($total_current_value,0));
                    $withdraw_details = new WithdrawDetails();
                    $withdraw_details->scheme_code = $code;
                    $withdraw_details->unique_ref_no = $unique_ref_no;
                    $withdraw_details->int_ref_no = $unique_ref_no;
                    $withdraw_details->withdraw_amount = $amount['amount'];
                    $withdraw_details->withdraw_date = date('Y-m-d');
                    $withdraw_details->portfolio_type = $scheme_details[$code];
                    $withdraw_details->withdraw_group_id = $withdraw_group_id;
                    if ($amount['f_nf'] == "f") {
                        $withdraw_details->full_amount = 1;
                    }
                    if ($amount['f_nf'] == "nf") {
                        $withdraw_details->full_amount = 0;
                    }
                    $withdraw_details->withdraw_status = 0;

                    
                    if($user->withdrawDetails()->save($withdraw_details)) {
                        $withdraw_status[$code] = 1;
                        $transact_mode = 'NEW';
                        

                        $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$code)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                            if (count($user_folio) == 0) {
                                $folio_number = ''; 
                            }else{
                                $folio_number = $user_folio['folio_number'];

                            }

                        
                        $buy_sell = 'R';
                        $buy_sell_type = 'FRESH';

                        if ($amount['f_nf'] == "f") {
                            $all_redeem = 'Y';
                            $withdraw_amount = '';
                        }else{
                            $all_redeem = 'N';
                            $withdraw_amount = $amount['amount'];
                        }

                        $this->getPassword();
                        if ($withdraw_amount >= 200000) {
                            $scheme_code = $bse_scheme_code[$code]."-L1";
                            
                        $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$scheme_code,$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);
                        }else{                              
                        $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code],$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);
                        }

                        // $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code],$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);

                        //dd($bse_withdraw_response);

                        $withdrawn_amount += $amount['amount'];
                    } else {
                        $withdraw_status[$code] = 0;
                    }
                }
            }

            //print_r($withdraw_status);
            
            if(!in_array(0,$withdraw_status)) {
                foreach ($bse_withdraw_response as $key => $value) {
                    $date = date('d/m/Y h:i:s A', strtotime($date));
                    WithdrawDetails::where('id',$value['wd_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => date('Y-m-d')]);
                }
                return response()->json(['msg'=>'success', 'status' => 'You have successfuly scheduled an withdraw','amount' => $withdrawn_amount,'date' => date('d-m-Y') ]) ;
            } else {
                //dd($withdraw_details);
                return response()->json(['msg'=>'failure', 'status' => 'Withdraw failed. Please check and try again']) ;
            }

        }
        
    }


    //function to create data for scheme performance details
    public function getSchemePerformanceDetails(Request $request)
    {
        $months;
        $scheme_code;
        $validator = \Validator::make($request->all(),[
            'duration' => 'required',
            'scheme_code' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'failure']);
        } else {
            $performance_data = array();
            $months = $request['duration'];
            $scheme_code = $request['scheme_code'];

            //calculating the range date to prepare data
            $nav_range_date = Carbon::now()->subMonths($months)->toDateString();

            $nav_details = HistoricNav::where('date', '>', $nav_range_date)
                ->where('scheme_code', $scheme_code)
                ->orderBy('date', 'asc')
                ->get();

            //creating performance data
            if(!empty($nav_details)) {
                foreach($nav_details as $nav_detail) {
                    $performance_data[] = array(
                        'amount' => $nav_detail->nav,
                        'year' => date('Y', strtotime($nav_detail->date)),
                        'month' => date('m', strtotime($nav_detail->date)) - 1,
                        'date' => date('d', strtotime($nav_detail->date)),
                    );
                }
                return response()->json(['msg' => 'success', 'response'=> $performance_data]);
            } else {
                return response()->json(['msg' => 'failure']);
            }
        }
    }

    //function to calculate sip
    /*public function sipCalculator(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'inv_amount' => 'required',
            'tenure' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['msg' => 'failure']);
        } else {
            $invested_amount = $request['inv_amount'];
            $tenure = round($request['tenure'] * 12);
            $inv_amount = array();
            $inv_date = array();
            $target_date = new Carbon::now()->addMonths($tenure);
            $current_date = new Carbon::now();
            if($target_date->diffInMonths($current_date) > 0) {

            }
        }
    }*/

    //function to get user portfoliodetails
    public function userPortfolioDetails($response_type = "document")
    {
        $portfolio_investment_details = array();
        $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
        $scheme_code_name = $scheme_details->toArray();
        $current_date = new Carbon();//Log::info($scheme_code_name);
        $user= \Auth::user();
        $portfolio_types = array('debt', 'bal', 'eq', 'ts');
        $total_investment_amount = 0;
        $total_units_held = 0;
        $total_investment_current_value = 0;
        $total_ltcg = 0;
        $xirr_fund_amount = array();//contains amount invested in the scheme to calculate the xirr
        $xirr_fund_date = array();//contains the date invested of the schemes to calculate xirr
        $user_active_schemes = array();
        $count = 1;
        $user_investment_performance_details = (new userController)->getInvestment_details();
        //print_r($user_investment_performance_details);
        //if(!(empty($nav_code_value))) {
        foreach ($portfolio_types as $portfolio_type) {
            $portfolio_details = $user->portfolioDetails()
                ->where('portfolio_type', $portfolio_type)
                ->where('portfolio_status', 1)
                ->orderBy('investment_date', 'asc')//
                ->get();
            $portfolio_details = $portfolio_details->groupBy('scheme_code');
            $total_portfolio_investment = 0;
            $total_portfolio_current_value = 0;
            $total_portfolio_units_held = 0;
            $total_portfolio_ltcg = 0;
            $current_date = Carbon::now()->toDateString();

            //processing grouped portfolio details
            foreach($portfolio_details as $code => $portfolio_detail) {
                
                //$portfolio_detail = $portfolio_detail;
                $scheme_code = $code;//processing scheme code
                $nav_code_value = NavDetails::where('scheme_code',$code)->orderBy('date', 'desc')->first();//scheme current nav value
                $nav_value = $nav_code_value->nav_value;
                $one_year_date = Carbon::now()->subMonths(12)->toDateString();
                $three_year_date = Carbon::now()->subMonths(36)->toDateString();
                $units_held = 0;
                $total_amount_invested = 0;
                $folio_number = '';
                $ltcg_return = 0;
                //processing each portfolio details
                foreach ($portfolio_detail as $portfolio) {
                    $folio_number = $portfolio->folio_number;
                    $units_held += $portfolio->units_held;
                    $total_amount_invested += $portfolio->amount_invested;

                    //checking eligibility for xirr return calculations
                    $old_active_investments = $user->portfolioDetails()
                        ->where('investment_date','<',$current_date)
                        ->where('portfolio_status', 1)
                        ->count();
                    if($old_active_investments > 0) {
                        //preparing investment data for calculating xirr return
                        $xirr_fund_amount[$code][] = -1 * $portfolio->initial_amount_invested;
                        $xirr_fund_date[$code][] = $portfolio->investment_date;
                    }

                    //checking eligibility for ltcg
                    if(($portfolio->portfolio_type == 'eq') && ($one_year_date > $portfolio->investment_date)) {
                        $ltcg_return += $portfolio->amount_invested;
                    } else if(($portfolio->portfolio_type == 'debt') && ($three_year_date > $portfolio->investment_date)) {
                        $ltcg_return += $portfolio->amount_invested;
                    }
                }

                //getting withdraw data for calculating xirr return
                    $withdraw_details = $user->withdrawDetails()
                        ->where('withdraw_status', 1)
                        ->where('scheme_code', $code)
                        ->get();

                    //preparing withdraw data for calculating xirr return
                    foreach($withdraw_details as $withdraw_detail) {

                        //adding withdraw data for calculating xirr return
                        $xirr_fund_amount[$code][] = $withdraw_detail->withdraw_amount;
                        $xirr_fund_date[$code][] = $withdraw_detail->withdraw_date;                   
                    }
                    if(!empty($xirr_fund_amount[$code])) {
                        $xirr_fund_amount[$code][] = round(($units_held * $nav_value), 2);
                        $xirr_fund_date[$code][] = date('Y-m-d');
                    }

                $xirr = 0;

                //Calculating XIRR for debt funds
                if(!empty($xirr_fund_amount[$code])) {//print_r($xirr_fund_amount[$code]);print_r($xirr_fund_date[$code]);
                    $xirr = PHPExcel_Calculation_Financial::XIRR($xirr_fund_amount[$code],$xirr_fund_date[$code]);
                    $xirr = round(($xirr * 100),2);
                    if($xirr == 0) {
                        $xirr = abs($xirr);
                    }
                    
                }

                $current_value = $units_held * $nav_value;
                $net_return = round(($current_value - $total_amount_invested), 2);
                if($net_return == 0) {
                    $net_return = abs($net_return);
                }
                $xirr_fund_amount[$code][] = $current_value;
                $xirr_fund_date[$code][] = date('Y-m-d');
                $ltcg_return = round($ltcg_return, 2);
                if($ltcg_return == 0) {
                    $ltcg_return = abs($ltcg_return);
                }

                if($response_type == 'data') {
                    $portfolio_investment_details[$portfolio_type][]=array(
                        'scheme_name' => $scheme_code_name[$code],
                        'folio_number' => $folio_number,
                        'units_held' => round($units_held,4),
                        'amount_invested' => $total_amount_invested,
                        'nav' => $nav_value,
                        'current_value' => round($current_value, 2),
                        'net_return' => $net_return,
                        'ltcg' => round($ltcg_return,2),
                        'xirr' => round(((round($current_value, 2) - $total_amount_invested)/$total_amount_invested)*100, 2),
                    ); 
                } else {
                    $portfolio_investment_details[]=array(
                        'SNo' => $count,
                        'Scheme Name' => $scheme_code_name[$code],
                        'Portfolio Type' => $portfolio_type,
                        'Folio Number' => $folio_number,
                        'Units Held' => round($units_held,4),
                        'Amount' => $total_amount_invested,
                        'NAV' => $nav_value,
                        'Current Value' => round($current_value, 2),
                        'Net Return' => round($net_return, 2),
                        'LTCG' => round($ltcg_return,2),
                        'Absolute Return' => round(((round($current_value, 2) - $total_amount_invested)/$total_amount_invested)*100, 2),
                    );
                }
                $count++;
                $user_active_schemes[] = $scheme_code;
                $total_portfolio_investment += $total_amount_invested;
                $total_portfolio_units_held += $units_held;
                $total_portfolio_current_value += $current_value;
                $total_portfolio_ltcg += $ltcg_return;
            }
            if($response_type == 'data') {

                //dd($user_investment_performance_details);
                if($total_portfolio_investment > 0 && $total_portfolio_units_held > 0 && $total_portfolio_current_value > 0){
                   $portfolio_investment_details[$portfolio_type]['total_amount_invested'] = $total_portfolio_investment;
                   $portfolio_investment_details[$portfolio_type]['total_units_held'] = round($total_portfolio_units_held,4);
                   $portfolio_investment_details[$portfolio_type]['total_current_value'] = round($total_portfolio_current_value,2);
                   $portfolio_investment_details[$portfolio_type]['total_net_value'] = round(($total_portfolio_current_value - $total_portfolio_investment),2);
                    $portfolio_investment_details[$portfolio_type]['total_ltcg'] = round($total_portfolio_ltcg, 2);
                    if(array_key_exists($portfolio_type, $user_investment_performance_details)) {
                        $portfolio_investment_details[$portfolio_type]['total_xirr'] = round((($portfolio_investment_details[$portfolio_type]['total_current_value'] - $portfolio_investment_details[$portfolio_type]['total_amount_invested'])/$portfolio_investment_details[$portfolio_type]['total_amount_invested'])*100, 2);
                    } else {
                        $portfolio_investment_details[$portfolio_type]['total_xirr'] = 0;
                    }
                }
                $total_investment_amount += $total_portfolio_investment;
                $total_units_held += $total_portfolio_units_held;
                $total_investment_current_value += $total_portfolio_current_value;
                $total_ltcg += $total_portfolio_ltcg;
            }
        }
       
        if($response_type == 'data') {
            if($total_investment_amount > 0 && $total_units_held > 0 && $total_investment_current_value > 0){
                $portfolio_investment_details['total_amount_invested'] = $total_investment_amount;
                $portfolio_investment_details['total_units_held'] = round($total_units_held,4);
                $portfolio_investment_details['total_current_value'] = round($total_investment_current_value,2);
                $portfolio_investment_details['total_net_value'] = round(($total_investment_current_value - $total_investment_amount),2);
                $portfolio_investment_details['total_ltcg_value'] = round($total_ltcg,2);
                if(array_key_exists('total_profit_percent', $user_investment_performance_details)) {
                    $portfolio_investment_details['total_xirr_value'] = round($user_investment_performance_details['total_profit_percent'],2);
                } else {
                    $portfolio_investment_details['total_xirr_value'] = 0;
                }
            }
        }

        //Adding withdraw details to user portfolio details
        foreach($user_active_schemes as $user_scheme_code) {
            $withdraw_details = $user->withdrawDetails
                ->where('scheme_code', $user_scheme_code)
                ->where('withdraw_status', 1);

            if($withdraw_details->count() > 0) {
                foreach($withdraw_details as $withdraw_detail) {
                    $xirr_fund_amount[$portfolio->scheme_code][] = $portfolio->withdraw_amount;
                    $xirr_fund_date[$portfolio->scheme_code][] = $portfolio->withdraw_date;
                }
                if($response_type == 'data') {
                    $portfolio_investment_details[$withdraw->portfolio_type][]=array(
                        'SNo' => $count,
                        'scheme_name' => $scheme_code_name[$user_scheme_code],
                        'folio_number' => $withdraw->folio_number,
                        'units_held' => round($withdraw_details->sum('withdraw_units'),4),
                        'amount_invested' => $withdraw_details->sum('withdraw_amount'),
                        'nav' => '',
                        'current_value' => '',
                        'net_return' =>''
                    );
                } else {
                    $portfolio_investment_details[]=array(
                        'SNo' => $count,
                        'Scheme Name' => $scheme_code_name[$user_scheme_code],
                        'Portfolio Type' => $withdraw->portfolio_type,
                        'Folio Number' => $withdraw->folio_number,
                        'Units Held' => round($withdraw_details->sum('withdraw_units'),4),
                        'Amount' => $withdraw_details->sum('withdraw_amount'),
                        'NAV' => '',
                        'Current Value' => '',
                        'Net Returns' =>'',
                        'LTCG' => '',
                        'Annualised Return' => '',
                    );
                }
            }
            $count++;
        }//print_r($portfolio_investment_details);
        // dd($portfolio_investment_details);
        return $portfolio_investment_details;
    }

    //function to get user portfolio data document
    public function getUserPortfolioDocument()
    {
        $user_portfolio = $this->userPortfolioDetails("document");
        $file_content = Excel::create('Portfolio Summary', function($excel) use ($user_portfolio) {
            $excel->sheet('mySheet', function($sheet) use ($user_portfolio)
            {
                $sheet->fromArray($user_portfolio);
            });
        })->download('xlsx');
    }

    //function to upload ekyc video
    public function uploadKycVideo(Request $request)
    {
        $user = \Auth::user();
        $validator = \Validator::make($request->all(),[
            'filename'    => 'required',
            //'audio-blob'    => 'required',
            'video-blob'    => 'required',
        ]);

        if ($validator->fails()) {
            
            //return redirect('preference')->with('errors',$validator->errors());
            return response()->json(['msg'=>'error','errors'=>'Something went wrong. Please refresh and try again.']);
            Log::info("File not received");
        } else{ 
            // dd($request["filename"]);
            $filename = $request["filename"].'.mp4';
            if($request["old_filename"] != '') {
                if(file_exists('tempvideo/'.$request['old_filename'].'.mp4')){
                    unlink('tempvideo/'.$request['old_filename'].'.mp4');
                }
            }
            if ($request->hasFile('video-blob')) {
                $uploadpath = 'tempvideo/'.$filename;
                if (!move_uploaded_file($_FILES["video-blob"]["tmp_name"], $uploadpath)) {
                    return response()->json(['msg'=>'error','errors'=>'Something went wrong. Please refresh and try again.']);
                    Log::info("Failed during moving files to tempvideo folder");
                }
                else {
                    $url = URL($uploadpath);
                    return response()->json(['msg'=>'success','url'=>$url]);//return $url;
                }
            } else {
                Log::info("No Video Blob file");
                return response()->json(['msg'=>'error','errors'=> 'Something went wrong. Please refresh and try again.']);
            }

        }
    }

    //function to submit kyc video uploaded
    public function saveKycVideo(Request $request)
    {
        $user = \Auth::user();
        $validator = \Validator::make($request->all(),[
            'filename'    => 'required',
            //'audio-blob'    => 'required',
            //'video-blob'    => 'required',
        ]);

        if ($validator->fails()) {
            Log::info("Filename not received");
            return response()->json(['msg'=>'failure','response'=>'Something went wrong. Please refresh and try again.']);
        } else {
            $request = $request->all();
            $filename = $request['filename'].'.mp4';
            $user_id = $user->id;
            $user_name = $user->name;
            $user_name = str_replace(' ', '', $user_name);


            if(file_exists('tempvideo/'.$filename)) {
                if(rename('tempvideo/'.$filename, 'ekycvideo/'.$user_id.'-'.$user_name.'-'.$request['filename'].'.mp4')) {
                    
                    $ekyc_save = $user->ekyc;
                    $ekyc_save->video_link =  $user_id.'-'.$user_name.'-'.$request['filename'].'.mp4';
                    $save_video = $user->ekyc()->save($ekyc_save);

                    if ($save_video) {


                        $save_kyc = \Auth::user()->nachDetails()->update(['kycvideo_status'=>NULL]);

                        if ($save_kyc) {
                            return response()->json(['msg'=>'success','response'=> 'eKyc video submitted successfully. We will get back to you shortly.']);    
                        }
                                     
                    }
                    else{
                        Log::info("Saving the video in ekyc table failed");
                        return response()->json(['msg'=>'failure','response'=> 'eKyc video submission failed. Please Try again.']);
                    }
                    
                } else {
                    Log::info("Failed during renaming and moving");
                    return response()->json(['msg'=>'failure','response'=> 'Video Upload Failed. Please refresh and try again.']);
                }
            } else {
                Log::info("File doesn't exist at tempvideo");
                return response()->json(['msg'=>'failure','response'=> 'Upload Failed. Please refresh and try again.']);
            }
        }
    }

    //function to get investment details
    function getInvestmentDetails(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'investment_id'    => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg'=>'error']);
        } else {
            $investment_details = array();
            $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
            $scheme_code_name = $scheme_details->toArray();
            $portfolio_details = PortfolioDetails::where('investment_id', $request['investment_id'])->get();

            //dd($portfolio_details);
            foreach($portfolio_details as $portfolio_detail) {
                
                if($portfolio_detail->portfolio_status == 1) {
                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                        'purchase_nav' => round($portfolio_detail->invested_nav,2),
                        'amount_invested' => round($portfolio_detail->amount_invested,2),
                        'status' => 'Success'
                    );


                }else if ($portfolio_detail->portfolio_status == 3 || $portfolio_detail->bse_order_status == 1) {

                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                        
                        'purchase_nav' => '-',
                        'amount_invested' => round($portfolio_detail->amount_invested,2),
                        'status' => 'Cancelled'
                    );

                } else {
                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                        // 'purchase_nav' => round($portfolio_detail->invested_nav,2),  
                        'purchase_nav' => '-',
                        'amount_invested' => round($portfolio_detail->amount_invested,2),
                        'status' => 'Processing'
                    );
                }
            }

            
            return response()->json(['msg'=>'success','response'=> $investment_details]);
        }
    }


    public function cancelInvestment(Request $request){
        $validator = \Validator::make($request->all(),[
            'investment_id'    => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg'=>'error','response'=>'Oops something went wrong. Try again Later.']);
        } else {

            $inv_id = $request['investment_id'];

            $user = \Auth::user();
            $investment_update = $user->investmentDetails()->where('id',$inv_id)->update(['investment_status'=> 3]);


            if ($investment_update) {
                $portfolio_update = PortfolioDetails::where('investment_id',$inv_id)->update(['portfolio_status'=>3]);

                if ($portfolio_update) {
                    return response()->json(['msg'=>'success','response'=>'Investments Cancelled Successfully']);
                }else{
                    $portfolio_update = PortfolioDetails::where('investment_id',$inv_id)->update(['portfolio_status'=>0]);
                    $investment_update = $user->investmentDetails()->where('id',$inv_id)->update(['investment_status'=> 0]);

                    if ($portfolio_update && $investment_update) {
                        return response()->json(['msg'=>'error','response'=>'Sorry Cannot Cancel your Investments rightnow. Please refresh and try again']);
                    }

                }
            }else{

                return response()->json(['msg'=>'error','response'=>'Sorry Cannot Cancel your Investments rightnow. Please refresh and try again']);
            }
            
            



           
        }
    }


    public function getPassword(){


        $soap_request = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
           <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
           <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
           </soap:Header>
           
           <soap:Body>
              <bses:getPassword>
                 <!--Optional:-->
                 <bses:UserId>1245601</bses:UserId>
                 <!--Optional:-->
                 <bses:Password>'.$this->password.'</bses:Password>
                 <!--Optional:-->
                 <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
              </bses:getPassword>
           </soap:Body>
        </soap:Envelope>';

        //echo "hello";


        $result = $this->makeSoapCall($soap_request);
        // var_dump($result);
        // die();
        $result = explode('http://bsestarmf.in/MFOrderEntry/getPasswordResponse', $result);
        $response = explode('|', $result[1]);

        //var_dump($response);
        //echo $response[1];

        ///preg_match('/<getPasswordResult>-(.*?)-<getPasswordResult>/', $result, $pass_response);


       // dd($pass_response);

        $pwd_response =  $response[0];
        $this->response_key = $response[1];

        //echo $this->response_key;

      }


      public function checkPayment(){

            $this->mfPassword();
            $inv_array = [];
            
            $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
                'soap_version' => SOAP_1_2, // !!!!!!!
                'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
              ));

            $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI',true);
            $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

            $client->__setSoapHeaders([$actionHeader,$toHeader]);


            $portfolio_details = PortfolioDetails::where('portfolio_status',0)->where('bse_order_status',0)->get();

            //****** Use the below commented code to get order status of the induvidual orders *******//

             // $params = array(
             //      'Flag' => '11',
             //      'UserId' => '1245601',
             //      'EncryptedPassword' => $this->response_key,
             //      'param' => 'ABEPF5833F|26184389|BSEMF',

             //    );

             // $result = $client->MFAPI($params);
             // $result = explode('|', $result->MFAPIResult);

             // $payment_res = explode('(', $result[1]);
             // dd($payment_res);


            //dd($portfolio_details);
            foreach ($portfolio_details as $portfolio) {
                //echo $portfolio->bse_order_no."-".$portfolio->user->personalDetails->pan."<br>";
                $params = array(
                  'Flag' => '11',
                  'UserId' => '1245601',
                  'EncryptedPassword' => $this->response_key,
                  'param' => $portfolio->user->personalDetails->pan.'|'.$portfolio->bse_order_no.'|BSEMF',

                );

                $result = $client->MFAPI($params);
                $result = explode('|', $result->MFAPIResult);

                $payment_res = explode('(', $result[1]);
                //echo $portfolio->user->personalDetails->pan." -- ".$portfolio->bse_order_no." -- ".trim($payment_res[0])."<br>";

                if (trim($payment_res[0]) == "APPROVED") {
                    // echo "Inside Approved";
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'apr']);
                }else if (trim($payment_res[0]) == "REJECTED") {
                    // reject the order.
                    // echo "Inside rejected";
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'rej','portfolio_status'=>3]);
                    $inv_array[] = $portfolio->investment_id;
                }else if (trim($payment_res[0]) == "AWAITING FOR FUNDS CONFIRMATION") {
                    // Change the Payment Status column to AWC
                    // echo "inside afc";
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'afc']);
                    $inv_array[] = $portfolio->investment_id;
                }else if (trim($payment_res[0]) == "PAYMENT NOT INITIATED FOR GIVEN ORDER") {
                    // Chnage the Payment STatus to PNI
                    // echo "Inside pni";

                    // if it stays "PNI" for more than 24 hours Cancel it off
                    PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'pni']);
                    $inv_array[] = $portfolio->investment_id;
                }else{

                }
                //echo ;
            }


            $update_pni_port = PortfolioDetails::where('created_at', '<', Carbon::now()->subDay())->where('bse_payment_status','pni')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

            $update_ac = PortfolioDetails::where('created_at', '<', Carbon::now()->subDays(7))->where('bse_payment_status','ac')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

            foreach ($inv_array as $inv) {

                $can_port_count = PortfolioDetails::where('investment_id',$inv)->where('portfolio_status','!=',3)->count();
                // echo $can_port_count."<br>";

                if ($can_port_count == 0) {
                    InvestmentDetails::where('id',$inv)->update(['investment_status'=>3]);
                }
            }
            // dd($update_ac);
           


            // $params = array(
            //       'Flag' => '11',
            //       'UserId' => '1245601',
            //       'EncryptedPassword' => $this->response_key,
            //       'param' => 'BFRPN4910B|24041305|BSEMF',

            //     );

            // $result = $client->MFAPI($params);
            
            // dd($result->MFAPIResult);
            // die();

            

            //dd($params);

            

              //dd($client->__getLastRequestHeaders());

              //dd($result);
            return response()->json(['msg'=>'success','response'=>'Status updated successfuly']);

      }


      public function mfPassword(){
        $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
                'soap_version' => SOAP_1_2, // !!!!!!!
                'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
              ));

        $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword',true);
        $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

        $client->__setSoapHeaders([$actionHeader,$toHeader]);

            $login_params = array(
              'UserId' => '1245601',
              'MemberId' => '12456',
              'Password' => 'Pwm$2017',
              'PassKey' => '1234569870',

            );

          $result = $client->getPassword($login_params);
          $result = $result->getPasswordResult;
          // dd($result);

          $response = explode('|', $result);

          // dd($response);
          $pwd_response =  $response[0];
          $this->response_key = $response[1];
      }

      

      public function placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code,$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_id){

        //dd($this->response_key);



        /*
              Order Response will be in this order. Kindly don't go through the BSE web file documentation.
              That will only confuse you. 

              The order response will be in this format.

              TRANSACTION_CODE|UNIQUE REFERENCE NUMBER|BSE ORDER NO|USER ID|MEMBER ID|CLIENT CODE|BSE REMARKS|SUCCESS FLAG

              UNIQUE REFERENCE NUMBER = $unique_ref_no //this should be unique for each order. This will be tracking for the order.
              BSE ORDER NO //This is generated by BSE
              TRANSACTION CODE // NEW||MOD||CXL -- New order || Modification || Cancel.
              SUCCESS FLAG // 0 == ORDER SUCCESS and 1 = ORDER FAILURE *What a Brilliant thing by BSE* 

        */

        $response_key = explode('</getPasswordResult></getPasswordResponse></s:Body></s:Envelope>', $this->response_key);



        $soap_request = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
           <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
           <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
           </soap:Header>
       <soap:Body>
          <bses:orderEntryParam>
             <!--Optional:-->
             <bses:TransCode>'.$transact_mode.'</bses:TransCode>
             <!--Optional:-->
             <bses:TransNo>'.$unique_ref_no.'</bses:TransNo>
             <!--Optional:-->
             <bses:OrderId></bses:OrderId>
             <!--Optional:-->
             <bses:UserID>1245601</bses:UserID>
             <!--Optional:-->
             <bses:MemberId>12456</bses:MemberId>
             <!--Optional:-->
             <bses:ClientCode>'.$pan.'</bses:ClientCode>
             <!--Optional:-->
             <bses:SchemeCd>'.$bse_scheme_code.'</bses:SchemeCd>
             <!--Optional:-->
             <bses:BuySell>'.$buy_sell.'</bses:BuySell>
             <!--Optional:-->
             <bses:BuySellType>'.$buy_sell_type.'</bses:BuySellType>
             <!--Optional:-->
             <bses:DPTxn>P</bses:DPTxn>
             <!--Optional:-->
             <bses:OrderVal>'.$withdraw_amount.'</bses:OrderVal>
             <!--Optional:-->
             <bses:Qty></bses:Qty>
             <!--Optional:-->
             <bses:AllRedeem>'.$all_redeem.'</bses:AllRedeem>
             <!--Optional:-->
             <bses:FolioNo>'.$folio_number.'</bses:FolioNo>
             <!--Optional:-->
             <bses:Remarks></bses:Remarks>
             <!--Optional:-->
             <bses:KYCStatus>Y</bses:KYCStatus>
             <!--Optional:-->
             <bses:RefNo>'.$int_ref_no.'</bses:RefNo>
             <!--Optional:-->
             <bses:SubBrCode></bses:SubBrCode>
             <!--Optional:-->
             <bses:EUIN>E173580</bses:EUIN>
             <!--Optional:-->
             <bses:EUINVal>N</bses:EUINVal>
             <!--Optional:-->
             <bses:MinRedeem>N</bses:MinRedeem>
             <!--Optional:-->
             <bses:DPC>N</bses:DPC>
             <!--Optional:-->
             <bses:IPAdd></bses:IPAdd>
             <!--Optional:-->
             <bses:Password>'.$response_key[0].'</bses:Password>
             <!--Optional:-->
             <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
             <!--Optional:-->
             <bses:Parma1></bses:Parma1>
             <!--Optional:-->
             <bses:Param2></bses:Param2>
             <!--Optional:-->
             <bses:Param3></bses:Param3>
          </bses:orderEntryParam>
       </soap:Body>
    </soap:Envelope>';

    //dd($soap_request);

        $result = $this->makeSoapCall($soap_request);
        //echo $result;



        $response = explode('http://bsestarmf.in/MFOrderEntry/orderEntryParamResponse', $result);
        $response = explode('|', $response[1]);

        $trans_code = explode('</a:Action></s:Header><s:Body><orderEntryParamResponse xmlns="http://bsestarmf.in/"><orderEntryParamResult>', $response[0]);
        $order_response = explode('</orderEntryParamResult></orderEntryParamResponse></s:Body></s:Envelope>', $response[7]);

        $order_response = array(
            'trans_code' => $trans_code[1],
            'unique_ref_no' => $response[1],
            'order_no' =>$response[2],
            'client_code' => $response[5],
            'remarks' =>$response[6],
            'order_status' => $order_response[0],
            'wd_id' => $withdraw_id,
          );

        //$this->place_order_count++;
        return $order_response;
        

      }

      public function makeSoapCall($soap_request){

        //dd($soap_request);
        $header = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
            "Accept: application/soap+xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: ".strlen($soap_request),
        );
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL,"http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc?singleWsdl" );
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do, CURLOPT_POST,           true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
        $result = curl_exec($soap_do);
        //dd($result);
        return $result;
      }


      public function makePaymentSoapCall($soap_request){
        //dd($soap_request);
        $header = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
            "Accept: application/soap+xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: ".strlen($soap_request),
        );
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL,"https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl" );
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do, CURLOPT_POST,           true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);


        $result = curl_exec($soap_do);
        //dd($result);
        //dd($soap_do);
        return $result;
      }

      public function dummy(){
        $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
                'soap_version' => SOAP_1_2, // !!!!!!!
                'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
              ));

        $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword',true);
        $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

        $client->__setSoapHeaders([$actionHeader,$toHeader]);

            $login_params = array(
              'UserId' => '1245601',
              'MemberId' => '12456',
              'Password' => 'Pwm$2017',
              'PassKey' => '1234569870',

            );

          $result = $client->getPassword($login_params);

          //dd($client->__getLastRequestHeaders());

          echo $result->getPasswordResult;
      }



}
