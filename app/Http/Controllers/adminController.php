<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use Carbon\Carbon;
use PHPExcel;
use PHPExcel_Calculation_Financial;
use App\Http\Requests;
use App\User;
use Excel;
use App\PersonalDetails;
use App\CorporateDetails;
use App\Directors;
use App\portfolioComp;
use App\DailyNav;
use App\NomineeDetails;
use App\SchemeDetails;
use App\InvestmentDetails;
use App\PortfolioDetails;
use App\WithdrawDetails;
use App\NachDetails;
use App\BankDetails;
use App\NavDetails;
use App\Sip;
use App\SipDetails;
use App\ekyc;
use App\SchemeHistory;
use App\Notifications;
use Illuminate\Support\Facades\Log;
use Mail;
use App\HistoricNav;
use App\SipEntries;
use Storage;
use App\BankInfo;
use App\BpoStatus;
//use Facebook;


use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;






/*Portfolio Status Code


            Portfolio status
            0 => 'Pending'
            1 => 'Rialised'
            2 => 'Withdraw'
            3 => 'cancelled'



*/

/*Investment Status Code


            Investment status
            0 => 'Processing'
            1 => 'Success'
            2 => 'Failed'
            3 => 'Cancelled'

*/

class adminController extends Controller
{
    protected  $investment_types = array('One Time', 'Scheduled', 'Monthly');

    protected $portfolio_types = array('Custom', 'Low Risk', 'Medium Risk', 'High Risk', 'Tax Saver');

    protected $investment_status = array('Processing', 'Successfull', 'Failed');

    protected $sip_port_type = array('eq'=>3, 'debt'=>1, 'bal'=>2,'ts'=>4, 'custom'=>0);

    protected $fund_type = array(
            'eq' => 'Equity', 
            'debt' => 'Debt', 
            'bal' => 'Hybrid',
            'ts' => 'ELSS'
        );


    public function adminOverview()
    {
        $fmt = new \NumberFormatter( 'en_IN', \NumberFormatter::DECIMAL );
        if (\Auth::id() == 145) {
            $nav_code_value = NavDetails::where('date','<=',Carbon::now()->subDays(1))->pluck('nav_value','scheme_code');
            $myUser = User::where('handle_by',0)->get();
            $myUserCount = $myUser->count();
            $total_investment = 0;
            foreach($myUser as $user_detail) {
                $portfolio_details = PortfolioDetails::where('user_id',$user_detail['id'])->where('portfolio_status',1)->get();
                foreach ($portfolio_details as $portfolio) {
                    $current_value = $portfolio->units_held * $nav_code_value[$portfolio->scheme_code];
                    $total_investment += $current_value;
                }
            }
            $total_investment = $fmt->format(round($total_investment, 2));
            return view('admin.bpo-overview',['count'=>$myUserCount,'total'=>$total_investment]);
        }
        $portfolio_types = array('debt', 'bal', 'eq', 'ts'); //portfolio type
        $portfolio_amount_invested = 0;                //Total amount invested
        $less_than_lakh_user_count = 0;                //Number of users having investment < 1 Lakhs
        $less_than_10lakh_user_count = 0;
        $greater_than_10lakh_user_count = 0;
        $new_user_today_count = 0;
        $new_user_week_count = 0;
        $new_user_month_count = 0;
        $current_date = new carbon();
        $user = User::get();
        $personal_details = PersonalDetails::get();
        $user_count = $user->count();
        $male_user_count = $personal_details->where('sex','male')->count();
        $female_user_count = $personal_details->where('sex','female')->count();
        $scheme_details = SchemeDetails::get();
        $portfolio_details = PortfolioDetails::get();
        $total_investment_amount = InvestmentDetails::where('investment_status',1)->get();
        $total_withdraw_amount = WithdrawDetails::where('withdraw_status',1)->get();
        $total_schemes_offered = $scheme_details->count();
        $portfolio_typewise_investment = array();
        $nach_pending_users = NachDetails::where('nach_status',0)->count();
        $kyc_pending_users = NachDetails::where(function($query){
             $query->where('kyc_status', null)
                  ->orWhere('kyc_status', 2);
        })->count();
        
        $week_old = Carbon::now()->subDays(7)->toDateString();
        $sip_this_week = InvestmentDetails::where('investment_type',2)->where('investment_date','>', $week_old)->count();

        $pending_onetime_count = InvestmentDetails::where('investment_type',0)->where('investment_status',0)->count();
        $onetime_count = InvestmentDetails::where('investment_type',0)->whereIn('investment_status',[1,2])->count();
        // $sip_count = Sip::all()->count();
        $sip_count = SipEntries::where('bse_reg',1)->get()->count();

        /*$helper = Facebook::getRedirectLoginHelper(); 

        $token = $helper->getAccessToken();
        echo $token;*/
        //$fb_likes = Facebook::get('/1602612290047840/likes',$token);

        foreach($user as $user_detail) {
            $user_exist_diffin_hours = $current_date->diffInHours($user_detail->created_at);
            if($user_exist_diffin_hours < 24) {
                $new_user_today_count++;    
            }
            if($user_exist_diffin_hours < 168) {
                $new_user_week_count++;    
            }
            if($user_exist_diffin_hours < 720) {
                $new_user_month_count++;
            }

            $user_amount_invested = $user_detail->portfolioDetails
                ->where('portfolio_status', 1)
                ->sum('amount_invested');
            $user_amount_withdrawn = $user_detail->withdrawDetails
                ->where('withdraw_status', 1)
                ->sum('withdraw_invested');
            $current_amount_value = 0;
            if($user_amount_invested != NULL) {
                if($user_amount_withdrawn != NULL) {
                    $current_amount_value = $user_amount_invested - $user_amount_withdrawn;
                } else {
                    $current_amount_value = $user_amount_invested;
                }
                if($current_amount_value < 100000) {
                    $less_than_lakh_user_count += 1;
                } else if($current_amount_value < 1000000){
                    $less_than_10lakh_user_count += 1;
                } elseif($current_amount_value > 1000000) {
                    $greater_than_10lakh_user_count += 1;
                }
            }
        }
        if(count($portfolio_details)>0) {
            foreach ($portfolio_types as $portfolio_type) {
                $amount_invested = $portfolio_details->where('portfolio_type', $portfolio_type)
                    ->where('portfolio_status', 1)
                    ->sum('amount_invested');
                if($amount_invested != NULL) {
                    $portfolio_typewise_investment[$portfolio_type] = $amount_invested;
                    $portfolio_amount_invested += $amount_invested;
                } else {
                    $portfolio_typewise_investment[$portfolio_type] = 0;
                    $portfolio_amount_invested += 0;
                }
            }
        } else {
            $portfolio_typewise_investment = array(
                'debt' => 0,
                'bal' => 0,
                'eq' => 0
            );
        }

        if($total_investment_amount != NULL) {
            if($total_investment_amount != NULL) {
                $total_investment = $total_investment_amount->sum('investment_amount') - $total_withdraw_amount->sum('withdraw_amount');
            } else {
                $total_investment = $total_investment_amount->sum('investment_amount');
            }
        } else {
             $total_investment = 0;
        }

        $portfolio_details = PortfolioDetails::where('portfolio_status',1)->get();
        $nav_code_value = NavDetails::where('date','<=',Carbon::now()->subDays(1))->pluck('nav_value','scheme_code');
        //dd($nav_code_value);

        $total_investment = 0;
        foreach ($portfolio_details as $portfolio) {
            
            $current_value = $portfolio->units_held * $nav_code_value[$portfolio->scheme_code];
            // echo $current_value."<br>";
            $total_investment += $current_value;
        }

        // dd($total_aum);
        if($total_investment > 0) {
            $admin_overview_data = array(
                'total_user_count' => $user_count,
                'total_amount_invested' => $fmt->format(round($total_investment, 2)),
                'total_schemes_offered' => $total_schemes_offered,
                'debt_investment_percent' => round((($portfolio_typewise_investment['debt'] / $total_investment)*100),2),
                'equity_investment_percent' => round((($portfolio_typewise_investment['eq'] / $total_investment)*100),2),
                'balanced_investment_percent' => round((($portfolio_typewise_investment['bal'] / $total_investment)*100),2),
                'average_investment_per_customer' => round(($total_investment/$user_count)),
                'male_female_customer' => $male_user_count.'/'.$female_user_count,
            );
        } else {
            $admin_overview_data = array(
                'total_user_count' => $user_count,
                'total_amount_invested' => $total_investment,
                'total_schemes_offered' => $total_schemes_offered,
                'debt_investment_percent' => 0,
                'equity_investment_percent' => 0,
                'balanced_investment_percent' => 0,
                'average_investment_per_customer' => 0,
                'male_female_customer' => $male_user_count.'/'.$female_user_count,
            );
        }

        //couting number of users for last week, month and last day
        // foreach($user as $invdidual_user) {

        // }
        // dd($new_user_today_count, $new_user_week_count, $new_user_month_count);

        $admin_overview_data['less_than_lakh'] = $less_than_lakh_user_count;
        $admin_overview_data['less_than_10lakh'] = $less_than_10lakh_user_count;
        $admin_overview_data['greater_than_10lakh'] = $greater_than_10lakh_user_count;
    
        $admin_overview_data['new_user_today'] = $new_user_today_count;
        $admin_overview_data['new_user_week'] = $new_user_week_count;
        $admin_overview_data['new_user_month'] = $new_user_month_count;
        $admin_overview_data['nach_pending'] = $nach_pending_users;
        $admin_overview_data['kyc_pending'] = $kyc_pending_users;

        $admin_overview_data['onetime_count'] = $onetime_count;
        $admin_overview_data['pending_onetime_count'] = $pending_onetime_count;
        $admin_overview_data['sip_count'] = $sip_count;
        $admin_overview_data['sip_this_week'] = $sip_this_week;


        //print_r($admin_overview_data);
        // dd($admin_overview_data);
        return view('admin.admin-overview')->with('overview_data',$admin_overview_data);
    }

    public function adminAmc(){
        return view('admin.admin-amc');
    }

    public function adminKyc(){
        return view('admin.admin-kyc');
    }

    public function adminNach($nach_status='all')
    {
        $column_name = '';//has the column name based on which the results will be generated
        $column_condition = '';// has the codition to be applied on the column to generate the result
        $column_value = '';//has the value to be used on the column with the condition to generate the result
        $nach_details ;
        switch($nach_status) {
            case "kyc": {
                $nach_details = NachDetails::where('kyc_date','!=',NULL)->get();
                break;
            }
            case "mandate_sent": {
                $nach_details = NachDetails::where('mandate_sent_date','!=',NULL)->get();
                break;
            }
            case "mandate_received": {
                $nach_details = NachDetails::where('mandate_received','!=',NULL)->get();
                break;
            }
            case "tpsl": {
                $nach_details = NachDetails::where('tpsl_date','!=',NULL)->get();
                break;
            }
            case "cleared": {
                $nach_details = NachDetails::where('cleared_date','!=',NULL)->get();
                break;
            }
            case "disapproved": {
                $nach_details = NachDetails::where('disapproved_date','!=',NULL)->get();
                break;
            }
            default: {
                $nach_details = NachDetails::where('nach_status',0)->get();
            }

        }

        $nach_pending_users = array();
        $pending_nach_count = 0;
        foreach($nach_details as $nach_detail) {
            $user = $nach_detail->User;
            
            //check if the user has provided basic details
            if($user->p_check == '1' && $user->role == 0) {          
                $pending_nach_count ++;
                $bank_details = $user->bankDetails->first();
                $pan_date = '';
                $kyc_date = '';
                $mandate_sent = '';
                $mandate_recev = '';
                $tpsl_date = '';
                $cleared = '';
                $disapproved_date = '';
                if($nach_detail->pan_date != NULL) {
                    $pan_date = date('d-m-Y', strtotime($nach_detail->pan_date));
                }
                if($nach_detail->kyc_date != NULL) {
                    $kyc_date = date('d-m-Y', strtotime($nach_detail->kyc_date));
                }
                if($nach_detail->mandate_sent_date != NULL) {
                    $mandate_sent = date('d-m-Y', strtotime($nach_detail->mandate_sent_date));
                }
                if($nach_detail->mandate_received != NULL) {
                    $mandate_recev = date('d-m-Y', strtotime($nach_detail->mandate_received));
                }
                if($nach_detail->tpsl_date != NULL) {echo $nach_detail->tpsl_date;
                    $tpsl_date = date('d-m-Y', strtotime($nach_detail->tpsl_date));
                }
                if($nach_detail->cleared_date != NULL) {
                    $cleared = date('d-m-Y', strtotime($nach_detail->cleared_date));
                }
                if($nach_detail->disapproved_date != NULL) {
                    $disapproved_date = date('d-m-Y', strtotime($nach_detail->disapproved_date));
                }
                
                $nach_pending_users[] = array(
                    'user_id' => $user->id,
                    'si_no' => $pending_nach_count,
                    'name' => $user->name,
                    'user_pan' => $user->personalDetails->pan,
                    'pan_date' => $pan_date,
                    'kyc_date' => $kyc_date,
                    'mandate_sent_date' => $mandate_sent,
                    'mandate_returned_date' => $mandate_recev,
                    'tpsl_date' => $tpsl_date,
                    'cleared_date' => $cleared,
                    'dissaproved_date' => $disapproved_date,
                );
            }
        }//print_r($nach_pending_users);
        return view('admin.admin-nach')->with('pending_nach_details', $nach_pending_users);
    }


    public function newNach(){

        if (\Auth::id() == 145) {
            $bpoids = User::where('handle_by',0)->pluck('id');
            $nach_details = NachDetails::whereIn('user_id',$bpoids)->get();
        }else{
            $nach_details = NachDetails::where('nach_status','0')->get();
        }

        foreach ($nach_details as $nach) {
            if ($nach->user->p_check == "0") {
                $nach['user_pan'] = '';    
            }else{
                $nach['user_pan'] = $nach->user->personalDetails->pan;
            }
            
            $nach['user_name'] = $nach->user->name;
            $nach['user_mobile'] = $nach->user->mobile;
            $nach['user_email'] = $nach->user->email;
        }

        // dd($nach_details);
        return view('admin.admin-new-nach')->with(compact('nach_details'));
    }




    public function getUserNachDetails(Request $request){

        $validator = \Validator::make($request->all(),[

            'user_id' => 'required',
            ]);

        if ($validator->fails()) {
            
            return $validator->errors();
        }

        else{
            
            $user_nach_details = NachDetails::where('user_id',$request['user_id'])->get();
            //dd($user_nach_details);
                foreach ($user_nach_details as $nach) {
                    
                    if ($nach->user->p_check == "0") {
                        $nach['user_pan'] = '';    
                    }else{
                        $nach['user_pan'] = $nach->user->personalDetails->pan;
                    }

                    $nach['user_name'] = $nach->user->name;
                    $nach['user_mobile'] = $nach->user->mobile;
                    $nach['user_email'] = $nach->user->email;
                }

                

            $ekyc_details = ekyc::where('user_id',$request['user_id'])->get();

            $user_status = BpoStatus::where('user_id',$request['user_id'])->value('user_status');







            if (!empty($user_nach_details)) {
                return response()->json(['msg'=>'success','response'=>$user_nach_details,'ekyc'=>$ekyc_details,'user_state'=>$user_status]);
            }else{
                return response()->json(['msg'=>'failure']);
            }
        }

    }


    public function updateUserNachDetails(Request $request){

        $validator = \Validator::make($request->all(),[

            'user_id' => 'required',
            'option' => 'required',
            'col_name' => 'required',
            ]);

        if ($validator->fails()) {
            
            return $validator->errors();
        }

        else{

            //dd($request['col_name']);

           $current_date = date('Y-m-d',strtotime(Carbon::now()->toDateString()));
           //dd($current_date);
           $col_date = explode('_', $request['col_name']);
           $col_date = $col_date[0];
           $col_date = $col_date.'_'.'date';


           if($request['col_name'] == "pan_status"){

                if($request['option'] == 1){
                    //die();

                    
                    $update_nach = NachDetails::where('user_id',$request['user_id'])->update([
                        'pan_status' => 1,
                        'pan_date' => $current_date,
                        'kycvideo_status' => 1,
                        'kycvideo_date' => $current_date,
                        'kyc_status' => 1,
                        'kyc_date' => $current_date,
                        'camskyc_status'=> 1,
                        'camskyc_date' => $current_date,
                        ]);

                       if ($update_nach) {


                            $user_nach = NachDetails::where('user_id',$request['user_id'])->get()->toArray();
                            $bseclient_status = $user_nach[0]['bseclient_status'];
                            $pan_status = $user_nach[0]['pan_status'];
                            $kyc_status = $user_nach[0]['kyc_status'];
                            $kycvideo_status = $user_nach[0]['kycvideo_status'];
                            $cc_status = $user_nach[0]['cc_status'];
                            $mandatesoft_status = $user_nach[0]['mandatesoft_status'];
                            $mandatehard_status = $user_nach[0]['mandatehard_status'];
                            $aof_uploaded = $user_nach[0]['aof_uploaded'];
                            $camskyc_status = $user_nach[0]['camskyc_status'];
                            $bsemandate_status = $user_nach[0]['bsemandate_status'];
                            $bseaof_status = $user_nach[0]['bseaof_status'];
                        
                        

                            // if($bseclient_status == 1 && $pan_status == 1 && $kyc_status == 1 && $kycvideo_status == 1 && $cc_status == 1 && $mandatesoft_status == 1 && $mandatehard_status == 1 && $aof_uploaded == 1 && $camskyc_status == 1 && $bsemandate_status == 1 && $bseaof_status == 1){

                            //     //dd('all one ');
                            //     $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 0]);
                            //     $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 1]);
                            //     if($user_nach){
                            //             $this->handleNotifications($request['user_id'],$request['col_name'],$request['option']);
                            //             return $this->getUserNachDetails($request);
                            //     }else{
                            //         return response()->json(['msg'=>'failure']);

                            //     }

                            // }else{

                                $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 1]);
                                $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 0]);
                                if($user_nach){
                                        $this->handleNotifications($request['user_id'],$request['col_name'],$request['option']);
                                        return $this->getUserNachDetails($request);
                                }else{
                                    return response()->json(['msg'=>'failure']);

                                }
                            // }
                           
                       }
                }
                if($request['option'] == 2){
                    $update_nach = NachDetails::where('user_id',$request['user_id'])->update([
                        'pan_status' => 2,
                        'pan_date' => $current_date,
                        'kycvideo_status' => NULL,
                        'kycvideo_date' => $current_date,
                        'kyc_status' => 2,
                        'kyc_date' => $current_date,
                        'camskyc_status'=> NULL,
                        'camskyc_date' => $current_date,
                        ]);

                       if ($update_nach) {

                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 1]);
                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 0]);

                                    if($user_nach){
                                        $this->handleNotifications($request['user_id'],$request['col_name'],$request['option']);
                                         return $this->getUserNachDetails($request);
                                     }else{
                                        return response()->json(['msg'=>'inga failure']);
                                     }

                        }else{
                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 1]);
                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 0]);

                                    if($user_nach){
                                        $this->handleNotifications($request['user_id'],$request['col_name'],$request['option']);
                                         return $this->getUserNachDetails($request);
                                     }else{
                                        return response()->json(['msg'=>'inga thaan failure']);
                                     }
                        }
                           
                       
                }
           }else{


                $update_nach = NachDetails::where('user_id',$request['user_id'])->update([$request['col_name']=>$request['option'],$col_date=>$current_date]);

                $user_nach = NachDetails::where('user_id',$request['user_id'])->get()->toArray();
                $bseclient_status = $user_nach[0]['bseclient_status'];
                $pan_status = $user_nach[0]['pan_status'];
                $kyc_status = $user_nach[0]['kyc_status'];
                $kycvideo_status = $user_nach[0]['kycvideo_status'];
                $cc_status = $user_nach[0]['cc_status'];
                $mandatesoft_status = $user_nach[0]['mandatesoft_status'];
                $mandatehard_status = $user_nach[0]['mandatehard_status'];
                $aof_uploaded = $user_nach[0]['aof_uploaded'];
                $camskyc_status = $user_nach[0]['camskyc_status'];
                $bsemandate_status = $user_nach[0]['bsemandate_status'];
                $bseaof_status = $user_nach[0]['bseaof_status'];

                   if ($update_nach) {

                        //dd('if ulla');

                        if($bseclient_status == 1 && $pan_status == 1 && $kyc_status == 1 && $kycvideo_status == 1 && $cc_status == 1 && $mandatesoft_status == 1 && $mandatehard_status == 1 && $aof_uploaded == 1 && $camskyc_status == 1 && $bsemandate_status == 1 && $bseaof_status == 1){

                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 1]);
                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 0]);
                            if($user_nach){
                                $this->handleNotifications($request['user_id'],$request['col_name'],$request['option']);
                                 return $this->getUserNachDetails($request);
                             }else{
                                return response()->json(['msg'=>'failure']);
                             }

                        }else{

                            //dd('inga');

                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 1]);
                            $user_nach = NachDetails::where('user_id',$request['user_id'])->update(['nach_status' => 0]);



                            //dd($user_nach);
                            if($user_nach){
                                $this->handleNotifications($request['user_id'],$request['col_name'],$request['option']);
                                //dd('inga');
                                 return $this->getUserNachDetails($request);
                             }else{
                                //dd('inga pa');
                                return response()->json(['msg'=>'failure']);
                             }
                        }

                       
                   }else{

                            //dd('else ulla');
                            return $this->getUserNachDetails($request);
                   }
           }

           
           //dd($col_date);
           
           
        }

    }


    public function getNoti(){
        $this->handleNotifications(54,'cc_status',1);
    }

    public function handleNotifications($user_id,$noti_name,$option){
            //user_id,noti_name,option
            $soft = array(' ','verified', 'not verified');
            $hard = array(' ','received', 'not received');



           $response = array(

                'pan_status' => array('Pan',$soft[$option]),
                'kyc_status' => array('KYC',$soft[$option]),
                'kycvideo_status' => array('KYC Video',$soft[$option]),
                'cc_status' => array('Cancelled Cheque',$soft[$option]),
                'mandatesoft_status' => array('Mandate',$soft[$option]),
                'mandatehard_status' => array('Mandate',$hard[$option]),
            );
           

           if(array_key_exists($noti_name, $response)){
                //echo "Your ".$response[$noti_name][0].' has been '.$response[$noti_name][1];

                $notify = new Notifications();
                $notify->description = "Your ".$response[$noti_name][0].' has been '.$response[$noti_name][1];
                $notify->user_id = $user_id;
                $notify->date = date('Y-m-d',strtotime(Carbon::now()->toDateString()));
                $save = $notify->save();

                if($save){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }

           // $pan_status = $user_nach[0]['pan_status'];
           // $kyc_status = $user_nach[0]['kyc_status'];
           // $kycvideo_status = $user_nach[0]['kycvideo_status'];
           // $cc_status = $user_nach[0]['cc_status'];
           // $mandatesoft_status = $user_nach[0]['mandatesoft_status'];
           // $mandatehard_status = $user_nach[0]['mandatehard_status'];
           // $aof_uploaded = $user_nach[0]['aof_uploaded'];
           // $camskyc_status = $user_nach[0]['camskyc_status'];
           // $bsemandate_status = $user_nach[0]['bsemandate_status'];
           // $bseaof_status = $user_nach[0]['bseaof_status'];

           // You are PAN is verified 
    }



    public function pendingOrders()
    {

        $this->createRecurringSipInvestment();
        $pending_investments = $this->getPendingOrders('data');
        return view('admin.admin-pending-orders')->with('investment_details',$pending_investments);
        //$this->createRecurringSipInvestment();
        //$investment_details = $this->getPendingOrders('data');

        
        //dd($investment_details);
        //foreach ($investment_details as $investment) {
            //$pending_orders_collection[] = PortfolioDetails::where('portfolio_status',0)->where('investment_id',$investment['investment_id'])->get();

            //echo $investment['investment_id'];
        //}
        //dd($pending_orders_collection,$investment_details);
        //return view('admin.new_pending_orders')->with(compact('investment_details','pending_orders_collection'));
        /*$users = User::all();

        //iterating all users to get pending investments
        foreach ($users as $user) {
            
            //for user investments
            $user_investments = $user->investmentDetails()
                ->where('investment_status', 0)
                ->get();

            //iterating all pending investments of the user
            foreach($user_investments as $user_investment) {
                $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1])
                    ->get();

                //iterating all user portfolios to get peding portfolio
                foreach($user_portfolios as $user_portfolio)
                {
                    $investment_date = '';
                    if(array_key_exists($user_portfolio->investment_id, $pending_investments)) {
                        $pending_investments[$user_portfolio->investment_id]['amount'] += $user_portfolio->amount_invested;
                    } else {
                        if($user_portfolio->investment_date != NULL && $user_portfolio->investment_date != '') {
                            $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                        }
                        $pending_investments[$user_portfolio->investment_id] = array(
                            'id' => $user_portfolio->investment_id,
                            'user_name' => $user->name,
                            'contact_no' => $user->mobile,
                            'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                            'investment_type' => $this->investment_types[$user_investment->investment_type],
                            'investment_or_withdraw' => 'Investment',
                            'date' => $investment_date,
                            'amount' => $user_portfolio->amount_invested,
                        );
                    }
                }
            }


            //for user withdrawls
            $user_withdraws = $user->WithdrawDetails
                ->where('withdraw_status', 0)
                ->groupBy('withdraw_group_id');
            if($user_withdraws->count() > 0) {
                foreach($user_withdraws as $user_withdraw) {
                    $withdraw_data = $user_withdraw->first();
                    $withdraw_date = '';
                    if($withdraw_data->withdraw_date != NULL && $withdraw_data->withdraw_date != '') {
                        $withdraw_date = date('d-m-Y',strtotime($withdraw_data->withdraw_date));
                    }
                    $pending_investments[$withdraw_data->withdraw_group_id] = array(
                        'id' => $withdraw_data->withdraw_group_id,
                        'user_name' => $user->name,
                        'contact_no' => $user->mobile,
                        'portfolio_type' => '',
                        'investment_type' => '',
                        'investment_or_withdraw' => 'Withdraw',
                        'date' => $withdraw_date,
                        'amount' => $user_withdraw->sum('withdraw_amount'),
                    );
                }
            }
        }*/
        //print_r($pending_investments);
        //return view('admin.admin-pending-orders')->with('investment_details',$pending_investments);
    }


    public function newPendingOrders($data = null){

        
        //calling the below function to check the SIP table for SIP investments and adding them in the investment details table
        $this->createRecurringSipInvestment();
        $user_id = '';
        $investment_details = array();
        $pending_investments = array();
        $pending_withdraw = array();
        $response_type = 'data';
        $user_folio;


        if($user_id == '') {
            if (\Auth::id() == 145) {
                // $bpoids = NachDetails::where('priority',null)->pluck('user_id');
                $users = User::where('handle_by',0)->get();
            }else{
                $users = User::all();
            }
        } else {
            $users = User::find($user_id)->get();
        }

        //dd($users);

        foreach ($users as $user) {

            //getting user's investment detail 
           $user_investments = $user->investmentDetails()
                        ->where('investment_status', 0)
                        ->where('investment_type','!=',2)
                        ->orderBy('investment_date', 'asc')
                        //->whereBetween('investment_date', [$start_date, $end_date]);
                        ->get();

            foreach($user_investments as $user_investment) {

                    $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1])
                    ->get();


                    foreach($user_portfolios as $user_portfolio)
                    {
                        $investment_date = '';
                        if(array_key_exists($user_portfolio->investment_id, $pending_investments)) {
                            if($response_type == 'data') {
                                $pending_investments[$user_portfolio->investment_id]['amount'] += $user_portfolio->amount_invested;
                            } else {
                                $pending_investments[$user_portfolio->investment_id]['Amount'] += $user_portfolio->amount_invested;
                                //dd($pending_investments);
                            }
                        } else {


                            //below if condition is to get the Investment placed date and order to be executed date

                            if($user_portfolio->investment_date != NULL && $user_portfolio->investment_date != '') {
                                $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                                $order_placed_date = date('d-m-Y',strtotime($user_portfolio->created_at));

                            }
                            if($response_type == 'data') {


                                //echo $user_portfolio->scheme_code;
                                $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$user_portfolio->scheme_code)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                                

                                $pending_investments[$user_portfolio->investment_id] = array(
                                    'investment_id' => $user_portfolio->investment_id,
                                    'user_id' => $user->id,
                                    'user_name' => $user->name,
                                    'user_pan' => $user->personalDetails->pan,
                                    'contact_no' => $user->mobile,
                                    'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                                    'investment_type' => $this->investment_types[$user_investment->investment_type],
                                    'investment_or_withdraw' => 'Investment',
                                    'execution_date' => $investment_date,
                                    'placed_date' => $order_placed_date,
                                    'amount' => $user_portfolio->amount_invested,
                                    'folio_numer' => $user_folio['folio_number'],
                                );
                            } else {
                                $pending_investments[$user_portfolio->investment_id] = array(
                                    'SNo' => $count,
                                    'User Name' => $user->name,
                                    'Contact Number' => $user->mobile,
                                    'Portfolio Type' => $this->portfolio_types[$user_investment->portfolio_type],
                                    'Investment Type' => $this->investment_types[$user_investment->investment_type],
                                    'Transaction Type' => 'Investment',
                                    'Date' => $investment_date,
                                    'Amount' => $user_portfolio->amount_invested,
                                );
                                $count++;
                            }
                        }
                    }//foreach of User Portfolio ends here

            }//foreach of User Investments ends here


             /*
                    Getting Withdrawal Order starts below   

            */



                    $user_withdraws = $user->WithdrawDetails
                        ->where('withdraw_status', 0)
                        ->groupBy('withdraw_group_id');

                        foreach ($user_withdraws as $user_withdraw => $withdraws) {
                           foreach ($withdraws as $withdraw) {


                            if($withdraw->withdraw_date != NULL && $withdraw->withdraw_date != '') 
                            {
                                            $withdraw_date = date('d-m-Y',strtotime($withdraw->withdraw_date));
                                            $order_placed_date = date('d-m-Y',strtotime($withdraw->created_at));

                            }

                                //echo $user_portfolio->scheme_code;
                                $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$withdraw->scheme_code)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();



                                if (array_key_exists($withdraw->withdraw_group_id, $pending_withdraw)) {

                                    //echo $withdraw->withdraw_group_id."<br>";

                                    $pending_withdraw[$withdraw->withdraw_group_id] = array(
                                                'withdraw_id' => $withdraw->id,
                                                'withdraw_group_id' => $withdraw->withdraw_group_id,
                                                'user_id' => $user->id,
                                                'user_name' => $user->name,
                                                'user_pan' => $user->personalDetails->pan,
                                                'contact_no' => $user->mobile,
                                                'portfolio_type' => $withdraw->portfolio_type,
                                                //'investment_type' => $this->investment_types[$user_investment->investment_type],
                                                'investment_or_withdraw' => 'Withdraw',
                                                'execution_date' => $withdraw_date,
                                                'placed_date' => $order_placed_date,
                                                'amount' => $pending_withdraw[$withdraw->withdraw_group_id]['amount'] + $withdraw->withdraw_amount,
                                                'folio_numer' => $user_folio['folio_number'],
                                            );

                                    
                                }else{

                                    $pending_withdraw[$withdraw->withdraw_group_id] = array(
                                                'withdraw_id' => $withdraw->id,
                                                'withdraw_group_id' => $withdraw->withdraw_group_id,
                                                'user_id' => $user->id,
                                                'user_name' => $user->name,
                                                'user_pan' => $user->personalDetails->pan,
                                                'contact_no' => $user->mobile,
                                                'portfolio_type' => $withdraw->portfolio_type,
                                                //'investment_type' => $this->investment_types[$user_investment->investment_type],
                                                'investment_or_withdraw' => 'Withdraw',
                                                'execution_date' => $withdraw_date,
                                                'placed_date' => $order_placed_date,
                                                'amount' => $withdraw->withdraw_amount,
                                                'folio_numer' => $user_folio['folio_number'],
                                            );
                                }

                               
                           }
                        }



       }//foreach of $users ends here

        
        $investment_details = $pending_investments;
        $withdraw_details = $pending_withdraw;

        //The below code is to loop through pending Investments and store it in the variable $pending_investment_collection

        foreach ($investment_details as $investment_detail) {
                //dd($investment_detail['investment_id']);
                $pending_investment_collection[] = PortfolioDetails::whereIn('portfolio_status',[0,1])->where('investment_id',$investment_detail['investment_id'])->get();                      
        }


        //The below code is to loop through pending Withdraws and store it in the variable $pending_withdraws_collection
        foreach ($withdraw_details as $withdraw_detail) {



                $pending_withdraw_collection[] = WithdrawDetails::where('withdraw_status',0)->where('withdraw_group_id',$withdraw_detail['withdraw_group_id'])->get();                      
        }

        //dd($pending_investment_collection);

        if (!empty($pending_investment_collection)) {
            foreach ($pending_investment_collection as $pending_orders) {

                foreach ($pending_orders as $pending_order) {
                    # code...
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name','bse_scheme_code')->toArray();
                    $bse_scheme_code = array_keys($scheme_name);
                    $pending_order['scheme_name'] = $scheme_name[$bse_scheme_code[0]];
                    $pending_order['bse_scheme_code'] = $bse_scheme_code[0];

                    //dd($pending_order);

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];

                }
            }
        }


        if (!empty($pending_withdraw_collection)) {
            foreach ($pending_withdraw_collection as $pending_orders) {

                foreach ($pending_orders as $pending_order) {
                    # code...
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name');
                    $pending_order['scheme_name'] = $scheme_name[0];

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];

                }
            }
        }


        

        /*Getting Pending Investments and Pending Withdrawals are Over. 

                ***** About Investment *****

                Investment details are stored in variable $investment_details
                Portfolio details of the investments are stored in $pending_investment_collection


                ***** About Withdrawals ****

                Withdraw details are stored in variable $withdraw_details
                Details of the withdrawals are stored in $pending_withdraw_collection

        */


        if($data == 'export'){

            $response_array = array('pending_investment' => $pending_investment_collection,'pending_withdraw'=>$pending_withdraw_collection);

            return $response_array;

        }else{

            // dd($pending_investment_collection);
            usort($investment_details, function($a, $b) {
                return strtotime($a['execution_date']) - strtotime($b['execution_date']);
            });

            return view('admin.new_pending_orders')->with(compact('investment_details','pending_investment_collection','withdraw_details','pending_withdraw_collection'));
        }

        
    }

    public function pendingSipOrders(){

                //calling the below function to check the SIP table for SIP investments and adding them in the investment details table
        $this->createRecurringSipInvestment();
        $user_id = '';
        $investment_details = array();
        $pending_investments = array();
        $pending_withdraw = array();
        $response_type = 'data';
        $user_folio;


        if($user_id == '') {
            if (\Auth::id() == 145) {
                // $bpoids = NachDetails::where('priority',null)->pluck('user_id');
                $users = User::where('handle_by',0)->get();
            }else{
                $users = User::all();
            }
        } else {
            $users = User::find($user_id)->get();
        }

        //dd($users);

        foreach ($users as $user) {

            //getting user's investment detail 
           $user_investments = $user->investmentDetails()
                        ->where('investment_status', 0)
                        ->where('investment_type',2)
                        ->orderBy('investment_date', 'asc')
                        //->whereBetween('investment_date', [$start_date, $end_date]);
                        ->get();


            foreach($user_investments as $user_investment) {

                    $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1])
                    ->get();



                    foreach($user_portfolios as $user_portfolio)
                    {
                        $investment_date = '';
                        if(array_key_exists($user_portfolio->investment_id, $pending_investments)) {
                            if($response_type == 'data') {
                                $pending_investments[$user_portfolio->investment_id]['amount'] += $user_portfolio->amount_invested;
                            } else {
                                $pending_investments[$user_portfolio->investment_id]['Amount'] += $user_portfolio->amount_invested;
                                //dd($pending_investments);
                            }
                        } else {


                            //below if condition is to get the Investment placed date and order to be executed date
                            // $now = date('m-Y',strtotime(Carbon::now()));

                            if($user_portfolio->investment_date != NULL && $user_portfolio->investment_date != '') {
                                // $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                                $order_placed_date = date('d-m-Y',strtotime($user_portfolio->created_at));

                                $date = Sip::where('id',$user_portfolio->sip_id)->value('start_date');
                                $day = date('d',strtotime($date));
                                $investment_date = $day."-".date('m')."-".date('Y');
                                // dd($investment_date);
                            }
                            if($response_type == 'data') {


                                //echo $user_portfolio->scheme_code;
                                $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$user_portfolio->scheme_code)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                                

                                $pending_investments[$user_portfolio->investment_id] = array(
                                    'investment_id' => $user_portfolio->investment_id,
                                    'user_id' => $user->id,
                                    'user_name' => $user->name,
                                    'user_pan' => $user->personalDetails->pan,
                                    'contact_no' => $user->mobile,
                                    'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                                    'investment_type' => $this->investment_types[$user_investment->investment_type],
                                    'investment_or_withdraw' => 'Investment',
                                    'execution_date' => $investment_date,
                                    'placed_date' => $order_placed_date,
                                    'amount' => $user_portfolio->amount_invested,
                                    'folio_numer' => $user_folio['folio_number'],
                                );
                            } else {
                                $pending_investments[$user_portfolio->investment_id] = array(
                                    'SNo' => $count,
                                    'User Name' => $user->name,
                                    'Contact Number' => $user->mobile,
                                    'Portfolio Type' => $this->portfolio_types[$user_investment->portfolio_type],
                                    'Investment Type' => $this->investment_types[$user_investment->investment_type],
                                    'Transaction Type' => 'Investment',
                                    'Date' => $investment_date,
                                    'Amount' => $user_portfolio->amount_invested,
                                );
                                $count++;
                            }
                        }
                    }//foreach of User Portfolio ends here

            }//foreach of User Investments ends here


       }//foreach of $users ends here

        
        $investment_details = $pending_investments;

        //The below code is to loop through pending Investments and store it in the variable $pending_investment_collection

        foreach ($investment_details as $investment_detail) {
                //dd($investment_detail['investment_id']);
                $pending_investment_collection[] = PortfolioDetails::whereIn('portfolio_status',[0,1])->where('investment_id',$investment_detail['investment_id'])->get();                      
        }

        //dd($pending_investment_collection);

        if (!empty($pending_investment_collection)) {
            foreach ($pending_investment_collection as $pending_orders) {
                //dd($pending_orders);
                foreach ($pending_orders as $pending_order) {

                    //dd($pending_order);
                    
                    $scheme_code = $pending_order['scheme_code'];
                    //dd(PortfolioDetails::where('scheme_code',$scheme_code)->where('portfolio_type'=>$pending_order['portfolio_type']))
                    # code...
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name','bse_scheme_code')->toArray();


                    $bse_scheme_code = array_keys($scheme_name);
                    $pending_order['scheme_name'] = $scheme_name[$bse_scheme_code[0]];
                   //$pending_order['scheme_code'] = $scheme_name[$bse_scheme_code[0]];
                    $pending_order['bse_scheme_code'] = $bse_scheme_code[0];
                    $bse_reg = SipEntries::where('sip_id',$pending_order['sip_id'])->where('scheme_code',$pending_order['scheme_code'])->get()->toArray();


                    //dd($pending_order);

                    if (count($bse_reg) == 0) {
                        $pending_order['bse_sip_reg'] = '';  
                        $pending_order['bse_reg_date'] = '';
                    }else{
                        $bse_reg = $bse_reg[0];
                        if ($bse_reg['bse_reg'] == null || $bse_reg['bse_reg'] == 0) {
                            $pending_order['bse_sip_reg'] = '';  
                            $pending_order['bse_reg_date'] = '';
                        }else{
                            $pending_order['bse_sip_reg'] = $bse_reg['bse_reg'];
                            $pending_order['bse_reg_date'] = date('d-m-Y',strtotime($bse_reg['bse_reg_date']));
                        }
                    }
                    
                    //dd($pending_order);

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];

                }
            }
        }
        

        /*Getting Pending Investments and Pending Withdrawals are Over. 

                ***** About Investment *****

                Investment details are stored in variable $investment_details
                Portfolio details of the investments are stored in $pending_investment_collection

        */


        // if($data == 'export'){

        //     $response_array = array('pending_investment' => $pending_investment_collection,'pending_withdraw'=>$pending_withdraw_collection);

        //     return $response_array;

        // }else{
            return view('admin.sip_pending_orders')->with(compact('investment_details','pending_investment_collection'));
        //}
    }


    public function getAllSip(){

        if (\Auth::id() == 145) {
            // $bpoids = NachDetails::where('priority',null)->pluck('user_id');
            $bpoids = User::where('handle_by',0)->pluck('id');
            $sips = Sip::whereIn('user_id',$bpoids)->get();
        }else{
            $sips = Sip::all();
        }
        $sip_array = [];
        $sip_details_array = [];
        $count = 0;

        $scheme_name = SchemeDetails::all()->pluck('scheme_name','scheme_code')->toArray();
        foreach ($sips as $sip) {
            $user_name = User::where('id',$sip['user_id'])->pluck('name');
            //$sip_array[$count]['user_name'] = $user_name[0];
            //$scheme_code = PortfolioDetails::where('id',$sip['port_id'])->pluck('scheme_code')->toArray();
            //$scheme_code = $scheme_code[0];
            //echo $scheme_code;
            //die();
            //$scheme_name = SchemeDetails::where('scheme_code',$scheme_code)->pluck('scheme_name');
// dd($sip);
            $sip_array[$count]['investor_name'] = $user_name[0];
            $sip_array[$count]['sip_amount'] = $sip->inv_amount;
            $sip_array[$count]['tenure'] = $sip->tenure;
            $sip_array[$count]['sip_id'] = $sip->id;

            //$sip_entries = ->toArray();

                $sip_entry_count = 0;
                foreach ($sip->sipEntries as $sip_entry) {
                    $sip_details_array[$sip->id][$sip_entry_count]['scheme_name'] = $scheme_name[$sip_entry->scheme_code];
                    $sip_details_array[$sip->id][$sip_entry_count]['inv_amount'] = $sip_entry->amount;
                    $sip_details_array[$sip->id][$sip_entry_count]['sip_id'] = $sip_entry->sip_id;
                    $sip_entry_count++;
                }
            $count++;
            //$sip_array[$count]['user_name'] = $user_name[0];
        }

        return view('admin.all_sip')->with(compact(['sip_array','sip_details_array']));
    }


    public function exportPendingOrders(){

        $response = $this->newPendingOrders('export');

        $export_array = array();

        $pending_investment = $response['pending_investment'];
        //dd($pending_investment);
        $pending_withdraw = $response['pending_withdraw'];

        foreach ($pending_investment as $key => $value) {
            foreach ($value as $investment) {

                if($investment['folio_number'] == ''){
                    $purchase_type = 'FRESH';
                    $folio_number = '';
                }else{
                    $purchase_type = 'ADDITIONAL';
                    $folio_number = trim($investment['folio_number']);
                }   

                $pan = PersonalDetails::where('user_id',$investment['user_id'])->pluck('pan');
                $export_array[] = trim($investment['bse_scheme_code']).'|'.'P'.'|'.$purchase_type.'|'.$pan[0].'|'.'P'.'|'.$investment['amount_invested'].'|'.$folio_number.'|'.''.'|'.'Y'.'|'.''.'|'.'E173580'.'|'.'Y'.'|'.'N'.'|'.'N'.'|'.'N'.'|'.''.'|'.PHP_EOL;
            }
        }

        // $file_name = date('d-m-Y').'- Pending Orders.txt';
        // $myfile = fopen($file_name, "w");
        // fwrite($myfile, 'hello');
        // fclose($myfile);
        //  return \Response::download($myfile);
        $file_name = date('d-m-Y').'- Pending Orders.txt';
        //foreach ($export_array as $entry ) {
            //echo $entry."<br>";
            \File::put(public_path('/files/bulkupload/'.$file_name),$export_array);
        //}

         // $data = json_encode(['Example 1','Example 2','Example 3',]);
         // 
         // \File::put(public_path('/files/upload'.$file_name),$data);
         return \Response::download(public_path('/files/bulkupload/'.$file_name));

        //dd($export_array);
    }


    //function to get pending orders details based on user and range
    public function getPendingOrders( $response_type, $user_id='', $start_date='', $end_date='')
    {
        $users;
        $count = 1;
        $pending_investments = array();
        if($user_id == '') {
            $users = User::all();
        } else {
            $users = User::find($user_id)->get();
        }

        //iterating all users to get pending investments
        foreach ($users as $user) {
            
            //for user investments if the start date and end date is given
             if($start_date != '' && $end_date !='') {
                    $user_investments = $user->investmentDetails()
                        ->where('investment_status', 0)
                        ->whereBetween('investment_date', [$start_date, $end_date])
                        ->orderBy('investment_date', 'asc')
                        ->get();
                } else {
                    $user_investments = $user->investmentDetails()
                        ->where('investment_status', 0)
                        ->orderBy('investment_date', 'asc')
                        //->whereBetween('investment_date', [$start_date, $end_date]);
                        ->get();
                }
    
            //iterating all pending investments of the user
            foreach($user_investments as $user_investment) {
                
                //getting all the portfolio of  each investment from the portfolio details table
                $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1])
                    ->get();

                //iterating all user portfolios to get pending portfolio
                foreach($user_portfolios as $user_portfolio)
                {
                    $investment_date = '';
                    if(array_key_exists($user_portfolio->investment_id, $pending_investments)) {
                        if($response_type == 'data') {
                            $pending_investments[$user_portfolio->investment_id]['amount'] += $user_portfolio->amount_invested;
                        } else {
                            $pending_investments[$user_portfolio->investment_id]['Amount'] += $user_portfolio->amount_invested;
                            //dd($pending_investments);
                        }
                    } else {
                        if($user_portfolio->investment_date != NULL && $user_portfolio->investment_date != '') {
                            $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                            $order_placed_date = date('d-m-Y',strtotime($user_portfolio->created_at));

                        }
                        if($response_type == 'data') {


                            //echo $user_portfolio->scheme_code;
                            $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$user_portfolio->scheme_code)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                            

                            $pending_investments[$user_portfolio->investment_id] = array(
                                'investment_id' => $user_portfolio->investment_id,
                                'user_id' => $user->id,
                                'user_name' => $user->name,
                                'user_pan' => $user->personalDetails->pan,
                                'contact_no' => $user->mobile,
                                'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                                'investment_type' => $this->investment_types[$user_investment->investment_type],
                                'investment_or_withdraw' => 'Investment',
                                'execution_date' => $investment_date,
                                'placed_date' => $order_placed_date,
                                'amount' => $user_portfolio->amount_invested,
                                'folio_numer' => $user_folio['folio_number'],
                            );
                        } else {
                            $pending_investments[$user_portfolio->investment_id] = array(
                                'SNo' => $count,
                                'User Name' => $user->name,
                                'Contact Number' => $user->mobile,
                                'Portfolio Type' => $this->portfolio_types[$user_investment->portfolio_type],
                                'Investment Type' => $this->investment_types[$user_investment->investment_type],
                                'Transaction Type' => 'Investment',
                                'Date' => $investment_date,
                                'Amount' => $user_portfolio->amount_invested,
                            );
                            $count++;
                        }
                    }
                }
            }


            //for user withdrawls
            $user_withdraws = $user->WithdrawDetails
                ->where('withdraw_status', 0)
                ->groupBy('withdraw_group_id');
            if($user_withdraws->count() > 0) {
                foreach($user_withdraws as $user_withdraw) {
                    $withdraw_data = $user_withdraw->first();
                    $withdraw_date = '';
                    if($withdraw_data->withdraw_date != NULL && $withdraw_data->withdraw_date != '') {
                        $withdraw_date = date('d-m-Y',strtotime($withdraw_data->withdraw_date));
                    }
                    if($response_type == 'data') {
                        $pending_investments[$withdraw_data->withdraw_group_id] = array(
                            'id' => $withdraw_data->withdraw_group_id,
                            'user_name' => $user->name,
                            'contact_no' => $user->mobile,
                            'portfolio_type' => '',
                            'investment_type' => '',
                            'investment_or_withdraw' => 'Withdraw',
                            'date' => $withdraw_date,
                            'amount' => $user_withdraw->sum('withdraw_amount'),
                        );
                    } else {
                        $pending_investments[] = array(
                            'SNo' => $count,
                            'User Name' => $user->name,
                            'Contact Number' => $user->mobile,
                            'Portfolio Type' => '',
                            'Investment Type' => '',
                            'Transaction Type' => 'Withdraw',
                            'Date' => $withdraw_date,
                            'Amount' => $user_withdraw->sum('withdraw_amount'),
                        );
                        $count++;
                    }
                }
            }
        }
        return $pending_investments;
    }




    ////function to get invdvidual user pending order details
    public function getIndUserPendingOrders(Request $request)
    {
        $response_type = 'document';
        $user_id = '';
        $start_date = '';
        $end_date = '';
        $user_pending_orders = array();
        $request = $request->all();
        if(array_key_exists('user_id', $request)) {
            $user_id = $request['user_id'];
        }
        if(array_key_exists('response_type', $request)) {
            $response_type = $request['response_type'];
        }
        if(array_key_exists('start_date', $request)) {
            if($request['start_date'] != '') {
                $start_date = date('Y-m-d', strtotime($start_date));
            }
        }
        if(array_key_exists('end_date', $request)) {
            if($request['end_date'] != '') {
                $end_date = date('Y-m-d', strtotime($end_date));
            } 
        }
        $user_pending_orders = $this->getPendingOrders($user_id, $start_date, $end_date, $response_type);
        if($response_type == 'data') {
            return response()->json(['msg' => 'success', 'response' => $user_pending_orders]);    
        } else {
            $file_content = Excel::create('Pending Orders', function($excel) use ($user_pending_orders) {
                $excel->sheet('mySheet', function($sheet) use ($user_pending_orders)
                {
                    $sheet->fromArray($user_pending_orders);
                });
            })->download('xlsx');   
        }
        
    }

    


    public function updateBseDate(Request $request){


        $validator = \Validator::make($request->all(),[

            'portfolio_id' => 'required',
            'status' => 'required',
            'pending_type' => 'required',
            ]);

        if ($validator->fails()) {
            
            return $validator->errors();
        }

        else{


            // return "else la irukku pa";

            $date = Carbon::now();

            if ($request['status'] == 'checked') {
                    
                if ($request['pending_type'] == 'i') {
                    //echo $request['portfolio_id'];
                   $update =  PortfolioDetails::where('id',$request['portfolio_id'])->update(['bse_order_date'=>$date]);

                    if ($update) {

                        //return "updated";

                       $date = date('d/m/Y h:i:s A', strtotime($date));
                       return response()->json(['msg'=>'date_success','portfolio_id'=>$request['portfolio_id'],'date'=>$date]);
                    }
                }

                if ($request['pending_type'] == "w") {


                    
    
                    $update =  WithdrawDetails::where('id',$request['portfolio_id'])->update(['withdraw_bse_date'=>$date]);

                    //return "hello";

                    if ($update) {


                        //return "updated";

                       $date = date('d/m/Y h:i:s A', strtotime($date));
                       return response()->json(['msg'=>'date_success','portfolio_id'=>$request['portfolio_id'],'date'=>$date]);
                    }
                }
               
            }

            

            if ($request['status'] == 'not_checked') {
                    
                if ($request['pending_type'] == 'i') {

                    $update =  PortfolioDetails::where('id',$request['portfolio_id'])->update(['bse_order_date'=> NULL]);

                    if ($update) {
                        return response()->json(['msg'=>'nodate_success','portfolio_id'=>$request['portfolio_id'],'date'=>'']);
                    }
                }

                if ($request['pending_type'] == 'w') {

                    $update =  WithdrawDetails::where('id',$request['portfolio_id'])->update(['withdraw_bse_date'=> NULL]);

                    if ($update) {
                        return response()->json(['msg'=>'nodate_success','portfolio_id'=>$request['portfolio_id'],'date'=>'']);
                    }
                }
            }

        }


    }



    public function updateSipBseDate(Request $request){


        $validator = \Validator::make($request->all(),[

            'portfolio_id' => 'required',
            'status' => 'required',
            ]);

        if ($validator->fails()) {
            
            return $validator->errors();
        }

        else{


            $date = Carbon::now();

            if ($request['status'] == 'checked') {


                //$bse_sip_reg = Sip::where('p')
                    
                   $update =  PortfolioDetails::where('id',$request['portfolio_id'])->update(['bse_order_date'=>$date]);

                    if ($update) {

                       $date = date('d/m/Y h:i:s A', strtotime($date));
                       return response()->json(['msg'=>'date_success','portfolio_id'=>$request['portfolio_id'],'date'=>$date]);
                    }
               
            }

            

            if ($request['status'] == 'not_checked') {
                    
                    $update =  PortfolioDetails::where('id',$request['portfolio_id'])->update(['bse_order_date'=> NULL]);

                    if ($update) {
                        return response()->json(['msg'=>'nodate_success','portfolio_id'=>$request['portfolio_id'],'date'=>'']);
                    }

            }

        }


    }


    public function updateSipRegNo(Request $request){


        $validator = \Validator::make($request->all(),[

            'sip_id' => 'required',
            // 'sipreg_no' => 'required',
            'sip_status' => 'required',
            'sip_scheme_code' => 'required',
            ]);

        if ($validator->fails()) {
            
            return $validator->errors();
        }

        else{

            $date = Carbon::now();
            $date = date('d/m/Y', strtotime($date));

            if ($request['sip_status'] == 'checked') {
                $reg_date = Carbon::now();
                $reg_date = date('Y-m-d',strtotime($reg_date));
                $update_sip = SipEntries::where('sip_id',$request['sip_id'])->where('scheme_code',$request['sip_scheme_code'])->update(['sip_reg_no'=>$request['sipreg_no'],'bse_reg_date'=>$reg_date,'bse_reg'=>1]);

                $port_details = PortfolioDetails::where('sip_id',$request['sip_id'])->where('scheme_code',$request['sip_scheme_code'])->update(['sip_reg_no'=>$request['sipreg_no']]);
            }else{
                $update_sip = SipEntries::where('sip_id',$request['sip_id'])->where('scheme_code',$request['sip_scheme_code'])->update(['sip_reg_no'=>null,'bse_reg_date'=>null,'bse_reg'=>null]);
            }
            
            
            if ($update_sip) {

                return response()->json(['msg'=>'success','sip_id'=>$request['sip_id'],'date'=>$date,'scheme_code'=>$request['sip_scheme_code']]);
               
            }else{
                return response()->json(['msg'=>'failed','sip_id'=>$request['sip_id'],'date'=>$date,'scheme_code'=>$request['sip_scheme_code']]);
            }   
        }


    }


    public function updateSipRegDate($sip_id,$status){
        
            
            $date = Carbon::now();

            if ($status == 'checked') {


                //$bse_sip_reg = Sip::where('p')

                    
                   $update =  Sip::where('id',$sip_id)->update(['bse_reg_date'=>$date,'bse_reg'=>1]);

                    if ($update) {

                       $date = date('d/m/Y h:i:s A', strtotime($date));
                       return response()->json(['msg'=>'success','sip_id'=>$sip_id,'date'=>$date]);
                    }
               
            }

            

            if ($status == 'not_checked') {
                    
                    $update =  Sip::where('id',$sip_id)->update(['bse_reg_date'=> NULL,'bse_reg'=>0]);

                    if ($update) {
                        return response()->json(['msg'=>'failed','sip_id'=>$sip_id,'date'=>'']);
                    }

            }
        
    }

    public function checkSipReg(Request $request){
        $validator = \Validator::make($request->all(),[

            'sip_id' => 'required',
            'scheme_code' => 'required',
            
            ]);

        if ($validator->fails()) {            
            return $validator->errors();
        }

        else{

            $sip_reg = SipEntries::where('sip_id',$request['sip_id'])->where('scheme_code',$request['scheme_code'])->pluck('bse_reg');
            if ($sip_reg[0] == 1) {
                return response()->json(['msg'=>'r']);
            }else{
                return response()->json(['msg'=>'nr']);
            }
        }
    }


    public function completePendingOrders(Request $request){
        $validator = \Validator::make($request->all(),[

            'order_ids' => 'required',
            'order_type' => 'required',
            
            ]);

        if ($validator->fails()) {            
            return $validator->errors();
        }

        else{

            $order_ids = $request['order_ids'];
            $order_type = $request['order_type'];

            $transaction_status_data = array();
            $nav_date = new Carbon();
            $current_date = $nav_date->format('Y-m-d');
            //$nav_date->subDay();
            $nav_date = $nav_date->format('Y-m-d');
            $investment_id = '';
            $save_status = false;
            $approved_amount = 0;
            $save_message = '';//Log::info($pending_portfolios);
            $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
            //dd($pending_portfolios);

            $scheme_code_name = $scheme_details->toArray();
            $notif_mode = 0;
            $user;


                //If order type is Investment it is processed below

                if ($order_type == 'inv') {

                    $portfolio_ids = preg_split('/,/', $order_ids);
                    $portfolio_orders = array();

                    foreach ($portfolio_ids as $id) {

                        $portfolio_orders[] = PortfolioDetails::where('id',$id)->get();
                    }

                    //print_r($portfolio_orders);     


                    foreach ($portfolio_orders as $portfolio_order) {


                        foreach ($portfolio_order as $portfolio) {

                            $portfolio_detail = PortfolioDetails::where('id',$portfolio->id)
                            ->first();

                            if($portfolio_detail != NULL) {
                                if($portfolio_detail->portfolio_status == 0){

                                    //getting Investment detail of the particular portfolio details
                                    $investment_id = intval($portfolio_detail->investment_id);//Log::info('Portfolio id'.$portfolio_detail->id);
                                    $amount_invested = $portfolio_detail->amount_invested;
                                    $scheme_nav = DailyNav::where('date', '<', $nav_date)
                                    ->where('scheme_code', $portfolio_detail->scheme_code)
                                    ->orderBy('date', 'desc')
                                    ->first();//Log::info($nav_date);Log::info($scheme_nav);die();Log::info('get me if you can');

                                    $units_held = $amount_invested / $scheme_nav->nav_value;
                                    $portfolio_detail->invested_nav = $scheme_nav->nav_value;
                                    $portfolio_detail->units_held = $units_held;
                                    $portfolio_detail->initial_units_held = $units_held;
                                    $portfolio_detail->investment_date = $current_date;
                                    $portfolio_detail->portfolio_status = 1;
                                    $portfolio_detail->save();

                                    
                                    $transaction_status_data[] = array(
                                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                                        'scheme_amount'=> $amount_invested,
                                        'status' => $portfolio_detail->portfolio_status,
                                    );
                                    $user = $portfolio_detail->User;
            //Log::info($transaction_status_data);
                                    $save_status = true;
                                    $save_message = "You have successfully approved an investment";
                                    $approved_amount += $amount_invested;
                                    $notif_mode = $portfolio_detail->investmentDetails->realise_response;
                                    
                                    /*if($notif_mode == 1) {
                                        $user = $portfolio_detail->User;
                                        Mail::send('admin.investment_realise', array('user_name' => $user->name, 'investment_date' => date('d-m-Y', strtotime($current_date)), 'investment_type' => 'Investment', 'amount_invested' => $approved_amount, 'investment_details' => $transaction_status_data), function ($message) use ($user) {
                                            $message->from('test@rightfundspartners.com', 'Investment Summary');
                                            $message->to($user->email, $user->name);
                                            //$message->attach('profile_pic.jpg');
                                        });
                                    } elseif($notif_mode == 2) {
                                        $ch = curl_init();
                                        $user="naveen@rightfunds.com:Rightfunds20";
                                        $receipientno = ;
                                        $senderID="RFUNDS";
                                        $msgtxt="Your Investment of Rs. ".$approved_amount." approved successfully"; 
                                        curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                                        $buffer = curl_exec($ch);
                                        curl_close($ch);
                                        //if(!empty ($buffer)) {
                                    }*/
                                }
                            } else {
                                $save_status = false;
                                $save_message = "Investment approve failed. Please check and try again";
                            }
                        }//foreach of portfolio id ends
                    }//foreach $portfolio_order ends
                    
                }//Investment order type ends

                /*If the order type is Withdrawal it is process below*/

                if ($order_type == 'wd') {



                    $withdraw_ids = preg_split('/,/', $order_ids);
                    $withdraw_orders = array();
                    //dd($withdraw_ids);
                    foreach ($withdraw_ids as $id) {

                        $withdraw_orders[] = WithdrawDetails::where('id',$id)->get();
                    }

                    //print_r($portfolio_orders);     

                    //dd($withdraw_orders);
                    foreach ($withdraw_orders as $withdraw_order) {
                        foreach ($withdraw_order as $withdraw) {
                            $withdraw_detail = WithdrawDetails::where('id',$withdraw->id)
                            ->first();

                            if($withdraw_detail != NULL) {
                                
                                //echo $withdraw_detail->withdraw_date;
                                $current_value = 0; //current value of the portfolio
                                $scheme_code = $withdraw_detail->scheme_code; //scheme code of the portfolio to be processed

                                // echo $scheme_code;
                                $withdraw_amount = $withdraw_detail->withdraw_amount; //amount to be withdrawn
                                // echo $withdraw_amount;
                                //get the nav details related to the current processing withdraw id

                                //$daily_nav = DailyNav::all();
                                //dd($daily_nav);
                                //dd($nav_date);

                                //echo $withdraw->scheme_code;
                                $scheme_nav =  DailyNav::where('date', '<', $nav_date)
                                    ->where('scheme_code', $withdraw_detail->scheme_code)
                                    ->orderBy('date', 'desc')
                                    ->first();
                                //dd($scheme_nav->nav_);


                                
                                //get the user related to the withdraw
                                $user = $withdraw_detail->User;


                                
                                //get the portfolio's detail related to the withdraw
                                $portfolio_details = $user->portfolioDetails()
                                    ->where('scheme_code', $scheme_code)
                                    ->where('portfolio_status', 1)
                                    ->get();


                                


                                //Getting current value just to check if the withdrawal amount is lesser than current amount;
                               $current_value = $portfolio_details->sum('units_held') * $scheme_nav->nav_value;

                               

                                if($current_value > $withdraw_amount) {
                                    $withdraw_units = $withdraw_amount / $scheme_nav->nav_value; //calculating the units to be withdrawn
                                    $withdraw_detail->withdraw_units = $withdraw_units;
                                    $withdraw_detail->withdraw_nav = $scheme_nav->nav_value;
                                    $withdraw_detail->withdraw_date = date('Y-m-d');
                                    $withdraw_detail->withdraw_status = 1;
                                    $portfolio_save_status;
                                    //loop through all active portfolio details of the scheme to reduce the withdrawn units
                                    foreach($portfolio_details as $portfolio_detail) {
                                        $user = $portfolio_detail->User;
                                        //check if the withdraw units is greater than 0
                                        if($withdraw_units > 0) {
                                            //if the units held and withdraw units are same
                                            if($portfolio_detail->units_held == $withdraw_units) {
                                                $withdraw_units = 0;
                                                $portfolio_detail->units_held = 0;
                                                $portfolio_detail->amount_invested = 0;
                                                $portfolio_detail->portfolio_status = 2;
                                                $portfolio_save_status = $portfolio_detail->save();
                                            } elseif($portfolio_detail->units_held < $withdraw_units) {
                                                //if the units held less than  withdraw units
                                                $withdraw_units -= $portfolio_detail->units_held;
                                                $portfolio_detail->units_held = 0;
                                                $portfolio_detail->amount_invested = 0;
                                                $portfolio_detail->portfolio_status = 2;
                                                $portfolio_save_status = $portfolio_detail->save();
                                            } elseif($portfolio_detail->units_held > $withdraw_units) {
                                                //if the units held is greater than the units to be withdrawn
                                                $portfolio_detail->units_held -= $withdraw_units;
                                                $portfolio_detail->amount_invested -= ($withdraw_units * $scheme_nav->nav_value);
                                                $withdraw_units = 0;
                                                $portfolio_save_status = $portfolio_detail->save();
                                            }
                                        }
                                    }
                                    if($portfolio_save_status) {
                                        if($withdraw_detail->save()) {
                                            $save_status = true;
                                            $save_message = "You have successfully approved an withdraw";
                                            $approved_amount += $withdraw_amount;
                                            $transaction_status_data[] = array(
                                                'scheme_name' => $scheme_code_name[$scheme_code],
                                                'scheme_amount'=> $withdraw_amount,
                                                'status' => 1,
                                            );
                                        } else {
                                            $save_status = false;
                                            $save_message = "Withdraw save failed. Please check and try again";
                                        }
                                    } else {
                                        $save_status = false;
                                        $save_message = "Portfolio Save Status failed. Please check and try again";
                                    }
                                } else {
                                    $save_status = false;
                                    $save_message = "Withdraw value is greater than current value. Please check and try again";
                                }
                            }
                        }//foreach of $withdraw ends
                    }//foreach of $withdraw_order ends
                }// Withdraw Order type ends

                if($investment_id !='' && $save_status) {
                    $portfolios = PortfolioDetails::where('investment_id', $investment_id)
                        ->where('portfolio_status',0);//Log::info(gettype($portfolios));
                    if($portfolios->count() == 0) { 
                        $investment_detail = InvestmentDetails::where('id',$investment_id)->first();
                        $investment_detail->investment_status = 1;
                        $investment_detail->investment_date = $current_date; 
                        $investment_detail->save();
                    }
                }

                //checking to send information regarding realisation
                if($save_status && ($notif_mode > 0)) {
                    //$transaction_type
                    if($notif_mode == 1) {
                        $this->sendNotificationEmail($user, $approved_amount, $transaction_type, $transaction_status_data);
                    } elseif($notif_mode == 2) {
                        $this->sendNotificationSms($user, $approved_amount, $transaction_type);
                        //if(!empty ($buffer)) {
                    }
                }

                //preparing response for approve withdraw and investment
                if($save_status) {
                    return response()->json(['msg' =>'success', 'status' => $save_message, 'date'=> date('d-m-Y',strtotime($current_date)), 'amount'=>$approved_amount]);
                } else {
                    return response()->json(['msg' =>'fail', 'status' => $save_message]);
                }
            
        }
    }

    public function portfolioDetails($id)
    {

        $user_id = $id;
        $portfolio_success = PortfolioDetails::where('user_id', $id)->where('portfolio_status', 1)->get();
        $investment_status = $this->getUserInvestmentPerformance($id);
        $portfolio_total = PortfolioDetails::where('user_id', $id)->where('portfolio_status', 1)->sum('amount_invested');
        $user_info = User::where('id',$id)->get();
        $user_info = $user_info[0];
        $userName = $user_info['name'];
        $userMobile = $user_info['mobile'];
        $userEmail = $user_info['email'];
        $pass_portfolio = array();
        $portfolio = array();
        $portfolio_code_name = array();

        $current_value_total = 0;
        $nr_total = 0;
        $ltcg = 0;
        $ltcg_total = 0;

        $dateArray = [];
        $amountArray = [];

        foreach ($portfolio_success as $portfolio) {


            $scheme_name = SchemeDetails::where('scheme_code',$portfolio->scheme_code)->pluck('scheme_name');

            $current_nav = NavDetails::where('scheme_code',$portfolio->scheme_code)->orderBy('date', 'desc')->first();//where('date',Carbon::now()->subDays(1)->toDateString())->pluck('nav_value');print_r(Carbon::now()->subDays(1)->toDateString());
            $current_value = (($portfolio->units_held) * $current_nav->nav_value);
            $current_value_total += $current_value;
            $current_value_total = round($current_value_total,2);
            $amount_invested = $portfolio->amount_invested;
            $net_returns = round(($current_value - $amount_invested),2);
            $nr_total += $net_returns;


            if ($portfolio->portfolio_type == "eq") {
                
                $invested_date = $portfolio->investment_date;
                $year_old = Carbon::now()->subDays(365)->toDateString();

                if ($invested_date < $year_old) {
                    $ltcg = $net_returns;
                    $ltcg_total += $ltcg;
                }
                else{
                    $ltcg = 0;
                    $ltcg_total += $ltcg;
                }

            }

            if ($portfolio->portfolio_type == "debt") {
                $invested_date = $portfolio->investment_date;

                $year_old = Carbon::now()->subDays(1095)->toDateString();

                if ($invested_date < $year_old) {
                    $ltcg = $net_returns;
                    $ltcg_total += $ltcg;
                }
                else{
                    $ltcg = 0;
                    $ltcg_total += $ltcg;
                }
            }

            $pass_portfolio[] = array(
                'id' => $portfolio->id,
                'scheme_code' => $portfolio->scheme_code,
                'fund_name' => $scheme_name[0],
                'folio_number' => $portfolio->folio_number,
                'type' => $portfolio->portfolio_type,
                'transaction_type' => 'Inv',
                'date'=> date('d-m-Y', strtotime($portfolio->bse_allot_date)),
                'amount'=> $portfolio->amount_invested,
                'inv_nav' => $portfolio->invested_nav,
                'units_held' => round($portfolio->units_held,4),
                'current_nav' => $current_nav->nav_value,
                'current_value' => round(($portfolio->units_held) * ($current_nav->nav_value),2),
                'net_returns' => $net_returns,
                'ltcg' => $ltcg,
            );

            if(!array_key_exists($portfolio->scheme_code, $portfolio_code_name)) {
                $portfolio_code_name[$portfolio->scheme_code] =  $scheme_name[0];
            }
        }


        $withdraw_details = WithdrawDetails::where('user_id', $id)->where('withdraw_status',1)->get();
        $scheme_code_name = SchemeDetails::pluck('scheme_name','scheme_code');
        $scheme_code_name = $scheme_code_name->toArray();
        //iterating withdraw deatils
        foreach($withdraw_details as $withdraw_detail) {
            $pass_portfolio[] = array(
                'id' => $withdraw_detail->id,
                'scheme_code' => $withdraw_detail->scheme_code,
                'fund_name' => $scheme_code_name[$withdraw_detail->scheme_code],
                'folio_number' => '',
                'transaction_type' => 'WD',
                'type' => $withdraw_detail->portfolio_type,
                'date'=> date('d-m-Y', strtotime($withdraw_detail->withdraw_date)),
                'amount'=> $withdraw_detail->withdraw_amount,
                'inv_nav' => $withdraw_detail->withdraw_nav,
                'units_held' => round($withdraw_detail->withdraw_units,4),
                'current_nav' => '',
                'current_value' => '',
                'net_returns' => '',
                'ltcg' => '',
            );

        }

        usort($pass_portfolio, function($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        });
        return view('admin.admin-portfolio-details')->with(compact('userName','userMobile','userEmail','pass_portfolio','portfolio_total','current_value_total','nr_total','ltcg_total','portfolio_code_name','id','user_sex','investment_status'));
    
    }


    public function updatePortfolioDetails(Request $request){

        $validator = \Validator::make($request->all(),[

            'p_id' => 'required',
            'date' => 'required',
            'inv_nav' => 'required',
            'folio_number' => 'required',
            'transaction_type' => 'required',
            ]);

        if ($validator->fails()) {
            
            return $validator->errors();
        }

        else{
            
            if($request['transaction_type'] == 'Inv') { 
                $portfolio_entry = PortfolioDetails::where('id',$request['p_id'])->first();
                $scheme_code = $portfolio_entry->scheme_code;
                $date = date('Y-m-d', strtotime($request['date']));
                $nav = $request['inv_nav'];
                //$originalDate = "2010-03-21";
                $date = date("Y-m-d", strtotime($date));//Log::info($date);

                /*$nav = DailyNav::where('scheme_code',$scheme_code)->where('date',$date)->pluck('nav_value');
                Log::info($nav);
                if(empty($nav)) {
                    $nav = HistoricNav::where('scheme_code',$scheme_code)->where('date',$date)->pluck('nav');
                }Log::info($nav);
                
                //var_dump($scheme_code);

                //var_dump($nav);
                $nav = current($nav);
                //echo $nav;*/

                $port_amount = $portfolio_entry->amount_invested;
                $units_held = ($port_amount/$nav);


                


                $update_portfolio = PortfolioDetails::where('id',$request['p_id'])->update(['bse_allot_date'=>$date,'invested_nav'=>$nav,'units_held'=>$units_held,'folio_number'=>$request['folio_number']]);

                if ($update_portfolio) {
                    $investment_details = InvestmentDetails::find($portfolio_entry->investment_id);//Log::info($investment_details);
                    $investment_details->investment_date = $date;
                    $investment_details->save();//Log::info($investment_details);
                    $response = array();
                    
                    $portfolio_res = PortfolioDetails::where('id',$request['p_id'])->get();
                    $ltcg = 0;

                            foreach ($portfolio_res as $portfolio) {

                                if ($portfolio->portfolio_type == "eq") {
                    
                                    $invested_date = $portfolio->investment_date;
                                    $year_old = Carbon::now()->subDays(365)->toDateString();

                                    if ($invested_date < $year_old) {
                                        $ltcg = $net_returns;
                                        
                                    }
                                    else{
                                        $ltcg = 0;
                                        
                                    }

                                }

                                if ($portfolio->portfolio_type == "debt") {
                                    $invested_date = $portfolio->investment_date;

                                    $year_old = Carbon::now()->subDays(1095)->toDateString();

                                    if ($invested_date < $year_old) {
                                        $ltcg = $net_returns;
                                        
                                    }
                                    else{
                                        $ltcg = 0;
                                        
                                    }
                                }

                                $scheme_name = SchemeDetails::where('scheme_code',$scheme_code)->pluck('scheme_name');
                                $scheme_name = $scheme_name[0];
                                $current_nav;
                                $current_nav_details = DailyNav::where('scheme_code',$portfolio->scheme_code)
                                    ->orderBy('date', 'asc')
                                    ->get();
                                if($current_nav_details != '') {
                                    $current_nav = $current_nav_details->first();
                                    $current_nav = $current_nav->nav_value;
                                }
                                $nav = $nav[0];

                                $current_val = (($portfolio->units_held) * $current_nav);
                                $net_return = round(($current_val - ($portfolio->amount_invested)),2);
                                
                                $response['scheme_code'] = $portfolio->scheme_code;
                                $response['scheme_name'] = $scheme_name;
                                $response['portfolio_type'] = $portfolio->portfolio_type;
                                $response['folio_number'] = $portfolio->folio_number;
                                $response['investment_date'] = $portfolio->investment_date;
                                
                                $response['amount_invested'] = $portfolio->amount_invested;
                                $response['invested_nav'] = $portfolio->invested_nav;
                                $response['units_held'] = round($portfolio->units_held,4);
                                $response['current_nav'] = $current_nav;
                                $response['current_val'] = round($current_val,2);
                                $response['net_return'] =  round($net_return,2);
                                $response['ltcg'] = $ltcg;
                                $response['id'] = $portfolio->id;
                                


                            }

                    //$response = $response->toArray();
                    //$response = json_encode($response);
                    //return response()->json(['msg'=> 'success','response'=>$response]);
                    return response()->json(['msg'=> '1','response' => $response]);

                }

                else{
                    return response()->json(['msg'=>'0']);
                }
            } elseif($request['transaction_type'] == 'WD') {
                $withdraw_id = $request['p_id'];
                $withdraw_date = $request['date'];

                $withdraw_detail = WithdrawDetails::find(intval($withdraw_id));
                if($withdraw_detail->withdraw_date != $withdraw_date) {
                    $portfolio_details = $withdraw_detail->User
                        ->PortfolioDetails
                        ->where('scheme_code',$withdraw_detail->scheme_code)
                        ->where('portfolio_status', 1);
                    $nav_value = DailyNav::where('date', $withdraw_date)
                        ->where('scheme_code', $withdraw_detail->scheme_code);
                    $withdraw_amount = $withdraw_detail->withdraw_amount;
                    $old_withdraw_units = $withdraw_detail->withdraw_units;
                    $new_withdraw_units = $withdraw_amount / $nav_value->nav_value;
                    $withdraw_detail->withdraw_units = $new_withdraw_units;
                    $withdraw_detail->withdraw_nav = $nav_value->nav_value;
                    $withdraw_detail->withdraw_date = $withdraw_date;
                    $withdraw_detail->save();
                    if($new_withdraw_units > $old_withdraw_units) {
                        $withdraw_units_diff = $new_withdraw_units - $old_withdraw_units;
                        $portfolio_detail = $portfolio_details->first();
                        $portfolio_detail->units_held += $withdraw_units_diff;
                        $portfolio_detail->save(); 
                    } elseif($new_withdraw_units < $old_withdraw_units) {
                        foreach($portfolio_details as $portfolio_detail) {
                            $withdraw_units_diff = $old_withdraw_units - $new_withdraw_units;
                            //check if the withdraw units is greater than 0
                            if($withdraw_units_diff > 0) {
                                //if the units held and withdraw units are same
                                if($portfolio_detail->units_held == $withdraw_units_diff) {
                                    $withdraw_units_diff = 0;
                                    $portfolio_detail->units_held = 0;
                                    $portfolio_detail->amount_invested = 0;
                                    $portfolio_detail->portfolio_status = 2;
                                    $portfolio_detail->save();
                                } elseif($portfolio_detail->units_held < $withdraw_units_diff) {
                                    //if the units held less than  withdraw units
                                    $withdraw_units_diff -= $portfolio_detail->units_held;
                                    $portfolio_detail->units_held = 0;
                                    $portfolio_detail->amount_invested = 0;
                                    $portfolio_detail->portfolio_status = 2;
                                    $portfolio_detail->save();
                                } elseif($portfolio_detail->units_held > $withdraw_units) {
                                    //if the units held is greater than the units to be withdrawn
                                    $portfolio_detail->units_held -= $withdraw_units_diff;
                                    $withdraw_units_diff = 0;
                                    $portfolio_detail->save();
                                }
                            }
                        }
                    }
                }

                $response['scheme_code'] = $withdraw_detail->scheme_code;
                $response['scheme_name'] = $scheme_name;
                $response['portfolio_type'] = $withdraw_detail->portfolio_type;
                $response['folio_number'] = '';
                $response['investment_date'] = $withdraw_detail->withdraw_date;
                $response['amount_invested'] = $withdraw_detail->withdraw_amount;
                $response['invested_nav'] = $withdraw_detail->withdraw_invested;
                $response['units_held'] = $withdraw_detail->withdraw_units;
                $response['current_nav'] = '';
                $response['current_val'] = '';
                $response['net_return'] =  '';
                $response['ltcg'] = '';

                return response()->json(['msg'=> '1','response' => $response]);
            }
        }
    }


    public function taxSaving($id){
        return view('admin.admin-tax-saving')->with(['id'=>$id]);
    }





    public function pendingDetails($type, $id)
    {
        /* Portfolio status
            0 => 'Pending'
            1 => 'Rialised'
            2 => 'Withdraw'
            3 => 'cancelled'
        */
        $portfolios = array();
        $scheme_types = array('eq'=>'Equity','bal'=>'Balanced','debt'=>'Debt', 'ts'=>'ELSS');
        $scheme_name_code = SchemeDetails::all()->pluck('scheme_name','scheme_code');
        if($type == 'investment') {
            $investment_details = InvestmentDetails::find($id);
            $user_details = $investment_details->User;
            $personal_details = $user_details->PersonalDetails;
            $amount = 0;
            $portfolio_details = $investment_details->PortfolioDetails->whereIn('portfolio_status', [0,1,2,3]);
            $portfolios['portfolio_type'] = $this->portfolio_types[$investment_details->portfolio_type];
            $portfolios['investment_type'] = $this->investment_types[$investment_details->investment_type];
            $portfolios['transaction_type'] = 'Investment';
            $portfolios['date'] = date('d-m-Y',strtotime($investment_details->investment_date));
            $portfolios['name'] = $user_details->name;
            $portfolios['mobile'] = $user_details->mobile;
            $portfolios['email'] = $user_details->email;
            $portfolios['profile_image'] = $personal_details->profile_img;

            foreach ($portfolio_details as $portfolio_detail) {
                $portfolios['portfolio_details'][$portfolio_detail->portfolio_type][] = array(
                    'scheme_code' => $portfolio_detail->scheme_code,
                    'portfolio_id' => $portfolio_detail->id,
                    'scheme_name' => $scheme_name_code[$portfolio_detail->scheme_code],
                    'amount' => $portfolio_detail->amount_invested,
                    'portfolio_status' => $portfolio_detail->portfolio_status,
                );
                $amount += $portfolio_detail->amount_invested;
            }
            $portfolios['amount'] = $amount;
        } elseif($type == 'withdraw') {
            $withdraw_details = WithdrawDetails::where('withdraw_group_id',intval($id))
                ->where('withdraw_status',0)
                ->get();//print_r($withdraw_details);
            if($withdraw_details->count() > 0) {
                $withdraw = $withdraw_details->first();
                $user_details = $withdraw->User;
                $personal_details = $user_details->PersonalDetails;
                $portfolios['portfolio_type'] = '';
                $portfolios['investment_type'] = '';
                $portfolios['transaction_type'] = 'Withdraw';
                $portfolios['date'] = date('d-m-Y', strtotime($withdraw->created_at));
                $portfolios['amount'] = $withdraw_details->sum('withdraw_amount');
                $portfolios['name'] = $user_details->name;
                $portfolios['mobile'] = $user_details->mobile;
                $portfolios['email'] = $user_details->email;
                $portfolios['profile_image'] = $personal_details->profile_img;

                foreach ($withdraw_details as $withdraw_detail) {
                    $portfolios['portfolio_details'][$withdraw_detail->portfolio_type][] = array(
                        'scheme_code' => $withdraw_detail->scheme_code,
                        'portfolio_id' => $withdraw_detail->id,
                        'scheme_name' => $scheme_name_code[$withdraw_detail->scheme_code],
                        'amount' => $withdraw_detail->withdraw_amount,
                        'portfolio_status' => $withdraw_detail->withdraw_status,
                    );
                }
            } else {
                //return view('admin.admin-pending-orders');
                //return redirect()->action('UserController@profile', ['id' => 1]);
            }
        }
        return view('admin.pending-details')->with('pending_order_details',$portfolios);
    }

    //function to show investment history
    public function investmentHistory($id){

        $user_detail = User::find($id);

            $investment_summary = array();//Log::info($range_date);
            $sum = [];
            $investment_details = $user_detail->investmentDetails()
                ->orderBy('investment_date', 'asc')
                ->get();

             foreach ($investment_details as $investment_detail) {
                
            
                $portfolio_details = $investment_detail->portfolioDetails
                    ->whereIn('portfolio_status', [0,1])
                    ->groupBy('portfolio_type')
                    ->all();//Log::info($portfolio_type);


                foreach ($portfolio_details as $inv_id => $portfolio_detail) {//Log::info($inv_id);

                    // dd($portfolio_detail->sum('amount_invested'));
                    // echo $portfolio_detail->sum('amount_invested');
                        $investment_summary[$inv_id][] = array(
                            'inv_amount' => $portfolio_detail->sum('amount_invested'),
                            'date' => date('d-m-Y', strtotime($investment_detail->investment_date)),
                            'investment_type' => $this->investment_types[$investment_detail->investment_type],
                            'investment_status' => $this->investment_status[$investment_detail->investment_status],
                            'portfolio_type' => $inv_id,
                        );
                        // $count += $portfolio_detail->sum('amount_invested'); 

                    }
                    // $sum[$inv_id] = $count;
                }
        // dd($investment_summary);


        $user_details = User::where('id',$id)->get();
        $investment_status = $this->getUserInvestmentPerformance($id);
        $user_info = $user_details[0];
        $userName = $user_info['name'];
        $userMobile = $user_info['mobile'];
        $userEmail = $user_info['email'];
        // dd($eq_inv);
        // dd($user_info);
        return view('admin.admin-inv-history')->with(compact('userName','userMobile','userEmail','investment_summary','investment_status','id'));
        // return view('admin.admin-inv-history');
    }







    public function userSearch(Request $request){

        $validator = \Validator::make($request->all(),[
                'search-input' => 'required',
            ]);
//Log::info($request['search-input']);
        if ($validator->fails()) {
            return $validator->$errors();
        }

        else{
            $user_name = $request['search-input'];
            
            $users = User::where('name','LIKE','%'.$user_name.'%')
                ->orwhere('mobile','LIKE',$user_name.'%')
                ->orwhere('email','LIKE',$user_name.'%')
                ->orwhere('pan','LIKE',$user_name.'%')
                ->distinct()
                ->limit(10)
                ->get();//Log::info(get_class($users));
                if (\Auth::id() == 145) {
                    $users = $users->where('role', 0)->where('handle_by',0);
                    // $nach_status = nachDetails::where('priority',null)->pluck('user_id')->toArray();
                    // $final = [];
                    // foreach ($users as $value) {
                    //     if (in_array($value['id'], $nach_status)) {
                    //         array_push($final, $value);
                    //     }
                    // }

                    return json_encode($users);
                }
                $users = $users->where('p_check','1')->where('role', 0);
            /*foreach($users as $user) {
                $user_suggestions[$user->name.'/'.$user->mobile.'/'.$user->email] = $user->name.'/'.$user->mobile.'/'.$user->email;
            }*///Log::info($users);
            return json_encode($users);
        }
    }

    public function findNach(Request $request)
    {
            $validator = \Validator::make($request->all(),[
                'search_nach_user' => 'required',
            ]);

            if ($validator->fails()) {
                return $validator->$errors();
            }else{
                $user_name = $request['search_nach_user'];
            
                $users = User::where('name','LIKE',$user_name.'%')
                    ->orwhere('mobile','LIKE',$user_name.'%')
                    ->orwhere('email','LIKE',$user_name.'%')
                    ->orwhere('pan','LIKE',$user_name.'%')
                    ->distinct()
                    ->limit(10)
                    ->get();
                $users = $users->where('role', 0);
                $nach_status = nachDetails::where('nach_status',0)->pluck('user_id')->toArray();
                $final = [];
                foreach ($users as $value) {
                    if (in_array($value['id'], $nach_status)) {
                        array_push($final, $value);
                    }
                }

                return json_encode($final);
            }
        
    }


    public function showUserKyc(Request $request){

        $validator = \Validator::make($request->all(),[
                'user_id' => 'required',
            ]);

        if ($validator->fails()) {
            return $validator->$errors();
        }

        else{
            $user_id = $request['user_id'];
            $response = array();

            //Name and mobile number from Users table.

            $user_details = User::where('id',$user_id)->get();
           

            foreach ($user_details as $user) {
                $response['user_name'] = $user['name'];
                $response['user_mob'] = $user['mobile'];
                $response['user_email'] = $user['email'];
                //$response['user_pan'] = $user['pan'];
                $response['kyc'] = $user['kyc_check'];
                $response['user_id'] = $user['id'];
                $response['created_at'] = $user['created_at'];
            }

            //Occupation,DOB,address,Pin Code and Nationality from Personal Details table.
            $personal_details = PersonalDetails::where('user_id',$user_id)->get();

            $per_response = array();

            foreach ($personal_details as $p_details) {
                $sex = $p_details['sex'];
                $profile_image = 'male.png';
                if($p_details['profile_img'] != '') {
                    $profile_image = $p_details['profile_img'];
                } else {
                    if($sex == 'male'){
                        $profile_image = 'male.png';
                    } elseif($sex == 'female'){
                        $profile_image = 'female.png';
                    }
                }
                $response['occupation'] = $p_details['occ_type'];
                $response['dob'] = date('d-m-Y', strtotime($p_details['dob']));
                $response['user_address'] = $p_details['address'];
                $response['user_pin'] = $p_details['pincode'];
                $response['nationality'] = $p_details['nationality'];
                $response['sex'] = $p_details['sex'];
                $response['mar_status'] = $p_details['mar_status'];
                $response['profile_img'] = $profile_image;
                $response['mothers_name'] = $p_details['mothers_name'];
                $response['fs_name'] = $p_details['f_s_name'];
                $response['user_pan'] = $p_details['pan'];
                $response['aadhar'] = $p_details['aadhar'];
                $response['b_city'] = $p_details['b_city'];

            }

            //print_r($personal_details);

            //Nominee Details from Nominee Table.
            $nominee_details1 = NomineeDetails::where('user_id',$user_id)->get();
                $count = 0;

                foreach ($nominee_details1 as $nd1) {
                    $count++;
                    $response['nom'.$count.'_name'] = $nd1['nomi_name'];
                    if($nd1['nomi_dob'] != NULL) {
                        $response['nom'.$count.'_dob'] = date('d-m-Y', strtotime($nd1['nomi_dob']));
                    } else {
                        $response['nom'.$count.'_dob'] = '';
                    }
                    $response['nom'.$count.'_relationship'] = $nd1['nomi_relationship'];
                    $response['nom'.$count.'_holding']= $nd1['nomi_holding'];
                }

        
            //Getting Bank Details from bank details table

            $bank_details = BankDetails::where('user_id',$user_id)->get();
            $bank_id = '';
            foreach ($bank_details as $b_details) {
                $response['acc_no'] = $b_details['acc_no'];
                $response['ifsc_code'] = $b_details['ifsc_code'];
                $response['bank_name'] = $b_details['bank_name'];
                $response['branch_name'] = $b_details['branch_name'];
                $response['bank_addr'] = $b_details['address'];
                $response['bank_city'] = $b_details['city'];
                $response['bank_type'] = $b_details['bank_type'];
                $response['bank_id'] = $b_details['bank_id'];
                $response['acc_type'] = $b_details['acc_type'];
                $bank_id = $b_details['bank_id'];
            }

            $ekyc_details = ekyc::where('user_id',$user_id)->get();

            foreach ($ekyc_details as $ekyc) {
                $response['mandate_link'] = $ekyc['mandate_link'];
                $response['cc_link'] = $ekyc['cc_link'];
                $response['video_link'] = $ekyc['video_link'];
                $response['pan_link'] = $ekyc['pan_link'];
                $response['address_link'] = $ekyc['address_link'];
                $response['ekycform_link'] = $ekyc['ekyc_form_link'];
                $response['aof_link'] = $ekyc['aof_link'];      
                $response['apf_link'] = $ekyc['apf_link'];
                $response['apb_link'] = $ekyc['apb_link'];
                $response['sig_link'] = $ekyc['sig_link'];
                $response['mandate_hc_link'] = $ekyc['mandate_hc_link'];
            }

            $user_investment_details = $this->getUserInvestmentPerformance($user_id);

            $response['amount_invested'] = $user_investment_details['amount_invested'];
            $response['current_value'] = $user_investment_details['current_value'];
            $response['profit_loss_amount'] = $user_investment_details['profit_loss_amount'];
            $response['xirr_return'] = $user_investment_details['xirr'];
            $response['day_change'] = $user_investment_details['day_change'];
            $bank_info = BankInfo::all()->toArray();
            usort($bank_info, function($a, $b){ return strcmp($a['bank_name'], $b['bank_name']); });

            return response()->json(['msg'=> 'success','response'=>$response,'bank_info'=>$bank_info]);
        }
    }

    //function to save nach status
    public function saveNach(Request $request)
    {
        $nach_details = $request['nachdetails'];
        $nach_save_status = array();Log::info($nach_details);
        foreach($nach_details as $user_id => $nach_detail) {
            $nach_status = 1;
            $user = User::find($user_id);
            $user_nach = $user->NachDetails;
            
            if(array_key_exists('pan_completed', $nach_detail)) {
                if( ($user_nach->pan_date == '' || $user_nach->pan_date == NULL) && $nach_detail['pan_completed'] != '') {
                    $user_nach->pan_date = date('Y-m-d',strtotime($nach_detail['pan_completed']));
                }/* else {
                    $user_nach->kyc_date = NULL;
                }*/
            }

            if(array_key_exists('kyc_completed', $nach_detail)) {
                if( ($user_nach->kyc_date == '' || $user_nach->kyc_date == NULL) && $nach_detail['kyc_completed'] != '') {
                    $user_nach->kyc_date = date('Y-m-d',strtotime($nach_detail['kyc_completed']));
                }/* else {
                    $user_nach->kyc_date = NULL;
                }*/
            }

            if(array_key_exists('mandate_sent', $nach_detail)) {
                if( ($user_nach->mandate_sent_date == '' || $user_nach->mandate_sent_date == NULL) && $nach_detail['mandate_sent'] != '') {
                    $user_nach->mandate_sent_date = date('Y-m-d',strtotime($nach_detail['mandate_sent']));
                }/* else {
                    $user_nach->mandate_sent_date = NULL;
                }*/
            }

            if(array_key_exists('mandate_rec', $nach_detail)) {
                if( ($user_nach->mandate_received == '' || $user_nach->mandate_received == NULL) && $nach_detail['mandate_rec'] != '') {
                    $user_nach->mandate_received = date('Y-m-d',strtotime($nach_detail['mandate_rec']));
                }/* else {
                    $user_nach->mandate_received =NULL;
                }*/
            }

            if(array_key_exists('tpsl_send', $nach_detail)) {
                if( ($user_nach->tpsl_date == '' || $user_nach->tpsl_date == NULL) && $nach_detail['tpsl_send'] != '') {
                    $user_nach->tpsl_date = date('Y-m-d',strtotime($nach_detail['tpsl_send']));
                }/* else {
                    $user_nach->tpsl_date = NULL;
                }*/
            }

            if(array_key_exists('cleared', $nach_detail)) {
                if( ($user_nach->cleared_date == '' || $user_nach->cleared_date == NULL) && $nach_detail['cleared'] != '') {
                    $user_nach->cleared_date = date('Y-m-d',strtotime($nach_detail['cleared']));
                }/* else {
                    $user_nach->cleared_date = NULL;
                }*/
            }

            if(array_key_exists('disapp', $nach_detail)) {
                if( ($user_nach->disapproved_date == '' || $user_nach->disapproved_date == NULL) && $nach_detail['disapp'] != '') {
                    $user_nach->disapproved_date = date('Y-m-d',strtotime($nach_detail['disapp']));
                }/* else {
                    $user_nach->disapproved_date = NULL;
                }*/
            }
            
            foreach ($nach_detail as $key => $value) {
                if($value == '') {
                    $nach_status = 0;
                }
            }
            $user_nach->nach_status = $nach_status;
            if($user->nachDetails()->save($user_nach)) {
                $nach_save_status[] = 'success';//print_r('user details updated successfully');
            } else {
                $nach_save_status[] = 'failed';//print_r('user details update unsuccessful');
            }
        }Log::info($nach_save_status);
        if(in_array('success', $nach_save_status)) {
            return response()->json(['msg' => 'success', 'status' => 'Nach updated successfully']);
        } else {
            return response()->json(['msg' => 'fail', 'status' => 'Nach update failed']);
        }
    }


    //function to save pending order details
    public function savePendingOrders(Request $request)
    {
        $pending_portfolios = $request['portfolio_details'];
        $transaction_type = $request['transaction_type'];
        $transaction_status_data = array();
        $nav_date = new Carbon();
        $current_date = $nav_date->format('Y-m-d');
        //$nav_date->subDay();
        $nav_date = $nav_date->format('Y-m-d');
        $investment_id = '';
        $save_status = false;
        $approved_amount = 0;
        $save_message = '';Log::info($pending_portfolios);
        $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
        //dd($pending_portfolios);

        $scheme_code_name = $scheme_details->toArray();
        $notif_mode = 0;
        $user;
        
        //Pending Portfolios ids are stored in $pending_portfolios. Looping through it and getting 
        foreach($pending_portfolios as $key => $pending_portfolio) {
            //dd($pending_portfolio);
            $id = intval($pending_portfolio['id']);
            //dd($id);
            if($transaction_type == 'Investment') {
                $portfolio_detail = PortfolioDetails::where('id',$id)
                    //->where('portfolio_status',0)
                    ->first();
                if($portfolio_detail != NULL) {
                    if(($portfolio_detail->portfolio_status == 0)  && (array_key_exists('checked', $pending_portfolio))){
                        $investment_id = intval($portfolio_detail->investment_id);//Log::info('Portfolio id'.$portfolio_detail->id);
                        $amount_invested = $portfolio_detail->amount_invested;
                        $scheme_nav = DailyNav::where('date', '<', $nav_date)
                        ->where('scheme_code', $portfolio_detail->scheme_code)
                        ->orderBy('date', 'desc')
                        ->first();//Log::info($nav_date);Log::info($scheme_nav);die();Log::info('get me if you can');

                        $units_held = $amount_invested / $scheme_nav->nav_value;
                        $portfolio_detail->invested_nav = $scheme_nav->nav_value;
                        $portfolio_detail->units_held = $units_held;
                        $portfolio_detail->initial_units_held = $units_held;
                        $portfolio_detail->investment_date = $current_date;
                        $portfolio_detail->portfolio_status = 1;
                        $portfolio_detail->save();
                        $transaction_status_data[] = array(
                            'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                            'scheme_amount'=> $amount_invested,
                            'status' => $portfolio_detail->portfolio_status,
                        );
                        $user = $portfolio_detail->User;
//Log::info($transaction_status_data);
                        $save_status = true;
                        $save_message = "You have successfully approved an investment";
                        $approved_amount += $amount_invested;
                        $notif_mode = $portfolio_detail->investmentDetails->realise_response;
                        
                        /*if($notif_mode == 1) {
                            $user = $portfolio_detail->User;
                            Mail::send('admin.investment_realise', array('user_name' => $user->name, 'investment_date' => date('d-m-Y', strtotime($current_date)), 'investment_type' => 'Investment', 'amount_invested' => $approved_amount, 'investment_details' => $transaction_status_data), function ($message) use ($user) {
                                $message->from('test@rightfundspartners.com', 'Investment Summary');
                                $message->to($user->email, $user->name);
                                //$message->attach('profile_pic.jpg');
                            });
                        } elseif($notif_mode == 2) {
                            $ch = curl_init();
                            $user="naveen@rightfunds.com:Rightfunds20";
                            $receipientno = ;
                            $senderID="RFUNDS";
                            $msgtxt="Your Investment of Rs. ".$approved_amount." approved successfully"; 
                            curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                            $buffer = curl_exec($ch);
                            curl_close($ch);
                            //if(!empty ($buffer)) {
                        }*/
                    }
                } else {
                    $save_status = false;
                    $save_message = "Investment approve failed. Please check and try again";
                }
                
            } 
            elseif($transaction_type == 'Withdraw') {
                $withdraw_detail = WithdrawDetails::where('id',$id)
                    //->where('withdraw_status')
                    ->first();

                if(($withdraw_detail != NULL) && (array_key_exists('checked', $pending_portfolio))) {
                    
                    $current_value = 0; //current value of the portfolio
                    $scheme_code = $withdraw_detail->scheme_code; //scheme code of the portfolio to be processed
                    $withdraw_amount = $withdraw_detail->withdraw_amount; //amount to be withdrawn

                    //get the nav details related to the current processing withdraw id
                    $scheme_nav = DailyNav::where('date', $nav_date)
                        ->where('scheme_code', $scheme_code)
                        ->first();
                    
                    //get the user related to the withdraw
                    $user = $withdraw_detail->User;
                    
                    //get the portfolio's detail related to the withdraw
                    $portfolio_details = $user->portfolioDetails()
                        ->where('scheme_code', $scheme_code)
                        ->where('portfolio_status', 1)
                        ->get();
                    $current_value = $portfolio_details->sum('units_held') * $scheme_nav->nav_value;
                    if($current_value > $withdraw_amount) {
                        $withdraw_units = $withdraw_amount / $scheme_nav->nav_value; //calculating the units to be withdrawn
                        $withdraw_detail->withdraw_units = $withdraw_units;
                        $withdraw_detail->withdraw_nav = $scheme_nav->nav_value;
                        $withdraw_detail->withdraw_date = date('Y-m-d');
                        $withdraw_detail->withdraw_status = 1;
                        $portfolio_save_status;
                        //loop through all active portfolio details of the scheme to reduce the withdrawn units
                        foreach($portfolio_details as $portfolio_detail) {
                            $user = $portfolio_detail->User;
                            //check if the withdraw units is greater than 0
                            if($withdraw_units > 0) {
                                //if the units held and withdraw units are same
                                if($portfolio_detail->units_held == $withdraw_units) {
                                    $withdraw_units = 0;
                                    $portfolio_detail->units_held = 0;
                                    $portfolio_detail->amount_invested = 0;
                                    $portfolio_detail->portfolio_status = 2;
                                    $portfolio_save_status = $portfolio_detail->save();
                                } elseif($portfolio_detail->units_held < $withdraw_units) {
                                    //if the units held less than  withdraw units
                                    $withdraw_units -= $portfolio_detail->units_held;
                                    $portfolio_detail->units_held = 0;
                                    $portfolio_detail->amount_invested = 0;
                                    $portfolio_detail->portfolio_status = 2;
                                    $portfolio_save_status = $portfolio_detail->save();
                                } elseif($portfolio_detail->units_held > $withdraw_units) {
                                    //if the units held is greater than the units to be withdrawn
                                    $portfolio_detail->units_held -= $withdraw_units;
                                    $portfolio_detail->amount_invested -= ($withdraw_units * $scheme_nav->nav_value);
                                    $withdraw_units = 0;
                                    $portfolio_save_status = $portfolio_detail->save();
                                }
                            }
                        }
                        if($portfolio_save_status) {
                            if($withdraw_detail->save()) {
                                $save_status = true;
                                $save_message = "You have scuccessfully approved an withdraw";
                                $approved_amount += $withdraw_amount;
                                $transaction_status_data[] = array(
                                    'scheme_name' => $scheme_code_name[$scheme_code],
                                    'scheme_amount'=> $withdraw_amount,
                                    'status' => 1,
                                );
                            } else {
                                $save_status = false;
                                $save_message = "Withdraw approve failed. Please check and try again";
                            }
                        } else {
                            $save_status = false;
                            $save_message = "Withdraw approve failed. Please check and try again";
                        }
                    } else {
                        $save_status = false;
                        $save_message = "Withdraw approve failed. Please check and try again";
                    }
                }
            }//else if of transaction type of withdraw ends
        }
        if($investment_id !='' && $save_status) {
            $portfolios = PortfolioDetails::where('investment_id', $investment_id)
                ->where('portfolio_status',0);//Log::info(gettype($portfolios));
            if($portfolios->count() == 0) { 
                $investment_detail = InvestmentDetails::where('id',$investment_id)->first();
                $investment_detail->investment_status = 1;
                $investment_detail->investment_date = $current_date; 
                $investment_detail->save();
            }
        }

        //checking to send information regarding realisation
        if($save_status && ($notif_mode > 0)) {
            //$transaction_type
            if($notif_mode == 1) {
                $this->sendNotificationEmail($user, $approved_amount, $transaction_type, $transaction_status_data);
            } elseif($notif_mode == 2) {
                $this->sendNotificationSms($user, $approved_amount, $transaction_type);
                //if(!empty ($buffer)) {
            }
        }

        //preparing response for approve withdraw and investment
        if($save_status) {
            return response()->json(['msg' =>'success', 'status' => $save_message, 'date'=> date('d-m-Y',strtotime($current_date)), 'amount'=>$approved_amount]);
        } else {
            return response()->json(['msg' =>'fail', 'status' => $save_message]);
        }
    }

    //function to send email for investment or withdraw during success
    public function sendNotificationEmail($user, $approved_amount, $transaction_type, $transaction_status_data)
    {
        $current_date = Carbon::now()->toDateString();
        Mail::send('admin.investment_realise', array('user_name' => $user->name, 'investment_date' => date('d-m-Y', strtotime($current_date)), 'investment_type' => $transaction_type, 'amount_invested' => $approved_amount, 'investment_details' => $transaction_status_data), function ($message) use ($user, $transaction_type) {
            $message->from('test@rightfundspartners.com', 'Rightfunds');
            $message->subject($transaction_type.' Summary');
            $message->to($user->email, $user->name);
        });
    }

    //function to send sms for investment or withdraw during success
    public function sendNotificationSms($user_details, $approved_amount, $transaction_type)
    {
        $ch = curl_init();
        $user="naveen@rightfunds.com:Rightfunds20";
        $receipientno = $user_details->mobile;
        $senderID="RFUNDS";
        $msgtxt="Dear ".$user_details->name." , Your investment has been realised. Rightfunds Invest Better";//Your Investment of Rs. ".$approved_amount." approved successfully"; 
        curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
        $buffer = curl_exec($ch);Log::info($buffer); Log::info($msgtxt);
        curl_close($ch);
    }
    //function for manual withdrawl
    public function manualWithdraw(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'withdraw_scheme_code' => 'required',
            'withdraw_date' => 'required',
            'withdraw_amount' => 'required',
            'withdraw_nav' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors());
        } else {
            $scheme_details = SchemeDetails::get()->pluck('scheme_type','scheme_code');
            $scheme_details = $scheme_details->toArray();   //scheme type and scheme code
            $withdraw_group_id = WithdrawDetails::max('withdraw_group_id'); //withdraw group id
            $withdraw_group_id++;
            $scheme_code = $request['withdraw_scheme_code']; //scheme code of the portfolio to be processed
            $date = $request['withdraw_date'];  //withdraw date
            $amount = $request['withdraw_amount'];  //withdraw amount
            $nav = $request['withdraw_nav'];    //withdraw NAV
            $user_id = $request['user_id']; //withdraw user id
            $withdraw_units = $amount / $nav; //calculating the units to be withdrawn
            $withdraw_details = new WithdrawDetails();
            $withdraw_details->scheme_code = $scheme_code;
            $withdraw_details->user_id = $user_id;
            $withdraw_details->withdraw_amount = $amount;
            $withdraw_details->withdraw_nav = $nav;
            $withdraw_details->withdraw_units = $withdraw_units;
            $withdraw_details->withdraw_date = Date('Y-m-d', strtotime($date));
            $withdraw_details->portfolio_type = $scheme_details[$scheme_code];
            $withdraw_details->withdraw_group_id = $withdraw_group_id;
            $withdraw_details->withdraw_status = 1;
            if(($withdraw_details->save())) {
                
                $current_value = 0; //current value of the portfolio
                $withdraw_amount = $amount; //amount to be withdrawn

                //get the nav details related to the current processing withdraw id
                /*$scheme_nav = NavDetails::where('date', $nav_date)
                    ->where('scheme_code', $scheme_code)
                    ->first();*/
                
                //get the user related to the withdraw
                $user = User::find($user_id);
                
                //get the portfolio's detail related to the withdraw
                $portfolio_details = $user->portfolioDetails()
                    ->where('scheme_code', $scheme_code)
                    ->where('portfolio_status', 1)
                    ->get();
                $current_value = $portfolio_details->sum('units_held') * $nav;
                if($current_value > $withdraw_amount) {

                    //loop through all active portfolio details of the scheme to reduce the withdrawn units
                    foreach($portfolio_details as $portfolio_detail) {
                        //check if the withdraw units is greater than 0
                        if($withdraw_units > 0) {
                            //if the units held and withdraw units are same
                            if($portfolio_detail->units_held == $withdraw_units) {
                                $withdraw_units = 0;
                                $portfolio_detail->units_held = 0;
                                $portfolio_detail->amount_invested = 0;
                                $portfolio_detail->portfolio_status = 2;
                                $portfolio_detail->save();
                            } elseif($portfolio_detail->units_held < $withdraw_units) {
                                //if the units held less than  withdraw units
                                $withdraw_units -= $portfolio_detail->units_held;
                                $portfolio_detail->units_held = 0;
                                $portfolio_detail->amount_invested = 0;
                                $portfolio_detail->portfolio_status = 2;
                                $portfolio_detail->save();
                            } elseif($portfolio_detail->units_held > $withdraw_units) {
                                //if the units held is greater than the units to be withdrawn
                                $portfolio_detail->units_held -= $withdraw_units;
                                $portfolio_detail->amount_invested -= ($withdraw_units * $nav);
                                $withdraw_units = 0;
                                $portfolio_detail->save();
                            }
                        }
                    }
                    return response()->json(['msg'=>'success', 'status'=>'Manual Withdraw done successfully']);
                } else {
                    return response()->json(['msg'=>'failure', 'status'=>'Manual Withdraw failed. Please try again']);
                }
            } else {
                return response()->json(['msg'=>'failure', 'status'=>'Manual Withdraw failed. Please try again']);
            }
        }
    }

    //function to handle order history request
    public function orderHistory()
    {
        $order_history = $this->get_order_history('data');//print_r($order_history);
        usort($order_history, function($a, $b) {
            return strtotime($b['date']) - strtotime($a['date']);
        });
        return view('admin.order-history')->with('order_history',$order_history);
    }


    public function newOrderHistory()
    {
        $users = User::all();

        $investment_history = array();
        $withdraw_history = array();
        $investment_history_collection  = array();
        $withdraw_history_collection = array();

        foreach ($users as $user) {
            $investment_details = $user->investmentDetails;
            //dd($investment_details);

            $user_investments = $user->investmentDetails()
                ->whereIn('investment_status', [0,1,2,3]) //to get all the investment entry
                //->where('investment_status','1') //
                ->orderBy('investment_date', 'asc')
                ->get();

            foreach ($user_investments as $user_investment) {

                $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1,2,3])
                    ->get();

                if($user_investment->investment_date != NULL && $user_investment->investment_date != '') {
                                $investment_date = date('d-m-Y',strtotime($user_investment->investment_date));
                                $order_placed_date = date('d-m-Y',strtotime($user_investment->created_at));

                            }

                $investment_history[] = array(
                    'investment_id' => $user_investment->id,
                    'user_name' => $user->name,
                    'user_pan' => $user->personalDetails->pan,
                    'order_type' => 'Investment',
                    'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                    'investment_type' => $this->investment_types[$user_investment->investment_type],
                    'execution_date' => $investment_date,
                    'placed_date' => $order_placed_date,
                    'amount' => round($user_portfolios->sum('amount_invested'),2),

                    );
                
            }

            /*foreach ($investment_history as $investments) {
                dd($investments['user_name']);

                
            }*/


            $user_withdraws = $user->WithdrawDetails
                    ->whereIn('withdraw_status', [1])
                    ->groupBy('withdraw_group_id');

            /*$user_withdraws = $user->WithdrawDetails
                    ->where('withdraw_status', '1')
                    ->groupBy('withdraw_group_id');
*/

            //dd(count($user_withdraws));
    
            
            
            if($user_withdraws->count() > 0) {
                foreach($user_withdraws as $user_withdraw) {
                    $withdraw_data = $user_withdraw->first();

                    if($withdraw_data->withdraw_date != NULL && $withdraw_data->withdraw_date != '') {
                                $withdraw_date = date('d-m-Y',strtotime($withdraw_data->withdraw_date));
                                $order_placed_date = date('d-m-Y',strtotime($withdraw_data->created_at));

                    }
                    
                    
                        $withdraw_history[] = array(
                            'id' => $withdraw_data->withdraw_group_id,
                            'withdraw_id' => $withdraw_data->id,
                            'user_name' => $user->name,
                            'user_pan' => $user->personalDetails->pan,
                            'portfolio_type' => $withdraw_data->portfolio_type,
                            'investment_type' => '',
                            'order_type' => 'Withdraw',
                            'date' => date('d-m-Y',strtotime($withdraw_data->withdraw_date)),
                            'amount' => $user_withdraw->sum('withdraw_amount'),
                            'execution_date' => $withdraw_date,
                            'placed_date' => $order_placed_date,
                        );
                     
                }
            }//$user_withdraws->count() ends

            
        }

        //dd($withdraw_history);

        $investment_details = $investment_history;
        $withdraw_details = $withdraw_history;



        foreach ($investment_details as $investment_detail) {
                //dd($investment_detail['investment_id']);
                $investment_history_collection[] = PortfolioDetails::whereIn('portfolio_status',[0,1,2,3])->where('investment_id',$investment_detail['investment_id'])->get();                      
        }


        //The below code is to loop through pending Withdraws and store it in the variable $pending_withdraws_collection
        foreach ($withdraw_details as $withdraw_detail) {


                //echo $withdraw_detail['withdraw_group_id']."<br>";
                $withdraw_history_collection[] = WithdrawDetails::whereIn('withdraw_status',[0,1,2])->where('withdraw_group_id',$withdraw_detail['id'])->get();                      
        }

        //dd($withdraw_history_collection);

        foreach ($investment_history_collection as $pending_orders) {

                foreach ($pending_orders as $pending_order) {
                    # code...
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name');
                    $pending_order['scheme_name'] = $scheme_name[0];

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];
                    $pending_order['amount_invested'] = round($user_folio['amount_invested'],2);

                }
        }


        foreach ($withdraw_history_collection as $pending_orders) {

                foreach ($pending_orders as $pending_order) {
                    # code...
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name');
                    $pending_order['scheme_name'] = $scheme_name[0];

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];

                }
        }

        //dd($investment_history_collection,$withdraw_history_collection);
        //dd($investment_history);
        //dd($withdraw_history);
        //$order_history = $this->get_order_history('data');//print_r($order_history);
        return view('admin.new-order-history')->with(compact('investment_history','withdraw_history','investment_history_collection','withdraw_history_collection'));
    }



    public function revokeOrders(Request $request){

        $validator = \Validator::make($request->all(),[
            'portfolio_ids' => 'required',
            'order_type' => 'required',
            'order_id' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors());
        } else {

            if ($request['order_type']=='inv') {

                $portfolio_ids = preg_split('/,/', $request['portfolio_ids']);
                // $portfolio_orders = array();


                if ($request['portfolio_ids'] == "empty") {
                    
                    $inv_id = $request['order_id'];

                    $portfolio_details = PortfolioDetails::where('investment_id',$inv_id)->get();

                    $p_count = count($portfolio_details);
                    $count = 0;

                            foreach ($portfolio_details as $portfolio) {
                                $portfolio->portfolio_status = 0;
                                $portfolio->units_held = 0;
                                $portfolio->units_held = NULL;
                                $portfolio->invested_nav = 0;
                                
                                if ($portfolio->save()) {
                                    $count++;
                                }


                            }


                            if ($p_count == $count) {

                                $p_count = PortfolioDetails::where('investment_id',$inv_id)->where('portfolio_status','1')->get()->count();
                                if ($p_count == 0) {
                                    $update_inv = InvestmentDetails::where('id',$inv_id)->update(['investment_status'=>0]);

                                    if ($update_inv) {
                                        return response()->json(['msg'=>'success']);
                                    }
                                }
                                
                            }else{
                               return response()->json(['msg'=>'failure']);
                            }

                }else{      

                    if(count($request['portfolio_ids']) > 1)
                    {
                        $ids = $request['portfolio_ids'];   
                    }else{
                         $ids = explode(',', $request['portfolio_ids']);  
                    }
                    foreach ($portfolio_ids as $id) {
                        $portfolio_orders[] = PortfolioDetails::where('investment_id',$request['order_id'])->whereNotIn('id',$ids)->get();
                    }


                    $count = 0;

                            foreach ($portfolio_orders as $portfolio_order) {
                                    $p_count = count($portfolio_order);
                                foreach ($portfolio_order as $portfolio) {
                                    # code...
                                

                                    
                                    $portfolio->portfolio_status = 0;
                                    $portfolio->units_held = 0;
                                    $portfolio->units_held = NULL;
                                    $portfolio->invested_nav = 0;
                                    
                                    if ($portfolio->save()) {
                                        $count++;
                                    }

                                }
                            }


                            if ($p_count == $count) {
                               return response()->json(['msg'=>'success']);
                            }else{
                                return response()->json(['msg'=>'failure']);
                            }

                    
                }

                



            }

            if ($request['order_type'] == 'wd') {
                if ($request['portfolio_ids'] == "empty") {
                    //dd('empty');
                    $wd_group_id = $request['order_id'];
                    $withdraw_details = WithdrawDetails::where('withdraw_group_id',$wd_group_id)->get();
                    //dd($withdraw_details);
                    $count = 0;
                    $w_count = count($withdraw_details);

                        foreach ($withdraw_details as $withdraw ) {
                            $withdraw->withdraw_status = 0;
                            $withdraw->withdraw_nav = NULL;
                            $withdraw->withdraw_units = NULL;
                            $withdraw->withdraw_date = NULL;
                            
                            if ($withdraw->save()) {
                                $count++;
                            }



                        }

                        if ($count == $w_count) {
                            return response()->json(['msg'=>'success']);
                        }else{
                            return response()->json(['msg'=>'failure']);
                        }

                }else{

                    if(count($request['portfolio_ids']) == 1)
                    {
                        $ids = $request['portfolio_ids'];   
                        $withdraw_orders[] = WithdrawDetails::where('id',$request['order_id'])->where('id','!=',$ids)->get();
                    }else{
                        $ids = explode(',', $request['portfolio_ids']); 
                        foreach ($withdraw_ids as $id) {
                            $withdraw_orders[] = WithdrawDetails::where('id',$request['order_id'])->whereNotIn('id',$ids)->get();
                        }   
                    }
                    
                    $withdraw_ids = $ids;
                    


                    $count = 0;

                            foreach ($withdraw_orders as $withdraw_order) {
                                    $p_count = count($withdraw_order);

                                    //dd($p_count);
                                foreach ($withdraw_order as $withdraw) {
                                    # code...
                                

                                    
                                    $withdraw->withdraw_status = 0;
                                    $withdraw->withdraw_units = 0;
                                    $withdraw->withdraw_nav = NULL;
                                    $withdraw->withdraw_date = NULL;
                                    
                                    if ($withdraw->save()) {
                                        $count++;
                                    }

                                }
                            }


                            if ($p_count == $count) {
                               return response()->json(['msg'=>'success']);
                            }else{
                                return response()->json(['msg'=>'failure']);
                            }
                   
                }
            }
        }

    }

    //function to get filtered order history data or document
    public function getIndUserOrderHistory(Request $request)
    {
        $user_id = '';
        $start_date = '';
        $end_date = '';
        $request = $request->all();//Log::info($request);
        $response_type = 'document';
        if(array_key_exists('user_id', $request)) {
            $user_id = $request['user_id'];
        }
        if(array_key_exists('from_exp', $request)) {
            $start_date = $request['from_exp'];
        }
        if(array_key_exists('to_exp', $request)) {
            $end_date = $request['to_exp'];
        }
        if(array_key_exists('response_type', $request)) {
            $response_type = $request['response_type'];
        }
        $ind_user_order_history = $this->get_order_history($response_type, $user_id, $start_date, $end_date);
        if($response_type == 'data') {//Log::info($ind_user_order_history);
            if(!empty($ind_user_order_history)) {
                return response()->json(['msg' => 'success', 'order_history' => $ind_user_order_history]);
            } else {
                return response()->json(['msg' => 'failure']);
            }

        } else {
            $file_content = Excel::create('Order History', function($excel) use ($ind_user_order_history) {
                $excel->sheet('mySheet', function($sheet) use ($ind_user_order_history)
                {
                    $sheet->fromArray($ind_user_order_history);
                });
            })->download('xlsx');
        }

    }

    //function to get order history
    public function get_order_history($response_type = 'document', $user_id = '', $start_date = '', $end_date = '')
    {//Log::info($start_date);Log::info($end_date);
        $user_investments_withdraws = array();
        $count =1;
        if($user_id == '') {
            if (\Auth::id() == 145) {
                // $bpoids = NachDetails::where('priority',null)->pluck('user_id');
                $users = User::where('handle_by',0)->get();//Log::info($users->id);
            }else{
                $users = User::all();
            }
        } else {
                $users = User::where('id', $user_id)->get();//Log::info($users->id);
        }//Log::info($users->count());

        //iterating all users to get investments
        foreach ($users as $user) {//Log::info($user->id);
            
            //for user investments
            $user_investments = $user->investmentDetails()
                ->whereIn('investment_status', [0,1,2,3])
                ->orderBy('investment_date', 'asc')
                ->get();

            //iterating all investments of the user
            foreach($user_investments as $user_investment) {
                if($start_date != '' && $end_date != '') {
                    $start_date = date('Y-m-d', strtotime($start_date));
                    $end_date = date('Y-m-d', strtotime($end_date));
                    $user_portfolios = $user_investment->portfolioDetails()
                    ->whereBetween('investment_date', [$start_date, $end_date])
                    //->where('investment_date','>', $start_date)
                    //->where('investment_date','<', $end_date)
                    ->whereIn('portfolio_status',[0,1,2,3])
                    ->get();
                } else {
                     $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1,2,3])
                    ->get();
                }

                if($user_portfolios->count() > 0) {
                    $user_portfolio = $user_portfolios->first();
                    $investment_date = '';
                   
                    if($user_portfolio->investment_date != NULL) {
                        $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                    }
                    
                    if($response_type == 'data') {
                        $user_investments_withdraws[] = array(
                            'id' => $user_portfolio->investment_id,
                            'user_name' => $user->name,
                            'contact_no' => $user->mobile,
                            'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                            'investment_type' => $this->investment_types[$user_investment->investment_type],
                            'investment_or_withdraw' => 'Investment',
                            'date' => $investment_date,
                            'amount' => $user_portfolios->sum('amount_invested'),
                            'status' => $user_investment->investment_status,
                        );
                    } else {
                        $user_investments_withdraws[] = array(
                            'SNO' => $count,
                            'Name' => $user->name,
                            'Contact Number' => $user->mobile,
                            'Portfolio Type' => $this->portfolio_types[$user_investment->portfolio_type],
                            'Investment Type' => $this->investment_types[$user_investment->investment_type],
                            'Transaction Type' => 'Investment',
                            'Date' => $investment_date,
                            'Amount' => $user_portfolios->sum('amount_invested'),
                            'Status' => $user_investment->investment_status,
                        );
                        $count++;
                    }
                }
            }


            //for user withdrawls
            if($start_date != '' && $end_date != '') {
                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));
               
                $user_withdraws = $user->withdrawDetails()
                    ->whereBetween('withdraw_date', [$start_date, $end_date])
                    ->whereIn('withdraw_status', [0,1,2])
                    ->groupBy('withdraw_group_id');
            } else {
                $user_withdraws = $user->WithdrawDetails
                    ->whereIn('withdraw_status', [0,1,2])
                    ->groupBy('withdraw_group_id');
            }
            
            if($user_withdraws->count() > 0) {
                foreach($user_withdraws as $user_withdraw) {
                    $withdraw_data = $user_withdraw->first();
                    
                    if($response_type == 'data') {
                        $user_investments_withdraws[] = array(
                            'id' => $withdraw_data->withdraw_group_id,
                            'user_name' => $user->name,
                            'contact_no' => $user->mobile,
                            'portfolio_type' => '',
                            'investment_type' => '',
                            'investment_or_withdraw' => 'Withdraw',
                            'date' => date('d-m-Y',strtotime($withdraw_data->withdraw_date)),
                            'amount' => $user_withdraw->sum('withdraw_amount'),
                            'status' => $withdraw_data->withdraw_status,
                        );
                    } else {
                        $user_investments_withdraws[] = array(
                            'SNO' => $count,
                            'Name' => $user->name,
                            'Contact Number' => $user->mobile,
                            'Portfolio Type' => '',
                            'Investment Type' => '',
                            'Transaction Type' => 'Withdraw',
                            'Date' => date('d-m-Y',strtotime($withdraw_data->withdraw_date)),
                            'Amount' => $user_withdraw->sum('withdraw_amount'),
                            'Status' => $withdraw_data->withdraw_status,
                        );
                        $count++;
                    }
                }
            }
        }
        
        return $user_investments_withdraws;
    }

    //function to get successfull investments
    public function modifyDetails($type, $id)
    {
        $portfolios = $this->get_investment_details($type, $id);//print_r($portfolios);
        return view('admin.modify-details')->with('modify_order_details',$portfolios);
    }

    //function to get successfull investments
    public function viewDetails($type, $id)
    {
        //print_r($portfolios);
        $portfolios = $this->get_investment_details($type, $id);
        return view('admin.view-details')->with('view_order_details',$portfolios);
    }

    //function to save modify order details
    public function saveModifyOrders(Request $request)
    {
        $modify_portfolios = $request['portfolio_details'];
        $transaction_type = $request['transaction_type'];
        $nav_date = new Carbon();
        $current_date = $nav_date->format('Y-m-d');
        $nav_date->subDay();
        $nav_date = $nav_date->format('Y-m-d');
        $investment_id = '';
        $save_status = false;
        $save_message = '';
        //processing each portfolio details
        foreach($modify_portfolios as $key => $modify_portfolio) {
            $id = intval($modify_portfolio['id']);

            //for processing modify Investment
            if($transaction_type == 'Investment') {
                $portfolio_detail = PortfolioDetails::where('id',$id)
                    ->where('portfolio_status',1)
                    ->first();
                if(($portfolio_detail != NULL) && !(array_key_exists('checked', $modify_portfolio))) {
                    $investment_id = intval($portfolio_detail->investment_id);//Log::info('Portfolio id'.$portfolio_detail->id);
                    $amount_invested = $portfolio_detail->amount_invested;
                    $scheme_nav = DailyNav::where('date', $nav_date)
                    ->where('scheme_code', $portfolio_detail->scheme_code)
                    ->first();
                    //$units_held = $amount_invested / $scheme_nav->nav_value;
                    $portfolio_detail->invested_nav = '';
                    $portfolio_detail->units_held = '';
                    //$portfolio_detail->investment_date = $current_date;
                    $portfolio_detail->portfolio_status = 0;
                    $portfolio_detail->save();
                    $save_status = true;
                    $save_message = "Investment Modified successfully";
                } else {
                    $save_status = false;
                    $save_message = "Investment Modify failed. Please check and try again";
                }
                
            } /*elseif($transaction_type == 'Withdraw') {
                $withdraw_detail = WithdrawDetails::where('id',$id)
                    ->where('withdraw_status',1)
                    ->first();

                if(($withdraw_detail != NULL) && !(array_key_exists('checked', $pending_portfolio))) {
                    
                    $current_value = 0; //current value of the portfolio
                    $scheme_code = $withdraw_detail->scheme_code; //scheme code of the portfolio to be processed
                    $withdraw_amount = $withdraw_detail->withdraw_amount; //amount to be withdrawn

                    //get the nav details related to the current processing withdraw id
                    $scheme_nav = NavDetails::where('date', $nav_date)
                        ->where('scheme_code', $scheme_code)
                        ->first();
                    
                    //get the user related to the withdraw
                    $user = $withdraw_detail->User;
                    
                    //get the portfolio's detail related to the withdraw
                    $portfolio_details = $user->portfolioDetails()
                        ->where('scheme_code', $scheme_code)
                        ->where('portfolio_status', 1)
                        ->get();
                    $current_value = $portfolio_details->sum('units_held') * $scheme_nav->nav_value;
                    if($current_value > $withdraw_amount) {
                        $withdraw_units = $withdraw_amount / $scheme_nav->nav_value; //calculating the units to be withdrawn
                        $withdraw_detail->withdraw_units = $withdraw_units;
                        $withdraw_detail->withdraw_nav = $scheme_nav->nav_value;
                        $withdraw_detail->withdraw_date = date('Y-m-d');
                        $withdraw_detail->withdraw_status = 1;
                        $portfolio_save_status;
                        //loop through all active portfolio details of the scheme to reduce the withdrawn units
                        foreach($portfolio_details as $portfolio_detail) {
                            //check if the withdraw units is greater than 0
                            if($withdraw_units > 0) {
                                //if the units held and withdraw units are same
                                if($portfolio_detail->units_held == $withdraw_units) {
                                    $withdraw_units = 0;
                                    $portfolio_detail->units_held = 0;
                                    $portfolio_detail->amount_invested = 0;
                                    $portfolio_detail->portfolio_status = 2;
                                    $portfolio_save_status = $portfolio_detail->save();
                                } elseif($portfolio_detail->units_held < $withdraw_units) {
                                    //if the units held less than  withdraw units
                                    $withdraw_units -= $portfolio_detail->units_held;
                                    $portfolio_detail->units_held = 0;
                                    $portfolio_detail->amount_invested = 0;
                                    $portfolio_detail->portfolio_status = 2;
                                    $portfolio_save_status = $portfolio_detail->save();
                                } elseif($portfolio_detail->units_held > $withdraw_units) {
                                    //if the units held is greater than the units to be withdrawn
                                    $portfolio_detail->units_held -= $withdraw_units;
                                    $portfolio_detail->amount_invested -= ($withdraw_units * $scheme_nav->nav_value);
                                    $withdraw_units = 0;
                                    $portfolio_save_status = $portfolio_detail->save();
                                }
                            }
                        }
                        if($portfolio_save_status) {
                            if($withdraw_detail->save()) {
                                $save_status = true;
                                $save_message = "Withdraw approved successfully";
                            } else {
                                $save_status = false;
                                $save_message = "Withdraw approve failed. Please check and try again";
                            }
                        } else {
                            $save_status = false;
                            $save_message = "Withdraw approve failed. Please check and try again";
                        }
                    } else {
                        $save_status = false;
                        $save_message = "Withdraw approve failed. Please check and try again";
                    }
                }
            }*/
        }
        if($investment_id !='' && $save_status) {
            /*$portfolios = PortfolioDetails::where('investment_id', $investment_id)
                ->where('portfolio_status',0);Log::info(gettype($portfolios));
            if($portfolios->count() == 0) { */
                $investment_detail = InvestmentDetails::where('id',$investment_id)->first();
                $investment_detail->investment_status = 0;
                //$investment_detail->investment_date = $current_date; 
                $investment_detail->save();
            //}
        }
        if($save_status) {
            return response()->json(['msg' => 'success', 'status' => $save_message]);
        } else {
            return response()->json(['msg' => 'success', 'status' => $save_message]);
        }
    }

    //function to get investment details
    public function get_investment_details($type, $id)
    {
        $portfolios = array();
        $scheme_types = array('eq'=>'Equity','bal'=>'Balanced','debt'=>'Debt', 'ts' => 'Tax Saver');
        $scheme_name_code = SchemeDetails::all()->pluck('scheme_name','scheme_code');
        if($type == 'investment') {
            $investment_details = InvestmentDetails::find($id);
            $user_details = $investment_details->User;
            $personal_details = $user_details->PersonalDetails;
            $portfolio_details = $investment_details->PortfolioDetails;
            $portfolios['portfolio_type'] = $this->portfolio_types[$investment_details->portfolio_type];
            $portfolios['investment_type'] = $this->investment_types[$investment_details->investment_type];
            $portfolios['transaction_type'] = 'Investment';
            $portfolios['date'] = date('d-m-Y', strtotime($investment_details->investment_date));
            $portfolios['amount'] = $investment_details->investment_amount;
            $portfolios['name'] = $user_details->name;
            $portfolios['mobile'] = $user_details->mobile;
            $portfolios['email'] = $user_details->email;
            $portfolios['profile_image'] = $personal_details->profile_img;

            foreach ($portfolio_details as $portfolio_detail) {
                $portfolios['portfolio_details'][$portfolio_detail->portfolio_type][] = array(
                    'scheme_code' => $portfolio_detail->scheme_code,
                    'portfolio_id' => $portfolio_detail->id,
                    'scheme_name' => $scheme_name_code[$portfolio_detail->scheme_code],
                    'amount' => $portfolio_detail->amount_invested,
                    'portfolio_status' => $portfolio_detail->portfolio_status,
                );
            }
        } elseif($type == 'withdraw') {
            $withdraw_details = WithdrawDetails::where('withdraw_group_id',intval($id))->get();
            $withdraw = $withdraw_details->first();
            $user_details = $withdraw->User;
            $personal_details = $user_details->PersonalDetails;
            $portfolios['portfolio_type'] = '';
            $portfolios['investment_type'] = '';
            $portfolios['transaction_type'] = 'Withdraw';
            $portfolios['date'] = date('d-m-Y', strtotime($withdraw->created_at));
            $portfolios['amount'] = $withdraw_details->sum('withdraw_amount');
            $portfolios['name'] = $user_details->name;
            $portfolios['mobile'] = $user_details->mobile;
            $portfolios['email'] = $user_details->email;
            $portfolios['profile_image'] = $personal_details->profile_img;

            foreach ($withdraw_details as $withdraw_detail) {
                $portfolios['portfolio_details'][$withdraw_detail->portfolio_type][] = array(
                    'scheme_code' => $withdraw_detail->scheme_code,
                    'portfolio_id' => $withdraw_detail->id,
                    'scheme_name' => $scheme_name_code[$withdraw_detail->scheme_code],
                    'amount' => $withdraw_detail->withdraw_amount,
                    'portfolio_status' => $withdraw_detail->withdraw_status,
                );
            }
        }
        return $portfolios;
    }

    //function to cancel pending order
    public function cancelPendingOrder(Request $request)
    {
        $validator = \Validator::make($request->all(),[

            'id' => 'required',
            'transaction_type' => 'required',
            ]);

        if ($validator->fails()) {
            
            return response()->json(['msg' => 'failure', 'status' => 'Something went wrong. Please refresh and try again']);
        } else {
            $transaction_type = $request['transaction_type'];
            $id = intval($request['id']);

            //processing cancel order for withdraw
            if($transaction_type == 'Withdraw') {
                $withdraw_detail = WithdrawDetails::find($id);
                if($withdraw_detail != NULL && $withdraw_detail->withdraw_status == 0) {
                    $withdraw_detail->withdraw_status = 2;
                    if($withdraw_detail->save()) {
                        return response()->json(['msg' => 'success', 'status' => 'You have successfully cancelled  withdraw', 'amount' => $withdraw_detail->withdraw_amount, 'date' => date('d-m-Y')]);
                    } else {
                        return response()->json(['msg' => 'failure', 'status' => 'Withdraw cancel failed. Please refresh and try again']);
                    }
                } else {
                    return response()->json(['msg' => 'failure', 'status' => 'Withdraw cancel failed. Please refresh and try again']);
                }
            } else if($transaction_type == 'Investment') {//processing cancel order for investment
                $portfolio_detail = PortfolioDetails::find($id);Log::info($portfolio_detail);
                if($portfolio_detail != NULL && $portfolio_detail->portfolio_status == 0) {
                    $portfolio_detail->portfolio_status = 3;
                    if($portfolio_detail->save()) {
                        /*$investment_detail = $portfolio_detail->investmentDetails();
                        //$investment_detail = InvestmentDetails::find($portfolio_detail->investment_id);
                        
                        $portfolio_count = $investment_detail->portfolioDetials()->count();
                        if($portfolio_count == 1) {
                            $investment_detail->investment_status = 3;
                            $investment_detail->save();
                        }*/
                        
                        return response()->json(['msg' => 'success', 'status' => 'You have successfully cancelled investment ', 'amount' => $portfolio_detail->amount_invested, 'date' => date('d-m-Y')]);
                    } else {
                        return response()->json(['msg' => 'failure', 'status' => 'Investment cancel failed. Please refresh and try again']);
                    }

                } else {
                    return response()->json(['msg' => 'failure', 'status' => 'Investment cancel failed. Please refresh and try again']);    
                }
            } else {
                return response()->json(['msg' => 'failure', 'status' => 'Something went wrong. Please refresh and try again']);
            }
        }
    }

    //public function to create user investment details
    public function getUserInvestmentPerformance($user_id)
    {
        $user_details = User::find($user_id);//Log::info($user_id);
        $xirr_inv_wd_amount = array();
        $xirr_inv_wd_date = array();
        $xirr = 0;
        $portfolio_amount_invested = 0;
        $portfolio_current_value = 0;
        $portfolio_day_change = 0;
        $portfolio_performance = array();
        $current_date = Carbon::now()->toDateString();

        $portfolio_details = $user_details->portfolioDetails()
            ->whereIn('portfolio_status', [1,2])
            ->orderBy('investment_date', 'asc')
            ->get();

        $withdraw_details = $user_details->withdrawDetails()
            ->where('withdraw_status', 1)
            ->orderBy('withdraw_date', 'asc')
            ->get();

        //$portfolioDetails = $portfolio_details->groupBy('scheme_code');
        
        //Log::info(get_class($nav_details));
        $old_active_investments = $user_details->portfolioDetails()
            ->where('investment_date','<',$current_date)
            ->where('portfolio_status', 1)
            ->count();

        //checking whether an active old investment exist
        //if($old_active_investments > 0) {
            //processing investment details for xirr
            foreach($portfolio_details as $portfolio_detail) {
                $nav_details = DailyNav::where('scheme_code', $portfolio_detail->scheme_code)
                ->orderBy('date', 'desc')
                ->first();
                $day_sub_one = DailyNav::where('scheme_code', $portfolio_detail->scheme_code)
                ->where('date','<',$nav_details->date)
                ->orderBy('date', 'desc')
                ->first();
                if($old_active_investments > 0) {
                    $xirr_inv_wd_amount[] = -1 * $portfolio_detail->initial_amount_invested;
                    $xirr_inv_wd_date[] = $portfolio_detail->investment_date;
                }
                $portfolio_current_value += ($portfolio_detail->units_held * $nav_details->nav_value);
                $portfolio_day_change += ($portfolio_detail->units_held * $day_sub_one->nav_value);
                $portfolio_amount_invested += $portfolio_detail->units_held * $portfolio_detail->invested_nav;
            }
        //}

        //checking if the inestments are there
        if(!empty($xirr_inv_wd_amount)) {
            
            //processing withdraw details for xirr
            foreach($withdraw_details as $withdraw_detail) {
                $xirr_inv_wd_amount[] = $withdraw_detail->withdraw_amount;
                $xirr_inv_wd_date[] = $withdraw_detail->withdraw_date;
            }

            //adding current value for xirr calculation
            $xirr_inv_wd_amount[] = $portfolio_current_value;
            $xirr_inv_wd_date[] = $current_date;
        }//Log::info($xirr_inv_wd_date);Log::info($xirr_inv_wd_amount);

        if(!empty($xirr_inv_wd_amount)) {Log::info($xirr_inv_wd_amount);Log::info($xirr_inv_wd_date);
            $xirr = PHPExcel_Calculation_Financial::XIRR($xirr_inv_wd_amount,$xirr_inv_wd_date);
            $xirr = round(($xirr * 100),2);
        }
        //Log::info($xirr);
        $day_change = $portfolio_current_value - $portfolio_day_change;
        $portfolio_performance['amount_invested'] = $portfolio_amount_invested;
        $portfolio_performance['current_value'] = round($portfolio_current_value, 2);
        $portfolio_performance['profit_loss_amount'] = round(($portfolio_current_value - $portfolio_amount_invested),2);
        $portfolio_performance['day_change'] = round($day_change, 2);
        $portfolio_performance['xirr'] = round($xirr, 2);Log::info($xirr);

        return $portfolio_performance;
    }

    //function to get new enrolled user investment performance
    public function getNewUserInvestmentPerformance()
    {
        $user_investment_performance = array();
        $range_date = Carbon::now()->subDays(7)->toDateTimeString();
        $user_details = User::where('p_check', '1')
            //->where('created_at','>', $range_date)
            ->get();

        foreach($user_details as $user_detail) {
            $sex = $user_detail->PersonalDetails->sex;
            $profile_image = 'male.png';
            if($user_detail->PersonalDetails->profile_image != '') {
                $profile_image = $user_detail->PersonalDetails->profile_image;
            } else {
                if($sex == 'male'){
                    $profile_image = 'male.png';
                } elseif($sex == 'female'){
                    $profile_image = 'female.png';
                }
            }

            $investment_status = $this->getUserInvestmentPerformance($user_detail->id);
            $user_investment_performance[] = array(
                'profile_image' => $profile_image,
                'name' =>$user_detail->name,
                'email' => $user_detail->email,
                'mobile' => $user_detail->mobile,
                'amount_invested' => $investment_status['amount_invested'],
                'current_value' => $investment_status['current_value'],
                'profit_loss_amount' => $investment_status['profit_loss_amount'],
                'xirr_return' => $investment_status['xirr'],
            );
        }
        if(!empty($user_investment_performance)) {
            return response()->json(['msg' => 'success', 'response' => $user_investment_performance]);
        } else {
            return response()->json(['msg' => 'failure']);
        }
    }

    //function to get new enrolled user investment performance
    public function getIndUserInvestmentPerformance(Request $request)
    {
        $user_investment_performance = array();
        $portfolio_types = array('debt', 'bal', 'eq', 'ts'); //portfolio type
        $id = $request['user_id'];
        $range = $request['transaction_range'];
        $response_data = $request['response_type'];
        $range_date;
        switch($range) {
            case '4': {
                $range_date = Carbon::now()->subMonths(6)->toDateTimeString();
                break;
            }

            case '2': {
                $range_date = Carbon::now()->subDays(7)->toDateTimeString();
                break;
            }

            case '3': {
                $range_date = Carbon::now()->subMonths(1)->toDateTimeString();
                break;
            }

            default: {
                $range_date = Carbon::now()->subDays(1)->toDateTimeString();
                break;
            }

        }
        if($id != '') {
            $user_detail = User::find($id);
                //->where('created_at','>', $range_date)
                //->get();

            //foreach($user_details as $user_detail) {
                $sex = $user_detail->PersonalDetails->sex;
                $profile_image = 'male.png';
                if($user_detail->PersonalDetails->profile_image != '') {
                    $profile_image = $user_detail->PersonalDetails->profile_image;
                } else {
                    if($sex == 'male'){
                        $profile_image = 'male.png';
                    } elseif($sex == 'female'){
                        $profile_image = 'female.png';
                    }
                }

                $investment_status = $this->getUserInvestmentPerformance($user_detail->id);
                $user_investment_performance[] = array(
                    'profile_image' => $profile_image,
                    'name' =>$user_detail->name,
                    'email' => $user_detail->email,
                    'mobile' => $user_detail->mobile,
                    'amount_invested' => $investment_status['amount_invested'],
                    'current_value' => $investment_status['current_value'],
                    'profit_loss_amount' => $investment_status['profit_loss_amount'],
                    'xirr_return' => $investment_status['xirr'],
                );
            //}

            

            $investment_summary = array();//Log::info($range_date);
            $investment_details = $user_detail->investmentDetails()
                ->orderBy('investment_date', 'asc')
                ->get();
            $count = 0;
            foreach ($investment_details as $investment_detail) {
            
                //foreach($portfolio_types as $portfolio_type) {
                    $portfolio_details = $investment_detail->portfolioDetails
                        //->where('portfolio_type', $portfolio_type)
                        ->whereIn('portfolio_status', [0,1])
                        ->groupBy('portfolio_type')
                        ->all();//Log::info($portfolio_type);
                        // var_dump(die($portfolio_details));
                    // dd($portfolio_details);
                    foreach ($portfolio_details as $inv_id => $portfolio_detail) {//Log::info($inv_id);
                        //$ind_portfolio_details = $portfolio_detail->first();
                        if($response_data == 'data') {
                            $investment_summary[$inv_id][] = array(
                                'sno' => $count,
                                'sum' => $portfolio_detail->sum('amount_invested'),
                                'date' => date('d-m-Y', strtotime($investment_detail->investment_date)),
                                'investment_type' => $this->investment_types[$investment_detail->investment_type],
                                'investment_status' => $this->investment_status[$investment_detail->investment_status],
                                'portfolio_type' => $inv_id,
                            );
                        } else {
                            $investment_summary[] = array(
                                'sno' => $count,
                                'Investment Amount' => $portfolio_detail->sum('amount_invested'),
                                'Date' => date('d-m-Y', strtotime($investment_detail->investment_date)),
                                'Investment Type' => $this->investment_types[$investment_detail->investment_type],
                                'Investment Status' => $this->investment_status[$investment_detail->investment_status],
                                //'Portfolio Type' => $inv_id,
                            );
                        }
                        $count++;
                    }
                //}//Log::info($investment_summary);
            }

            if(!empty($user_investment_performance)) {
                if($response_data == 'data') {
                    return response()->json(['msg' => 'success', 'response' => $user_investment_performance, 'investment_summary' => $investment_summary, 'id' => $id]);
                } else {
                    $file_content = Excel::create('Investment Summary - '.$user_detail->name, function($excel) use ($investment_summary) {
                        $excel->sheet('mySheet', function($sheet) use ($investment_summary)
                        {
                            $sheet->fromArray($investment_summary);
                        });
                    })->download('xlsx');
                }
            } else {
                if($response_data == 'data') {
                    return response()->json(['msg' => 'failure']);
                } else {
                    return 'Unable to fetch data';
                }
            }
        }
    }

    //function to get new enrolled user portfolio performance
    public function getIndUserPortfolioPerformance(Request $request)
    {
        $user_investment_performance = array();
        $portfolio_types = array('debt', 'bal', 'eq', 'ts'); //portfolio type
        $id = $request['user_id'];
        $range = $request['transaction_range'];
        $response_type = $request['response_type'];
        $range_date;
        $count = 1; //for serial number in document
        $scheme_name = SchemeDetails::pluck('scheme_name','scheme_code');
        switch($range) {
            case '4': {
                $range_date = Carbon::now()->subMonths(6)->toDateTimeString();
                break;
            }

            case '2': {
                $range_date = Carbon::now()->subDays(7)->toDateTimeString();
                break;
            }

            case '3': {
                $range_date = Carbon::now()->subMonths(1)->toDateTimeString();
                break;
            }

            default: {
                $range_date = Carbon::now()->subDays(1)->toDateTimeString();
                break;
            }

        }
        if($id != '') {
            $user_detail = User::find($id);
                //->where('created_at','>', $range_date)
                //->get();

            //foreach($user_details as $user_detail) {
                $sex = $user_detail->PersonalDetails->sex;
                $profile_image = 'male.png';
                if($user_detail->PersonalDetails->profile_image != '') {
                    $profile_image = $user_detail->PersonalDetails->profile_image;
                } else {
                    if($sex == 'male'){
                        $profile_image = 'male.png';
                    } elseif($sex == 'female'){
                        $profile_image = 'female.png';
                    }
                }

                $investment_status = $this->getUserInvestmentPerformance($user_detail->id);
                $user_investment_performance[] = array(
                    'profile_image' => $profile_image,
                    'name' =>$user_detail->name,
                    'email' => $user_detail->email,
                    'mobile' => $user_detail->mobile,
                    'amount_invested' => $investment_status['amount_invested'],
                    'current_value' => $investment_status['current_value'],
                    'profit_loss_amount' => $investment_status['profit_loss_amount'],
                    'xirr_return' => $investment_status['xirr'],
                );
            //}

            


            //creating portfolio summary
            $portfolio_summary = array();
            $portfolio_summary_sum = array();
            $portfolio_details = $user_detail->portfolioDetails()
                ->whereIn('portfolio_status', [1])
                ->get();
            $ltcg_total = 0;
            $total_initial_investment = 0;
            $total_net_return = 0;
            $total_current_value = 0;

            foreach ($portfolio_details as $portfolio) {

                $ltcg = 0;
                $net_returns = 0;
                $current_value = 0;
                $current_nav = 0;

                if($portfolio->portfolio_status == 1) {

                    $nav_details = DailyNav::where('scheme_code',$portfolio->scheme_code)
                        ->orderBy('date', 'desc')
                        ->first();

                    $current_nav = $nav_details->nav_value;
                    $current_value = $portfolio->units_held * $current_nav;
                    $net_returns = round($current_value,2) - $portfolio->amount_invested;
                    //calculating ltcg return for equity funds
                    if ($portfolio->portfolio_type == "eq") {
                        $invested_date = $portfolio->investment_date;
                        $year_old = Carbon::now()->subDays(365)->toDateString();

                        if ($invested_date < $year_old) {
                            $ltcg = $net_returns;
                            $ltcg_total += $ltcg;
                        } else {
                            $ltcg = 0;
                            $ltcg_total += $ltcg;
                        }
                    }

                    //calculating ltcg return for debt funds
                    if ($portfolio->portfolio_type == "debt") {
                        $invested_date = $portfolio->investment_date;
                        $year_old = Carbon::now()->subDays(1095)->toDateString();
                        if ($invested_date < $year_old) {
                            $ltcg = $net_returns;
                            $ltcg_total += $ltcg;
                        }
                        else{
                            $ltcg = 0;
                            $ltcg_total += $ltcg;
                        }
                    }
                }
            
                if($response_type == 'data') {
                    $portfolio_summary[] = array(
                        'id' => $portfolio->id,
                        'scheme_code' => $portfolio->scheme_code,
                        'fund_name' => $scheme_name[$portfolio->scheme_code],
                        'folio_number' => $portfolio->folio_number,
                        'type' => $portfolio->portfolio_type,
                        'transaction_type' => 'Inv',
                        'date'=> date('d-m-Y', strtotime($portfolio->bse_allot_date)),
                        'amount'=> $portfolio->amount_invested,
                        'inv_nav' => $portfolio->invested_nav,
                        'units_held' => round($portfolio->units_held,4),
                        'current_nav' => $current_nav,
                        'current_value' => round($current_value, 2),
                        'net_returns' => round($net_returns, 2),
                        'ltcg' => round($ltcg, 2),
                    );
                } else {
                    $portfolio_summary[] = array(
                        'SNo' => $count,
                        'Scheme Code' => $portfolio->scheme_code,
                        'Fund Name' => $scheme_name[$portfolio->scheme_code],
                        'Folio Number' => $portfolio->folio_number,
                        'Portfolio Type' => $portfolio->portfolio_type,
                        'Transaction Type' => 'Investment',
                        'Date'=> date('d-m-Y', strtotime($portfolio->bse_allot_date)),
                        'Amount'=> $portfolio->amount_invested,
                        'Invested NAV' => $portfolio->invested_nav,
                        'Units Held' => round($portfolio->units_held,4),
                        'Current NAV' => $current_nav,
                        'Current Value' => round($current_value, 2),
                        'Net Returns' => round($net_returns, 2),
                        'LTCG' => round($ltcg, 2),
                    );
                    $count++;
                }
                
                $ltcg_total += $ltcg;
                $total_initial_investment += $portfolio->amount_invested;
                $total_net_return += $net_returns;
                $total_current_value += $current_value;
            }

            $withdraw_details = $user_detail->withdrawDetails()
                ->whereIn('withdraw_status', [1])
                ->get();

            //creating withdraw details for summary
            foreach ($withdraw_details as $withdraw_detail) {
                if($response_type == 'data') {
                    $portfolio_summary[] = array(
                        'id' => $withdraw_detail->id,
                        'scheme_code' => $withdraw_detail->scheme_code,
                        'fund_name' => $scheme_name[$withdraw_detail->scheme_code],
                        'folio_number' => '',
                        'type' => $withdraw_detail->portfolio_type,
                        'transaction_type' => 'Wd',
                        'date'=> date('d-m-Y', strtotime($withdraw_detail->withdraw_date)),
                        'amount'=> $withdraw_detail->withdraw_amount,
                        'inv_nav' => '',
                        'units_held' => '',
                        'current_nav' => '',
                        'current_value' => '',
                        'net_returns' => '',
                        'ltcg' => '',
                    );
                } else {
                    $portfolio_summary[] = array(
                        'SNo' => $count,
                        'Scheme Code' => $withdraw_detail->scheme_code,
                        'Fund Name' => $scheme_name[$withdraw_detail->scheme_code],
                        'Folio Number' => '',
                        'Portfolio Type' => $withdraw_detail->portfolio_type,
                        'Transaction Type' => 'Withdraw',
                        'Date'=> date('d-m-Y', strtotime($withdraw_detail->withdraw_date)),
                        'Amount'=> $withdraw_detail->withdraw_amount,
                        'Invested NAV' => '',
                        'Units Held' => '',
                        'Current Nav' => '',
                        'Current Value' => '',
                        'Net Returns' => '',
                        'LTCG' => '',
                    );
                    $count++;
                }   
            }
            $portfolio_summary_sum = array(
                'total_ltcg' => round($ltcg_total, 2),
                'total_initial_investment' => $total_initial_investment,
                'total_net_return' => round($total_net_return, 2),
                'total_current_value' => round($total_current_value,2),
            );
            //Log::info($user_investment_performance);
            //Log::info($portfolio_summary);
            //Log::info($portfolio_summary_sum);
            

            if(!empty($user_investment_performance)) {
                if($response_type == 'data') {
                    usort($portfolio_summary, function($a, $b) {
                        return strtotime($a['date']) - strtotime($b['date']);
                    });

                    return response()->json(['msg' => 'success', 'response' => $user_investment_performance, 'portfolio_summary' => $portfolio_summary, 'portfolio_summary_sum' => $portfolio_summary_sum, 'id' => $id]);
                } else {
                    $portfolio_summary[] = array(
                        'SNo' => '',
                        'Scheme Code' => 'Total',
                        'Fund Name' => '',
                        'Folio Number' => '',
                        'Portfolio Type' => '',
                        'Transaction Type' => '',
                        'Date'=> '',
                        'Amount'=> $total_initial_investment,
                        'Invested NAV' => '',
                        'Units Held' => '',
                        'Current Nav' => '',
                        'Current Value' => round($total_current_value,2),
                        'Net Returns' => round($total_net_return, 2),
                        'LTCG' => round($ltcg_total, 2),
                    );
                    $file_content = Excel::create('Portfolio Summary', function($excel) use ($portfolio_summary) {
                        $excel->sheet('mySheet', function($sheet) use ($portfolio_summary)
                        {
                            $sheet->fromArray($portfolio_summary);
                        });
                    })->download('xlsx');
                }
            } else {
                if($response_type == 'data') {
                    return response()->json(['msg' => 'failure']);
                } else {
                    return 'Unable to fetch data';
                }
            }
        }
    }

    //function to check and make auto entry for recuring sip's
    public function createRecurringSipInvestment()
    {
        $sips = Sip::all();
        $scheme_details = SchemeDetails::all()->pluck('bse_scheme_code','scheme_code');
        foreach($sips as $sip) {
            $sip_start_date = new Carbon($sip->start_date);
            $current_date = carbon::now();
            $current_sip_date = new Carbon($current_date->year.'-'.$current_date->month.'-'.$sip_start_date->day);
            //$sip_start_date->addMonths(1);

            $diff_in_days = $current_sip_date->diffInDays($current_date);
            if($diff_in_days >= 0 && $diff_in_days <= 7) {

                $current_month = $current_date->month;
                $current_year = $current_date->year;
                $sip_id = $sip->id;

                $sip_details = SipDetails::all()
                    ->where('sip_id', $sip_id)
                    ->where('month', $current_month)
                    ->where('year', $current_year);
                // dd($sip_details);
                    // dd(count($sip_details));
                if(count($sip_details) == 0) {

                    // dd($sip_id);
                    $user = $sip->user;
                    //$investment_detail = $sip->investmentDetails;
                    $portfolio_details = $sip->sipEntries;
                    $new_investment_details = new InvestmentDetails();
                    // dd($sip, $sip_id, $current_month, $current_year);
                    $new_investment_details->portfolio_type = $this->sip_port_type[$sip->port_type];
                    $new_investment_details->investment_amount = $sip->inv_amount;
                    $new_investment_details->investment_date = $current_sip_date->toDateString();
                    $new_investment_details->investment_type = 2;
                    $new_investment_details->investment_status = 0;
                    $new_investment_details->sip_id = $sip_id;
                    $new_investment_save = $user->investmentDetails()->save($new_investment_details);


                    if($new_investment_save) {
                        foreach($portfolio_details as $portfolio_detail) {
                            $scheme_code_sip = SipEntries::where('sip_id',$sip->id)->value('scheme_code');
                            $port_type_sip = SchemeDetails::where('scheme_code',$scheme_code_sip)->value('scheme_type');
                            // if ($port_type_sip == NULL) {
                            //     dd($sip->id,$portfolio_detail);
                            // }

                            $new_portfolio_detail = new PortfolioDetails();
                            $new_portfolio_detail->investment_id = $new_investment_save->id;
                            $new_portfolio_detail->scheme_code = $portfolio_detail->scheme_code;
                            $new_portfolio_detail->bse_scheme_code = $scheme_details[$portfolio_detail->scheme_code];
                            $new_portfolio_detail->folio_number = '';
                            $new_portfolio_detail->portfolio_status = 0;
                            $new_portfolio_detail->sip_reg_no = $portfolio_detail->sip_reg_no;
                            $new_portfolio_detail->initial_amount_invested = $portfolio_detail->amount;
                            // $new_portfolio_detail->portfolio_type = $sip->port_type;
                            $new_portfolio_detail->portfolio_type = $port_type_sip;
                            $new_portfolio_detail->amount_invested = $portfolio_detail->amount;
                            $new_portfolio_detail->investment_date = $current_sip_date->toDateString();
                            $new_portfolio_detail->units_held = '';
                            $new_portfolio_detail->sip_id = $sip_id;
                            $user->portfolioDetails()->save($new_portfolio_detail);
                        }

                        $sipdetails = new SipDetails();
                        $sipdetails->month = $current_month;
                        $sipdetails->year = $current_year;

                        $sip->sipDetails()->save($sipdetails);
                    }
                }
            }
        }
    }

    //function to get sip values
    public function getSipReturn(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'investment_amount' => 'required',
            'investment_period' => 'required',
            'interest_rate' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(['msg'=> 'error', 'errors' => $validator->errors()]);
        } else {
            setlocale(LC_NUMERIC, 'in_IN');
            //$fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);
            $request = $request->all();
            $investment_amount = $request['investment_amount'];
            $interest_rate = (($request['interest_rate'])/100)/12;
            $investment_period = $request['investment_period'];
            $increment_rate = 0;
            $sip_return = 0;
            $current_year = carbon::now()->year;
            $sip_investment_data = array();
            if(array_key_exists('increment_rate', $request)) {
                $increment_rate = $request['increment_rate'];
            }

            $sip_investment_data[] = array(
                'investment_year' => $current_year,
                'non_sip_amount' => 0,
                'sip_amount' => 0,
            ); 

            $initial_amount = $investment_amount;
            $new_investment_amount = $investment_amount;
            $current_investment_amount = $new_investment_amount;
            $investment_month_period = 0;
            $non_sip_amount = 0;
            $max_investment_month_period = ($investment_period * 12);
            //calculating sip return sum
            while($investment_month_period < $max_investment_month_period) {
                $investment_month_period ++;
                $non_sip_amount += $initial_amount;
                $new_investment_amount += ($new_investment_amount * $interest_rate);
                if($investment_month_period % 12 == 0){
                    $current_year++;
                    $sip_investment_data[] = array(
                        'investment_year' => $current_year,
                        'non_sip_amount' => round($non_sip_amount),//$fmt->format(round($non_sip_amount)),
                        'sip_amount' => round($new_investment_amount),//$fmt->format(round($new_investment_amount)),
                    ); 
                    if($increment_rate > 0){
                        $initial_amount += (($initial_amount * $increment_rate)/100);
                    }
                }
               
                if($investment_month_period < $max_investment_month_period) {
                    $new_investment_amount += $initial_amount;
                }
                
            }
            return response()->json(['msg' => 'success', 'sip_return' => round($new_investment_amount), 'sip_details' => $sip_investment_data]);
        }
    }

     function manageSchemes()
    {
        $fund_type = array(
            'eq' => 'Equity', 
            'debt' => 'Debt', 
            'bal' => 'Hybrid',
            'ts' => 'ELSS'
        );
        $scheme_data = array();
        $scheme_details = SchemeDetails::all();
        foreach($scheme_details as $scheme_detail) {
            $scheme_data[] = array(
                'scheme_code' => $scheme_detail->scheme_code,
                'scheme_name' => $scheme_detail->scheme_name,
                'scheme_type' => $fund_type[$scheme_detail->scheme_type],
                'scheme_priority' => $scheme_detail->scheme_priority,
                'bse_scheme_code' => $scheme_detail->bse_scheme_code,
                'scheme_status' => $scheme_detail->scheme_status,
            );
        }
        // dd($scheme_data);
        usort($scheme_data, function($a, $b) {
            return $a['scheme_priority'] - $b['scheme_priority'];
        });

        return view('admin.scheme')->with('schemes', $scheme_data);
    }

    //function to add schemes
    public function addScheme(Request $request)
    {

         $this->revertScheme($request['scheme_code']);
        $validator = \Validator::make($request->all(),[
            'scheme_name' => 'required',
            'scheme_code' => 'required|min:6|max:6|unique:scheme_details',
            'bse_scheme_code' => 'required|unique:scheme_details',
            'scheme_type' => 'required',
            'scheme_priority' => 'required',
            'scheme_file' => 'required',
            'fund_manager' => 'required',
            'launch_date' => 'required',
            'asset_size' => 'required',
            'benchmark' => 'required',
            'exit_load' => 'required',
            'fund_type' => 'required'
        ]);

        if ($validator->fails()) {
            
            return response()->json(['msg' => $validator->errors()]);
            
        } else {

            //dd($_POST);

           
            set_time_limit (180);
            $existing_fund_count = SchemeDetails::where('scheme_type', $request['scheme_type'])
                ->where('scheme_priority', $request['scheme_priority'])
                ->get();

            //Checking if the uploaded new scheme priority is already present in the same scheme type.

            if($existing_fund_count->count() == 0) {    //Log::info($request);//return $request;
                if ($request->hasFile('scheme_file')) {
                    $date = '';
                    $new_scheme = new SchemeDetails();
                    $new_scheme_details = new SchemeHistory();
                    $new_scheme->scheme_name = $request['scheme_name'];
                    $new_scheme->scheme_code = $request['scheme_code'];
                    $new_scheme->bse_scheme_code = $request['bse_scheme_code'];
                    $new_scheme->scheme_type = $request['scheme_type'];
                    $new_scheme->scheme_priority = $request['scheme_priority'];
                    $new_scheme->scheme_status = 0;

                    //$save_scheme = $new_scheme->save();
                    
                    $new_scheme_details->scheme_code = $request['scheme_code'];
                    $new_scheme_details->name = $request['scheme_name'];
                    $new_scheme_details->investment_plan = 'Growth Option';
                    $new_scheme_details->launch_date = date('Y-m-d', strtotime($request['launch_date']));
                    $new_scheme_details->fund_type = $request['fund_type'];
                    $new_scheme_details->fund_manager = $request['fund_manager'];
                    $new_scheme_details->asset_size = $request['asset_size'];
                    $new_scheme_details->benchmark = $request['benchmark'];
                    $new_scheme_details->exit_load = $request['exit_load'];

                    //$save_scheme_details = $new_scheme_details->save();

                    //if ($save_scheme && $save_scheme_details) {
                        $file = $request->file('scheme_file');
                        $status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());

                        if ($status) {
                            if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE)
                            {

                                $no_of_lines = file(base_path().'/storage/csv/'.$file->getClientOriginalName());
                                $no_of_lines = count($no_of_lines);
                                $count = 0;
                                $is_scheme_saved = false;
                                $is_scheme_code_checked = false;
                                while (($data = fgetcsv($handle, 1000, ',')) !==FALSE)
                                {

                                    //dd($data[0]);

                                    $nav = new HistoricNav();
                                    if(!$is_scheme_code_checked) {
                                        if($data[0] == $request['scheme_code']) {
                                            $is_scheme_code_checked = true;
                                        } else {
                                            return response()->json(['msg'=>['status' => 'Scheme code provided does not match with the scheme code in the file.']]);
                                        }
                                    }
                                    
                                    if(!$is_scheme_saved) {
                                        if ($new_scheme->save() && $new_scheme_details->save()) {
                                            $is_scheme_saved =true;
                                        } else {
                                            return response()->json(['msg'=>['status' => 'Unable to create Scheme. Please check and try again.']]);
                                        }
                                    }
                                    
                                    $nav->scheme_code = $data[0];
                                    $nav->scheme_name = $data[1];
                                    $nav->nav = $data[2];
                                    $date = date('Y-m-d',strtotime($data[3]));
                                    $nav->date = $date;

                                    
                                    $nav->save();
                                    $count++;
                                    //echo $count;
                                }
                                    //echo $no_of_lines."<br>";
                                    //echo $count."<br>";
                                //die();

                                if ($no_of_lines == $count) {

                                    $store_daily_nav = $this->insertDailyNav($request['scheme_code']);
                                    
                                    if($store_daily_nav){
                                        $scheme_data = array(
                                            'scheme_code' => $request['scheme_code'],
                                            'scheme_name' => $request['scheme_name'],
                                            'fund_type' => $this->fund_type[$request['scheme_type']],
                                            'priority' => $request['scheme_priority'],
                                        );
                                        return response()->json(['msg'=>'1', 'scheme_data' => $scheme_data]);
                                    }else{
                                         return response()->json(['msg'=>['status' => 'Cannot upload rightnow. Try again later.']]);
                                    }
                                    
                                } else {//if the file lines are empty    
                                    return response()->json(['msg'=>['status' => 'Invalid file data format. Please check file data format and try again.']]);
                                }                               
                            } else {//if file opening fails
                                return response()->json(['msg'=>['status' => 'Unable to open the file. Please recheck the file and try again.']]);
                            }

                        } else {//if unable to move file to handle it.
                            return response()->json(['msg'=>['status' => 'Unable to use the file. Please recheck the file and try again.']]);
                        }

                    // } else {//if scheme creation fails
                        

                    // }
                   
                   set_time_limit(60); 
                } else { //if the nav doesn't exist
                    return response()->json(['msg'=>['status' => 'Invalid file. Please check and try again.']]);
                }
            } else {
                return response()->json(['msg'=>['status' => 'Scheme type with priority already exist.']]);
            }
        }
    }

    function insertDailyNav($scheme_code){
            
        $historic_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','desc')->limit(8)->get();
        $count = 0;
        //dd('inga varudhu');
        foreach ($historic_nav as $nav) {
            $store_nav = new DailyNav();
            $store_nav->scheme_code = $nav->scheme_code;
            $store_nav->nav_value = $nav->nav;
            $store_nav->date = $nav->date;
            if ($store_nav->save()) {
                $count++;
            }
            
        }

        if ($count == 8) {
            return true;
        }else{
            return false;
        }

    }

    function revertScheme($scheme_code){

        $scheme_history_count = count(SchemeHistory::where('scheme_code',$scheme_code)->get());
        $scheme_details_count = count(SchemeDetails::where('scheme_code',$scheme_code)->get());
        $historic_nav_count = count(HistoricNav::where('scheme_code',$scheme_code)->get());
        $daily_nav_count = count(DailyNav::where('scheme_code',$scheme_code)->get());


        //dd($scheme_history_count,$scheme_details_count,$historic_nav_count);

        if($scheme_history_count > 0){
            SchemeHistory::where('scheme_code',$scheme_code)->delete();
        }

        if($scheme_details_count > 0){
            SchemeDetails::where('scheme_code',$scheme_code)->delete();
        }

        if($historic_nav_count > 0){
            HistoricNav::where('scheme_code',$scheme_code)->delete();
        }

        if($daily_nav_count > 0){
            DailyNav::where('scheme_code',$scheme_code)->delete();
        }

            /*
                Uncomment below code to check if the deletion is successful. Successfull deletion results in count 0;

            */

        // $scheme_history_count = count(SchemeHistory::where('scheme_code',$scheme_code)->get());
        // $scheme_details_count = count(SchemeDetails::where('scheme_code',$scheme_code)->get());
        // $historic_nav_count = count(HistoricNav::where('scheme_code',$scheme_code)->get());


        // dd($scheme_history_count,$scheme_details_count,$historic_nav_count);
        
        

    }

    //function to get Scheme details for update form
    public function getUpdateSchemeData(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'scheme_code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'Something went wrong. Please refersh and try again.', 'status' => 0]);
        } else {
            $scheme_code = $request['scheme_code'];
            $scheme_data = array();
            $scheme_details = SchemeDetails::where('scheme_code', $scheme_code)->get()->first();
            $scheme_history = SchemeHistory::where('scheme_code', $scheme_code)->get()->first();//Log::info(get_object_vars($scheme_history));Log::info($scheme_details);
            if(!empty($scheme_details) && !empty($scheme_history)) {
                //$scheme_details = $scheme_details->first();
                $scheme_data = array(
                    'scheme_code' => $scheme_details['scheme_code'],
                    'bse_scheme_code' => $scheme_details['bse_scheme_code'],
                    'scheme_name' => $scheme_details['scheme_name'],
                    'scheme_type' => $scheme_details['scheme_type'],
                    'scheme_priority' => $scheme_details['scheme_priority'],
                    'fund_type' => $scheme_history['fund_type'],
                    'fund_manager' => $scheme_history['fund_manager'],
                    'launch_date' => date('d-m-Y', strtotime($scheme_history['launch_date'])),
                    'asset_size' => $scheme_history['asset_size'],
                    'benchmark' => $scheme_history['benchmark'],
                    'exit_load' => $scheme_history['exit_load'],
                );
                return response()->json(['msg' => $scheme_data, 'status' => 1]);
            } else {
                return response()->json(['msg' => 'Something went wrong. Please refersh and try again.got', 'status' => 0]);
            }
        }
    }

    //function to update scheme details
    public function updateSchemeDetails(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'scheme_code' => 'required',
            'bse_scheme_code' => 'required',
            'scheme_name' => 'required',
            'scheme_type' => 'required',
            'scheme_priority' => 'required',
            'fund_type' => 'required',
            'fund_manager' => 'required',
            'launch_date' => 'required',
            'asset_size' => 'required',
            'benchmark' => 'required',
            'exit_load' => 'required',
            'scheme_status' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return response()->json(['msg' => 'Something went wrong. Please refersh and try again.', 'status' => 0]);
        } else {



            $scheme_code = $request['scheme_code'];
            $scheme_data = array();

            $existing_priorities = count(SchemeDetails::where('scheme_type',$request['scheme_type'])->where('scheme_priority',$request['scheme_priority'])->where('scheme_code','!=',$request['scheme_code'])->get());

            if($existing_priorities > 0){
                     return response()->json(['msg' => ['text'=>'Scheme With same priority already exists. Kindly Change the scheme priority of the existing scheme. '], 'status' => 0]);
            }else{
                $scheme_details = SchemeDetails::where('scheme_code', $scheme_code)->update(['bse_scheme_code' => $request['bse_scheme_code'], 'scheme_type' => $request['scheme_type'],'scheme_name' => $request['scheme_name'],'scheme_priority' => $request['scheme_priority'],'scheme_status'=>$request['scheme_status']]);
                $scheme_history = SchemeHistory::where('scheme_code', $scheme_code)->update(['scheme_code' => $request['scheme_code'],'name' => $request['scheme_name'],'launch_date' => date('Y-m-d', strtotime($request['launch_date'])),'fund_type' => $request['fund_type'],'fund_manager' => $request['fund_manager'],'asset_size' => $request['asset_size'],'benchmark' => $request['benchmark'],'exit_load' => $request['exit_load']]);

                if($scheme_details && $scheme_history) {
                    $scheme_data = array(
                        'scheme_code' => $scheme_code,
                        'bse_scheme_code' => $request['bse_scheme_code'],
                        'scheme_name' => $request['scheme_name'],
                        'scheme_type' => $request['scheme_type'],
                        'scheme_priority' => $request['scheme_priority'],
                        'fund_type' => $request['fund_type'],
                        'fund_manager' => $request['fund_manager'],
                        'launch_date' => $request['launch_date'],
                        'asset_size' => $request['asset_size'],
                        'benchmark' => $request['benchmark'],
                        'exit_load' => $request['exit_load'],
                        'scheme_status' => $request['scheme_status'],
                    );
                    return response()->json(['msg' => 1, 'scheme_data' => $scheme_data]);
                } else {
                    return response()->json(['msg' => 'Somethings went wrong. Please refersh and try again.', 'status' => 0]);
                }
            }
        }
    }


    /*Below Function was used to copy user 'pan' to personal details 'pan'*/

    // public function migratePan(){
    //     $users = User::all();

    //     foreach ($users as $user) {

    //         //echo $user['pan']."<br>";
    //        $user->personalDetails()->update(['pan'=>$user['pan']]);
    //     }
    // }



    public function updateAllotmentStatus(Request $request){
        $validator = \Validator::make($request->all(),[
            'allotment-file' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return response()->json(['msg' => 'Allotment File Not included']);
        } else {

            if($request->hasFile('allotment-file')){
                    $allotment_file = $request->file('allotment-file');
                    $allot_file_name = \Auth::user()->id.'-allot-'.date('d-m-Y').'-'.str_random().'.'. $allotment_file->getClientOriginalExtension();
                    $new_file = $request->file('allotment-file')->move(base_path() . '/public/files/AF/', $allot_file_name);
                    //$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());

                    $file_contents = file($new_file);
                    $allot_count = 0;
                    foreach ($file_contents as $line ) {
                        $allot = explode('|', $line);
                        //echo $allot[1];

                        $update_portfolio = PortfolioDetails::where('bse_order_no',$allot[1])->where('bse_payment_status','apr')->update(['folio_number'=>$allot[12],'initial_units_held'=>$allot[19],'units_held'=>$allot[19],'invested_nav'=>$allot[18],'portfolio_status'=>1,'bse_allot_date'=>date('Y-m-d',strtotime($allot[4]))]);

                        // http_response_code(500);
                        // dd($update_portfolio);
                        
                        if($update_portfolio){
                            $investment_id = PortfolioDetails::where('bse_order_no',$allot[1])->value('investment_id');
                            $portfolios = PortfolioDetails::where('investment_id',$investment_id)->where('portfolio_status',0)->count();
                            if($portfolios == 0){
                                InvestmentDetails::where('id',$investment_id)->update(['investment_status'=>1]);
                            }

                            $allot_count++;

                        }
                        


                    }

                    if (count($file_contents) == $allot_count) {
                        return response()->json(['msg'=>'success','response'=>'Allotment Status have been uploaded Successfully.']);
                    }else{
                        return response()->json(['msg'=>'success','response'=>'Allotment Status Partially Updated.']);
                    }   



                }else{
                        return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
                }

        }
    }



    public function updateSipAllotmentStatus(Request $request){
        $validator = \Validator::make($request->all(),[
            'allotment-file' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return response()->json(['msg' => 'Allotment File Not included']);
        } else {

            if($request->hasFile('allotment-file')){
                    $allotment_file = $request->file('allotment-file');
                    $allot_file_name = \Auth::user()->id.'-allot-'.date('d-m-Y').'-'.str_random().'.'. $allotment_file->getClientOriginalExtension();
                    $new_file = $request->file('allotment-file')->move(base_path() . '/public/files/AF/', $allot_file_name);
                    //$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());

                    $file_contents = file($new_file);
                    $allot_count = 0;

                    foreach ($file_contents as $line ) {
                        $allot = explode('|', $line);

                        //dd($allot);

                        //BSE Order => $allot[1]
                        //Folio Number => $allot[12]
                        //Initial Units held => $allot[19]
                        //units held => $allot[19]
                        //invested Nav => $allot[18]
                        //SIP Reg No => $allot[26]
                        //Bse Scheme code => $allot[5]
                        // Bse Allot date = $allot[4]

                        $month_old = Carbon::now()->subDays(30)->toDateString();
                        // if ($file_contents[3] == $line) {
                        //     http_response_code(500);
                        //     dd($allot[26]);
                        //     dd(PortfolioDetails::where('sip_reg_no','<>',null)->where('bse_scheme_code',$allot[5])->where('sip_reg_no',$allot[26])->where('investment_date','LIKE',date('Y-m',strtotime($allot[4])).'%')->get());
                        // }
                            $update_portfolio = PortfolioDetails::where('sip_reg_no','<>',null)->where('bse_scheme_code',$allot[5])->where('sip_reg_no',$allot[26])->where('investment_date','LIKE',date('Y-m',strtotime($allot[4])).'%')
                            ->update(['folio_number'=>$allot[12],'initial_units_held'=>$allot[19],'units_held'=>$allot[19],'invested_nav'=>$allot[18],'bse_order_no'=>$allot[1],'portfolio_status'=>1,'bse_order_status'=>0,'bse_allot_date'=>date('Y-m-d',strtotime($allot[4]))]);
                            // $allot_count++;
                        //}


                            // $update_portfolio = PortfolioDetails::where('sip_reg_no','<>',null)->where('bse_scheme_code',$allot[5])->where('sip_reg_no',$allot[26])
                            // ->update(['bse_order_no'=>$allot[1],'portfolio_status'=>1]);


                        //below is to check to see if the portfolio details are updated. If not looping through the portfolio details table with investment id and checking all the portfolio status has become '1'. If yes then updated the investment details entry as sucess('1'). Don't call me dumb for looping it again with out doing in the previous loop. you will know why i have done if you watch closesly. 


                            if($update_portfolio){
                                $investment_id = PortfolioDetails::where('bse_order_no',$allot[1])->where('bse_allot_date',date('Y-m-d',strtotime($allot[4])))->value('investment_id');
                                $portfolios = PortfolioDetails::where('investment_id',$investment_id)->where('portfolio_status',0)->count();
                                if($portfolios == 0){
                                    InvestmentDetails::where('id',$investment_id)->update(['investment_status'=>1]);
                                }

                                $allot_count++;

                            }
                        

                        


                    }


                    if (count($file_contents) == $allot_count) {
                        return response()->json(['msg'=>'success','response'=>'Allotment Status have been uploaded Successfully for all the entries.']);
                    }else if(count($file_contents) > $allot_count){
                        return response()->json(['msg'=>'success','response'=>'Allotment Status have been updated Partially. Refresh the page and check the Pending orders.']);
                    }else{
                        return response()->json(['msg'=>'success','response'=>'Please reload the page and see the pending orders.']);
                    }   



                }else{
                        return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
                }

        }
    }


//    public function updateRedemptionStatus(Request $request){
//        $validator = \Validator::make($request->all(),[
//            'redemption-file' => 'required',
//        ]);
//
//        if ($validator->fails()) {
//            //return $validator->errors();
//            return response()->json(['msg' => 'Redemtion File Not included']);
//        } else {
//
//            if($request->hasFile('redemption-file')){
//                    $redeem_file = $request->file('redemption-file');
//                    $redeem_file_name = \Auth::user()->id.'-redeem-'.date('d-m-Y').'-'.str_random().'.'. $redeem_file->getClientOriginalExtension();
//                    $new_file = $request->file('redemption-file')->move(base_path() . '/public/files/RF/', $redeem_file_name);
//                    //$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());
//
//                    $file_contents = file($new_file);
//
//                    foreach ($file_contents as $line ) {
//                        $redeem = explode('|', $line);
//
//
//
//                        $withdraw_entry = WithdrawDetails::where('bse_order_no',$redeem[1])->get();
//                        $withdraw_count = 0;
//
//                        foreach ($withdraw_entry as $withdraw) {
//                            $withdraw_units = (float)$redeem[19];
//
//
//                            if ($withdraw->full_amount == '0') {
//
//
//                                $portfolio_details = PortfolioDetails::where('user_id',$withdraw->user_id)
//                                ->where('scheme_code',$withdraw->scheme_code)
//                                ->where('portfolio_status',1)
//                                ->get();
//
////                                dd($portfolio_details, $redeem[19]);
//
//                                foreach ($portfolio_details as $portfolio_detail) {
//                                    if($portfolio_detail->units_held == $redeem[19]) {
//                                        $withdraw_units = 0;
//                                        $portfolio_detail->units_held = 0;
//                                        $portfolio_detail->amount_invested = 0;
//                                        $portfolio_detail->portfolio_status = 2;
//                                        $portfolio_detail->save();
//                                        $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1]);
//                                        $withdraw_count++;
//                                    } elseif($portfolio_detail->units_held < $redeem[19]) {
//                                        //if the units held less than  withdraw units
//                                        $withdraw_units -= $portfolio_detail->units_held;
//                                        $portfolio_detail->units_held = 0;
//                                        $portfolio_detail->amount_invested = 0;
//                                        $portfolio_detail->portfolio_status = 2;
//                                        $portfolio_detail->save();
//                                        $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1]);
//                                        $withdraw_count++;
//                                    } elseif($portfolio_detail->units_held > $redeem[19]) {
//                                        //if the units held is greater than the units to be withdrawn
//                                        // dd($portfolio_detail->units_held,$redeem[19]);
//                                        $portfolio_detail->units_held -= $redeem[19];
//                                        $portfolio_detail->amount_invested -= ($redeem[19] * $redeem[18]);
//                                        //$withdraw_units = 0;
//                                        $portfolio_detail->save();
//                                        $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1]);
//                                        $withdraw_count++;
//                                    }
//                                }
//
//                            }
//
//                            if ($withdraw->full_amount == '1') {
//                                $update_portfolio = PortfolioDetails::where('user_id',$withdraw->user_id)
//                                ->where('scheme_code', $withdraw->scheme_code)
//                                ->where('portfolio_status', 1)
//                                ->update(['units_held'=> 0,'amount_invested'=>0,'portfolio_status'=>2]);
//
//                                $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1]);
//
//                                if ($update_portfolio && $update_withdraw) {
//                                    $withdraw_count++;
//                                }
//                            }
//
//
//                        }
//                        // dd(count($file_contents),$withdraw_count);
//
//
//                        //die();
//
//                        //$redeem[18] ==> WIthdraw NAV
//                        //$redeem[19] ==> WIthdraw unit
//                        //$redeem[20] ==> WIthdraw amount
//
//                        // PortfolioDetails::where('bse_order_no',$allot[1])->update(['folio_number'=>$allot[12],'initial_units_held'=>$allot[19],'units_held'=>$allot[19],'invested_nav'=>$allot[18],'portfolio_status'=>1]);
//
//                        // $port_inv_id = PortfolioDetails::where('bse_order_no',$allot[1])->pluck('investment_id');
//
//                        // $not_invested_port = PortfolioDetails::where('investment_id',$port_inv_id[0])->where('portfolio_status',0)->count();
//
//                        // if ($not_invested_port == 0) {
//                        //     InvestmentDetails::where('id',$port_inv_id[0])->update(['investment_status'=>1]);
//                        // }
//
//
//                    }
//                    if ($withdraw_count > 0) {
//                        return response()->json(['msg'=>'success','response'=>'Redemption Status Updated Succesfully']);
//                    }else{
//                        return response()->json(['msg'=>'failure','response'=>'Redemption Status Update Failed']);
//                    }
//
//
//                }else{
//                        return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
//                }
//
//        }
//    }


    public function updateRedemptionStatus(Request $request){



        /*
         * IMPORTANT NOTE :
         *
         * 1. Portfolio STATUS is set to '1' for both partial Withdrawn and full active entries. The difference can be found by comparing the `initial_amount_invested` and `amount_invested` columns
         * and also `units_held` and `initial_units_held`
         *
         * PORTFOLIO STATUS Definitions
         *
         * 1 - Active and partially redeemed;
         * 2 - Fully Withdrawn
         * 3 - Cancelled.
         *
         *
         * */


        $validator = \Validator::make($request->all(),[
            'redemption-file' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return response()->json(['msg' => 'Redemption File Not included']);
        } else {

            if($request->hasFile('redemption-file')){
                $redeem_file = $request->file('redemption-file');
                $redeem_file_name = \Auth::user()->id.'-redeem-'.date('d-m-Y').'-'.str_random().'.'. $redeem_file->getClientOriginalExtension();
                $new_file = $request->file('redemption-file')->move(base_path() . '/public/files/RF/', $redeem_file_name);
                //$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());

                $file_contents = file($new_file);


                $orderNo = [];

                foreach ($file_contents as $line ) {

                    $redeem = explode('|', $line);



                    $withdraw_entry = WithdrawDetails::where('bse_order_no', $redeem[1])->get();
                    $withdraw_count = 0;




                    foreach ($withdraw_entry as $withdraw) {
//                            dd($withdraw);
                        $withdraw_units = (float)$redeem[19];
                        $save_portfolio = 0;
                        if ($withdraw->full_amount == '0') {


                            $portfolio_details = PortfolioDetails::where('user_id',$withdraw->user_id)
                                ->where('scheme_code',$withdraw->scheme_code)
                                ->where('portfolio_status',1)
                                ->get();

//                                dd(count($portfolio_details));

                            foreach ($portfolio_details as $portfolio_detail) {

//                                    dd($withdraw_units);
                                if($withdraw_units != 0){


                                    if($portfolio_detail->units_held == $withdraw_units) {
//                                            dd('1');
                                        //Checking wthether the available portfolio units is equal to withdraw units. in case it is "YES". So changing the portfolio status to 2 (Fully withdrawn)
                                        $withdraw_units = 0;
                                        $portfolio_detail->units_held = 0;
                                        $portfolio_detail->amount_invested = 0;
                                        $portfolio_detail->portfolio_status = 2;
                                        $save_portfolio = $portfolio_detail->save();
                                    } elseif($portfolio_detail->units_held < $withdraw_units) {
//                                            dd($portfolio_detail->units_held, $withdraw_units);

                                        //if the units held less than  withdraw units
                                        $withdraw_units -= $portfolio_detail->units_held;
                                        $portfolio_detail->units_held = 0;
                                        $portfolio_detail->amount_invested = 0;
                                        $portfolio_detail->portfolio_status = 2;
                                        $save_portfolio = $portfolio_detail->save();
                                    } elseif($portfolio_detail->units_held > $withdraw_units) {
//                                            dd(3);
                                        //if the units held is greater than the units to be withdrawn
                                        $portfolio_detail->units_held -= $withdraw_units;
                                        $portfolio_detail->amount_invested -= ($withdraw->withdraw_units * $redeem[18]);
                                        $withdraw_units = 0;
                                        $portfolio_detail->portfolio_status = 1;
                                        $save_portfolio = $portfolio_detail->save();
                                    }
                                }
                            }

                            $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1]);

                            if ($save_portfolio && $update_withdraw) {
                                $withdraw_count++;
                            }

                        }

                        if ($withdraw->full_amount == '1') {
                            $update_portfolio = PortfolioDetails::where('user_id',$withdraw->user_id)
                                ->where('scheme_code', $withdraw->scheme_code)
                                ->where('portfolio_status', 1)
                                ->update(['units_held'=> 0,'amount_invested'=>0,'portfolio_status'=>2]);

                            $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1]);

                            if ($update_portfolio && $update_withdraw) {
                                $withdraw_count++;
                            }
                        }


                    }




                    //die();

                    //$redeem[18] ==> WIthdraw NAV
                    //$redeem[19] ==> WIthdraw unit
                    //$redeem[20] ==> WIthdraw amount



                }

//                    dd($orderNo);

                if (count($file_contents) == $withdraw_count) {
                    return response()->json(['msg'=>'success','response'=>'Redemption Status Updated Succesfully']);
                }else if((count($file_contents) > $withdraw_count) && ($withdraw_count != 0)){
                    return response()->json(['msg'=>'success','response'=>'Redemption Status partially Updated']);
                }else{
                    return response()->json(['msg'=>'failure','response'=>'Redemption Status update Failed']);

                }

            }else{
                return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
            }

        }
    }


    public function addManualPortfolio(Request $request){
        $validator = \Validator::make($request->all(),[
            'user_id' => 'required',
            'inv_amount' => 'required',
            'inv_type' => 'required',
            'port_type' => 'required',
            'inv_date' => 'required',
            'payment_type' => 'required',
            'port_details' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
            //return response()->json(['msg' => 'Cannot Be Added Rightnow.']);
        } else {
            //dd($request['port_details']);
            $inv_type;
            $port_type;
            if ($request['inv_type'] == 'monthly') {
                $inv_type = 0;
            }

            if ($request['inv_type'] == 'onetime') {
                $inv_type = 2;
            }

            $date = date('Y-m-d',strtotime($request['inv_date']));

            if ($request['port_type'] == 'Conservative') {
                    $port_type = 1;
                    $pp_type = 'debt';
            }

            if ($request['port_type'] == 'Moderate') {
                $port_type = 2;
                $pp_type = 'bal';
            }

            if ($request['port_type'] == 'Aggressive') {
                $port_type = 3;
                $pp_type = 'eq';
            }

            if ($request['port_type'] == 'Tax Saver') {
                $port_type = 4;
                $pp_type = 'ts';
            }

            $scheme_details = SchemeDetails::pluck('scheme_status','scheme_code')->toArray();
            //dd($scheme_details,)
            //dd($scheme_details,array_key_exists('118191', $scheme_details));


            //dd(count(json_decode(stripslashes($request['port_details']))));


            $save_inv = InvestmentDetails::create([
                'user_id' => $request['user_id'],
                'investment_amount' => $request['inv_amount'],
                'investment_type' => $inv_type,
                'portfolio_type' => $port_type,
                'investment_status' => 1,
                'investment_date' => $date,
                'payment_type' => $request['payment_type'],
            ]);
            if ($save_inv) {
                //dd($investment);
                $port_details = json_decode(stripslashes($request['port_details']));

                $count = 0;
                $port_count = count($port_details);

                foreach ($port_details as $portfolio) {

                    $portfolio_details = new PortfolioDetails();
                    $portfolio_details->user_id = $request['user_id'];
                    $portfolio_details->investment_id = $save_inv->id;
                    $portfolio_details->folio_number = $portfolio->folio_number;

                    //dd(array_key_exists((int) $portfolio->scheme_code, $scheme_details),$portfolio->scheme_code, $scheme_details);
                    if (array_key_exists((int) $portfolio->scheme_code, $scheme_details)) {
                        $portfolio_details->scheme_code = $portfolio->scheme_code;    
                    }else{

                        if ($port_count == 0) {
                            return response()->json(['msg' => 'success','response'=>'Cannot Add the Portfolio Details right now.']);
                        }else{
                            $this->removeInvestments($save_inv->id);
                            return response()->json(['msg' => 'success','response'=>'Scheme code doesn\'t match']);
                        }
                        
                    }
                    
                    $portfolio_details->units_held = $portfolio->units_held;
                    $portfolio_details->amount_invested = $portfolio->inv_amount;
                    $portfolio_details->initial_amount_invested = $portfolio->inv_amount;
                    $portfolio_details->initial_units_held = $portfolio->units_held;

                    $portfolio_details->portfolio_type = $pp_type;
                    $portfolio_details->portfolio_status = 1;
                    $portfolio_details->invested_nav = $portfolio->invested_nav;
                    $portfolio_details->investment_date = $date;
                    $portfolio_details->bse_allot_date = $date;

                    if ($portfolio_details->save()) {
                        $count++;
                    }


                }   

                if ($port_count == $count) {
                    return response()->json(['msg' => 'success','response'=>'Portfolio Succesfully Added. Kindly reload the page and check the portfolio']);
                }else{
                    
                    $this->removeInvestments($save_inv->id);
                    return response()->json(['msg' => 'success','response'=>'Cannot Add the Portfolio Details right now.']);
                }
            }
        }
    }

    public function removeInvestments($inv_id){
        PortfolioDetails::where('investment_id',$inv_id)->delete();
        InvestmentDetails::where('id',$inv_id)->delete();
    }

    public function fileUpload(Request $request)
    {
        // dd($request['user_id']);
        if ($request['user_id'] == '') {
            return response()->json(['msg' => 0]);
        }else{
            $type = '';
            $col_name = '';
            if ($request['type'] == 'pan') {
                $type = 'pan';
                $col_name = 'pan_link';
            }else if ($request['type'] == 'apf') {
                $type = 'apf';
                $col_name = 'apf_link';
            }else if ($request['type'] == 'apb') {
                $type = 'apb';
                $col_name = 'apb_link';
            }else if ($request['type'] == 'sig') {
                $type = 'sig';
                $col_name = 'sig_link';
            }else if ($request['type'] == 'video') {
                $type = 'video';
                $col_name = 'video_link';

                $redeem_file = $request->file('file');
                $redeem_file_name = $type.'_'.str_random().'.'. $redeem_file->getClientOriginalExtension();
                $new_file = $request->file('file')->move(base_path() . "/public/ekycvideo/", $redeem_file_name);
                $save = ekyc::where('user_id',$request['user_id'])
                                ->update([$col_name => $redeem_file_name]);
                if ($save == 1) {
                    return response()->json(['msg' => 1,'type' => $type, 'file_name' => $redeem_file_name]);
                }else{
                    return response()->json(['msg' => 3]);
                }
            }else if ($request['type'] == 'cc') {
                $type = 'cc';
                $col_name = 'cc_link';

                $redeem_file = $request->file('file');
                $redeem_file_name = $type.'_'.str_random().'.'. $redeem_file->getClientOriginalExtension();
                $new_file = $request->file('file')->move(base_path() . "/public/files/cc/", $redeem_file_name);
                $save = ekyc::where('user_id',$request['user_id'])
                            ->update([$col_name => $redeem_file_name]);
                if ($save == 1) {
                    return response()->json(['msg' => 1,'type' => $type, 'file_name' => $redeem_file_name]);
                }else{
                    return response()->json(['msg' => 3]);
                }
            }else if ($request['type'] == 'mandate') {
                $type = 'mandates';
                $col_name = 'mandate_link';

                $redeem_file = $request->file('file');
                $redeem_file_name = $type.'_'.str_random().'.'. $redeem_file->getClientOriginalExtension();
                $new_file = $request->file('file')->move(base_path() . "/public/files/mandates/", $redeem_file_name);
                $save = ekyc::where('user_id',$request['user_id'])
                            ->update([$col_name => $redeem_file_name]);
                if ($save == 1) {
                    return response()->json(['msg' => 1,'type' => $type, 'file_name' => $redeem_file_name]);
                }else{
                    return response()->json(['msg' => 3]);
                }
            }else if ($request['type'] == 'ekyc') {
                $type = 'ekyc';
                $col_name = 'ekyc_form_link';
            }else if ($request['type'] == 'aof') {
                $type = 'aof';
                $col_name = 'aof_link';
            }else if ($request['type'] == 'mnt') {
                $type = 'mandate';
                $col_name = 'mandate_hc_link';
            }else {
                return response()->json(['msg' => 2]);
            }
            $redeem_file = $request->file('file');
            $redeem_file_name = $type.'_'.str_random().'.'. $redeem_file->getClientOriginalExtension();
            $new_file = $request->file('file')->move(base_path() . "/public/storage/".$type."/", $redeem_file_name);
            $save = ekyc::where('user_id',$request['user_id'])
                            ->update([$col_name => $redeem_file_name]);
            if ($save == 1) {
                return response()->json(['msg' => 1,'type' => $type, 'file_name' => $redeem_file_name]);
            }else{
                return response()->json(['msg' => 3]);
            }
        }
    }

    public function createUser(){
        $user_creation_status = User::create([
            'name' => 'Udhayakumar T',
            'email' => 'udhayakumar8195@gmail.com',
            'password' => bcrypt('udhaya8195'),
            'mobile' => '8778173764',
            //'pan' => $data['pan'],
            'acc_type' => '0',
            'p_check' => '0',
            'b_check' => 0,
            'kyc_check' => '0',
            'otp_check' => 1,
            //'otp' => bcrypt($otp_value),
            //'otp_start_time' => $otp_start_time,
        ]);

        //dd($user_creation_status);
        if($user_creation_status) {

            $nach_details = new NachDetails();
            $nach_details->user_id = $user_creation_status->id;
            
            //$create_nach = \Auth::user()->nachDetails()->save(new NachDetails());
              $create_nach = $nach_details->save();
            //have to be tested out cause otp cannot be sent from localhost.
            if ($create_nach) {

                $notification = new Notifications();
                $notification->description = "You can start activating your Account from the Account Activate tab.";
                $notification->date = date('Y-m-d');
                $notification->user_id = $user_creation_status->id;

                $save_notification = $notification->save();

                if ($save_notification) {

                        $ekyc = new ekyc();
                        $ekyc->user_id = $user_creation_status->id;
                        $save_ekyc = $ekyc->save();

                        if ($save_ekyc) {
                          //unset($user_creation_status);
                          //$user_creation_status = true;
                          return $user_creation_status;
                        }
                        
                }
                
            }
            
            
        }
    }

    public function addBankCode(){
        $files = file(public_path('/files/bank_code.txt'));
        

        foreach($files as $file){
            $bank = explode('|', $file);
            // dd($bank);
            $bank_info = new BankInfo;
            $bank_info->bank_name = $bank[1];
            $bank_info->bank_id = $bank[2];
            $bank_info->mode = $bank[0];
            $bank_info->save();


        }
    }

    public function updatePersonalInfo(Request $request)
    {
        $panVal = PersonalDetails::where('pan',$request['srch-per-pan'])->where('user_id','!=',$request['user_id'])->get()->toArray();
        $aadharVal = PersonalDetails::where('aadhar',$request['srch-per-aadhar'])->where('user_id','!=',$request['user_id'])->get()->toArray();

        if (count($panVal) > 0) {
                return response()->json(['msg'=>3, 'info'=>'PAN already Taken.']);
        }else if (count($aadharVal) > 0) {
                return response()->json(['msg'=>4, 'info'=>'Aadhar number already Taken.']);
        }else{
            $update = PersonalDetails::where('user_id',$request['user_id'])
                                            ->update(['f_name'=>$request['srch-per-name'],
                                                    'pan'=>$request['srch-per-pan'],
                                                    'aadhar'=>$request['srch-per-aadhar'],
                                                    'nationality'=>$request['srch-per-nat'],
                                                    'f_s_name'=>$request['srch-per-fsn'],
                                                    'b_city'=>$request['srch-per-pob'],
                                                    'mothers_name'=>$request['srch-per-mn'],
                                                    'dob'=>date('Y-m-d', strtotime($request['srch-per-dob'])),
                                                    'sex'=>$request['sex'],
                                                    'mar_status'=>$request['mar-status']]);
            if ($update) {
                $getmobile = User::where('mobile',$request['srch-per-mob'])->where('id','!=',$request['user_id'])->get()->toArray();
                if (count($getmobile) <= 0) {
                    $mobileUpdate = User::where('id',$request['user_id'])->update(['mobile'=>$request['srch-per-mob']]);
                    if ($mobileUpdate) {
                        return response()->json(['msg'=>1]);
                    }else{
                        return response()->json(['msg'=>0]);
                    }            
                }else{
                    return response()->json(['msg'=>2, 'info'=>'Mobile number already Taken.']);
                }
            }else{
                return response()->json(['msg'=>0]);
            }
            
        }
    }

    public function updateAddressInfo(Request $request)
    {
        $update = PersonalDetails::where('user_id',$request['user_id'])
                                    ->update(['address'=>$request['srch-per-addr'],
                                            'b_city'=>$request['srch-per-city'],
                                            'pincode'=>$request['srch-per-pin']]);
        if ($update) {
            return response()->json(['msg' => 1]);
        }else{
            return response()->json(['msg' => 0]);
        }
    }

    public function updateNomineeInfo(Request $request)
    {
        $updateNominee1 = NomineeDetails::where('user_id',$request['user_id'])
                                            ->where('nomi_type',1)
                                            ->update(['nomi_name'=>$request['srch-per-nom1-name'],
                                                    'nomi_relationship'=>$request['srch-per-nom1-rel'],
                                                    'nomi_dob'=>date('Y-m-d',strtotime($request['srch-per-nom1-dob'])),
                                                    'nomi_holding'=>$request['srch-per-nom1-per']]);

        $updateNominee2 = NomineeDetails::where('user_id',$request['user_id'])
                                            ->where('nomi_type',2)
                                            ->update(['nomi_name'=>$request['srch-per-nom2-name'],
                                                    'nomi_relationship'=>$request['srch-per-nom2-rel'],
                                                    'nomi_dob'=>date('Y-m-d',strtotime($request['srch-per-nom2-dob'])),
                                                    'nomi_holding'=>$request['srch-per-nom2-per']]);

        $updateNominee3 = NomineeDetails::where('user_id',$request['user_id'])
                                            ->where('nomi_type',3)
                                            ->update(['nomi_name'=>$request['srch-per-nom3-name'],
                                                    'nomi_relationship'=>$request['srch-per-nom3-rel'],
                                                    'nomi_dob'=>date('Y-m-d',strtotime($request['srch-per-nom3-dob'])),
                                                    'nomi_holding'=>$request['srch-per-nom3-per']]);
        if ($updateNominee3 && $updateNominee2 && $updateNominee1) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }
    }

    public function updateBankInfo(Request $request)
    {
        $acc_type = '';
        if ($request['savings']) {
            $acc_type = 'savings';
        }else{
            $acc_type = 'current';
        }
        $updateBank = BankDetails::where('user_id',$request['user_id'])
                                    ->update(['acc_no'=>$request['srch-per-bank1-accno'],
                                            'ifsc_code'=>$request['srch-per-bank1-ifsc'],
                                            'bank_id'=>$request['srch-per-bank1-name'],
                                            'branch_name'=>$request['srch-per-branch1-name'],
                                            'address'=>$request['bank1-addr'],
                                            'city'=>$request['srch-per-bank1-city'],
                                            'acc_type'=>$acc_type]);
        if ($updateBank) {
            User::where('id',$request['user_id'])->update(['b_check'=>1]);
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }
    }

    public function updatePriorityInfo(Request $request)
    {
        if ($request['status'] == 2) {
            $update = nachDetails::where('user_id',$request['user_id'])
                                    ->update(['priority'=>0]);
            $userUpdate = User::where('id',$request['user_id'])
                                    ->update(['handle_by'=>0]);
        }else if ($request['status'] == 1) {
            $update = nachDetails::where('user_id',$request['user_id'])
                                    ->update(['priority'=>1]);
            $userUpdate = User::where('id',$request['user_id'])
                                    ->update(['handle_by'=>1]);
        }
    }

    public function nachComplete(Request $request)
    {
        $nachUpdate = nachDetails::where('user_id', $request['user_id'])
                                ->update(['nach_status'=>1]);
        if ($nachUpdate) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }

    }

    public function superLogin($id){

        
        $auth  = \Auth::loginUsingId($id, true);
        if ($auth) {
            return redirect('/home');
        }

    }

    public function addMandateLink(Request $request)
    {

        $nachUpdate = nachDetails::where('user_id', $request['user_id'])
                                ->update(['mandate_link'=>json_decode($request['mandate_link'])]);
        if ($nachUpdate) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }
    }

    public function bpoUpdate(Request $request)
    {
        $check = BpoStatus::where('user_id',$request['user_id'])->get();
        if (count($check) == 0) {
            $insert = BpoStatus::insert(['user_id'=>$request['user_id'],'user_status'=>$request['val']]);
            if ($insert) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0]);
            }
        }else{
            $update = BpoStatus::where('user_id',$request['user_id'])->update(['user_status'=>$request['val']]);
            if ($update) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0]);
            }
        }
    }

    public function filterBystatus(Request $request)
    {   
        if ($request['filter'] == 0) {
            $bpoids = User::where('handle_by',0)->pluck('id');
            $nach_details = NachDetails::whereIn('user_id',$bpoids)->get();
        }else{
            $filter_id = BpoStatus::where('user_status',$request['filter'])->pluck('user_id');
            $nach_details = NachDetails::whereIn('user_id',$filter_id)->where('nach_status',0)->get();
        }
        foreach ($nach_details as $nach) {

            if ($nach->user->p_check == "0") {
                $nach['user_pan'] = '';    
            }else{
                $nach['user_pan'] = $nach->user->personalDetails->pan;
            }
            
            $nach['user_name'] = $nach->user->name;
            $nach['user_mobile'] = $nach->user->mobile;
            $nach['user_email'] = $nach->user->email;
        }

        return response()->json(['msg'=>1,'data'=>$nach_details]);

    }

    public function investmenStatus()
    {
        $investments = InvestmentDetails::where('investment_type',0)->where('investment_status',0)->get()->toArray();
        foreach ($investments as $value) {
            $portfolio = PortfolioDetails::where('investment_id',$value['id'])->whereIn('portfolio_status',[1,0])->get()->toArray();
            $count = count($portfolio);
            // echo "id-".$value['id']."  "."Count-".$count."\n";
            if ($count == 0) {
                $update= InvestmentDetails::where('id',$value['id'])->update(['investment_status'=>1]);
            }
        }
        dd('done');
    }

    public function removeUser()
    {
        // $id = NachDetails::where('priority',1)->pluck('user_id');
        // $update = User::whereIn('id',$id)->update(['handle_by'=>'1']);
        $remove = NachDetails::all()->pluck('user_id');
        $users = User::whereNotIn('id',$remove)->delete();
        dd($users);
    }

    public function changeDate()
    {
        $data = PortfolioDetails::where('bse_allot_date',NULL)->get()->toArray();
        // dd($data);
        foreach ($data as $value) {
            $update = PortfolioDetails::where('id',$value['id'])->update(['bse_allot_date'=>$value['investment_date']]);
        }
        dd('work done!!!!');
    }

}

