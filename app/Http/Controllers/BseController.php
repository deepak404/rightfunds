<?php

namespace App\Http\Controllers;
use App\NachDetails;
use App\Notifications;
use App\ekyc;

use Illuminate\Http\Request;

use App\Http\Requests;

use Artisaninweb\SoapWrapper\SoapWrapper;
use SoapClient;
use SoapHeader;
use App\InvestmentDetails;
use App\PortfolioDetails;
use App\User;
use App\BankInfo;
use Carbon\Carbon;
//use App\Soap\Request\GetConversionAmount;
//use App\Soap\Response\GetConversionAmountResponse;

class BseController extends Controller
{
  /**
   * @var SoapWrapper
   */
  protected $soapWrapper;
  protected $pass_key = 'abcdef7895';
  protected $password = 'Pwm$2017';
  protected $response_key;
  protected $order_no;

  /**
   * SoapController constructor.
   *
   * @param SoapWrapper $soapWrapper
   */
  public function __construct()
  {
      //$this->pass_key = $passkey;
  }

  public function test(){


    $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
        'soap_version' => SOAP_1_2, // !!!!!!!
        'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
      ));

    $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword',true);
    $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

    $client->__setSoapHeaders([$actionHeader,$toHeader]);

        $login_params = array(
          'UserId' => '1245601',
          'MemberId' => '12456',
          'Password' => 'Pwm$2017',
          'PassKey' => '1234569870',

        );

      $result = $client->getPassword($login_params);

      //dd($client->__getLastRequestHeaders());

      echo $result->getPasswordResult;

    
  }


  public function getPassword(){


    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
       <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
       <wsa:To>http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc</wsa:To>
       </soap:Header>
       
       <soap:Body>
          <bses:getPassword>
             <!--Optional:-->
             <bses:UserId>1245601</bses:UserId>
             <!--Optional:-->
             <bses:Password>'.$this->password.'</bses:Password>
             <!--Optional:-->
             <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
          </bses:getPassword>
       </soap:Body>
    </soap:Envelope>';

    //echo "hello";


    $result = $this->makeSoapCall($soap_request);
    // var_dump($result);
    // die();
    $result = explode('http://bsestarmf.in/MFOrderEntry/getPasswordResponse', $result);
    $response = explode('|', $result[1]);

    //var_dump($response);
    //echo $response[1];

    ///preg_match('/<getPasswordResult>-(.*?)-<getPasswordResult>/', $result, $pass_response);


   // dd($pass_response);

    $pwd_response =  $response[0];
    $this->response_key = $response[1];

    //echo $this->response_key;

  }

  

  public function placeOrder(){

    //dd($this->response_key);



    /*
          Order Response will be in this order. Kindly don't go through the BSE web file documentation.
          That will only confuse you. 

          The order response will be in this format.

          TRANSACTION_CODE|UNIQUE REFERENCE NUMBER|BSE ORDER NO|USER ID|MEMBER ID|CLIENT CODE|BSE REMARKS|SUCCESS FLAG

          UNIQUE REFERENCE NUMBER = $unique_ref_no //this should be unique for each order. This will be tracking for the order.
          BSE ORDER NO //This is generated by BSE
          TRANSACTION CODE // NEW||MOD||CXL -- New order || Modification || Cancel.
          SUCCESS FLAG // 0 == ORDER SUCCESS and 1 = ORDER FAILURE *What a Brilliant thing by BSE* 

    */


    $transaction_no = '';
    $int_ref_no = '12345678901234567890';
    $response_key = explode('</getPasswordResult></getPasswordResponse></s:Body></s:Envelope>', $this->response_key);



    $date = date('Y-m-d');
    $date = explode('-', $date);
    $date = $date[0].$date[1].$date[2];
    $time = explode('.', microtime());
    $time = substr($time[1], 0 ,6);

    $unique_ref_no = $date."12456".$time;


    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
       <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
       <wsa:To>http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc</wsa:To>
       </soap:Header>
   <soap:Body>
      <bses:orderEntryParam>
         <!--Optional:-->
         <bses:TransCode>NEW</bses:TransCode>
         <!--Optional:-->
         <bses:TransNo>'.$unique_ref_no.'</bses:TransNo>
         <!--Optional:-->
         <bses:OrderId></bses:OrderId>
         <!--Optional:-->
         <bses:UserID>1245601</bses:UserID>
         <!--Optional:-->
         <bses:MemberId>12456</bses:MemberId>
         <!--Optional:-->
         <bses:ClientCode>CBCPR3555E</bses:ClientCode>
         <!--Optional:-->
         <bses:SchemeCd>32</bses:SchemeCd>
         <!--Optional:-->
         <bses:BuySell>P</bses:BuySell>
         <!--Optional:-->
         <bses:BuySellType>FRESH</bses:BuySellType>
         <!--Optional:-->
         <bses:DPTxn>P</bses:DPTxn>
         <!--Optional:-->
         <bses:OrderVal>1000.00</bses:OrderVal>
         <!--Optional:-->
         <bses:Qty></bses:Qty>
         <!--Optional:-->
         <bses:AllRedeem>N</bses:AllRedeem>
         <!--Optional:-->
         <bses:FolioNo></bses:FolioNo>
         <!--Optional:-->
         <bses:Remarks></bses:Remarks>
         <!--Optional:-->
         <bses:KYCStatus>Y</bses:KYCStatus>
         <!--Optional:-->
         <bses:RefNo>'.$int_ref_no.'</bses:RefNo>
         <!--Optional:-->
         <bses:SubBrCode></bses:SubBrCode>
         <!--Optional:-->
         <bses:EUIN>E173580</bses:EUIN>
         <!--Optional:-->
         <bses:EUINVal>N</bses:EUINVal>
         <!--Optional:-->
         <bses:MinRedeem>N</bses:MinRedeem>
         <!--Optional:-->
         <bses:DPC>N</bses:DPC>
         <!--Optional:-->
         <bses:IPAdd></bses:IPAdd>
         <!--Optional:-->
         <bses:Password>'.$response_key[0].'</bses:Password>
         <!--Optional:-->
         <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
         <!--Optional:-->
         <bses:Parma1></bses:Parma1>
         <!--Optional:-->
         <bses:Param2></bses:Param2>
         <!--Optional:-->
         <bses:Param3></bses:Param3>
      </bses:orderEntryParam>
   </soap:Body>
</soap:Envelope>';

//dd($soap_request);

    $result = $this->makeSoapCall($soap_request);
    //echo $result;

    $response = explode('http://bsestarmf.in/MFOrderEntry/orderEntryParamResponse', $result);
    $response = explode('|', $response[1]);

    $trans_code = explode('</a:Action></s:Header><s:Body><orderEntryParamResponse xmlns="http://bsestarmf.in/"><orderEntryParamResult>', $response[0]);
    $order_response = explode('</orderEntryParamResult></orderEntryParamResponse></s:Body></s:Envelope>', $response[7]);

    $order_response = array(
        'trans_code' => $trans_code[1],
        'unique_ref_no' => $response[1],
        'order_no' =>$response[2],
        'client_code' => $response[5],
        'remarks' =>$response[6],
        'order_status' => $order_response[0],
      );

    return $order_response;
    

  }

  public function makeSoapCall($soap_request){

    //dd($soap_request);
    $header = array(
        "Content-type: application/soap+xml;charset=\"utf-8\"",
        "Accept: application/soap+xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "Content-length: ".strlen($soap_request),
    );
    $soap_do = curl_init();
    curl_setopt($soap_do, CURLOPT_URL,"http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc?singleWsdl" );
    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($soap_do, CURLOPT_POST,           true );
    curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
    curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
    $result = curl_exec($soap_do);
    //dd($result);
    return $result;
  }


    public function paymentGatewayPassword(){
          $client = new SoapClient("https://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc?singleWsdl",array(
                  'soap_version' => SOAP_1_2, // !!!!!!!
                  'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
                  'trace' => 1,
                ));

          $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://tempuri.org/IStarMFPaymentGatewayService/GetPassword',true);
          $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic');

          $client->__setSoapHeaders([$actionHeader,$toHeader]);

              $login_params = array(
                
                'Param' => array(
                  'MemberId' => '12456',
                  'Password' => 'Pwm$2017',
                  'PassKey' => '1234569870',
                  'UserId' => '1245601',
                )

              );

            $result = $client->getPassword($login_params);
//            dd($result);
            $result = $result->GetPasswordResult->Status.'|'.$result->GetPasswordResult->ResponseString;
            //dd($result);

            $response = explode('|', $result);

            // dd($response);
            $pwd_response =  $response[0];
            $this->response_key = $response[1];

            // echo $this->response_key;

    }

    public function PaymentGatewayIntegration($id){
//      dd('hello')
      $this->paymentGatewayPassword();
      $investment_id = $id;

      $investment_details = InvestmentDetails::where('id',$investment_id)->get();
      $portfolio_details = PortfolioDetails::where('investment_id',$investment_id)->get();
      
      $amount_invested = $portfolio_details->sum('amount_invested');
      $order_nos = $portfolio_details->pluck('bse_order_no')->toArray();

      // dd($order_nos);
      

      $client = new SoapClient("https://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc?singleWsdl",array(
                  'soap_version' => SOAP_1_2, // !!!!!!!
                  'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
                  'trace' => 1,
                ));

      $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://tempuri.org/IStarMFPaymentGatewayService/PaymentGatewayAPI',true);
      $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic');

      $client->__setSoapHeaders([$actionHeader,$toHeader]);
      $bank = \Auth::user()->bankDetails()->get()->toArray();
      $bank = $bank[0];
      // $bank['bank_id'] = stripslashes($bank['bank_id']);
      $bank_id = $bank['bank_id'];
      // dd($bank_id);
      $payment_mode = BankInfo::where('bank_id',$bank['bank_id'])->pluck('mode');
      // dd($payment_mode);

      $bank_info = BankInfo::all();

      $params = array(
          'Param' => array(
              'AccNo' => $bank['acc_no'],
              'BankID' => $bank['bank_id'],
              'ClientCode' => \Auth::user()->personalDetails->pan,
              'EncryptedPassword' => $this->response_key,
              'IFSC' => $bank['ifsc_code'],
              'LogOutURL' => 'https://www.rightfunds.com/payment_response/'.$id,
              'MemberCode' => '12456',
              'Mode' => $payment_mode[0],
              'Orders' => array('string'=> $order_nos),
              'TotalAmount' => $amount_invested,

          )
      );

      // dd($params);

       $result = $client->PaymentGatewayAPI($params);
      echo $result->PaymentGatewayAPIResult->ResponseString;

    }

    public function paymentResponse($id){
          $this->paymentGatewayPassword();

           $inv_array = [];
           session_start();
           $_SESSION['inv_id'] = $id;
           
          $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
              'soap_version' => SOAP_1_2, // !!!!!!!
              'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
            ));

          $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI',true);
          $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

          $client->__setSoapHeaders([$actionHeader,$toHeader]);


          $portfolio_details = PortfolioDetails::where('investment_id',$id)->where('portfolio_status',0)->where('bse_order_status',0)->get();

          //dd($portfolio_details);
          foreach ($portfolio_details as $portfolio) {
              //echo $portfolio->bse_order_no."-".$portfolio->user->personalDetails->pan."<br>";
              // echo $portfolio->id."<br>";
              $params = array(
                'Flag' => '11',
                'UserId' => '1245601',
                'EncryptedPassword' => $this->response_key,
                'param' => $portfolio->user->personalDetails->pan.'|'.$portfolio->bse_order_no.'|BSEMF',

              );

              $result = $client->MFAPI($params);
              $result = explode('|', $result->MFAPIResult);

              $payment_res = explode('(', $result[1]);
              //echo $portfolio->user->personalDetails->pan." -- ".$portfolio->bse_order_no." -- ".trim($payment_res[0])."<br>";

              if (trim($payment_res[0]) == "APPROVED") {
                  // echo "Inside Approved";
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'apr']);
              }else if (trim($payment_res[0]) == "REJECTED") {
                  // reject the order.
                  // echo "Inside rejected";
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'rej','portfolio_status'=>3]);
                  $inv_array[] = $portfolio->investment_id;
              }else if (trim($payment_res[0]) == "AWAITING FOR FUNDS CONFIRMATION") {
                  // Change the Payment Status column to AWC
                  // echo "inside afc";
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'afc']);
                  $inv_array[] = $portfolio->investment_id;
              }else if (trim($payment_res[0]) == "PAYMENT NOT INITIATED FOR GIVEN ORDER") {
                  // Chnage the Payment STatus to PNI
                  // echo "Inside pni";

                  // if it stays "PNI" for more than 24 hours Cancel it off
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'pni']);
                  $inv_array[] = $portfolio->investment_id;
              }else{

              }
              //echo ;
          }


          $update_pni_port = PortfolioDetails::where('created_at', '<', Carbon::now()->subDay())->where('bse_payment_status','pni')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

          $update_ac = PortfolioDetails::where('created_at', '<', Carbon::now()->subDays(7))->where('bse_payment_status','afc')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);


          foreach ($inv_array as $inv) {

              $can_port_count = PortfolioDetails::where('investment_id',$inv)->where('portfolio_status','!=',3)->count();
              //echo $can_port_count."<br>";

              if ($can_port_count == 0) {
                  InvestmentDetails::where('id',$inv)->update(['investment_status'=>3]);
              }
          }


          // $updated_portfolio = PortfolioDetails::where('investment_id',$id)->get();
          
          return redirect('/fund_selector');
    }



}
