<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\NachDetails;
use App\ekyc;
use Mail;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Notifications;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/sign-up';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            //'pan' => ['required','min:10','max:10','unique:users','regex:/^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/'],
            'mobile' => ['required','min:10','max:10','unique:users','regex:/^[789]\d{9}/'],
            'acc_type' => 'required',
        ]);
        
        if($validator->fails()) {Log::info($data);
            return $validator;//response()->json(['response'=>$validator->errors()]);
        } else {
            return $validator;
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        /*$otp_value = rand(1000,9999);
        $otp_start_time = date('Y-m-d H:i:s');
        Log::info('otp'.$otp_value);
        Log::info('otp start time'.$otp_start_time);*/

        // $new_user = new User();
        // $new_user->name = $data['name'];
        // $new_user->email = $data['email'];
        // $new_user->password = bcrypt($data['password']);
        // $new_user->mobile = $data['mobile'];
        // $new_user->acc_type = '0';
        // $new_user->p_check = '0';
        // $new_user->b_check = 0;
        // $new_user->kyc_check = '0';
        // $new_user->otp_check = 1;

        // $user_creation_status = $new_user->save();

        $user_creation_status = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'mobile' => $data['mobile'],
            //'pan' => $data['pan'],
            'acc_type' => '0',
            'p_check' => '0',
            'b_check' => 0,
            'kyc_check' => '0',
            'otp_check' => 1,
            //'otp' => bcrypt($otp_value),
            //'otp_start_time' => $otp_start_time,
        ]);

        //dd($user_creation_status);
        if($user_creation_status) {

            $nach_details = new NachDetails();
            $nach_details->user_id = $user_creation_status->id;
            
            //$create_nach = \Auth::user()->nachDetails()->save(new NachDetails());
              $create_nach = $nach_details->save();
            //have to be tested out cause otp cannot be sent from localhost.
            if ($create_nach) {

                $notification = new Notifications();
                $notification->description = "You can start activating your Account from the Account Activate tab.";
                $notification->date = date('Y-m-d');
                $notification->user_id = $user_creation_status->id;

                $save_notification = $notification->save();

                if ($save_notification) {

                        $ekyc = new ekyc();
                        $ekyc->user_id = $user_creation_status->id;
                        $save_ekyc = $ekyc->save();

                        if ($save_ekyc) {
                          //unset($user_creation_status);
                          //$user_creation_status = true;
                          return $user_creation_status;
                        }
                        
                }
                
            }
            
            
        }
    }


    public function showLoginForm()
    {
        return view('users.version2.login');
    }

    public function showRegistrationForm(){
        return view('users.version2.register');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json(['msg'=> $this->getFailedLoginMessage()]);
    }

    protected function authenticated(Request $request, $user)
    {
        $user = \Auth::user();
        /*if($user->otp_check == 0) {
            return response()->json(['msg'=> 'otp_check']);
        } else {*/
        if (\Auth::user()->nachDetails->kyc_status != 1 || \Auth::user()->nachDetails->pan_status != 1 || \Auth::user()->nachDetails->kycvideo_status != 1 || \Auth::user()->nachDetails->cc_status != 1 || \Auth::user()->nachDetails->mandatesoft_status != 1 || \Auth::user()->nachDetails->aof_uploaded != 1 || \Auth::user()->nachDetails->fatca_status != 1 || \Auth::user()->nachDetails->bseaof_status != 1 || \Auth::user()->nachDetails->bseclient_status != 1) {
            return response()->json(['msg'=> 'activation','redirect'=>'account_activation']);
        }else{
            return response()->json(['msg'=> 'success','redirect'=>'home']);
        }
        //}
    }

}
