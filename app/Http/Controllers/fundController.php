<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SchemeDetails;
use App\InvestmentDetails;
use App\Sip;
use App\PortfolioDetails;
use Carbon\Carbon;
use App\SchemeHistory;
use App\SipDetails;
use App\SipEntries;
use App\HistoricNav;

use Illuminate\Support\Facades\Log;

class fundController extends Controller
{

	  protected $pass_key = 'abcdef7895';
	  protected $password = 'Pwm$2017';
	  protected $response_key;
	  protected $order_no = 0;

	  protected $place_order_count = 0;

    	public function showFunds(Request $request){

			$validator = \Validator::make($request->all(),[
				'p_type' => 'required',
				'inv_amount' => 'required',
				//'inv_type' => 'required',
				]);

			if ($validator->fails()) {
				
				//return response()->json(['msg'=>'val_fail']);
				return response()->json(['msg'=>$validator->errors()]);
			}

			else{

				$port_type = $request['p_type'];
				$inv_amount = $request['inv_amount'];
				$inv_type = $request['inv_type'];
				//echo $port_type;

					//checking the selected portfolio type

				if ($port_type == 'own') {
					

				}

				

				elseif($port_type == 'Conservative'){

					$scheme_type = 'debt';
					$response = $this->suggestFund($scheme_type,$inv_amount,$inv_type);

					if ($response['r_code'] == 'min_5000') {

							//$response = json_encode($response['response']);
							return response()->json(['msg'=>'min_5000']);
						}

						elseif($response['r_code'] == 'success'){

							
							$response = json_encode($response['response']);
							return response()->json(['msg'=>'debt','response'=>$response]);
						}

				}


				elseif($port_type == 'Moderate'){

					$scheme_type = 'bal';
					$response = $this->suggestFund($scheme_type,$inv_amount,$inv_type);
					$msg = "";

					//print_r($response);
					//echo $response['r_code'];

					

						if ($response['r_code'] == 'min_5000') {

							//$response = json_encode($response['response']);
							return response()->json(['msg'=>'min_5000']);
						}

						elseif($response['r_code'] == 'success'){

							
							$response = json_encode($response['response']);
							return response()->json(['msg'=>'bal','response'=>$response]);
						}
					

				}

				elseif($port_type == 'Aggressive'){

					$scheme_type = 'eq';
					$response = $this->suggestFund($scheme_type,$inv_amount,$inv_type);
					$msg = "";

						if ($response['r_code'] == 'min_5000') {

							//$response = json_encode($response['response']);
							return response()->json(['msg'=>'min_5000']);
						}

						elseif($response['r_code'] == 'success'){

							
							$response = json_encode($response['response']);
							return response()->json(['msg'=>'eq','response'=>$response]);
						}
				}


				elseif($port_type == 'Tax Saver'){

					$scheme_type = 'ts';
					$response = $this->suggestFund($scheme_type,$inv_amount,$inv_type);
					$msg = "";

						if ($response['r_code'] == 'min_5000') {

							//$response = json_encode($response['response']);
							return response()->json(['msg'=>'min_5000']);
						}

						elseif($response['r_code'] == 'success'){

							
							$response = json_encode($response['response']);
							return response()->json(['msg'=>'ts','response'=>$response]);
						}
				}
				// else{

				// 	$scheme_type = 'debt';
				// 	$response = $this->suggestFund($scheme_type,$inv_amount);
				// }


			}	

			
		}//Show funds function ends


	public function suggestFund($scheme_type,$inv_amount,$inv_type)
	{

		$user = \Auth::user();


		$split_amount = array();
		$scheme_suggestions = array(); //Variable which will hold suggested Scheme 
		$base_amount = 0; //Amount which will be invested in each schemes

		if($inv_type != '') {
			if ($inv_type == 'monthly') {
				$new_minimum_fund_amount = 1000;
			}
			else{
				$new_minimum_fund_amount = 5000;
			}

			if($scheme_type == 'ts') {
				$new_minimum_fund_amount = 1000;
			}

			// Minimum fund amount for first time investment
			$schemes_invested = array(); // Variable will hold the schemes user has invested in.
			$excess_amount = 0; //Variable which will hold excess amount after splitting

			//Getting All the funds schemes 
			$funds = SchemeDetails::where('scheme_type',$scheme_type)->where('scheme_status',1)->orderBy('scheme_priority', 'asc')->limit(5)->get(); 

			//Counting the number of funds
			$funds_count = count($funds);
			
			//Getting scheme codes of the schemes user has already invested in and converting it to an array
			$invested_scheme_codes = $user->portfolioDetails()
				->where('portfolio_type',$scheme_type)
				->whereIn('portfolio_status', [1,2])
				->groupBy('scheme_code')
				->pluck('scheme_code');
			$invested_scheme_codes = $invested_scheme_codes->toArray();

			//echo count($invested_scheme_codes);

			/*Checking if the user is investing for the first time or not*/
			if(count($invested_scheme_codes) == 0){  //New investor

				//If amount exceeds filling all the funds with minimum funds amount() then increasing the base amount value.
				//this below if will get executed only if the investment amount > the total no debt funds * 5000

				if ($inv_amount < $new_minimum_fund_amount) {

					$suggestions = array();
					$suggestions['r_code'] = 'min_5000';
					return $suggestions;

				}

				else{

					/*if ($inv_amount < 1000) {
						$suggestions = array();
						$suggestions['r_code'] = 'min_1000';
						return $suggestions;

					}*/

					if($inv_amount > ($funds_count * $new_minimum_fund_amount)) {

						$without_excess_amount = $inv_amount; // -($inv_amount % ($funds_count*1000));
						$base_amount = $new_minimum_fund_amount; //$without_excess_amount / $funds_count;
						$excess_amount = $inv_amount - $without_excess_amount;
					} 

					// if the investment amount < the total debt funds amount * 5000

					else {
					$base_amount = $new_minimum_fund_amount;
					$excess_amount = $inv_amount % $base_amount;
					}					
				}
				
			} //New user if condition ends

			//The below else will get executed if the user has alread invested

			else { 

				//checking if all the debt funds schemes are filled with first investment.

				if(count($invested_scheme_codes) == $funds_count){

					//echo "Printing if funds counts is eq";
					/*$without_excess_amount = $inv_amount -($inv_amount % ($funds_count*1000));
					$base_amount = $without_excess_amount / $funds_count;
					$excess_amount = $inv_amount - $without_excess_amount;*/

					if (($funds_count*1000) > $inv_amount) {
							$without_excess_amount = $inv_amount /* -($inv_amount % ($funds_count*1000))*/;
							$base_amount = 1000;
							$excess_amount = $inv_amount - $without_excess_amount;
						}else{
							$without_excess_amount = $inv_amount; // -($inv_amount % ($funds_count*1000));
							$base_amount = 1000; //$without_excess_amount / $funds_count;
							$excess_amount = $inv_amount - $without_excess_amount;
					}
				}

				//if debt funds schemes are partially filled 

				else{

					//echo "Printing if funds counts is not eq";
					//If amount exceeds filling all the funds with minimum fund amount(i.e 5000) then increasing the base amount value.
					//this below if will get executed only if the investment amount > the total no debt funds * 5000


					if($inv_amount > ($funds_count * $new_minimum_fund_amount)) {
						$without_excess_amount = $inv_amount; // -($inv_amount % ($funds_count*1000));
						$base_amount = 1000; //$without_excess_amount / $funds_count;
						$excess_amount = $inv_amount - $without_excess_amount;
					} //if ends
					// if the investment amount < the total debt funds amount * 5000
					else {
						$base_amount = $new_minimum_fund_amount;
						$excess_amount = $inv_amount % $base_amount;
					}//else ends

				}//else ends

			} //Else ends

			/*Separating round investment amount from original investment amount
				i.e 25000 from 26000 and excess amount will have 1000

			*/

			$inv_amount = $inv_amount - $excess_amount;

			//Log::info($base_amount);
			while ($inv_amount != 0) {
				
				//Split amount is the amount to be invested in each fund and splitting the investment amount with base amount.
				/* if inv_amount = 26000
						
						then after the loop $split_amount = [5000,5000,5000,5000,5000]
						
				*/
				$split_amount[] = $base_amount;
				$inv_amount -= $base_amount ;
			}

			/*Getting all the scheme codes user has invested in from his portfolio details and converting it to an array*/

			$schemes_invested_so_far = $user->portfolioDetails
				->where('portfolio_type',$scheme_type)
				->whereIn('portfolio_status', [1,2])
				->groupBy('scheme_code')
				->all();
			$schemes_invested = array();
			$available_schemes = SchemeDetails::where('scheme_type',$scheme_type)
					->whereIn('scheme_priority', [1, 2, 3, 4, 5])
					->where('scheme_status',1)
				  	->pluck('scheme_code');//print_r($schemes_invested_so_far);
				  	$available_schemes = $available_schemes->toArray();//Log::info($available_schemes);
			foreach($schemes_invested_so_far as $scheme_code => $scheme_detail) {
				//->pluck('amount_invested','scheme_code');
				if(in_array($scheme_code, $available_schemes)) {
					$schemes_invested[$scheme_code] = $scheme_detail->sum('amount_invested');Log::info($scheme_code);
				}
			}
			//$schemes_invested = $schemes_invested->toArray();

			Log::info($schemes_invested);
			foreach($split_amount as $amount) {


				//Only when the user is investing for the first time IF condition is executed 


				if(empty($scheme_suggestions) && empty($schemes_invested)) { //To check whether any investment is made already and no suggestions made already
					$funds = SchemeDetails::where('scheme_type',$scheme_type)
					->whereIn('scheme_priority', [1, 2, 3, 4, 5])
					->where('scheme_status',1)
				  	->orderBy('scheme_priority','asc')
				  	//->limit(5)
				  	->first();

				  	$scheme_suggestions[$funds->scheme_code] = $amount;
				  	$schemes_invested[$funds->scheme_code] = $amount;
				  	$invested_scheme_codes[] = $funds->scheme_code;			  	
				} 

				else {
					$next_scheme = SchemeDetails::whereNotIn('scheme_code',$invested_scheme_codes)
					->where('scheme_type',$scheme_type)
					->whereIn('scheme_priority', [1, 2, 3, 4, 5])
					->where('scheme_status',1)
					->orderBy('scheme_priority','asc')
					//->limit(5)
					->first();

					//echo $next_scheme;


					if($next_scheme != NULL){

					  	$scheme_suggestions[$next_scheme['scheme_code']] = $amount;
					  	$schemes_invested[$next_scheme['scheme_code']] = $amount;
					  	$invested_scheme_codes[] = $next_scheme['scheme_code'];
					  	//print_r($scheme_suggestions);
					}

					else{
						
						$minimum_invested_amount = min($schemes_invested);
						$maximum_invested_amount = max($schemes_invested);


						if( $minimum_invested_amount == $maximum_invested_amount)
						{
							$funds = SchemeDetails::where('scheme_type',$scheme_type)
							->whereIn('scheme_priority', [1, 2, 3, 4, 5])
							->where('scheme_status',1)
						  	->orderBy('scheme_priority','asc')
						  	->limit(5)
						  	->first();

						  	if(array_key_exists($funds['scheme_code'], $scheme_suggestions)) {
						  		$scheme_suggestions[$funds['scheme_code']] += $amount;
						  	} 

						  	else {
						  		$scheme_suggestions[$funds['scheme_code']] = $amount;
						  	}
						  	
						  	$schemes_invested[$funds['scheme_code']] += $amount;
						} 

						else{

							$no_of_minimum_invested_schemes_codes = array_keys($schemes_invested,$minimum_invested_amount);
							if(count($no_of_minimum_invested_schemes_codes)>1) {

								$next_scheme = SchemeDetails::whereIn('scheme_code',$no_of_minimum_invested_schemes_codes)
								->where('scheme_type',$scheme_type)
								->where('scheme_status',1)
								->whereIn('scheme_priority', [1, 2, 3, 4, 5])
								->orderBy('scheme_priority','asc')
								->limit(5)
								->first();

						  		if($next_scheme != NULL) {
						  			if(array_key_exists($next_scheme->scheme_code, $scheme_suggestions)) {
									  	$scheme_suggestions[$next_scheme->scheme_code] += $amount;
									} else {
										$scheme_suggestions[$next_scheme->scheme_code] = $amount;
									}
							  		$schemes_invested[$next_scheme->scheme_code] += $amount;
							  	}
							} 
							else {
								
						  		if(array_key_exists(current($no_of_minimum_invested_schemes_codes), $scheme_suggestions)) {
								  	$scheme_suggestions[current($no_of_minimum_invested_schemes_codes)] += $amount;
								} else {
									$scheme_suggestions[current($no_of_minimum_invested_schemes_codes)] = $amount;
								}
						  		$schemes_invested[current($no_of_minimum_invested_schemes_codes)] += $amount;
							}
						}
						
					}
				} // else ends 
			}// Foreach ends 

			
			if ($excess_amount > 0) {
				end($schemes_invested);
				$least_p_scheme = key($schemes_invested);
				//echo $least_p_scheme;
				if(array_key_exists($least_p_scheme, $scheme_suggestions)) {
					$scheme_suggestions[$least_p_scheme] += $excess_amount;
				} else {
					$scheme_suggestions[$least_p_scheme] = $excess_amount;
				}
				$schemes_invested[$least_p_scheme] += $excess_amount;
			}

			$passing_suggestion = array();
			$scheme_name_code = SchemeDetails::where('scheme_type',$scheme_type)->where('scheme_status',1)->pluck('scheme_name','scheme_code');
			$scheme_name_code = $scheme_name_code->toArray();
			Log::info($scheme_suggestions);
			foreach ($scheme_suggestions as $schemes => $amount) {
				
				$passing_suggestion[] = array(
					'scheme_code' => $schemes,
					'scheme_name' => $scheme_name_code[$schemes],
					'scheme_type' => $scheme_type,
					'amount' => $amount,
					
				);
				
			}
		} else {

			$passing_suggestion = array();
			$scheme_name_code = SchemeDetails::where('scheme_type',$scheme_type)
				->where('scheme_status',1)
				->whereIn('scheme_priority', [1,2,3,4,5])
				->pluck('scheme_name','scheme_code');
			$scheme_name_code = $scheme_name_code->toArray();
			//Log::info($scheme_suggestions);
			foreach ($scheme_name_code as $scheme_code => $scheme_name) {
				
				$passing_suggestion[] = array(
					'scheme_code' => $scheme_code,
					'scheme_name' => $scheme_name,
					'scheme_type' => $scheme_type,
					'amount' => '',
					
				);
				
			}

		}

			//print_r($passing_suggestion);

			$suggestions = array();
			$suggestions['r_code'] = 'success';
			$suggestions['response'] = $passing_suggestion;

			//dd($scheme_name_code);
			return $suggestions;

			//return response()->json(['msg'=>$passing_suggestion]);
	}


	public function investFunds(Request $request)
	{

		$user = \Auth::user();
		$investment_id = '';
		$port_type = '';
		$pass_date;

		if ($user->p_check == "0") {
			$errors = array();
			$errors['activation_needed'] = 'Activate your account to start investing.';
			return response()->json(['msg'=>'errors', 'errors' => $errors]);

		}else{
			$pan = $user->personalDetails->pan;
		}
		
		$future_date;
		$bse_scheme_code = SchemeDetails::pluck('bse_scheme_code','scheme_code');

		$validator = \Validator::make($request->all(),[
			'port_type' => 'required',
			'inv_amount' => 'required',
			'inv_date' => 'required',
			'inv_type' => 'required',
			//'payment_type' => 'required',
		]);



		if ($validator->fails()) {
			
			$errors = array();
			$validator_errors = $validator->errors()->toArray();
			
			if(array_key_exists('port_type', $validator_errors)) {
				$errors['port_type'] = 'Portfolio type is required';
			}

			if(array_key_exists('inv_type', $validator_errors)) {
				$errors['inv_type'] = 'Investment type is required';
			}

			if(array_key_exists('inv_date', $validator_errors)) {
				$errors['inv_date'] = 'Investment date is required';
			}

			if(array_key_exists('inv_amount', $validator_errors)) {
				$errors['inv_amount'] = 'Investment amount is required';
			}

			// if(array_key_exists('payment_type', $validator_errors)) {
			// 	$errors['payment_type'] = 'Payment type is required (Neft or Mandate)';
			// }

			return response()->json(['msg'=>'errors', 'errors' => $errors]);
		} else {

			$user_nach = \Auth::user()->nachDetails;
			$bank = \Auth::user()->bankDetails()->get()->toArray();
			$bank = $bank[0];
			$bank_id = $bank['bank_id'];
			$aof = $user_nach->bseaof_status;
			$mandate = $user_nach->bsemandate_status;
			//dd($user_nach->pan_status);

			if ($user_nach->bseclient_status == '' || $user_nach->bseclient_status == 2){
				$errors['inactive'] = 'Internal Verification is pending. you can start investing once the verification is completed.';
				return response()->json(['msg'=>'errors', 'errors' => $errors]);
			}

			if ($user_nach->pan_status != 1 || $user_nach->kyc_status != 1) {
				$errors['inactive'] = 'Your PAN is not verified yet. You can start investing once it is verified.';
				return response()->json(['msg'=>'errors', 'errors' => $errors]);
			}

			if ($request['inv_type'] == "onetime") {
				
				$inv_type = 0;
			}
			if ($request['inv_type'] == "future") {
				if ($user_nach->bseaof_status == 1 && $user_nach->aof_uploaded == 1) {
					$inv_type = 1;
				}else{
					$errors['no_sip'] = 'You cannot schedule a future investment rightnow.';
					return response()->json(['msg'=>'errors', 'errors' => $errors]);
				}
			}
			if ($request['inv_type'] == "monthly") {

				if ($user_nach->pan_status == 1 && $user_nach->kyc_status == 1 && $user_nach->kycvideo_status == 1 && $user_nach->camskyc_status == 1 && $user_nach->bseaof_status == 1 && $user_nach->mandatesoft_status == 1 && $user_nach->mandatehard_status == 1 && $user_nach->aof_uploaded == 1 && $user_nach->fatca_status == 1 && $user_nach->bsemandate_status == 1) {
					$inv_type = 2;	
				}else{
					$errors['no_sip'] = 'You cannot Invest in SIP unless your account is activated. You can invest in Onetime and Future Investment';
					return response()->json(['msg'=>'errors', 'errors' => $errors]);
				}
				
			}


			if ($request['port_type'] == 'custom') {
				$port_type = 0;
			}

			if ($request['port_type'] == 'Conservative') {
				$port_type = 1;
			}

			if ($request['port_type'] == 'Moderate') {
				$port_type = 2;
			}

			if ($request['port_type'] == 'Aggressive') {
				$port_type = 3;
			}

			if ($request['port_type'] == 'Tax Saver') {
				$port_type = 4;
			}

			/*Date Format Conversion*/
			if ($request['inv_type'] == "future") {
				$date = date('Y-m-d', strtotime($request['inv_date']));
				$future_date = date('Y-m-d', strtotime($request['inv_date']));
				$pass_date = $future_date;
			} else {
				$date = Carbon::now()->addDays(0)->toDateString();
				$pass_date = $date;
			}

				/*Date format ends*/

			//die("vandhuduchi");

			// $check_limit = $this->checkRemainingInvestments($request['inv_amount'],$pass_date);

			//dd($check_limit);
			$check_limit[0] = '';

			if ($check_limit[0]) {
				$errors['inactive'] = 'You have already placed the orders worth Rs.'.round($check_limit[1],0).'. You can only place order worth Rs. 1 Lakh/day';
				return response()->json(['msg'=>'errors', 'errors' => $errors]);
			}




			$investment = new InvestmentDetails();

			$investment->portfolio_type = $port_type;
			$investment->investment_amount = $request['inv_amount'];
			$investment->investment_date = $date;
			$investment->investment_type = $inv_type;
			$investment->investment_status = 0;
			//$investment->payment_type = $this->getPaymentType();

			if ($request['inv_type'] == 'monthly') {

				$portfolio_type = '';
				$requested_date = explode('/', $request['inv_date']);
				$start_end_date = explode('-', $requested_date[0]);
				$current_date = Carbon::now();
				$start_date = $current_date->year.'-'.$current_date->month.'-'.$requested_date[0];
				$end_date = $current_date->year.'-'.$current_date->month.'-'.$requested_date[0];;
				/*if(($start_end_date[0]+5) < $current_date->day) {
					$start_date = $current_date->year.'-'.$current_date->month.'-'.$start_end_date[0];
					$end_date = $current_date->year.'-'.$current_date->month.'-'.$start_end_date[1];
				} else {
					$current_date->addMonth();
					$start_date = $current_date->year.'-'.$current_date->month.'-'.$start_end_date[0];
					$end_date = $current_date->year.'-'.$current_date->month.'-'.$start_end_date[1];
				}*///Log::info($start_date.$end_date);
				$tenure = intval($requested_date[1]);
				//get the portfolio type 
				if($request['port_type'] == 'Conservative') {
						$portfolio_type = 'debt';
				} else if($request['port_type'] == 'Moderate') {
					$portfolio_type = 'bal';
				} else if($request['port_type'] == 'Aggressive') {
					$portfolio_type = 'eq';
				}else if($request['port_type'] == 'Tax Saver') {
					$portfolio_type = 'ts';
				}Log::info($portfolio_type);
				if($portfolio_type != '') {

					$sip_details = new Sip();
					//$sip_details->port_id = $save_inv->id; 
					$sip_details->inv_amount = $request['inv_amount'];
					$sip_details->start_date = $start_date;
					$sip_details->port_type = $port_type;
					$sip_details->end_date = $end_date;
					$sip_details->active_status = 0;
					$sip_details->tenure = $tenure;

					$save_sip = $user->sip()->save($sip_details);

					if ($save_sip) {

						$portfolio_details = $this->suggestFund($portfolio_type, $request['inv_amount'],$request['inv_type']);

						if($portfolio_details['r_code'] == 'success') {
							foreach ($portfolio_details['response'] as $portfolio) {
								$sip_entries = new SipEntries();
								$sip_entries->sip_id = $save_sip->id;
								$sip_entries->user_id = $user->id;
								$sip_entries->scheme_code = $portfolio['scheme_code'];
								$sip_entries->amount = $portfolio['amount'];
								$sip_entries->save();

							}
						}
						

					$investment->sip_id = $save_sip->id;

					$save_inv = $user->investmentDetails()->save($investment);
					
					//check if funds are suggested
					if($portfolio_details['r_code'] == 'success') {
						foreach ($portfolio_details['response'] as $portfolio) {//Log::info($portfolio_details);
							//echo $portfolio['scheme_code'];
							//echo $portfolio['scheme_name'];
							//echo $portfolio['amount'];
							$investment_id = $save_inv->id;
							$portfolio_save = new PortfolioDetails();
							$portfolio_save->investment_id = $save_inv->id;
							$portfolio_save->sip_id = $save_sip->id;
							$portfolio_save->scheme_code = $portfolio['scheme_code'];
							$portfolio_save->folio_number = '';
							$portfolio_save->portfolio_status = 0;
							$portfolio_save->initial_amount_invested = $portfolio['amount'];
							$portfolio_save->portfolio_type = $portfolio['scheme_type'];
							$portfolio_save->amount_invested = $portfolio['amount'];
							$portfolio_save->investment_date = $date;
							$portfolio_save->units_held = '';
							$save_portfolio = $user->PortfolioDetails()->save($portfolio_save);

						}

						

							$investment_date = Carbon::now()->addDays(0);
							$sipdetails = new SipDetails();
	                        $sipdetails->month = $investment_date->month;
	                        $sipdetails->year = $investment_date->year;

	                        $save_sip->sipDetails()->save($sipdetails);

	                        //echo $date;

	                        
							return response()->json(['msg'=>'success', 'status'=>'You have successfully scheduled an investment', 'date'=> date('d-m-Y',strtotime($start_date)), 'id' => $save_inv->id,'inv_amount' => $request['inv_amount'],'aof'=>$aof,'mandate' => $mandate,'bank_id' => $bank_id]);
						} else {
							return response()->json(['msg'=>'fail', 'status'=>'Investment failed. Please check and try again']);
						}
					}
				}
			} else {

				$save_inv = $user->investmentDetails()->save($investment);

				$portfolio_type = '';

				//get the portfolio type 
				if($request['port_type'] == 'Conservative') {
					$portfolio_type = 'debt';
				} else if($request['port_type'] == 'Moderate') {
					$portfolio_type = 'bal';
				} else if($request['port_type'] == 'Aggressive') {
					$portfolio_type = 'eq';
				}
				else if($request['port_type'] == 'Tax Saver') {
					$portfolio_type = 'ts';
				}Log::info($date);
				
				if($portfolio_type != '') {
					$portfolio_details = $this->suggestFund($portfolio_type,$request['inv_amount'],$request['inv_type']);
					//dd($portfolio_details);
					$investment_id;
					if(array_key_exists('response', $portfolio_details)){
						$count = 0;
						$bse_order_response = array();
						foreach ($portfolio_details['response'] as $portfolio) {

							$bse_date = date('Y-m-d');
						    $bse_date = explode('-', $bse_date);
						    $bse_date = $bse_date[0].$bse_date[1].$bse_date[2];
						    $time = explode('.', microtime());
						    $time = substr($time[1], 0 ,6);

						    $unique_ref_no = $bse_date."12456".$time;
						    $int_ref_no = $unique_ref_no;

						    $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$portfolio['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

						    if (count($user_folio) == 0) {
						    	$folio_number = '';	
						    }else{
						    	$folio_number = $user_folio['folio_number'];

						    }

							
							$investment_id = $save_inv->id;
							$portfolio_save = new PortfolioDetails();
							$portfolio_save->investment_id = $save_inv->id;
							$portfolio_save->scheme_code = $portfolio['scheme_code'];
							$portfolio_save->folio_number = $folio_number;
							$portfolio_save->bse_scheme_code = $bse_scheme_code[$portfolio['scheme_code']];
							$portfolio_save->int_ref_no = $int_ref_no;

							if ($request['inv_type'] == "future") {
								$bse_date = $future_date;
							}
							$portfolio_save->bse_order_date = $bse_date;
							$portfolio_save->unique_ref_no = $unique_ref_no;
							$portfolio_save->portfolio_status = 0;
							$portfolio_save->portfolio_type = $portfolio_type;
							$portfolio_save->initial_amount_invested = $portfolio['amount'];
							$portfolio_save->amount_invested = $portfolio['amount'];
							$portfolio_save->investment_date = $date;		
							$portfolio_save->units_held = '';
							$save_portfolio = $user->PortfolioDetails()->save($portfolio_save);



							if ($request['inv_type'] == "onetime") {
								$this->getPassword();
													    

							    $transact_mode = 'NEW';
							    $buy_sell = 'P';

							    if ($folio_number == '') {
							    	$buy_sell_type = 'FRESH'; //FRESH OR ADDITIONAL WILL COME HERE;	
							    }else{
							    	$buy_sell_type = 'ADDITIONAL'; //FRESH OR ADDITIONAL WILL COME HERE;
							    }
							     

							    if ($portfolio['amount'] > 200000) {
							    	$scheme_code = $bse_scheme_code[$portfolio['scheme_code']]."-L1";
							    	$bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$scheme_code,$buy_sell,$buy_sell_type,$portfolio['amount'],$folio_number,$int_ref_no,$investment_id);
							    }else{						    	
								    $bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$portfolio['scheme_code']],$buy_sell,$buy_sell_type,$portfolio['amount'],$folio_number,$int_ref_no,$investment_id);
							    }

							    //echo "hello"."<br>";

								
							    }
							}

							

						} // foreach ends

						//die();
						
						//dd($bse_order_request);
						if ($save_portfolio) {

							//$portfolio_details = PortfolioDetails::where('investment_id',$investment_id)->get();

							if ($request['inv_type'] == "onetime") {

								foreach ($bse_order_response as $key => $value) {
									$date = date('d/m/Y h:i:s A', strtotime($date));
									PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $bse_date]);
								}

								return response()->json(['msg'=>'success', 'status'=>'you have successfully scheduled an investment', 'date'=> date('d-m-Y',strtotime($bse_date)), 'id' => $save_inv->id,'inv_amount' => $request['inv_amount'],'aof'=>$aof,'mandate' => $mandate,'bank_id' => $bank_id]);
							}else{
								return response()->json(['msg'=>'success', 'status'=>'you have successfully scheduled an investment', 'date'=> date('d-m-Y',strtotime($bse_date)), 'id' => $save_inv->id,'inv_amount' => $request['inv_amount'],'aof'=>$aof,'mandate' => $mandate,'bank_id' => $bank_id]);
							}

						} else {
							return response()->json(['msg'=>'fail', 'status'=>'Investment failed. Please check and try again']);
						}


					} else {
						return response()->json(['msg'=>'fail', 'status'=>'Investment failed. Please check and try again']);
						//response if failure
					}
				}
		}//else ends
	}//showFunds function ends


	public function showCustomFunds(Request $request){

		$user = \Auth::user();

		$validator = \Validator::make($request->all(),[
				'p_type' => 'required',
				'inv_type'=>'required',
				]);



			if ($validator->fails()) {
				

				return response()->json(['msg'=>$validator->errors()]);
			}

			else{

				$investment_type = $request['inv_type'];
				$p_type = $request['p_type'];
		
				if($investment_type == 'monthly'){
					$minimum_investment_amount = 1000;
				}
		 			
		 		else{
		 			$minimum_investment_amount = 5000;
		 		}


				$scheme_details = SchemeDetails::where('scheme_status',1)->get();
				$suggestion_list = array();

				//print_r($scheme_details);


				//$response = json_encode($response);

				/*Getting all the scheme codes user has invested in from his portfolio details and converting it to an array*/

				
				
					//print_r($scheme_details);
				

						foreach ($scheme_details as $scheme) {


							//echo $scheme->scheme_type;


							$amount_invested = $user->portfolioDetails()->where('scheme_code',$scheme->scheme_code)->whereIn('portfolio_status',[0,1])->sum('amount_invested');

							//echo $amount_invested;

							if ($amount_invested >= $minimum_investment_amount) {
								$suggestion_list[] = array(
									'scheme_code' => $scheme->scheme_code,
									'scheme_name' => $scheme->scheme_name,
									'scheme_type' => $scheme->scheme_type,
									'minimum_amount' => 1000,
								);	
							}
							else{

								$suggestion_list[] = array(
								'scheme_code' => $scheme->scheme_code,
								'scheme_name' => $scheme->scheme_name,
								'scheme_type' => $scheme->scheme_type,
								'minimum_amount' => $minimum_investment_amount,
								);
							}
							
						}


						$suggestion_list = json_encode($suggestion_list);
						//echo $suggestion_list;
						return response()->json(['msg'=>'custom','response'=>$suggestion_list]);
				
			}
	}


	public function investCustomFunds(Request $request)
	{

		$user = \Auth::user();
		$save_status = false;
		$status_msg = '';
		$total_inv;
		$inv_date = '';
		$investment_id = '';
		$total_inv_amount;
		$minimum_investment_amount;
		$pass_date;
		$saved_sip_id;
		if ($user->p_check == "0") {
			$errors = array();
			$errors['inactive'] = 'Activate your account to start investing.';
			return response()->json(['msg'=>'errors', 'errors' => $errors]);

		}else{
			$pan = $user->personalDetails->pan;
		}
		$bse_scheme_code = SchemeDetails::pluck('bse_scheme_code','scheme_code');

		$validator = \Validator::make($request->all(),[
			'code_amount' => 'required',
			'inv_type'=>'required',
			'inv_date'=> 'required',
			//'payment_type' => 'required',
		]);



		if ($validator->fails()) {
			
			$errors = array();
			$validator_errors = json_decode($validator->errors());
			
			if($validator_errors->code_amount!= NULL) {
				$errors['code_amount'] = 'Investment amount is required';
			}

			if($validator_errors->inv_type != NULL) {
				$errors['inv_type'] = 'Investment type is required';
			}

			if($validator_errors->inv_date != NULL) {
				$errors['inv_date'] = 'Investment date is required';
			}

			// if($validator_errors->payment_type != NULL) {
			// 	$errors['payment_type'] = 'Payment type is required (Neft or Mandate)';
			// }

			return response()->json(['msg'=>'fail', 'errors' => $errors]);
		} else {

			$user_nach = \Auth::user()->nachDetails;
			$bank = \Auth::user()->bankDetails()->get()->toArray();
			$bank = $bank[0];
			$bank_id = $bank['bank_id'];

			$aof = $user_nach->bseaof_status;
			$mandate = $user_nach->bsemandate_status;
			//dd($user_nach->pan_status);

			if ($user_nach->bseclient_status == '' || $user_nach->bseclient_status == 2){
				$errors['inactive'] = 'Internal Verification is pending. you can start investing once the verification is completed.';
				return response()->json(['msg'=>'errors', 'errors' => $errors]);
			}

			if ($user_nach->pan_status != 1 || $user_nach->kyc_status != 1) {
				$errors['inactive'] = 'Your PAN is not verified yet. You can start investing once it is verified.';
				return response()->json(['msg'=>'errors', 'errors' => $errors]);
			}


			$start_date = '';
			$end_date = '';
			$date = '';
			$tenure = '';

			$code_amount = $request['code_amount'];
			$inv_type = $request['inv_type'];
			$inv_date = $request['inv_date'];

			//dd($code_amount,$inv_type,$inv_date);

			/*
				$code_amount contains all the custom schemes with amount to be invested in the below format

				[{"code":"100520","amount":"10000","scheme":"eq"}],

			*/

			if ($request['inv_type'] == "onetime") {
				
				$inv_type = 0;
			}
			if ($request['inv_type'] == "future") {
				if ($user_nach->bseaof_status == 1 && $user_nach->aof_uploaded == 1) {
					$inv_type = 1;
				}else{
					$errors['no_sip'] = 'You cannot schedule a future investment rightnow.';
					return response()->json(['msg'=>'errors', 'errors' => $errors]);
				}
			}
			if ($request['inv_type'] == "monthly") {

				if ($user_nach->pan_status == 1 && $user_nach->kyc_status == 1 && $user_nach->kycvideo_status == 1 && $user_nach->camskyc_status == 1 && $user_nach->bseaof_status == 1 && $user_nach->mandatesoft_status == 1 && $user_nach->mandatehard_status == 1 && $user_nach->aof_uploaded == 1 && $user_nach->fatca_status == 1 && $user_nach->bsemandate_status == 1 ) {
					$inv_type = 2;	
				}else{
					$errors['no_sip'] = 'You cannot Invest in SIP unless your account is activated. You can invest in Onetime and Future Investment';
					return response()->json(['msg'=>'errors', 'errors' => $errors]);
				}
				
			}

			/*Date Format Conversion*/
			if ($request['inv_type'] == "future") {
				$date = date('Y-m-d', strtotime($request['inv_date']));
				$pass_date = $date;
			} else if($request['inv_type'] == 'monthly') {

				$portfolio_type = '';
				$pass_date = $request['inv_date'];
				$requested_date = explode('/', $request['inv_date']);
				//$start_end_date = explode('-', $requested_date[0]);
				$current_date = Carbon::now()->addDays(0);
				$start_date = $current_date->year.'-'.$current_date->month.'-'.$requested_date[0];
				$end_date = $current_date->year.'-'.$current_date->month.'-'.$requested_date[0];
				$date = Carbon::now()->addDays(0)->toDateString();

				Log::info($start_date.$end_date);
				$tenure = intval($requested_date[1]);
			} else {
				$date = Carbon::now()->addDays(0)->toDateString();
				$pass_date = $date;
			}

			/*Date format ends*/

			//var_dump($code_amount);
			$scheme_to_invest = json_decode($code_amount);
			$total_inv = 0;
			$empty_inv = array();
			$inv_scheme = array();

			//dd($inv_type);

			if($request['inv_type'] == 'monthly'){
				$minimum_investment_amount = 1000;
			} else {
	 			$minimum_investment_amount = 5000;
	 		}

	 		

			foreach ($scheme_to_invest as $scheme ) {
				$total_inv += $scheme->amount;
				$count = 0;



				/*
					Below is separating all the empty schemes and scheme to be invested
				*/

				if ($scheme->amount === 0) {

					
					$empty_inv[] = array('code' => $scheme->code,
					'amount' => $scheme->amount,
					'scheme' => $scheme->scheme);
				}

				if ($scheme->amount !== 0) {

					//$total_inv_amount += $scheme->amount; 
					$inv_scheme[] = array('code' => $scheme->code,
					'amount' => $scheme->amount,
					'scheme' => $scheme->scheme);
				}
			}


			// $check_limit = $this->checkRemainingInvestments($total_inv,$pass_date);
			//dd($check_limit)
			$check_limit[0] = '';

			if ($check_limit[0]) {
				$errors['inactive'] = 'You have already placed the orders worth Rs.'.round($check_limit[1],0).'. You can only place order worth Rs. 1 Lakh/day';
				return response()->json(['msg'=>'errors', 'errors' => $errors]);
			}

			$suggestion_list = array();
			$scheme_details = SchemeDetails::where('scheme_status',1)->get();

			foreach ($scheme_details as $scheme ) {
				$amount_invested = $user->portfolioDetails()->where('scheme_code',$scheme->scheme_code)->where('portfolio_status',1)->sum('amount_invested');

				//echo $amount_invested.'-'.$scheme->scheme_code.'-'.$minimum_investment_amount."<br>";


				if ($amount_invested >= $minimum_investment_amount) {
					$suggestion_list[] = array(
						'scheme_code' => $scheme->scheme_code,
						'scheme_name' => $scheme->scheme_name,
						'scheme_type' => $scheme->scheme_type,
						'minimum_amount' => 1000,
					);	
				}else{

					$suggestion_list[] = array(
						'scheme_code' => $scheme->scheme_code,
						'scheme_name' => $scheme->scheme_name,
						'scheme_type' => $scheme->scheme_type,
						'minimum_amount' => $minimum_investment_amount,
					);
				}
						
			}

			//die();


			$count = 0;
			$passed_code =array();
			$failed_code = array();

			//dd($suggestion_list);
			

			//to check the scheme code received from user exist in scheme codes in scheme details
			foreach($inv_scheme as $inv){

				foreach($suggestion_list as $suggestion){

					if ($inv['code'] == $suggestion['scheme_code']) {

						//echo $inv['code'];

						//echo $inv['amount']."<br>";
						//echo $suggestion['minimum_amount']."<br>";

						if(($suggestion['minimum_amount'] > $inv['amount']) && ($suggestion['minimum_amount'] != $inv['amount'])){
							  $failed_code[$inv['code']]['code'] = $inv['code'];
							  $failed_code[$inv['code']]['min_amt'] = $suggestion['minimum_amount'];
						}
						else{

							$passed_code[] = array('code' => $inv['code'],
							'amount' => $inv['amount'],
							'scheme' => $inv['scheme']);
						}	
							
					}
				
				}
											
			}

			if (!empty($failed_code)) {
				
				//$failed_code = json_encode($failed_code);
				return response()->json(['msg'=>'min_fail','response'=>$failed_code]);

			} else {

				$save_investment = new InvestmentDetails();
				$save_investment->investment_amount = $total_inv;
				$save_investment->investment_type = $inv_type;
				$save_investment->portfolio_type = 0;
				$save_investment->investment_date = date('Y-m-d');
				$save_investment->investment_status = 0;
				//$save_investment->payment_type = $this->getPaymentType();

				
				$save_portfolio;

				if ($inv_type == 2) {
					$inv_date = $date;

					$sip_details = new Sip();
					// $sip_details->port_id = $saved->id;
					$sip_details->inv_amount = $total_inv;
					$sip_details->port_type = 'custom';
					$sip_details->start_date = $start_date;
					$sip_details->end_date = $end_date;
					$sip_details->tenure = $tenure;

					$save_sip = $user->sip()->save($sip_details);


					foreach ($passed_code as $portfolio) {
						//dd($portfolio);
						$sip_entries = new SipEntries();
						$sip_entries->sip_id = $save_sip->id;
						$sip_entries->user_id = $user->id;
						$sip_entries->scheme_code = $portfolio['code'];
						$sip_entries->amount = $portfolio['amount'];
						$sip_entries->save();
					}

					$save_investment->sip_id = $save_sip->id;
					$saved = $user->investmentDetails()->save($save_investment);
					$saved_sip_id = $save_sip->id;


				}
				else{
					$inv_date = $date;
					$saved_sip_id = null;
				}

				$saved = $user->investmentDetails()->save($save_investment);
				

				if ($saved) {
					$investment_id = $saved->id;
					
					//dd(count($passed_code));
					foreach ($passed_code as $passed) {

						$bse_date = date('Y-m-d');
						$bse_date = explode('-', $bse_date);
						$bse_date = $bse_date[0].$bse_date[1].$bse_date[2];
						$time = explode('.', microtime());
						$time = substr($time[1], 0 ,6);

						$unique_ref_no = $bse_date."12456".$time;
						$int_ref_no = $unique_ref_no;

						//$this->getPassword();
						//dd($passed);
						 $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$passed['code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

						 //dd($user_folio);

						    if (count($user_folio) == 0) {
						    	$folio_number = '';	
						    }else{
						    	$folio_number = $user_folio['folio_number'];

						    }

						$portfolio_save = new PortfolioDetails();

						$portfolio_save->investment_id = $saved->id;
						$portfolio_save->scheme_code = $passed['code'];
						$portfolio_save->sip_id = $saved_sip_id;
						$portfolio_save->folio_number = $folio_number;
						$portfolio_save->bse_scheme_code = $bse_scheme_code[$passed['code']];
						$portfolio_save->int_ref_no = $int_ref_no;
						$portfolio_save->bse_order_date = $bse_date;
						$portfolio_save->unique_ref_no = $unique_ref_no;
						$portfolio_save->portfolio_status = 0;
						$portfolio_save->initial_amount_invested = $passed['amount'];
						$portfolio_save->portfolio_type = $passed['scheme'];
						$portfolio_save->amount_invested = $passed['amount'];
						$portfolio_save->investment_date = $inv_date;
						$portfolio_save->units_held = '';
						$save_portfolio = $user->PortfolioDetails()->save($portfolio_save);

						if ($request['inv_type'] == "onetime") {
							$this->getPassword();													    
							$transact_mode = 'NEW';
							$buy_sell = 'P';
							if ($folio_number == '') {
						    	$buy_sell_type = 'FRESH'; //FRESH OR ADDITIONAL WILL COME HERE;	
						    }else{
						    	$buy_sell_type = 'ADDITIONAL'; //FRESH OR ADDITIONAL WILL COME HERE;
						    } 

							// $bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$passed['code']],$buy_sell,$buy_sell_type,$passed['amount'],$folio_number,$int_ref_no,$investment_id);
						    if ($passed['amount'] > 200000) {
						    	$scheme_code = $bse_scheme_code[$passed['code']]."-L1";
								$bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$scheme_code,$buy_sell,$buy_sell_type,$passed['amount'],$folio_number,$int_ref_no,$investment_id);
						    }else{						    	
							   $bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$passed['code']],$buy_sell,$buy_sell_type,$passed['amount'],$folio_number,$int_ref_no,$investment_id);
						    }
						}
					}//foreach ends

					
					if ($save_portfolio) {

						if ($request['inv_type'] == "onetime") {

								foreach ($bse_order_response as $key => $value) {
									$date = date('d/m/Y h:i:s A', strtotime($date));
									PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $bse_date]);
								}

							$save_status = true;
							$status_msg = 'Your Order has been placed successfully';

							//return response()->json(['msg'=>'success', 'status'=>'you have successfully scheduled an investment', 'date'=> date('d-m-Y',strtotime($bse_date)), 'id' => $save_inv->id,'inv_amount' => $request['inv_amount']]);
						}

						if ($inv_type == 2) {
						
							
							if ($save_sip) {

								$investment_date = Carbon::now()->addDays(5);
								$sipdetails = new SipDetails();
		                        $sipdetails->month = $investment_date->month;
		                        $sipdetails->year = $investment_date->year;

		                        $save_sip->sipDetails()->save($sipdetails);

								$save_status = true;
								$status_msg = 'you have successfully scheduled an monthly investment';
							} else {
								$save_status = false;
								$status_msg = 'Investment failed. Please check and try again';
							}// Save sip if else ends

								
							} else {
								$save_status = true;
								$status_msg = 'you have successfully scheduled an investment';
							}
						} else {

							$save_status = false;
							$status_msg = 'Investment failed. Please check and try again';
						}
						} else {
							$save_status = false;
							$status_msg = 'Investment failed. Please check and try again';

						}// save portfolio if else ends					
			}// else of failed code

		}//validator else ends

		if ($save_status) {
			return response()->json(['msg'=>'success', 'status'=>$status_msg,'inv_amount'=>$total_inv, 'date'=> date('d-m-Y',strtotime($inv_date)), 'id' => $investment_id,'aof'=>$aof,'mandate'=>$mandate,'bank_id' => $bank_id]);
		}else {
				return response()->json(['msg'=>'failure', 'status'=>$status_msg]);
		}
	}


	public function showPendingOrders(Request $request)
	{
		$user = \Auth::user();
		$validator = \Validator::make($request->all(),[
		'inv_type'=>'required',
		]);

		if ($validator->fails()) {
			return response()->json(['msg'=>'failed']);
		}

		else{

			$inv_type = $request['inv_type'];

			$inv_details = 	$user->investmentDetails()
				->where('investment_status', 0)
				->where('investment_type',$inv_type)
				->get();

			return response()->json(['msg'=>'success','response'=>$inv_details]);

		}
	}

	//function to prepare data for scheme details for scheme performance
	public function showSchemeDetails(Request $request)
	{
		$validator = \Validator::make($request->all(),[
			'scheme_code'=>'required',
		]);

		if ($validator->fails()) {
			return response()->json(['msg'=>'failed']);
		}
		else{

			//dd('hello');

			$scheme_code = $request['scheme_code'];
			$scheme_details_response = array();
			$scheme_details = SchemeHistory::where('scheme_code',$scheme_code)->get();

			foreach($scheme_details as $scheme_detail) {//Log::info($scheme_detail);



				$nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','aesc')->pluck('nav');

				$current_date = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','aesc')->pluck('date');
				$current_nav = $nav[0];

				//$five_day = Carbon::now()->subDays(5)->toDateString();
				//$five_day_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','>=',$five_day)->pluck('date');


				// $one_day_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(1)->toDateString())->orderBy('date','aesc')->first();
				// $one_day_nav  = $one_day_nav['nav'];
				// $one_day_nav = round((($current_nav - $one_day_nav)/$one_day_nav)*100,2);


				// $five_day_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(5)->toDateString())->orderBy('date','aesc')->first();
				// $five_day_nav  = $five_day_nav['nav'];
				// $five_day_nav = round((($current_nav - $five_day_nav)/$five_day_nav)*100,2);



				$thirty_day_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(30)->toDateString())->orderBy('date','aesc')->first();
				$thirty_day_nav  = $thirty_day_nav['nav'];
				$thirty_day_nav = round((($current_nav - $thirty_day_nav)/$thirty_day_nav)*100,2);


				$ninety_day_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(90)->toDateString())->orderBy('date','aesc')->first();
				$ninety_day_nav  = $ninety_day_nav['nav'];
				$ninety_day_nav = round((($current_nav - $ninety_day_nav)/$ninety_day_nav)*100,2);


				$six_month_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(180)->toDateString())->orderBy('date','aesc')->first();
				$six_month_nav  = $six_month_nav['nav'];
				$six_month_nav = round((($current_nav - $six_month_nav)/$six_month_nav)*100,2);


				$one_year_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(365)->toDateString())->orderBy('date','aesc')->first();
				$one_year_nav  = $one_year_nav['nav'];
				$one_year_nav = round((($current_nav - $one_year_nav)/$one_year_nav)*100,2);

				$three_year_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(1095)->toDateString())->orderBy('date','aesc')->first();
				$three_year_nav  = $three_year_nav['nav'];

				if ($three_year_nav == 0) {
					$three_year_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','aesc')->get()->last();
					$three_year_nav = $three_year_nav['nav'];
				}

				$three_year_nav = round((($current_nav - $three_year_nav)/$three_year_nav)*100,2);


				$five_year_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(1825)->toDateString())->orderBy('date','aesc')->first();
				$five_year_nav  = $five_year_nav['nav'];

				if ($five_year_nav == 0) {
					$five_year_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','aesc')->get()->last();
					$five_year_nav = $five_year_nav['nav'];
				}

				//dd(($current_nav-$five_year_nav)/$five_year_nav);
				$five_year_nav = round((($current_nav - $five_year_nav)/$five_year_nav)*100,2);



				$scheme_details_response['name'] = $scheme_detail->name;
	            $scheme_details_response['fund_manager'] = $scheme_detail->fund_manager;
	            $scheme_details_response['asset_size'] = number_format($scheme_detail->asset_size, 0,'',',');
	            $scheme_details_response['exit_load'] = $scheme_detail->exit_load;
	            $scheme_details_response['benchmark'] = $scheme_detail->benchmark;
	            $scheme_details_response['launch_date'] = date('d-m-Y', strtotime($scheme_detail->launch_date));
	            $scheme_details_response['investment_plan'] = $scheme_detail->investment_plan;
	            $scheme_details_response['fund_type'] = $scheme_detail->fund_type;

	            // $scheme_details_response['one_day_nav'] = $one_day_nav;
	            // $scheme_details_response['five_day_nav'] = $five_day_nav;
	            $scheme_details_response['thirty_day_nav'] = $thirty_day_nav;
	            $scheme_details_response['ninety_day_nav'] = $ninety_day_nav;
	            $scheme_details_response['six_month_nav'] = $six_month_nav;
	            $scheme_details_response['one_year_nav'] = $one_year_nav;
	            $scheme_details_response['five_year_nav'] = $five_year_nav;
	            $scheme_details_response['three_year_nav'] = $three_year_nav;
	            $scheme_details_response['current_nav'] = $current_nav;


			}


			return response()->json(['msg'=>'success','response'=>array($scheme_details_response)]);

		}
	}

	//function to save investment notify status
	public function saveNotifyMode(Request $request)
	{
		$user = \Auth::user();
		$validator = \Validator::make($request->all(), [
			'id' => 'required',
			'notif_mode' => 'required'
		]);

		if(!$validator->fails()) {
			$id = intval($request['id']);
			$mode = $request['notif_mode'];
			$notify_type = 0;
			if($mode == 'email') {
				$notify_type = 1;
			} else if( $mode == 'phone') {
				$notify_type = 2;
			}

			if($notify_type != 0) {
				$investment_detail = $user->investmentDetails->find($id);
				if(!empty($investment_detail)) {
					$investment_detail->realise_response = $notify_type;
					$investment_detail->save();
				}
			}
		}
	}

	public function getPaymentType(){

		//var_dump(\Auth::user()->nachDetails);
		if(\Auth::user()->nachDetails->bsemandate_status == 1){
			return 1;
		}else{
			return 2;
		}
 
	}



	public function checkRemainingInvestments($inv_amount,$pass_date){
		$maximum_limit = 100000;
		$pass_date = date('Y-m-d',strtotime($pass_date));
			$already_placed = InvestmentDetails::where('investment_date',$pass_date)->where('user_id',\Auth::user()->id)->where('investment_status',0)->sum('investment_amount');

		$remaining_amount = $maximum_limit - $already_placed;
		//dd($remaining_amount,$inv_amount);

		if ($inv_amount > $remaining_amount) {

			return array(true,$already_placed);
		}else{
			return array(false);
		}
	}



	/*Below Function is for making BSE calls and response. This has nothing to do with Fund selector logic
		whoever it is kindly don't disturb the below code unless you want to work on BSE Stuff. 

	*/


	public function getPassword(){


	    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
	    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
	       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
	       <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
	       <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
	       </soap:Header>
	       
	       <soap:Body>
	          <bses:getPassword>
	             <!--Optional:-->
	             <bses:UserId>1245601</bses:UserId>
	             <!--Optional:-->
	             <bses:Password>'.$this->password.'</bses:Password>
	             <!--Optional:-->
	             <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
	          </bses:getPassword>
	       </soap:Body>
	    </soap:Envelope>';

	    //echo "hello";


	    $result = $this->makeSoapCall($soap_request);
	    // var_dump($result);
	    // die();
	    $result = explode('http://bsestarmf.in/MFOrderEntry/getPasswordResponse', $result);
	    $response = explode('|', $result[1]);

	    //var_dump($response);
	    //echo $response[1];

	    ///preg_match('/<getPasswordResult>-(.*?)-<getPasswordResult>/', $result, $pass_response);


	   //dd($response);

	    $pwd_response =  $response[0];
	    $this->response_key = $response[1];

	    //echo $this->response_key;

	  }

	  

	  public function placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code,$buy_sell,$buy_sell_type,$inv_amount,$folio_number,$int_ref_no,$investment_id){

	    //dd($this->response_key);



	    /*
	          Order Response will be in this order. Kindly don't go through the BSE web file documentation.
	          That will only confuse you. 

	          The order response will be in this format.

	          TRANSACTION_CODE|UNIQUE REFERENCE NUMBER|BSE ORDER NO|USER ID|MEMBER ID|CLIENT CODE|BSE REMARKS|SUCCESS FLAG

	          UNIQUE REFERENCE NUMBER = $unique_ref_no //this should be unique for each order. This will be tracking for the order.
	          BSE ORDER NO //This is generated by BSE
	          TRANSACTION CODE // NEW||MOD||CXL -- New order || Modification || Cancel.
	          SUCCESS FLAG // 0 == ORDER SUCCESS and 1 = ORDER FAILURE *What a Brilliant thing by BSE* 

	    */

	    $response_key = explode('</getPasswordResult></getPasswordResponse></s:Body></s:Envelope>', $this->response_key);



	    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
	    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
	       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
	       <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
	       <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
	       </soap:Header>
	   <soap:Body>
	      <bses:orderEntryParam>
	         <!--Optional:-->
	         <bses:TransCode>'.$transact_mode.'</bses:TransCode>
	         <!--Optional:-->
	         <bses:TransNo>'.$unique_ref_no.'</bses:TransNo>
	         <!--Optional:-->
	         <bses:OrderId></bses:OrderId>
	         <!--Optional:-->
	         <bses:UserID>1245601</bses:UserID>
	         <!--Optional:-->
	         <bses:MemberId>12456</bses:MemberId>
	         <!--Optional:-->
	         <bses:ClientCode>'.$pan.'</bses:ClientCode>
	         <!--Optional:-->
	         <bses:SchemeCd>'.$bse_scheme_code.'</bses:SchemeCd>
	         <!--Optional:-->
	         <bses:BuySell>'.$buy_sell.'</bses:BuySell>
	         <!--Optional:-->
	         <bses:BuySellType>'.$buy_sell_type.'</bses:BuySellType>
	         <!--Optional:-->
	         <bses:DPTxn>P</bses:DPTxn>
	         <!--Optional:-->
	         <bses:OrderVal>'.$inv_amount.'</bses:OrderVal>
	         <!--Optional:-->
	         <bses:Qty></bses:Qty>
	         <!--Optional:-->
	         <bses:AllRedeem>N</bses:AllRedeem>
	         <!--Optional:-->
	         <bses:FolioNo>'.$folio_number.'</bses:FolioNo>
	         <!--Optional:-->
	         <bses:Remarks></bses:Remarks>
	         <!--Optional:-->
	         <bses:KYCStatus>Y</bses:KYCStatus>
	         <!--Optional:-->
	         <bses:RefNo>'.$int_ref_no.'</bses:RefNo>
	         <!--Optional:-->
	         <bses:SubBrCode></bses:SubBrCode>
	         <!--Optional:-->
	         <bses:EUIN>E173580</bses:EUIN>
	         <!--Optional:-->
	         <bses:EUINVal>N</bses:EUINVal>
	         <!--Optional:-->
	         <bses:MinRedeem>N</bses:MinRedeem>
	         <!--Optional:-->
	         <bses:DPC>N</bses:DPC>
	         <!--Optional:-->
	         <bses:IPAdd></bses:IPAdd>
	         <!--Optional:-->
	         <bses:Password>'.$response_key[0].'</bses:Password>
	         <!--Optional:-->
	         <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
	         <!--Optional:-->
	         <bses:Parma1></bses:Parma1>
	         <!--Optional:-->
	         <bses:Param2></bses:Param2>
	         <!--Optional:-->
	         <bses:Param3></bses:Param3>
	      </bses:orderEntryParam>
	   </soap:Body>
	</soap:Envelope>';

	//dd($soap_request);

	    $result = $this->makeSoapCall($soap_request);
	    //echo $result;



	    $response = explode('http://bsestarmf.in/MFOrderEntry/orderEntryParamResponse', $result);
	    $response = explode('|', $response[1]);

	    $trans_code = explode('</a:Action></s:Header><s:Body><orderEntryParamResponse xmlns="http://bsestarmf.in/"><orderEntryParamResult>', $response[0]);
	    $order_response = explode('</orderEntryParamResult></orderEntryParamResponse></s:Body></s:Envelope>', $response[7]);

	    $order_response = array(
	        'trans_code' => $trans_code[1],
	        'unique_ref_no' => $response[1],
	        'order_no' =>$response[2],
	        'client_code' => $response[5],
	        'remarks' =>$response[6],
	        'order_status' => $order_response[0],
	        'inv_id' => $investment_id,
	      );

	    $this->place_order_count++;
	    return $order_response;
	    

	  }

	  public function makeSoapCall($soap_request){

	    //dd($soap_request);
	    $header = array(
	        "Content-type: application/soap+xml;charset=\"utf-8\"",
	        "Accept: application/soap+xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "Content-length: ".strlen($soap_request),
	    );
	    $soap_do = curl_init();
	    curl_setopt($soap_do, CURLOPT_URL,"http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc?singleWsdl" );
	    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($soap_do, CURLOPT_POST,           true );
	    curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
	    curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
	    $result = curl_exec($soap_do);
	    //dd($result);
	    return $result;
	  }


}
