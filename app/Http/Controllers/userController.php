<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PHPExcel_Calculation_Financial;
use App\Http\Controllers\adminController;
use App\Http\Requests;
use App\PersonalDetails;
use App\CorporateDetails;
use App\User;
use App\Directors;
use App\portfolioComp;
use App\PortfolioDetails;
use App\DailyNav;
use App\NomineeDetails;
use App\SchemeDetails;
use App\InvestmentDetails;
use App\NachDetails;
use App\BankDetails;
use App\NavDetails;
use App\Notifications;
use App\ekyc;
use Image;
use Mail;
use Hash;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\historic_bse;
use App\smallcap;
use App\BankInfo;

class userController extends Controller
{


	public function showHome(){

		$user = \Auth::user();
		$TodayDate = Carbon::now()->format('Y-m-d');

		$sensex_day_one = historic_bse::where('date','<=',$TodayDate)->orderBy('date','desc')->first();
		$sensex_day_two = historic_bse::where('date','<',$sensex_day_one['date'])->orderBy('date','desc')->first();
		$sensex_diff = $sensex_day_one['closing_index']-$sensex_day_two['closing_index'];
		$sensex_val = ($sensex_diff/$sensex_day_two['closing_index'])*100;
		$sensex_diff = round($sensex_diff, 2);
		$sensex_val = round($sensex_val, 2);

		$smallcap_day_one = smallcap::where('date','<=',$TodayDate)->orderBy('date','desc')->first();
		$smallcap_day_two = smallcap::where('date','<',$smallcap_day_one['date'])->orderBy('date','desc')->first();
		$smallcap_diff = $smallcap_day_one['close']-$smallcap_day_two['close'];
		$smallcap_val = ($smallcap_diff/$smallcap_day_two['close'])*100;
		$smallcap_diff = round($smallcap_diff, 2);
		$smallcap_val = round($smallcap_val, 2);

		$sensex = ['close_val' => $sensex_day_one['closing_index'],
				   'diff_val' => $sensex_diff,
				   'avg_val' => $sensex_val];
		$smallcap = ['close_val' => $smallcap_day_one['close'],
					 'diff_val' => $smallcap_diff,
					 'avg_val' => $smallcap_val];

		$investment_details = $this->getInvestment_details();

				// dd($smallcap, $sensex);


			//$user_kyc = array(); //initializing user kyc data with empty array
			//$investment_details = array(); //initializing user investment details array
			//$user_video = $user->ekyc()->get();
			//$user_video = $user_video->toArray();

		//if($user->otp_check == 1) {
			//if($user->p_check == "0"){
			/*
				Redirecting the User to "View" according to the user type(Individual or Corporate)
				"0" => Individual
				"1" => Corporate
			*/

				/*if($user->acc_type == "0"){
					return view('users.sign_up');
				}

				if($user->acc_type == "1"){
					$d_count = $user->directors()->count();
					$directors = \Auth::user()->directors()->get();
					return view('users.signup_cor')->with(compact('directors','d_count'));
				}
			}*/

			//if($user->p_check == "1"){

				//$portfolio_details = $user->portfolioComp->first();
				//var_dump($portfolio_details);

				/* Getting all the necessary details from the database to populate in the view*/
				//$eq_amt_inv = $portfolio_details->eq_amt;
				//$debt_amt_inv = $portfolio_details->debt_amt;
				//$bal_amt_inv = $portfolio_details->bal_amt;
				//$total_inv = $eq_amt_inv + $debt_amt_inv + $bal_amt_inv;

				/*Getting all the details from the DB ends*/
				// if($user->p_check == 1){ 
				// 	$user_kyc = $user->nachDetails()->get();
				// 	$user_kyc = $user_kyc->toArray();
				// 	$investment_details = $this->getInvestment_details();
				// }
				// $pan_verified = '';
				// $kyc_check = '';
				// $mandate_received = '';
				// $mandate_cleared = '';
				// $user_video = '';
				// if(!empty($user_kyc)) {
				// 	$pan_verified = $user_kyc[0]['pan_date'];
				// 	$kyc_check = $user_kyc[0]['kyc_date'];
				// 	$mandate_received = $user_kyc[0]['mandate_received'];
				// 	$mandate_cleared = $user_kyc[0]['cleared_date'];
				// }
				// if(is_array($user_video) && is_array(current($user_video))) {
				// 	$user_video = $user_video[0]['video_link'];
				// } else {
				// 	$user_video = '';
				// }

				//print_r($user_kyc);

				

				//dd($investment_details);


				return view('users.version2.index')->with(['user_investment_details' => $investment_details, 'Sensex' => $sensex, 'Smallcap' => $smallcap]);/*->with('pan_verified',$pan_verified)->with('kyc_check',$kyc_check)->with('mandate_received',$mandate_received)->with('mandate_cleared',$mandate_cleared)->with('video_link',$user_video);*//*->with(compact('eq_amt_inv',$eq_amt_inv,'debt_amt_inv',$debt_amt_inv,'bal_amt_inv',$bal_amt_inv,'total_inv',$total_inv))*/;
			//}
		/*} else {
			return view('users.otp');
		}*/
		
	}//showHome() function ends

	public function signUp(){
		$user = \Auth::user();

		//if($user->otp_check == 0) {
			//if($user->p_check == "0"){

			/*
				Redirecting the User to "View" according to the user type(Individual or Corporate)
				"0" => Individual
				"1" => Corporate
			*/

				/*if($user->acc_type == "0"){
					return view('users.sign_up');
				}else if($user->acc_type == "1"){

					$directors = \Auth::user()->directors()->get();
					//dd($directors);
					$d_count = $user->directors()->count();
					//echo $d_count;
					return view('users.signup_cor')->with(compact('directors','d_count'));
				}

			} else {*/
				return redirect('/home');
			//}
		/*} else {
			return view('users.otp');
		}*/

	}//signUp function ends


	public function personalDetails(Request $request){

			/*
					
					The below variable Validates for the input sent from the personal details forms
					and responds with errors if the validation fails. This is to check whether the necessary fields are received in the backened,
					Filterations are done in the frontend itself.

			*/

			//dd($_POST);

			$validator = \Validator::make($request->all(),[

				// 'name' => 'required',
				'pan' => ['required','regex:/^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/'],
				// 'dob' => 'required',
				'aadhar' => 'min:12|max:12',
				// 'fathers-name' => 'required',
				// 'mothers-name' => 'required',
				// 'occupation' => 'required',
				// 'gender' => 'required',
				// 'resident' => 'required',
				// 'marital-status' => 'required',
				// 'address' => 'required',
				'pincode' => 'min:6|max:6',
				// 'nationality' => 'required',
				// 'birth-city' => 'required',
			]);

			/*
					If the validator fails redirects back to the page with errors
			*/

			if ($validator->fails()) {
				
				return response()->json(['msg'=>'error','errors'=>$validator->errors()]);
				
				
				/*

				Below code is the dump the validation errors. Use it when Time comes
				
				*/

				//echo "Validator Fails";
				//dd($validator->errors());
			}

			/*
				If it passes the validation data from the fields are received and stored.
			*/


			else {



				$check_pan = $this->checkPan($request['pan']);
				$check_aadhar = $this->checkAadhar($request['aadhar']);
				$kyc_status = $this->kycStatus($request['pan']);
				/*
					if $check_pan == true then the PAN is already used by somether user. if its false then allow the user to update the pan 

				*/

				//dd($check_pan);
				if($check_pan){
					$pan_error = array('pan' => 'The PAN has already been taken.');
					return response()->json(['msg'=>'error','errors'=>$pan_error]);
				}
				elseif($check_aadhar){
					$aadhar_error = array('pan' => 'The Aadhar has already been taken.');
					return response()->json(['msg'=>'error','errors'=>$aadhar_error]);
				}else{

					//strtoupper($request['pan']);

					$personal_details = new PersonalDetails();

					$personal_details->f_name = $request['name'];
					$personal_details->pan = strtoupper($request['pan']);
					$personal_details->aadhar = $request['aadhar'];
					/*$personal_details->l_name = $request['f-ln-name'];*/
					$personal_details->nationality = $request['nationality'];
					$personal_details->f_s_name = $request['fathers-name'];
					$personal_details->mothers_name = $request['mothers-name'];
					$personal_details->b_city = $request['birth-city'];
					$personal_details->dob = date('Y-m-d',strtotime($request['dob']));
					$personal_details->sex = $request['gender'];
					$personal_details->mar_status = $request['marital-status'];
					$personal_details->resident = $request['resident'];
					$personal_details->address = $request['address'];
					$personal_details->pincode = $request['pincode'];
					$personal_details->occ_type = $request['occupation'];


					$user = \Auth::user();

					//$user->pan = $request['pan'];
					//$update_pan = $user->save();

					//if($update_pan){

						$update_array = array(

								'f_name' => $request['name'],
								'nationality' => $request['nationality'],
								'pan' => strtoupper($request['pan']),
								'aadhar' => $request['aadhar'],
								'f_s_name' => $request['fathers-name'],
								'mothers_name' => $request['mothers-name'],
								'b_city' => $request['birth-city'],
								'dob' => date('Y-m-d',strtotime($request['dob'])),
								'sex' => $request['gender'],
								'mar_status' => $request['marital-status'],
								'resident' => $request['resident'],
								'address' => $request['address'],
								'pincode' => $request['pincode'],
								'occ_type' => $request['occupation'],

							);

						//dd($user->personalDetails()->save($personal_details));
						if ($user->p_check == "1") {

							/*Not updating the pan of the user if the pan is verified and 
							  removing it from the $update_array.
								

							*/

							if($user->nachDetails->pan_status == 1){
								//dd($update_array);
								unset($update_array['pan']);

								//dd($update_array);

								$update_or_save_personal = $user->personalDetails()->update($update_array);
							}else{
								$update_or_save_personal = $user->personalDetails()->update($update_array);
							}
							
						}else{
							$update_or_save_personal = $user->personalDetails()->save($personal_details);
						}
						
						//dd($save_personal);
						if ($update_or_save_personal) {
							//dd($update_or_save_personal);
							$user->p_check = "1";
							if ($user->save()) {

								$portfolio = new portfolioComp();

								$portfolio->eq_amt = 0;
								$portfolio->debt_amt = 0;
								$portfolio->bal_amt = 0;
								$portfolio->goal_amt = 0;

								$user->portfolioComp()->save($portfolio);

								if ($user->p_check == "1") {

									$nominee_details = NomineeDetails::where('user_id',$user->id)->get();
									$nom_count = count($nominee_details);

									
									if ($nom_count != 0) {
										$TodayDate = Carbon::now()->format('Y-m-d');
										if ($kyc_status == 0) {
					                    $update_nach = NachDetails::where('user_id',$user->id)->update([
								                        'pan_status' => 2,
								                        'pan_date' => $TodayDate,
								                        'kycvideo_status' => NULL,
								                        'kycvideo_date' => $TodayDate,
								                        'kyc_status' => 2,
								                        'kyc_date' => $TodayDate,
								                        'camskyc_status'=> NULL,
								                        'camskyc_date' => $TodayDate,
								                        ]);
										}else{
					                    $update_nach = NachDetails::where('user_id',$user->id)->update([
								                        'pan_status' => 1,
								                        'pan_date' => $TodayDate,
								                        'kycvideo_status' => 1,
								                        'kycvideo_date' => $TodayDate,
								                        'kyc_status' => 1,
								                        'kyc_date' => $TodayDate,
								                        'camskyc_status'=> 1,
								                        'camskyc_date' => $TodayDate,
								                        ]);
										}
										// $kycupdate = nachDetails::where('user_id',$user->id)
										// 							->update(['camskyc_status' => $kyc_status, 'camskyc_date' => $TodayDate]);
										return response()->json(['msg'=>'success','response'=>'Your Personal Details have been updated Successfully','kycStatus' => $kyc_status]);
									}else{
										$save_nom = $user->nomineeDetails()->saveMany([
										    new NomineeDetails(['nomi_type' => '1']),
										    new NomineeDetails(['nomi_type' => '2']),
										    new NomineeDetails(['nomi_type' => '3']),
										]);

										//dd($save_nom);

										if ($save_nom) {
											return response()->json(['msg'=>'success','response'=>'Your Personal Details have been updated Successfully','kycStatus' => $kyc_status]);
										}
									}
									
								}else{
									return response()->json(['msg'=>'failure','response'=>'Sorry! Something went wrong ','kycStatus' => $kyc_status]);
								}
								
								//$ekyc = new ekyc();
								//$user->ekyc()->save(new ekyc());
								//$user->nachDetails()->save(new NachDetails(['nach_status' => '0']));
								//return redirect('/home');
								
							}else{
								//if the user->p_check = "1" fails 
								return response()->json(['msg'=>'failure','response'=>'Sorry! Something went wrong ','kycStatus' => $kyc_status]);
							}//Inner Else condition ends





							// Getting the IFSC CODE and storing in bank details table.

							// $client = new Client(['base_uri' => 'http://api.techm.co.in/api/v1/ifsc/']);
							// $ifsc_caps = strtoupper($request['ifsc-code']);

				   //          $response = $client->request('GET',$ifsc_caps);
				   //          $contents = $response->getBody();
				   //          $response = json_decode($contents,true);



				   //          $response = $response['data'];
				   //          //print_r($response);

				   //          $save_bank = new BankDetails();

				   //          $state = $response['STATE'];
				   //          $save_bank->bank_name = $response['BANK'];
				   //          $save_bank->branch_name = $response['BRANCH'];
				   //          $save_bank->address = $response['ADDRESS'];
				   //          $save_bank->city = $response['DISTRICT'];
				   //          $save_bank->state = $response['STATE'];
				   //          $save_bank->bank_type = '1';
				   //          $save_bank->acc_no = $request['f-s-acc-no'];

							// $save_bank->ifsc_code = $request['ifsc-code'];
							// $save_bank->acc_name = $request['f-s-acc-name'];

							// $user->BankDetails()->save($save_bank);



							//Below is assigning his/portfolio to Zero

						// }else{
						// 	return response()->json(['msg'=>'failure','response'=>'Oops ! Personal Details Cannot be saved right now. Kindly Check in sometime']);
						// }

					}else{
						return response()->json(['msg'=>'failure','response'=>'Sorry! Your Personal Details cannot be saved right now.']);
					}

				}


				
				
			}// Outer else condition ends

	}//personalDetails() function ends


	public function checkPan($pan){


		$stored_pan = PersonalDetails::where('user_id','!=',\Auth::user()->id)->pluck('id','pan');
		//dd($stored_pan);
		if(array_key_exists($pan, $stored_pan)){
			return true;
		}else{
			return false;
		}
	}

	public function checkAadhar($aadhar){


		$stored_aadhar = PersonalDetails::where('user_id','!=',\Auth::user()->id)->pluck('id','aadhar');
		//dd($stored_pan);
		if(array_key_exists($aadhar, $stored_aadhar)){
			return true;
		}else{
			return false;
		}
	}


	public function getActiveDetails(Request $request){

		// if($request['type'] == 'p'){
			
		// }
		// if($request['type'] == 'b'){
		// 	$user_details = \Auth::user()->bankDetails;
		// }

		$user_details = \Auth::user()->personalDetails;

		
		return response()->json(['msg'=>'success','response'=>$user_details]);
	}


	public function saveNewBankDetails(Request $request){

		$mandate_file_name = '';
		$cc_file_name = '';
		$validator = \Validator::make($request->all(),[

				'acc-name' => 'required',
				'bank-name' => 'required',
				// 'acc-no' => 'required',
				// 'acc-type' => 'required',
				// 'ifsc-code' => 'required',
				// 'cancelled-cheque' => 'image|mimes:jpeg,png,jpg|max:3072',
				// 'mandate'=>'image|mimes:jpeg,png,jpg|max:3072',
			]);

			/*
					If the validator fails redirects back to the page with errors
			*/

			if ($validator->fails()) {
				// dd('validation fails');
				return response()->json(['msg'=>'error','errors'=>$validator->errors()]);
				
			}

			/*
				If it passes the validation data from the fields are received and stored.
			*/


			else {
				// dd('validation Passed');

				//print_r($_POST);
				//die();

				// if($request->hasFile('cancelled-cheque')){
				// 	$cancelled_cheque = $request->file('cancelled-cheque');
				// 	$cc_file_name = \Auth::user()->id.'-cc-'.str_random().'.'. $cancelled_cheque->getClientOriginalExtension();
    //             	$new_file = $request->file('cancelled-cheque')->move(base_path() . '/public/files/cc/', $cc_file_name);
					//$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());
				// }
				// else{
				// 		return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
				// }

				// if($request->hasFile('mandate')){
				// 	$mandate = $request->file('mandate');
				// 	$mandate_file_name = \Auth::user()->id.'-mandate-'.str_random().'.'. $mandate->getClientOriginalExtension();
    //             	$new_file = $request->file('mandate')->move(base_path() . '/public/files/mandates/', $mandate_file_name);
					//$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());
				// }
				// else{
				// 		return response()->json(['msg'=>'failure','response'=>'Mandate Upload Failed. Check Your internet Connection.']);
				// }

				//die();

				//dd(base_path().'\files\cc');
				$user = \Auth::user();
				$bank_details = $user->bankDetails;

				//dd($bank_details);

				$bank_name = BankInfo::where('bank_id',$request['bank-name'])->pluck('bank_name');
				// print_r($bank_name[0]);
				// die();
				
				if (count($bank_details) == 0) {
					$bank_details = new BankDetails();
					$bank_details->acc_name = $request['acc-name'];
					$bank_details->bank_id = $request['bank-name'];
					//$bank_details->bank_name = $bank_name[0];
					$bank_details->acc_no = $request['acc-no'];
					$bank_details->acc_type = $request['acc-type'];
					$bank_details->ifsc_code = $request['ifsc-code'];

					$save_bank = $user->bankDetails()->save($bank_details);

					if($save_bank){
						$user->b_check = 1;
						$update_bank_status = $user->save();
						return response()->json(['msg'=>'success','response'=>'Your Bank Details have been updated Successfully']);

						// if($user->ekyc->cc_link != ''){
						// 	$cc_file = $user->ekyc->cc_link;
						// 	if(file_exists(base_path().'/public/files/cc/'.$cc_file)){
						// 		unlink(base_path().'/public/files/cc/'.$cc_file);
						// 	}
							
							
						// }
						// if($user->ekyc->mandate_link != ''){
						// 	$mandate_file = $user->ekyc->mandate_link;
						// 	if(file_exists(base_path().'/public/files/mandates/'.$mandate_file)){
						// 		unlink(base_path().'/public/files/mandates/'.$mandate_file);
						// 	}
						// }

						//$save_files = $user->ekyc()->update()

						// $ekyc_details = $user->ekyc;

						// if(count($ekyc_details) == 0){

						// 	$files = new ekyc();
						// 	$files->mandate_link = $mandate_file_name;
						// 	$files->cc_link = $cc_file_name;

						// 	$save_file = $user->ekyc()->save($files);

						// 	if($save_file){


						// 		$update_bank_status = \Auth::user()->update(['b_check'=>1]);

						// 		if($update_bank_status){
						// 				return response()->json(['msg'=>'success','response'=>'Your Bank Details have been updated Successfully']);
						// 		}else{
						// 				return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 		}
						// 	}else{
						// 		return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 	}

						// }else{

						// 	$update_ekyc = $user->ekyc()->update(['mandate_link'=>$mandate_file_name,'cc_link'=>$cc_file_name]);

						// 	if($update_ekyc){

						// 		$update_bank_status = \Auth::user()->update(['b_check'=>1]);

						// 		if($update_bank_status){
						// 				return response()->json(['msg'=>'success','response'=>'Your Bank Details have been updated Successfully']);
						// 		}else{
						// 				return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 		}
						// 	}else{
						// 		return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 	}

						// }
						
					}else{
						return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
					}

				}else{

					$update_bank = $user->bankDetails()->update(['acc_name'=> $request['acc-name'],'acc_no'=> $request['acc-no'],'acc_no'=> $request['acc-no'],'acc_type'=>$request['acc-type'],'ifsc_code'=>$request['ifsc-code'],'bank_id'=>$request['bank-name']]);

					if($update_bank){
						$user->b_check = 1;
						$update_bank_status = $user->save();
						return response()->json(['msg'=>'success','response'=>'Your Bank Details have been updated Successfully']);

						//dd($user->ekyc);

						// if($user->ekyc->cc_link != ''){
						// 	$cc_file = $user->ekyc->cc_link;
						// 	if(file_exists(base_path().'/public/files/cc/'.$cc_file)){
						// 		unlink(base_path().'/public/files/cc/'.$cc_file);
						// 	}
							
							
						// }
						// if($user->ekyc->mandate_link != ''){
						// 	$mandate_file = $user->ekyc->mandate_link;
						// 	if(file_exists(base_path().'/public/files/mandates/'.$mandate_file)){
						// 		unlink(base_path().'/public/files/mandates/'.$mandate_file);
						// 	}
						// }

						

						// $ekyc_details = $user->ekyc;

						// if(count($ekyc_details) == 0){



						// 	$files = new ekyc();
						// 	$files->mandate_link = $mandate_file_name;
						// 	$files->cc_link = $cc_file_name;

						// 	$save_file = $user->ekyc()->save($files);

						// 	if($save_file){

						// 		//$user = new User();
						// 		$user->b_check = 1;
						// 		$update_bank_status = $user->save();

						// 		if($update_bank_status){

						// 				NachDetails::where('user_id',$user->id)->update([
						// 					'cc_status'=> '','mandatesoft_status'=> '']);
						// 				return response()->json(['msg'=>'success','response'=>'Your Bank Details are updated Successfully']);
						// 		}else{
						// 				return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 		}
						// 	}else{
						// 		return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 	}

						// }else{

						// 	if ($mandate_file_name != '' && $cc_file_name != '') {
						// 		$update_ekyc = $user->ekyc()->update(['mandate_link'=>$mandate_file_name,'cc_link'=>$cc_file_name]);
						// 	}elseif ($mandate_file_name != '') {
						// 		$update_ekyc = $user->ekyc()->update(['mandate_link'=>$mandate_file_name]);
						// 	}elseif ($cc_file_name != '') {
						// 		$update_ekyc = $user->ekyc()->update(['cc_link'=>$cc_file_name]);
						// 	}else{
						// 		// print_r("error");
						// 		// die();
						// 		$update_bank_status = User::where('id',\Auth::user()->id)->update(['b_check'=>1]);
						// 		if ($update_bank_status) {
						// 			return response()->json(['msg'=>'success','response'=>'Your Bank Details are updated Successfully']);
						// 		}
						// 		else{
						// 			return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 		}
						// 	}

						// 	if($update_ekyc){
						// 		$update_bank_status = User::where('id',\Auth::user()->id)->update(['b_check'=>1]);

						// 		if($update_bank_status){
						// 			NachDetails::where('user_id',$user->id)->update([
						// 					'cc_status'=> '','mandatesoft_status'=> '']);
						// 				return response()->json(['msg'=>'success','response'=>'Your Bank Details are updated Successfully']);
						// 		}else{
						// 				return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 		}
						// 	}else{
						// 		return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
						// 	}


						// }
						

					}else{
						return response()->json(['msg'=>'failure','response'=>'Your Bank details cannot be updated. Try again Later']);
					}
					
				}
			
			}
	}

	public function userDocument(Request $request){
		$user = \Auth::user();
		$ekyc_details = $user->ekyc;
		if($request->hasFile('cancelled-cheque')){
			$cancelled_cheque = $request->file('cancelled-cheque');
			$cc_file_name = \Auth::user()->id.'-cc-'.str_random().'.'. $cancelled_cheque->getClientOriginalExtension();
        	$new_file = $request->file('cancelled-cheque')->move(base_path() . '/public/files/cc/', $cc_file_name);

			if($user->ekyc->cc_link != ''){
				$cc_file = $user->ekyc->cc_link;
				if(file_exists(base_path().'/public/files/cc/'.$cc_file)){
					unlink(base_path().'/public/files/cc/'.$cc_file);
				}
			}

				$update_ekyc = $user->ekyc()->update(['cc_link'=>$cc_file_name]);
				if ($update_ekyc) {
					$update_bank_status = $user->update(['b_check'=>1]);
					NachDetails::where('user_id',$user->id)->update(['cc_status'=>NULL,'cc_date'=>date('Y-m-d',strtotime(Carbon::now()))]);
					if ($update_bank_status) {
						return response()->json(['msg'=>'success','state'=>'1']);
					}
				}		

		}elseif ($request->hasFile('mandate')) {
			$mandate = $request->file('mandate');
			$mandate_file_name = \Auth::user()->id.'-mandate-'.str_random().'.'. $mandate->getClientOriginalExtension();
        	$new_file = $request->file('mandate')->move(base_path() . '/public/files/mandates/', $mandate_file_name);

			if($user->ekyc->mandate_link != ''){
				$mandate_file = $user->ekyc->mandate_link;
				if(file_exists(base_path().'/public/files/mandates/'.$mandate_file)){
					unlink(base_path().'/public/files/mandates/'.$mandate_file);
				}
			}
			$update_ekyc = $user->ekyc()->update(['mandate_link'=>$mandate_file_name]);
			if ($update_ekyc) {
				$update_bank_status = $user->update(['b_check'=>1]);
				NachDetails::where('user_id',$user->id)->update(['mandatesoft_status'=>NULL,'mandatesoft_date'=>date('Y-m-d',strtotime(Carbon::now()))]);
				if ($update_bank_status) {
					return response()->json(['msg'=>'success','state'=>'2']);
				}
			}
		}else{
			return response()->json(['msg'=>'error']);
		}
			
	}


	public function corporateDetails(Request $request){

		/*
					
					The below variable Validates for the input sent from the Corporate details forms
					and responds with errors if the validation fails. This is to check whether the necessary fields are received in the backened,
					Filterations are done in the frontend itself.

			*/

			$validator = \Validator::make($request->all(),[

				'doi' => 'required',
				'poi' => 'required',
				'cin' => 'required',
				'org'   => 'required',
				'f-s-acc-name' => 'required',
				'f-s-acc-no' => 'required',
				'ifsc-code' => 'required',

			]);

			/*
					If the validator fails redirects back to the page with errors
			*/

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
				/*

				Below code is the dump the validation errors. Use it when Time comes
				
				*/

			}

			/*
				If it passes the validation data from the fields are received and stored.
			*/


			else {

				$corp_details = new CorporateDetails();

				$corp_details->doi = $request['doi'];
				$corp_details->poi = $request['poi'];
				$corp_details->cin = $request['cin'];
				$corp_details->org = $request['org'];
				$corp_details->acc_name = $request['f-s-acc-name'];
				$corp_details->acc_no = $request['f-s-acc-no'];
				$corp_details->ifsc_code = $request['ifsc-code'];


				$user = \Auth::user();

				if ($user->corporateDetails()->save($corp_details)) {
					
					$user->p_check = "1";
					$user->save();
					$user->nomineeDetails()->saveMany([
					    new nomineeDetails(['nomi_type' => '1']),
					    new nomineeDetails(['nomi_type' => '2']),
					    new nomineeDetails(['nomi_type' => '3']),
					]);
					return redirect('/home');
				}
				else{
					return redirect()->back();
				}//Inner Else condition ends

			}// Outer else condition ends

	}


	public function addDirector(Request $request){


		/*
					
					The below variable Validates for the input sent from the Add direcor forms
					and responds with errors if the validation fails. This is to check whether the necessary fields are received in the backened,
					Filterations are done in the frontend itself.

			*/

			$validator = \Validator::make($request->all(),[

				'dir-name' => 'required',
				'dir-pan' => 'required',
				'dir-din' => 'required',
				'dir-addr'   => 'required',
				'dir-pic-file' => 'required',

			]);

			/*
					If the validator fails redirects back to the page with errors
			*/

			if ($validator->fails()) {
				
				//return redirect()->back()->withErrors($validator->errors());
				return dd($validator->errors());
				
				/*

				Below code is the dump the validation errors. Use it when Time comes
				
				*/

				//echo "Validator Fails";
				//dd($validator->errors());
			}

			/*
				If it passes the validation data from the fields are received and stored.
			*/


			else {


				$directors = new Directors();

				$directors->dir_name = $request['dir-name'];
				$directors->dir_pan = $request['dir-pan'];
				$directors->dir_din = $request['dir-din'];
				$directors->dir_addr = $request['dir-addr'];

					/*if( (Directors::where('dir_din',$request['dir-din'])->count())>= 1){
						return response()->json(['msg' => 'din_exists']);
						//echo "din_exists";
					}else{*/

						if ($request->hasFile('dir-pic-file')) {
							$dir_image = $request->file('dir-pic-file');
							$filename = time().'.'.$dir_image->getClientOriginalExtension();                           
	                        Image::make($dir_image)->resize(200,200)->save('assets/dir_images/'.$filename);
	                        $user=\Auth::user();
	                        $directors->dir_img = $filename;
                        
	                        if ($user->directors()->save($directors)) {
	                        	$d_count = $user->directors()->count();
	                        	return response()->json(['msg'=>'1','director'=>$directors,'d_count'=>$d_count]);
	                        }
	                        else{
	                        	return "failure";
	                        }
					 	}

					 	else{

							return "no-file";
						}

					//}

					

					


			}// Outer else condition ends

	}//addDirector() function ends


	public function checkDirector(){

		$user = \Auth::user();
		$d_count = $user->directors()->count();

		return response()->json(['d_count'=>$d_count]);

	}


	public function removeDirector(Request $request){

		/*Validation to check whether the ID is received from ajax request*/

		$validator = \Validator::make($request->all(),[

				'dir_id' => 'required',

		]);

		if ($validator->fails()) {
				
				//return redirect()->back()->withErrors($validator->errors());
				return response()->json(['msg'=>'0']);
				
				/*

				Below code is the dump the validation errors. Use it when Time comes
				
				*/

				//echo "Validator Fails";
				//dd($validator->errors());
			}

			/*
				If it passes the validation data from the fields are received and stored.
			*/

		else{

			$dir_id = $request['dir_id'];

			if(\Auth::user()->directors()->findOrFail($dir_id)->delete()){
				return response()->json(['msg'=>'1','dir_id'=>$dir_id]);
			}

			else{
				return response()->json(['msg'=>'0']);
			}
		}

	}//removeDirector() function ends

	
	public function showFundSelector(){

		$schemes = SchemeDetails::where('scheme_status',1)->get();
		//dd($schemes);
		session_start();
		// $_SESSION['inv_id'] = 228;
		// dd($_SESSION);
		if (isset($_SESSION['inv_id'])) {
			
			$updated_portfolio = PortfolioDetails::where('investment_id',$_SESSION['inv_id'])->get();
			return view('users.version2.fund-selector')->with(compact('ot_count','f_count','sip_count','schemes','updated_portfolio'));
		}

		return view('users.version2.fund-selector')->with(compact('ot_count','f_count','sip_count','schemes'));

		// if (\Auth::user()->nachDetails->cleared_date == "") {

		// 	$ot_count = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_type',0)->count();
		// 	$f_count = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_type',1)->count();
		// 	$sip_count = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_type',2)->count();

		// 	return view('users.fund_selector_d')->with(compact('ot_count','f_count','sip_count'));
		// }

		// else{
			
		// 	$ot_count = InvestmentDetails::where('user_id',\Auth::user()->id)
		// 		->where('investment_type',0)
		// 		->where('investment_status', 0)
		// 		->count();
		// 	$f_count = InvestmentDetails::where('user_id',\Auth::user()->id)
		// 		->where('investment_type',1)
		// 		->where('investment_status', 0)
		// 		->count();
		// 	$sip_count = InvestmentDetails::where('user_id',\Auth::user()->id)
		// 		->where('investment_type',2)
		// 		->where('investment_status', 0)
		// 		->count();

		// 	return view('users.version2.fund-selector')->with(compact('ot_count','f_count','sip_count'));
		// }
	}


	public function showFundSelector1(){

		if (\Auth::user()->nachDetails->cleared_date == "") {

			$ot_count = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_type',0)->count();
			$f_count = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_type',1)->count();
			$sip_count = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_type',2)->count();

			return view('users.fund_selector_d')->with(compact('ot_count','f_count','sip_count'));
		}

		else{
			
			$ot_count = InvestmentDetails::where('user_id',\Auth::user()->id)
				->where('investment_type',0)
				->where('investment_status', 0)
				->count();
			$f_count = InvestmentDetails::where('user_id',\Auth::user()->id)
				->where('investment_type',1)
				->where('investment_status', 0)
				->count();
			$sip_count = InvestmentDetails::where('user_id',\Auth::user()->id)
				->where('investment_type',2)
				->where('investment_status', 0)
				->count();

			return view('users.fund_selector1')->with(compact('ot_count','f_count','sip_count'));
		}
	}


	//function to show preference
	public function preference()
	{
		$user = \Auth::user();
		$personal_details = $user->personalDetails()->get();
		$bank_details = $user->bankDetails()->get();
		//dd($bank_details);
		$nom_count = $user->nomineeDetails->count();
		//dd($nom_count);
		//return view('users.preference');

		if($user->nachDetails->nach_status == 1){
			//dd(\Auth::user()->bankDetails->where('bank_type',0)->first());
			return view('users.version2.personal-details')->with(compact('nom_count'));	
		}else{
			return $this->activateAccount();
		}
		
	}


		//function to show account activation
	public function activateAccount()
	{
		$user = \Auth::user();

		// if ($user->nachDetails->nach_status == 1) {
		// 	return $this->preference();
		// }else{
			$name = $user->name;
		 	$personal_details = $user->personalDetails()->get();
		 	$bank_details = $user->bankDetails()->get();
		 	$bank_info = BankInfo::all()->toArray();
			// if($user->p_check == 1) {
			// 	return redirect('/home');
			// } else {
			// 	return view('users.version2.activate-account');//return view('users.sign_up');
			// }

			usort($bank_info, function($a, $b){ return strcmp($a['bank_name'], $b['bank_name']); });
			// dd($bank_details);
			return view('users.version2.activate-account')->with(compact('personal_details','bank_details','name','bank_info'));
			//}
		
		
	}

	public function storeProfileImage(Request $request){
		$user = \Auth::user();
		$validator = \Validator::make($request->all(),[
			'avatar'	=> 'required',
		]);

		if ($validator->fails()) {
			
			//return redirect('preference')->with('errors',$validator->errors());
			return response()->json(['msg'=>'error','errors'=>$validator->errors()]);
		} else{ 

			if ($request->hasFile('avatar')) {
				$personal_details = $user->PersonalDetails;
				$profile_image = $request->file('avatar');
				$filename = \Auth::user()->id.'-'.time().'.'.$profile_image->getClientOriginalExtension();                           
	            Image::make($profile_image)->resize(200,200)->save('assets/profile_images/'.$filename);
	            $update = PersonalDetails::where('user_id',\Auth::user()->id)->update(['profile_img'=>$filename]);

	            if($update){
	            	return response()->json(['msg'=>'1','response'=>$filename]);
	            }
	            else{
	            	return response()->json(['msg'=>'0','response'=>'Sorry Cannot update your image right now']);
	            }
			}

		}
	}
	
	//function to update user details
	public function saveUserDetails(Request $request)
	{
		$user = \Auth::user();
		$validator = \Validator::make($request->all(),[
			'name'	=> 'required|max:255',
			'email' => 'required|email|max:255',
			'mobile' => 'required|min:10|max:10',
			'aadhar' => 'required|min:12|max:12',
			'address' => 'required',
			'pincode' => 'required|min:6|max:6',
		]);
		if ($validator->fails()) {
			
			//return redirect('preference')->with('errors',$validator->errors());
			return response()->json(['msg'=>'error','errors'=>$validator->errors()]);
		} else { 
			if($user->email !== $request['email']) {
				$changeemailvalidator = \Validator::make($request->all(),[
					'email' => 'unique:users',
				]);
				if ($changeemailvalidator->fails()) {
					return response()->json(['msg'=>'error','errors'=>$changeemailvalidator->errors()]);
				}
			}
			if($user->mobile !== $request['mobile']) {
				$changemobilevalidator = \Validator::make($request->all(),[
					'mobile' => 'unique:users',
				]);
				
				if ($changemobilevalidator->fails()) {
					return response()->json(['msg'=>'error','errors'=>$changemobilevalidator->errors()]);
				}
			}
			if($user->personalDetails->aadhar !== $request['aadhar']) {
				$changeaadharvalidator = \Validator::make($request->all(),[
					'aadhar' => 'unique:personal_details',
				]);
				
				if ($changeaadharvalidator->fails()) {
					return response()->json(['msg'=>'error','errors'=>$changeaadharvalidator->errors()]);
				}
			}
		}		
		
		$personal_details = $user->PersonalDetails;		
		// dd($request['address']);
		$user->name = $request['name'];
		$user->email = $request['email'];
		$user->mobile = $request['mobile'];
		$personal_details->aadhar = $request['aadhar'];
		$personal_details->address = $request['address'];
		$personal_details->pincode = $request['pincode'];
		// $personal_details->profile_img = $request['avatar'];
		$save_status = true;
		// if ($request->hasFile('avatar')) {
		// 	$profile_image = $request->file('avatar');
		// 	$filename = time().'.'.$profile_image->getClientOriginalExtension();                           
  //           Image::make($profile_image)->resize(200,200)->save('assets/profile_images/'.$filename);
  //           $personal_details->profile_img = $filename;
		// }
		if($personal_details->save()) {
        	$save_status = true;
        } else {
        	$save_status = false;
        }
		if($user->save()) {
			$save_status = true;
        } else {
        	$save_status = false;
        }
        if($save_status) {
        	return response()->json(['msg'=>'success','response'=>'Your details has been updated Successfully.']);
        } else {
        	return response()->json(['msg'=>'fail','response'=>'Something went wrong. Please try again']);
        }
	}

	//function to update user details
	public function saveBankDetails(Request $request)
	{
		$user = \Auth::user();
		$bank_details = $user->BankDetails->where('bank_type',1)->first();
		$validator = \Validator::make($request->all(),[
			'bank_name'	=> 'required',
			'bank_number' => 'required',
			'bank_ifsc' => 'required|min:11|max:11',
			'bank_account_type' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['msg' => 'error', 'errors' => $validator->errors()]);
		}

		$bank_details->acc_name = $request['bank_name'];
		$bank_details->acc_no = $request['bank_number'];
		$bank_details->ifsc_code = $request['bank_ifsc'];
		$bank_details->acc_type = $request['bank_account_type'];
		if($user->bankDetails()->save($bank_details)) {
        	return response()->json(['msg' => 'success', 'save_status' => 'Bank details updated successfully']);
        } else {
        	return response()->json(['msg' => 'fail', 'save_status' => 'Update Bank details failed. Please check and try again']);
        }
	}


	public function changePassword(Request $request){
		$validator = \Validator::make($request->all(),[
			'current-password'	=> 'required',
			'new-password' => 'required',
			'confirm-password' => 'required',

		]);
		if ($validator->fails()) {
			return response()->json(['msg' => 'error','errors' => $validator->errors()]);
		}
		else{

			$current_pass = $request['current-password'];
			$new_pass = $request['new-password'];
			$confirm_pass = $request['confirm-password'];

			$new_pass = bcrypt($new_pass);

			if(\Hash::check($current_pass,\Auth::user()->password)){

				//echo "Matching same";
				$changed = User::where('id',\Auth::user()->id)->update(['password'=>$new_pass]);

				if($changed){
					return response()->json(['msg'=>'success','response' => 'Password Changed Successfully']);
				}
			}
			else{

				//echo $current_pass;
				//echo \Auth::user()->password;

				return response()->json(['msg'=>'failure','response' => 'Password Update Failed']);
			}
		}
	}

	//function to save/update nominee details
	public function saveNominee1Details(Request $request)
	{
		$validator = \Validator::make($request->all(),[
			'nom1-name'	=> 'required',
			'nom1-relationship' => 'required',
			'nom1-dob' => 'required',
			'nom1-address' => 'required',
			'nom1-perc' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['msg' => 'error','errors' => $validator->errors()]);
		}

		$nominee_details = \Auth::user()->nomineeDetails()->where('nomi_type','2')->pluck('nomi_holding');
		$nominee1_details = \Auth::user()->nomineeDetails()->where('nomi_type','1')->get()->toArray();
		$nominee1_details[0]['nomi_dob'] = date('d-m-Y',strtotime($nominee1_details[0]['nomi_dob']));
		$nom2_holding = $nominee_details[0];
		//dd($nom2_holding + $request['nom1-perc']);

		if (($nom2_holding + $request['nom1-perc']) > 100) {
			return response()->json(['msg' => 'failure', 'response' => 'Your Nominee Allocation cannot be more than 100%','nomi_details'=>$nominee1_details]);
		}

		$details = array(
			'nom_type' => '1',
			'name' => $request['nom1-name'],
			'relationship' =>$request['nom1-relationship'],
			'dob' => $request['nom1-dob'],
			'address' => $request['nom1-address'],
			'holding' => $request['nom1-perc'],
		);


		

		if($this->saveNomineeDetails($details)) {
        	return response()->json(['msg' => 'success', 'response' => 'Nominee details updated successfully']);
        } else {
        	return response()->json(['msg' => 'failure', 'response' => 'Update Nominee details failed. Please check and try again','nomi_details'=>$nominee1_details]);
        }

	}

	//function to save/update nominee details
	public function saveNominee2Details(Request $request)
	{
		$validator = \Validator::make($request->all(),[
			'nom2-name'	=> 'required',
			'nom2-relationship' => 'required',
			'nom2-dob' => 'required',
			'nom2-address' => 'required',
			'nom2-perc' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['msg' => 'error','errors' => $validator->errors()]);
		}

		$nominee_details = \Auth::user()->nomineeDetails()->where('nomi_type','1')->pluck('nomi_holding');
		$nominee2_details = \Auth::user()->nomineeDetails()->where('nomi_type','2')->get()->toArray();
		$nominee2_details[0]['nomi_dob'] = date('d-m-Y',strtotime($nominee2_details[0]['nomi_dob']));
		$nom1_holding = $nominee_details[0];

		if (($nom1_holding + $request['nom2-perc']) > 100) {
			return response()->json(['msg' => 'failure', 'response' => 'Your Nominee Allocation cannot be more than 100%','nomi_details'=>$nominee2_details]);
		}

		$details = array(
			'nom_type' => '2',
			'name' => $request['nom2-name'],
			'relationship' =>$request['nom2-relationship'],
			'dob' => $request['nom2-dob'],
			'address' => $request['nom2-address'],
			'holding' => $request['nom2-perc'],
		);
		if($this->saveNomineeDetails($details)) {
        	return response()->json(['msg' => 'success', 'response' => 'Nominee details updated successfully']);
        } else {
        	return response()->json(['msg' => 'failure', 'response' => 'Update Nominee details failed. Please check and try again','nomi_details'=>$nominee1_details]);
        }
	}

	//function to save/update nominee details
	public function saveNominee3Details(Request $request)
	{
		$validator = \Validator::make($request->all(),[
			'nominee3_name'	=> 'required',
			'nominee3_relationship' => 'required',
			'nominee3_dob' => 'required',
			'nominee3_address' => 'required',
			'nominee3_percent' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['msg' => 'error','errors' => $validator->errors()]);
		}

		$nominee_details = \Auth::user()->nomineeDetails()->pluck('nomi_holding')->toArray();
		$nom_sum = array_sum($nominee_details);

		if (($nom_sum + $request['nom1-perc']) > 100) {
			return response()->json(['msg' => 'failure', 'response' => 'Your Nominee Allocation cannot be more than 100%']);
		}

		$details = array(
			'nom_type' => '3',
			'name' => $request['nominee3_name'],
			'relationship' =>$request['nominee3_relationship'],
			'dob' => $request['nominee3_dob'],
			'address' => $request['nominee3_address'],
			'holding' => $request['nominee3_percent'],
		);
		if($this->saveNomineeDetails($details)) {
        	return response()->json(['msg' => 'success', 'save_status' => 'Nominee details updated successfully']);
        } else {
        	return response()->json(['msg' => 'fail', 'save_status' => 'Update Nominee details failed. Please check and try again']);
        }
	}


	//function to save nominee details
	public function saveNomineeDetails($details)
	{
		$user = \Auth::user();
		$nomi_details = $user->NomineeDetails->where('nomi_type', $details['nom_type'])->first();
		$nomi_details->nomi_name = $details['name'];
		$nomi_details->nomi_relationship = $details['relationship'];
		$nomi_details->nomi_dob = date('Y-m-d', strtotime($details['dob']));
		$nomi_details->nomi_addr = $details['address'];
		$nomi_details->nomi_holding = $details['holding'];
		if($user->nomineeDetails()->save($nomi_details)) {
        	return true;
        } else {
        	return false;
        }
	}


	public function showFunds(Request $request){

			$validator = \Validator::make($request->all(),[
				'p_type' => 'required',
				'inv_amount' => 'required',
				]);

			if ($validator->fails()) {
				
				//return response()->json(['msg'=>'val_fail']);
				return response()->json(['msg'=>$validator->errors()]);
			}

			else{

				$port_type = $request['p_type'];
				$inv_amount = $request['inv_amount'];

					//checking the selected portfolio type

				if($port_type == 'Conservative'){

					$user = \Auth::user();


					$split_amount = array();
					$scheme_suggestions = array(); //Variable which will hold suggested Scheme 
					$base_amount = 0; //Amount which will be invested in each schemes
					$new_minimum_fund_amount = 5000; // Minimum fund amount for first time investment
					$schemes_invested = array(); // Variable will hold the schemes user has invested in.
					$excess_amount = 0; //Variable which will hold excess amount after splitting

					//Getting All the debt funds schemes  
					$debt_funds = SchemeDetails::where('scheme_type','debt')->get(); 

					//echo "john";
					return $debt_funds;					//Counting the number of Debt funds
					$debt_funds_count = count($debt_funds);
					

					
					//Getting scheme codes of the schemes user has already invested in and converting it to an array
					$invested_scheme_codes = $user->portfolioDetails()->groupBy('scheme_code')->pluck('scheme_code');
					$invested_scheme_codes = $invested_scheme_codes->toArray();

					//echo count($invested_scheme_codes);

					/*Checking if the user is investing for the first time or not*/
					if(count($invested_scheme_codes) == 0){  //New investor

						//If amount exceeds filling all the funds with minimum funds amount() then increasing the base amount value.
						//this below if will get executed only if the investment amount > the total no debt funds * 5000
						if($inv_amount > ($debt_funds_count * $new_minimum_fund_amount)) {

							$without_excess_amount = $inv_amount -($inv_amount % $new_minimum_fund_amount);
							$base_amount = $without_excess_amount / $debt_funds_count;
							$excess_amount = $inv_amount - $without_excess_amount;
						} 

						// if the investment amount < the total debt funds amount * 5000

						else {
							$base_amount = $new_minimum_fund_amount;
							$excess_amount = $inv_amount % $base_amount;
						}

					} //New user if condition ends

					//The below else will get executed if the user has alread invested

					else { 

						//checking if all the debt funds schemes are filled with first investment.

						if(count($invested_scheme_codes) == $debt_funds_count){
							$without_excess_amount = $inv_amount -($inv_amount % $new_minimum_fund_amount);
							$base_amount = $without_excess_amount / $debt_funds_count;
							$excess_amount = $inv_amount - $without_excess_amount;
						}

						//if debt funds schemes are partially filled 

						else{

							//If amount exceeds filling all the funds with minimum fund amount(i.e 5000) then increasing the base amount value.
							//this below if will get executed only if the investment amount > the total no debt funds * 5000


							if($inv_amount > ($debt_funds_count * $new_minimum_fund_amount)) {

							$without_excess_amount = $inv_amount -($inv_amount % $new_minimum_fund_amount);
							$base_amount = $without_excess_amount / $debt_funds_count;
							$excess_amount = $inv_amount - $without_excess_amount;
							} //if ends


							// if the investment amount < the total debt funds amount * 5000

							else {
								$base_amount = $new_minimum_fund_amount;
								$excess_amount = $inv_amount % $base_amount;
							}//else ends

						}//else ends


					} //Else ends

					/*Separating round investment amount from original investment amount
						i.e 25000 from 26000 and excess amount will have 1000

					*/

					$inv_amount = $inv_amount - $excess_amount;


					while ($inv_amount != 0) {
						
						//Split amount is the amount to be invested in each fund and splitting the investment amount with base amount.
								/* if inv_amount = 26000
										
										then after the loop $split_amount = [5000,5000,5000,5000,5000]
										
								*/
						$split_amount[] = $base_amount;
						$inv_amount -= $base_amount ;
					}


					//$scheme_codes = SchemeDetails::all();

					/*Getting all the scheme codes user has invested in from his portfolio details and converting it to an array*/

					$schemes_invested = $user->portfolioDetails()->groupBy('scheme_code')->pluck('amount_invested','scheme_code');
					$schemes_invested = $schemes_invested->toArray();


					foreach($split_amount as $amount) {


						//Only when the user is investing for the first time IF condition is executed 


						if(empty($scheme_suggestions) && empty($schemes_invested)) { //To check whether any investment is made already and no suggestions made already
							$debt_funds = SchemeDetails::where('scheme_type','debt')
						  	->orderBy('scheme_priority','asc')
						  	->first();

						  	//print_r($debt_funds);

						  	$scheme_suggestions[$debt_funds->scheme_code] = $amount;

						  	$schemes_invested[$debt_funds->scheme_code] = $amount;
						  	$invested_scheme_codes[] = $debt_funds->scheme_code;
						  	//print_r($schemes_invested);
						  	//print_r($scheme_suggestions);


						} 

						else {
							$next_scheme = SchemeDetails::whereNotIn('scheme_code',$invested_scheme_codes)
							->where('scheme_type','debt')
							->orderBy('scheme_priority','ASC')
							->first();

							//echo $next_scheme;


							if($next_scheme != NULL){

							  	$scheme_suggestions[$next_scheme['scheme_code']] = $amount;
							  	$schemes_invested[$next_scheme['scheme_code']] = $amount;
							  	$invested_scheme_codes[] = $next_scheme['scheme_code'];
							  	//print_r($scheme_suggestions);
							}

							else{
								
								$minimum_invested_amount = min($schemes_invested);
								$maximum_invested_amount = max($schemes_invested);


								if( $minimum_invested_amount == $maximum_invested_amount)
								{
									$debt_funds = SchemeDetails::where('scheme_type','debt')
								  	->orderBy('scheme_priority','asc')
								  	->first();

								  	if(array_key_exists([$debt_funds['scheme_code']], $scheme_suggestions)) {
								  		$scheme_suggestions[$debt_funds['scheme_code']] += $amount;
								  	} 

								  	else {
								  		$scheme_suggestions[$debt_funds['scheme_code']] = $amount;
								  	}
								  	
								  	$schemes_invested[$debt_funds['scheme_code']] += $amount;
								} 

								else{

									$no_of_minimum_invested_schemes_codes = array_keys($schemes_invested,$minimum_invested_amount);
									//print_r($no_of_minimum_invested_schemes_codes);
									if(count($no_of_minimum_invested_schemes_codes)>1) {

										$next_scheme = SchemeDetails::whereIn('scheme_code',$no_of_minimum_invested_schemes_codes)
										->where('scheme_type','debt')
										->orderBy('scheme_priority','ASC')
										->first();


								  		if(array_key_exists($next_scheme->scheme_code, $scheme_suggestions)) {
										  	$scheme_suggestions[$next_scheme->scheme_code] += $amount;
										} else {
											$scheme_suggestions[$next_scheme->scheme_code] = $amount;
										}
								  		$schemes_invested[$next_scheme->scheme_code] += $amount;

								  		//print_r($scheme_suggestions);

									} 
									else {
										
								  		if(array_key_exists(current($no_of_minimum_invested_schemes_codes), $scheme_suggestions)) {
										  	$scheme_suggestions[current($no_of_minimum_invested_schemes_codes)] += $amount;
										} else {
											$scheme_suggestions[current($no_of_minimum_invested_schemes_codes)] = $amount;
										}
								  		$schemes_invested[current($no_of_minimum_invested_schemes_codes)] += $amount;
									}
								}
								
							}


						} // else ends 
					}// Foreach ends 

					
					end($schemes_invested);
					$least_p_scheme = key($schemes_invested);

					$scheme_suggestions[$least_p_scheme] += $excess_amount;
					$schemes_invested[$least_p_scheme] += $excess_amount;


						
						//print_r($scheme_suggestions);

						$passing_suggestion = array();
						$scheme_name_code = SchemeDetails::where('scheme_type','debt')->pluck('scheme_name','scheme_code');
						$scheme_name_code = $scheme_name_code->toArray();

						//print_r($scheme_name_code);

						foreach ($scheme_suggestions as $schemes => $amount) {
							
							$passing_suggestion[] = array(
								'scheme_code' => $schemes,
								'scheme_name' => $scheme_name_code[$schemes],
								'amount' => $amount
							);
							
						}

						//print_r($passing_suggestion);

						return response()->json(['msg'=>$passing_suggestion]);



				if($port_type == 'moderate-risk'){

				}

				if($port_type == 'high-risk'){

				}


			}	

			}
	}

	//function to create details required to show for user in the homepage
	public function getInvestment_details()
	{
		$portfolio_investment_details = array();	//Contains portfolio investment details used for user homepage
		$debt_xirr = 0;	//Debt funds XIRR return for user
		$equity_xirr = 0;	//Equity funds XIRR return for user
		$balanced_xirr = 0;	//Balanced Funds XIRR return for user
		$ts_xirr = 0;	//Taxer Saver Funds XIRR return for user
		$total_investment_xirr = 0;	//Total Investmetn XIRR return for user
		$debt_inv_withdraw_amount = array();	//Contains all investment and withdrawl amount in debt fund of user
    	$debt_inv_withdraw_date = array();	//Contains all investment and withdrawl date in debt fund of user
    	$equity_inv_withdraw_amount = array();	//Contains all investment and withdrawl amount in equity fund of user
    	$equity_inv_withdraw_date = array();	//Contains all investment and withdrawl date in equity fund of user
    	$balanced_inv_withdraw_amount = array();	//Contains all investment and withdrawl amount in balanced fund of user
    	$balanced_inv_withdraw_date = array();	//Contains all investment and withdrawl date in balanced fund of user
    	$ts_inv_withdraw_amount = array();	//Contains all investment and withdrawl amount in tax saver fund of user
    	$ts_inv_withdraw_date = array();	//Contains all investment and withdrawl date in tax saver fund of user
    	$active_investment_schemes = array();	//contains all the debt investment scheme codes eligible for xirr calculation
    	/*$equity_active_investment_schemes = array();	//contains all the equity investment scheme codes eligible for xirr calculation
    	$balanced_active_investment_schemes = array();	//contains all the balanced investment scheme codes eligible for xirr calculation*/
        $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
        $scheme_code_name = $scheme_details->toArray();	//Conatains Schemecode as key and Schemename as value
        /*$nav_details = NavDetails::get()->pluck('nav_value','scheme_code');
        $nav_code_value = $nav_details->toArray();*/ //Conatains Schemecode as key and navvalue as value
        $user= \Auth::user();	//Contains current authenticated user
        $portfolio_details = $user->portfolioDetails()->orderBy('investment_date','desc')->get(); //Contains all portofolio records of the user
        $portfolio_types = array('debt', 'bal', 'eq', 'ts');	//Conatains the type of schemes avaialable
        $total_investment_amount = 0;	//Contains total amount invested - total withdrawl
        $total_investment_current_value = 0;	//Contains current value of amount invested - total withdrawl
        $withdraw_details = $user->WithdrawDetails;	//All withdrawl of the user
        $xirr_available_funds_count = 0;

        //calculating and creating data needed for user homepage
        foreach ($portfolio_types as $portfolio_type) {

        	//Retreiving portfolio details of user which are active for specified portfolio type
            $portfolio_type_details = $portfolio_details
                ->where('portfolio_type', $portfolio_type)
                ->where('portfolio_status', 1)
                ->groupBy('scheme_code')
                ->all();
            $total_portfolio_investment = 0;	//Total amount invested in $portfolio_type 
            $total_portfolio_current_value = 0;	//Total current value of the  $portfolio_type

    		//retreiveing data from each porfolio
            foreach($portfolio_type_details as $portfolio_detail) {
            	$portfolio = $portfolio_detail->first();
            	$nav_code_value = NavDetails::where('scheme_code',$portfolio->scheme_code)->orderBy('date', 'desc')->first();
                $units_held = $portfolio_detail->sum('units_held');	//number of units held in the portfolio
                $total_amount_invested = $portfolio_detail->sum('amount_invested');	//amount invested in the portfolio
                //$nav_value = ;	//current nav value of the $potfolio_type
                $current_value = $units_held * $nav_code_value->nav_value;	//current value of the portfolio
                $total_portfolio_investment += $total_amount_invested;	
                $total_portfolio_current_value += $current_value;
            }//echo $total_portfolio_current_value.'<br>';

            $portfolio_withdrawn_amount = $withdraw_details
            	->where('portfolio_type', $portfolio_type)
            	->where('withdraw_status', 1)
            	->sum('withdraw_amount');

            //$total_portfolio_current_value = $total_portfolio_current_value;

            //calculating $portfolio_type specific details Total amount invested, total current value, net profit and profit percent
            if($total_portfolio_investment > 0 && $total_portfolio_current_value > 0){
            	//$total_portfolio_investment -= $portfolio_withdrawn_amount;
               $portfolio_investment_details[$portfolio_type]['total_amount_invested'] = $total_portfolio_investment;
               $portfolio_investment_details[$portfolio_type]['total_current_value'] = round($total_portfolio_current_value,2);
               $portfolio_profit = round($total_portfolio_current_value,2) - $total_portfolio_investment;
               //echo round($total_portfolio_current_value,2).$total_portfolio_investment.'<br>';
               $portfolio_investment_details[$portfolio_type]['total_net_profit'] =round($portfolio_profit, 2);
               //$portfolio_investment_details[$portfolio_type]['total_profit_percent'] = round(((($total_portfolio_current_value / $total_portfolio_investment) * 100) - 100),2);
            } else {

            	$portfolio_investment_details[$portfolio_type]['total_amount_invested'] = 0;
               $portfolio_investment_details[$portfolio_type]['total_current_value'] = 0;
               $portfolio_profit = 0;
               //echo round($total_portfolio_current_value,2).$total_portfolio_investment.'<br>';
               $portfolio_investment_details[$portfolio_type]['total_net_profit'] = 0;

            }
            $total_investment_amount += $total_portfolio_investment;
            $total_investment_current_value += $total_portfolio_current_value;
        }

         $total_investment_amount = round($total_investment_amount, 2);
         $total_investment_current_value = round($total_investment_current_value, 2);

        //calculating Total amount invested, total current value, net profit and profit percent for all investment
        if($total_investment_amount > 0 && $total_investment_current_value > 0){
            $portfolio_investment_details['total_amount_invested'] = $total_investment_amount;
            $portfolio_investment_details['total_current_value'] = $total_investment_current_value;
            $portfolio_investment_details['total_net_profit'] = round(($total_investment_current_value - $total_investment_amount),2);
        } else {
        	$portfolio_investment_details['total_amount_invested'] = 0;
            $portfolio_investment_details['total_current_value'] = 0;
            $portfolio_investment_details['total_net_profit'] = 0;
        }

        //interating through the type of investements
        foreach($portfolio_types as $portfolio_type) {

        	//calculating investment percent, profit percent and total investment percent for each portfolio type for donut chart
        	if(array_key_exists($portfolio_type, $portfolio_investment_details)) {

        		if($portfolio_investment_details[$portfolio_type]['total_current_value'] > 0) {
	        		//calculating profit/loss
	        		$profit_percent = round((($portfolio_investment_details[$portfolio_type]['total_net_profit'] / $portfolio_investment_details[$portfolio_type]['total_current_value'])*100),1);

	        		if($profit_percent < 0) {
	        			//calculating investment percent
	        			$portfolio_investment_details[$portfolio_type]['investment_percent'] = round((($portfolio_investment_details[$portfolio_type]['total_current_value'] / $portfolio_investment_details['total_current_value'])*100),1);

	        			$portfolio_investment_details[$portfolio_type]['profit_percent'] = 0;
	        		} else {

	        			//calculating investment percent
	        			$portfolio_investment_details[$portfolio_type]['investment_percent'] = round((($portfolio_investment_details[$portfolio_type]['total_current_value'] / $portfolio_investment_details['total_current_value'])*100),1) - $profit_percent;

	        			//calculating investment profit percent
	        			$portfolio_investment_details[$portfolio_type]['profit_percent'] = $profit_percent;
	        		}

	        		$portfolio_investment_details[$portfolio_type]['total_investment_percent'] = round((100 - ($portfolio_investment_details[$portfolio_type]['investment_percent'] + $portfolio_investment_details[$portfolio_type]['profit_percent'])),1);
	        	} else {

	        		$profit_percent = 0;

	        		//calculating investment percent
	        		$portfolio_investment_details[$portfolio_type]['investment_percent'] = 0;
        			//calculating investment profit percent
        			$portfolio_investment_details[$portfolio_type]['profit_percent'] = 0;
	        		$portfolio_investment_details[$portfolio_type]['total_investment_percent'] = 0;

	        	}
        	}

        }


        //retreiving all portfolio records of user which are active or closed ordered based on investment date.
    	$portfolio_inv_details = $user->portfolioDetails()
            ->whereIn('portfolio_status', [1,2])
            ->orderBy('investment_date','asc')
            ->get();

        //grouping portfolios based on the portfolio type
		$portfolio_inv_details = $portfolio_inv_details->groupBy('portfolio_type');

        //Building investment data for calculalting XIRR
        foreach($portfolio_inv_details as $type => $portfolio_inv_detail) {

        	$active_old_investment_count = $user->portfolioDetails()->where('investment_date','<',date('Y-m-d'))
        		->where('portfolio_status', 1)->where('portfolio_type', $type)
        		->count();

        	//checking there exist an active investment in the scheme made before current day
        	if($active_old_investment_count > 0) {
        		foreach($portfolio_inv_detail as $portfolio_inv) {
		        	if($portfolio_inv->portfolio_type == 'debt') {
		        		$debt_inv_withdraw_amount[] = -1 * $portfolio_inv->initial_amount_invested;
		        		$debt_inv_withdraw_date[] = $portfolio_inv->investment_date;
		        	} elseif($portfolio_inv->portfolio_type == 'eq') {
		        		$equity_inv_withdraw_amount[] = -1 * $portfolio_inv->initial_amount_invested;
		        		$equity_inv_withdraw_date[] = $portfolio_inv->investment_date;
		        	} elseif($portfolio_inv->portfolio_type == 'bal') {
		        		$balanced_inv_withdraw_amount[] = -1 * $portfolio_inv->initial_amount_invested;
		        		$balanced_inv_withdraw_date[] = $portfolio_inv->investment_date;
		        	} elseif($portfolio_inv->portfolio_type == 'ts') {
		        		$ts_inv_withdraw_amount[] = -1 * $portfolio_inv->initial_amount_invested;
		        		$ts_inv_withdraw_date[] = $portfolio_inv->investment_date;
		        	}

		        	//keeping track of active investments
		        	if(!in_array($portfolio_inv->scheme_code, $active_investment_schemes)) {
	        			$active_investment_schemes[] = $portfolio_inv->scheme_code;
	        		}
		        }
	        }
        }

        //Building withdraw data for calculalting XIRR
        $active_withdraw_details = $withdraw_details->whereIn('scheme_code', $active_investment_schemes)
        	->where('withdraw_status',1);

        //check whether any withdraw exist
        if($active_withdraw_details->count() > 0) {

        	foreach( $active_withdraw_details as $withdraw_detail) {//processing each withdraw

	        	if($withdraw_detail->portfolio_type == 'debt') {//processing debt withdraws
	        		$debt_inv_withdraw_amount[] = $withdraw_detail->withdraw_amount;
	        		$debt_inv_withdraw_date[] = $withdraw_detail->withdraw_date;
	        	} elseif($withdraw_detail->portfolio_type == 'eq') {//processing equity withdraws
	        		$equity_inv_withdraw_amount[] = $withdraw_detail->withdraw_amount;
	        		$equity_inv_withdraw_date[] = $withdraw_detail->withdraw_date;
	        	} elseif($withdraw_detail->portfolio_type == 'bal') {//processing balanced withdraws
	        		$balanced_inv_withdraw_amount[] = $withdraw_detail->withdraw_amount;
	        		$balanced_inv_withdraw_date[] = $withdraw_detail->withdraw_date;
	        	} elseif($withdraw_detail->portfolio_type == 'ts') {//processing balanced withdraws
	        		$ts_inv_withdraw_amount[] = $withdraw_detail->withdraw_amount;
	        		$ts_inv_withdraw_date[] = $withdraw_detail->withdraw_date;
	        	}

	        }

	    }

	    setlocale(LC_MONETARY, 'en_IN');
        //adding current value to  investment for calculating XIRR
        if(!empty($debt_inv_withdraw_amount)) {
        	$debt_inv_withdraw_amount[] = $portfolio_investment_details['debt']['total_current_value'];;
       		$debt_inv_withdraw_date[] = date('Y-m-d');
        }
        if(!empty($equity_inv_withdraw_amount)) {
        	$equity_inv_withdraw_amount[] = $portfolio_investment_details['eq']['total_current_value'];
       		$equity_inv_withdraw_date[] = date('Y-m-d');
        }
        if(!empty($balanced_inv_withdraw_amount)) {
        	$balanced_inv_withdraw_amount[] = $portfolio_investment_details['bal']['total_current_value'];
    		$balanced_inv_withdraw_date[] = date('Y-m-d');
        }

        if(!empty($ts_inv_withdraw_amount)) {
        	$ts_inv_withdraw_amount[] = $portfolio_investment_details['ts']['total_current_value'];
    		$ts_inv_withdraw_date[] = date('Y-m-d');
        }

        //Calculating XIRR for debt funds
        //dd($debt_inv_withdraw_amount,$debt_inv_withdraw_date);
        if(!empty($debt_inv_withdraw_amount)) {

        	//$debt_inv_withdraw_amount = array_reverse($debt_inv_withdraw_amount);
        	//$debt_inv_withdraw_date = array_reverse($debt_inv_withdraw_date);
        	// $amount = [-1000.0,-1000.0,-1000.0,-1000.0,-1000.0,-10000.0,-10000.0,-10000.0,-10000.0,-10000.0,-10000.0,-10000.0,-10000.0,-10000.0,-10000.0,10000,10000,10000,4000];
        	// $date = ['2017-08-24','2017-08-24','2017-08-24','2017-08-24','2017-08-24','2017-08-29','2017-08-29','2017-08-29','2017-08-29','2017-08-29','2017-08-31','2017-08-31','2017-08-31','2017-08-31','2017-08-31','2017-09-04','2017-09-04','2017-09-04','2017-09-13'];
        	// $amount = array_reverse($amount);
        	// $date = array_reverse($date);
        	//dump($debt_inv_withdraw_amount,$debt_inv_withdraw_date);
        	//print_r($debt_inv_withdraw_amount);print_r($debt_inv_withdraw_date);
        	//$debt_xirr = PHPExcel_Calculation_Financial::XIRR($amount,$date);

        	//dd($debt_inv_withdraw_amount,$debt_inv_withdraw_date);

        	$debt_xirr = PHPExcel_Calculation_Financial::XIRR($debt_inv_withdraw_amount,$debt_inv_withdraw_date);
        	//dd("Debt XIRR-".$debt_xirr);
        	//echo $debt_xirr;
        	//print_r($debt_xirr);
        	//die();
    		$debt_xirr = number_format($debt_xirr, 10);
    		$debt_xirr = round(($debt_xirr * 100),2);
    		$xirr_available_funds_count++;
    		//dd($debt_xirr);
        	
        }

        if(array_key_exists('debt', $portfolio_investment_details)) {
        	$portfolio_investment_details['debt']['total_profit_percent'] = $debt_xirr;
        }

        //Calculating XIRR for equity funds

        //dd($portfolio_investment_details);
        if(!empty($equity_inv_withdraw_amount)) {
        	$equity_xirr = PHPExcel_Calculation_Financial::XIRR($equity_inv_withdraw_amount,$equity_inv_withdraw_date);
			$equity_xirr = number_format($equity_xirr, 10);
			$equity_xirr = round(($equity_xirr * 100),2);
			$xirr_available_funds_count++;
        	
        }

       	if(array_key_exists('eq', $portfolio_investment_details)) {
        	$portfolio_investment_details['eq']['total_profit_percent'] = $equity_xirr;
        }

        //Calculating XIRR for balanced funds
        if(!empty($balanced_inv_withdraw_date)) {
        	$balanced_xirr = PHPExcel_Calculation_Financial::XIRR($balanced_inv_withdraw_amount,$balanced_inv_withdraw_date);
    		$balanced_xirr = round(($balanced_xirr * 100),2);
    		$xirr_available_funds_count++;
        	
        }

        if(array_key_exists('bal', $portfolio_investment_details)) {
        	$portfolio_investment_details['bal']['total_profit_percent'] = $balanced_xirr;
        }

        //Calculating XIRR for balanced funds
        if(!empty($ts_inv_withdraw_date)) {

        	$ts_xirr = PHPExcel_Calculation_Financial::XIRR($ts_inv_withdraw_amount,$ts_inv_withdraw_date);
    		$ts_xirr = number_format($ts_xirr, 10);
    		$ts_xirr = round(($ts_xirr * 100),2);
    		$xirr_available_funds_count++;
        }
        
		if(array_key_exists('ts', $portfolio_investment_details)) {
        	$portfolio_investment_details['ts']['total_profit_percent'] = $ts_xirr;
        }

        //getting overall performance of the user portfolio
		$investment_performance = (new adminController)->getUserInvestmentPerformance($user->id);
		$portfolio_investment_details['total_profit_percent'] = $investment_performance['xirr'];
		// dd($user->personalDetails);
		// if ($user->personal_details == NULL) {
		// 	// dd('hello');
  //       	$goal_amount = 0;
  //       	$portfolio_investment_details['goal_amount'] =  $goal_amount;
  //       	$portfolio_investment_details['goal_percent'] = 0;
		// }else
		if ($user->personalDetails == NULL) {
        	$goal_amount = 0;
        	$portfolio_investment_details['goal_amount'] =  $goal_amount;
        	$portfolio_investment_details['goal_percent'] = 0;
		}else{
			if($user->personalDetails->count() > 0){
	        	$goal_amount = $user->personalDetails->goal_amount;

		        $portfolio_investment_details['goal_amount'] =  $goal_amount;

		        //calculating whether the goal is acheived or not
		        if($goal_amount > 0) {
		        	if($goal_amount > $total_investment_current_value) {
			        	$portfolio_investment_details['goal_percent'] = round(($total_investment_current_value / $goal_amount) * 100);
			        } else {
			        	$portfolio_investment_details['goal_percent'] = 100;
			        }
			    } else {
			    	$portfolio_investment_details['goal_percent'] = 0;
			    }
	        }else{
	        	$goal_amount = 0;
	        	$portfolio_investment_details['goal_amount'] =  $goal_amount;
	        	$portfolio_investment_details['goal_percent'] = 0;
	        }
		}

        

        //creating slab value for goal chart
        $portfolio_investment_details['goal_slab'] = $goal_amount / 6;
        // dd($portfolio_investment_details);
        return $portfolio_investment_details;
	}

	//function to send email
	public function SendEmail()
	{
		//Log::info('password'.env('MAIL_PASSWORD','stugotech').'ends');
		$user = \Auth::user();
		$email = $user->email;
		$name = $user->name;
		Mail::send('users.email', array('user_name' => $user->name), function ($message) use ($user) {
		    $message->from('test@rightfundspartners.com', 'Rightfunds');
		    $message->subject('Rightfunds OTP');
		    $message->to($user->email, $user->name);
		    //$message->attach('profile_pic.jpg');
		});

		return view('users.email');
	}

	//function to save goal
	public function saveGoal(Request $request)
	{
		$user = \Auth::user();	//Get current authenticated user
		$personal_details = $user->PersonalDetails;	//get current user personal details


		if (count($personal_details) == 0) {
			return response()->json(['msg'=>'nope']);
		}else{
			//check if valid goal amount
			if($request['goal_amount']>0) {
				$personal_details->goal_amount = $request['goal_amount'];

				//saving new goal
				if($user->personalDetails()->save($personal_details)) {
					return response()->json(['msg'=>'true','goal_amount'=>number_format($request['goal_amount'],0,'.',',')]);
				} else {
					return response()->json(['msg'=>'false']);
				}
			} else {
				return response()->json(['msg'=>'false']);
			}
		}

		
	}

	//function to resent opt for user
	public function sendOtp(Request $request)
	{
		//rules for validating register data
		$validator = \Validator::make($request->all(), [
			'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            //'pan' => ['required','min:10','max:10','unique:users','regex:/^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/'],
            'mobile' => 'required|min:10|max:10|unique:users',
            'acc_type' => 'required',
        ]);

        //check error in data recieved
		if($validator->fails()) {
			return response()->json(['msg' => 'error', 'response' => $validator->errors()]);
		} else {
			
			$ch = curl_init();
	        $user="naveen@rightfunds.com:Rightfunds20";
	        $receipientno = $request['mobile'];
	        $senderID="RFUNDS";
	        $otp_value = rand(1000,9999);
        	$msgtxt="Your OTP is ".$otp_value; 
	        curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
	        $buffer = curl_exec($ch);
	        curl_close($ch);
	        if(!empty ($buffer)) { 
	        	Log::info('Succeeded:'.$buffer);
	        	$mail_status = Mail::send('users.email', array('user_name' => $request['name'], 'otp' =>$otp_value), function ($message) use ($request) {
	                $message->from('test@rightfundspartners.com', 'Rightfunds');
	                $message->subject('Rightfunds OTP');
	                $message->to($request['email'], $request['name']);
	                //$message->attach('profile_pic.jpg');
	            });
	            if($mail_status) {
	            	$request->session()->put('otp', bcrypt($otp_value));
	        		$request->session()->put('otp_start_time', date('Y-m-d H:i:s'));
	            	return response()->json(['msg'=>'success', 'response'=>'OTP sent Successfully']);
	            } else {
	            	return response()->json(['msg'=>'error', 'email'=>'Unable to verify email. Please check email and try again.']);
	            }
	        } else {
	        	Log::info('failed');
	        	return response()->json(['msg'=>'error','mobile'=>'Unable to verify mobile. Please check Mobile number and try again.']);
	        }
		}
	}

	//function to verify the user mobile
	public function verifyOtp(Request $request)
	{
		$validator = \Validator::make($request->all(), [
			'otp' => 'required'
		]);
		if($validator->fails()) {
			return response()->json(['msg'=>'failure','response'=>'Please input the OTP received']);
		} else {
			if($request->session()->has('otp') && $request->session()->has('otp_start_time')) {
				$session_otp = $request->session()->get('otp');
				$session_otp_time = $request->session()->get('otp_start_time');Log::info($session_otp_time);
				$otp = $request['otp'];Log::info($otp);
				$otp_start_time = Carbon::createFromFormat('Y-m-d H:i:s', $session_otp_time);
				$current_date_time = Carbon::now();
				if(Hash::check($otp, $session_otp)) {
					Log::info('Diff in mins: '.$current_date_time->diffInMinutes($otp_start_time));
					if($current_date_time->diffInMinutes($otp_start_time) < 5){
						return response()->json(['msg'=>'success','response'=>'Mobile verified Successfully']);
					} else {
						return response()->json(['msg'=>'failure','response'=>'OTP expired. Resend OTP and try again']);	
					}
				} else {
					return response()->json(['msg'=>'failure','response'=>'Invalid OTP. Please check and try again']);
				}
			} else {
				return response()->json(['msg'=>'failure','response'=>'Something went wrong. Please refresh the page and try again.']);
			}
		}
		
	}

	//function to send mail for contact us
	public function sendContactUsEmail(Request $request)
	{
		$msg = 'failure';
		$status = '';
		$validator = \Validator::make($request->all(), [
			'name' => 'required|max:50',
            'email' => 'required|email|max:255',
            'mobile' => 'required|min:10|max:10',
            'message' => 'required',
		]);

		if($validator->fails()) {
			$msg = 'failure';
			$status = 'Something went wrong. Please refresh and try again.';
		} else {
			$contact_us_details = array(
				'name' => $request['name'],
				'email' => $request['email'],
				'mobile' => $request['mobile'],
				'message' => $request['message'],
			);
			$mail_status = Mail::send('email.contact_us', array('contact_us_details' => $contact_us_details), function ($message) use ($contact_us_details) {
			    $message->from('test@rightfundspartners.com', 'Rightfunds Contact');
			    $message->subject('Contact Query');
			    $message->to('contact@rightfunds.com', 'Contact us');
			});
			if($mail_status) {
				$msg = 'success';
				$status = 'Thanks for reaching us. We will get back to you shortly.';
			} else {
				$msg = 'failure';
				$status = 'Something went wrong. Please refresh and try again.';
			}
		}
		return response()->json(['msg' => $msg, 'status' => $status]);
	}

	//function to get date for one time investment
	public function getOnetimeInvestmentDate()
	{
		$date = Carbon::now()->addDays(0);
		return response()->json(['response' => date('d-m-Y', strtotime($date))]);
	}


	public function ekyc(){
		return view('users.ckyc-index');
		//return view('users.version2.ekyc');
	}

	public function ekyc1(){

		// if(\Auth::user()->NachDetails->kycvideo_status == 1){
		// 	return redirect()->back();
		// }else{
			return view('users.version2.ekyc');	
		//}
	}


	public function regCheck(Request $request){


		//return "hello";
			$validator = \Validator::make($request->all(),[

				'user_id' => 'required'

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());

			}
			else{

				$user_id = $request['user_id'];

				if (\Auth::user()->id == $user_id) {
					
					$update = User::where('id',\Auth::user()->id)->update(['r_check' => '1']);

					if ($update) {
						return response()->json(['msg'=>'1']);
					}

					else{
						return response()->json(['msg' => '0']);
					}
				}
			}

	}


	public function getMandateStatus(){

		//var_dump(\Auth::user()->nachDetails);
		if(\Auth::user()->nachDetails->bsemandate_status == 1){
			return response()->json(['msg'=>'approved']);
		}else{
			return response()->json(['msg'=>'not_approved']);
		}
 
	}

	public function getifsc(){
		// create curl resource 
/*        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, "http://api.techm.co.in/api/v1/ifsc/CNRB0008628"); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

        // $output contains the output string 
        $output = json_decode(curl_exec($ch),true); 
        
        var_dump($output);
        echo gettype($output);

        echo $output['data']['MICRCODE'];

        // close curl resource to free up system resources 
        curl_close($ch);*/



        		// create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, "http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=93&random=6102017153"); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

        // $output contains the output string 
        $output = json_decode(curl_exec($ch),true); 
        
        var_dump($output);
        echo gettype($output);

        echo $output;

        // close curl resource to free up system resources 
        curl_close($ch);



	}


	public function showFutureOrders(){

		$bse_order_response = array();

		$future_investment = InvestmentDetails::where('investment_type','1')->where('investment_date',date('Y-m-d'))->where('investment_status',0)->get();

		foreach ($future_investment as $investment) {
			$portfolio_details = $investment->portfolioDetails;

			$bse_date = date('Y-m-d');
		    $bse_date = explode('-', $bse_date);
		    $bse_date = $bse_date[0].$bse_date[1].$bse_date[2];
		    $time = explode('.', microtime());
		    $time = substr($time[1], 0 ,6);

		    $unique_ref_no = $bse_date."12456".$time;
		    $int_ref_no = $unique_ref_no;


			foreach ($portfolio_details as $portfolio) {
				$user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$portfolio['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

			    if (count($user_folio) == 0) {
			    	$folio_number = '';	
			    }else{
			    	$folio_number = $user_folio['folio_number'];

			    }

			    $this->getPassword();
													    

			    $transact_mode = 'NEW';
			    $buy_sell = 'P';

			    if ($folio_number == '') {
			    	$buy_sell_type = 'FRESH'; //FRESH OR ADDITIONAL WILL COME HERE;	
			    }else{
			    	$buy_sell_type = 'ADDITIONAL'; //FRESH OR ADDITIONAL WILL COME HERE;
			    }


				$bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$portfolio['scheme_code']],$buy_sell,$buy_sell_type,$portfolio['amount'],$folio_number,$int_ref_no,$investment_id);

			}//portfolio details foreach ends 
		}// future investment array ends


		foreach ($bse_order_response as $key => $value) {
			$date = date('d/m/Y h:i:s A', strtotime($date));
			PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $date]);
		} //foreach ends
	}

	public function kycStatus($data)
	{

	 $vars = array(
              'sAppName' => '',
              'sDOB' => '',
              'sPAN' => $data,
              'sExemptCategory' => '',
          );

      $vars = json_encode($vars);


      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"http://camskra.com/MyKYCNew.aspx/showKYCInfo");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $headers = [
          'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
          'Accept-Encoding: gzip, deflate',
          'Accept-Language: en-US,en;q=0.5',
          'Cache-Control: no-cache',
          'Content-Type: application/json;',
          'Host: www.camskra.com',
          'Referer: http://www.camskra.com', //Your referrer address
          'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0',
          'X-MicrosoftAjax: Delta=true'
      ];

      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $server_output = curl_exec ($ch);

      //dd($server_output);

      curl_close ($ch);
      $server_output = json_decode($server_output);
      // echo gettype($server_output);CQLPD4121Q
      // var_dump($server_output);
      // die();
      // dd($server_output->d->Status);
      if (strpos($server_output->d->Status,'Not') !== false) {
      	return 0;
      	// dd('kyc not-verified');
      }else{
      	return 1;
      	// dd('kyc verified');
      }
		
	}


	public function fatcaUpdate()
	{
		$TodayDate = Carbon::now()->format('Y-m-d');

		$dataUp = NachDetails::where('nach_status',1)->update(['fatca_status' => 1, 'fatca_date' => $TodayDate]);
		dd($dataUp);
	}

	public function handlePayment(){

		// This function is to clear the session variable.
		session_start();
		unset($_SESSION);
		session_destroy();
		
		// print_r($_SESSION);
		return response()->json(['msg'=>'success']);
	}

	public function getMandateLink(Request $request)
	{
		$url = NachDetails::where('user_id',\Auth::user()->id)->value('mandate_link');
		if ($url == NULL || $url == '') {
			return response()->json(['msg' => 2]);
		}else{
			return response()->json(['msg'=>1,'mandate_url'=>$url]);
		}
	}

}
