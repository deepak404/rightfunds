<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
                
            $user = \Auth::user();

            /* -------
                Checking whether the Authenticated user has Submitted the personal Details,
                if Submitted redirect the user to home page, else redirect them to sign-up page
               ------*/

            //if($user->otp_check == 1) {
                if ($user->p_check == "0") {
                    return redirect('/sign-up');
                } elseif ($user->p_check == "1") {
                    if($user->role == 1) {
                        return redirect('/admin/overview');
                    } else{
                        return redirect('/home');
                    }

                }
            /*} else {
                return redirect('/otp');
            }*/
        }

        return $next($request);
    }
}
