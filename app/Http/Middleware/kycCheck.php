<?php

namespace App\Http\Middleware;

use Closure;

class kycCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if ($user->nachDetails->cleared_date != "") {
                return $next($request);
            }

            else{

                return $next($request);
            }
        return $next($request);
    }   
}
