    $('.from-to').datepicker({ dateFormat: "dd-mm-yy",changeMonth: true, changeYear: true, yearRange: '2016:2050' });

    $('.bse_order_input').datepicker();


    $(document).ready(function(){
        // $('.bse_date_selector').on('change',function(e){
        //   if ($(this).prop('checked') == true) {
        //     var p_id,sip_id;
        //     p_id = $(this).data('pid');
        //     sip_id = $(this).parent().next().find('.bse_reg_date_selector').attr('data-sipid');

        //     check_sip = checkSipReg(sip_id);

        //     if (check_sip == 'r') {
        //       //alert('SIP is not registered in the BSE');
        //       updateBseDate(p_id,'checked')
        //     }else{
        //       alert('SIP is not registered in the BSE');
        //       $(this).prop('checked',false);
        //     }

        //     console.log(check_sip);
        //     //
        //   }
        //  if ($(this).prop('checked') == false) {
        //     var p_id,sip_id;
        //     sip_id = $(this).parent().next().find('.bse_reg_date_selector').attr('data-sipid');
        //     check_sip = checkSipReg(sip_id);


        //     console.log(check_sip);
        //     var p_id = $(this).data('pid');
        //     //console.log(p_type);
        //     updateBseDate(p_id,'not_checked')
        //   }
        // })






        $('.portfolio_scheme').on('change',function(e){
          var p_id,sip_id,scheme_code;
          sip_id = $(this).parent().next().find('.bse_reg_date_selector').attr('data-sipid');
          scheme_code = $(this).parent().next().find('.bse_reg_date_selector').attr('data-schemecode');
          //p_id = $(this).data('pid');

          if ($(this).prop('checked') == true) {
            check_sip = checkSipReg(sip_id,scheme_code);

            if (check_sip == 'r') {
            }else{
              alert('SIP is not registered in the BSE');
              $(this).prop('checked',false);
            }
            console.log(check_sip);
          }
         if ($(this).prop('checked') == false) {
            check_sip = checkSipReg(sip_id,scheme_code);
            console.log(check_sip);
          }
        })






        $('.bse_reg_date_selector').on('change',function(){
          if ($(this).prop('checked') == true) {
            var sip_id;
            sip_id = $(this).attr('data-sipid');
            var scheme_code = $(this).attr('data-schemecode');
            $('#sip_scheme_code').val(scheme_code);
            $('#sip_id').val(sip_id);
            $('#sip_status').val('checked');
            $('#sipRegModal').modal('show');

            //updateBseRegDate(sip_id,'checked')
          }
         if ($(this).prop('checked') == false) {
            var sip_id;
            sip_id = $(this).attr('data-sipid');
            var scheme_code = $(this).attr('data-schemecode');
            $('#sip_scheme_code').val(scheme_code);
            $('#sip_id').val(sip_id);
            //$('#sipreg_no').val('');
            $('#sip_status').val('not_checked');
            $('#sipRegNoForm').submit();
            //updateBseRegDate(sip_id,'not_checked')
          }
        });


        $('#sipRegNoForm').on('submit',function(e){
            e.preventDefault();
            var form_data = $(this).serialize();
            console.log(form_data);
            $.ajax({
                type: "POST",
                url: "/admin/updateSipRegNo",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: form_data,
                beforeSend: function() {},
                success: function(data) {
                    if (data.msg=="success") {

                        if ($('#sip_status').val() == "checked") {
                          $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').prop('checked',true);
                          $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').parent().next().find('span').text(data.date);
                        }else{
                          $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').prop('checked',false);
                          $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').parent().next().find('span').text('');
                        }
                        
                    }else{
                        
                    }

                },
                error: function(xhr, status, error) {},
            })


        });

        // function updateBseRegDate(sip_id,status,e){

        //     var dataString = 'sip_id=' + sip_id + '&status=' + status;
        //     console.log(dataString);
        //     $.ajax({
        //         type: "POST",
        //         url: "/admin/updateSipRegDate",
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         cache: !1,
        //         processData: !1,
        //         data: dataString,
        //         beforeSend: function() {},
        //         success: function(data) {
        //             console.log(data.msg);

        //             if (data.msg == 'date_success') {


        //                 console.log(data.sip_id);
        //                 $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').prop('checked',true);
        //                 $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').parent().next().find('span').text(data.date);

        //             }

        //             if (data.msg == 'nodate_success') {
        //                 console.log(data.sip_id);
        //                 $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').prop('checked',false);
        //                 console.log(data.sip_id);
        //                 $('.bse_reg_date_selector[data-sipid="'+data.sip_id+'"]').parent().next().find('span').text('');
        //             }

        //         },
        //         error: function(xhr, status, error) {},
        //     })
        // }







        // function updateBseDate(portfolio_id,pending_type,status,e){

        //     var dataString = 'portfolio_id=' + portfolio_id + '&status=' + status + '&pending_type=' + pending_type;
        //     console.log(dataString);
        //     $.ajax({
        //         type: "POST",
        //         url: "/admin/updateBseDate",
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         cache: !1,
        //         processData: !1,
        //         data: dataString,
        //         beforeSend: function() {},
        //         success: function(data) {
        //             console.log(data.msg);

        //             if (data.msg == 'date_success') {


        //                 console.log(data.portfolio_id);
        //                 $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').prop('checked',true);
        //                 $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').next().find('span').text(data.date);

        //             }

        //             if (data.msg == 'nodate_success') {
        //                 console.log(data.portfolio_id);
        //                 $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').prop('checked',false);
        //                 console.log(data.portfolio_id);
        //                 $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').next().find('span').text('');
        //             }

        //         },
        //         error: function(xhr, status, error) {},
        //     })
        // }





        function checkSipReg(sip_id,scheme_code){
          var dataString = 'sip_id=' +sip_id+'&scheme_code='+scheme_code;
          var response;
            console.log(dataString);
            $.ajax({
                type: "POST",
                url: "/admin/checkSipReg",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                async:false,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                   response = data.msg;

                },
                error: function(xhr, status, error) {},
            })

            return response;
        }

        //Pending type may be NEW ORDER or WITHDRAWAL




        $(document).on('click','.confirm-order',function(){
          var investment_container = $(this).closest('.pending-details-container').find('.portfolio_scheme:checkbox:checked');

          var portfolio_ids = [];
          $.each(investment_container,function(){
              portfolio_ids.push($(this).data('pid'));
          })

          console.log(portfolio_ids);
          var order_type = 'inv';
          
          completePendingOrders(portfolio_ids,order_type);
          
        })



        $(document).on('click','.confirm-withdraw',function(){
          

          var withdrawal_container = $(this).closest('.pending-details-container').find('.portfolio_scheme:checkbox:checked');

          var withdrawal_ids = [];
          $.each(withdrawal_container,function(){
              withdrawal_ids.push($(this).data('pid'));
          })

          var order_type = 'wd';

          completePendingOrders(withdrawal_ids,order_type);
          
        })


        function completePendingOrders(ids,order_type){

          console.log(order_type);
          var dataString = 'order_ids=' +ids+ '&order_type=' +order_type;
          console.log(dataString);

          $.ajax({
                type: "POST",
                url: "/admin/completePendingOrders",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                if (data.msg == 'success') {
                    $('#notify-done').attr('onclick', "javascript:location.href='/admin/pending_orders'");
                    $('#great').html('Great !');
                    $('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.amount + '</span> on <span class="blue">' + data.date + '</span>');
                    $('#scheduleInvModal').modal('show')
                } else {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Oops !');
                    $('#you-have').html(data.status);
                    $('#scheduleInvModal').modal('show')
                }
                },
                error: function(xhr, status, error) {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Oops !');
                    $('#you-have').html('Something went wrong. Please try again');
                    $('#scheduleInvModal').modal('show')
                },
            })

        }

          $(document).on('click','.close-pending-details',function(){
              var p_type = $(this).data('ptype');
              var p_id = $(this).data('pid');

              var dataString = 'id=' +p_id+ '&transaction_type=' +p_type;
              console.log(dataString);

              $.ajax({
                type: "POST",
                url: "/admin/cancel_pending_order",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                    if (data.msg == 'success') {
                        $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                        $('#great').html('Great !');
                        $('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.amount + '</span> on <span class="blue">' + data.date + '</span>');
                        $(this).closest("tr").remove();
                        $('#scheduleInvModal').modal('show')
                    } else {
                        $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                        $('#great').html('Oops !');
                        $('#you-have').html(data.status);
                        $('#scheduleInvModal').modal('show')
                    }
                },
                error: function(xhr, status, error) {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Oops !');
                    $('#you-have').html('Something went wrong. Please try again');
                    $('#scheduleInvModal').modal('show')
                },
            })




          });


           $(document).on('change','#allotment-status',function(){
                 

                  if ($(this).val() == '') {
                    alert('No file Found');
                  }else{
                      var file = $(this).get(0).files[0];
                      var file_type = file['type'];

                      var ext = $(this).val().split('.').pop();
                      if (ext == 'txt') {
                          $('#confirmAllotModal').modal('show');

                      }else{
                        alert("Wrong File Format");
                      }
                  }
            });


            

           $(document).on('click','#confirm-allot-upload',function(){
                  $('#allotment-status-form').submit();
           });


           $(document).on('submit','#allotment-status-form',function(e){
              e.preventDefault();

              var file = $('#allotment-status').get(0).files[0];

              var formData = new FormData();
              formData.append('allotment-file',file);


              $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '/admin/update_sip_allotment_status',
                        data: formData,
                        async : false,
                        contentType: false,
                        processData: false,
                        success:function(data){
                          if (data.msg == "success") {
                            $('#you-have').text(data.response);
                            $('#confirmAllotModal').modal('hide');
                            $('#scheduleInvModal').modal('show');
                          }else{
                            $('#you-have').text(data.response);
                            $('#confirmAllotModal').modal('hide');
                            $('#scheduleInvModal').modal('show');
                          }
                        },
                        error:function(){ 
                            
                        }
                    });

           });


          $(document).on('submit','#redemption-status-form',function(e){
              e.preventDefault();

              var file = $('#redemption-status').get(0).files[0];

              var formData = new FormData();
              formData.append('redemption-file',file);


              $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '/admin/update_redemption_status',
                        data: formData,
                        async : false,
                        contentType: false,
                        processData: false,
                        success:function(data){
                          if (data.msg == "success") {
                            $('#you-have').text(data.response);
                            $('#confirmRedemptionModal').modal('hide');
                            $('#scheduleInvModal').modal('show');
                          }else{
                            $('#you-have').text(data.response);
                            $('#confirmRedemptionModal').modal('hide');
                            $('#scheduleInvModal').modal('show');
                          }

                        },
                        error:function(){ 
                            
                        }
                    });

           });


              //$('#confirmAllotModal').modal('show');


    });