$(document).ready(function() {
    $('input[type="number"]').on('keyup', function() {
        var id = $(this).attr('id');
        var current_value = parseInt($('#' + id + '_current_value').text());
        var withdraw_value = parseInt($(this).val());
        if (withdraw_value > current_value) {
            $(this).val('');
            $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
            $('#great').html('Oops !');
            $('#you-have').html('Withdraw Value cannot be greater than Current Value');
            $('#scheduleInvModal').modal('show')
        }
    });
    $('#wd-btn').on('click', function() {
        var withdraw_funds;
        var withdraw_funds_code = [];
        var withdraw_funds_status = !0;
        var data = 'withdraw_funds=';
        $("input[type='checkbox']:checked").each(function() {
            withdraw_funds_code.push(this.value)
        });
        if (withdraw_funds_code.length !== 0) {
            for (var i = 0; i < withdraw_funds_code.length; i++) {
                var current_value = parseInt($('#' + withdraw_funds_code[i] + '_current_value').text());
                var withdraw_value = $('#' + withdraw_funds_code[i]).val() != '' ? parseInt($('#' + withdraw_funds_code[i]).val()) : 0;
                if ((withdraw_value > current_value) || (withdraw_value < 1)) {
                    withdraw_funds_status = !1
                } else {
                    data += withdraw_funds_code[i] + '/' + withdraw_value + '|'
                }
            }

            console.log(data);
            if (withdraw_funds_status) {
                $.ajax({
                    type: "POST",
                    url: "/withdraw_user_funds",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: data,
                    cache: !1,
                    processData: !1,
                    success: function(data) {
                        if (data.msg == 'success') {
                            $('#notify-done').attr('onclick', "javascript:location.href='/home'");
                            $('#great').html('Great !');
                            $('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.amount + '</span> on <span class="blue">' + data.date + '</span>');
                            $('#scheduleInvModal').modal('show')
                        } else {
                            $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                            $('#great').html('Oops !');
                            $('#you-have').html(data.status);
                            $('#scheduleInvModal').modal('show')
                        }
                    },
                    error: function(xhr, status, error) {
                        $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                        $('#great').html('Oops !');
                        $('#you-have').html('Something went wrong. Please try again');
                        $('#scheduleInvModal').modal('show')
                    }
                })
            } else {
                $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                $('#great').html('Oops !');
                $('#you-have').html('Please check withdraw amount and try again.');
                $('#scheduleInvModal').modal('show')
            }
        } else {
            $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
            $('#great').html('Oops !');
            $('#you-have').html('Please select atleast one scheme to withdraw');
            $('#scheduleInvModal').modal('show')
        }
    });
    $('#send_email').on('click', function() {
        $.ajax({
            type: "GET",
            url: "/send_email",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            success: function(data) {
                alert(data)
            },
            error: function(xhr, status, error) {}
        })
    });

    $('.custom-amount').on('keydown',function(e){
            if (e.shiftKey) {
                e.preventDefault();
            }
    });
    $('.checkbox-div input').on('change', function() {
        var parentDiv = $(this).parents(".checkbox-div:eq(0)");
        if ($(this).is(":checked")) {
            $(parentDiv).find("img").attr("src", "icons/check_en.png")
        } else {
            $(parentDiv).find("img").attr("src", "icons/check_dis.png")
        }
    })
})