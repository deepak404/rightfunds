$(document).ready(function() {
    var form_submit = !1;
    $(document).on('click', ".suggested-user", function() {
        var user_id = $(this).data('id');
        $('#user_id').val(user_id);
        get_user_details()
    });

    function appendUserDetails(data) {
        if (data.msg == "success") {
            var order_history_details = '';
            $('#order_history_data').html('');
            $.each(data.order_history, function(key, response) {
                var status;
                if(response.status == "1"){
                    status = "Success";
                }else if (response.status == "0") {
                    status = "Pending";
                }else if(response.status == "2"){
                    status = "Failed";
                }else{
                    status = "Cancelled";
                }
                order_history_details += '<tr class="table-row"><td><div class="col-xs-12"><p class="cus-name">' + response.user_name + '</p><p class="cus-phone">' + response.contact_no + '</p></div></td><td>' + response.investment_or_withdraw + '</td><td>' + response.investment_type + '</td><td class="status">' + '<p>'+status+'</p>' + '</td><td>' + response.portfolio_type + '</td><td>' + response.date + '</td><td>' + response.amount + '</td><td><a target="_blank" href="/admin/modify_details/investment/id/' + response.id + '"><img src="../icons/pending-det.png" class = "pend-img"></a></td></tr>'
            });
            $('#order_history_data').html(order_history_details)
            updateColor()
        } else {}
    }
    $('.checkbox-div input').on('change', function() {
        var parentDiv = $(this).parents(".checkbox-div:eq(0)");
        if ($(this).is(":checked")) {
            $(parentDiv).find("img").attr("src", "../icons/check_en.png")
        } else {
            $(parentDiv).find("img").attr("src", "../icons/check_dis.png")
        }
    });


    function updateColor(){
        // var text = $(document).find('.status p').text();
        $('.status p').each(function(){
            var text = $(this).text();
            if (text == "Success") {
                $(this).addClass('green');
            }else if(text == "Pending"){
                $(this).addClass('orange');
            }else if(text == "Failed"){
                $(this).addClass('red');
            }else{
                $(this).addClass('orange');
            }
        });
    }

    function get_user_details() {
        var user_id = $('#user_id').val();
        var range = $('#transact_range option:selected').val();
        var from_date = $('#from_exp').val();
        var to_date = $('#to_exp').val();
        var millisBetween = 0;
        if (from_date != '' || to_date != '') {
            var start = from_date.split('-');
            var to = to_date.split('-');
            var startDay = new Date(start[2], start[1] - 1, start[0]);
            var endDay = new Date(to[2], to[1] - 1, to[0]);
            millisBetween = endDay.getTime() - startDay.getTime()
        }
        if (user_id != '') {
            if (millisBetween >= 0) {
                var datastring = 'user_id=' + user_id + '&from_exp=' + from_date + '&to_exp=' + to_date + '&response_type=data'
            } else {
                var datastring = 'user_id=' + user_id + '&from_exp=""&to_exp=""&response_type=data'
            }
            $.ajax({
                type: "POST",
                url: "/admin/get_ind_user_order_history",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: datastring,
                cache: !1,
                processData: !1,
                beforeSend: function() {},
                success: function(data) {
                    if (data.msg == 'success') {
                        appendUserDetails(data)
                    }
                },
                error: function(xhr, status, error) {},
            })
        } else {}
    }
    $('#export_data').on('submit', function(e) {
        var formData = $('#export_data').serialize();
        if ($('#transaction_range').val() != '' && $('#user_id').val() != '' && $('#response_type').val() != '') {
            return !0
        }
    });
    $('#from_exp,#to_exp').on('change', function() {
        get_user_details()
    })
})