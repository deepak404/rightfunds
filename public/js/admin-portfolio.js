$(document).ready(function() {
    var form_submit = false;
    $('.units-held-p').on('click', function() {
        $('#p-unit-val').on('click', function() {
            $(this).hide();
            $('#inp-units-val').show();
            $('#inp-units-val').val($(this).text())
        });
        $('#inp-units-val').on('blur', function() {
            var changed_units = $(this).val();
            $(this).hide();
            $('#p-unit-val').text(changed_units).show()
        })
    });
    $(document).on('click', ".edit-portfolio", function() {
        var port_details = Array();
        var child_len = $(this).parent().parent().children().length;
        var child = $(this).parent().parent().children();
        var parent = $(this).parent().parent().find('td');
        var count = 0;
        $.each(child, function() {
            port_details.push(parent.eq(count).text());
            count++
        });
        console.log(port_details);
        $('#port-edit-fund').val(port_details[0]).prop('disabled', !0);
        $('#port-edit-ptype').val(port_details[1]).prop('disabled', !0);
        $('#port-edit-folio').val(port_details[2]);
        $('#port-edit-pt').val(port_details[11]).prop('disabled', !0);
        $('#port-edit-date').val(port_details[3]);
        $('#port-edit-ai').val(port_details[4]).prop('disabled', !0);
        $('#port-edit-nav').val(port_details[5]);
        $('#port-edit-uh').val(port_details[6]).prop('disabled', !0);
        $('#port-edit-c-nav').val(port_details[7]).prop('disabled', !0);
        $('#port-edit-cv').val(port_details[8]).prop('disabled', !0);
        $('#port-edit-nr').val(port_details[9]).prop('disabled', !0);
        $('#port-edit-ltcg').val(port_details[10]).prop('disabled', !0);
        $('#p_id').val($(this).data('id'));
        $('#editPortfolio').modal('show')
    });
    $('#port-edit-date').datepicker({
        "dateFormat": "dd-mm-yy",
        "minDate": -7,
        "stepMonths": !0,
        'maxDate': 0,
    });
    $('#withdraw_date').datepicker({
        "dateFormat": "dd-mm-yy",
        "minDate": -7,
        "stepMonths": !0,
        'maxDate': 0,
    });

    $('#inv-date').datepicker({
        "dateFormat": "dd-mm-yy",
        changeMonth:true,
        changeYear:true,
    });
    $('#wd-btn').on('click', function() {
        $('#wdModal').modal('show')
    });

    $('#inv-btn').on('click', function() {
        $('#manual-investment').trigger('reset');
        $('.portfolio-holder').empty();
        $('#invModal').modal('show')
    });
    $(document).on('submit', '#manual-withdraw', function(e) {
        e.preventDefault(e);
        formdata = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/admin/manual_withdraw",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            data: formdata,
            cache: false,
            processData: false,
            success: function(data) {
                if (data.msg == 'success') {
                    alert(data.status)
                } else {
                    alert(data.status)
                }
            },
            error: function(xhr, error, status) {},
        })
    });
    $(document).on('click', ".suggested-user", function() {
        var user_id = $(this).data('id');
        $('#user_id').val(user_id);
        $('#inv_user_id').val(user_id);
        get_user_details()
    });

    function appendUserDetails(data) {
        if (data.msg == "success") {
                $('#inv_port').attr('href','/admin/portfolio_details/'+data.id);
                $('#inv_his').attr('href','/admin/inv_history/'+data.id);
                $('#inv_ts').attr('href','/admin/tax_saving/'+data.id);
            var investment_performance = '';
            //$('#user_performance_det').html('');
            $.each(data.response, function(key, response) {
                $('.profile-name').text(response.name);
                $('#search-email').text(response.email);
                $('#srch-user-no').text(response.mobile);
                $('#amount_invested').text(response.amount_invested);
                $('#current_value').text(response.current_value);
                $('#profit_loss').text(response.profit_loss_amount);
                $('#xirr_return').text(response.xirr_return);

                // user_personal_details = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block "><div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><img class="img profile-image" src="/assets/profile_images/' + response.profile_image + '" id = "search-image"></div><div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content"><p class="profile-name">' + response.name + '!</p><p class="invest-text" id="search-email">' + response.email + '</p><p id ="srch-user-no">' + response.mobile + '</p></div></div>';
                // user_investment_details = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Current Value</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="amount_invested">' + response.amount_invested + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Total Investment</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="current_value">' + response.current_value + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Net Profit</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="profit_loss">' + response.profit_loss_amount + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Return (XIRR)</p><p class="return-det"><span id="xirr_return">' + response.xirr_return + '</span>%</p></div></div>';
                // investment_performance += '<div class = "row investment-summary" id="user-det-row">' + user_personal_details + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><p class="srch-options"> Rebalancing Required</p><img src= "../icons/rebal.png" class="center-block" id="rebal-icon"></div></div>' + user_investment_details + '</div>'
            });
            // $('#user_performance_det').html(investment_performance);
            // $('#user_performance_det').append(investment_performance);
            //$('#debt_body').html('');
            //$('#portfolio-summary-details').html('');
            var portfolio_summary_content = '';
            if (data.portfolio_summary != '') {
                $.each(data.portfolio_summary, function(key, value) {
                    portfolio_summary_content += '<tr id="' + value.id + '">'+
                    '<td>' + value.fund_name + '</td>'+
                    '<td>' + value.type + '</td>'+
                    '<td>' + value.folio_number + '</td>'+
                    '<td>' + value.date + '</td>'+
                    '<td>' + value.amount + '</td>'+
                    '<td>' + value.inv_nav + '</td>'+
                    '<td><div class="units-held-p"><p>' + value.units_held + '</p></div></td>'+
                    '<td>' + value.current_nav + '</td>'+
                    '<td>' + value.current_value + '</td>'+
                    '<td>' + value.net_returns + '</td>'+
                    '<td class="ltc-amount">' + value.ltcg + '</td>'+
                    '<td>' + value.transaction_type + '</td>'+
                    '<td><a data-id = "' + value.id + '" class="edit-portfolio"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>'+
                    '</tr>'
                });
                portfolio_summary_content += '<tr>'+
                '<td class="total bt-green ">TOTAL</td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green ">' + data.portfolio_summary_sum.total_initial_investment + '</td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green ">' + data.portfolio_summary_sum.total_current_value + '</td>'+
                '<td class="total bt-green ">' + data.portfolio_summary_sum.total_net_return + '</td>'+
                '<td class="total  bt-green ltc-amount">' + data.portfolio_summary_sum.total_ltcg + '</td>'+
                '<td class="total bt-green "></td>'+
                '<td class="total bt-green "></td>'+
                '</tr>'
            } else {
                portfolio_summary_content += '<tr>'+
                '<td></td>'+
                '<td></td>'+
                '<td colspan="10" >No Portfolio during the transaction range.</td>'+
                '</tr>'
            }
            $('#portfolio-summary-details').html(portfolio_summary_content)
        } else {}

        $('#user-det-row').next('div.investment-summary').show();
    }
    $('.checkbox-div input').on('change', function() {
        var parentDiv = $(this).parents(".checkbox-div:eq(0)");
        if ($(this).is(":checked")) {
            $(parentDiv).find("img").attr("src", "../icons/check_en.png")
        } else {
            $(parentDiv).find("img").attr("src", "../icons/check_dis.png")
        }
    });

    function get_user_details() {
        var user_id = $('#user_id').val();
        var range = $('#transact_range option:selected').val();
        var datastring = 'user_id=' + user_id + '&transaction_range=' + range + '&response_type=data';
        $.ajax({
            type: "POST",
            url: "/admin/get_ind_user_portfolio_performance",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: datastring,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                if (data.msg == 'success') {
                    appendUserDetails(data)
                }
            },
            error: function(xhr, status, error) {},
        })
    }
    $('#transact_range').on('change', function() {
        get_user_details();
        var range = $('#transact_range option:selected').val();
        $('#transaction_range').val(range)
    });
    // $('#export_data').on('submit', function(e) {
    //     if (!form_submit) {
    //         e.preventDefault();
    //         var formData = $('#export_data').serialize();
    //         $('#export_data').submit()
    //         // if ($('#user_id').val() != '' && $('#response_type').val() != '') {
    //         //     console.log('formAction');
    //         //     form_submit = !0;
    //         // }
    //     }
    // });
    $(document).on('submit', '#update-portfolio-form', function(e) {
        e.preventDefault();
        var port_id = $('#p_id').val();
        var update_date = $('#port-edit-date').val();
        var inv_nav = $('#port-edit-nav').val();
        var folio_number = $('#port-edit-folio').val();
        var trans_type = $('#port-edit-pt').val();
        var dataString = 'p_id=' + port_id + '&date=' + update_date + '&inv_nav=' + inv_nav + '&folio_number=' + folio_number + '&transaction_type=' + trans_type;
        $.ajax({
            type: "POST",
            url: "/admin/update_portfolio_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: dataString,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                updatePortfolio(data)
            },
            error: function(xhr, status, error) {},
        })
    });

    function updatePortfolio(data) {
        if (data.msg == "1") {
            var table_entry = $(document).find('tr#' + data.response.id);
            table_entry.find('td').eq(2).text(data.response.folio_number);
            table_entry.find('td').eq(3).text(data.response.investment_date);
            table_entry.find('td').eq(5).text(data.response.invested_nav);
            table_entry.find('td').eq(6).text(data.response.units_held);
            table_entry.find('td').eq(8).text(data.response.current_val);
            table_entry.find('td').eq(9).text(data.response.net_return);
            table_entry.find('td').eq(10).text(data.response.ltcg);
            $('#editPortfolio').modal('hide')
        }
    }


    $('#add-portfolio').on('click',function(){

        var port_children = $('.portfolio-holder').children().length;
        var append_next_value = port_children + 1;
        $('.portfolio-holder').append(
            '<div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero port-child-holder" id = "port-child-'+append_next_value+'">'+
            '<div class="col-xs-3">'+
                '<p class="form-helper">Scheme Code</p>'+
                  '<div class="form-group">'+
                    '<input class="port-scheme-code signup-f-input" type="text" name="scheme-code" id="scheme-code" required>'+
                  '</div>'+
              '</div>'+
              '<div class="col-xs-3">'+
                '<p class="form-helper">Amount Invested</p>'+
                  '<div class="form-group">'+
                    '<input class="port-amount-inv signup-f-input" type="text" name="amount-inv" id="amount-inv" required>'+
                  '</div>'+
              '</div>'+

              '<div class="col-xs-3">'+
                '<p class="form-helper">Units Held</p>'+
                  '<div class="form-group">'+
                    '<input class="port-units-held signup-f-input" type="text" name="units-held" id="units-held" required>'+
                  '</div>'+
              '</div>'+

              '<div class="col-xs-3">'+
                '<p class="form-helper">Invested NAV</p>'+
                  '<div class="form-group">'+
                    '<input class="port-invested-nav signup-f-input" type="text" name="invested-nav" id="invested-nav" required>'+
                  '</div>'+
              '</div>'+

              '<div class="col-xs-3">'+
                '<p class="form-helper">Folio Number</p>'+
                  '<div class="form-group">'+
                    '<input class="port-folio-number signup-f-input" type="text" name="folio-number" id="folio-number" required>'+
                  '</div>'+
              '</div>'+
              '</div>'
              );
    });


        $('#manual-investment').on('submit',function(e){

            e.preventDefault();
            var user_id = $('#inv_user_id').val();
            var inv_amount = $('#inv_amount').val();
            var inv_type = $('#inv-type').val();
            var port_type = $('#portfolio-type').val();
            var inv_date = $('#inv-date').val();
            var payment_type = $('#payment-type').val();
            var dataString = 'inv_amount=' + inv_amount + '&user_id=' + user_id + '&inv_date=' + inv_date + '&inv_type=' + inv_type + '&port_type=' + port_type + '&payment_type=' + payment_type;
            
            var port_children = $('.portfolio-holder').children().length;
            
            if (port_children == 0) {
                alert('Kindly Add portfolio Details');
            }else{

                var port_details = [];
                var total_port_amount = 0;
                $('.port-child-holder').each(function(){
                    total_port_amount += parseFloat($(this).find('.port-amount-inv').val());
                    port_details.push({
                                'scheme_code': $(this).find('.port-scheme-code').val(),
                                'inv_amount': $(this).find('.port-amount-inv').val(),
                                'units_held': $(this).find('.port-units-held').val(),
                                'invested_nav' : $(this).find('.port-invested-nav').val(),
                                'folio_number' : $(this).find('.port-folio-number').val()
                            })

                });

                console.log(port_details);
                console.log(total_port_amount,inv_amount);

                if (total_port_amount != inv_amount) {
                    alert('Investment Amount and Portfolio Sum doesn\'t match' );
                }else{

                    var formData = new FormData();

                    console.log(inv_amount,user_id,inv_type,port_type,inv_date,payment_type);

                    formData.append('inv_amount',inv_amount);
                    formData.append('user_id',user_id);
                    formData.append('inv_type',inv_type);
                    formData.append('port_type',port_type);
                    formData.append('inv_date',inv_date);
                    formData.append('payment_type',payment_type);
                    myString = JSON.stringify(port_details);
                    formData.append('port_details',myString);

                    console.log(formData);
                    console.log(total_port_amount,inv_amount);
                    //var dataString = 'inv_amount=' + inv_amount + '&user_id=' + user_id + '&inv_date=' + inv_date + '&inv_type=' + inv_type + '&port_type=' + port_type + '&payment_type=' + payment_type + '&port_details=' + port_details;

                    $.ajax({
                        type: "POST",
                        url: "/admin/add_manual_portfolio",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: formData,
                        cache: !1,
                        contentType: !1,
                        processData: !1,
                        beforeSend: function() {},
                        success: function(data) {
                            if (data.msg == 'success') {
                                $('#update-info').text('');
                                $('#update-info').text(data.response);
                                $('#showInfoModal').modal('show');
                            }
                        },
                        error: function(xhr, status, error) {},
                    })

                }
            }
            
        });

        //$('#showInfoModal').modal('show');
})