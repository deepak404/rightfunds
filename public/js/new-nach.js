$(document).ready(function(){


	var video_link = '';
	var cc_link = '';
	var mandate_link = '';
    var userId = '';
    $(document).on('click', '.nach_user_container', function(){

		var dataString = 'user_id=' + $(this).data('id');
        var route = '/admin/get_user_nach_details';

       	var data = ajaxCalls(dataString,route);
       		//Handling ajax Response
       	//console.log(data);
       	updateNachContainer(data);
       				
	})
    $(document).on('click', ".suggested-user", function() {
        
        var dataString = 'user_id=' + $(this).data('id');
        var route = '/admin/get_user_nach_details';

        var data = ajaxCalls(dataString,route);

        updateNachContainer(data);
    });

	function updateNachContainer(data){

		console.log(data);
		if (data.msg == "success") {
                if (data.user_state == null) {
                    $('#status-change').val(0);
                }else{
                    $('#status-change').val(data.user_state);
                }
			//console.log(data.response);
        		$.each(data.response,function(key,value){
        			console.log(value);
        			$('#active_user_name').text(value.user_name);

        			if (value.user_pan == '') {
        				$('#active_user_pan').text("PAN Not received yet.");
        			}else{
        				$('#active_user_pan').text(value.user_pan);
        			}
                    if (value.mandatesoft_status == 1 && value.mandatehard_status == 1 && value.bsemandate_status == 1) {
                        $('#mandate-id').addClass('active-checkicon');
                    }else{
                        $('#mandate-id').removeClass('active-checkicon');
                    }
                    if (value.bseclient_status == 1 && value.aof_uploaded == 1 && value.bseaof_status == 1) {
                        $('#bse-id').addClass('active-checkicon');
                    }else{
                        $('#bse-id').removeClass('active-checkicon');
                    }
                    if (value.pan_status == 1 && value.kycvideo_status == 1 && value.kyc_status == 1 && value.fatca_status == 1) {
                        $('#cams-id').addClass('active-checkicon');
                    }else{
                        $('#cams-id').removeClass('active-checkicon');
                    }
                    if (value.cc_status == 1) {
                        $('#bank-id').addClass('active-checkicon');
                    }else{
                        $('#bank-id').removeClass('active-checkicon');
                    }

                    if (value.mandatesoft_status == 1 && value.mandatehard_status == 1 && value.bsemandate_status == 1 && value.pan_status == 1 && value.kycvideo_status == 1 && value.kyc_status == 1 && value.fatca_status == 1 && value.cc_status == 1) {
                        $('#complete-btn-id').removeAttr("disabled");
                        $('#complete-btn-id').data('userid', value.user_id);
                    }else{
                        $('#complete-btn-id').attr("disabled", true);
                    }

        			$('#nach_content_bar').attr('data-user',value.user_id);
                    userId = value.user_id;

        			$('#pan_date').text(value.pan_date);
        			$('#pan_status').val(value.pan_status);
                    $('#bseclient_date').text(value.bseclient_date);
                    $('#bseclient_status').val(value.bseclient_status);
        			$('#kycvideo_date').text(value.kycvideo_date);
        			$('#kycvideo_status').val(value.kycvideo_status);
        			$('#kyc_date').text(value.kyc_date);
        			$('#kyc_status').val(value.kyc_status);
        			$('#cc_date').text(value.cc_date);
        			$('#cc_status').val(value.cc_status);
        			$('#mandatesoft_date').text(value.mandatesoft_date);
        			$('#mandatesoft_status').val(value.mandatesoft_status);
        			$('#mandatehard_date').text(value.mandatehard_date);
        			$('#mandatehard_status').val(value.mandatehard_status);
        			$('#aof_date').text(value.aof_date);
        			$('#aof_uploaded').val(value.aof_uploaded);
        			$('#camskyc_date').text(value.camskyc_date);
        			$('#camskyc_status').val(value.camskyc_status);
        			$('#bsemandate_date').text(value.bsemandate_date);
        			$('#bsemandate_status').val(value.bsemandate_status);
        			$('#bseaof_date').text(value.bseaof_date);
        			$('#bseaof_status').val(value.bseaof_status);
                    $('#fatca_status').val(value.fatca_status);
                    $('#fatca_date').val(value.fatca_date);
                    $('#mandate_link').val(value.mandate_link);



          	// 		$('#active_user_mobile').text(value.user_mobile);
        			// $('#active_user_email').text(value.user_email);
        			// $('#pan_update_date').text(value.pan_date);
        			// $('#ekyc_update_date').text(value.kyc_date);
        			// $('#ekyc_video_date').text(value.ekyc_date);
        			// $('#cc_date').text(value.cc_date);
        			// $('#mandate_soft_date').text(value.mandate_soft_date);
        			// $('#aof_update_date').text(value.aof_date);
        			// $('#mandate_status_date').text(value.bse_mandate_date);
        			// $('#nach_content_bar').attr('data-user',value.user_id);
        			// $('#pan_status').val(value.pan_status);
        			// $('#kyc_status').val(value.kyc_status);
        			// $('#ekyc_status').val(value.ekyc_status);
        			// $('#cc_status').val(value.cc_status);
        			// $('#mandate_soft_status').val(value.mandate_soft_status);
        			// $('#aof_status').val(value.aof_status);
        			// $('#bse_mandate_status').val(value.bse_mandate_status);

        		})

        		$.each(data.ekyc,function(key,value){

        			video_link = value.video_link;
					cc_link = value.cc_link;
					mandate_link = value.mandate_link;

                    if ((video_link != null) && (video_link != '')){
                        $('#ekyc-video-update').text('EKYC Video Done. Kindly Check them.').removeClass('file-neg-alert').addClass('file-pos-alert');
                    }else{
                        $('#ekyc-video-update').text('EKYC Video not uploaded yet.').removeClass('file-pos-alert').addClass('file-neg-alert');
                    }

                    if ((cc_link != null) && (cc_link != '')) {
                        $('#cc-update').text('Cancelled Cheque uploaded. Kindly Check them.').removeClass('file-neg-alert').addClass('file-pos-alert');
                    }else{
                        $('#cc-update').text('Cancelled Cheque not uploaded yet.').removeClass('file-pos-alert').addClass('file-neg-alert');
                    }

                    if ((mandate_link != null) && (mandate_link != '')){
                        $('#mandate-update').text('Mandate uploaded. Kindly Check them.').removeClass('file-neg-alert').addClass('file-pos-alert');
                    }else{
                        $('#mandate-update').text('Mandate not uploaded yet.').removeClass('file-pos-alert').addClass('file-neg-alert');
                    }

					console.log(cc_link);
					console.log(mandate_link);
        		});
        }

	}

	$('.nach-select').on('change',function(){
		var option = $(this).val();
		var col_name = $(this).attr('id');
		var user_id = $(this).closest('#nach_content_bar').attr('data-user');
		console.log(user_id);
		var dataString = 'option=' +option+'&col_name='+col_name+'&user_id='+user_id;
		console.log(dataString);
		
		var route = '/admin/update_user_nach_details';

       	var data = ajaxCalls(dataString,route);
       	console.log(data)
       	updateNachContainer(data)
	})

	function ajaxCalls(dataString,route){

		var response;
		console.log(route);
		$.ajax({
                type: "POST",
                url: route,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                async:false,
                beforeSend: function() {},
                success: function(data) {
                	
                	response = data;
                	//console.log(response);
                },
                error: function(xhr, status, error) {

                },
            })

		return response;
	}

	$('#play_ekyc').on('click',function(){

		//console.log(video_link);
		$('#inner_modal_body').empty();
		$('#inner_modal_body').append(
			'<p id="great">EKYC Video</p>'+
			'<video width = "100%" controls>'+
			  '<source src="../ekycvideo/'+video_link+'" type="video/mp4">'+
			  // '<source src="mov_bbb.ogg" type="video/ogg">'+
			  'Your browser does not support HTML5 video.'+
			'</video>');
		$('#nach_popup').modal('show');
	});


	$('#mandate_soft').on('click',function(){
		$('#inner_modal_body').empty();
		console.log(mandate_link);
		$('#inner_modal_body').append(
			'<p id="great">Mandate Soft Copy</p>'+
			'<img style = "width:100%;" src="/files/mandates/'+mandate_link+'">'
			);
		$('#nach_popup').modal('show');

	});

	$('#show_cheque').on('click',function(){
		$('#inner_modal_body').empty();
		$('#inner_modal_body').append(
			'<p id="great">Cancelled Cheque / Bank Soft Copy</p>'+
			'<img style = "width:100%;" src="/files/cc/'+cc_link+'">'
			);
		$('#nach_popup').modal('show');

	});


	//Assigning Null to all the value on Page Load

					

	        		$('#pan_date').text('');
        			$('#pan_status').val(null);
                    $('#bseclient_date').text('');
                    $('#bseclient_status').val(null);
        			$('#kycvideo_date').text('');
        			$('#kycvideo_status').val(null);
        			$('#kyc_date').text('');
        			$('#kyc_status').val(null);
        			$('#cc_date').text('');
        			$('#cc_status').val(null);
        			$('#mandatesoft_date').text('');
        			$('#mandatesoft_status').val(null);
        			$('#mandatehard_date').text('');
        			$('#mandatehard_status').val(null);
        			$('#aof_date').text('');
        			$('#aof_uploaded').val(null);
        			$('#camskyc_date').text('');
        			$('#camskyc_status').val(null);
        			$('#bsemandate_date').text('');
        			$('#bsemandate_status').val(null);
        			$('#bseaof_date').text('');
        			$('#bseaof_status').val(null);
                    $('#fatca_date').text('');
                    $('#fatca_status').val(null);
                    $('#mandate_link').val('');

        			//$('#nach_popup').modal('show');
    $('.priority-star').on('click',function(){
        var data = $(this).data('id');
        if ($(this).hasClass('user-imp')) {
            $(this).removeClass('user-imp');
            priorityUpdate(data,2);
        }else{
            $(this).addClass('user-imp');
            priorityUpdate(data,1);
        }
    });

    function priorityUpdate(data,status){
        var postData = 'user_id='+data+'&status='+status;
        // console.log(postData);
        $.ajax({
            type: "POST",
            url: "/admin/update_priority_info",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: postData,
            success: function(data) {

            },
            error: function(xhr, status, error) {
                alert(error);
            },
        });
    }

    $('#complete-btn-id').on('click',function(){
        var user_id = $('#complete-btn-id').data('userid');
        $.ajax({
            type: "POST",
            url: "/admin/nach_complete",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: 'user_id='+user_id,
            success: function(data) {
                if (data.msg == 1) {
                    location.reload(); 
                }else{
                    alert('try Again!!!');
                }
            },
            error: function(xhr, status, error) {
                alert(error);
            },
        });
    });

    $('#add_btn').on('click',function(){
        var mandate_link =  JSON.stringify($('#mandate_link').val());
        $.ajax({
            type: "POST",
            url: "/admin/add_mandate_link",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {user_id:userId,
                    mandate_link:mandate_link},
            success: function(data){
                if (data.msg == 1) {
                    alert('success');
                }else{
                    alert('error');
                }
            },
            error: function(xhr, status, error) {

            },
        });
    });

    $('#status-change').on('change',function(){
        var option = $(this).val();
        var user_id = $(this).closest('#nach_content_bar').attr('data-user');
        var formData = 'user_id='+user_id+'&val='+option;
        $.ajax({
            type: "POST",
            url: "/admin/bpo_status_update",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function(data){
               
            },
            error: function(xhr, status, error) {

            },
        });
    });

    $('.filter-select').on('change',function(){
        var filter_val = 'filter='+$(this).val();
        $.ajax({
            type: "POST",
            url: "/admin/filter_list",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: filter_val,
            success: function(data){
               if (data.msg == 1) {
                $('#nach_user_sidebar').empty();
                $.each(data.data,function(key,value){
                    $('#nach_user_sidebar').append('<div class="col-lg-12 col-md-12 col-sm-12 border-bot nach_user_container" data-id="'+value.user_id+'">'+
                                            '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">'+
                                              '<p class="nach_user_name">'+value.user_name+'</p>'+
                                              '<p class="nach_user_pan">'+value.user_pan+'</p>'+
                                              '<p class="nach_user_pan">'+value.user_mobile+'</p>'+
                                              '<p class="nach_user_pan">'+value.user_email+'</p>'+
                                            '</div>'+
                                          '</div>');
                    
                });
               }else{
                console.log('error');
               }
            },
            error: function(xhr, status, error) {

            },
        });
    });

});