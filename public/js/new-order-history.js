$(document).ready(function(){
	$(document).on('click','.confirm-revoke',function(){


          var investment_container = $(this).closest('.pending-details-container');
          var checked_checkboxes = investment_container.find('.portfolio_scheme:checkbox:checked')
          
          var order_type = investment_container.data('ordertype');
          var portfolio_ids = [];
          $.each(checked_checkboxes,function(){
              portfolio_ids.push($(this).data('pid'));
          })


          console.log(portfolio_ids.length);



          if (order_type == "wd") {

          	if (portfolio_ids.length == 0) {
          		portfolio_ids = "empty";	
          	}

          	var order_id = investment_container.data('wid');
          	revokeOrders(order_id,order_type,portfolio_ids);
          	
          }

          if (order_type == 'inv') {

          	if (portfolio_ids.length == 0) {
          		portfolio_ids = "empty";	
          	}

          	var order_id = investment_container.data('invid');
          	revokeOrders(order_id,order_type,portfolio_ids);
          }
          
          
        })


		function revokeOrders(order_id,order_type,portfolio_ids){

			var dataString = 'portfolio_ids=' + portfolio_ids + '&order_type=' + order_type + '&order_id=' + order_id;
            console.log(dataString);

            $.ajax({
                type: "POST",
                url: "/admin/revoke_orders",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                    if (data.msg == "success") {
                    		$('#great').text('Great');
                    		$('#you-have').text('Successfully Revoked');
                    		$('#scheduleInvModal').modal('show');
                    }

                    if (data.msg == "failure") {
                    		$('#great').text('Oops');
                    		$('#you-have').text('Revoke Failed');
                    		$('#scheduleInvModal').modal('show');
                    }

                },
                error: function(xhr, status, error) {},
            })

		}



		
});