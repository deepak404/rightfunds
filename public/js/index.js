$('#equity-chart').donut();
$('#debt-chart').donut();
$('#elss-chart').donut();
$('#bal-chart').donut();
$(document).ready(function() {
    $('#close-inv-btn').on('click', function() {
        $('#goal-ach-div').slideUp('slow');
        $('.goal-amount-div').slideUp('slow');
        $('#x-button').slideUp('slow');
        $('#abv-x').slideUp('slow');
        $('#add-goal-text-link').fadeIn('slow')
    });
    var goal_amount = $('#target_goal_amount').html();
    goal_amount = goal_amount.replace(/,/g, '');
    goal_amount = parseInt(goal_amount);
    if (!(goal_amount > 0)) {}
    $('#add-goal-text-link').on('click', function() {
        $('#addGoalModal').modal('show');
        $('.navbar').css({
            'z-index': '0'
        })
    });
    $('#addGoalModal').on('hidden.bs.modal', function() {
        $('.navbar').css({
            'z-index': 'auto'
        })
    });
    $('#set-g-btn').on('click', function() {
        var goal_amount = $('#goal_amount').val();
        goal_amount = goal_amount.replace(/,/g, '');
        goal_amount = parseInt(goal_amount);
        var dataString = 'goal_amount=' + goal_amount;
        var totalcurrentamount = $('#total_current_value').html();
        totalcurrentamount = totalcurrentamount.replace(/,/g, '');
        totalcurrentamount = parseInt(totalcurrentamount);
        $.ajax({
            type: "POST",
            url: "/save_goal",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: dataString,
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg) {
                    $('#goal-row').css({
                        display: 'block'
                    });
                    var achieved_goal_percent = Math.round((totalcurrentamount / goal_amount) * 100);
                    console.log(goal_amount);
                    var progress_bar_content = '<progress class="amount-progress" value="' + totalcurrentamount + '" max="' + goal_amount + '">' + achieved_goal_percent + ' %</progress>';
                    $('#goal_progress').html(progress_bar_content);
                    $('#target_goal_amount').html(data.goal_amount);
                    console.log(progress_bar_content);
                    if (achieved_goal_percent < 100) {
                        $('#goal_achieved_percent').text(achieved_goal_percent + '%');
                        var achieve_status = '<span id="goal-away-text">You are only ' + (100 - achieved_goal_percent) + '% away from achieving your Goal.</span>';
                        var invest_button = '<button type="button" class = "btn btn-primary" id="invest-now-btn">INVEST NOW &nbsp;></button>';
                        $('#ach_away').html(achieve_status + invest_button);
                        $('#goal-away-text').text('You are only ' + (100 - achieved_goal_percent) + '% away from achieving your Goal.')
                    } else {
                        $('#goal_achieved_percent').text('100%');
                        var achieve_status = '<span id="goal-away-text">You have achieved your goal.</span>';
                        var invest_button = '<button type="button" class = "btn btn-primary" id="change_goal">CHANGE GOAL&nbsp;></button>';
                        $('#goal-away-text').text('You have achieved your Goal.');
                        $('#ach_away').html(achieve_status + invest_button)
                    }
                } else {
                    alert('unable to add goal amount')
                }
            },
            error: function(xhr, status, error) {}
        });
        $('#add-goal-text-link').fadeOut('slow');
        $('#goal-ach-div').slideDown('slow');
        $('.goal-amount-div').slideDown('slow');
        $('#x-button').slideDown('slow');
        $('#abv-x').slideDown('slow');
        $('#addGoalModal').modal('hide')
    });
    $('#change_goal').on('click', function() {
        $('#addGoalModal').modal('show');
        $('.navbar').css({
            'z-index': '0'
        })
    });
    $('#edit-goal').on('click', function() {
        $('#addGoalModal').modal('show');
        $('.navbar').css({
            'z-index': '0'
        })
    });
    $('.close-tab').on('click', function() {
        $(this).parent().slideUp()
    })
})







function removeRegBar(no){
	var formData = 'user_id='+no;
	console.log(formData);

	        $.ajax({
            type: "POST",
            url: "/reg_check",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                if (data.msg == "1") {
                	$('#reg_bar').slideUp();
                }
            },
            error: function(xhr, status, error) {
                
            },
        })
}