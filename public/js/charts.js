
    function drawChart() {
        var o=google.visualization.arrayToDataTable([["", "INVESTMENT AMOUNT", "RETURNS", {
            role: "style"
        }
        ], ["", 1e4, -2528, "color : #ff6666;stroke-width:0;"], ["", 1e4, -762, "color : #ff6666;stroke-width:0;"], ["", 1e4, 5037, "color : #18C3A1;stroke-width:0;"], ["", 1e4, 6851, "color : #18C3A1;stroke-width:0;"], ["", 1e4, 7019, "color : #18C3A1;stroke-width:0;"], ["", 1e4, 13366, "color : #18C3A1;stroke-width:0;"], ["", 1e4, 20638, "color : #18C3A1;stroke-width:0;"]]), t= {
            isStacked:!0, bar: {
                groupWidth: "35%"
            }
            , colors:["#0091EA", "#18C3A1"], legend: {
                position: "none"
            }
            , enableInteractivity: false, height:400, vAxis: {
                baselineColor:"#d7d7d7", gridlines: {
                    color: "transparent"
                }
                , ticks:[]
            }
            , tooltip: {
                trigger: "none"
            }
        }
        , e=new google.visualization.ColumnChart(document.getElementById("chart"));
        e.draw(o, t)
    }
    google.charts.setOnLoadCallback(drawChart);





