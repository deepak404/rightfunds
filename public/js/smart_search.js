$(document).ready(function()
	{
		var users=new Bloodhound({
			datumTokenizer:Bloodhound.tokenizers.whitespace('name'),
			queryTokenizer:Bloodhound.tokenizers.whitespace,
			remote:{
				url:'/admin/find?search-input=%QUERY%',
				wildcard:'%QUERY%',},
			});
		$('#search-input').typeahead({hint:!1,highlight:!1,minLength:1,},
			{
				source:users.ttAdapter(),
				name:'usersList',
				source:users,
				displayKey:'name',
				templates:{
					empty:['<div class = "suggestion no-user">No User Found!</div>'],
					header:['<div class="list-group search-results-dropdown">'],
					suggestion:function(data){
						user_data=data;
						return '<a data-id="'+user_data.id+'" data-name = '+user_data.name+' class="list-group-item suggested-user"> <span class="suggested-name">'+user_data.name+'</span> <br><span class="suggested-email">'+user_data.email+'</span><br><span class="suggested-email">'+user_data.mobile+'</span></a>'}
					}
				});
		var nach_user=new Bloodhound({
			datumTokenizer:Bloodhound.tokenizers.whitespace('name'),
			queryTokenizer:Bloodhound.tokenizers.whitespace,
			remote:{
				url: '/admin/find_nach?search_nach_user=%QUERY%',
				wildcard:'%QUERY',},
			});
		$('#search_nach_user').typeahead({hint:!1,highlight:!1,minLength:1,},
			{
				source:users.ttAdapter(),
				name:'usersList',
				source:users,
				displayKey:'name',
				templates:{
					empty:['<div class = "suggestion no-user">No User Found!</div>'],
					header:['<div class="list-group search-results-dropdown">'],
					suggestion:function(data){
						user_data=data;
						return '<a data-id="'+user_data.id+'" data-name = '+user_data.name+' class="list-group-item suggested-user"> <span class="suggested-name">'+user_data.name+'</span> <br><span class="suggested-email">'+user_data.email+'</span><br><span class="suggested-email">'+user_data.mobile+'</span></a>'}
					}
		});
	})
