$(document).ready(function(){
                $('#sche-sel-date').datepicker({
                inline: !0,
                showOtherMonths: !0,
                dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                dateFormat: "dd-mm-yy",
                minDate: "+1d",
                changeMonth: !0,
                changeYear: !0,
                beforeShow: function() {
                    setTimeout(function() {
                        $('.ui-datepicker').css('z-index', 99999999999999);
                        $('.ui-datepicker').css('padding', 0)
                    }, 0)
                }
            });


            $('#sip-date').datepicker({
                inline: !0,
                showOtherMonths: !0,
                changeMonth: !1,
                dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                dateFormat: "dd",
                beforeShow: function() {
                    setTimeout(function() {
                        $('.ui-datepicker').css('z-index', 99999999999999);
                        $('.ui-datepicker').css('padding', 0)
                        $('.ui-datepicker-year,.ui-datepicker-month').css({
                            'display': 'none'
                        })
                    }, 0)
                },
                beforeShowDay: function(date) {
                    if (date.getDate() == 29 || date.getDate() == 30 || date.getDate() == 31) {
                        return [!1, '', 'Unavailable']
                    }
                    return [!0, '']
                },
            });
});