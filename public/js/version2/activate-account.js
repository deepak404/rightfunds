         $(document).ready(function(){

            // var count = 0;
            // $('#personal-details-tab .input-field').each(function(){
            //     if ($(this).val() == '') {
            //         count++;
            //     }            
            // });

            // if (count == 0) {
            //     $('#personal-details-tab .input-field').each(function(){
            //         $(this).prop('disabled',true);
            //     });
            //     $('#submit-personal').prop('disabled',false);
            // }else{
            //     $('#edit-personal').hide();
            //     $('#next-btn').hide();
            //     $('#submit-personal').show();
            //     $('#submit-personal').prop('disabled',true);
            // }

            // $('#edit-personal').on('click',function(){
            //     $('#submit-personal').show();
            //     document.getElementById("name").focus();
            //     $('#personal-details-tab .input-field').each(function(){
            //         $(this).prop('disabled',false);
            //     });
            //     $('#edit-personal').hide();
            //     $('#next-btn').hide();
            // });

            $('#next-btn').on('click',function(){
                        // $('#settings-nav li:nth-child(2)>span>i').removeClass('non-active-tick').addClass('activate-tick');
                        $('#settings-nav li:first').removeClass('active-settings-nav').addClass('inactive-sidemenu');
                        $('#settings-nav li:nth-child(2)').removeClass('inactive-sidemenu').addClass('active-settings-nav');
                        $('.section-header').text('Bank Details');
                        $('#bank-details-info').show();
                        $('#bank-details-tab').show();
                        $('#nominee-details-tab,#change-password-tab,#personal-details-tab,#ekyc-tab').hide();
            });

            $('#next-kyc').on('click',function(){
                        $('#settings-nav li:nth-child(2)').removeClass('active-settings-nav').addClass('inactive-sidemenu');
                        $('#settings-nav li:nth-child(3)').removeClass('inactive-sidemenu').addClass('active-settings-nav');
                        $('.section-header').text('EKYC and Documents');
                        $('#bank-details-info').hide();
                        $('#ekyc-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#personal-details-tab').hide();
            });

            // var file_check_status = 0;
            //  $('input').on('keyup',function(){
            //     if ($('#name').val() != "" && $('#pan').val() != "" && $('#dob').val() != "" && $('#fathers-name').val() != "" && $('#mothers-name').val() != "" && $('#occupation').val() != "" && $('#address').val() != "" && $('#pincode').val() != "" && $('#nationality').val() != "" && $('#birth-city').val() != "" && $('#aadhar').val() != "") {
            //         $("#submit-personal").prop('disabled',false);
            //     }else{
            //         $("#submit-personal").prop('disabled',true);
            //     }
            //  });

            $('#settings-nav').on('click','li.activate-acc-menu',function(){
                var data = '';

                $('#settings-nav').find('li').removeClass('active-settings-nav');
                $('#settings-nav').find('li').addClass('inactive-sidemenu');
                $(this).removeClass('inactive-sidemenu').addClass('active-settings-nav');
                

                switch($(this).index()){

                    case 0 :
                        data = 'type='+'p';
                        $('.section-header').text('Personal Details');
                        $('#bank-details-info').hide();
                        $('#personal-details-tab').show();
                        

                        $.ajax({
                              type: 'POST',
                              url: 'get_active_user_details',
                              data: data,
                              success:function(data){
                                var response = data;
                                showPersonalDetails(response);
                                
                              },
                              error:function(){ 
                                  
                              }
                          });
                        $('#nominee-details-tab,#bank-details-tab,#change-password-tab,#ekyc-tab').hide();
                        break;

                    case 1 :
                        data = 'type='+'b';
                        $('.section-header').text('Bank Details');
                        $('#bank-details-info').show();
                        $('#bank-details-tab').show();
                        $('#nominee-details-tab,#change-password-tab,#personal-details-tab,#ekyc-tab').hide();
                        break;

                    case 2 :

                        $('.section-header').text('EKYC and Documents');
                        $('#bank-details-info').hide();
                        $('#ekyc-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#personal-details-tab').hide();
                        break;
                        

                    /*case 3 :
                        $('.section-header').text('Nominee Details');
                        $('.section-header').append('<button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button>');
                        $('#add-nominee-btn').show();
                        $('#bank-details-info').hide();
                        $('#nominee-details-tab').show();
                        $('#bank-details-tab,#change-password-tab,#personal-details-tab').hide();

                        break;*/
                }
            });

            $('#dob').datepicker({
                changeMonth: !0,
                changeYear: !0,
                yearRange: '1940:2010',
                dateFormat: 'dd-mm-yy'
            });


            function showPersonalDetails(data){
                if(data.msg == "success"){

                    var res = data.response;

                    if (res != null) {
                        $('#name').val(res.f_name);
                        $('#pan').val(res.pan);

                        var dob = res.dob.split('-');
                        $('#dob').val(dob[2]+'/'+dob[1]+'/'+dob[0]);
                        $('#fathers-name').val(res.f_s_name);
                        $('#mothers-name').val(res.mothers_name);
                        $('#address').val(res.address);
                        $('#pincode').val(res.pincode);
                        $('#nationality').val(res.nationality);
                        $('#birth-city').val(res.b_city);

                        $('#'+res.sex).prop('checked',true);
                        $('#'+res.resident).prop('checked',true);
                        $('#'+res.mar_status).prop('checked',true);
                    }
                    
                }
            }

            $('#personal-details-form').on('submit',function(e){
                var data = $(this).serialize();

                $.ajax({
                      type: 'POST',
                      url: 'personal_details',
                      data: data,
                      success:function(data){
                        var response = data;
                        if (response.msg == "error") {
                            handleErrors(response.errors)
                        }
                        if (response.msg == "success") {
                            // $('#submit-personal').hide();
                            // $('#edit-personal').show();
                            // $('#next-btn').show();
                            // $('#settings-nav li:first>span>i').removeClass('non-active-tick').addClass('activate-tick');
                            $('#settings-nav li:first').removeClass('active-settings-nav').addClass('inactive-sidemenu');
                            $('#settings-nav li:nth-child(2)').removeClass('inactive-sidemenu').addClass('active-settings-nav');

                            if (response.kycStatus == 0) {
                                // $('#settings-nav li:nth-child(3)>span>i').removeClass('non-active-tick').addClass('activate-tick');
                                $('#next-kyc').show();
                                $('#kyc-info-text').text('You are not a KYC Compliant. Start your EKYC');
                                $('#settings-nav li:nth-child(3)').removeClass('inactive-menu').addClass('activate-acc-menu');
                            }else{
                                $('#next-kyc').hide();
                                $('#settings-nav li:nth-child(3)>span>i').removeClass('non-active-tick').addClass('activate-tick');
                                $('#settings-nav li:nth-child(3)').removeClass('activate-acc-menu').addClass('inactive-menu');
                                $('#kyc-info-text').text('You are a KYC Compliant').css('color','color: #333333');
                            }
                        data = 'type='+'b';
                        $('.section-header').text('Bank Details');
                        $('#bank-details-info').show();
                        $('#bank-details-tab').show();
                        $('#nominee-details-tab,#change-password-tab,#personal-details-tab,#ekyc-tab').hide();
                            handleAlerts(response.msg,response.response)
                        }
                        if (response.msg == "failure") {
                            handleAlerts(response.msg,response.response)                    
                        }
                        
                      },
                      error:function(){ 
                          
                      }
                });
                e.preventDefault();
            });

            $('#cancelled-cheque').on('change',function(e){
                e.preventDefault();
                var formData = new FormData();
                var cancelled_cheque = $('#cancelled-cheque').get(0).files[0];
                formData.append('cancelled-cheque',cancelled_cheque);
                  $.ajax({
                          type: 'POST',
                          url: '/user_documents',
                          data: formData,
                          contentType: false,
                          processData: false,
                          success:function(data){
                            var response = data;
                            if (response.msg == "success") {
                                uploadStatus(data)
                            }

                          },
                          error:function(){ 
                              
                          }
                  });

            });

            $('#mandate').on('change',function(e){
                e.preventDefault();
                var formData = new FormData();
                var mandate = $('#mandate').get(0).files[0];
                formData.append('mandate',mandate);
                  $.ajax({
                          type: 'POST',
                          url: '/user_documents',
                          data: formData,
                          contentType: false,
                          processData: false,
                          success:function(data){
                            var response = data;
                            if (response.msg == "success") {
                                uploadStatus(data)
                            }

                          },
                          error:function(){ 
                              
                          }
                  });
            });

            function uploadStatus(date) {
                $('#settings').css('opactiy','1');
                if (date.state == '2') {
                    $('#mandate').next().find('a').addClass('gray-btn');
                    $('#mandate').next().find('a').text('Uploaded');
                    $('#mandate').next().find('label').removeAttr("for");
                    $('#settings').css('opactiy','1');
                }else {
                    $('#cancelled-cheque').next().find('a').addClass('gray-btn');
                    $('#cancelled-cheque').next().find('a').text('Uploaded');
                    $('#cancelled-cheque').next().find('label').removeAttr("for");
                    $('#settings').css('opactiy','1');
                }
            }

            $('#bank-details-form').on('submit',function(e){
                e.preventDefault();
                var dataString = '';
                var acc_name = $('#acc-name').val();
                var acc_no = $('#acc-no').val();
                var bank_name = $('#bank-name').val();
                var ifsc_code = $('#ifsc-code').val();
                var acc_type = $('#acc-type').val();

                // var checkCheque = checkFile($('#cancelled-cheque'));
                // var checkMandate = checkFile($('#mandate'));
                var checkCheque = 0;
                var checkMandate = 0;
                //console.log(checkCheque);
                //console.log(checkMandate);

                // if ($(this).find('.input-field').val() == '') {
                //     var input = $(this).find('.input-field');
                //     $.each(input,function(){
                //         var name = $(this).siblings('label').text();
                //         $(this).siblings('.error').text('The '+name+' field is required');
                //     });
                // }else{

                    if(checkCheque == 0 && checkMandate == 0){

                        var formData = new FormData();

                        // console.log($('#cancelled-cheque').val());
                        // if ($('#cancelled-cheque').val() === "undefined" ) {

                        // }else{
                        //     var cancelled_cheque = $('#cancelled-cheque').get(0).files[0];
                        //     var mandate = $('#mandate').get(0).files[0];    
                        // }
                        

                        formData.append('acc-name',acc_name);
                        formData.append('acc-no',acc_no);
                        formData.append('acc-type',acc_type);
                        formData.append('ifsc-code',ifsc_code);
                        formData.append('bank-name',bank_name);
                        // formData.append('cancelled-cheque',cancelled_cheque);
                        // formData.append('mandate',mandate);

                        //console.log(formData);

                        $.ajax({
                          type: 'POST',
                          url: '/activate_bank_form',
                          data: formData,
                          contentType: false,
                          processData: false,
                          success:function(data){
                            var response = data;
                            if (response.msg == "error") {
                                handleErrors(response.errors)
                            }
                            if (response.msg == "success") {
                                // $('#settings-nav li:nth-child(2)>span>i').removeClass('non-active-tick').addClass('activate-tick');
                                //console.log('inside success');
                                handleAlerts(response.msg,response.response)
                            }
                            if (response.msg == "failure") {
                                handleAlerts(response.msg,response.response)                    
                            }

                          },
                          error:function(){ 
                              
                          }
                      });

                    }else{

                        //handleAlerts('failure','Oops! Bank details cannot be saved right now. Please refresh and try again')

                    }
                // }           
            });

            function checkFile(checkFile){


                    if(checkFile.val() == ''){
                        checkFile.parent().parent().parent().find('span.text-danger').text('File Missing');
                        file_check_status++;
                    }else{
                        var file = checkFile.get(0).files[0];//console.log(file);
                        var fileType = file["type"];
                        var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
                        //check file type
                        if ($.inArray(fileType, ValidImageTypes) < 0) {
                            checkFile.parent().parent().parent().find('span.text-danger').text('Invalid File Format');
                            check_status++;
                        }else{
                            file_check_status = 0;
                        }

                        //check file size
                        var fileSize = (file.size / 1024) / 1024;//console.log(fileSize); //size in MB
                        //check file size is less than 5
                        if (fileSize > 3) {
                            checkFile.parent().parent().parent().find('span.text-danger').text('File Size should be less than 3MB');
                            file_check_status++;
                        }else{
                            file_check_status = 0;
                        }
                    }    

                    ////console.log(file_check_status);
                    return file_check_status;                                    
            }



                function handleErrors(errors){
                    //console.log(errors);
                    $.each(errors,function(key,value){

                        if (key == "gender" || key == "resident" || key == "marital-status") {
                            $('input[name='+key+']').parent().parent().siblings('span.error').text(value);
                        }else{
                            $('input[name='+key+']').parent().find('span.error').text(value);
                        }
                    });
                }

                $('.acc-activate-forms :input[type=text]').on('keydown',function(){
                    $(this).parent().find('span').text('');
                })
                $('.acc-activate-forms :input[type=radio]').on('change',function(){
                    $(this).parent().parent().siblings('span.error').text('');
                })

                function handleAlerts(status,message){
                    $('#modal-body').empty();
                    if (status == "success") {
                        $('#modal-header').text(message);
                        $('#modal-body').append(
                            '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                            '<button type = "button" class = "btn btn-primary center-block popup-btn" data-dismiss = "modal">Okay</button>'
                            );

                             $('#infoModal').modal('show');

                    }
                    if (status == "failure") {
                        $('#modal-header').text(message);
                        $('#modal-body').append(
                            '<img class = "center-block modal-img" src = "icons/failed-tick.png">'+
                            '<button type = "button" class = "btn btn-primary center-block popup-btn" data-dismiss = "modal">Okay</button>'
                            );
                        $('#infoModal').modal('show');

                    }
                }

                $('#cancelled-cheque,#mandate').on('change',function(){
                    $(this).next().next().css({'display':'none'});
                    ////console.log('changed');
                });

                $('#pincode').on('keydown',function(e){
                    if ($(this).val().length == 6) {

                        if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true) {
                            return true;
                        }
                        return false;
                    }
                });

                $('#pan').on('keydown',function(e){
                    if ($(this).val().length == 10 ) {

                        if (e.keyCode == 8) {
                            return true;
                        }
                        return false;
                    }
                });

                $('.acc-select').on('click',function(){
                    var val;
                    $('.occupation-menu').html($(this).text()+'<span><i id="acc-type-icon" class="material-icons">keyboard_arrow_down</i></span>');
                    if ($(this).text() == "Savings") {
                        val = 1;
                    }

                    if ($(this).text() == "Current") {
                        val = 0;
                    }

                    $('#acc-type').val(val);
                    //alert($('#acc-type').val());
                });

                // $('#mandate,#cancelled-cheque').on('change',function(e){
                //         $(this).parent().parent().parent().find('span.text-danger').text('');
                //         $(this).parent().next().find('.upload-filename').text(e.target.files[0].name).show();
                // });

                $('#acc-no').on('keydown',function(e){
                        if ($(this).val().length == 18) {

                            if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                                return true;
                            }
                            return false;
                        }
                });

                $('#ifsc-code').on('keydown',function(e){

                        // $(this).val($(this).val().toUpperCase());


                        if (e.shiftKey) {
                            e.preventDefault();
                        }
                        //console.log($(this).val().length);
                    
                        if ($(this).val().length == 11) {
                            //console.log(e.which);
                            if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                                return true;
                            }else{
                                return false;
                            }                            
                        }

                        if ((e.keyCode > 47 && e.keyCode <58) || (e.keyCode > 95 && e.keyCode <106) || (e.keyCode > 64 && e.keyCode <91) || e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                                //console.log('here');
                                return true;
                        }else{
                            return false;
                        }
                });


                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });


                $('.bank-select').on('click',function(){
                    var val;
                    $('#bank-name-btn span:first-child').html($(this).text());
                    
                    val = $(this).data('bankid');
                    $('#bank-name').val(val);
                    // alert($('#bank-name').val());
                });

                
         });