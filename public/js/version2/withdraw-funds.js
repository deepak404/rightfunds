            $(document).ready(function(){
                $count = 0;
                var wd_amount;
                var current_value;
                $('input[type="checkbox"]').on('change',function(){
                    if ($(this).prop('checked')) {
                        $(this).parent().find('i').text('check_box').addClass('green');
                        //$count++;
                        ////console.log("Checked".$count);
                    }else{
                        $(this).parent().find('i').text('check_box_outline_blank').removeClass('green');
                    }
                });


                $(document).on('change','.full-amount',function(){

                    if ($(this).prop('checked')) {
                        var scheme_code = $(this).data('scheme');
                        var current_inv = $(this).closest('tr').find('span#'+scheme_code+'').text();
                        $(this).closest('tr').find('.custom-amount').val(parseFloat(current_inv)).prop('disabled',true);
                        amountAddition()
                        makeEmpty();
                    }else{
                        $(this).closest('tr').find('.custom-amount').val('').prop('disabled',false);
                        amountAddition()
                        makeEmpty();
                    }
                });


                $(document).on('keyup', '.custom-amount', function() {
                    amountAddition()
                    makeEmpty();
                
                    
                });

                function amountAddition(){
                    var total = 0;
                    $('.custom-amount').each(function() {
                        var enter_amt = $(this).val();
                        if (enter_amt.length !== 0) {
                            total += parseFloat(enter_amt);
                            //console.log(total);
                            if (total == '') {
                                //console.log("empty");
                            }
                            $('.withdraw-info-amount > span').text(total.toFixed(2));
                        }
                    });
                }

                function makeEmpty(){
                    ////console.log();
                    var total_input = $('.custom-amount').length;
                    var empty_amount = 0;
                    $('.custom-amount').each(function() {
                        var enter_amt = $(this).val();
                        if (enter_amt.length == 0) {
                            
                            empty_amount++;
                        }
                        if (total_input == empty_amount) {
                            $('.withdraw-info-amount > span').text('0');
                        }
                    });
                }


                $('#withdrawal-form').on('submit',function(e){

                    $('#withdraw-btn').prop('disabled',true);
                    var data = 'withdraw_funds=';
                    e.preventDefault();
                    var total = 0;
                    var full_or_not = '';
                    $('.custom-amount').each(function() {
                        var wd_amount = $(this).val();
                        if (wd_amount != '') {
                            
                            var scheme_code = $(this).attr('id');
                            var current_value = $(this).closest('tr').find('span#'+scheme_code+'').text();

                            //console.log(parseFloat(wd_amount),parseFloat(current_value));
                            if (parseFloat(wd_amount) > parseFloat(current_value)) {
                                handleErrors(error_type = "cv");
                            }
                            if(wd_amount < 999){
                                handleErrors(error_type = "1000");
                            }else{

                                if (parseFloat(current_value) == parseFloat(wd_amount)) {
                                    full_or_not = "f";
                                }else{
                                    full_or_not = "nf";
                                }
                                total += parseFloat(wd_amount);
                                data += scheme_code +'/'+wd_amount+'/'+full_or_not+'|';
                                
                            }

                        }                        
                    });

                    if (total > 999) {

                        if (parseFloat(wd_amount) > parseFloat(current_value)) {
                                handleErrors(error_type = "cv");
                        }else{
                            //var response = ajaxCalls('POST','withdraw_user_funds',data);
                            ////console.log(response);
                            
                            ////console.log(data);


                            $.ajax({
                                type: 'POST',
                                url: 'withdraw_user_funds',
                                data: data,
                                //async : false,
                                success:function(data){
                                  var response = data;
                                  handleResponse(response);
                                  //console.log(data);
                                },
                                error:function(){ 
                                    
                                }
                            });
                        }
                        
                    }else{
                        handleErrors(error_type = "ngt1000");

                    }

                    
                    $('#withdraw-btn').prop('disabled',false);

                });

                    $('.custom-amount').on('keydown',function(e){
                            if (e.shiftKey) {
                                e.preventDefault();
                            }

                            var scheme_code = $(this).attr('id');

                            ////console.log(parseFloat($(this).closest('tr').find('span#'+scheme_code+'').text()));
                            ////console.log($(this).val());
                            if ($(this).val() > parseFloat($(this).closest('tr').find('span#'+scheme_code+'').text())) {
                                if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                                return true;
                            }else{
                                return false;
                            }                            
                            }
                    });


                function handleErrors(data){
                    $('#modal-body').empty();
                    $('#modal-header').text('');
                    //console.log(data);
                    if (data == "1000") {
                        $('#modal-body').append(
                            '<p class="text-center general-info">Withdrawal Amount should be greater than 1000<p>'
                            );
                    }
                    if (data == "cv") {
                        $('#modal-body').append(
                            '<p class="text-center general-info">Withdrawal Amount cannot be greater than the Current Value<p>'
                            );
                    }
                    if (data == "ngt1000") {
                        $('#modal-body').append(
                            '<p class="text-center general-info">Minimal Withdrawal Amount is 100000<p>'
                            );
                    }

                    $('#infoModal').modal('show');
                }

                function handleResponse(data){
                    //console.log("inside Handle Response");
                    $('#modal-body').empty();
                    //console.log(data);
                    if (data.msg == "success") {
                        $('#modal-header').text('');
                         $('#modal-body').append(
                            '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                            '<p class="text-center general-info">'+data.status+' of Rs. <span class = "blue-text">'+data.amount+'</span> on <span class="blue-text">'+data.date+'</span><p>'+
                            '<button type = "button" class = "btn btn-primary center-block popup-btn" onclick="redirect()">Okay</button>');
                         $('#infoModal').modal('show');
                    }
                    if (data.msg == "failure") {
                        $('#modal-header').text('');
                         $('#modal-body').append(
                            '<img class = "center-block modal-img" src = "icons/failed-tick.png">'+
                            '<p class="text-center general-info">'+data.status+'<p>'+
                            '<button type = "button" class = "btn btn-primary center-block popup-btn" onclick="redirect()">Okay</button>');

                         $('#infoModal').modal('show');

                    }

                }

                // $('#infoModal').modal('show');

                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });

            });