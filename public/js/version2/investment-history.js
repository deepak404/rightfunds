$(document).ready(function(){

	var modal_body;


		$('.inv-info').on('click',function(){
			var id = $(this).attr('id');
			var method = "POST";
			var route = "/get_investment_details";
			var dataString = 'investment_id='+id;

			var body = '<table class="table" style = "margin-top: -10px;">'+
                    '<thead>'+
                      '<tr>'+
                        '<th>Scheme Name</th>'+
                        '<th>Date</th>'+
                        '<th>Amount</th>'+
                        '<th>Status</th>'+
                        '<th>Purchase NAV</th>'+
                      '</tr>'+
                    '</thead>'+
                    '<tbody id="table-body">'+
                      
                    '</tbody>'+
                  '</table>';

			var data = ajaxCalls(method,route,dataString);
			console.log(data);
			if (data.msg == 'success') {
				console.log(body)
				$('#modal-body').empty().append(body);
				$('#invDetailModal').modal('show');
				$('#modal-header').text('Investment Details')
				$.each(data.response,function(key,value){
					$amt = inrformat(value.amount_invested)

					$(document).find('#table-body').append(
						  '<tr>'+
					        '<td>'+value.scheme_name+'</td>'+
					        '<td>'+value.investment_date+'</td>'+
					        '<td>'+$amt+'.00</td>'+
					        '<td>'+value.status+'</td>'+
					        '<td>'+value.purchase_nav+'</td>'+
					      '</tr>'

						);
				});
			}
		});




		function ajaxCalls(method,route,dataString){
			  var response;
			  $.ajaxSetup({
			    headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			  });

			  $.ajax({
			      type: method,
			      url: route,
			      data: dataString,
			      async : false,
			      success:function(data){
			        response = data;
			        //console.log(data);
			      },
			      error:function(){ 
			          
			      }
			  });

			  ////console.log(response);
			  return response;
		}

	    function inrformat(data) {
              var x=data;
              x=x.toString();
              var afterPoint = '';
              if(x.indexOf('.') > 0)
                 afterPoint = x.substring(x.indexOf('.'),x.length);
              x = Math.floor(x);
              x=x.toString();
              var lastThree = x.substring(x.length-3);
              var otherNumbers = x.substring(0,x.length-3);
              if(otherNumbers != '')
                  lastThree = ',' + lastThree;
              var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
              return res;
	    }

		$(document).on('click','.cancel-inv',function(){
			var inv_id = $(this).attr('id');
			var dataString = 'investment_id='+inv_id;
			var method = "POST";
			var route = "/cancel_investment";

			var info_text,img;


			var data = ajaxCalls(method,route,dataString);

			if (data.msg == "error") {
				info_text = data.response;
				img = 'icons/failed-tick.png';
				appendModalBody(info_text,img)
				$('#modal-header').text('Cancel Investment');				
				$('#modal-body').empty().append(modal_body);
				$('.modal-dialog').css({'width':'300px'});
				$('#invDetailModal').modal('show');
			}
			if (data.msg == "success") {
				info_text = data.response;
				img = 'icons/success-tick.png';
				appendModalBody(info_text,img)
				$('#modal-header').text('Cancel Investment');
				$('#modal-body').empty().append(modal_body);
				$('.modal-dialog').css({'width':'300px'});
				$('#invDetailModal').modal('show');
			}

		})


		function appendModalBody(info_text,img){
			modal_body =  '<div class="row">'+
	                        '<div class = "col-lg-12 col-md-12 col-sm-12">'+
	                            '<p class="modal-info-text text-center">'+info_text+'</p>'+
	                            '<img src="'+img+'" class="modal-img center-block">'+
	                            '<a class="btn btn-primary" onclick="location.reload()" data-dismiss = "modal">OKAY</a>'+
	                        '</div> '+     
	                    '</div>'
		}


		$(document).on('change','#investment-duration',function(){

			window.location.href = '/investment_history/'+$(this).val();
			// var dataString = '';
			// var route = '/investment_history/'+$(this).val();
			// var method = "GET";

			// ajaxCalls(method,route,dataString)
		});



});