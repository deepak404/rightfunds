$(document).ready(function(){
    $(document).on('keyup', '.custom-amount', function() {
        amountAddition()
        makeEmpty();
    
        
    });

    function amountAddition(){
        var total = 0;
        $('.custom-amount').each(function() {
            var enter_amt = $(this).val();
            if (enter_amt.length !== 0) {
                total += parseFloat(enter_amt);
                ////console.log(total);
                if (total == '') {
                    //console.log("empty");
                }
                $('#total-inv-amount > span').text(total);
            }
        });
    }

    function makeEmpty(){
        ////console.log();
        var total_input = $('.custom-amount').length;
        var empty_amount = 0;
        $('.custom-amount').each(function() {
            var enter_amt = $(this).val();
            if (enter_amt.length == 0) {
                
                empty_amount++;
            }
            if (total_input == empty_amount) {
                $('#total-inv-amount > span').text('0');
            }
        });
    } 
});