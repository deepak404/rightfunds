$(document).ready(function(){

	var existing_goal = $('#current-goal-amount').text();
	var current_value = $('#current-value').text();

	//console.log("The current value is" +parseInt(current_value));
	$('#goal-chart').donut();


	$('#edit-goal').on('click',function(){
		$(this).prev().text('');
		$(this).prev().append('<input type ="text" id="goal-amount" class = "input-field">'+
			'<a class="text-center center-block blue-text" id="save-goal">Save Goal</a>');
		//$('#goal-amount').next().append('')
		$(this).hide();
	});

	// $(document).on('blur','#goal-amount',function(){
	// 	setPrevGoal();
	// });

	function setPrevGoal(){
		$('#edit-goal').show();
		$('#edit-goal').prev().empty();
		$('#edit-goal').prev().append('Rs.<span id= "current-goal-amount">' +existing_goal);
		$('#edit-goal').prev().show();
		$('#goal-amount').hide();

		if ((current_value > existing_goal) || (current_value == existing_goal)) {
			$('#goal-away-text').text('Goal Reach. Set new Goal !');
		}

		var remaining_goal = parseInt(existing_goal) - parseInt(current_value);
		$('#goal-reached-val').attr('data-value',current_value);
		$('#remaining-goal-val').attr('data-value',remaining_goal);
		$('#goal-chart').donut();


		
	}

	$(document).on('click','#save-goal',function(e){
		if ($('#goal-amount').val() != 0) {
			var dataString = 'goal_amount='+$('#goal-amount').val();
			var response;


			$.ajax({
                type: 'POST',
                url: '/save_goal',
                data: dataString,
                processData: false,
                success:function(data){
                  response = data;
                  if (response.msg == "true") {
						existing_goal = response.goal_amount;
						setPrevGoal();
						$('#goal-away-text').text('Goal Updated !');
					}
					if (response.msg == "false") {
						//existing_goal = response.goal_amount;
						setPrevGoal();
					}
					if (response.msg == "nope") {
						$('#general-info').text('');
						$('#general-info').text('Kindly fill your personal details to set a personalised goal');
						$('#generalInfoModal').modal('show');
					}
                },
                error:function(){ 
                    
                }
            });

			
		}else{
			setPrevGoal();
		}
	});


	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    

});