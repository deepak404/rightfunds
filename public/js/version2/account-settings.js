$(document).ready(function(){

            $('#settings-nav').on('click','li',function(){

                $('#settings-nav').find('li').removeClass('active-settings-nav');
                $('#settings-nav').find('li').addClass('inactive-sidemenu');
                $(this).removeClass('inactive-sidemenu').addClass('active-settings-nav');
                

                switch($(this).index()){

                    case 0 :
                        $('.section-header').text('Personal Details');
                        $('#bank-details-info').hide();
                        $('#personal-details-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#change-password-tab').hide();
                        break;

                    case 1 :
                        $('.section-header').text('Change Password');
                        $('#bank-details-info').hide();
                        $('#change-password-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#personal-details-tab').hide();
                        break;

                    case 2 :
                        $('.section-header').text('Bank Details');
                        $('#bank-details-info').show();
                        $('#bank-details-tab').show();
                        $('#nominee-details-tab,#change-password-tab,#personal-details-tab').hide();
                        break;

                    case 3 :
                        $('.section-header').text('Nominee Details');
                        if (nom_count > 2) {

                        }else{
                        	$('.section-header').append('<button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button>');
                        }
                        
                        $('#add-nominee-btn').show();
                        $('#bank-details-info').hide();
                        $('#nominee-details-tab').show();
                        $('#bank-details-tab,#change-password-tab,#personal-details-tab').hide();

                        break;
                }
            });

            $('#nom1-dob,#nom2-dob').datepicker({
            	changeMonth:true,
            	changeYear:true,
            	yearRange: '1940:2017',
            });

            $('#personal-details-form').on('submit',function(e){
				e.preventDefault();
				data = $(this).serialize();
			

				$.ajax({
				      type: 'POST',
				      url: '/save_user_details',
				      data: data,
				      success:function(data){
				      	if (data.msg == "success") {
							handleAlerts(data.msg,data.response)
						}
						if (data.msg == "error") {
							//console.log("inside errors");
							handleErrors(data.errors)
						}
				      },
				      error:function(){ 
				          
				      }
				  });
				
			});

			$('#change-password-form').on('submit',function(e){
				e.preventDefault();

				if ($('#new-password').val() != $('#confirm-password').val()) {
					$('#no-match').text('Password Doesn\'t match').show();
				}else{

					data = $(this).serialize();
					$.ajax({
				      type: 'POST',
				      url: '/change_password',
				      data: data,
				      success:function(data){
				      	var response = data;
				      	//console.log(response.msg);
						if (response.msg == "success") {
							//console.log('inside success');
							handleAlerts(response.msg,response.response)
							$(this).trigger('reset');
						}
						if (response.msg == "error") {
							//console.log("inside errors");
							handleErrors(response.errors)
						}
						if (response.msg == "failure") {
							handleAlerts(response.msg,response.response)
							$(this).trigger('reset');

						}
				      },
				      error:function(){ 
				          
				      }
				  });
				}
			});

			$('#new-password,#confirm-password').on('keydown',function(){
				$('#no-match').text('').hide();
			});



			function handleErrors(errors){
				$.each(errors,function(key,value){
					$('input[name='+key+']').parent().find('span').text(value);
				});
			}

			$('.settings-form :input').on('keydown',function(){
				$(this).parent().find('span').text('');
			})


			function handleAlerts(status,message){
				$('#modal-body').empty();
				if (status == "success") {
					$('#modal-header').text(message);
					$('#modal-body').append(
                        '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                        '<button type = "button" class = "btn btn-primary center-block popup-btn" data-dismiss = "modal">Okay</button>');

                         $('#infoModal').modal('show');

				}
				if (status == "failure") {
					$('#modal-header').text(message);
					$('#modal-body').append(
                        '<img class = "center-block modal-img" src = "icons/failed-tick.png">'+
                        '<button type = "button" class = "btn btn-primary center-block popup-btn" data-dismiss = "modal">Okay</button>');
					$('#infoModal').modal('show');

				}
			}

			// $('#add-nominee-btn').on('click',function(){
			// 	//console.log('nominee cliccked');
			// });


			$(document).on('click','#add-nominee-btn',function(){
				$('#nominee-details-tab').append(
					'<form class="nominee-details-form" data-id="2">'+
	                    '<div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">'+
	                        '<div class="col-lg-4 col-md-4 col-sm-4">'+
	                            '<div class="form-group">'+
	                                '<input type="text" name = "nom2-name" id="nom2-name" class="input-field" required>'+
	                                '<label>Nominee Name</label>'+
	                                '<span></span>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-4 col-md-4 col-sm-4">'+
	                            '<div class="form-group">'+
	                                '<input type="text" name = "nom2-relationship" id="nom2-relationship" class="input-field" required>'+
	                                '<label>Relationship</label>'+
	                                '<span></span>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-4 col-md-4 col-sm-4">'+
	                            '<div class="form-group">'+
	                                '<input type="text" name = "nom2-dob" id="nom2-dob" class="input-field" required>'+
	                                '<label>Date of Birth</label>'+
	                                '<span></span>'+
	                            '</div>'+
	                        '</div>'+
	                    '</div>'+
	                    '<div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">'+
	                        '<div class="col-lg-4 col-md-4 col-sm-4">'+
	                            '<div class="form-group">'+
	                                '<input type="text" name = "nom2-address" id="nom2-address" class="input-field" required>'+
	                                '<label>Communication Address</label>'+
	                                '<span></span>'+
	                            '</div>'+
	                        '</div>'+
	                        // '<div class="col-lg-4 col-md-4 col-sm-4">'+
	                        //     '<div class="form-group">'+
	                        //         '<input type="text" name = "nom1-pin" id="nom1-pin" class="input-field" required>'+
	                        //         '<label>Pincode</label>'+
	                        //     '</div>'+
	                        // '</div>'+
	                        '<div class="col-lg-4 col-md-4 col-sm-4">'+
	                            '<div class="form-group">'+
	                                '<input type="text" name = "nom2-perc" id="nom2-perc" class="input-field" required>'+
	                                '<label>Percentage Holding</label>'+
	                                '<span></span>'+
	                            '</div>'+
	                        '</div>'+
	                    '</div>'+
	                    '<input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">'+
	                '</form>');
			});


			
			$(document).on('submit','.nominee-details-form',function(e){
				var route = '';
				var form;
				//console.log($(this).data('id'));
				var nominee_type = $(this).data('id');

				if (nominee_type == "1") {
					form = $('.nominee-details-form[data-id=1]');	
					route = '/save_nominee1_details'
				}
				if (nominee_type == "2") {
					form = $('.nominee-details-form[data-id=2]');
					route = '/save_nominee2_details'
				}

				var data = $(this).serialize();

				$.ajax({
				      type: 'POST',
				      url: route,
				      data: data,
				      success:function(data){
				      	var response = data;
				      	//console.log(response.msg);
						if (response.msg == "success") {
							//console.log('inside success');
							handleAlerts(response.msg,response.response)
						}

						if (response.msg == "failure") {
							//console.log('inside failure');
							$.each(response.nomi_details,function(key,value){
								if (nominee_type == "1") {
									form.find('#nom1-name').val(value.nomi_name);
									form.find('#nom1-relationship').val(value.nomi_relationship);
									form.find('#nom1-dob').val(value.nomi_dob);
									form.find('#nom1-address').val(value.nomi_addr);
									form.find('#nom1-perc').val(value.nomi_holding);
								}

								if (nominee_type == "2") {
									form.find('#nom2-name').val(value.nomi_name);
									form.find('#nom2-relationship').val(value.nomi_relationship);
									form.find('#nom2-dob').val(value.nomi_dob);
									form.find('#nom2-address').val(value.nomi_addr);
									form.find('#nom2-perc').val(value.nomi_holding);
								}
							});
							handleAlerts(response.msg,response.response)
						}

						if (response.msg == "error") {
							//console.log("inside errors");
							handleErrors(response.errors)
						}
				      },
				      error:function(){ 
				          
				      }
				  });

				e.preventDefault();
			});

			    $('input[type="checkbox"]').on('change',function(){
                    if ($(this).prop('checked')) {
                        $(this).parent().find('i').text('check_box').addClass('green');
                        //$count++;
                        ////console.log("Checked".$count);
                    }else{
                        $(this).parent().find('i').text('check_box_outline_blank').removeClass('green');
                    }
                });


                $.ajaxSetup({
				    headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    }
				});

				$('#nom1-perc,#nom2-perc').on('keydown',function(e){
					if ($(this).val().length > 2) {
						if (e.keyCode == 8 || e.keyCode == 9 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
							return true;
						}else{
							e.preventDefault();
						}
					}
				});
         });