/*$(document).ready(function(){$('#pending_orders').on('submit',function(e){e.preventDefault(e);var formdata=$(this).serialize();$.ajax({type:"POST",url:"/admin/save_pending_orders",headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},data:formdata,cache:!1,processData:!1,success:function(data){if(data.msg=='success'){$('#notify-done').attr('onclick',"javascript:location.href='/admin/pending_orders'");$('#great').html('Great !');$('#you-have').html(data.status+' of <span class="blue">Rs. '+data.amount+'</span> on <span class="blue">'+data.date+'</span>');$('#scheduleInvModal').modal('show')}else{$('#notify-done').attr('onclick',"javascript:$('#scheduleInvModal').modal('hide')");$('#great').html('Oops !');$('#you-have').html(data.status);$('#scheduleInvModal').modal('show')}},error:function(xhr,status,error)
{$('#notify-done').attr('onclick',"javascript:$('#scheduleInvModal').modal('hide')");$('#great').html('Oops !');$('#you-have').html('Something went wrong. Please try again');$('#scheduleInvModal').modal('show')},})});$('.remove-order').on('click',function(){var order_type=$('#transaction_type').val();var portfolio_id=$(this).attr('data-id');var formdata='transaction_type='+order_type+'&id='+portfolio_id;$.ajax({type:"POST",url:"/admin/cancel_pending_order",headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},data:formdata,cache:!1,processData:!1,success:function(data){if(data.msg=='success'){$('#notify-done').attr('onclick',"javascript:$('#scheduleInvModal').modal('hide')");$('#great').html('Great !');$('#you-have').html(data.status+' of <span class="blue">Rs. '+data.amount+'</span> on <span class="blue">'+data.date+'</span>');$(this).closest("tr").remove();$('#scheduleInvModal').modal('show')}else{$('#notify-done').attr('onclick',"javascript:$('#scheduleInvModal').modal('hide')");$('#great').html('Oops !');$('#you-have').html(data.status);$('#scheduleInvModal').modal('show')}},error:function(xhr,status,error)
{$('#notify-done').attr('onclick',"javascript:$('#scheduleInvModal').modal('hide')");$('#great').html('Oops !');$('#you-have').html('Something went wrong. Please try again');$('#scheduleInvModal').modal('show')},})});$(document).on('click',".suggested-user",function(){var user_id=$(this).data('id');$('#user_id').val(user_id);get_user_details()})})*/



$(document).ready(function() {
    $('#pending_orders').on('submit', function(e) {
        e.preventDefault(e);
        var formdata = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/admin/save_pending_orders",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formdata,
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg == 'success') {
                    $('#notify-done').attr('onclick', "javascript:location.href='/admin/pending_orders'");
                    $('#great').html('Great !');
                    $('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.amount + '</span> on <span class="blue">' + data.date + '</span>');
                    $('#scheduleInvModal').modal('show')
                } else {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Oops !');
                    $('#you-have').html(data.status);
                    $('#scheduleInvModal').modal('show')
                }
            },
            error: function(xhr, status, error) {
                $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                $('#great').html('Oops !');
                $('#you-have').html('Something went wrong. Please try again');
                $('#scheduleInvModal').modal('show')
            },
        })
    });
    $('.remove-order').on('click', function() {
        var order_type = $('#transaction_type').val();
        var portfolio_id = $(this).attr('data-id');
        var formdata = 'transaction_type=' + order_type + '&id=' + portfolio_id;
        $.ajax({
            type: "POST",
            url: "/admin/cancel_pending_order",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formdata,
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg == 'success') {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Great !');
                    $('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.amount + '</span> on <span class="blue">' + data.date + '</span>');
                    $(this).closest("tr").remove();
                    $('#scheduleInvModal').modal('show')
                } else {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Oops !');
                    $('#you-have').html(data.status);
                    $('#scheduleInvModal').modal('show')
                }
            },
            error: function(xhr, status, error) {
                $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                $('#great').html('Oops !');
                $('#you-have').html('Something went wrong. Please try again');
                $('#scheduleInvModal').modal('show')
            },
        })
    });
    $(document).on('click', ".suggested-user", function() {
        var user_id = $(this).data('id');
        $('#user_id').val(user_id);
        get_user_details()
    })
})