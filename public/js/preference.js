$(document).ready(function() {
    $('#per-edit-btn').on('click', function() {
        $(this).css({
            'display': 'none'
        });
        $('#per-save-btn,#per-cancel-btn').css({
            'display': 'block'
        });
        var input = $('#pers-det-form').find('input');
        input.prop('disabled', !1);
        $('#pers-det-form').find('select').prop('disabled', !1);
        $('textarea').prop('disabled', !1);
        $('#name').focus()
    });
    $('#per-cancel-btn').on('click', function() {
        $('#per-cancel-btn,#per-save-btn').css({
            'display': 'none'
        });
        $('#per-edit-btn').css({
            'display': 'block'
        });
        var input = $('#pers-det-form').find('input');
        $('textarea').prop('disabled', !0);
        input.prop('disabled', !0);
        $('#pers-det-form')[0].reset()
    });
    $('#bank-edit-btn').on('click', function() {
        $(this).css({
            'display': 'none'
        });
        $('#bank-save-btn,#bank-cancel-btn').css({
            'display': 'block'
        });
        var input = $('#bank-det-cont').find('input');
        input.prop('disabled', !1);
        $('#bank-det-cont').find('select').prop('disabled', !1);
        $('#bank_name').focus()
    });
    $('#bank-cancel-btn').on('click', function() {
        $('#bank-cancel-btn,#bank-save-btn').css({
            'display': 'none'
        });
        $('#bank-edit-btn').css({
            'display': 'block'
        });
        var input = $('#bank-det-cont').find('input');
        input.prop('disabled', !0);
        $('#bank-det-form')[0].reset()
    });
    $('#password-edit-btn').on('click', function() {
        $(this).css({
            'display': 'none'
        });
        $('#password-save-btn,#password-cancel-btn').css({
            'display': 'block'
        });
        var input = $('#password-det-cont').find('input');
        input.prop('disabled', !1);
        $('#bank-det-cont').find('select').prop('disabled', !1);
        $('#bank_name').focus()
    });
    $('#password-cancel-btn').on('click', function() {
        $('#password-cancel-btn,#password-save-btn').css({
            'display': 'none'
        });
        $('#password-edit-btn').css({
            'display': 'block'
        });
        var input = $('#password-det-cont').find('input');
        input.prop('disabled', !0)
    });
    $('#nominee1-edit-btn').on('click', function() {
        $(this).css({
            'display': 'none'
        });
        $('#nominee1-save-btn,#nominee1-cancel-btn').css({
            'display': 'block'
        });
        var input = $('#nominee1-det-cont').find('input');
        input.prop('disabled', !1);
        $('#nominee1-det-cont').find('checkbox').prop('disabled', !1);
        $('#nominee1_address').prop('disabled', !1);
        $('#nominee1_name').focus()
    });
    $('#nominee1-cancel-btn').on('click', function() {
        $('#nominee1-cancel-btn,#nominee1-save-btn').css({
            'display': 'none'
        });
        $('#nominee1-edit-btn').css({
            'display': 'block'
        });
        var input = $('#nominee1-det-cont').find('input');
        input.prop('disabled', !0);
        $('#nominee1_address').prop('disabled', !0);
        $('#nominee1-det-form')[0].reset()
    });
    $('#nominee2-edit-btn').on('click', function() {
        $(this).css({
            'display': 'none'
        });
        $('#nominee2-save-btn,#nominee2-cancel-btn').css({
            'display': 'block'
        });
        var input = $('#nominee2-det-cont').find('input');
        input.prop('disabled', !1);
        $('#nominee2-det-cont').find('checkbox').prop('disabled', !1);
        $('#nominee2_address').prop('disabled', !1);
        $('#nominee2_name').focus()
    });
    $('#nominee2-cancel-btn').on('click', function() {
        $('#nominee2-cancel-btn,#nominee2-save-btn').css({
            'display': 'none'
        });
        $('#nominee2-edit-btn').css({
            'display': 'block'
        });
        var input = $('#nominee2-det-cont').find('input');
        input.prop('disabled', !0);
        $('#nominee2_address').prop('disabled', !0);
        $('#nominee2-det-form')[0].reset()
    });
    $('#nominee3-edit-btn').on('click', function() {
        $(this).css({
            'display': 'none'
        });
        $('#nominee3-save-btn,#nominee3-cancel-btn').css({
            'display': 'block'
        });
        var input = $('#nominee3-det-cont').find('input');
        input.prop('disabled', !1);
        $('#nominee3-det-cont').find('checkbox').prop('disabled', !1);
        $('#nominee3_address').prop('disabled', !1);
        $('#nominee3_name').focus();
        $('#nominee3-det-form')[0].reset()
    });
    $('#nominee3-cancel-btn').on('click', function() {
        $('#nominee3-cancel-btn,#nominee3-save-btn').css({
            'display': 'none'
        });
        $('#nominee3-edit-btn').css({
            'display': 'block'
        });
        var input = $('#nominee3-det-cont').find('input');
        input.prop('disabled', !0);
        $('#nominee3_address').prop('disabled', !0);
        $('#nominee3-det-form')[0].reset()
    });
    $('#pers-det-form').on('submit', function(e) {
        e.preventDefault(e);
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/save_user_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            processData: !1,
            success: function(data) {
                $('#email-error').html('');
                $('#mobile-error').html('');
                $('#name-error').html('');
                $('#addr-error').html('');
                $('#pincode-error').html('');
                if (data.msg == 'success') {
                    $('#per-edit-btn').css({
                        'display': 'block'
                    });
                    $('#per-save-btn, #per-cancel-btn').css({
                        'display': 'none'
                    });
                    var input = $('#pers-det-form').find('input');
                    input.prop('disabled', !0);
                    $('#pers-det-form').find('select').prop('disabled', !0);
                    $('textarea').prop('disabled', !0);
                    $('#great').html('Great !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'fail') {
                    $('#great').html('Error !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'error') {
                    var errors = data.errors;
                    if (errors.hasOwnProperty('email')) {
                        $('#email-error').html('<strong>' + errors.email + '</strong>')
                    }
                    if (errors.hasOwnProperty('mobile')) {
                        $('#mobile-error').html('<strong>' + errors.mobile + '</strong>')
                    }
                    if (errors.hasOwnProperty('name')) {
                        $('#name-error').html('<strong>' + errors.name + '</strong>')
                    }
                    if (errors.hasOwnProperty('address')) {
                        $('#addr-error').html('<strong>' + errors.address + '</strong>')
                    }
                    if (errors.hasOwnProperty('pincode')) {
                        $('#pincode-error').html('<strong>' + errors.pincode + '</strong>')
                    }
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#bank-det-form').on('submit', function(e) {
        e.preventDefault(e);
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/save_bank_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            processData: !1,
            success: function(data) {
                $('#bank_name_error').html('');
                $('#bank_number_error').html('');
                $('#bank_ifsc_error').html('');
                $('#bank_type_error').html('');
                if (data.msg == 'success') {
                    $('#bank-save-btn, #bank-cancel-btn').css({
                        'display': 'none'
                    });
                    $('#bank-edit-btn').css({
                        'display': 'block'
                    });
                    var input = $('#bank-det-cont').find('input');
                    input.prop('disabled', !0);
                    $('#bank-det-cont').find('select').prop('disabled', !0);
                    $('#great').html('Great !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'fail') {
                    $('#great').html('Error !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'error') {
                    var errors = data.errors;
                    if (errors.hasOwnProperty('bank_name')) {
                        $('#bank_name_error').html('<strong>' + errors.bank_name + '</strong>')
                    }
                    if (errors.hasOwnProperty('bank_number')) {
                        $('#bank_number_error').html('<strong>' + errors.bank_number + '</strong>')
                    }
                    if (errors.hasOwnProperty('bank_ifsc')) {
                        $('#bank_ifsc_error').html('<strong>' + errors.bank_ifsc + '</strong>')
                    }
                    if (errors.hasOwnProperty('bank_account_type')) {
                        $('#bank_type_error').html('<strong>' + errors.bank_account_type + '</strong>')
                    }
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#password-det-form').on('submit', function(e) {
        e.preventDefault(e);
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/change_password",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            processData: !1,
            success: function(data) {
                $('#current_password_error').html('');
                $('#new_password_error').html('');
                $('#confirm_password_error').html('');
                if (data.msg == "1") {
                    $('#password-save-btn, #password-cancel-btn').css({
                        'display': 'none'
                    });
                    $('#password-edit-btn').css({
                        'display': 'block'
                    });
                    var input = $('#password-det-cont').find('input');
                    input.prop('disabled', !0);
                    input.val('');
                    $('#great').text('Great!');
                    $('#you-have').text(data.response);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == "0") {
                    $('#password-save-btn').css({
                        'display': 'none'
                    });
                    $('#password-edit-btn').css({
                        'display': 'block'
                    });
                    var input = $('#password-det-cont').find('input');
                    input.prop('disabled', !0);
                    input.val('');
                    $('#great').text('Oops!');
                    $('#you-have').text(data.response);
                    $('#preferenceInfoModal').modal('show')
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#nominee1-det-form').on('submit', function(e) {
        e.preventDefault(e);
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/save_nominee1_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            processData: !1,
            success: function(data) {
                $('#nominee1_name_error').html('');
                $('#nominee1_address_error').html('');
                $('#nominee1_rel_error').html('');
                $('#nominee1_holding_error').html('');
                $('#nominee1_dob_error').html('');
                if (data.msg == 'success') {
                    $('#nominee1-save-btn, #nominee1-cancel-btn').css({
                        'display': 'none'
                    });
                    $('#nominee1-edit-btn').css({
                        'display': 'block'
                    });
                    var input = $('#nominee1-det-cont').find('input');
                    input.prop('disabled', !0);
                    $('#nominee1-det-cont').find('checkbox').prop('disabled', !0);
                    $('#nominee1_address').prop('disabled', !0);
                    $('#great').html('Great !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'fail') {
                    $('#great').html('Error !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'error') {
                    var errors = data.errors;
                    if (errors.hasOwnProperty('nominee1_name')) {
                        $('#nominee1_name_error').html('<strong>' + errors.nominee1_name + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee1_address')) {
                        $('#nominee1_address_error').html('<strong>' + errors.nominee1_address + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee1_relationship')) {
                        $('#nominee1_rel_error').html('<strong>' + errors.nominee1_relationship + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee1_percent')) {
                        $('#nominee1_holding_error').html('<strong>' + errors.nominee1_percent + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee1_dob')) {
                        $('#nominee1_dob_error').html('<strong>' + errors.nominee1_dob + '</strong>')
                    }
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#nominee2-det-form').on('submit', function(e) {
        e.preventDefault(e);
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/save_nominee2_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            processData: !1,
            success: function(data) {
                $('#nominee2_name_error').html('');
                $('#nominee2_address_error').html('');
                $('#nominee2_rel_error').html('');
                $('#nominee2_holding_error').html('');
                $('#nominee2_dob_error').html('');
                if (data.msg == 'success') {
                    $('#nominee2-save-btn, #nominee2-cancel-btn').css({
                        'display': 'none'
                    });
                    $('#nominee2-edit-btn').css({
                        'display': 'block'
                    });
                    var input = $('#nominee2-det-cont').find('input');
                    input.prop('disabled', !0);
                    $('#nominee2-det-cont').find('checkbox').prop('disabled', !0);
                    $('#nominee2_address').prop('disabled', !0);
                    $('#great').html('Great !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'fail') {
                    $('#great').html('Error !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'error') {
                    var errors = data.errors;
                    if (errors.hasOwnProperty('nominee2_name')) {
                        $('#nominee2_name_error').html('<strong>' + errors.nominee2_name + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee2_address')) {
                        $('#nominee2_address_error').html('<strong>' + errors.nominee2_address + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee2_relationship')) {
                        $('#nominee2_rel_error').html('<strong>' + errors.nominee2_relationship + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee2_percent')) {
                        $('#nominee2_holding_error').html('<strong>' + errors.nominee2_percent + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee2_dob')) {
                        $('#nominee2_dob_error').html('<strong>' + errors.nominee2_dob + '</strong>')
                    }
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#nominee3-det-form').on('submit', function(e) {
        e.preventDefault(e);
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/save_nominee3_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            processData: !1,
            success: function(data) {
                $('#nominee3_name_error').html('');
                $('#nominee3_address_error').html('');
                $('#nominee3_rel_error').html('');
                $('#nominee3_holding_error').html('');
                $('#nominee3_dob_error').html('');
                if (data.msg == 'success') {
                    $('#nominee3-save-btn, #nominee3-cancel-btn').css({
                        'display': 'none'
                    });
                    $('#nominee3-edit-btn').css({
                        'display': 'block'
                    });
                    var input = $('#nominee3-det-cont').find('input');
                    input.prop('disabled', !0);
                    $('#nominee3-det-cont').find('checkbox').prop('disabled', !0);
                    $('#nominee3_address').prop('disabled', !0);
                    $('#great').html('Great !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'fail') {
                    $('#great').html('Error !')
                    $('#you-have').html(data.save_status);
                    $('#preferenceInfoModal').modal('show')
                } else if (data.msg == 'error') {
                    var errors = data.errors;
                    if (errors.hasOwnProperty('nominee3_name')) {
                        $('#nominee3_name_error').html('<strong>' + errors.nominee3_name + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee3_address')) {
                        $('#nominee3_address_error').html('<strong>' + errors.nominee3_address + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee3_relationship')) {
                        $('#nominee3_rel_error').html('<strong>' + errors.nominee3_relationship + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee3_percent')) {
                        $('#nominee3_holding_error').html('<strong>' + errors.nominee3_percent + '</strong>')
                    }
                    if (errors.hasOwnProperty('nominee3_dob')) {
                        $('#nominee3_dob_error').html('<strong>' + errors.nominee3_dob + '</strong>')
                    }
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#copy1-addr').on('click', function() {
        if ($('#copy1-addr').prop("checked") == !0) {
            $('#nominee1_address').val($('#address').val())
        } else {
            $('#nominee1_address').val('')
        }
    });
    $('#copy2-addr').on('click', function() {
        if ($('#copy2-addr').prop("checked") == !0) {
            $('#nominee2_address').val($('#address').val())
        } else {
            $('#nominee2_address').val('')
        }
    });
    $('#copy3-addr').on('click', function() {
        if ($('#copy3-addr').prop("checked") == !0) {
            $('#nominee3_address').val($('#address').val())
        } else {
            $('#nominee3_address').val('')
        }
    });
    $('#nominee1_dob,#nominee2_dob,#nominee3_dob').datepicker({
        changeMonth: !0,
        changeYear: !0,
        yearRange: '1940:2010',
        dateFormat: 'dd-mm-yy'
    });
    $("#avatar").change(function() {
        var form_data = new FormData();
        form_data.append('avatar', $('#avatar').get(0).files[0]);
        $.ajax({
            type: "POST",
            url: "/change_image",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: form_data,
            cache: !1,
            contentType: !1,
            processData: !1,
            success: function(data) {
                appendImage(data)
            },
            error: function(xhr, status, error) {
                alert(xhr)
            },
        })
    });

    function appendImage(data) {
        if (data.msg == "1") {
            $('#per-det-pic').attr('src', '/assets/profile_images/' + data.response);
            $('#great').html('Great !')
            $('#you-have').html('Image Updated Successfully.');
            $('#preferenceInfoModal').modal('show')
        }
        if (data.msg == "0") {
            $('#great').html('Error !')
            $('#you-have').html(data.response);
            $('#preferenceInfoModal').modal('show');
            $('#img-error').display(block)
        }
    }
})