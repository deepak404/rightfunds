$(document).ready(function() {
    var radios = $('input:radio[name=acc_type]');
    if (radios.is(':checked') === !1) {
        radios.filter('[value=0]').prop('checked', !0)
    }

    $('#register-form').on('submit', function(e) {
        e.preventDefault(e);
        send_otp()
    });



    //function to handle Register Error Response

    function handle_register_response(data) {
        if (data.hasOwnProperty('email')) {
            $('#email_error').html('<span>' + data.email[0] + '</span>')
        }
        if (data.hasOwnProperty('mobile')) {
            $('#mobile_error').html('<span>' + data.mobile[0] + '</span>')
        }
        // if (data.hasOwnProperty('pan')) {
        //     $('#pan_error').html('<span>' + data.pan[0] + '</span>')
        // }
        if (data.hasOwnProperty('password')) {
            $('#password_error').html('<span>' + data.password[0] + '</span>')
        }
        if (data.hasOwnProperty('password_confirmation')) {
            $('#password_confirmation_error').html('<span>' + data.password_confirmation[0] + '</span>')
        }
        if (data.hasOwnProperty('name')) {
            $('#name_error').html('<span>' + data.name[0] + '</span>')
        }
    }


    $("#otp-ver").on('submit', function(e) {
        e.preventDefault(e);
        var otp = $('#otp1').val() + $('#otp2').val() + $('#otp3').val() + $('#otp4').val();
        var formdata = 'otp=' + otp;
        $.ajax({
            type: "POST",
            url: "/verify_otp",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formdata,
            async: !1,
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg == 'success') {
                    $('#otp_success_msg').html('<span>' + data.response + '<span>');
                    $('#otp_error_msg').html('');
                    window.setTimeout(function() {
                        var register_status = register_user();
                        $('#otpModal').modal('hide');
                        if (register_status == 'success') {
                            $("#register-form").trigger('reset');
                            //changing location after successfull registration to Home page
                            window.location.href = "account_activation";
                        } else {
                            alert('Registration failed. Please refresh and try again.')
                        }
                    }, 2000)
                } else {
                    $('#otp_error_msg').html('<span>' + data.response + '<span>');
                    $('#otp_success_msg').html('')
                }
            },
            error: function(xhr, status, error) {},
        })
    });


    $("#resend").on('click', function() {
        $('.otp-field').val('');
        send_otp()
    });


    //function to send OTP

    function send_otp() {
        var name = $('#name').val();
        //var pan = $('#pan').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var password_conf = $('#password-confirmation').val()
        var mobile = $('#mobile').val();
        var acc_type = "0";
        var formdata = 'name=' + name + '&email=' + email + '&mobile=' + mobile + '&acc_type=' + acc_type + '&password=' + password + '&password_confirmation=' + password_conf;
        $.ajax({
            type: "POST",
            url: "/send_otp",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formdata,
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg == 'success') {
                    // $('#otp_success_msg').html('<span>' + data.response + '<span>');
                    // $('#otp_error_msg').html('');
                    // $('#otpModal').modal('show')

                    $('#reg-form-holder > h3').text('We have sent you an Access code');
                    $('#otp-ver').show();
                    $('#register-form').hide();
                } else if (data.msg == 'error') {
                    response = data.response;
                    //console.log(response);
                    $.each(response,function(key,value){
                        //console.log(key+'='+value);
                        $('#'+key).siblings('.text-danger').text(value);
                    });
                } else {
                    alert('Something went wrong. Please refersh the page and try again.')
                }
            },
            error: function(xhr, status, error) {
                if (error == 'Unprocessable Entity') {
                    handle_register_response(xhr.responseJSON)
                } else {
                    alert('Something went wrong. Please refersh the page and try again.')
                }
            },
        })
    }

    //OTP function ends

    //Login form through AJAX

    $('#login-form').on('submit', function(e) {
        e.preventDefault(e);
        var formdata = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/login",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formdata,
            async: !1,
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg == 'success') {
                    window.location.href = data.redirect;
                } 
                else if(data.msg == 'activation'){
                    window.location.href = data.redirect;
                }
                else {
                    if (data.hasOwnProperty('msg')) {
                        $('#login-error').text(data.msg);
                    }
                }
            },
            error: function(xhr, status, error) {
                var jsonsresponse = xhr.responseJSON;
                if (jsonsresponse.hasOwnProperty('email')) {
                    $('#email_error').html('<span>' + jsonsresponse.email[0] + '</span>')
                }
                if (jsonsresponse.hasOwnProperty('password')) {
                    $('#password_error').html('<span>' + jsonsresponse.password[0] + '</span>')
                }
            },
        })
    });

    //LOGIN form through AJAX Ends

    $('.otp-field').keyup(function(e) {
        if ($(this).val() == '') {
            var inputs = $(this).closest('form').find('.otp-field');
            inputs.eq(inputs.index(this) + 0).focus()
        } else {
            if (e.keyCode == 8) {
                $(this).focus()
            } else {
                var inputs = $(this).closest('form').find('.otp-field');
                inputs.eq(inputs.index(this) + 1).focus()
            }
        }
    });



    /*Below Function is to send Registration through AJAX*/

    function register_user() {
        var name = $('#name').val();
        //var pan = $('#pan').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var password_conf = $('#password-confirmation').val();
        var mobile = $('#mobile').val();
        var acc_type = "0";

        // Pan commendted out 
        //'&pan=' + pan +

        var formdata = 'name=' + name + '&email=' + email + '&mobile=' + mobile +'&acc_type=' + acc_type + '&password=' + password + '&password_confirmation=' + password_conf;
        //console.log(formdata);
        var register_status;
        $.ajax({
            type: "POST",
            url: "/register",
            async: !1,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formdata,
            cache: !1,
            processData: !1,
            success: function(data) {
                register_status = 'success'
            },
            error: function(xhr, status, error) {
                $('#otpModal').modal('hide');
                if (error == 'Unprocessable Entity') {
                    handle_register_response(xhr.responseJSON)
                } else {
                    $('#error-done').attr('onclick', "javascript:$('#registerFailModal').modal('hide')");
                    $('#error-msg').html('Something went wrong. Please refersh the page and try again.');
                    $('#registerFailModal').modal('show')
                }
                register_status = 'failure'
            },
        });
        return register_status
    }


    /*Registration through AJAX ends*/



    $('#pan').keyup(function() {
        $(this).val($(this).val().toUpperCase())
    })
    $('#email').keyup(function() {
        $(this).val($(this).val().toLowerCase())
    })
    // $(".form-inp").on("keydown", function(event) {
    //     var arr = [8, 9, 16, 17, 20, 35, 32, 36, 37, 38, 39, 40, 45, 46];
    //     for (var i = 65; i <= 90; i++) {
    //         arr.push(i)
    //     }
    //     if (jQuery.inArray(event.which, arr) === -1) {
    //         event.preventDefault()
    //     }
    // });
    $('#mobile').on('keydown',function(e){
        //console.log(e.which);
        if ($(this).val().length == 10) {

            if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                return true;
            }
            return false;
        }
    });

    $(document).ready(function() {
        $('#trbl-log').on('click', function() {
            $('#login_div').slideToggle(250, 'swing');
            $('#trouble_login').slideToggle()
        });
        $('#back-login').on('click', function() {
            $('#trouble_login').slideToggle(250, 'swing');
            $('#login_div').slideToggle()
        });
        $('#i_forgot,#i_want').on('click', function() {
            $('#trouble_login').slideToggle();
            $('#enter_email_div').slideToggle(250, 'swing')
        });
        $('#go_back').on('click', function() {
            $('#login_div').slideToggle(250, 'swing');
            $('#enter_email_div').slideToggle(250, 'swing')
        })
    })


    $('.input-field').keyup(function(){
        $(this).siblings('.text-danger').text('');
    })

    $('#login-form .input-field').keyup(function(){
        $('#login-error').text('');
    });

    //$('#otpModal').modal('show');

    $('#back-to-s').on('click',function(){
        $('#otp-ver').hide();
        $('#register-form').show();
    });


})