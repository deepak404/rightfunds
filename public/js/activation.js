$(document).ready(function() {
    var form_submit = false;
    
    //function to handle non alphabetic letter pressed
    $(".form-inp").on("keydown", function(event) {
        var arr = [8, 9, 16, 17, 20, 35, 32, 36, 37, 38, 39, 40, 45, 46];
        for (var i = 65; i <= 90; i++) {
            arr.push(i)
        }
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault()
        }
    });

    //function to handle non numeric keys pressed
    $('.pincode').on('keydown', function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        } else {
            return true;
        }
    });

    //function to handle date picker functions
    $('#dob').datepicker({
        changeMonth: !0,
        changeYear: !0,
        yearRange: '1940:2010',
        dateFormat: 'dd-mm-yy'
    });

    //no ifsc check will happen in version 1.1
    /*function checkIfsc(data) {
        var res = "failed";
        if (($('#ifsc-code').val() == "") || $('#ifsc-code').val().length != 11) {
            $('.bank-details').empty().addClass('no-ifsc').text('Invalid IFSC code');
            res = "failed";
            return res
        } else {
            var ifsc_code = $('#ifsc-code').val();
            ifsc_code = ifsc_code.toUpperCase();
            var status = data.status;
            if (data.status == "success") {
                var address = data.data.BANK + ", " + data.data.BRANCH + ", " + data.data.ADDRESS;
                $('.bank-details').removeClass('no-ifsc').text(address);
                res = "success";
                return res
            } else if (data.status == "failed") {
                $('.bank-details').addClass('no-ifsc').text("The code you have entered is incorrect, Try again.");
                res = "failed";
                return res
            }
        }
    }*/


    $('#male, #female, #res-ind,#non-res,#single,#married').on('change', function() {
        if (($('#male').is(":checked")) || ($('#female').is(":checked"))) {
            $('#sel_sex').css({
                'display': 'none'
            })
        }
        if (($('#res-ind').is(":checked")) || ($('#non-res').is(":checked"))) {
            $('#sel_res').css({
                'display': 'none'
            })
        }
        if (($('#single').is(":checked")) || ($('#married').is(":checked"))) {
            $('#sel_mar').css({
                'display': 'none'
            })
        }
    });


    $('#signup_personal').on('submit', function(e) {
        
        e.preventDefault();
        if(checkRadio() == 0) {

            var name = $('#full_name').val();
            var pan = $('#pan_number').val();
            var dob = $('#dob').val();
            var occupation = $('#occupation').val();
            var birthcity = $('#birth_city').val();
            var fname = $('#father_name').val();
            var mname = $('#mother_name').val();
            var pincode = $('#pincode').val();
            var nationality = $('#nationality').val();
            var address = $('#address').val();
            var account_name = $('#acc_name').val();
            var sex = $("input[name='sex']:checked").val();
            var resident = $("input[name='resident']:checked").val();
            var marital_status = $("input[name='marital_status']:checked").val();
            var cancelcheck = $('#cancelcheck_file').get(0).files[0];
            var mandate = $('#mandate_file').get(0).files[0];
            var kyc = $('#kyc_file').get(0).files[0];

            var formData = new FormData();

            formData.append('name',name);
            formData.append('pan',pan);
            formData.append('dob',dob);
            formData.append('occupation',occupation);
            formData.append('birthcity',birthcity);
            formData.append('fname', fname);
            formData.append('mname', mname);
            formData.append('pincode', pincode);
            formData.append('nationality', nationality );
            formData.append('address', address);
            formData.append('account_name', account_name);
            formData.append('sex', sex);
            formData.append('resident', resident);
            formData.append('marital_status', marital_status);
            formData.append('cancelcheck', cancelcheck );
            formData.append('mandate', mandate);
            formData.append('kyc', kyc);
            console.log(formData);
        }
        //rewrite form submit to ajax

        //var check_res = checkAjax();
        /*if (!form_submit) {
            
            if (!$("input[name='sex']:checked").val()) {
                $('#sel_sex').css({
                    'display': 'block'
                })
            } else if (!$("input[name='resident']:checked").val()) {
                $('#sel_res').css({
                    'display': 'block'
                })
            } else if (!$("input[name='mar-status']:checked").val()) {
                $('#sel_mar').css({
                    'display': 'block'
                })
            } else if (check_res == "failed") {
                e.preventDefault()
            } else {
                form_submit = !0
            }
            if (form_submit) {
                $('#signup_personal').submit()
            }
        }*/
    });

    //no need for this function as no ifsc is received in version 1.1
    /*$('#add_bank_link').on('click', function() {
        if (($('#ifsc-code').val() == "") || $('#ifsc-code').val().length != 11) {
            $('.bank-details').empty().addClass('no-ifsc').text('Invalid IFSC code');
            var res = "failed";
            return res
        } else {
            return checkAjax()
        }
    });*/

    //As the ifsc is removed the function is removed from version 1.1
    /*function checkAjax() {
        var ifsc_code = $('#ifsc-code').val();
        ifsc_code = ifsc_code.toUpperCase();
        var response = 'failed';
        $.ajax({
            type: "GET",
            url: 'https://api.techm.co.in/api/v1/ifsc/' + ifsc_code,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            async: !1,
            contentType: !1,
            processData: !1,
            success: function(data) {
                response = checkIfsc(data)
            },
            error: function(xhr, status, error) {
                alert(xhr);
                response = "failed"
            },
        });
        return response
    }*/

    //function to auto change pan input to upper case
    $('#pan_number').keyup(function() {
        $(this).val($(this).val().toUpperCase())
    });

    //function to 
    function checkRadio()
    {
        var radio_status = 0;
        //to check wether sex is checked
        if (!($('input[name="sex"]').is(':checked'))) {
            radio_status = 1;
            $('#sex_error').html('Sex is required');
        } else {
            $('#sex_error').html('');
        }

        //to check wether resident is checked
        if (!($('input[name="resident"]').is(':checked'))) {
            radio_status = 1;
            $('#nationality_error').html('Nationality is required');
        } else {
            $('#nationality_error').html('');
        }

        //to check wether marital status is checked
        if (!($('input[name="marital_status"]').is(':checked'))) {
            radio_status = 1;
            $('#status_error').html('Marital status is required');
        } else {
            $('#status_error').html('');
        }

        //to check wether cancelled cheque or bank statement added
        if($('#cancelcheck_file').val() == ''){
            radio_status = 1;
            $('#cancelcheck_error').html('Cancelled Cheque or Bank Statement document is required');
        } else {
            var file = $('#cancelcheck_file').get(0).files[0];console.log(file);
            var fileType = file["type"];
            var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
            //check file type
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                radio_status = 1;
                $('#cancelcheck_error').html('Invalid file format');
            }

            //check file size
            var fileSize = (file.size / 1024) / 1024;console.log(fileSize); //size in MB
            //check file size is less than 5
            if (fileSize > 5) {
                radio_status = 1;
                $('#cancelcheck_error').html('File size should be less than 5MB');
            }

            if(radio_status == 0){
                $('#cancelcheck_error').html('');
            }
        }

        //to check wether mandate added
        if($('#mandate_file').val() == ''){
            radio_status = 1;
            $('#mandate_error').html('Mandate document is required');

        } else {
            var file = $('#mandate_file').get(0).files[0];console.log(file);
            var fileType = file["type"];
            var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
            //check file type
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                radio_status = 1;
                $('#mandate_error').html('Invalid file format | Upload only jpg,jpeg and PNG files');
            }

            //check file size
            var fileSize = (file.size / 1024) / 1024;console.log(fileSize); //size in MB
            //check file size is less than 5
            if (fileSize > 5) {
                radio_status = 1;
                $('#mandate_error').html('File size should be less than 5MB');
            }

            if(radio_status == 0){
                $('#mandate_error').html('');
            }
        }

        //to check wether kyc added
        if($('#kyc_file').val() == ''){
            radio_status = 1;
            $('#kyc_error').html('KYC document is required');
        } else {
            var file = $('#kyc_file').get(0).files[0];console.log(file);
            var fileType = file["type"];
            var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
            //check file type
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                radio_status = 1;
                $('#kyc_error').html('Invalid file format');
            }

            //check file size
            var fileSize = (file.size / 1024) / 1024;console.log(fileSize); //size in MB
            //check file size is less than 5
            if (fileSize > 5) {
                radio_status = 1;
                $('#kyc_error').html('File size should be less than 5MB');
            }

            if(radio_status == 0){
                $('#kyc_error').html('');
            }
        }
        return radio_status;
    }
});