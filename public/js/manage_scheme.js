$(document).ready(function(){
  $('#add_scheme_btn').on('click', function() {
  	$('#addSchemeModal').modal('show');
  });

  //function to handle add scheme
  $(document).on('submit','#add_scheme_form',function(e){
    e.preventDefault();
    $('#add_scheme_submit').css({'display':'none'});

    var scheme_name = $('#add_scheme_name').val();
    var scheme_code = $('#add_scheme_code').val();
    var bse_scheme_code = $('#add_bse_scheme_code').val();
    var scheme_file = $('#add_scheme_nav').get(0).files[0];
    var scheme_type = $('#add_scheme_type').val();
    var scheme_priority = $('#add_scheme_priority').val();
    var scheme_fund_manager = $('#add_fund_manager').val();
    var scheme_launch_date = $('#add_launch_date').val();
    var scheme_asset_size = $('#add_asset_size').val();
    var scheme_benchmark = $('#add_benchmark').val();
    var fund_type = $('#add_fund_type').val();
    var exit_load = $('#add_exit_load').val();

    var formData = new FormData();

    formData.append('scheme_name',scheme_name);
    formData.append('scheme_code',scheme_code);
    formData.append('bse_scheme_code',bse_scheme_code);
    formData.append('scheme_file',scheme_file);
    formData.append('scheme_type',scheme_type);
    formData.append('fund_type',fund_type);
    formData.append('scheme_priority', scheme_priority);
    formData.append('fund_manager', scheme_fund_manager);
    formData.append('launch_date', scheme_launch_date);
    formData.append('asset_size', scheme_asset_size );
    formData.append('benchmark', scheme_benchmark);
    formData.append('exit_load',exit_load);
    console.log(formData);

    $.ajax({
      type: "POST",
      url: "/admin/add_scheme",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },

      async : false,
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        console.log(data);
        $('#add_scheme_status').text('');
          //addInvestment(data);
          //$('#addNewSchemeModal').modal('hide');
          if (data.msg == "1") {
            $('#add_scheme_form')[0].reset();
            if (data.scheme_data.scheme_status == 0) {
                $scheme_status = "Inactive";
            }else{
                $scheme_status = "active";
            }
            var scheme_data ='<tr><td>'+data.scheme_data.scheme_name+'</td><td>'+data.scheme_data.scheme_code+'</td><td>'+data.scheme_data.fund_type+'</td><td>'+data.scheme_data.scheme_priority+'</td><td>'+data.scheme_data.bse_scheme_code+'</td><td><i data-code="'+data.scheme_data.scheme_code+'" class="remove-scheme material-icons delete_scheme_icon">delete</i></td><td><i data-code="'+data.scheme_data.scheme_code+'" class="edit-scheme material-icons edit_scheme_icon">mode_edit</i></td></tr>';
            var scheme_body = $('#scheme_details').html();console.log(scheme_body);
            $('#scheme_details').html(scheme_body+scheme_data);
            $('#addSchemeModal').modal('hide');
            $('#userStatusModal').modal('show');
            $('#modal_header').text('Scheme Added successfully');
            $('#modal_header').css("color", "green");
          } else {
            $('#add_scheme_status').css("color", "red");
            var add_status = '';
            $.each(data.msg, function(key, value) {
            	add_status += value;
            });
            $('#add_scheme_status').text(add_status);
          }
      },
      error: function(xhr, status, error) {
          $('#add_scheme_status').css("color", "red");
          $('#add_scheme_status').text('New Scheme creation failed');
      },
    });
  });

  //function update scheme
  $(document).on('click', '.edit-scheme', function() {
    var scheme_code = $(this).data("code");
    var formData ='scheme_code='+scheme_code;
    $('#modal_header').text('');
    $.ajax({
      type: "POST",
      url: "/admin/get_update_scheme_data",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },

      async : false,
      data: formData,
      cache: false,
      processData: false,
      success: function(data) {
        console.log(data);
        $('#update_scheme_status').text('');
        if(data.status == 1) {
          $('#update_scheme_form')[0].reset();
          //$('#old_scheme_code').val(scheme_code);
          $('#update_scheme_name').val(data.msg.scheme_name);
          $('#update_scheme_code').val(data.msg.scheme_code);
          $('#update_bse_scheme_code').val(data.msg.bse_scheme_code);
          $('#update_fund_manager').val(data.msg.fund_manager);
          $('#update_launch_date').val(data.msg.launch_date);
          $('#update_asset_size').val(data.msg.asset_size);
          $('#update_exit_load').val(data.msg.exit_load);
          $('#update_benchmark').val(data.msg.benchmark);
          $('#update_bse_scheme_code').val(data.msg.bse_scheme_code);
          $('#update_scheme_priority').val(data.msg.scheme_priority);
          $('#update_scheme_status').val(data.msg.scheme_status);
          $('#updateSchemeModal').modal('show');
          if($('#update_fund_type option[value="'+data.msg.fund_type+'"]').length > 0) {
            $('#update_fund_type option[value="'+data.msg.fund_type+'"]').prop("selected", true);
          } else {
            $('#update_fund_type option[value=""]').prop("selected", true);
          }
          if($('#update_scheme_type option[value="'+data.msg.scheme_type+'"]').length > 0) {
            $('#update_scheme_type option[value="'+data.msg.scheme_type+'"]').prop("selected", true);
          } else {
            $('#update_scheme_type option[value=""]').prop("selected", true);
          }
          var priority = parseInt(data.msg.scheme_priority);
          if(data.msg.scheme_priority < 0 ) {
            $('#update_scheme_status option[value="1"]').prop("selected", true);
          } else {
            $('#update_scheme_status option[value="0"]').prop("selected", true);
          }
        } else {
          $('#modal_header').text(data.msg);
          $('#userStatusModal').modal('show');
        }
      },
      error: function(xhr, status, error) {
        $('#modal_header').text('Something went wrong. Please refersh and try again.');
        $('#userStatusModal').modal('show');
      },
    });
  });

    //function to handle update scheme
  $(document).on('submit','#update_scheme_form',function(e){
    e.preventDefault();

    var scheme_name = $('#update_scheme_name').val();
    var scheme_code = $('#update_scheme_code').val();
    var bse_scheme_code = $('#update_bse_scheme_code').val();
    var scheme_type = $('#update_scheme_type').val();
    var scheme_priority = $('#update_scheme_priority').val();
    var scheme_fund_manager = $('#update_fund_manager').val();
    var scheme_launch_date = $('#update_launch_date').val();
    var scheme_asset_size = $('#update_asset_size').val();
    var scheme_benchmark = $('#update_benchmark').val();
    var fund_type = $('#update_fund_type').val();
    var exit_load = $('#update_exit_load').val();
    var scheme_status = $('#update_scheme_status option:checked').val();
    if(scheme_status == 0){
      scheme_priority = -999;
    }

    console.log(scheme_status);

    var formData = new FormData();

    formData.append('scheme_name',scheme_name);
    formData.append('scheme_code',scheme_code);
    formData.append('bse_scheme_code',bse_scheme_code);
    formData.append('scheme_type',scheme_type);
    formData.append('fund_type',fund_type);
    formData.append('scheme_priority', scheme_priority);
    formData.append('fund_manager', scheme_fund_manager);
    formData.append('launch_date', scheme_launch_date);
    formData.append('asset_size', scheme_asset_size );
    formData.append('benchmark', scheme_benchmark);
    formData.append('exit_load',exit_load);
    formData.append('scheme_status',scheme_status);
    console.log(formData);

    $.ajax({
      type: "POST",
      url: "/admin/update_scheme_data",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },

      async : false,
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
        console.log(data);
        $('#update_scheme_status').text('');
          //addInvestment(data);
          //$('#addNewSchemeModal').modal('hide');
          if (data.msg == "1") {
            $('#update_scheme_form')[0].reset();
            //$(document).('#'+scheme_code).remove();
            $(document).closest('tr').remove();
            if(scheme_priority < 0) {
              var scheme_status = data.scheme_data.scheme_status = 1 ? 'Active' : 'In-active';
              var scheme_data ='<tr><td>'+data.scheme_data.scheme_name+'</td><td>'+data.scheme_data.scheme_code+'</td><td>'+data.scheme_data.fund_type+'</td><td>'+data.scheme_data.scheme_priority+'</td><td>'+data.scheme_data.bse_scheme_code+'</td><td>'+scheme_status+'</td><td><i data-code="'+data.scheme_data.scheme_code+'" class="edit-scheme material-icons edit_scheme_icon">mode_edit</i></td></tr>';
            }else {
              var scheme_data ='<tr><td style="color: black;">'+data.scheme_data.scheme_name+'</td><td style="color: black;">'+data.scheme_data.scheme_code+'</td><td style="color: black;">'+data.scheme_data.fund_type+'</td><td style="color: black;">'+data.scheme_data.scheme_priority+'</td><td>'+data.scheme_data.bse_scheme_name+'</td><td>'+scheme_status+'</td><td><i data-code="'+data.scheme_data.scheme_code+'" class="edit-scheme material-icons edit_scheme_icon">mode_edit</i></td></tr>';
            }
            var scheme_body = $('#scheme_details').html();//console.log(scheme_body);
            $('#scheme_details').html(scheme_body+scheme_data);
            $('#updateSchemeModal').modal('hide');
            $('#userStatusModal').modal('show');
            $('#modal_header').text('Scheme updated successfully');
            $('#modal_header').css("color", "green");
          } else {
            $('#update_scheme_status').css("color", "red");
            var add_status = '';
            $.each(data.msg, function(key, value) {
            	add_status += value;
            });
            $('#update_scheme_status').text(add_status);
          }
      },
      error: function(xhr, status, error) {
          $('#update_scheme_status').css("color", "red");
          $('#update_scheme_status').text('Scheme updation failed');
      },
    });
  });
});