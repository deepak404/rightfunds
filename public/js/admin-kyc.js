$(document).ready(function() {
    var imgType = '';
    var file = '';
    var userId = '';
    $(document).on('click', ".suggested-user", function() {
        console.log('clicked');
        $user_id = $(this).data('id');
        var dataString = 'user_id=' + $user_id;
        $.ajax({
            type: "POST",
            url: "/admin/showUserKyc",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: dataString,
            cache: !1,
            // async :false,
            processData: !1,
            beforeSend: function() {},
            success: function(data) {
                $('#kyc_details').css({
                    display: 'block'
                });
                appendUserDetails(data)
            },
            error: function(xhr, status, error) {},
        });

        function appendUserDetails(data) {
            if (data.msg == "success") {
                $('.srch-result-img').attr('src','../icons/pic_upload.png').css({'opacity':'0.3'});
                $('.download-files').attr('href','#');
                var bank_info = data.bank_info;
                var response = data.response;
                $('#srch-per-bank1-name').empty()
                $.each(bank_info, function(key, responses) {
                    $('#srch-per-bank1-name').append('<option value="'+responses['bank_id']+'">'+responses['bank_name']+'</option>');
                });

                $('#srch-per-bank1-name').val(response.bank_id);
                $('#srch-per-name').val(response.user_name);
                $('#srch-per-mn').val(response.mothers_name);
                $('#srch-per-fsn').val(response.fs_name);
                $('#srch-per-mob').val(response.user_mob);
                $('#srch-per-occ').val(response.occupation);
                $('#srch-per-dob').val(response.dob);
                $('#srch-per-nat').val(response.nationality);
                $('#srch-per-addr').val(response.user_address);
                $('#srch-per-city').val(response.b_city);
                $('#srch-per-pin').val(response.user_pin);
                $('#amount_invested').html(response.amount_invested);
                $('#current_value').html(response.current_value);
                $('#profit_loss').html(response.profit_loss_amount);
                $('#xirr_return').html(response.xirr_return);
                $('#profile-name').text(response.user_name);
                $('#search-email').text(response.user_email);
                $('#srch-user-no').text(response.user_mob);
                $('#pan_id').val(response.user_id);
                $('#video_id').val(response.user_id);
                $('#sig_id').val(response.user_id);
                $('#apb_id').val(response.user_id);
                $('#apf_id').val(response.user_id);
                $('#mnt_id').val(response.user_id);
                $('#aof_id').val(response.user_id);
                $('#ekyc_id').val(response.user_id);
                $('#cc_id').val(response.user_id);
                $('#mandate_id').val(response.user_id);
                $('#kyc-port').attr('href','portfolio_details/'+response.user_id);
                $('#kyc-inv').attr('href','inv_history/'+response.user_id);
                $('#kyc-tax').attr('href','tax_saving/'+response.user_id);
                $('#srch-per-aadhar').val(response.aadhar);
                $('#srch-per-pob').val(response.b_city);
                $('#personal-form-id').val(response.user_id);
                $('#address-form-id').val(response.user_id);
                $('#nominee-form-id').val(response.user_id);
                $('#bank-form-id').val(response.user_id);
                $('#super-id').attr('href','super_login/'+response.user_id);

                // $('#mandate-img').attr('src','../storage/mandate/'+response.mandate_link);
                var investment_performance = '';
                var user_personal_details = '';
                var user_investment_details = '';
                if (response.day_change >= 0) {
                    user_personal_details = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 user-content"><p class="profile-name">' + response.user_name + ' |&nbsp;</p><p class="invest-text" id="search-email">' + response.user_email + ' |&nbsp;</p><p id ="srch-user-no">' + response.user_mob + '</p><p class="day-change">1-Day Change : <span id="up-val">Rs.'+response.day_change+'</span></p></div></div>';
                }else{
                    user_personal_details = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 user-content"><p class="profile-name">' + response.user_name + ' |&nbsp;</p><p class="invest-text" id="search-email">' + response.user_email + ' |&nbsp;</p><p id ="srch-user-no">' + response.user_mob + '</p><p class="day-change">1-Day Change : <span id="down-val">Rs.'+response.day_change+'</span></p></div></div>';                
                }
                user_investment_details = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Total Investment</p><p class="return-det"><span id="amount_invested">Rs.' + response.amount_invested + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Current Value</p><p class="return-det"><span id="current_value">Rs.' + response.current_value + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Net Profit</p><p class="return-det"><span id="profit_loss">Rs.' + response.profit_loss_amount +'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Return (XIRR)</p><p class="return-det"><span id="xirr_return">' + response.xirr_return + '</span>%</p></div></div>';
                investment_performance += '<div class = "row investment-summary" id="user-det-row">' + user_personal_details + '' + user_investment_details + '</div>';
                $('#user_performance_det').html(investment_performance);
                $('#user-det-row').slideDown();
                var sex = response.sex;
                if (sex == 'male') {
                    $('#male').prop('checked', !0)
                }
                if (sex == 'female') {
                    $('#female').prop('checked', !0)
                }
                var kyc = response.kyc;
                if (kyc == "1") {
                    $('#kyc-yes').prop('checked', !0)
                }
                if (kyc == "0") {
                    $('#kyc-no').prop('checked', !0)
                }
                var mar_status = response.mar_status;
                if (mar_status == 'single') {
                    $('#single').prop('checked', !0)
                }
                if (mar_status == 'married') {
                    $('#married').prop('checked', !0)
                }
                $('#srch-per-bank1-accno').val(response.acc_no);
                $('#srch-per-bank1-ifsc').val(response.ifsc_code);
                // $('#srch-per-bank1-name').val(response.bank_name);
                $('#srch-per-branch1-name').val(response.branch_name);
                $('#bank1-addr').val(response.bank_addr);
                var bank_type = response.bank_type;
                if (response.acc_type == 'savings') {
                    $('#savings').attr('checked', !0);
                    $('#savings').parent().find("img").attr("src", "../icons/check_en.png")
                }else{
                    $('#current').attr('checked', !0);
                    $('#current').parent().find("img").attr("src", "../icons/check_en.png")         
                }
                $('#srch-per-pan').val(response.pan);
                $('#srch-per-nom1-name').val(response.nom1_name);
                $('#srch-per-nom1-dob').val(response.nom1_dob);
                $('#srch-per-nom1-rel').val(response.nom1_relationship);
                $('#srch-per-nom2-name').val(response.nom2_name);
                $('#srch-per-nom2-dob').val(response.nom2_dob);
                $('#srch-per-nom2-rel').val(response.nom2_relationship);
                $('#srch-per-nom3-name').val(response.nom3_name);
                $('#srch-per-nom3-dob').val(response.nom3_dob);
                $('#srch-per-nom3-rel').val(response.nom3_relationship);
                $('#srch-per-nom1-per').val(response.nom1_holding);
                $('#srch-per-nom2-per').val(response.nom2_holding);
                $('#srch-per-nom3-per').val(response.nom3_holding);
                $('#srch-per-pan').val(response.user_pan)

                if (response.pan_link!==null) {
                    $('#pan-img').attr('src','../storage/pan/'+response.pan_link).css({'opacity':'1'});
                    $('#download-pan').attr('href','../storage/pan/'+response.pan_link);
                }

                if (response.apf_link !== null) {
                    $('#addr-pr-front').attr('src','../storage/apf/'+response.apf_link).css({'opacity':'1'});
                    $('#download-apf').attr('href','../storage/apf/'+response.apf_link);
                }

                if (response.apb_link !== null) {
                    $('#addr-pr-back').attr('src','../storage/apb/'+response.apb_link).css({'opacity':'1'});
                    $('#download-apb').attr('href','../storage/apb/'+response.apb_link);
                }

                if (response.video_link !== null) {
                    $('#ekyc-video').attr('src','../ekycvideo/'+response.video_link).css({'opacity':'1'});
                    $('#download-video').attr('href','../ekycvideo/'+response.video_link);
                }
    
                if (response.sig_link !== null) {
                    $('#sign-img').attr('src','../storage/sig/'+response.sig_link).css({'opacity':'1'});
                    $('#download-sig').attr('href','../storage/sig/'+response.sig_link);
                }

                if (response.cc_link !== null) {
                    $('#cc-img').attr('src','../files/cc/'+response.cc_link).css({'opacity':'1'});
                    $('#download-cc').attr('href','../files/cc/'+response.cc_link);
                }

                if (response.mandate_link !== null) {
                    $('#mandate-img').attr('src','../files/mandates/'+response.mandate_link).css({'opacity':'1'});
                    $('#download-mandate').attr('href','../files/mandates/'+response.mandate_link);
                }



                if (response.ekycform_link !== null) {
                    $('#kyc-form').attr('src','../storage/ekyc/'+response.ekycform_link).css({'opacity':'1'});
                    $('#download-ekyc').attr('href','../storage/ekyc/'+response.ekycform_link);
                }

                if (response.aof_link !== null) {
                    $('#acc-op-form').attr('src','../storage/aof/'+response.aof_link).css({'opacity':'1'});
                    $('#download-aof').attr('href','../storage/aof/'+response.aof_link);
                }

                if (response.mandate_hc_link !== null) {
                    $('#mandate-form').attr('src','../storage/mandate/'+response.mandate_hc_link).css({'opacity':'1'});
                    $('#download-mnt').attr('href','../storage/mandate/'+response.mandate_hc_link);
                }
            }
        }
        $('.checkbox-div input').on('change', function() {
            var parentDiv = $(this).parents(".checkbox-div:eq(0)");
            if ($(this).attr('id') == 'savings') {
                $('#current').attr('checked', 0);
                $('#savings').attr('checked', !0);
                $('#current').parent().find("img").attr("src", "../icons/check_dis.png");
                $('#savings').parent().find("img").attr("src", "../icons/check_en.png");
            }else{
                $('#savings').attr('checked', 0);
                $('#current').attr('checked', !0);
                $('#savings').parent().find("img").attr("src", "../icons/check_dis.png");
                $('#current').parent().find("img").attr("src", "../icons/check_en.png");
            }
            // if ($(this).is(":checked")) {
            //     $(parentDiv).find("img").attr("src", "../icons/check_en.png")
            // } else {
            //     $(parentDiv).find("img").attr("src", "../icons/check_dis.png")
            // }
        });
    });
    $('#newly_enrolled').on('click', function() {
        $.ajax({
            type: "GET",
            url: "/admin/get_user_investment_performance",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            success: function(data) {
                if (data.msg == 'success') {
                    var investment_performance = '';
                    $('#user_performance_det').html('');
                    $('#kyc_details').css({
                        'display': 'none'
                    });
                    $.each(data.response, function(key, response) {
                        user_personal_details = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block "><div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><img class="img profile-image" src="/assets/profile_images/' + response.profile_image + '" id = "search-image"></div><div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content"><p class="profile-name">' + response.name + '!</p><p class="invest-text" id="search-email">' + response.email + '</p><p id ="srch-user-no">' + response.mobile + '</p></div></div>';
                        user_investment_details = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Current Value</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="amount_invested">' + response.amount_invested + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Total Investment</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="current_value">' + response.current_value + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Net Profit</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="profit_loss">' + response.profit_loss_amount + '</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Return (XIRR)</p><p class="return-det"><span id="xirr_return">' + response.xirr_return + '</span>%</p></div></div>';
                        investment_performance += '<div class = "row investment-summary" id="user-det-row">' + user_personal_details + '' + user_investment_details + '</div>'
                    });
                    $('#user_performance_det').html(investment_performance);
                    $('#user-det-row').css({
                        'display': 'block'
                    })
                }
            },
            error: function(xhr, status, error) {},
        })
    });
    $('#update_kyc').on('click', function() {})

    $(document).on('change','.upload-file', function(){
        // alert($(this).val());
          file = $(this).get(0).files[0];
          var ext = $(this).val().split('.').pop();
          imgType = $(this).attr('id');
          userId = $('#'+imgType+'_id').val();
          var format = ['mp4','jpg','jpeg','png'];
          if ($.inArray(ext, format) !== -1) {
              $('#file-upload-modal').modal('show');
          }else{
            alert('Unknown file format');
          }
    });

    $(document).on('click','#confirm-btn', function(){
      $('#file-upload-modal').modal('hide');
        var formData = new FormData();
              formData.append('file',file);
              formData.append('user_id',userId);
              formData.append('type',imgType);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/file_upload',
            data: formData,
            //async : false,
            contentType: false,
            processData: false,
            success:function(data){
                if (data['msg'] == 0) {
                      // $('#file-uplode-modal').modal('hide');
                    alert('error');
                }else if (data['msg'] == 2) {
                      // $('#file-uplode-modal').modal('hide');
                    alert('Try Again!!!');
                }else if (data['msg'] == 1){
                    imageUpdate(data)
                }
            },
            error:function(){ 
                
            }
        });

    });

    function imageUpdate(data){
        console.log(data);
        if (data['type'] == 'mandate') {
            $('#mandate-form').attr('src','../storage/mandate/'+data['file_name']).css({'opacity':'1'});
            $('#download-mnt').attr('href','../storage/mandate/'+data['file_name']);
        }else if (data['type'] == 'aof') {
            $('#acc-op-form').attr('src','../storage/aof/'+data['file_name']).css({'opacity':'1'});
            $('#download-aof').attr('href','../storage/aof/'+data['file_name']);
        }else if (data['type'] == 'ekyc') {
            $('#kyc-form').attr('src','../storage/ekyc/'+data['file_name']).css({'opacity':'1'});
            $('#download-ekyc').attr('href','../storage/ekyc/'+data['file_name']);
        }else if (data['type'] == 'mandates') {
            $('#mandate-form').attr('src','../storage/mandate/'+data['file_name']).css({'opacity':'1'});
            $('#download-mnt').attr('href','../storage/mandate/'+data['file_name']);
        }else if (data['type'] == 'cc') {
            $('#cc-img').attr('src','../files/cc/'+data['file_name']).css({'opacity':'1'});
            $('#download-cc').attr('href','../files/cc/'+data['file_name']);
        }else if (data['type'] == 'video') {
            $('#ekyc-video').attr('src','../ekycvideo/'+data['file_name']).css({'opacity':'1'});
            $('#download-video').attr('href','../ekycvideo/'+data['file_name']);
        }else if (data['type'] == 'sig') {
            $('#sign-img').attr('src','../storage/sig/'+data['file_name']).css({'opacity':'1'});
            $('#download-sig').attr('href','../storage/sig/'+data['file_name']);
        }else if (data['type'] == 'apb') {
            $('#addr-pr-back').attr('src','../storage/apb/'+data['file_name']).css({'opacity':'1'});
            $('#download-apb').attr('href','../storage/apb/'+data['file_name']);
        }else if (data['type'] == 'apf') {
            $('#addr-pr-front').attr('src','../storage/apf/'+data['file_name']).css({'opacity':'1'});
            $('#download-apf').attr('href','../storage/apf/'+data['file_name']);
        }else if (data['type'] == 'pan') {
            $('#pan-img').attr('src','../storage/pan/'+data['file_name']).css({'opacity':'1'});
            $('#download-pan').attr('href','../storage/pan/'+data['file_name']);
        }

        $('body').css({'opacity':'1'});
        // $('#file-uplode-modal').modal('hide');
    }


    $(document).on('click','#ekyc-video',function(){
        $('#ekyc_video').attr('src',$(this).attr('src'));
        $('#video-modal').modal('show');
        // alert($(this).attr('src'));
    });

    $('#srch-per-bank1-ifsc').on('click',function(){
        $.ajax({
            type: "GET",
            url: "https://ifsc.razorpay.com/"+$('#srch-per-bank1-ifsc').val(),
            cache: !1,
            processData: !1,
            success: function(data) {
                console.log(data);
                $('#bank1-addr').val(data.ADDRESS);
                $('#srch-per-branch1-name').val(data.BRANCH);
                $('#srch-per-bank1-city').val(data.CITY);
            },
            error: function(xhr, status, error) {},
        })
    });


    $('#personal-form').on('submit', function(event){
        event.preventDefault();
        var formData = $('#personal-form').serialize();
        $.ajax({
            type: "POST",
            url: "/admin/update_personal_info",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function(data) {
                if (data.msg == 1) {
                    // alert('Success');
                    $('#response-modal').modal('show');
                }else{
                    alert(data.info);
                    if (data.msg == 2) {
                        $('#srch-per-mob').val('');
                        $('#srch-per-mob').focus();
                    }else if (data.msg == 3) {
                        $('#srch-per-pan').val('');
                        $('#srch-per-pan').focus();
                    }else if (data.msg == 4) {
                        $('#srch-per-aadhar').val('');
                        $('#srch-per-aadhar').focus();  
                    }
                }
            },
            error: function(xhr, status, error) {

            },
        });
    });

    $('#address-form').on('submit',function(e){
        e.preventDefault();
        var formData = $('#address-form').serialize();
        $.ajax({
            type: "POST",
            url: "/admin/update_address_info",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function(data) {
                if (data.msg == 1) {
                    // alert('Success');
                    $('#response-modal').modal('show');

                }else{
                    alert('Error');
                }
            },
            error: function(xhr, status, error) {

            },
        });
    });

    $('#nominee-form').on('submit', function(e){
        e.preventDefault();
        var formData = $('#nominee-form').serialize();
        $.ajax({
            type: "POST",
            url: "/admin/update_nominee_info",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function(data) {
                if (data.msg == 1) {
                    // alert('Success');
                    $('#response-modal').modal('show');
                    
                }else{
                    alert('Error');
                }
            },
            error: function(xhr, status, error) {

            },
        });
    });

    $('#bank-form').on('submit',function(e){
        e.preventDefault();
        var formData = $('#bank-form').serialize();
        $.ajax({
            type: "POST",
            url: "/admin/update_bank_info",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            success: function(data) {
                if (data.msg == 1) {
                    // alert('Success');
                    $('#response-modal').modal('show');
                }else{
                    alert('Error');
                }
            },
            error: function(xhr, status, error) {

            },
        });
    });

});