$(document).ready(function() {
    $('#contact-form').on('submit', function(e) {
        e.preventDefault();
        var name = $('#name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();
        var message = $('#message').val();
        if (name != '' && email != '' && mobile != '' && message != '') {
            var dataString = 'name=' + name + '&email=' + email + '&mobile=' + mobile + '&message=' + message;
            $('#contact_us_request_status').html('');
            $.ajax({
                type: "POST",
                url: "/send_contact_us_email",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                    $('#contact_us_request_status').html(data.status)
                },
                error: function(xhr, status, error) {
                    $('#contact_us_request_status').html('Something went wrong. Please refresh and try again.')
                },
            })
        } else {}
    });

        $('#mobile').on('keydown',function(e){
        console.log(e.which);
        if ($(this).val().length == 10) {

            if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                return true;
            }
            return false;
        }
    });
});

