$(document).ready(function(){
    $(document).on('click',".suggested-user",function(){
        var user_id=$(this).data('id');
        $('#user_id').val(user_id);
        get_user_details()
    });

function appendUserDetails(data)
{
    if(data.msg=="success")
{                $('#inv_port').attr('href','/admin/portfolio_details/'+data.id);
                $('#inv_his').attr('href','/admin/inv_history/'+data.id);
                $('#inv_ts').attr('href','/admin/tax_saving/'+data.id);

            var investment_performance='';
            // $('#user_performance_det').html('');
            // $('#kyc_details').css({display:'none'});
            // $('#user-det-row').css({display:'block'});
            $.each(data.response,function(key,response){
                // user_personal_details='<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block "><div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><img class="img profile-image" src="/assets/profile_images/'+response.profile_image+'" id = "search-image"></div><div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content"><p class="profile-name">'+response.name+'!</p><p class="invest-text" id="search-email">'+response.email+'</p><p id ="srch-user-no">'+response.mobile+'</p></div></div>';
                // user_investment_details='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Current Value</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="amount_invested">'+response.amount_invested+'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Total Investment</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="current_value">'+response.current_value+'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Net Profit</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="profit_loss">'+response.profit_loss_amount+'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Return (XIRR)</p><p class="return-det"><span id="xirr_return">'+response.xirr_return+'</span>%</p></div></div>';
                // investment_performance+='<div class = "row investment-summary" id="user-det-row">'+user_personal_details+'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><p class="srch-options"> Rebalancing Required</p><img src= "../icons/rebal.png" class="center-block" id="rebal-icon"></div></div>'+user_investment_details+'</div>'
                $('.profile-name').text(response.name);
                $('#search-email').text(response.email);
                $('.srch-user-no').text(response.mobile);
                $('#amount_invested').text(response.amount_invested);
                $('#current_value').text(response.current_value);
                $('#profit_loss').text(response.profit_loss_amount);
                $('#xirr_return').text(response.xirr_return);
                $('.profile-image').attr('src','/assets/profile_images/'+response.profile_image);

            });

            // $('#user_performance_det').html(investment_performance);
            // $('#debt_body').html('');
            // $('#equity_body').html('');
            // $('#balanced_body').html('');
            // $('#elss_body').html('');
            if(data.hasOwnProperty('investment_summary'))
                {
                    var investment_summary=data.investment_summary;
                    if(investment_summary.hasOwnProperty('debt'))
                        {
                            var debt_body_content='';
                            $('#debt_body').empty('');
                            var debt_sum=0;
                            $.each(investment_summary.debt,function(key,value)
                            {
                                debt_body_content+='<tr><td>'+value.date+'</td><td>'+value.investment_type+'</td><td><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+value.sum+'</div></td><td>'+value.investment_status+'</td></tr>';
                                debt_sum+=parseInt(value.sum)
                            });
                            if(debt_sum>0){
                                debt_body_content+='<tr><td class="total  bt-green">TOTAL AMOUNT INVESTED</td><td class="total  bt-green"></td><td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+debt_sum+'</div></td><td class="total  bt-green"></td></tr>'
                            }
                            $('#debt_body').html(debt_body_content)
                        }else{
                            $('#debt_body').html('<tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr>')
                        }

                        if(investment_summary.hasOwnProperty('eq')){
                            var equity_body_content='';
                            $('#equity_body').empty('');
                            var equity_sum=0;
                            $.each(investment_summary.eq,function(key,value){
                                equity_body_content+='<tr><td>'+value.date+'</td><td>'+value.investment_type+'</td><td><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+value.sum+'</div></td><td>'+value.investment_status+'</td></tr>';equity_sum+=parseInt(value.sum)
                            });
                            if(equity_sum>0){
                                equity_body_content+='<tr><td class="total  bt-green">TOTAL AMOUNT INVESTED</td><td class="total  bt-green"></td><td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+equity_sum+'</div></td><td class="total  bt-green"></td></tr>'
                            }

                            $('#equity_body').html(equity_body_content)
                        }else{
                            $('#equity_body').html('<tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr>')
                        }

                        if(investment_summary.hasOwnProperty('bal')){
                            var balanced_body_content='';
                            $('#balanced_body').empty('');
                            var balanced_sum=0;
                            $.each(investment_summary.bal,function(key,value){
                                balanced_body_content+='<tr><td>'+value.date+'</td><td>'+value.investment_type+'</td><td><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+value.sum+'</div></td><td>'+value.investment_status+'</td></tr>';
                                balanced_sum+=parseInt(value.sum)
                            });
                            if(balanced_sum>0){
                                balanced_body_content+='<tr><td class="total  bt-green">TOTAL AMOUNT INVESTED</td><td class="total  bt-green"></td><td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+balanced_sum+'</div></td><td class="total  bt-green"></td></tr>'
                            }
                        $('#balanced_body_content').html(balanced_body_content)
                    }else{
                        $('#balanced_body').html('<tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr>')
                    }
            if(investment_summary.hasOwnProperty('ts'))
                {
                    var elss_body_content='';
                    $('#elss_body').empty('');
                    var elss_sum=0;
                    $.each(investment_summary.ts,function(key,value){
                        elss_body_content+='<tr><td>'+value.date+'</td><td>'+value.investment_type+'</td><td><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+value.sum+'</div></td><td>'+value.investment_status+'</td></tr>';
                        elss_sum+=parseInt(value.sum)});
                    if(elss_sum>0){
                        elss_body_content+='<tr><td class="total  bt-green">TOTAL AMOUNT INVESTED</td><td class="total  bt-green"></td><td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">'+elss_sum+'</div></td><td class="total  bt-green"></td></tr>'
                    }
                    $('#elss_body').html(elss_body_content)
                }else{
                    $('#elss_body').html('<tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr>')
                }
            }
        }else{

        }
    }

$('.checkbox-div input').on('change',function(){
    var parentDiv=$(this).parents(".checkbox-div:eq(0)");
    if($(this).is(":checked")){
        $(parentDiv).find("img").attr("src","../icons/check_en.png")
    }else{
        $(parentDiv).find("img").attr("src","../icons/check_dis.png")
    }
});

$('#newly_enrolled').on('click',function(){
    $.ajax({
        type:"GET",
        url:"/admin/get_user_investment_performance",
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        },
        cache:!1,
        processData:!1,
        success:function(data)
        {
            if(data.msg=='success'){
                var investment_performance='';
                // $('#user_performance_det').html('');
                // $('#kyc_details').css({display:'none'});
                // $('#user-det-row').css({display:'block'});
                $.each(data.response,function(key,response){
                    user_personal_details='<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block "><div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><img class="img profile-image" src="/assets/profile_images/'+response.profile_image+'" id = "search-image"></div><div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content"><p class="profile-name">'+response.name+'!</p><p class="invest-text" id="search-email">'+response.email+'</p><p id ="srch-user-no">'+response.mobile+'</p></div></div>';
                    user_investment_details='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Current Value</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="amount_invested">'+response.amount_invested+'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Total Investment</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="current_value">'+response.current_value+'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Net Profit</p><p class="return-det"><span class="rs-sym">&#x20B9;</span><span id="profit_loss">'+response.profit_loss_amount+'</span></p></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><p class="port-result-det">Return (XIRR)</p><p class="return-det"><span id="xirr_return">'+response.xirr_return+'</span>%</p></div></div>';
                    investment_performance+='<div class = "row investment-summary" id="user-det-row">'+user_personal_details+''+user_investment_details+'</div>'});
                $('#user_performance_det').html(investment_performance)
            }
        },
        error:function(xhr,status,error)
        {

        },
    })
});

function get_user_details()
{
    var user_id=$('#user_id').val();
    var range=$('#transact_range option:selected').val();
    var datastring='user_id='+user_id+'&response_type=data';
    $.ajax({
        type:"POST",
        url:"/admin/get_ind_user_investment_performance",
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        },
        data:datastring,
        cache:!1,
        processData:!1,
        beforeSend:function(){

        },
        success:function(data){
            // $('#kyc_details').css({display:'block'});
            appendUserDetails(data)
        },
        error:function(xhr,status,error)
        {

        },
    })
}
$('#transact_range').on('change',function(){
    get_user_details();
    var range=$('#transact_range option:selected').val();
    $('#transaction_range').val(range)
});
$('#export_data').on('submit',function(e){
    var formData=$('#export_data').serialize();
    if($('#transaction_range').val()!=''&&$('#user_id').val()!=''&&$('#response_type').val()!=''){
        return!0
    }
})
})
