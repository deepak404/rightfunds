
$(document).ready(function(){

    var port_val;
    var ret_per = 14;
    var amount_invested,wealth_gained;
     google.charts.load('current', {packages: ['corechart']});
            google.charts.setOnLoadCallback(drawLogScales);
    /*$('#social_facebook').hover(function(){

        $('#facebook_img').attr('src','icons/facebook_hover.png');
    },function(){
        $('#facebook_img').attr('src','icons/facebook_h.png');
    });

    $('#social_twitter').hover(function(){

        $('#twitter_img').attr('src','icons/twitter_hover.png');
    },function(){
        $('#twitter_img').attr('src','icons/twitter_h.png');
    });

    $('#social_linkedin').hover(function(){

        $('#linkedin_img').attr('src','icons/linkedin_hover.png');
    },function(){
        $('#linkedin_img').attr('src','icons/linked_in_h.png');
    });*/

      var port_array = ['<p class="text-center portfolio_type">Conservative (8%)</p>' , 
      '<p class="text-center portfolio_type">Moderate (14%)</p>' , 
      '<p class="text-center portfolio_type">Aggressive (20%)</p>',
      '<input type="text" placeholder="%" id="custom_perc" class="sip_inputs center-block">'];
      var last_key = port_array[port_array.length - 1];



      

      $('.fa').on('click',function(){

          var click_ctrl = $(this).attr('id');
          var port_type = $('#port_type').html();
          console.log(port_type);

            if (click_ctrl == "right") {

              if (port_type == last_key) {
                console.log(' right level reached');
                $('#right').css({'color' : '#999'});
                $('#left').css({'color' : '#333'});
              }else{
                $('#left').css({'color' : '#333'});
                var write = $.inArray(port_type, port_array);
                //console.log(write);
                $('#port_type').html(port_array[write + 1]);
                var get_port = $('#port_type').text();
                console.log(get_port);
                divChange(get_port);
              }



            }

            if (click_ctrl == "left") {
              if (port_type == port_array[0]) {
                console.log('left level reached');
                $('#left').css({'color' : '#999'});
                $('#right').css({'color' : '#333'});
              }else{
                $('#right').css({'color' : '#333'});
                var write = $.inArray(port_type, port_array);
                //console.log(write);
                $('#port_type').html(port_array[write - 1]);

                var get_port = $('#port_type').text();
                console.log(get_port);
                divChange(get_port);
                  

              }
            }


            google.charts.load('current', {packages: ['corechart']});
            google.charts.setOnLoadCallback(drawLogScales);


      });

        function divChange(get_port){

              if (get_port == "Conservative (8%)") {
                  $('#dyna_div').css({'margin-top' : '20px'});
                  $('#custom_perc').css({'margin-top' : '4%'});
                  $('#left').css({'color' : '#333'});
                  console.log("inside conservative");
                  ret_per = 8;
                      google.charts.load('current', {packages: ['corechart']});
                      google.charts.setOnLoadCallback(drawLogScales);
                }

                else if(get_port == "Moderate (14%)") {
                  $('#dyna_div').css({'margin-top' : '20px'});
                  $('#custom_perc').css({'margin-top' : '4%'});
                  console.log("inside moderate");
                  ret_per = 14;
                      google.charts.load('current', {packages: ['corechart']});
                      google.charts.setOnLoadCallback(drawLogScales);
                  


                }
                else if(get_port == "Aggressive (20%)") {
                 $('#dyna_div').css({'margin-top' : '20px'});
                  $('#custom_perc').css({'margin-top' : '4%'});
                  console.log("inside aggr");
                  ret_per = 20;
                      google.charts.load('current', {packages: ['corechart']});
                      google.charts.setOnLoadCallback(drawLogScales);


                }

                else if(get_port == ''){

                  $('#dyna_div').css({'margin-top' : '0px'});
                  $('#custom_perc').css({'margin-top' : '0px'});

                  $(document).on('keyup','#custom_perc',function(){
                      ret_per = $(this).val();
                      console.log(ret_per);
                      google.charts.load('current', {packages: ['corechart']});
                      google.charts.setOnLoadCallback(drawLogScales);
                  });


                }

            }

      




function drawLogScales() 
{
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Year');
      data.addColumn('number', 'Wealth Gained');
      data.addColumn('number', 'Amount Invested');
      var chart_data = [];
      var inv_amount = parseInt($('#inv_amount').val());
      var inv_period = parseInt($('#inv_period').val());
      var port_type;



      //console.log(port_type);
      //console.log(dataString);
      
      var ann_incr = parseInt($('#ann_incr').val());
      var dataString = 'investment_amount='+inv_amount+'&investment_period='+inv_period+ '&interest_rate='+ret_per+ '&increment_rate=' +ann_incr;
      var sip_details = [];
      console.log(dataString);
      var year_arr = [];
      var sip_amount_arr = [];
      var non_sip_amount_arr = [];
      $.ajax({
        type: "POST",
        url: "/getsipreturn",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        cache: false,
        processData: false,
        async: false,
        data: dataString,
        success: function(data) {
          
          $.each(data.sip_details,function(key,value){//console.log(value);
            
            var inv_year = (value.investment_year).toString();
            var sip_amount = value.sip_amount;
            var non_sip_amount = value.non_sip_amount;
            var array_data = [inv_year, sip_amount, non_sip_amount];
            amount_invested = non_sip_amount;
            wealth_gained = sip_amount;

            var position = chart_data.push([inv_year, sip_amount, non_sip_amount]);
            
          });
        },
        error : function(xhr ,status ,error)
        {
          console.log(status);
        },
      });

      console.log(amount_invested +" "+ wealth_gained);

      data.addRows(chart_data);
      var length = chart_data.length;
      length = length - 1;
      var interval = 0;
      if(length < 6) {
        interval = 1;
      } else if(length < 12) {
        interval = 2;
      } else if(length < 18) {
        interval = 3;
      } else {
        interval = Math.floor(length / 4);
      }
      console.log(interval);
      var options = {
      hAxis: {
        title: 'vertical',
        //showTextEvery: 4,
        textPosition: 'none',
      },
      vAxis: {
        title: 'horizontal',
        //showTextEvery: 4,
        textPosition: 'none',
      },
       animation:{
        duration: 1000,
        easing: 'in',
      },

      lineWidth : 4,
      series: {
        1: {curveType: 'function'}
      },
      tooltip: {isHtml: true, trigger: 'both'},

      hAxis:{ title: '',showTextEvery: parseInt(interval),baselineColor:"transparent",gridlines:{color:"transparent"},ticks:[]},
      vAxis:{title: '',baselineColor:'#d7d7d7',gridlines:{color:"transparent"},ticks:[],},
      legend:'left',
      focusTarget: 'category',
      colors:['#18C3A1', '#0091EA']
    };

      var chart = new google.visualization.LineChart(document.getElementById('sip_chart'));
      console.log(data);
      $('#wealth_gained').text(indianNumberFormat(wealth_gained));
      $('#amount_invested1').text(indianNumberFormat(amount_invested));
      
      indianNumberFormat(amount_invested);
      chart.draw(data, options);
      //chart.setSelection([{row:length, column:[1,2]}]);
      //chart.setSelection([{row:length, column:[1,2]}]);
    }

    function indianNumberFormat(amount){
        var x=amount;
         x=x.toString();
         var lastThree = x.substring(x.length-3);
         var otherNumbers = x.substring(0,x.length-3);
          if(otherNumbers != ''){
            lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
          }
          var res = "Rs. " +res;
          return res;
    }


    $(document).on('keyup','.sip_inputs',function(){

        var inv_amount = parseInt($('#inv_amount').val());
        var inv_period = parseInt($('#inv_period').val());

        $('#lakhs-99,#years-99').hide();

        if (inv_amount > 9999999) {
          $('#lakhs-99').css({'display' : 'block'});
        }

        else if(inv_period > 99 ) {
          $('#lakhs-99').css({'display' : 'block'});
        }

        else{
          google.charts.load('current', {packages: ['corechart']});
          google.charts.setOnLoadCallback(drawLogScales); 
        }


      
    });


    function resize () {
    // change dimensions if necessary
      /*var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Dogs');
      data.addColumn('number', 'Cats');

      data.addRows([
        [1500,30000,5000],[2014,85000,20000],[2015,90000,70000],[2016,120000,90000],[2017,200000,100000],[2018,200000,150000]
      ]);

      var options = {
      hAxis: {
        title: 'vertical',
        //showTextEvery: 4,
        textPosition: 'none',
      },
      vAxis: {
        title: 'horizontal'
      },
      series: {
        1: {curveType: 'function'}
      },
      tooltip: {isHtml: true},
      lineWidth : 3,

      hAxis:{ title: '',baselineColor:"transparent",gridlines:{color:"transparent"},ticks:[]},
      vAxis:{title: '',baselineColor:'#333333',gridlines:{color:"transparent"},ticks:[]},
      legend : { position:"none"},
      colors:['#18C3A1', '#0091EA']
      /*,width: chart_width*/
    /*};
    /*var chart = new google.visualization.LineChart(document.getElementById('sip_chart'));
    chart.draw(data, options);*/
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawLogScales);
}

if (window.addEventListener) {

    window.addEventListener('resize', resize);
}
else {
    window.attachEvent('onresize', resize);
}


$(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});

//redraw graph when window resize is completed  
$(window).on('resizeEnd', function() {
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawLogScales);
});


        $('#inv_amount,#inv_period').on('keydown',function(e){

              if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105 )) {
        //display error message
                //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                       return false;
            }
        });

});
