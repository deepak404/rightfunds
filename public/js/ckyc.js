$(document).ready(function() {
    $('#get-started-btn').on('click', function() {
        $('#ckyc-intro').slideToggle(250, 'swing');
        $('#selfie').slideToggle(250, 'swing')
    });
    $('#back-video-btn').on('click', function() {
        $('#ckyc-intro').slideToggle(250, 'swing');
        $('#selfie').slideToggle(250, 'swing')
    });
    $('#watch_sample').on('click', function() {
        $('#watchSampleModal').modal('show')
    });
    $('#timer').hide();

    var record = document.getElementById('record');
    var submit = document.getElementById('submit');
    var stop = document.getElementById('stop');
    var old_filename = '';
    var audio = document.querySelector('audio');
    var recordVideo = document.getElementById('record-video');
    var preview = document.getElementById('preview');
    var upload = document.getElementById('upload');
    var csrftoken = document.getElementById('csrf-token').content;
    var container = document.getElementById('container');
    var recordAudio, recordVideo;
    var maxcount = 20;
    var counter;

    $('#submit').hide();
    $('#stop').hide();
    $('#stopV').hide();

    function PostBlob(audioBlob, videoBlob, fileName) {
        var formData = new FormData();
        formData.append('_token', csrftoken);
        formData.append('filename', fileName);
        formData.append('old_filename', old_filename);
        formData.append('audio-blob', audioBlob);
        formData.append('video-blob', videoBlob);
        xhr('/upload_video_kyc', formData, function(ffmpeg_output) {
            record.innerHTML = 'Retake';
            preview.play();
            preview.muted = !1
        })
    }

    $('#stopV').on('click',function(){
        // stop.click();
         $('#stopV').hide();
        $('#stop').show();
        $('#submit').show();
        record.disabled = !1;
        stop.disabled = !1;
        submit.disabled = !1;
        preview.src = '';
        preview.poster = 'icons/ajax_loader.gif';
        old_filename = fileName;
        fileName = Math.round(Math.random() * 99999999) + 99999999;
        recordAudio.stopRecording(function() {
            recordVideo.stopRecording(function() {
                PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
            })
        })
    });

    record.onclick = function() {
        maxcount = 20;
        $('#timer').show();
        $('#stop').hide();
        $('#submit').hide();
        $('#stopV').show();
        record.innerHTML = 'Recording...';
        record.disabled = !0;
        submit.disabled = !0;
        !window.stream && navigator.getUserMedia({
            audio: !0,
            video: !0,
        }, function(stream) {
            window.stream = stream;
            onstream()
        }, function(error) {});
        window.stream && onstream();

        function onstream() {
            preview.src = window.URL.createObjectURL(stream);
            preview.play();
            preview.muted = !0;
            recordAudio = RecordRTC(stream, {
                type: 'audio',
                recorderType: StereoAudioRecorder,
                bufferSize: 16384,
                onAudioProcessStarted: function() {
                    recordVideo.startRecording()
                }
            });
            var videoOnlyStream = new MediaStream();
            videoOnlyStream.addTrack(stream.getVideoTracks()[0]);
            recordVideo = RecordRTC(videoOnlyStream, {
                type: 'video',
            });
            recordAudio.startRecording();
            stop.disabled = !1
        }
        var maxvideolengthtime = 20000;
        counter=setInterval(timer, 1000);
        setTimeout(function() {
            if (record.disabled) {
                $('#stopV').click()
            }
        }, maxvideolengthtime)
    };
    var fileName = '';
    stop.onclick = function() {
       preview.play();
    };
    
    function timer()
    {
      maxcount=maxcount-1;
      if (maxcount <= 0)
      {
         clearInterval(counter);
         $('#timer').hide();
         return;
      }
      if (maxcount < 10) {
         $('#timer').text("00:0"+maxcount); 
      }else{
         $('#timer').text("00:"+maxcount); 
      }
    }
    function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                var response = JSON.parse(request.responseText);
                if (response.msg == 'success') {
                    preview.src = response.url;
                    callback(response.response);
                } else {
                    $('#great').html('Error !');
                    $('#you-have').html(response.errors);
                    $('#notify-done').attr('onclick', "javascript:location.href='#'");
                    $('#ekycvideostatusModal').modal('show');
                }
            }
        };
        request.open('POST', url);
        request.send(data)
    }
    submit.onclick = function() {
        var request = new XMLHttpRequest();
        var formData = new FormData();
        formData.append('_token', csrftoken);
        formData.append('filename', fileName);
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                var response = JSON.parse(request.responseText);
                handleResponse(response)
            }
        };
        request.open('POST', '/submit_video_kyc');
        request.send(formData);
    }


    function handleResponse(data){
            console.log("inside Handle Response");
            $('#modal-body').empty();
            console.log(data);
            if (data.msg == "success") {
                $('#modal-header').text('Great!');
                 $('#modal-body').append(
                    '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                    '<p class="text-center general-info">'+data.response+'</span><p>'+
                    '<input type = "button" class = "btn btn-primary center-block" onclick = "redirectHome()" value = "okay">');

                 $('#infoModal').modal('show');
            }
            if (data.msg == "failure") {
                $('#modal-header').text('Oops!');
                 $('#modal-body').append(
                    '<img class = "center-block modal-img" src = "icons/failed-tick.png">'+
                    '<p class="text-center general-info">'+data.response+'<p>'+
                    '<input type = "button" class = "btn btn-primary center-block" onclick = "redirectHome()" value = "okay">');

                 $('#infoModal').modal('show');

            }
    }

});