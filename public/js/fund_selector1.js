$(document).ready(function() {


    var graph_scheme_code;
    var graph_duration;

    $('#equity-chart').defaultDonut();
    $('#debt-chart').defaultDonut();
    $('#gold-chart').defaultDonut();
    $('#elss-chart').defaultDonut();
    $('#onetime-date,#schedule-date,#monthly-date').hide();
    $(document).ajaxStart(function() {
        $("#ajax-loader").css("display", "block")
    });
    $(document).ajaxComplete(function() {
        $("#ajax-loader").css("display", "none")
    });
    $('.inv-amount').keyup(function(e) {
        $('#total-amt').text('');
        $('#bal-body,#equity-body,#debt-body,#elss-body').text('');
        var inv_amount = 0;
        if ($('#inv-amount').val() != '') {
            inv_amount = parseInt($('#inv-amount').val())
        }
        var balance = 0;
        balance = inv_amount % 1000;
        if (inv_amount < 10000000) {
            if (balance == 0) {
                if ($('#monthly').is(":checked")) {
                    if (inv_amount >= 1000) {
                        $('#min-val').css({
                            'color': '#797979'
                        });
                        var p_type = '';
                        if (inv_amount >= 1000) {
                            $('#total-amt').text($(this).val());
                            showFunds(p_type)
                        }
                        $('#equity-sm,#debt-sm,#hybrid-sm,#elss-sm').click()
                    } else {
                        var p_type = $('#port_type').val();
                        $('#min-val').css({
                            'color': 'red'
                        });
                        if (inv_amount >= 1000) {
                            $('#total-amt').text($(this).val());
                            showFunds(p_type)
                        }
                        $('#equity-sm,#debt-sm,#hybrid-sm,#elss-sm').click()
                    }
                } else {
                    var p_type = $('#port_type').val();
                    if (inv_amount >= 1000) {
                        $('#total-amt').text($(this).val());
                        showFunds(p_type)
                    }
                    $('#equity-sm,#debt-sm,#hybrid-sm,#elss-sm').click()
                }
            }
        } else {
            if (e.keyCode != 8) {
                e.preventDefault()
            }
        }
    });
    $('#inv-amount').on('blur', function(e) {
        var p_type = $('#port_type').val()
        var inv_amount = 0;
        if ($('#inv-amount').val() != '') {
            var amount = parseInt($('#inv-amount').val());
            amount = Math.round(amount / 1000);
            inv_amount = amount * 1000;
            $('#inv-amount').val(inv_amount)
        }
        showFunds(p_type)
    });
    $('#inv-amount').on('keyup', function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return !1
        }
    });
    $('#onetime, #future, #monthly').on('change', function() {
        var inv_type = $('input[name="inv_type"]:checked').val();
        var p_type = $('#port_type').val();
        if (p_type === "undefined") {
            p_type = ''
        } else {}
        if ($('#onetime').is(":checked")) {
            $('#min-val').text('5000');
            $('#one-sel-date').css({
                'display': 'block'
            });
            $('#sche-sel-date,#monthly-date').css({
                'display': 'none'
            });
            $('.radio').find('#one_img').attr('src', 'icons/r_en.png');
            $('.radio').find('#future_img,#monthly_img').attr('src', 'icons/r_dis.png');
            $('#one-sel-date,#month-start-date,#month-end-date,#sche-sel-date').prop('disabled', !0);
            $('#one-sel-date').css('cursor', 'pointer');
            $('#sche-sel-date').css('cursor', 'default');
            $('#month-start-date,#month-end-date').css('cursor', 'default');
            $.ajax({
                type: "GET",
                url: "/get_onetime_investment_date",
                cache: !1,
                processData: !1,
                beforeSend: function() {},
                success: function(data) {
                    $('#one-sel-date').val('');
                    $('#one-sel-date').val(data.response);
                    $('#month-start-date,#month-end-date,#sche-sel-date').val('')
                },
                error: function(xhr, status, error) {},
            })
        } else if ($('#future').is(":checked")) {
            $('#min-val').text('5000');
            $('#sche-sel-date').css({
                'display': 'block'
            });
            $('#one-sel-date,#monthly-date,#sip-date,#to,#sip-duration').css({
                'display': 'none'
            });

            $('.radio').find('#one_img,#monthly_img').attr('src', 'icons/r_dis.png');
            $('.radio').find('#future_img').attr('src', 'icons/r_en.png');
            $('#one-sel-date,#month-start-date,#month-end-date').prop('disabled', !0);
            $('#sche-sel-date').prop('disabled', !1);
            $('#month-start-date,#month-end-date').val('');
            $('#one-sel-date').css('cursor', 'default');
            $('#sche-sel-date').css('cursor', 'pointer');
            $('#one-sel-date,#month-start-date,#month-end-date').val('');
            $('#sche-sel-date').datepicker('show')
        } else if ($('#monthly').is(":checked")) {
            $('#min-val').text('Min : 1000');
            if ($(this).val() >= 1000) {
                $('#min-val').css({
                    'color': '#797979'
                });
                var p_type = ''
            }
            $('#sip-date,#to,#sip-duration').css({
                'display': 'inline'
            });
            $('#sche-sel-date,#one-sel-date').css({
                'display': 'none'
            });
            $('.radio').find('#one_img,#future_img').attr('src', 'icons/r_dis.png');
            $('.radio').find('#monthly_img').attr('src', 'icons/r_en.png');
            $('#month-start-date,#month-end-date').prop('disabled', !1);
            $('#one-sel-date,#sche-sel-date').prop('disabled', !0);
            $('#one-sel-date,#sche-sel-date').css('cursor', 'default');
            $('#month-start-date,#month-end-date').css('cursor', 'pointer');
            $('#one-sel-date,#sche-sel-date').val('');
            $('#month-start-date').datepicker('show')
        }
        if (p_type == 'own') {
            showCustomFunds(p_type)
        } else {
            showFunds(p_type)
        }
    });


    $('#email,#phone').on('change', function() {
        if ($('#email').is(":checked")) {
            $('.notify-radio').find('#email-img').attr('src', 'icons/r_en.png');
            $('.notify-radio').find('#phone-img').attr('src', 'icons/r_dis.png')
        }
        if ($('#phone').is(":checked")) {
            $('.notify-radio').find('#phone-img').attr('src', 'icons/r_en.png');
            $('.notify-radio').find('#email-img').attr('src', 'icons/r_dis.png')
        }
    });
    $(function() {
        if ($('#radio').is(":checked", !0)) {
            $('.checkbox-div input[type="checkbox"]').prop('checked', !0)
        }
    })
    $('#onetime-inv-icon,#fut-icon,#monthly-icon').on('click', function() {
        $('#invDetailsModal').modal('show');
        $('.navbar').css({
            'z-index': '0'
        })
    });
    $('.cancel-inv-link').on('click', function() {
        $('#cancelInvModal').modal('show');
        $('#cancelInvModal').on('shown.bs.modal', function() {
            $('#invDetailsModal').css({
                'opacity': '0'
            })
        });
        $('#cancelInvModal').on('hidden.bs.modal', function() {
            $('#invDetailsModal').css({
                'opacity': '1'
            })
        })
    });
    $('#sche-sel-date').datepicker({
        inline: !0,
        showOtherMonths: !0,
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dateFormat: "dd-mm-yy",
        minDate: "+6d",
        changeMonth: !0,
        changeYear: !0,
        beforeShow: function() {
            setTimeout(function() {
                $('.ui-datepicker').css('z-index', 99999999999999);
                $('.ui-datepicker').css('padding', 0)
            }, 0)
        }
    });
    $('#sip-date,#month-end-date').datepicker({
        inline: !0,
        showOtherMonths: !0,
        changeMonth: !1,
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dateFormat: "dd",
        beforeShow: function() {
            setTimeout(function() {
                $('.ui-datepicker').css('z-index', 99999999999999);
                $('.ui-datepicker').css('padding', 0)
                $('.ui-datepicker-year,.ui-datepicker-month').css({
                    'display': 'none'
                })
            }, 0)
        },
        beforeShowDay: function(date) {
            if (date.getDate() == 29 || date.getDate() == 30 || date.getDate() == 31) {
                return [!1, '', 'Unavailable']
            }
            return [!0, '']
        },
    });
    var checkboxes = $('.checkbox-div').find('input[type=checkbox]');
    var checkboximg = $('.checkbox-div').find('img');
    $('input[name="port_type"]').on('change', function() {
        var parentDiv = $(this).parents(".checkbox-div:eq(0)");
        if ($(this).is(':checked') && $(this).attr('id') == 'own') {
            checkboxes.prop('checked', !1).prop('disabled', !1);
            checkboximg.attr("src", "icons/check_dis.png")
        }
        if (($(this).is(':checked')) && (($(this).attr('id') == 'low-risk') || ($(this).attr('id') == 'moderate-risk') || ($(this).attr('id') == 'high-risk'))) {
            checkboxes.prop('disabled', !0);
            checkboximg.attr("src", "icons/check_dis.png")
        }
    });
    $('.checkbox-div input').on('change', function() {
        var parentDiv = $(this).parents(".checkbox-div:eq(0)");
        if ($(this).is(":checked")) {
            $(parentDiv).find("img").attr("src", "icons/check_en.png")
        } else {
            $(parentDiv).find("img").attr("src", "icons/check_dis.png")
        }
    });
    $('#wd-maintain, #wd-now').on('change', function() {
        if ($('#wd-maintain').is(":checked")) {
            $('.withdraw-radio-div').find('#wd-maintain-img').attr('src', 'icons/r_en.png');
            $('.withdraw-radio-div').find('#wd-now-img').attr('src', 'icons/r_dis.png');
            $('.table-group').css({
                'opacity': '0.5',
                'pointer-events': 'none'
            });
            var checkboxes = $('.checkbox-div').find('input[type=checkbox]');
            var checkboximg = $('.checkbox-div').find('img');
            checkboxes.prop('checked', !1);
            checkboximg.attr("src", "icons/check_dis.png");
            $('.fund-amount').children().remove();
            $('.fund-amount').append('<p class = "fund-amount-inner">Rs. 50,00,000</p>')
        }
        if ($('#wd-now').is(":checked")) {
            $('.withdraw-radio-div').find('#wd-maintain-img').attr('src', 'icons/r_dis.png');
            $('.withdraw-radio-div').find('#wd-now-img').attr('src', 'icons/r_en.png');
            $('.table-group').css({
                'opacity': '1',
                'pointer-events': 'auto'
            });
            $('.fund-amount').children().remove();
            $('.fund-amount').append('<input type="number" class="custom-amount" id="" placeholder = "Rs." >')
        }
    });


    function showFunds(p_type, e) {
        var inv_type = '';
        var inv_amount = '';
        if ($('#inv-amount').val() != '') {
            inv_amount = parseInt($('#inv-amount').val())
        }
        if ($('input[name="inv_type"]').is(':checked')) {
            inv_type = $('input[name="inv_type"]:checked').val()
        }
        if (p_type == '' || inv_amount == '') {
            $('.empty-sugg').hide();
            $('#bal-body,#equity-body,#debt-body,#elss-body').empty().append('<p class="text-center empty-sugg empty-alert">After entering the amount and select the portfolio type</p>')
        } else {
            var balance_amount = 0;
            var p_type = p_type;
            var dataString = 'p_type=' + p_type + '&inv_amount=' + inv_amount + '&inv_type=' + inv_type;
            console.log(dataString);
            $.ajax({
                type: "POST",
                url: "/showFunds",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                    console.log(data);
                    suggestFunds(data)
                },
                error: function(xhr, status, error) {},
            })
        }
    }

    function suggestFunds(data) {
        $response = data.response;
        if (data.msg == "min_5000") {
            $('#equity-panel,#debt-panel,#bal-panel').show();
            $val = $("#port_type").val();
            if ($val == 'moderate-risk') {
                $('#debt-panel,#equity-panel,#elss-panel').css({
                    'display': 'none'
                });
                $('#bal-panel').css({
                    'display': 'block'
                });
                $('#bal-body').empty().append('<p class="text-center empty-sugg empty-alert">Minimum Amount for this portfolio must be 5000.</p>');
                $('#hybrid-sm').click()
            } else if ($val == 'high-risk') {
                $('#debt-panel,#bal-panel,#elss-panel').css({
                    'display': 'none'
                });
                $('#equity-panel').css({
                    'display': 'block'
                });
                $('#equity-body').empty().append('<p class="text-center empty-sugg empty-alert">Minimum Amount for this portfolio must be 5000.</p>');
                $('#equity-sm').click()
            } else if ($val == 'low-risk') {
                $('#equity-panel,#bal-panel,#elss-panel').css({
                    'display': 'none'
                });
                $('#debt-panel').css({
                    'display': 'block'
                });
                $('#debt-body').empty().append('<p class="text-center empty-sugg empty-alert">Minimum Amount for this portfolio must be 5000.</p>');
                $('#debt-sm').click()
            } else if ($val == 'elss') {
                $('#equity-panel,#bal-panel,#debt-panel').css({
                    'display': 'none'
                });
                $('#elss-panel').css({
                    'display': 'block'
                });
                $('#elss-body').empty().append('<p class="text-center empty-sugg empty-alert">Minimum Amount for this portfolio must be 5000.</p>');
                $('#elss-sm').click()
            }
        } else if (data.msg == "debt") {
            var body = $('#debt-details-container');
            var panel_hide = $('#equity-panel,#bal-panel,#elss-panel');
            var sm = $('#debt-sm');
            $('#equity-panel,#debt-panel,#bal-panel,#elss-panel').show();
            fundAppend(body, panel_hide, sm);
            sm.click()
        } else if (data.msg == "bal") {
            var body = $('#bal-details-container');
            var panel_hide = $('#equity-panel,#debt-panel,#elss-panel');
            var sm = $('#hybrid-sm');
            $('#equity-panel,#debt-panel,#bal-panel,#elss-panel').show();
            fundAppend(body, panel_hide, sm);
            sm.click()
        } else if (data.msg == "eq") {
            var body = $('#eq-details-container');
            var panel_hide = $('#bal-panel,#debt-panel,#elss-panel');
            var sm = $('#equity-sm');
            $('#equity-panel,#debt-panel,#bal-panel,#elss-panel').show();
            fundAppend(body, panel_hide, sm);
            sm.click()
        } else if (data.msg == "elss") {
            var body = $('#ts-details-container');
            var panel_hide = $('#bal-panel,#debt-panel,#equity-panel');
            var sm = $('#elss-sm');
            $('#equity-panel,#debt-panel,#bal-panel,#elss-panel').show();
            fundAppend(body, panel_hide, sm);
            sm.click()
        }
    }

    function fundAppend(body, panel_hide, sm) {
        var body = body;
        var panel_hide = panel_hide;
        var sm = sm;
        body.empty();
        panel_hide.hide();
        $('.empty-sugg').remove();
        var total_amount = 0;
        var fund_title = '';


        fund_title = body.data('cname');
        console.log(fund_title);
        switch(fund_title){

            case 'equity':
              fund_title = '';
              $('.equity-container').show();
              $('.debt-container,.bal-container,.taxsaver-container').hide();
              break;

            case 'debt':

              fund_title = '';
              $('.debt-container').show();
              $('.equity-container,.bal-container,.taxsaver-container').hide();
              break;

            case 'bal':
              fund_title = '';
              $('.bal-container').show();
              $('.debt-container,.equity-container,.taxsaver-container').hide();
              break;

            case 'taxsaver':
              fund_title = '';
              $('.taxsaver-container').show();
              $('.debt-container,.bal-container,.equity-container').hide();
              break;
        }

        

        /*body.append('<div class="fund-title-container">'+
        '<p class="fund-title">'+fund_title+'</p>'+
      '</div>');*/

        $.each(JSON.parse($response), function(key, value) {
            total_amount += value.amount;

           /* body.append(
                '<div id="'+value.scheme_type'">');*/
            body.append(

      
      '<div id="'+value.scheme_code+'" class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">'+
          '<div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >'+
             '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">'+
               '<i class="material-icons arrow-down" data-toggle = "collapse" href="#cont-'+value.scheme_code+'" data-schemecode = '+value.scheme_code+' >keyboard_arrow_down</i>'+
             '</div>'+
             '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">'+
               '<p class = "span-fund-name">'+value.scheme_name+'</p>'+
               '<p class = "currently-inv"> Currently invested <span class = "curr-span-amt">Rs. 50,000</span></p>'+
             '</div>'+
             '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">'+
               '<p class = "fund-amount-inner">Rs.'+value.amount+'</p>'+
             '</div>'+
          '</div>'+
      '</div>'
      );





            $(document).find('#'+value.scheme_code).append(
                
                '<div id="cont-'+value.scheme_code+'" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">'+

             '<div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">'+

                 '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Fund Manager</p>'+
                     '<p class="fund-details-content" id="fund-manager-'+value.scheme_code+'"></p>'+
                   '</div>'+

                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Benchmark</p>'+
                     '<p class="fund-details-content" id="fund-benchmark-'+value.scheme_code+'"></p>'+
                   '</div>'+
                 '</div>'+

                  '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Launch</p>'+
                     '<p class="fund-details-content" id="fund-launch-'+value.scheme_code+'"></p>'+
                   '</div>'+

                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Investment</p>'+
                     '<p class="fund-details-content" id="fund-growth-'+value.scheme_code+'"></p>'+
                   '</div>'+
                  '</div>'+


                 '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Exit Load</p>'+
                     '<p class="fund-details-content" id="fund-exit-'+value.scheme_code+'"></p>'+
                   '</div>'+

                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Asset Size</p>'+
                     '<p class="fund-details-content" id="fund-asset-'+value.scheme_code+'"></p>'+
                   '</div>'+
                 '</div>'+
             '</div> <!-- Fund Detail container closes-->'+

             '<div class="col-lg-12 col-md-12 col-sm-12">'+
               '<p id="nav_container"><span>NAV - </span><span id="scheme_nav_'+value.scheme_code+'"></span></p>'+
             '</div>'+

             '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                 '<p class="graph-duration text-center active-duration border-right" data-scode = "'+value.scheme_code+'">3 Months</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                 '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">6 Months</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                 '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">1 Year</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                 '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">3 Years</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                 '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">5 Years</p>'+
               '</div>'+
             '</div>'+

            

             '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">'+
               '<div id="'+value.scheme_code+'-graph-container">'+
                 
               '</div>'+
             '</div>'+

             '<div class="col-lg-12 col-md-12 col-sm-12 border-top">'+
               '<p id="fund-ret-header">Fund Returns</span></p>'+
            ' </div>'+

             '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">'+
               '<div id="border-blah">'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">1 Day</p>'+
                   '<p class="return-perc neg-ret" id="one_day'+value.scheme_code+'"></p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">5 Day</p>'+
                   '<p class="return-perc pos-ret" id="five_day'+value.scheme_code+'"></p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">1 Month</p>'+
                   '<p class="return-perc pos-ret" id="one_month'+value.scheme_code+'"></p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">3 Months</p>'+
                   '<p class="return-perc pos-ret" id="three_month'+value.scheme_code+'"></p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">1 year</p>'+
                   '<p class="return-perc pos-ret" id="one_year'+value.scheme_code+'"></p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">5 Years</p>'+
                   '<p class="return-perc pos-ret" id="five_year'+value.scheme_code+'"></p>'+
                 '</div>'+
               '</div>'+
             '</div>'+
         '</div>');

        });



        
        sm.click();
        $('#total-amt').text(total_amount)
    }
    $('#fund_sel_form').on('submit', function(e) {
        console.log('hello');
        e.preventDefault();
        var portfolio = $("#port_type").val();
        if (portfolio == "Conservative" || portfolio == "Moderate" || portfolio == "Aggressive" || portfolio == "elss") {
            if ($('#inv-amount').val() === '') {
                e.preventDefault()
            } else if (!$("input[name='inv_type']:checked").val()) {
                e.preventDefault()
            } else if (!$("input[name='port_type']:checked").val()) {
                e.preventDefault()
            } else if ($('#monthly').is(':checked')) {
                $('#month-start-date,#month-end-date').prop('required', !0);
                e.preventDefault()
            } else if ($('#future').is(':checked')) {
                $('#sche-sel-date').prop('required', !0);
                e.preventDefault()
            } else if ($('#onetime').is(':checked')) {
                $('#one-sel-date').prop('required', !0);
                e.preventDefault()
            }
            investNow()
        }
        if (portfolio == 'own') {
            e.preventDefault();
            customInvestNow()
        }
    });

    function investNow() {
        var inv_amount = $('#inv-amount').val();
        var inv_type = '';
        if ($("input[name='inv_type']").is(":checked")) {
            inv_type = $("input[name='inv_type']:checked").val()
        }
        var port_type = $("#port_type").val();
        if (port_type == 'own') {
            port_type = 'custom'
        }
        var inv_date = '';
        if ($('#monthly').is(':checked')) {
            inv_date = $('#sip-date').val() + "/" + $('#sip-duration').val()
        } else if ($('#future').is(':checked')) {
            inv_date = $('#sche-sel-date').val()
        } else if ($('#onetime').is(':checked')) {
            inv_date = $('#one-sel-date').val()
        }
        var dataString = 'port_type=' + port_type + '&inv_type=' + inv_type + '&inv_date=' + inv_date + '&inv_amount=' + inv_amount;
        var portfolio_type = '';
        if ($('#own').is(":checked")) {
            portfolio_type = 'Custom'
        } else if ($('#low-risk').is(":checked")) {
            portfolio_type = 'Conservative'
        } else if ($('#moderate-risk').is(":checked")) {
            portfolio_type = 'Moderate'
        } else if ($('#high-risk').is(":checked")) {
            portfolio_type = 'Aggressive'
        } else if ($('#elss').is(":checked")) {
            portfolio_type = 'ELSS'
        }
        balance_amount = parseInt(inv_amount) % 1000;
        if (balance_amount == 0 && inv_amount < 10000001) {
            $.ajax({
                type: "POST",
                url: "/investFunds",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: dataString,
                cache: !1,
                processData: !1,
                beforeSend: function() {},
                success: function(data) {
                    if (data.msg == 'success') {
                        $('#great').html('Great !');
                        $('#notify-opt-div').css({
                            'display': 'block'
                        });
                        $('#you-have').html(data.status + 'of <span class="blue">Rs. ' + inv_amount + '</span> in <span class="blue">' + portfolio_type + '</span> portfolio on <span class="blue">' + data.date + '</span>');
                        $('#investment_id').val(data.id);
                        $('#scheduleInvModal').modal('show')
                    } else {
                        $('#notify-opt-div').css({
                            'display': 'none'
                        });
                        $('#great').html('Error !');
                        var error_response = '';
                        if (data.hasOwnProperty('status')) {
                            error_response += data.status + '<br>'
                        } else {
                            if (data.errors.hasOwnProperty('inv_amount')) {
                                error_response += data.errors.inv_amount + '<br>'
                            }
                            if (data.errors.hasOwnProperty('inv_date')) {
                                error_response += data.errors.inv_date + '<br>'
                            }
                            if (data.errors.hasOwnProperty('inv_type')) {
                                error_response += data.errors.inv_type + '<br>'
                            }
                            if (data.errors.hasOwnProperty('port_type')) {
                                error_response += data.errors.port_type + '<br>'
                            }
                        }
                        $('#you-have').html(error_response);
                        $('#notify-opt-div').css({
                            'display': 'none'
                        });
                        $('#notify-done').attr('onclick', "javascript:location.href='#'");
                        $('#scheduleInvModal').modal('show')
                    }
                },
                error: function(xhr, status, error) {
                    $('#great').html('Error !');
                    $('#you-have').html('Investment Failed. Please check and try again');
                    $('#notify-opt-div').css({
                        'display': 'none'
                    });
                    $('#scheduleInvModal').modal('show')
                },
            })
        } else {
            if (balance_amount > 0) {
                $('#great').html('Error !');
                $('#equity-body, #debt-body, #bal-body, #elss-body').html('');
                $('#you-have').html('Investment amount should in multiples of thousands.');
                $('#notify-opt-div').css({
                    'display': 'none'
                });
                $('#scheduleInvModal').modal('show')
            } else if (inv_amount > 10000000) {
                $('#great').html('Error !');
                $('#equity-body, #debt-body, #bal-body, #elss-body').html('');
                $('#you-have').html('You can invest a maximum of 10000000 at a time.');
                $('#notify-opt-div').css({
                    'display': 'none'
                });
                $('#scheduleInvModal').modal('show')
            }
        }
    }

    function customInvestNow() {
        var code_amount = [];
        var myString = '';
        var scheme_type;
        var inv_date = '';
        if ($('#monthly').is(':checked')) {
            inv_date = $('#sip-date').val() + "/" + $('#sip-duration').val()
        } else if ($('#future').is(':checked')) {
            inv_date = $('#sche-sel-date').val()
        } else if ($('#onetime').is(':checked')) {
            inv_date = $('#one-sel-date').val()
        }
        $('.custom-amount').each(function() {
            var amount = $(this).val();
            var code = $(this).attr('id');
            var scheme_type = $(this).data('scheme');
            if (amount == 0) {
                code_amount.push({
                    'code': code,
                    'amount': 0,
                    'scheme': scheme_type
                })
            } else {
                code_amount.push({
                    'code': code,
                    'amount': amount,
                    'scheme': scheme_type
                })
            }
            myString = JSON.stringify(code_amount)
        });
        var inv_type = '';
        if ($('input[name="inv_type"]').is(':checked')) {
            inv_type = $('input[name="inv_type"]:checked').val()
        }
        balance_amount = parseInt($('#total-amt').html()) % 1000;
        if (inv_type != '' && inv_date != '' && balance_amount == 0) {
            var formData = new FormData();
            formData.append('code_amount', myString);
            formData.append('inv_type', inv_type);
            formData.append('inv_date', inv_date);
            $.ajax({
                type: "POST",
                url: "/investCustomFunds",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: formData,
                cache: !1,
                contentType: !1,
                processData: !1,
                beforeSend: function() {},
                success: function(data) {
                    investCustomFunds(data)
                },
                error: function(xhr, status, error) {},
            })
        } else {
            var response = '';
            if (inv_date == '') {
                response += 'Investment date is required' + '<br>'
            }
            if (inv_type == '') {
                response += 'Investment type is required' + '<br>'
            }
            if ($('#total-amt').html() == 0) {
                response += 'Investment amount is required' + '<br>'
            }
            if (balance_amount != 0) {
                response += 'Investment amount should in multiples of thousands.' + '<br>'
            }
            $('#great').html('Error !');
            $('#you-have').html(response);
            $('#notify-done').attr('onclick', "javascript:location.href='#'");
            $('#notify-opt-div').css({
                'display': 'none'
            });
            $('#scheduleInvModal').modal('show')
        }
    }

    function showCustomFunds(p_type, e) {
        var inv_type = '';
        inv_type = $("input[name='inv_type']:checked").val()
        var p_type = p_type;
        var dataString = 'p_type=' + p_type + '&inv_type=' + inv_type;
        $.ajax({
            type: "POST",
            url: "/showCustomFunds",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            data: dataString,
            beforeSend: function() {},
            success: function(data) {
                console.log(data);
                suggestCustomFunds(data)
            },
            error: function(xhr, status, error) {},
        })
    }

    function suggestCustomFunds(data) {
        $('#inv-amount').val();
        $('#total-amt').html(0);
        if (data.msg == 'custom') {
            var response = data.response;
            var debt_body = $('#debt-details-container');
            var bal_body = $('#bal-details-container');
            var equity_body = $('#eq-details-container');
            var elss_body = $('#ts-details-container');
            //$('#debt-body,#bal-body,#equity-body,#elss-body').empty();
            $('#ts-details-container,#bal-details-container,#eq-details-container,#debt-details-container').empty();
            $('.debt-container,.bal-container,.taxsaver-container,.equity-container').show();
            $.each(JSON.parse(response), function(key, value) {
                var suggestions = '<tr>' + '<td class = "fund-name">' + '<div class = "col-xs-11 col-scheme-offset fund-name-div"><span class = "span-fund-name" id = "' + value.scheme_code + '">' + value.scheme_name + '</span>' + '<p class = "currently-inv"> Minimum Investment Amount - Rs. <span class = "curr-span-amt">' + value.minimum_amount + '</span></p></div></td>' + '<td class = "fund-amount"><input type="number" name="" class="custom-amount" data-scheme = ' + value.scheme_type + ' id=' + value.scheme_code + ' placeholder="Rs."></td>' + '</tr>';
                var suggestions_head = 

                '<div id="'+value.scheme_code+'" class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">'+
                  '<div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >'+
                     '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">'+
                       '<i class="material-icons arrow-down" data-toggle = "collapse" href="#cont-'+value.scheme_code+'" data-schemecode = '+value.scheme_code+' >keyboard_arrow_down</i>'+
                     '</div>'+
                     '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">'+
                       '<p class = "span-fund-name">'+value.scheme_name+'</p>'+
                       '<p class = "currently-inv"> Currently invested <span class = "curr-span-amt">Rs. 50,000</span></p>'+
                     '</div>'+
                     '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">'+
                       //'<p class = "fund-amount-inner">Rs.'+value.amount+'</p>'+
                       '<input type="number" name="" class="custom-amount" data-scheme = ' + value.scheme_type + ' id=' + value.scheme_code + ' placeholder="Rs.">'+
                     '</div>'+
                  '</div>'+
              '</div>';


              console.log(value.scheme_code);


              //$(document).find('#'+value.scheme_code).append('hello');



                if (value.scheme_type == "debt") {
                    debt_body.append(suggestions_head)
                } else if (value.scheme_type == "bal") {
                    bal_body.append(suggestions_head)
                } else if (value.scheme_type == "eq") {
                    equity_body.append(suggestions_head)
                } else if (value.scheme_type == "ts") {
                    elss_body.append(suggestions_head)
                }
                
            });


            $.each(JSON.parse(response),function(key,value){
                $(document).find('#'+value.scheme_code).append(
                
                    '<div id="cont-'+value.scheme_code+'" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">'+

                         '<div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">'+

                             '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                               '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                                 '<p class="fund-details-heading">Fund Manager</p>'+
                                 '<p class="fund-details-content" id="fund-manager-'+value.scheme_code+'"></p>'+
                               '</div>'+

                               '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                                 '<p class="fund-details-heading">Benchmark</p>'+
                                 '<p class="fund-details-content" id="fund-benchmark-'+value.scheme_code+'"></p>'+
                               '</div>'+
                             '</div>'+

                              '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                               '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                                 '<p class="fund-details-heading">Launch</p>'+
                                 '<p class="fund-details-content" id="fund-launch-'+value.scheme_code+'"></p>'+
                               '</div>'+

                               '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                                 '<p class="fund-details-heading">Investment</p>'+
                                 '<p class="fund-details-content" id="fund-growth-'+value.scheme_code+'"></p>'+
                               '</div>'+
                              '</div>'+


                             '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                               '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                                 '<p class="fund-details-heading">Exit Load</p>'+
                                 '<p class="fund-details-content" id="fund-exit-'+value.scheme_code+'"></p>'+
                               '</div>'+

                               '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                                 '<p class="fund-details-heading">Asset Size</p>'+
                                 '<p class="fund-details-content" id="fund-asset-'+value.scheme_code+'"></p>'+
                               '</div>'+
                             '</div>'+
                         '</div> <!-- Fund Detail container closes-->'+

                         '<div class="col-lg-12 col-md-12 col-sm-12">'+
                           '<p id="nav_container"><span>NAV - </span><span id="scheme_nav_'+value.scheme_code+'"></span></p>'+
                         '</div>'+

                         '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">'+
                           '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                             '<p class="graph-duration text-center active-duration border-right" data-scode = "'+value.scheme_code+'">3 Months</p>'+
                           '</div>'+
                           '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                             '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">6 Months</p>'+
                           '</div>'+
                           '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                             '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">1 Year</p>'+
                           '</div>'+
                           '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                             '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">3 Years</p>'+
                           '</div>'+
                           '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">'+
                             '<p class="graph-duration text-center border-right" data-scode = "'+value.scheme_code+'">5 Years</p>'+
                           '</div>'+
                         '</div>'+

                        

                         '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">'+
                           '<div id="'+value.scheme_code+'-graph-container">'+
                             
                           '</div>'+
                         '</div>'+

                         '<div class="col-lg-12 col-md-12 col-sm-12 border-top">'+
                           '<p id="fund-ret-header">Fund Returns</span></p>'+
                        ' </div>'+

                         '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">'+
                           '<div id="border-blah">'+
                             '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                               '<p class="graph-duration text-center">1 Day</p>'+
                               '<p class="return-perc neg-ret" id="one_day'+value.scheme_code+'"></p>'+
                             '</div>'+
                             '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                               '<p class="graph-duration text-center">5 Day</p>'+
                               '<p class="return-perc pos-ret" id="five_day'+value.scheme_code+'"></p>'+
                             '</div>'+
                             '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                               '<p class="graph-duration text-center">1 Month</p>'+
                               '<p class="return-perc pos-ret" id="one_month'+value.scheme_code+'"></p>'+
                             '</div>'+
                             '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                               '<p class="graph-duration text-center">3 Months</p>'+
                               '<p class="return-perc pos-ret" id="three_month'+value.scheme_code+'"></p>'+
                             '</div>'+
                             '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                               '<p class="graph-duration text-center">1 year</p>'+
                               '<p class="return-perc pos-ret" id="one_year'+value.scheme_code+'"></p>'+
                             '</div>'+
                             '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                               '<p class="graph-duration text-center">5 Years</p>'+
                               '<p class="return-perc pos-ret" id="five_year'+value.scheme_code+'"></p>'+
                             '</div>'+
                           '</div>'+
                         '</div>'+



                     '</div>');
            });


            
        }
    }

    function investCustomFunds(data) {
        var portfolio_type = '';
        if ($('#own').is(":checked")) {
            portfolio_type = 'Custom'
        } else if ($('#low-risk').is(":checked")) {
            portfolio_type = 'Conservative'
        } else if ($('#moderate-risk').is(":checked")) {
            portfolio_type = 'Moderate'
        } else if ($('#high-risk').is(":checked")) {
            portfolio_type = 'Aggressive'
        } else if ($('#elss').is(":checked")) {
            portfolio_type = 'ELSS'
        }
        if (data.msg == 'success') {
            $('#great').html('Great !');
            $('#notify-opt-div').css({
                'display': 'block'
            });
            $('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.inv_amount + '</span> in <span class="blue">' + portfolio_type + '</span> portfolio on <span class="blue">' + data.date + '</span>');
            $('#investment_id').val(data.id);
            $('#scheduleInvModal').modal('show')
        } else {
            $('#notify-opt-div').css({
                'display': 'none'
            });
            $('#great').html('Error !');
            if (data.hasOwnProperty('status')) {
                $('#you-have').html(data.status)
            } else {
                var response = '';
                if (data.errors.hasOwnProperty('inv_date')) {
                    response += data.errors.inv_date + '<br>'
                }
                if (data.errors.hasOwnProperty('inv_type')) {
                    response += data.errors.inv_type + '<br>'
                }
                if (data.errors.hasOwnProperty('code_amount')) {
                    response += data.errors.code_amount + '<br>'
                }
                $('#you-have').html(response)
            }
            $('#scheduleInvModal').modal('show')
        }
    }
    $(document).on('keyup', '.custom-amount', function() {
        var total = 0;
        $('.custom-amount').each(function() {
            var enter_amt = $(this).val();
            if (enter_amt.length !== 0) {
                total += parseInt(enter_amt);
                $('#total-amt').text(total)
            }
        })
    });
    $('.i-icon').on('click', function(e) {
        var inv_type = $(this).data('value');
        var dataString = 'inv_type=' + inv_type;
        $.ajax({
            type: "POST",
            url: "/showPendingOrders",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            data: dataString,
            beforeSend: function() {},
            success: function(data) {
                showPendingOrders(data)
            },
            error: function(xhr, status, error) {},
        })
    });

    function showPendingOrders(data) {
        if (data.msg == "success") {
            $('#pending-inv-body').empty();
            if (data.response.length == 0) {
                $('#invDetailsModalBody').children().hide();
                $('#invDetailsModalBody').append('<p class = "text-center" id = "no-inv-text">You have no investments Scheduled in this portfolio !!</p>')
            } else {
                $.each(data.response, function(key, value) {
                    if (value.portfolio_type == 0) {
                        p_type = "Custom"
                    }
                    if (value.portfolio_type == 1) {
                        p_type = "Conservative"
                    }
                    if (value.portfolio_type == 2) {
                        p_type = "Moderate"
                    }
                    if (value.portfolio_type == 3) {
                        p_type = "Aggressive"
                    }
                    $('#invDetailsModalBody').children().show();
                    $('#invDetailsModalBody>p').hide();
                    $('#pending-inv-body').append('<tr>' + '<td><p class="text-center gotham_book">' + value.investment_date + '</p></td>' + '<td><p class="text-center gotham_book table_content_modal">Pending</p></td>' + '<td><p class="text-center gotham_book table_content_modal">' + p_type + '</p></td>' + '<td><p class="text-center gotham_book table_content_modal">Rs.' + value.investment_amount + '</p></td>' + '</tr>')
                })
            }
        }
    }
    $('#notify-done').on('click', function() {
        var notif_mode = '';
        var inv_id = '';
        if ($('input[name=notify-mode]').is(':checked')) {
            notif_mode = $('input[name=notify-mode]:checked').val()
        }
        inv_id = $('#investment_id').val();
        var dataString = 'notif_mode=' + notif_mode + '&id=' + inv_id;
        console.log(dataString);
        if (notif_mode != '' && inv_id != '') {
            $.ajax({
                type: "POST",
                url: "/save_notify_mode",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                success: function(data) {
                    $('#scheduleInvModal').modal('hide');
                    location.href = '/home'
                },
                error: function(xhr, status, error) {},
            })
        } else {
            if (inv_id != '') {
                $('#scheduleInvModal').modal('hide');
                location.href = '/home'
            }
            $('#scheduleInvModal').modal('hide')
        }
    });
    /*$(document).on('click', '.span-fund-name', function() {
        var scheme_id = $(this).attr('id');
        var dataString = 'scheme_code=' + scheme_id;
        $.ajax({
            type: "POST",
            url: "/showSchemeDetails",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            data: dataString,
            beforeSend: function() {},
            success: function(data) {
                showSchemeDetails(data, scheme_id)
            },
            error: function(xhr, status, error) {},
        })
    });*/


    function schemeDetails(schemeCode){
        var dataString = 'scheme_code=' + schemeCode;
        $.ajax({
            type: "POST",
            url: "/showSchemeDetails",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            data: dataString,
            beforeSend: function() {},
            success: function(data) {
                console.log(data);
                showSchemeDetails(data,schemeCode)
            },
            error: function(xhr, status, error) {},
        })
    }

    function showSchemeDetails(data,schemeCode) {
        var response = data;
        var scheme_details;

        var scheme_container = $(document).find('#cont-'+schemeCode);
        if (data.msg == 'success') {


            
            $.each(data.response, function(key, value) {
                var scheme_details = value;

                $(document).find('#fund-manager-'+schemeCode).text(scheme_details.fund_manager);
                $(document).find('#fund-benchmark-'+schemeCode).text(scheme_details.benchmark);
                $(document).find('#fund-launch-'+schemeCode).text(scheme_details.launch_date);
                $(document).find('#fund-growth-'+schemeCode).text(scheme_details.fund_type);
                $(document).find('#fund-exit-'+schemeCode).text(scheme_details.exit_load);
                $(document).find('#fund-asset-'+schemeCode).text(scheme_details.asset_size);



                $(document).find('#one_day'+schemeCode).text(scheme_details.one_day_nav);
                $(document).find('#five_day'+schemeCode).text(scheme_details.five_day_nav);
                $(document).find('#one_month'+schemeCode).text(scheme_details.thirty_day_nav);
                $(document).find('#three_month'+schemeCode).text(scheme_details.ninety_day_nav);
                $(document).find('#one_year'+schemeCode).text(scheme_details.one_year_nav);
                $(document).find('#five_year'+schemeCode).text(scheme_details.five_year_nav);

                $(document).find('#scheme_nav_'+schemeCode).text(scheme_details.current_nav);


                console.log(scheme_details.name);
                console.log(scheme_details.fund_manager);
                /*$('#fund_name').text(scheme_details.name);
                $('#fund_manager').text(scheme_details.fund_manager);
                $('#asset_size').text(scheme_details.asset_size);
                $('#entry_load').text();
                $('#exit_load').text(scheme_details.exit_load);
                $('#benchmark').text(scheme_details.benchmark);
                $('#launch_date').text(scheme_details.launch_date);
                $('#investment_plan').text(scheme_details.investment_plan);
                $('fund_type').text(scheme_details.fund_type);
                //$('#scheme_id').val(scheme_id);
                //$('#sid').attr('href', '/sid/' + scheme_id + '.pdf')*/
            });

            graph_scheme_code = schemeCode;
            graph_duration = 3;

            create_scheme_graph();
            //$('#schemeModal').modal('show')
        } else {}
    }

    function create_scheme_graph() {
        //var scheme_code = schemeCode;
        var a = 2;
        google.charts.load('current', {
            packages: ['corechart', 'line']
        });
        google.charts.setOnLoadCallback(drawCurveTypes)
    }

    function drawCurveTypes() {
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'X');
        data.addColumn('number', 'NAV ');
        var chart_data = [];
        var formdata = 'duration=' + graph_duration + '&scheme_code='+graph_scheme_code;
        console.log(formdata);
        $.ajax({
            type: "POST",
            url: "/get_scheme_performance_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            async: !1,
            data: formdata,
            success: function(response) {
                if (response.msg == 'success') {
                    var id = 1;
                    $.each(response.response, function(key, value) {
                        chart_data.push([new Date(value.year, value.month, value.date), value.amount])
                    })
                }
            },
            error: function(xhr, status, error) {},
        });
        data.addRows(chart_data);
        var options = {
            hAxis: {
                title: 'vertical',
                textPosition: 'none',
            },
            vAxis: {
                title: 'horizontal'
            },
            series: {
                1: {
                    curveType: 'function'
                }
            },
            hAxis: {
                title: 'Duration',
                baselineColor: "transparent",
                gridlines: {
                    color: "transparent"
                },
                ticks: []
            },
            vAxis: {
                title: 'NAV',
                baselineColor: '#d7d7d7'
            },

            height : 400,
            legend: {
                position: "none"
            },
            colors: ["#0091EA"]
        };
        var chart = new google.visualization.LineChart(document.getElementById(''+graph_scheme_code+'-graph-container'));
        chart.draw(data, options)
    }
    $('.duration-time').on('click', function(e) {
        var duration = $(this).attr('data-value');
        $('#duration_time').val(duration);
        create_scheme_graph()
    });
    $con_head = "Conservative!";
    $mod_head = "Moderate!";
    $aggr_head = "Aggressive!";
    $elss_head = "Equity linked savings scheme!";
    $con_text = "A conservative portfolio is best suited for short term investments. The portfolio comprises of debt mutual funds that offer investors steady returns which are unaffected by market volatility. They are usually seen as alternatives to fixed deposits and savings account. Holding investments for three years in a conservative portfolio provides the investor with indexation benefits (reduction of taxable interest to the extent of inflation).";
    $mod_text = "A moderate portfolio comprises of balanced mutual funds that hold roughly 40% debt instruments and the rest equity. The debt holdings yield steady returns while the equity component provides capital appreciation. By holding investments in a moderate portfolio for a period of one year investors are completely exempt of paying tax on the capital gains.";
    $aggr_text = "An aggressive portfolio is best suited for investments with a time horizon of 3 years and above. They comprise of equity mutual funds which provide the investors with capital appreciation. They are subject to market volatility, however have historically provided investors with superior returns when compared to the conservative and moderate portfolios. By holding investments in an aggressive portfolio for a period of one year investors are completely exempt of paying tax on the capital gains.";
    $elss_text = "An Equity Linked Savings Scheme comprises of equity mutual funds that provide an investor with tax benefits. Under section 80C of the Indian Income tax act investors can avail an exemption of up to Rupees One lakh and fifty thousand from their taxable income each year by investing in ELSS. The investments come with a lock in period of three years after which investors may withdraw their investments.";
    $('.port-text').on('click', function() {
        if ($(this).text() == "Conservative") {
            $('#port_modal_name').text($con_head);
            $('#port_explanation').text($con_text);
            $('#helloModal').modal('show')
        }
        if ($(this).text() == "Moderate") {
            $('#port_modal_name').text($mod_head);
            $('#port_explanation').text($mod_text);
            $('#helloModal').modal('show')
        }
        if ($(this).text() == "Aggressive") {
            $('#port_modal_name').text($aggr_head);
            $('#port_explanation').text($aggr_text);
            $('#helloModal').modal('show')
        }
        if ($(this).text() == "ELSS") {
            $('#port_modal_name').text($elss_head);
            $('#port_explanation').text($elss_text);
            $('#helloModal').modal('show')
        }
    })




    /*New Fund Selector */

            $('#port_type').on('change',function(){
                //alert($(this).val());

                if ($(this).val() == "own") {
                    $('#portfolio-heading').text('Custom');
                    $('#recom-spl').text('A conservative portfolio is best suited for short term investments. The portfolio comprises of debt mutual funds that offer investors steady returns which are unaffected by market volatility. They are usually seen as alternatives to fixed deposits and savings account. Holding investments for three years in a conservative portfolio provides the investor with indexation benefits (reduction of taxable interest to the extent of inflation).');
                    console.log("custom Selected");
                    showCustomFunds('own');

                }else{


                    switch($(this).val()){
                    case "Conservative":
                        $('#portfolio-heading').text('Conservative');
                        $('#recom-spl').text('A conservative portfolio is best suited for short term investments. The portfolio comprises of debt mutual funds that offer investors steady returns which are unaffected by market volatility. They are usually seen as alternatives to fixed deposits and savings account. Holding investments for three years in a conservative portfolio provides the investor with indexation benefits (reduction of taxable interest to the extent of inflation).');
                        break;

                    case "Moderate":
                        $('#portfolio-heading').text('Moderate');
                        $('#recom-spl').text('A moderate portfolio comprises of balanced mutual funds that hold roughly 40% debt instruments and the rest equity. The debt holdings yield steady returns while the equity component provides capital appreciation. By holding investments in a moderate portfolio for a period of one year investors are completely exempt of paying tax on the capital gains.');
                    break;


                    case "Aggressive" :
                        $('#portfolio-heading').text('Aggressive');
                        $('#recom-spl').text('An aggressive portfolio is best suited for investments with a time horizon of 3 years and above. They comprise of equity mutual funds which provide the investors with capital appreciation. They are subject to market volatility, however have historically provided investors with superior returns when compared to the conservative and moderate portfolios. By holding investments in an aggressive portfolio for a period of one year investors are completely exempt of paying tax on the capital gains.');
                    break;


                    case "Tax Saver" :
                        $('#portfolio-heading').text('Tax Saver');
                        $('#recom-spl').text('An Equity Linked Savings Scheme comprises of equity mutual funds that provide an investor with tax benefits. Under section 80C of the Indian Income tax act investors can avail an exemption of up to Rupees One lakh and fifty thousand from their taxable income each year by investing in ELSS. The investments come with a lock in period of three years after which investors may withdraw their investments.');               
                    break;

                    }       

                    console.log($(this).val());
                    showFunds($(this).val());
                }



               

            });



            console.log($('#port_type').val());



            $(document).on('click','.arrow-down',function(){


                if ($(this).text() == 'keyboard_arrow_down') {
                    var scheme_code = $(this).data('schemecode');
                    schemeDetails(scheme_code);
                }

            });


            $(document).on('click','.graph-duration',function(){

                $(this).parent().parent().find('p.active-duration').removeClass('active-duration');
                $(this).addClass('active-duration');
                console.log($(this).data('scode'));

                switch($(this).text()){

                    case '3 Month':
                        console.log('3');
                        graph_scheme_code = $(this).data('scode');
                        graph_duration = 3;
                        create_scheme_graph();
                        break;

                    case '6 Months':
                        console.log('6');
                        graph_scheme_code = $(this).data('scode');
                        graph_duration = 6;
                        create_scheme_graph();
                        break;


                    case '1 Year':
                        console.log('12');
                        graph_scheme_code = $(this).data('scode');
                        graph_duration = 12;
                        create_scheme_graph();
                        break;

                    case '3 Years':
                        console.log('12');
                        graph_scheme_code = $(this).data('scode');
                        graph_duration = 36;
                        create_scheme_graph();
                        break;

                    case '5 Years':
                        console.log('12');
                        graph_scheme_code = $(this).data('scode');
                        graph_duration = 60;
                        create_scheme_graph();
                        break;
                }

            });



    /*New Fund Selector Ends*/



   /* $(document).find('#'+value.scheme_code).append(
                
                '<div id="'+value.scheme_code+'" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">'+

             '<div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">'+

                 '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Fund Manager</p>'+
                     '<p class="fund-details-content">Mr. Navaneetha Krishnan</p>'+
                   '</div>'+

                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Benchmark</p>'+
                     '<p class="fund-details-content">S&P BSE</p>'+
                   '</div>'+
                 '</div>'+

                  '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Launch</p>'+
                     '<p class="fund-details-content">12-Apr-2017</p>'+
                   '</div>'+

                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Investment</p>'+
                     '<p class="fund-details-content">Growth</p>'+
                   '</div>'+
                  '</div>'+


                 '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">'+
                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Exit Load</p>'+
                     '<p class="fund-details-content">12-Apr-2017</p>'+
                   '</div>'+

                   '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">'+
                     '<p class="fund-details-heading">Asset Size</p>'+
                     '<p class="fund-details-content">Growth</p>'+
                   '</div>'+
                 '</div>'+
             '</div> <!-- Fund Detail container closes-->'+

             '<div class="col-lg-12 col-md-12 col-sm-12">'+
               '<p id="nav_container"><span>NAV - </span><span id="scheme_nav">3000.0000</span></p>'+
             '</div>'+

             '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                 '<p class="graph-duration text-center active-duration border-right">1 Day</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                 '<p class="graph-duration text-center border-right">5 Day</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                 '<p class="graph-duration text-center border-right">1 Month</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                 '<p class="graph-duration text-center border-right">3 Months</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                 '<p class="graph-duration text-center border-right">1 year</p>'+
               '</div>'+
               '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                 '<p class="graph-duration text-center">5 Years</p>'+
               '</div>'+
             '</div>'+

            

             '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">'+
               '<div id="bal-graph-container">'+
                 
               '</div>'+
             '</div>'+

             '<div class="col-lg-12 col-md-12 col-sm-12 border-top">'+
               '<p id="fund-ret-header">Fund Returns</span></p>'+
            ' </div>'+

             '<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">'+
               '<div id="border-blah">'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">1 Day</p>'+
                   '<p class="return-perc neg-ret">5.6%</p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">5 Day</p>'+
                   '<p class="return-perc pos-ret">0.2%</p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">1 Month</p>'+
                   '<p class="return-perc pos-ret">0.9%</p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">3 Months</p>'+
                   '<p class="return-perc pos-ret">2%</p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">1 year</p>'+
                   '<p class="return-perc pos-ret">16%</p>'+
                 '</div>'+
                 '<div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">'+
                   '<p class="graph-duration text-center">5 Years</p>'+
                   '<p class="return-perc pos-ret">22%</p>'+
                 '</div>'+
               '</div>'+
             '</div>'+



         '</div>');*/
})