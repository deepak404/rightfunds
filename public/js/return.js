$(document).ready(function(){

    var curr_val,amt_inv,eq_amt_inv,eq_curr_val,debt_amt_inv,debt_curr_val,ts_amt_inv,ts_curr_val;


    if (("{{$user_investment_details['total_current_value']}}") && ("{{$user_investment_details['total_amount_invested']}}")) {
          curr_val = "{{$user_investment_details['total_current_value']}}";
          amt_inv = "{{$user_investment_details['total_amount_invested']}}";
    }

    else{
          curr_val = 0;
          amt_inv = 0;
    }

    if (("{{$user_investment_details['eq']['total_amount_invested']}}") && ("{{$user_investment_details['eq']['total_current_value']}}")) {

          eq_amt_inv = "{{$user_investment_details['eq']['total_amount_invested']}}";
          eq_curr_val = "{{$user_investment_details['eq']['total_current_value']}}";

    }

    else{
          eq_amt_inv  = 0;
          eq_curr_val = 0; 
    }

    if (("{{$user_investment_details['debt']['total_amount_invested']}}") && ("{{$user_investment_details['debt']['total_current_value']}}")) {

          debt_amt_inv = "{{$user_investment_details['debt']['total_amount_invested']}}";
          debt_curr_val = "{{$user_investment_details['debt']['total_current_value']}}";

    }

    else{
          debt_amt_inv  = 0;
          debt_curr_val = 0; 
    }

    if (("{{$user_investment_details['bal']['total_amount_invested']}}") && ("{{$user_investment_details['bal']['total_current_value']}}")) {

          bal_amt_inv = "{{$user_investment_details['bal']['total_amount_invested']}}";
          bal_curr_val = "{{$user_investment_details['bal']['total_current_value']}}";

    }

    else{
          bal_amt_inv  = 0;
          bal_curr_val = 0; 
    }




    $('#growth_type').on('click',function(){

        if ($(this).text() == "XIRR") {
          $(this).text('ABSOLUTE');
          var average_return = (((curr_val - amt_inv)/amt_inv) * 100).toFixed(2);
          //console.log("average return is" + (average_return * 100));

          var eq_return = (((eq_curr_val - eq_amt_inv)/eq_amt_inv) * 100).toFixed(2);
          //console.log("Equity return is" + (eq_return * 100));

          var debt_return = (((debt_curr_val - debt_amt_inv)/debt_amt_inv) * 100).toFixed(2);
          //console.log("Equity return is" + (debt_return * 100));

          var bal_return = (((bal_curr_val - bal_amt_inv)/bal_amt_inv) * 100).toFixed(2);
          //console.log("Equity return is" + (bal_return * 100));

          $('#average_return').text(average_return+"%");
          $('#eq_return').text(eq_return);
          $('#debt_return').text(debt_return);
          $('#bal_return').text(bal_return);
        }

        else if ($(this).text() == "ABSOLUTE"){
          $(this).text('XIRR');

          $('#average_return').text(xirr_avg_return+"%");
          $('#eq_return').text(xirr_eq_return);
          $('#debt_return').text(xirr_debt_return);
          $('#bal_return').text(xirr_bal_return);

        }




    });
});