<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEkycsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ekyc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('video_link')->nullable();
            $table->string('pan_link')->nullable();
            $table->string('address_link')->nullable();
            $table->string('mandate_link')->nullable();
            $table->string('cc_link')->nullable();
            $table->string('ekyc_form_link')->nullable();
            $table->string('aof_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ekyc');
    }
}
