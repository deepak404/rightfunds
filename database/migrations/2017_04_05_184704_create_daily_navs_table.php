<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyNavsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_navs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('daily');

            $table->double('debt_106212');
            $table->double('debt_112422');
            $table->double('debt_114239');
            $table->double('debt_128053');
            $table->double('debt_111803');


            $table->double('bal_104685');
            $table->double('bal_100122');
            $table->double('bal_118191');
            $table->double('bal_131666');

            $table->double('eq_100520');
            $table->double('eq_118102');
            $table->double('eq_100349');
            $table->double('eq_112090');
            $table->double('eq_105989');
            $table->double('eq_112932');
            $table->double('eq_105758');
            $table->double('eq_103360');
            $table->double('eq_107745');
            $table->double('eq_126675');
            $table->double('eq_101979');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_navs');
    }
}
