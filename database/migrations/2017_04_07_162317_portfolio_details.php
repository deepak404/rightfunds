<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('portfolio_details', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->string('investment_id');
            $table->string('scheme_code');
            $table->string('folio_number')->nullable();
            $table->integer('portfolio_status');
            $table->string('portfolio_type');
            $table->double('amount_invested');
            $table->double('initial_amount_invested');
            $table->date('investment_date')->nullable();
            $table->double('units_held')->nullable();
            $table->double('initial_units_held')->nullable();
            $table->double('invested_nav')->nullable();
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_details');
    }
}
