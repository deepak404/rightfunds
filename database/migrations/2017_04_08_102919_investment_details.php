<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvestmentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->double('investment_amount');
            $table->integer('investment_type');
            $table->integer('portfolio_type');
            $table->integer('investment_status');
            $table->date('investment_date')->nullable();
            $table->integer('realise_response');
            $table->integer('payment_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investment_details');
    }
}
