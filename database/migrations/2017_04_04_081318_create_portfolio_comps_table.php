<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioCompsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_comps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->double('eq_amt');
            $table->double('debt_amt');
            $table->double('bal_amt');
            $table->double('goal_amt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_comps');
    }
}
