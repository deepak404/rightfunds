<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('scheme_code');
            $table->string('name');
            $table->string('investment_plan');
            $table->date('launch_date');
            $table->string('fund_type');
            $table->string('fund_manager');
            $table->integer('asset_size');
            $table->string('benchmark');
            $table->string('exit_load');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scheme_history');
    }
}
