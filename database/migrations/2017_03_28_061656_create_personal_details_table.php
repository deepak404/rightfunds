<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->unique();
            $table->string('f_name');
            $table->string('pan');
            $table->string('aadhar')->unique();
            /*$table->string('m_name');
            $table->string('l_name');*/
            $table->string('nationality');
            $table->string('f_s_name');
            $table->string('b_city');
            $table->string('mothers_name');
            $table->string('dob');
            $table->string('sex');
            $table->string('mar_status');
            $table->string('resident');
            $table->string('address');
            $table->string('pincode');
            $table->string('occ_type');
			$table->string('profile_img');
            $table->double('goal_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_details');
    }
}
