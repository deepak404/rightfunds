<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NachDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nach_details', function(Blueprint $table){
            $table->increments('id');
            $table->string('user_id');
            $table->date('kyc_date')->nullable();
            $table->integer('kyc_status')->nullable();
            $table->date('bse_client_date')->nullable();
            $table->integer('bse_client_status')->nullable();
            $table->integer('pan_status')->nullable();
            $table->date('pan_date')->nullable();
            $table->integer('kycvideo_status')->nullable();
            $table->date('kycvideo_date')->nullable();            
            $table->integer('cc_status')->nullable();
            $table->date('cc_date')->nullable();
            $table->integer('mandatesoft_status')->nullable();
            $table->date('mandatesoft_date')->nullable();
            $table->integer('mandatehard_status')->nullable();
            $table->date('mandatehard_date')->nullable();
            $table->integer('aof_uploaded')->nullable();
            $table->date('aof_date')->nullable();
            $table->integer('camskyc_status')->nullable();
            $table->date('camskyc_date')->nullable();
            $table->integer('bsemandate_status')->nullable();
            $table->date('bsemandate_date')->nullable();
            $table->integer('bseaof_status')->nullable();
            $table->date('bseaof_date')->nullable();
            $table->integer('nach_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nach_details');
    }
}
