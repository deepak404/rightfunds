<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('port_id');
            $table->double('inv_amount');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('tenure');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sips');
    }
}
