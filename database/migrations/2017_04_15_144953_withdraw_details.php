<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WithdrawDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_details', function(Blueprint $table){
            $table->increments('id');
            $table->string('user_id');
            $table->integer('withdraw_group_id');
            $table->string('scheme_code');
            $table->double('withdraw_amount');
            $table->date('withdraw_date')->nullable();
            $table->string('portfolio_type');
            $table->integer('withdraw_status');
            $table->string('full_amount');
            $table->string('int_ref_no');
            $table->string('unique_ref_no');
            $table->string('bse_remarks');
            $table->integer('bse_order_status');
            $table->string('bse_order_no');
            $table->double('withdraw_nav')->nullable();
            $table->double('withdraw_units')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('withdraw_details');
    }
}
