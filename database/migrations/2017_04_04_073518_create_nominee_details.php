<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNomineeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nominee_details', function (Blueprint $table) {
			$table->increments('id');
            $table->string('user_id');
            $table->string('nomi_name');
            $table->string('nomi_relationship');
            $table->date('nomi_dob')->nullable();
            $table->string('nomi_addr');
            $table->string('nomi_holding');
            $table->enum('nomi_type',['1','2','3']);
            $table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nominee_details');
    }
}
