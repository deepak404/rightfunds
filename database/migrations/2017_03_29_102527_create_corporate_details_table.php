<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorporateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->unique();
            $table->string('doi');
            $table->string('poi');
            $table->string('cin');
            $table->string('org');
            $table->string('address');
            $table->string('profile_img');
            $table->enum('acc_type', ['savings','current','NRE']);
            $table->string('acc_name');
            $table->string('acc_no');
            $table->string('ifsc_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('corporate_details');
    }
}
