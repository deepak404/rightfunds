<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        DB::table('scheme_details')->insert([

        	/*Below Five are Debt funds -- For Conservative Portfolio*/
            [
            	'scheme_code' => '106212',
            	'scheme_name' => 'SBI Ultra Short Term Debt Fund Growth',
            	'scheme_type' => 'debt',
            	'scheme_priority' => '1',
            ],

            [
            	'scheme_code' => '112423',
            	'scheme_name' => 'L&T Ultra Short Term Fund Growth',
            	'scheme_type' => 'debt',
            	'scheme_priority' => '2',
            ],

            [
            	'scheme_code' => '114239',
            	'scheme_name' => 'ICICI Prudential Regular Savings Fund Growth',
            	'scheme_type' => 'debt',
            	'scheme_priority' => '3',
            ],

            [
            	'scheme_code' => '128053',
            	'scheme_name' => 'HDFC Corporate Debt Opportunities Fund Growth',
            	'scheme_type' => 'debt',
            	'scheme_priority' => '4',
            ],

            [
            	'scheme_code' => '111803',
            	'scheme_name' => 'Birla Sun Life Medium Term Plan Growth',
            	'scheme_type' => 'debt',
            	'scheme_priority' => '5',
            ],

            [
              'scheme_code' => '133805',
              'scheme_name' => 'Kotak Low Duration Fund- Regular Plan Growth',
              'scheme_type' => 'debt',
              'scheme_priority' => '6',
            ],

            [
              'scheme_code' => '133926',
              'scheme_name' => 'DSP BlackRock Ultra Short Term Fund - Regular Plan - Growth',
              'scheme_type' => 'debt',
              'scheme_priority' => '7',
            ],


            /*Debt funds Ends -- For Conservative Portfolio*/


            /*Below Four are Balanced funds -- For Moderate Portfolio*/
            [
            	'scheme_code' => '104685',
            	'scheme_name' => 'ICICI Prudential Balanced Advantage Fund Growth',
            	'scheme_type' => 'bal',
            	'scheme_priority' => '4',
            ],

            [
            	'scheme_code' => '100122',
            	'scheme_name' => 'HDFC Balanced Fund Growth',
            	'scheme_type' => 'bal',
            	'scheme_priority' => '2',
            ],

            [
            	'scheme_code' => '118191',
            	'scheme_name' => 'L&T India Prudence Fund Growth',
            	'scheme_type' => 'bal',
            	'scheme_priority' => '1',
            ],

            [
            	'scheme_code' => '131666',
            	'scheme_name' => 'Birla Sun Life Balanced Advantage Fund Growth',
            	'scheme_type' => 'bal',
            	'scheme_priority' => '3',
            ],

            [
              'scheme_code' => '100081',
              'scheme_name' => 'DSP BlackRock Balanced Fund Growth',
              'scheme_type' => 'bal',
              'scheme_priority' => '5',
            ],



            /*Balanced funds Ends -- For Moderate Portfolio*/


            /*Below Eleven are Equity funds -- For Aggressive Portfolio*/
            [
            	'scheme_code' => '100520',
            	'scheme_name' => 'Franklin India Prima Plus Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '6',
            ],

            [
            	'scheme_code' => '118102',
            	'scheme_name' => 'L&T India Value Fund-Regular Plan Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '2',
            ],

            [
            	'scheme_code' => '100349',
            	'scheme_name' => 'ICICI Prudential Top 100 Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '5',
            ],

            [
            	'scheme_code' => '112090',
            	'scheme_name' => 'Kotak Select Focus Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '4',
            ],

           /* [
            	'scheme_code' => '105989',
            	'scheme_name' => 'DSP BlackRock Micro Cap Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '6',
            ],*/


            /*[
            	'scheme_code' => '112932',
            	'scheme_name' => 'Mirae Asset Emerging Bluechip Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '7',
            ],*/

            [
            	'scheme_code' => '105758',
            	'scheme_name' => 'HDFC Mid-Cap Opportunities Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '3',
            ],

            [
            	'scheme_code' => '103360',
            	'scheme_name' => 'Franklin India Smaller Companies Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '1',
            ],

             [
              'scheme_code' => '113177',
              'scheme_name' => 'Reliance Small Cap Fund Growth',
              'scheme_type' => 'eq',
              'scheme_priority' => '7',
            ],

            [
              'scheme_code' => '101672',
              'scheme_name' => 'Tata Equity P/E Fund Growth',
              'scheme_type' => 'eq',
              'scheme_priority' => '8',
            ],

            [
              'scheme_code' => '104481',
              'scheme_name' => 'DSP BlackRock Small & Mid Cap Fund Growth',
              'scheme_type' => 'eq',
              'scheme_priority' => '9',
            ],


            /*Equity Funds ends*/




            /*Tax Saver Funds begins*/

            [
            	'scheme_code' => '107745',
            	'scheme_name' => 'Birla Sun Life Relief 96 Growth',
            	'scheme_type' => 'ts',
            	'scheme_priority' => '1',
            ],

            [
              'scheme_code' => '103196',
              'scheme_name' => 'Reliance Tax Saver (ELSS) Fund-Growth Plan',
              'scheme_type' => 'ts',
              'scheme_priority' => '2',
            ],

            [
            	'scheme_code' => '101979',
            	'scheme_name' => 'HDFC TaxSaver-Growth Plan',
            	'scheme_type' => 'ts',
            	'scheme_priority' => '3',
            ],

            /*Tax Saver funds Ends*/


        ]);


        DB::table('scheme_history')->insert([

            [
              'scheme_code' => '106212',
              'name' => 'SBI Ultra Short Term Debt Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2007-07-26',
              'fund_type' => 'Ultra Short Term Debt',
              'fund_manager' => 'Rajeev Radhakrishnan',
              'asset_size' => 2193,
              'benchmark' => 'CRISIL Liquid Fund',
              'exit_load' => '0%',
            ],

            /*[
              'scheme_code' => '112422',
              'name' => 'L&T Ultra Short Term Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2003-04-10',
              'fund_type' => 'Ultra Short Term Debt',
              'fund_manager' => 'Jalpan Shah and Vikas Garg',
              'asset_size' => 3483,
              'benchmark' => 'CRISIL Liquid Fund',
              'exit_load' => '0%',
            ],*/

            [
              'scheme_code' => '112423',
              'name' => 'L&T Ultra Short Term Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '',
              'fund_type' => 'Ultra Short Term Debt',
              'fund_manager' => 'Jalpan Shah and Vikas Garg',
              'asset_size' => 567,
              'benchmark' => 'CRISIL Liquid Fund',
              'exit_load' => '0%',
            ],

            [
              'scheme_code' => '114239',
              'name' => 'ICICI Prudential Regular Savings Fund',
              'investment_plan' => 'Growth',
              'launch_date' => '2010-11-29',
              'fund_type' => 'Open-Ended',
              'fund_manager' => 'Rahul Bhuskute & Manish Banthia',
              'asset_size' => 6052,
              'benchmark' => 'CRISIL Composite Bond Fund',
              'exit_load' => '1% (redeemed within 365 days)',
            ],
            
            [
              'scheme_code' => '128053',
              'name' => 'HDFC Corporate Debt Opportunities Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2014-03-25',
              'fund_type' => 'Credit Opportunities Fund',
              'fund_manager' => 'Shobhit Mehrotra',
              'asset_size' => 11436,
              'benchmark' => 'CRISIL Short Term Bond Fund',
              'exit_load' => '2% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '111803',
              'name' => 'Birla Sun Life Medium Term Plan',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2009-03-25',
              'fund_type' => 'Credit Opportunities Fund',
              'fund_manager' => 'Maneesh Dangi / Sunaina Da Cunha',
              'asset_size' => 10233,
              'benchmark' => 'CRISIL Short Term Bond Fund',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '133926',
              'name' => 'DSP BlackRock Ultra Short Term Fund',
              'investment_plan' => 'Growth option',
              'launch_date' => '2015-03-04',
              'fund_type' => 'Ultra Short Term',
              'fund_manager' => 'Kedar Karnik & Laukik Bagwe',
              'asset_size' => 3914,
              'benchmark' => 'CRISIL Liquid Fund',
              'exit_load' => 'N.A',
            ],

            /*DEBT FUNDS ENDS*/


            /*BALANCED FUNDS BEGINS*/

            [
              'scheme_code' => '104685',
              'name' => 'ICICI Prudential Balanced Advantage Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2006-11-08',
              'fund_type' => 'Balanced',
              'fund_manager' => 'Manish Gunwani / Manish Banthia / Ashwin Jain / Rajat Chandak',
              'asset_size' => 16847,
              'benchmark' => 'CRISIL Balanced Fund',
              'exit_load' => '1% (redeemed within 18 months)',
            ],

            [
              'scheme_code' => '100122',
              'name' => 'HDFC Balanced Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2000-09-11',
              'fund_type' => 'Balanced',
              'fund_manager' => 'Chirag Setalvad',
              'asset_size' => 10920,
              'benchmark' => 'CRISIL Balanced Fund',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '118191',
              'name' => 'L&T India Prudence Fund',
              'investment_plan' => 'Growth',
              'launch_date' => '2011-01-31',
              'fund_type' => 'Open-Ended',
              'fund_manager' => 'S. N. Lahiri / Shriram Ramanathan / Abhijeet Dakshikar',
              'asset_size' => 3270,
              'benchmark' => 'BSE 200',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '131666',
              'name' => 'Birla Sun Life Balanced Advantage Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2000-04-25',
              'fund_type' => 'Balanced',
              'fund_manager' => 'Mohit Sharma / Vineet Maloo',
              'asset_size' => 872,
              'benchmark' => 'CRISIL Balanced Fund',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '100081',
              'name' => 'DSP BlackRock Balanced Fund',
              'investment_plan' => 'Growth',
              'launch_date' => '1999-05-55',
              'fund_type' => 'Balanced',
              'fund_manager' => 'Atul Bhole / Vikram Chopra / Pankaj Sharma ',
              'asset_size' => 4059,
              'benchmark' => 'CRISIL Balanced Fund',
              'exit_load' => '1% if redeemed or switched out within 12 months.',
            ],


            /*BALANCED FUNDS ENDS*/


            /*EQUITY FUNDS BEGINS*/

            [
              'scheme_code' => '100520',
              'name' => 'Franklin India Prima Plus',
              'investment_plan' => 'Growth Option',
              'launch_date' => '1994-09-28',
              'fund_type' => 'Diversified Equity',
              'fund_manager' => 'R.Janakiraman / Anand Radhakrishnan',
              'asset_size' => 10964,
              'benchmark' => 'NIFTY 500',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '118102',
              'name' => 'L&T India Value Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2009-12-15',
              'fund_type' => 'Diversified Equity',
              'fund_manager' => 'Venugopal Manghat / Abhijeet Dakshikar',
              'asset_size' => 3899,
              'benchmark' => 'BSE 200',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '100349',
              'name' => 'ICICI Prudential Top 100 Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '1998-06-19',
              'fund_type' => 'Large Cap',
              'fund_manager' => 'Sankaran Naren / Mittul Kalawadia',
              'asset_size' => 1546,
              'benchmark' => 'NIFTY 50',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '112090',
              'name' => 'Kotak Select Focus Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2009-08-20',
              'fund_type' => 'Multi Cap',
              'fund_manager' => 'Harsha Upadhaya',
              'asset_size' => 6335,
              'benchmark' => 'NIFTY 200',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '133805',
              'name' => 'Kotak Low Duration fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2009-08-20',
              'fund_type' => 'Multi Cap',
              'fund_manager' => 'Harsha Upadhaya',
              'asset_size' => 10270,
              'benchmark' => 'NIFTY 200',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            /*[
              'scheme_code' => '105989',
              'name' => 'DSP BlackRock Micro Cap',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2007-05-04',
              'fund_type' => 'Small Cap',
              'fund_manager' => 'Vinit Sambre & Jay Kothari',
              'asset_size' => 5824,
              'benchmark' => 'BSE Small Cap',
              'exit_load' => '1% (redeemed within 364 days)',
            ],*/

            /*[
              'scheme_code' => '112932',
              'name' => 'Mirae Asset Emerging Bluechip Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2010-07-09',
              'fund_type' => 'Mid Cap',
              'fund_manager' => 'Neelesh Surana',
              'asset_size' => 3769,
              'benchmark' => 'NIFTY Free Float Midcap 100',
              'exit_load' => '1% (redeemed within 365 days)',
            ],*/

            [
              'scheme_code' => '105758',
              'name' => 'HDFC Mid-Cap Opportunities Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2007-06-25',
              'fund_type' => 'Mid Cap',
              'fund_manager' => 'Chirag Setalvad',
              'asset_size' => 16685,
              'benchmark' => 'NIFTY Free Float Midcap 100',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '103360',
              'name' => 'Franklin India Smaller Companies Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2006-01-13',
              'fund_type' => 'Small & Mid Cap',
              'fund_manager' => 'R. Janakiraman / Hari Shyamsunder',
              'asset_size' => 5579,
              'benchmark' => 'NIFTY Free Float Midcap 100',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '101672',
              'name' => 'TATA Equity P/E Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2004-06-15',
              'fund_type' => 'Mid Cap',
              'fund_manager' => 'Atul Bhole',
              'asset_size' => 1137,
              'benchmark' => 'Benchmark S&P BSE SENSEX',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '104481',
              'name' => 'DSP BlackRock Small and Mid Cap Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2006-09-29',
              'fund_type' => 'Small & Mid Cap',
              'fund_manager' => 'Vinit Sambre',
              'asset_size' => 3804,
              'benchmark' => 'NIFTY MIDCAP 100',
              'exit_load' => '1% (redeemed within 365 days)',
            ],

            [
              'scheme_code' => '113177',
              'name' => 'Reliance Small Cap Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2010-09-09',
              'fund_type' => 'Small Cap',
              'fund_manager' => 'Sunil Singhania',
              'asset_size' => 3718,
              'benchmark' => 'S&P BSE SMALLCAP',
              'exit_load' => '1% (redeemed within 365 days)',
            ],


            /*EQUITY FUNDS ENDS*/


            /*Tax Saver Scheme begins*/

            [
              'scheme_code' => '107745',
              'name' => 'Birla Sun Life Tax Relief 96',
              'investment_plan' => 'Growth Option',
              'launch_date' => '1996-03-29',
              'fund_type' => 'ELSS',
              'fund_manager' => 'Ajay Garg',
              'asset_size' => 3355,
              'benchmark' => 'BSE 200',
              'exit_load' => '0%',
            ],

            [
              'scheme_code' => '103196',
              'name' => 'Reliance Tax Saver (ELSS) Fund',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2005-09-21',
              'fund_type' => 'ELSS',
              'fund_manager' => 'Ashwani Kumar',
              'asset_size' => 8045,
              'benchmark' => 'BSE 100',
              'exit_load' => '0%',
            ],

            [
              'scheme_code' => '101979',
              'name' => 'HDFC Tax Saver',
              'investment_plan' => 'Growth Option',
              'launch_date' => '2009-12-15',
              'fund_type' => 'ELSS',
              'fund_manager' => 'Vinay Kulkarni',
              'asset_size' => 6309,
              'benchmark' => 'NIFTY 500',
              'exit_load' => '0%',
            ],

            /*Tax Saver schemes ends*/

            /*DB::table('users')->insert([
                  [
                   'id' => '1',
                   'name' => 'Naveen',
                   'email' => 'naveen@gmail.com',
                   'mobile' => '7845787900',
                   'pan' => '7894561230',
                   'p_check' => '1',
                   'password' => '$2y$10$LBBDnCwZ4WNC7TKga4dKTew7nFKs6SAZXsktbi8IHrWf.ldZ/8XYO',],

                  ]);

            DB::table('personal_details')->insert([
                  [
                   'id' => '1',
                   'user_id' => '1',
                   'f_name' => 'Naveen',
                   'm_name' => 'M',
                   'l_name' => 'Kumar',
                   'Nationality' => 'Indian',
                   'f_s_name' => 'Maran',
                   'mothers_name' => 'Nagamani',
                   'dob' => '12/04/2017',
                   'sex' => 'male',
                   'mar_status' => 'single',
                   'resident' => 'res-ind',
                   'address' => 'sdfhkjdsahfkdasgfgdsahfgdhsf',
                   'pincode' => '600049',
                   'occ_type' => 'service',
                   'profile_img' => ''    ],

                  ]);*/

             /*DB::table('personal_details')->insert([

                  [
                   'acc_name' => 'Naveen',
                   'acc_no' => '8628101052147',
                   'acc_type' => 'savings',
                   'bank_type' => ''

                  ],*/

              ]);



           
    }
}
