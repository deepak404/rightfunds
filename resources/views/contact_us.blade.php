<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/version2/index.css?v=1.1">
        <link rel="stylesheet" href="css/footer.css?v=1.1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/landing.css?v=1.1">
        <link rel="stylesheet" href="css/landing-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/contact-us.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        

        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="icons/nav-logo.svg	" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faq')}}">FAQ</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick="location.href='/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>


        <section id="contact-us">
            <div class="container box-shadow-all">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <h3 id="header">Contact Us</h3>

                        <div class = "col-lg-4 col-md-4 col-sm-4">
                            <img src="icons/location.png" class="contact-img">
                            <span class="contact-info">Location</span>
                            <p class="contact-inner-info">#1,1st 1st Floor, Balaji First Avenue,<br>
                            T.Nagar, Chennai<br>
                            Tamil Nadu - 600017</p>
                        </div>

                        <div class = "col-lg-4 col-md-4 col-sm-4">
                            <img src="icons/phone.png" class="contact-img">
                            <span class="contact-info">Phone</span>
                            <p class="contact-inner-info">+91 88258 88200</p>
                        </div>

                        <div class = "col-lg-4 col-md-4 col-sm-4">
                            <img src="icons/mail.png" class="contact-img">
                            <span class="contact-info">Mail</span>
                            <p class="contact-inner-info">contact@rightfunds.com</p>
                        </div>

                    </div>


                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <h3 id="header">Send us a Mail</h3>
                        <form id="contact-form" name="contact-form" >
                            <div class = "col-lg-12 col-md-12 col-sm-12">
                                <p id="contact_us_request_status"></p>
                                <div class = "col-lg-4 col-md-4 col-sm-4 input-spacing">
                                    <input type="text" name = "name" id="name" class="input-field text-field" required>
                                    <label>Name</label>
                                    <span class="text-danger"></span>
                                </div>

                                <div class = "col-lg-4 col-md-4 col-sm-4 input-spacing">
                                     <input type="text" name = "email" id="email" class="input-field" required>
                                     <label>Email</label>
                                     <span class="text-danger"></span>
                                </div>

                                <div class = "col-lg-4 col-md-4 col-sm-4 input-spacing">
                                    <input type="text" name = "mobile" id="mobile" class="input-field" required>
                                    <label>Mobile</label>
                                    <span class="text-danger"></span>   
                                </div>


                               <div class = "col-lg-12 col-md-12 col-sm-12 input-spacing" id="message-cont">
                                    <input type="text" name = "message" id="message" class="input-field" required>
                                    <label>Message</label>
                                    <span class="text-danger"></span>
                               </div>

                               <input type="submit" name="submit" class="btn btn-primary" value="Send" id="submit-contact">
                            </div>   
                        </form>

                    </div>
                </div>
            </div>
        </section>


        @extends('layouts.outside-footer')

     <script src="js/jquery.min.js"></script>
     <script src="js/version2/validation.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
