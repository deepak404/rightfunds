<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> Buy Mutual Funds | Buy Mutual Funds Online | Rightfunds </title>
        <meta name="description" content="Buy top performing mutual funds in India with Rightfunds. Paperless Signup, Zero Fees, Tax Free investments. Invest in the best Equity, Debt and Tax Saving mutual funds online."
">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
       <!--  <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/footer.css"> -->
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/landing.css?v=1.1">
        <link rel="stylesheet" href="css/landing-responsive.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        

        <section id="introduction-tab">

            <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
              <div class="container-fluid" id="navbar-container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{url('/home')}}"><img src="icons/nav-logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <!--  <ul class="nav navbar-nav">
                    <li class="active-menu"><a href="#">Home</a></li>
                    <li><a href="#">Account Statement</a></li>
                    <li><a href="#">Settings</a></li>
                    
                  </ul> -->
                  
                  <ul class="nav navbar-nav navbar-right" id="register-nav">
                    <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faq')}}">FAQ</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                    <li><button onclick="location.href='/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
            <div class="container-fluid" id="intro-container"> 
                <div class="row">   
                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                        <div class = "col-lg-6 col-md-6 col-sm-6 p-lr-zero" id="intro-text-container">  
                            <h1> Invest in Mutual Funds - Really Simple, Automated Investing.</h1>
                            <p class="head-info">Rightfunds is an Investment Platform for the best Equity, Debt & Tax Saving Mutual Funds.</p>
                            
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-zero intro-info-container">
                                <div class = "col-lg-2 col-md-2 col-sm-2">
                                    <img src="icons/assets.png" class="info-images">
                                </div>
                                <div class = "col-lg-10 col-md-10 col-sm-10 intro-info-sub">
                                    <p class="info-text">Total assets under management</p>
                                    <h2 class="info-numbers">Rs.25,00,00,000+</h2>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-zero intro-info-container">
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <img src="icons/returns.png" class="info-images">
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 intro-info-sub">
                                    <p class="info-text">Net Returns generated</p>
                                    <h2 class="info-numbers">Rs.4,00,00,000+</h2> 
                                </div>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-zero intro-info-container">
                                <div class = "col-lg-2 col-md-2 col-sm-2">
                                    <img src="icons/schemes.png" class="info-images">
                                </div>
                                <div class = "col-lg-10 col-md-10 col-sm-10 intro-info-sub">
                                    <p class="info-text">No of Schemes offered</p>
                                    <h2 class="info-numbers">35+</h2>
                                </div>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                <p id="zero-sub-text">Zero Fees,Top performing mutual fund schemes, Tax free investments</p>
                            </div>
                            <a href="{{url('/register')}}" id="intro-register-link"> Invest in mutual funds with rightfunds.com today! <span><i class="material-icons" id="link-arrow">keyboard_arrow_right</i></span></a>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6 p-lr-zero" id="intro-img-container">
                            <img src="icons/header-img.png" class="img img-responsive" id="intro-img">    
                        </div>  
                        
                    </div>  
                </div>  
            </div>  
        </section>        

        <section id="top-performing-mutual-fund">   
            <div class="container-fluid"> 
                <div class="row">   
                    <h2 class="text-center">Top Performing Mutual fund Schemes</h2>

                    <div id="top-scheme-container">
                        <div class = "text-center amc-parent-container">  
                            <div class = " amc-container">
                                <img src="icons/reliance.png" class="center-block " alt="buy mutual funds - Reliance">
                                <h4 >Reliance Top 200 fund</h4>
                                <p >Minimum Investment amount Rs.5000</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">12-06-2007</p>
                                    <p class="amc-info-text">S&P BSE 200</p>
                                    <p class="amc-info-text">Rs 3,606 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">18.1%</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">14.0%</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">17.4%</h3>
                                </div>   
                            </div>
                        </div>

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/kotak.png" class="center-block " alt="buy mutual funds - Kotak">
                                <h4 >Kotak Select Focus fund</h4>
                                <p >Minimum Investment amount Rs.5000</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">20-08-2009</p>
                                    <p class="amc-info-text">NIFTY 50</p>
                                    <p class="amc-info-text">Rs 9,867 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">20.1</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">18.5</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">21.0</h3>
                                </div>   
                            </div>
                        </div>

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/L&T.png" class="center-block " alt="buy mutual funds - l&t">
                                <h4 >L&T Midcap fund</h4>
                                <p >Minimum Investment amount Rs.5000</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">23-07-2004</p>
                                    <p class="amc-info-text">NIFTY MIDCAP 100</p>
                                    <p class="amc-info-text">Rs 1,018 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">30.7</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">25.2</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">28.1</h3>
                                </div>   
                            </div>
                        </div>

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/hdfc.png" class="center-block " alt="buy mutual funds - HDFC">
                                <h4 >HDFC Tax Saver</h4>
                                <p >Minimum Investment amount Rs.500</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">18-12-1995</p>
                                    <p class="amc-info-text">NIFTY 500</p>
                                    <p class="amc-info-text">Rs 6,113 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">19.6</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">11.9</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">16.8</h3>
                                </div>   
                            </div>
                        </div>

                        <!-- Next 4 schemes is below -->

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/icici.png" class="center-block " alt="buy mutual funds - ICICI">
                                <h4 >ICICI Top 100 Fund</h4>
                                <p >Minimum Investment amount Rs.5000</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">19-06-1998</p>
                                    <p class="amc-info-text">NIFTY 50</p>
                                    <p class="amc-info-text">Rs 2,173 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">17.8%</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">12.4%</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">16.7%</h3>
                                </div>   
                            </div>
                        </div>

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/tata.png" class="center-block " alt="buy mutual funds - Tata">
                                <h4 >Tata Equity P/E fund</h4>
                                <p >Minimum Investment amount Rs.5000</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">15-06-2004</p>
                                    <p class="amc-info-text">S&P BSE SENSEX</p>
                                    <p class="amc-info-text">Rs 1,447 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">29.3%</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">22.1%</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">23.1%</h3>
                                </div>   
                            </div>
                        </div>

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/Birla.png" class="center-block " alt="buy mutual funds - Birla">
                                <h4 >Birla Tax Relief 96</h4>
                                <p >Minimum Investment amount Rs.500</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">06-03-2008</p>
                                    <p class="amc-info-text">S&P BSE 200</p>
                                    <p class="amc-info-text">Rs 3,501 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">22.6%</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">20.1%</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">21.8%</h3>
                                </div>   
                            </div>
                        </div>

                        <div class = "text-center amc-parent-container">  
                            <div class = "amc-container">
                                <img src="icons/sundaram.png" class="center-block " alt="buy mutual funds - Sundaram">
                                <h4 >Sundaram Rural India fund</h4>
                                <p >Minimum Investment amount Rs.5000</p>

                                <div class = "scheme-info-header">  
                                    <p class="amc-info-text">Launch Date</p>
                                    <p class="amc-info-text">Benchmark</p>
                                    <p class="amc-info-text">Asset Size</p>
                                </div> 
                                <div class = "scheme-info-detail">  
                                    <p class="amc-info-text">19-04-2006</p>
                                    <p class="amc-info-text">S&P BSE 500</p>
                                    <p class="amc-info-text">Rs 1,679 Cr</p>
                                </div>
                                <div class = "return-block">
                                    <h4>Returns</h4>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">1 Year</p>
                                    <h3 class="green return-perc">24%</h3>
                                </div>
                                <div class = "return-perc-div">
                                    <p class="amc-year">3 Year</p>
                                    <h3 class="green return-perc">21.8%</h3>
                                </div> 
                                <div class = "return-perc-div">
                                    <p class="amc-year">5 Year</p>
                                    <h3 class="green return-perc">21.9%</h3>
                                </div>   
                            </div>
                        </div>
                    </div> <!-- top scheme container ends -->

                    <div id="direction-holder">
                        <p class="text-center" id="toggle-card-parent"><span class="toggle-scheme" id="show-one"><i class="material-icons">keyboard_arrow_left</i></span><span class="toggle-scheme" id="show-two"><i class="material-icons">keyboard_arrow_right</i></span></p> 
                    </div> 
                                   
                    </div>  <!-- #performing funds container ends -->
                </div>  
            </div>  
        </section>

        <section id="why-choose-us">    
            <div class="container"> 
                <div class="row">   
                    <h2 class="text-center">Why Choose us to help you invest your money ?</h2>
                    <div class = "col-lg-12 col-md-12 col-sm-12 feature-container">   
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/zero-fee.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Zero Fees</h4>
                            <p class="feature-info text-center">No hidden costs. We are paid by the Asset Management Companies. You pay us nothing at all.</p>
                        </div> 
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/invest-online.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Invest Online</h4>
                            <p class="feature-info text-center">Make and monitor your investments in just a few clicks. No more paper work.</p>
                        </div> 
                         <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/custom-portfolio.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Customized Portfolio</h4>
                            <p class="feature-info text-center">Build and track your own Portfolios based on your desired level of risk.</p>
                        </div>  
                    </div>  

                    <div class = "col-lg-12 col-md-12 col-sm-12 feature-container">   
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/bank-grade.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Bank Grade Security</h4>
                            <p class="feature-info text-center">256 bit SSL Encryption. Your investments go directly from your bank to the mutual fund</p>
                        </div> 
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/tax-free.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Tax Free Returns</h4>
                            <p class="feature-info text-center">The returns made from Equity mutual funds are completely tax free after a year of staying invested.</p>
                        </div> 
                         <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/secure.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Secure Transaction</h4>
                            <p class="feature-info text-center">Build and track your own Portfolios based on your desired level of risk.</p>
                        </div>  
                    </div>  

                </div>  
            </div>  
        </section>  


        <section id="articles"> 
            <div class="container"> 
                <div class="row">   
                    <h2 class="text-center">Articles to help you get started with investing in mutual funds.</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12">   
                        <div class="col-lg-4 col-md-4 col-sm-4">  
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero article-container" id="article-one">   
                                <p class="article-heading"><a target = "_blank" href="{{url('/blog/why_should_i_invest_my_money')}}">
                                    <span>Why Should i Invest my money ?</span><span><i class="material-icons">arrow_forward</i></span>
                                </a></p>
                            </div>  
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">  
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero article-container" id="article-two">   
                                <p class="article-heading"><a target = "_blank" href="{{url('/blog/introduction_to_mutual_funds')}}">
                                    <span>Introduction to Mutual funds.</span><span><i class="material-icons">arrow_forward</i></span>
                                </a></p>
                            </div>  
                        </div>
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero article-container" id="article-three">   
                                <p class="article-heading"><a target = "_blank" href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}">
                                    <span>How do I save on taxes with Mutual funds?</span><span><i class="material-icons">arrow_forward</i></span>
                                </a></p>
                            </div>  
                        </div>  

                    </div>  
                </div>  
            </div>  
        </section>


        <section id="our-partners">
            <div class="container"> 
                <div class="row">
                    <h2 class="text-center">Our Partners</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12 logo-container">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/franklin-mutual-funds.png" class="partners-logo img img-responsive" alt="Franklin mutual fund" id="franklin">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/dsp-mutual-funds.png" class="partners-logo img img-responsive" alt="dsp blackrock mutual fund" id="dsp">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/Birla-mutual-funds.png" class="partners-logo img img-responsive" alt="birla sunlife mutual fund" id="birla">
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/icici-mutual-funds.png" class="partners-logo img img-responsive" alt="icici prudential mutual fund" id="icici">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/kotak-mutual-funds.png" class="partners-logo img img-responsive" alt="kotak mutual fund" id="kotak">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/axis-mutual-funds.png" class="partners-logo img img-responsive" alt="axis mutual fund" id="axis">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12 logo-container">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/reliance-mutual-funds.png" class="partners-logo img img-responsive" alt="reliance mutual fund" id="reliance">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/sbi-mutual-funds.png" class="partners-logo img img-responsive" alt="sbi mutual fund" id="sbi">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/hdfc-mutual-funds.png" class="partners-logo img img-responsive" alt="hdfc mutual fund" id="hdfc">
                        </div>        
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/l&t-mutual-funds.png" class="partners-logo img img-responsive" alt="l&t mutual fund" id="lt">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/tata-mutual-funds.png" class="partners-logo img img-responsive" alt="tata mutual fund" id="tata">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/sundaram-mutual-funds.png" class="partners-logo img img-responsive" id="sundaram" alt="sundaram mutual fund" id="sundaram">
                        </div>
                      </div>                         
                    </div>
            </div>  
        </section>  

@extends('layouts.outside-footer')

     <script src="js/jquery.min.js"></script>
     <script src="js/version2/landing.js?v=1.1"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
