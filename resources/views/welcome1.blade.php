<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mutual fund investing made simple | Rightfunds.com</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Invest in mutual funds online with Rightfunds.com. India's best online investment portal for equity, debt & Tax saving mutual funds. Start investing today !">
    <meta name="author" content="Rightfunds Mutual Funds">
    <meta name="Keywords" content="Mutual Fund Investments, Mutual Funds Investing, Mutual Funds Analysis, Mutual Funds Ratings, Mutual Funds Investment, Mutual Funds Research, Online Mutual Funds, Best Mutual Funds, Definition of Mutual Funds, SBI Mutual Fund, Top Mutual Funds">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="msvalidate.01" content="340C07958EA7EBCDC6069A4C6DCEA228" />

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="{{url('js/welcome.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

     <!--<script type="text/javascript">
     google.charts.load('current', {packages: ['corechart']});     
     </script>
     <script src ="js/charts.js"></script>   -->

     <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/welcome.css">
    <link rel="stylesheet" href="css/welcome-responsive.css">
    <style type="text/css">
      div.google-visualization-tooltip {
background-color: #ffffff;
border: 2px solid #f5f5f5 !important;
width: 20%;
box-shadow: 0 0 5px #d7d7d7;
z-index: 150;
}

div.google-visualization-tooltip > ul > li > span {
color: #333 !important;
font-family: gotham_book !important;
font-size: 12px !important;
}

</style>
</head>
<body>


<nav class="navbar">
  
    <div class="container">
          <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="/"><img src="icons/rightfunds-mutual-funds.png"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right navbar-btn">
            <a href = "{{URL('/login')}}" class="nav-btn" id="login-btn">LOGIN</a>
            <a href = "{{URL('/register')}}" type="button" class="btn btn-primary nav-btn" id="signup-btn">SIGN UP</a>
          </ul>
        </div>
    </div>
  
</nav>

  
<section id="section1">
<div class="container">
  <p class="text-center" id="f-header"><span id="r-sim">Really Simple,</span><span id="r-aut"> Automated Investing</span></p>
  <p class="text-center" id="s-header">Rightfunds is an Investment Platform for the best Equity, Debt & Tax Saving Mutual Funds</p>
</div><!-- Section1 Container ends -->
</section>


<section id="sip_calculator">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
       <p class="text-center" id="start_monthly"><a href = "{{url('/register')}}" id="start_sip"> Start</a> a Monthly Investment Plan today!</p>
       
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -10px;">
          <div id="sip_chart" style="height: 230px !important;">
          <!--<img class = "img-responsive center-block" src="icons/chart.png" alt="Mutual Fund Growth"/> -->
          </div>
        </div>

        <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="donut_div">
          <canvas id="myChart" width="400" height="400"></canvas>
        </div>-->
      </div>
    </div>

    <div class="row">

      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4" id="legend_div">
        <ul style="font-family: gotham_book; list-style: none;" id="ul">
          <li id="net_return_dash"><span id="net_returns">Wealth Gained : </span><span id="wealth_gained"></span></li>
          <li id="amt_inv_dash"><span id="amount_invested">Amount Invested : </span><span id="amount_invested1"></span></li>
        </ul>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="sip_inp_div">
     
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <h5 class="text-center gotham-medium sip_inp_headings">MONTHLY INVESTMENT (Rs.)</h5>
          <input type="text" name="" id="inv_amount" class="sip_inputs center-block" placeholder="Rs." value="5000" maxlength="7">
          <p class="text-center gotham-book notify_amount" id="lakhs-99">Not more than 99 Lakhs</p>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <h5 class="text-center gotham-medium sip_inp_headings">INVESTMENT PERIOD (Yrs.)</h5>
          <input type="text" maxlength="2" name="" id="inv_period" class="sip_inputs center-block" placeholder="Yrs." value="25">
          <p class="text-center gotham-book notify_amount" id="years-99">Not more than 99 years</p>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">

          <h5 class="text-center gotham-medium sip_inp_headings">SELECT PORTFOLIO (%)</h5>
          <!--<select class="center-block risk-sel" id="port_type">
            <option value="lr">Conservative (8%)</option>
            <option value="mr">Moderate (14%)</option>
            <option value="hr">High Risk (20%)</option>
          </select>-->
          
            <div id="dyna_div">
              <div class = "col-xs-2 padding-lr-zero text-center"><i class="fa fa-chevron-left" id = "left" aria-hidden="true" style = "color : #333;"></i></div>
            <div class = "col-xs-8 padding-lr-zero text-center" id = "port_type"><p class="text-center portfolio_type">Moderate (14%)</p></div>
            <div class = "col-xs-2 padding-lr-zero text-center"><i class="fa fa-chevron-right" id = "right" aria-hidden="true" style = "color : #333;"></i>
            </div>

            </div>
          
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <h5 class="text-center gotham-medium sip_inp_headings">ANNUAL INCREMENT (%)</h5>
          <input type="number" name="" id="ann_incr" class="sip_inputs center-block" placeholder="%" value="0">
        </div>
      </div>
    </div>
  </div>
</section>

<section id="section3">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id = "taum-id">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sec3-img-div">
            <img src="icons/aum_h.png" class="sec3-img">
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <p class="sec3-headers">Total Assets Under Management</p>
          </div>
          <div class = "col-xs-12">
            <p class="sec3-sub">Rs.15,00,00,000+</p>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id = "taum-id">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sec3-img-div" >
            <img src="icons/nr_h.png" class="sec3-img">
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <p class="sec3-headers">Net returns generated</p>
          </div>
          <div class = "col-xs-12">
            <p class="sec3-sub">Rs.2,50,00,000+</p>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id = "taum-id">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sec3-img-div">
            <img src="icons/schemes.png" class="sec3-img">
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <p class="sec3-headers">Number of Schemes Offered</p>
          </div>
          <div class = "col-xs-12">
            <p class="sec3-sub">20+</p>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>


<section id="section4">
  <div class="container">

  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-4 col-md-4 col-sm-4" id = "why-choose">
          <p class="gotham-light why-c">WHY CHOOSE US</p>
          <p class="gotham-light why-c">TO HELP YOU</p>
           <h2 class="gotham-medium why-c">INVEST YOUR MONEY?</h2>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-bot-30">
          <div class="col-xs-4 padding-r-zero">
            <img src="icons/zero_fee_h.png" class="sec4-img">
          </div>
          <div class="col-xs-8 padding-l-zero">
            <h3 class="gotham-medium sec4-headers">ZERO FEES</h3>
            <p class="gotham-book sec4-sub-headers">No hidden costs. We are paid by the Asset Management Companies. You pay us nothing at all.</p>
          </div>            
      </div>


      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-bot-30">
            <div class="col-xs-4 padding-r-zero">
              <img src="icons/online_h.png" class="sec4-img">
            </div>

            <div class="col-xs-8 padding-l-zero">
              <h3 class="gotham-medium sec4-headers mar-b-0">INVESTING ONLINE</h3>
              <p class="gotham-book sec4-sub-headers">Make and monitor your investments in just a few clicks. No more paper work.</p>
              <p class="gotham-book sec4-sub-headers"></p>
              
            </div>            
      </div>

      
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-bot-30">
            <div class="col-xs-4 padding-r-zero">
              <img src="icons/tailor_h.png" class="sec4-img">
            </div>

            <div class="col-xs-8 padding-l-zero">
              <h3 class="gotham-medium sec4-headers mar-b-0">CUSTOMISED PORTFOLIOS</h3>
              <p class="gotham-book sec4-sub-headers">Build and track your own Portfolios based on your desired level of risk.</p>             
            </div>            
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-bot-30">
            <div class="col-xs-4 padding-r-zero">
              <img src="icons/bank_grade_h.png" class="sec4-img">
            </div>

            <div class="col-xs-8 padding-l-zero">
              <h3 class="gotham-medium sec4-headers mar-b-0">BANK GRADE SECURITY</h3>
              <p class="gotham-book sec4-sub-headers">256 bit SSL Encryption. Your investments go directly from your bank to the mutual fund.</p>
            </div>            
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-bot-30">
            <div class="col-xs-4 padding-r-zero">
              <img src="icons/tax_free_h.png" class="sec4-img">
            </div>
            <div class="col-xs-8 padding-l-zero">
              <h3 class="gotham-medium sec4-headers">TAX FREE RETURNS</h3>
              <p class="gotham-book sec4-sub-headers">The returns made from Equity mutual funds are completely tax free after a year of staying invested.</p>
            </div>           
      </div>



    </div>
  </div>

  </div>
</section>

<section id="section5">
  <p id="sec-5-text"><span class="gotham-light">Take your first step towards </span><span class="gotham-medium">financial independence!</span><a href="{{URL('/register')}}" class="gotham-medium btn btn-primary" id="a-signup">SIGNUP NOW</a></p>
</section>

<section id="partners">
  <div class="container">
    <p id="our_part"><span class="gotham-medium">OUR PARTNERS</p>
  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo_col">

      <div class="col-lg-6 col-md-6 col-sm-12 padding-lr-zero text-center margin-logo">
        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/franklin-mutual-funds.png" class="partners_logo img img-responsive" alt="Franklin mutual funds" id="franklin">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/dsp-mutual-funds.png" class="partners_logo img img-responsive" alt="dsp blackrock mutual fund" id="dsp">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/Birla-mutual-funds.png" class="partners_logo img img-responsive" alt="birla sunlife mutual fund" id="birla">
        </div>
      </div>

      

      <div class="col-lg-6 col-md-6 col-sm-12 padding-lr-zero text-center margin-logo">
        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/icici-mutual-funds.png" class="partners_logo img img-responsive" alt="icici prudential mutual fund" id="icici">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/kotak-mutual-funds.png" class="partners_logo img img-responsive" alt="kotal mutual fund" id="kotak">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/axis-mutual-funds.png" class="partners_logo img img-responsive" alt="axis mutual fund" id="axis">
        </div>
      </div>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo_col">
      <div class="col-lg-6 col-md-6 col-sm-12 padding-lr-zero text-center margin-logo">
        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/reliance-mutual-funds.png" class="partners_logo img img-responsive" alt="reliance mutual fund" id="reliance">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/sbi-mutual-funds.png" class="partners_logo img img-responsive" alt="sbi mutual fund" id="sbi">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/hdfc-mutual-funds.png" class="partners_logo img img-responsive" alt="hdfc mutual fund" id="hdfc">
        </div>        
      </div>

      <div class="col-lg-6 col-md-6 col-sm-12 padding-lr-zero text-center margin-logo">
        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/l&t-mutual-funds.png" class="partners_logo img img-responsive" alt="l&t mutual fund" id="lt">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/tata-mutual-funds.png" class="partners_logo img img-responsive" alt="tata mutual fund" id="tata">
        </div>

        <div class="col-xs-4 text-center">
          <img src= "icons/mutual-funds-companies/sundaram-mutual-funds.png" class="partners_logo img img-responsive" id="sundaram" alt="sundaram mutual fund" id="sundaram">
        </div>
      </div>   

      
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 col-sm-offset-10 col-xs-offset-10">
      <p class="text-center gotham-medium more-30">AND 30+ MORE...</p>
    </div>


    </div>
  </div>
</section>
<div class="hr"></div>

<section id="section6">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-7 col-md-7 col-sm-7" id = "footer-first">
          <img src="icons/rightfunds-mutual-funds.png" id="footer-logo">
          <p id="copyrights" class="gotham-book">&copy;Prosperity Technology Private Limited, 2017.</p>
          <p id="disclaimer" class="gotham-book">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>

          <p id="version" class="gotham-book" style="color: #999; font-size: 12px;">V 1.0(L)</p>

          
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2">
          <p class="gotham-medium" id="company-head">COMPANY</p>
          <ul id="company">
            <li><a href="{{URL('/about-us')}}">About us</a></li>
            <li><a href = "/terms">Terms of use</a></li>
            <li><a href = "/privacy_policy">Privacy Policy</a></li>
            <!--<li><a href = "/disclosure">Disclosures</a></li>-->
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <p class="gotham-medium" id="company-head">GET IN TOUCH</p>
          <ul id="company_contact">
            <li><a href="contact-us" id="contact-us-link">Contact us</a></li>
            <li><span>8825-888-200</span></li>
            <!--<li><a href = "mailto:vgupta@rightfunds.com?Subject=Website%20Enquiry" id="support-mail">support@rightfunds.com</a></li>-->
          </ul>
          <ul id="follow-us-ul">
            <li  id="follow-us"><a href = "#">FOLLOW US</a></li>
            <li id = "social-list">
              <span><a id= "social_facebook" href="https://www.facebook.com/rightfunds/"><img id="facebook_img" src="icons/facebook.png" class = "social-img"></a></span>
              <span><a id="social_twitter" href="https://twitter.com/rightfundsindia"><img id="twitter_img" src="icons/twitter.png" class = "social-img"></a></span>
              <span><a id="social_linkedin" href="https://www.linkedin.com/company/rightfunds.com"><img id="linkedin_img" src="icons/linked_in.png" class = "social-img"></a></span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

 

<!-- Modal -->
<div id="signupModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="thanks" class="gotham-medium text-center">Thanks for your interest in Rightfunds.</p>
        <p id="we-will" class="gotham-book text-center">We will be up and running soon.</p>
        <p id="signup-pop" class="gotham-book text-center">Please Sign up with your email for Updates</p>
        <form action="index.php" method="POST">
          
            <div id="signup-form">
              <input type="email" name="email" id="email"><button id="signup-btn-pop" class="btn btn-primary" type="submit">SIGN UP</button>
            </div>
          
        </form>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="alertModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="thanks" class="gotham-medium text-center">Looks like you have already signed up !</p>
        <p id="we-will" class="gotham-book text-center">We will keep you updated.</p>
        <button type="button" class="btn btn-primary center-block" id="okay-btn">Okay</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="oopsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="thanks" class="gotham-medium text-center">Oops !! Something went Wrong.</p>
        <p id="we-will" class="gotham-book text-center">We will be back in sometime.</p>
        <button type="button" class="btn btn-primary center-block" id="okay-btn">Okay</button>
      </div>
    </div>

  </div>
</div>

<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["Amount Invested", "Wealth Gained"],
        datasets: [{
            
            data: [60, 40],
            backgroundColor: [
                'rgba(0, 145, 234, 1)',
                'rgba(24, 195, 161, 1)',
            ],
            borderColor: [
                'rgba(0, 145, 234, 1)',
                'rgba(24, 195, 161, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    display:false,
                },
                gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                     drawBorder: false,
                }
            }],
            xAxes: [{

                gridLines: {
                  color: "rgba(0, 0, 0, 0)",
                     drawBorder: false,
                }
            }]
        },
         cutoutPercentage: 65,
         legend:{
          position : 'bottom',
          display : false
         }
    }
});
</script>

 
  </body>
  </html>
