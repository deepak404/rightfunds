<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <!-- <link rel="stylesheet" href="css/index.css"> -->
        <link rel="stylesheet" href="css/footer.css?v=1.1">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/landing.css?v=1.1">
        <link rel="stylesheet" href="css/landing-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/faq.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        

        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="icons/nav-logo.svg	" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick="location.href='/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>


        <section id="faq">
            <div class="container">
                <h2 id="faq-header">Frequently Asked Questions</h2>
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12" id="accordion">
                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" data-parent = "#accordion" href = "#collapse1">   
                                <h4 class="faq-heading">What is Mutual Fund?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse1" class="collapse faq-exp-container">
                                <p class="faq-exp">A mutual fund is a professionally managed investment scheme in which investors get an
opportunity to invest their money in a diversified portfolio of stocks &amp; shares, corporate
debentures, government bonds &amp; other money market instruments.
Mutual fund schemes are offered by Asset Management Companies such as, SBI AMC,
Reliance AMC, HDFC AMC, ICICI AMC etc. Each scheme offered by an AMC has an
investment objective which could for example be providing a steady monthly income or long
term wealth creation. It is important for an investor to a choose mutual fund scheme whose
objectives align with their own financial goals.

<span class="sub-faq-content">
    <span>Corporate debenture</span> – A medium to long term debt or loan taken by a company at a fixed
rate of interest. Similar to a fixed deposit, but in this case, instead of a bank a company pays
the investor the interest.
</span>

<span class="sub-faq-content">
<span>Government bond</span> – Long term debts or loans raised by the government in order to fund
various public projects and undertakings. The investor in return is payed a fixed percentage
of interest.
</span>

<span class="sub-faq-content">
<span>Money market Instruments</span> – Short term liquid debts raised by organizations to fund their
working capital requirements. They pay the investors a fixed percentage of interest.</span></p>
                            </div>
                        </div>  


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse2" data-parent ="#accordion">   
                                <h4 class="faq-heading">Why should I invest in Mutual Fund?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse2" class="collapse faq-exp-container">
                                <p class="faq-exp">Mutual fund investments offer numerous benefits to its investors.
                                <span class="sub-faq-content">
                                    1. The most prominent being high rates of return. It is not uncommon to witness
                                    equity mutual funds yield over 25% returns in a year as against investments in a
                                    savings account or Fixed deposit which typically yield returns between 3.5% to
                                    6.25%.
                                </span>

                                <span class="sub-faq-content">
                                    2. Capital gains made on equity mutual fund investments are completely tax free
                                    after a year of staying invested.
                                </span>

                                <span class="sub-faq-content">
                                    3. A single unit of a mutual fund gives the investor an exposure to numerous stocks
                                    in the market. This diversification minimises the risk associated with market
                                    fluctuations. So when one company or a sector does poorly the outperformance
                                    of the other sectors provide positive returns to the investor.
                                </span>

                                </p>
                            </div>
                        </div> 


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse3" data-parent ="#accordion">   
                                <h4 class="faq-heading">How much money do i need to start investing ?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse3" class="collapse faq-exp-container">
                                <p class="faq-exp">One may start a monthly investment plan on Rightfunds platform with as little as rupees 5000.</p>
                            </div>
                        </div>  


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse4" data-parent ="#accordion">   
                                <h4 class="faq-heading">What is KYC Procedure? And why should i complete it before being able to invest?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse4" class="collapse faq-exp-container">
                                <p class="faq-exp">KYC stands for Know – Your – Customer. As per the law laid down by Securities Exchange
                                Board of India it is mandatory for all investors to complete a one time KYC Process before
                                they can begin investing. The process involves providing a signed Pan card copy, signed

                                Aadhar card copy, signed KYC form &amp; a cancelled Cheque leaf. Rightfunds provides a way for
                                its customer to complete the KYC process online with minimal paper work.</p>
                            </div>
                        </div> 


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse5" data-parent ="#accordion">   
                                <h4 class="faq-heading">How is money Transferred from my bank account when i place purchase order ?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse5" class="collapse faq-exp-container">
                                <p class="faq-exp">Money is transferred directly from the customer’s account to the bank account of the Asset
                                Management Company (Rightfunds does not receive any investors money into its own
                                account). The transfer may be done via online banking or through a one-time bank mandate
                                that the customer signs when he opens an account with Rightfunds. The purchase orders
                                are routed through the Bombay stock exchange where they are executed.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse6" data-parent ="#accordion">   
                                <h4 class="faq-heading">How do i withdraw my investments?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse6" class="collapse faq-exp-container">
                                <p class="faq-exp">Like purchases, withdrawals can also be done online. The withdrawal option can be found at
                                    Home page&gt;Account statements&gt;withdraw. Once a withdrawal request is placed money is
credited back to the customer’s bank account within 3-5 working days.</p>
                            </div>
                        </div> 


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse7" data-parent ="#accordion">   
                                <h4 class="faq-heading">is there any fees or charge to use Rightfunds Platforms?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse7" class="collapse faq-exp-container">
                                <p class="faq-exp">No, we do not charge our customers anything. No setup fees, No subscriptions charges or
hidden costs. Rightfunds platform is completely free to use for its customers. We are
compensated by the Asset Management Companies who pay us a small commission for the
funds we disburse through our website.</p>
                            </div>
                        </div> 


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse8" data-parent ="#accordion">   
                                <h4 class="faq-heading">What are tax saving mutual funds?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse8" class="collapse faq-exp-container">
                                <p class="faq-exp">Tax saving mutual funds are essentially equity orientated schemes that invest in the stocks
&amp; shares of numerous companies. Under section 80C of the Indian Income tax act an
individual can save up to 1.5 Lakhs in taxable income each year by investing in tax saving
mutual funds. Tax saving mutual funds have the lowest lock in periods of 3 years when
compared to other post office and PPF schemes whose average tenure ranges from 5-15
years. Also historically tax saving mutual funds have provided much superior returns
compared to their counter-parts.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse9" data-parent ="#accordion">   
                                <h4 class="faq-heading">Are mutual funds risky?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse9" class="collapse faq-exp-container">
                                <p class="faq-exp">Not all mutual funds are built the same. ‘Debt mutual funds’ essentially invest in debt
instruments that offer a fixed rate return which is not in any way linked to market volatility.
‘Equity mutual funds’ on the other hand invest in the stock market and hence their returns
are tied to the market fluctuations. However, an equity mutual fund invests in numerous
companies in which case the risk in diversified. Also these are handled by well qualified
professionals who track and monitor each and every market movement. This makes mutual
fund investments many times safer than investing directly in the stock market by oneself.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse10" data-parent ="#accordion">   
                                <h4 class="faq-heading">Who are mutual funds for?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse10" class="collapse faq-exp-container">
                                <p class="faq-exp">If you earn an income or have some savings, then mutual funds are for you. You’ve worked
hard to earn money, if you do not invest it wisely the money will lose its value over the
years to come as things around you get expensive. At the moment you put your money in a

fixed deposit or a savings account which pays you 3.5-6% interest barely beating inflation.
Why not give mutual funds a try some of which have yielded over 20% percent returns year
on year for the last 10 years. Sign up to know more…</p>
                            </div>
                        </div>

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse11" data-parent ="#accordion">   
                                <h4 class="faq-heading">What Returns can I expect when I invest in mutual funds?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse11" class="collapse faq-exp-container">
                                <p class="faq-exp">While one cannot guarantee returns, historically equity mutual funds have provided returns
between 18-25%, balanced mutual funds have provided returns between 14-16% and Debt
mutual funds between 7-12% (depending upon the type of debt fund) over the last five
years. The difference in the returns generated is a product of the risk each type of mutual
fund takes to generate them. In the long run (5-7 years) the risk taken becomes negligible.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse12" data-parent ="#accordion">   
                                <h4 class="faq-heading">What is Net Asset Value of a Scheme?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse12" class="collapse faq-exp-container">
                                <p class="faq-exp">Net asset value of a scheme is similar to the share price of a company, except that it reflects
the collective price of numerous companies that the scheme has invested in.
Mathematically it may calculated as follows:
<pre>Net Asset Value (NAV) = (Assets – Debts) / (Number of Outstanding units)</pre>
Where,
<pre>Assets = Market Value of mutual fund investments + Receivables + Accrued Income.
Debts = Liabilities + Expenses (Accrued)</pre></p>
                            </div>
                        </div> 



                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse14" data-parent ="#accordion">   
                                <h4 class="faq-heading">How are capital gains on mutual funds taxed?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse14" class="collapse faq-exp-container">
                                <p class="faq-exp">Capital gains are the returns you make on your investments. In the case of equity mutual
funds capital gains are completely tax free after a year of staying invested. For debt mutual
funds the capital gains are indexed (i.e. the effect of inflation on the profits is subtracted)
prior to taxation, when investments are held for a period of 3 years.
When an investor invests in Tax saving mutual funds not only are the capital gains exempt
from being taxed after a year but also the principal amount (up to rupees 1.5 lakhs per year)
is exempt from being taxed as given under section 80C of the Indian income tax act.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse15" data-parent ="#accordion">   
                                <h4 class="faq-heading">How do you select the funds that you display on your platform?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse15" class="collapse faq-exp-container">
                                <p class="faq-exp">At Rightfunds we filter over 2000 mutual fund schemes to offer roughly 25 that cater to
every investment objective. Some factors that we analyse while filtering the schemes
include: Beta Ratios, Sharpe ratios, Percentage of exposure to small, mid and large cap
stocks, exit loads, average assets managed by the scheme, benchmarks, cumulative
experience of the fund manager, Reaction to market volatility via point to point comparison
with peers, portfolio churn rate, average maturity of the fund, modified duration, % of
papers which are credit rated &gt; AA, % of government securities etc.

In order to assist us with this complex process of filtering and dynamically monitoring
various mutual fund schemes we have built sophisticated software that automatically
updates itself every day based on market behavior.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse16" data-parent ="#accordion">   
                                <h4 class="faq-heading">Can I monitor the returns on my investments on Rightfunds platform?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse16" class="collapse faq-exp-container">
                                <p class="faq-exp">Yes, apart from being able to execute orders, Rightfunds will also let you track each of your
investments and provide you with a daily Investment summary. You can track your absolute
return as well as the compounded aggregate growth rate of your entire portfolio. We also
provide detailed account statements containing the folio number, date of purchase, units
held, current market value, eligibility for Long term capital gains etc. in an exportable excel
format. If you are not KYC compliant which is a mandatory requirement in order to begin
investing in mutual funds, you may complete your E-KYC online while signing up with
Rightfunds.</p>
                            </div>
                        </div> 


                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse17" data-parent ="#accordion">   
                                <h4 class="faq-heading">How have various asset classes performed over the last 5 years?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse17" class="collapse faq-exp-container">
                                <p class="faq-exp">The graph below shows the value of Rupees 10,000 invested in various asset classes over
the last 5 years.</p>
                                <img src="img/faq.jpeg" class="img  img-responsive faq-image">
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse18" data-parent ="#accordion">   
                                <h4 class="faq-heading">Who is the regulatory authority for mutual funds?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>

                            </div>  
                            <div id="collapse18" class="collapse faq-exp-container">
                                <p class="faq-exp">Securities Exchange Board of India (SEBI) formulates policies and regulates mutual fund
activity in India. Association of Mutual funds in India (AMFI) lays down guidelines in its AMFI
Code of Ethics in order to ensure mutual funds are distributed in the right manner. They also
address investor grievances related to mutual funds. Apart from the above the Reserve
Bank of India (RBI) &amp; Ministry of finance are also key stake holders in the mutual fund
industry.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse19" data-parent ="#accordion">   
                                <h4 class="faq-heading">What if Rightfunds shuts down? Do I lose my money?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse19" class="collapse faq-exp-container">
                                <p class="faq-exp">Rightfunds does not receive or store your money. When you place an order on the
Rightfunds platform money is directly sent to the Asset Management Company eg. SBI AMC,
HDFC AMC etc. When an investment is executed the AMC provides the investor with a
unique ‘Folio Number’ the investor may redeem his units at any time by submitting a
redemption form directly to the AMC quoting his unique folio number.</p>
                            </div>
                        </div> 

                        <div class = "col-lg-12 col-md-12 col-sm-12 faq-container">   
                            <div class="heading" data-toggle = "collapse" href = "#collapse20" data-parent ="#accordion">   
                                <h4 class="faq-heading">How much time does it take for my investment transactions to be executed?</h4>
                                <span class="pull-right arrow-icon"><i class="material-icons">keyboard_arrow_down</i></span>
                            </div>  
                            <div id="collapse20" class="collapse faq-exp-container">
                                <p class="faq-exp">Orders placed before 12:30pm are executed on the same day, after 12:30 pm they
are executed the next day. It may however take 2-3 business days for a successful
purchase to reflect on your portal. Orders placed over the weekend are executed the
next working day.</p>
                            </div>
                        </div> 


                    </div>
                </div>
            </div>
        </section>



    @extends('layouts.outside-footer')

     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

     <script type="text/javascript">
         $(document).ready(function(){
            //alert($('.faq-exp-container:visible').length);

            if($('.faq-exp-container:visible').length > 1){
                
            }else{
                //alert('not');
            }
         });

     </script>
    </body>
</html>
