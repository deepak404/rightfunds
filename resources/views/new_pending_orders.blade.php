<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{URL('/css/smart_search.js')}}"></script>

    <link rel="stylesheet" type="text/css" href="{{URL('../css/admin/hello.css')}}">
    <link rel="stylesheet" type="text/css" href="/css/admin/hello.css">
    <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{url('/css/admin/admin-pending-orders.css')}}">
    <link rel="stylesheet" href="{{url('/css/admin/admin-po-responsive.css')}}">
    <link rel="stylesheet" href="{{url('/css/admin/footer.css')}}">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('/css/admin/jqueryui.css')}}">
    <link rel="stylesheet" href="{{url('/css/admin/datepicker.css')}}">
    
    <link rel="stylesheet" href="{{url('/css/admin/extras.css')}}">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/loader.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


        <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="/css/hello.css">

    <link rel="stylesheet" href="{{URL('/css/admin/admin-pending-orders.css')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-responsive.css')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/footer.css')}}">
    <link href="{{URL('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css')}}">
    <link rel="stylesheet" href="{{URL('/css/suggestions.css')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/extras.css')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css')}}">
    <link rel="stylesheet" href="{{URL('/css/loader.css')}}">
    
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav">LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
        <li class="active menu-text"><a href="#">Overview</a></li>
        <li class="menu-text"><a href="#">Customer Support</a></li>
        <li class="menu-text menu-text-active"><a href="#">Pending Orders</a></li>
        <li class="menu-text"><a href="#">Commission Status</a></li>
        <li class="menu-text"><a href="#">Schemes</a></li>
        <li class="menu-text"><a href="#">Order History</a></li>
        <li class="menu-text"><a href="#">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">

    <div class = "row investment-summary">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero search-bar ">

              <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="pending-details-container">

                  <div class="col-lg-10 col-md-10 col-sm-10">
                    <p class="pend_inv_name">Sathish Kumar</p>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <i class="material-icons pull-right pending-order-details-cl" data-toggle = "collapse" data-target="#one">keyboard_arrow_down</i>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero pend-det-info-cont">
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info" id="pan_holder">PAN : BFRPN4910B</p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                              <p class="pending-det-info">Investment</p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                              <p class="pending-det-info">SIP</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Conservative</p>
                            </div>
                          
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                          <p class="text-center pull-right" id="pending-inv-amount">Rs. 10,00,00,000</p>
                        </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        
                        <p id="order_placed">Order Placed Date : <span id="order_placed_date"> 07-08-2017</span></p>
                      
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <p id="sip_placed">Order Placed Date : <span id="sip_placed_date"> 07-08-2017</span></p>
                    </div>
                  </div>


                  <div class="col-lg-12 col-md-12 col-sm-12 collapse" id="one">

                    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-top">
                      <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero pending-details-container">
                        <input type="checkbox" name=""> 
                        <p class="pending-details-scheme">SBI Ultra short term debt fund - Regular Plan Growth</p>
                        <i class="material-icons pull-right close-pending-details">close</i>
                      </div>

                      <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                          <p class="pending-portfolio-details">Portfolio No : <span>123456879/12398</span></p>
                        </div>
                         <div class="col-lg-4 col-md-4 col-sm-4">
                          <input type="checkbox" name="">
                          <p class="pending-bse-details">BSE : <span>123456879/12398</span></p>
                        </div>
                         <div class="col-lg-4 col-md-4 col-sm-4 padding-r-zero">
                          <p class="pull-right pending-amount-details">Rs.10,00,00,000</p>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-top">
                      <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero pending-details-container">
                        <input type="checkbox" name=""> 
                        <p class="pending-details-scheme">SBI Ultra short term debt fund - Regular Plan Growth</p>
                        <i class="material-icons pull-right close-pending-details">close</i>
                      </div>

                      <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                          <p class="pending-portfolio-details">Portfolio No : <span>123456879/12398</span></p>
                        </div>
                         <div class="col-lg-4 col-md-4 col-sm-4">
                          <input type="checkbox" name="">
                          <p class="pending-bse-details">BSE : <span>123456879/12398</span></p>
                        </div>
                         <div class="col-lg-4 col-md-4 col-sm-4 padding-r-zero">
                          <p class="pull-right pending-amount-details">Rs.10,00,00,000</p>
                        </div>
                      </div>
                    </div>
                    <button type="" class="center-block btn btn-primary" id="confirm-pending-btn">Order</button>
                  </div><!-- One ends -->



              </div> <!--Pending Details Container ends -->


              <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="pending-details-container">

                  <div class="col-lg-10 col-md-10 col-sm-10">
                    <p class="pend_inv_name">Naveen Kumar</p>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <i class="material-icons pull-right pending-order-details-cl">keyboard_arrow_down</i>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero pend-det-info-cont">
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info" id="pan_holder">PAN : BFRPN4910B</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Investment</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">SIP</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Conservative</p>
                            </div>
                          
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                          <p class="text-center pull-right" id="pending-inv-amount">Rs. 10,00,00,000</p>
                        </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        
                        <p id="order_placed">Order Placed Date : <span id="order_placed_date"> 07-08-2017</span></p>
                      
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <p id="sip_placed">Order Placed Date : <span id="sip_placed_date"> 07-08-2017</span></p>
                    </div>
                  </div>

              </div> <!--Pending Details Container ends -->


              <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="pending-details-container">

                  <div class="col-lg-10 col-md-10 col-sm-10">
                    <p class="pend_inv_name">Elangovan</p>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <i class="material-icons pull-right pending-order-details-cl">keyboard_arrow_down</i>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero pend-det-info-cont">
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info" id="pan_holder">PAN : BFRPN4910B</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Investment</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">SIP</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Conservative</p>
                            </div>
                          
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                          <p class="text-center pull-right" id="pending-inv-amount">Rs. 10,00,00,000</p>
                        </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        
                        <p id="order_placed">Order Placed Date : <span id="order_placed_date"> 07-08-2017</span></p>
                      
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <p id="sip_placed">Order Placed Date : <span id="sip_placed_date"> 07-08-2017</span></p>
                    </div>
                  </div>

              </div> <!--Pending Details Container ends -->


              <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="pending-details-container">

                  <div class="col-lg-10 col-md-10 col-sm-10">
                    <p class="pend_inv_name">Vinoj Kummar</p>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <i class="material-icons pull-right pending-order-details-cl">keyboard_arrow_down</i>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero pend-det-info-cont">
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info" id="pan_holder">PAN : BFRPN4910B</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Investment</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">SIP</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                              <p class="pending-det-info">Conservative</p>
                            </div>
                          
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                          <p class="text-center pull-right" id="pending-inv-amount">Rs. 10,00,00,000</p>
                        </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        
                        <p id="order_placed">Order Placed Date : <span id="order_placed_date"> 07-08-2017</span></p>
                      
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <p id="sip_placed">Order Placed Date : <span id="sip_placed_date"> 07-08-2017</span></p>
                    </div>
                  </div>

              </div> <!--Pending Details Container ends -->



              
                        
          </div> <!--Search Bar Ends -->
     </div>



           <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about_us')}}">About</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <li><a href="{{URL('/disclosure')}}">Disclosures</a></li>
                    <li><a href="{{URL('/contact_us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="icons/twitter.png"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="icons/facebook.png"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="icons/linked_in.png"></a>

                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
      

</div> <!--Content Ends -->

</div>




  <script src="admin/js/jquery.min.js"></script>
  <script src="admin/js/bootstrap.min.js"></script>
  <!--<script src="../js/index.js"></script>-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
  <script src="{{URL('/js/scroller.js')}}"></script>
  <script src="{{URL('/js/smart_search.js')}}"></script>
    <script src="{{url('js/loader.js')}}"></script>



</body>
</html>
