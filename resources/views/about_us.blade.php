<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/footer.css?v=1.1">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/landing.css?v=1.1">
        <link rel="stylesheet" href="css/landing-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/contact-us.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <style type="text/css">
        #email-span{
          display: block;
        }
      </style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        

        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="icons/nav-logo.svg " class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faq')}}">FAQ</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick="location.href='/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

  
        <section id="contact-us">
            <div class="container">
                <p id="privacy-policy">About Us</p>

              <div class="row">
                <div class="col-xs-12">

                  <p class="privacy-content">Knowledge, Dynamism &amp; Integrity are the cornerstones of Rightfunds existence. The
platform was built to provide a means for investors like you and me to begin investing in
Mutual funds. In the light of falling Fixed deposit rates and the inherent risks involved in
buying individual stocks, mutual funds provide for a great alternative to investors by
enabling them to maintain a professionally managed portfolio that is diversified across
various sectors.
                  </p>

                  <p class="privacy-content">The platform offers Equity, Debt, Balanced and Tax saving mutual fund schemes to choose
from based on your investment horizon and tolerance for risk.</p>

                  <p class="privacy-content">Coupling technology with distribution of financial services has given us the unprecedented
opportunity to service small investments. You may now begin investing regularly with as
little as Rs. 1,000!</p>

                  <p class="privacy-content">Remember, it is never too early to begin investing, if you’re 25 and invest just rupees 5,000
each month at a rate of 18% your portfolio would be worth roughly 3 crores by your 50 th
birthday. While 18% returns may seem unachievable there are several equity oriented
schemes that have given such aggregated growth rates for a prolonged period of time.</p>

                  <p class="privacy-content">Disciplined investing and the power of compounding can make you rich!</p>

                  <p class="privacy-content">In this journey of building Rightfunds together, we welcome your suggestions and feedback
on how we can serve you better. Happy Investing!</p>

                  <p class="privacy-content" id="founder-mail"><span></span>
                  <span>Vasudev Fatehpuria</span>
                  <span id="email-span">vgupta@rightfunds.com</span></p>


                  
                </div>
              </div>
            </div>
        </section>
  










@extends('layouts.outside-footer')

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>


  </body>
  </html>
