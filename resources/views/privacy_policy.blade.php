<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->

        <link rel="stylesheet" href="css/footer.css?v=1.1">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/landing.css?v=1.1">
        <link rel="stylesheet" href="css/landing-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/contact-us.css?v=1.1">
           <link rel="stylesheet" href="css/version2/index.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        

        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="icons/nav-logo.svg	" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faq')}}">FAQ</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick="location.href='/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

  
        <section id="contact-us">
            <div class="container">
                <p id="privacy-policy">Privacy Policy</p>

              <div class="row">
                <div class="col-xs-12">

                  <p class="privacy-content">This Privacy Policy sets forth the measures adopted by Rightfunds to protect the sensitive personal data or information shared by the registered users and visitors of this website, mobile sites or mobile applications (“Website”), and provides information about collection, storage, processing and transfer of the Personal Information and Non-Personal Information by us.
                  </p>

                  <p class="privacy-content">We value your trust at Rightfunds and are committed to protecting your information and being clear about what we do with it. This policy explains the privacy practices of Rightfunds and explains what personal information we collect, why we collect it, how we protect it, and how and why in certain cases we may share information about you.</p>

                  <p class="privacy-content">By providing us your Information or by making use of the facilities provided by the Website, you hereby agree to be bound by this Privacy Policy.</p>

                  <p class="privacy-content">This Privacy Policy is published and shall be construed in accordance with the provisions of The Information Technology Act, 2000, the Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011 and other applicable laws of India.</p>

                  <p class="privacy-content">This Privacy Policy is subject to change at any time. It is advised that you regularly check this Privacy Policy to apprise yourself of any updates. </p>

                  <h3 class="side-header">Information We Collect And From Where Do We Collect It:</h3>
                  <h4 class="sub-header">Visitors to Our Website:</h4>

                  <p class="privacy-content">As a visitor to our Website, you remain anonymous unless you register for a service or otherwise choose to disclose your identity to us.  We do not collect personal information that identifies people who simply visit our Website, however, we may collect certain limited information about our visitors, such as your IP address, unique device identifier and device information etc. This information is used solely for the purpose of analysis usage patterns and improving our services.</p>

                  <h4 class="sub-header">Registered Users:</h4>
                  <p class="privacy-content">When you use our Services, we collect information about you in the following general categories: </p>

                  <h4 class="sub-header">Personal Information We Collect:</h4>
                  <p class="privacy-content">We collect information to provide better services to all of our users - Information collected that can be used to uniquely identify or contact you and includes:</p>

                  <div class="sub-header-div">

                    <p class="privacy-content">1. Personal Information like your Name, PAN, Bank Account Number [“KYC information”] at the time of registration /activation of such facility.</p>

                    <p class="privacy-content">2. Your identity and contact details, email address and other personal details such as gender and marital status and financial information.
                    </p>

                    <p class="privacy-content">3. Details of your visits to our Website (including, but not limited to, IP address, operating system and browser type, traffic data, location data, weblogs and other communication data, and the resources that you access).
                    </p>

                    <p class="privacy-content">4. Website may also request other Personal Information from time to time, to provide you with other benefits of the Services.
                    </p>

                  </div>

                  <h4 class="sub-header">Personal Information may be collected in various ways including:</h4>

                    <div class="sub-header-div">
                      
                      <p class="privacy-content">1. Information online that you provide to us while registering as a user on the Website, availing certain services offered on the Website, on forms and applications you complete, and when you use our products and services. 
                      </p>

                      <p class="privacy-content">2. Information from third parties such as social media services, commercially available sources and business partners, and others to tailor your online experience. The information we collect may include your user name associated with that social media service, any information or content the social media service has the right to share with us, such as your profile picture, email address or friends list, and any information you have made public in connection with that social media service.
                      </p>

                      <p class="privacy-content">3. Our Website uses tools such as cookies and Web Analytics, to gather information for internal uses and to personalize your visit in order to provide you with a good user experience.</p>

                    </div>

                  <h4 class="sub-header">How Is Your Personal Information Utilized:</h4>

                  <p class="privacy-content">Rightfunds uses your personal information in a variety of ways in connection with providing you with services to meet your financial goals and needs. The Website uses and discloses your Personal Information only as follows:
                  </p>

                  <div class="sub-header-div">
                    <p class="privacy-content">1. To qualify you for products and services subscribed or availed by you on the Website, and to make investment suggestions.
                    </p>

                    <p class="privacy-content">2. To personalize aspects of the overall service to you and to provide you with information, products or services that you request from us or which we feel may interest you.</p>

                    <p class="privacy-content">3. To verify your identity, troubleshooting problems, detecting and protecting against error, fraud or other criminal activity.</p>
                    <p class="privacy-content">4. To conduct KYC registration, which is obligatory pursuant to the requirements of SEBI and other regulatory bodies.
                    </p>
                    <p class="privacy-content">5. To address any complaints or problems incurred on the Website including addressing any technical problems.
                    </p>
                    <p class="privacy-content">6. To notify you about changes to our Website, including changes in terms and conditions, and policies and/or other administrative information.
                    </p>
                    <p class="privacy-content">7. To offer you products or services, including special offers, promotions, or advertisements tailored to your preferences.
                    </p>
                    <p class="privacy-content">8. To perform data analytics for the internal purposes of developing or improving our products, services, security, service quality, and advertising strategies.
                    </p>
                  </div>
                  <p class="privacy-content">To respond to your queries or feedback. </p>


                  <p class="privacy-content">We may share your information with third parties who are bound by same privacy restrictions as Rightfunds, when we believe it will enhance the services we can provide, or as and when required and permitted by law. However, Rightfunds is not responsible for any breach of security or for the actions of any third parties that may result in loss or injury to you on account of information made available to any third party.
                  </p>


                  <p class="privacy-content">At Rightfunds, we are committed to respecting and protecting the privacy of our online customers and site visitors. Our online services protect your personal information during transit using encryption and other security measures to guard against unauthorized access to systems where your personal information is stored in our database.
                  </p>

                  <p class="sub-header">Collection And Use Of Non-Personal Information:</p>

                  <p class="privacy-content">We may also collect Non-Personal Information that does not, on its own, permit direct association with any specific individual. Such information may be stored in server logs and includes:</p>

                  <p class="privacy-content">Information such as your geographic location, language, unique device identifier, details of your internet service provider and the duration of your stay on the Website, so that we can better understand customer behavior and improve our products, services, and advertising.
                  </p>

                  <p class="privacy-content">Information regarding customer activities on our website, to help us provide more useful information to our customers and to understand which parts of our website, products, and services are of most interest.</p>

                  <p class="privacy-content">Information regarding how many people visit this website, the pages they visit, their IP address, and the type of browser they used while visiting.
                  </p>

                  <p class="privacy-content">Rightfunds may use cookies and other technologies to gather non-personal information. These technologies help us better understand user behavior and analyze web traffic. You can choose to accept or decline cookies, by modifying your browser settings. This may prevent you from using certain features of the Website.
                  </p>

                  <p class="sub-header">Disclosure:</p>

                  <p class="privacy-content">Rightfunds may disclose or part with all or any of your Information to any of our associates and affiliates, and third party service providers without any limitation and you hereby give your consent for the same. We may share information with third parties to protect or defend our rights, interests and property, and as permitted or required by law.
                  </p>

                  <p class="privacy-content">We may also share information where necessary to enforce our terms and conditions, or protect our operations or users. Additionally, information might be shared where appropriate to protect against fraud or for risk management purposes.
                  </p>


                  <p class="privacy-content">Please be assured that when we disclose your personal data to such parties, we contractually oblige them to protect your information and will make reasonable efforts to bind them to obligation to keep the same secure and confidential, and you hereby give your irrevocable consent for the same.
                  </p>


                  <p class="sub-header">Link to other Websites</p>

                  <p class="privacy-content">Rightfunds may feature links to third-party sites that offer goods, services, or information. We are not responsible for any actions or policies of such third party sites that you visit, and we urge you to review their privacy policies before you provide them with any personally identifiable information.</p>

                  <p class="privacy-content">In addition, we may employ the services of other parties for dealing with matters that may include, but are not limited to, payment handling, subscription and/or registration-based services, advertising, marketing and providing customer service.  The information provided by you to such third party websites shall be governed in accordance with the privacy policies of such third party websites.</p>


                  <p class="sub-header">Protecting Your Information:</p>

                  <p class="privacy-content">Security of your personal information is of great importance to Rightfunds and to protect your data, we have security policies and procedures reasonably designed to protect information from unauthorized access and misuse. To make sure your personal information is secure, we communicate our privacy and security guidelines to our employees and strictly enforce privacy safeguards within the company. You are responsible for maintaining the confidentiality of your account information and we recommend you to choose your password carefully, and keep your password and computer secure by signing out after using our services.
                  </p>

                  <p class="privacy-content">Notwithstanding anything contained in this Policy or elsewhere, by using the Website you acknowledge that no data transmission is completely secure and that we cannot provide any absolute guarantee of the security of the your personal information or the security of our servers, networks or databases,  and we are not responsible for any loss of information or the consequences thereof. 
                  </p>

                  <p class="privacy-content">Rightfunds retains personally identifiable information as long as necessary to serve the purpose for which it was collected remains and until it is no longer necessary for any other legal or business purposes.
                  </p>


                  <p class="privacy-content">You are also under an obligation to use this Website for reasonable and lawful purposes only, and shall not indulge in any activity that is not envisaged through the Website.
                  </p>


                  <p class="sub-header">User Information Choices And Opt Out:</p>

                  <p class="privacy-content">By using our Website and services, you agree and acknowledge that you are providing your information out of your free will and consent. You also hereby provide your unconditional consent to Rightfunds to collect your Personal Information without incurring any liability or obligation whatsoever upon Rightfunds under the Information Technology Act, 2000.
                  </p>

                  <p class="privacy-content">You have the right to send requests about your contact preferences, changes and deletions to your information including requests to opt-out of sharing your personal information with third parties by emailing to the grievance officer or such other electronic address of the respective Rightfunds entity as may be notified to the User. In case you choose to opt out of our services, you should neither visit Website nor use any services provided by Rightfunds. Further, Rightfunds may deny you access from using certain services offered on the Website.</p>

                  <p class="privacy-content">You can add or update your Personal Information on regular basis. Kindly note that Rightfunds would retain your previous Personal Information in its records. If you would like a copy of the personal information you have provided us, do get in touch with us at the address provided below.
                  </p>


                  <p class="sub-header">Business / Assets Sale or Transfers:</p>
                  <p class="privacy-content">Rightfunds may sell, transfer or otherwise share significant part its business, including your information in connection with a merger, acquisition, reorganization or sale of assets or business, or in the event of bankruptcy. If another company acquires our company, business or assets, that company will possess the personal information collected by us and will assume the rights and obligations regarding your personal information as described in this Policy.
                  </p>


                  <p class="sub-header">Amendments and Updates Of Our Privacy Policy:</p>
                  <p class="privacy-content">Rightfunds may change this Policy from time to time, and will make available the updated Policy on our Website. Your continued use of our Website, applications or services following notification will constitute your acceptance of such revised Policy.
                  </p>

                  <p class="sub-header">Contacting Us:</p>
                  <p class="privacy-content">If you have any questions about this Privacy Policy, please feel free to contact us at the email address given below:
                  </p>

                  <p class="privacy-content" id="founder-mail"><!-- <span><img alt="Vasudev Fatehpuria" src="icons/founder_pic.jpg" class="img img-circle" id="founder-pic"></span> -->vgupta@rightfunds.com</p>


                  
                </div>
              </div>
            </div>
        </section>
  










@extends('layouts.outside-footer')

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>


  </body>
  </html>
