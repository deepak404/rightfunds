<!-- <!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Be right back.</div>
            </div>
        </div>
    </body>
</html>
 -->


 <!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="css/version2/upgrade.css">
    </head>
    <body>
    <section id="upgrade-section">
        <div class="container-fluid">
            <div class="row">   
                <div class = "col-lg-12 col-md-12 col-sm-12" id="upgrade-card">   
                    <img src="icons/upgrade.svg">
                </div>
                <div class = "col-lg-12 col-md-12 col-sm-12" id="upgrade-card"> 
                    <p id="back-text" class = "text-center">Under Maintanance</p>  
                    <p class="header text-center">We are currently under maintanance!</p>
                    <!-- <p class="text-center" id="version">Version 2.0</p> -->
                    <p class="text-center" id="follow">Follow Us on</p>
                    <ul class="list-inline" id="social-parent">
                        <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook.png" class="footer-social"></a></li>
                        <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter.png" class="footer-social"></a></li>
                        <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linked_in.png" class="footer-social"></a></li>
                    </ul>
                    <p class="text-center" id="copy">&copy;2018, Rightfunds.com</p>
                </div>
            </div>  
        </div>  
    </section>
    </body>
</html>
