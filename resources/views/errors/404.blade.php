<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="../css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="../css/404.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">



</head>

<body>

  
<div class = "container">

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <p class="text-center mont" id="oops">OOPS</p>
    <p class="text-center mont" id="exist">THE PAGE DOESN'T EXIST</p>
    <button onclick="location.href='/home';" class="text-center btn btn-primary center-block" id="go-home" >GO HOME</button>
  </div>
</div>


</div>



</body>
</html>
