<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/welcome.min.css">
    <link rel="stylesheet" href="css/welcome-responsive.min.css">
    <link rel="stylesheet" href="css/privacy_policy.css">

</head>
<body>


<nav class="navbar">
  
    <div class="container">
          <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="/"><img src="icons/logo_white.png"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right navbar-btn">
            <a href = "{{URL('/login')}}" class="nav-btn" id="login-btn">LOGIN</a>
            <a href = "{{URL('/register')}}" type="button" class="btn btn-primary nav-btn" id="signup-btn">SIGN UP</a>
          </ul>
        </div>
    </div>
  
</nav>

<div class="container">
  <p class="text-center gotham_medium" id="privacy_policy">Portfolio</p>

<div class="row">
  <div class="col-xs-12">

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="portfolio-container">
        <a href="{{url('/portfolio/aggressive.pdf')}}"  target = "_blank" class="portfolio-links">Aggressive</a>
      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="portfolio-container">
        <a href="{{url('/portfolio/conservative.pdf')}}"  target = "_blank" class="portfolio-links">Conservative</a>
      </div>
    </div>

  </div>
</div>
</div>

  









<section id="section6">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-7 col-md-7 col-sm-7" id = "footer-first">
          <img src="icons/rightfunds-mutual-funds.png" id="footer-logo">
          <p id="copyrights" class="gotham-book">&copy;Rightfunds Private Limited, 2017.</p>
          <p id="disclaimer" class="gotham-book">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>

        </div>
        <div class="col-lg-2 col-md-2 col-sm-2">
          <p class="gotham-medium" id="company-head">COMPANY</p>
          <ul id="company">
            <li><a href = "/about_us">About us</a></li>
            <!--<li><a href = "/blog">Blog</a></li>-->
            <li><a href = "/terms">Terms of use</a></li>
            <li><a href = "/privacy_policy">Privacy Policy</a></li>
            <!--<li><a href = "/disclosure">Disclosures</a></li>-->
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <p class="gotham-medium" id="company-head">CONTACT US</p>
          <ul id="company_contact">
            <li><a href = "#">8825-888-200</a></li>
            <li><a href = "mailto:vgupta@rightfunds.com?Subject=Website%20Enquiry" id="support-mail">support@rightfunds.com</a></li>
            
          </ul>
          <ul id="follow-us-ul">
            <li  id="follow-us"><a href = "#">FOLLOW US</a></li>
            <li id = "social-list"><span><a id= "social_facebook" href="https://www.facebook.com/rightfunds/"><img src="icons/facebook_h.png" class = "social-img"></a></span><span><a id= "social_twitter" href="https://twitter.com/rightfundsindia"><img src="icons/twitter_h.png" class = "social-img"></a></span><span><a id= "social_linkedin" href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linked_in_h.png" class = "social-img"></a></span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="js/about_us.js"></script>

  <script type="text/javascript">
    
    $('#social_facebook').hover(function(){
        $('#social_facebook>img').attr('src','icons/facebook_hover.png');
    });
  </script>

  </body>
  </html>
