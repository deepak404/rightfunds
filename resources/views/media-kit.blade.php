<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/footer.css">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css">
        <link rel="stylesheet" href="css/landing.css">
        <link rel="stylesheet" href="css/landing-responsive.css">
        <link rel="stylesheet" href="css/version2/contact-us.css">
        <link rel="stylesheet" href="css/version2/media-kit.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        

        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="icons/nav-logo.svg	" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faq')}}">FAQ</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick="location.href='/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>


        <section id="media-kit">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h1 id="media-header">Media and Press Kit</h1>
                            <p class="usage-info">Download our assets if you want to create something with our name on it. Please read the documentation thoroughly before using our brand.</p>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div id="download-card" class="center-block text-center">
                                <i class="material-icons" id="kit-icon">file_download</i>
                                <a href="{{url('downloads/media_kit.zip')}}" id="kit-link" download>Download KIT</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="press-release">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1 id="press-header">Press Release</h1>
                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            
                            
                            <img src="icons/echai.png" class="press-image">
                            <p class="press-info">We constantly look at ways to do things better, so innovating as a means to overcoming hurdles is deeply ingrained in our company’s culture...</p>

                            <a target="_blank" href="https://echai.in/interviews/rightfunds-investing-made-simple" class="press-links">Read more <span class="link-arrow"><i class="material-icons">arrow_forward</i></span></a>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <img src="icons/silicon_india.png" class="press-image">
                            <p class="press-info">Chennai based Rightfunds.com believes Knowledge, Dynamism & Integrity are the cornerstones of their existence. The brainchild of Vasudev Gupta ...</p>

                            <a target="_blank" href="https://startup.siliconindiamagazine.com/vendor/rightfunds-disciplined-investing-cid-4153.html" class="press-links">Read more <span class="link-arrow"><i class="material-icons">arrow_forward</i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @extends('layouts.outside-footer')

     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
