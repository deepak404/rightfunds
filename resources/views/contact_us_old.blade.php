<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contact us | Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Keywords" content="what are mutual funds, top mutual funds, mutual fund performance, investing in mutual funds, top 10 mutual funds, best performing mutual funds, top performing mutual funds, top 5 mutual funds, equity mutual funds, top ten mutual funds">
    <meta name="author" content="rightfunds mutual funds">
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/about-us.css">
    
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/about-us-responsive.css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<nav class="navbar">
<div class="container">
    
    <div class="navbar-header">
      <button type="button" class="navbar-toggle btn btn-primary back-btn">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>

      </button>
      <a class="navbar-brand" href="/"><img src="icons/rightfunds-mutual-funds.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     
      <ul class="nav navbar-nav navbar-right navbar-btn ">
        <button type="button" class="btn btn-primary nav-back-btn" onclick="location.href='/';"><i class="fa fa-chevron-left" aria-hidden="true"></i>
&nbsp; BACK</button>
      </ul>
    </div>
</div>
  
</nav>

<div class="container">

    <div class="content">
        
        <div class="row  profile-bar">

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <p class="is-head">Contact Us</p>
                <p class="is-details">To start investing and achieve your life goals</p>
              
          </div> 

          <div class = "row adv-row">

            <div class = "col-lg-12 col-md-12 col-sm-12 contact-us-div">

              
              <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text address">
                  <img src="icons/loc.png" class = "center-block">
                  <p class = "adv-head">Location</p>
                  <p class = "adv-exp contact-exp">#1, 1<sup>st</sup> Floor, </p>
                  <p class = "adv-exp contact-exp">Balaji First Avenue,</p>
                  <p class = "adv-exp contact-exp">T.Nagar - Chennai</p>
                  <p class = "adv-exp contact-exp"> Tamil Nadu - 600017</p>
              </div>

              <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                <img src="icons/phone.png" class = "center-block">
                <p class = "adv-head">Phone</p>
                <p class = "adv-exp contact-exp">We are only a Phone call away</p>
                <p class="cont-phone">88258 88200</p>
                <!--<p class="cont-phone">(044) 4863 9050</p>-->

              </div>

              <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                <img src="icons/live_chat.png" class = "center-block">
                <p class = "adv-head">Email</p>
                <p class = "adv-exp contact-exp">Just Drop a mail</p>
                 <a id="live-chat"><p class="cont-phone">contact@rightfunds.com</p></a>

              </div>
          </div>

        </div> <!-- signup-adv-div ends -->

        </div> 

          <div class=" row contact-us-div profile-bar">
            <p id="send-mail-text" class="text-center">Just Send us Mail</p>
            <p class="is-details">Feel free to drop your queries.</p>
            <p class="text-center" id="contact_us_request_status"><p>

            <form id="contact-form" name="contact-form" >
              <div class="col-lg-12 padding-lr-zero">
                <div class="form-group">
                  <input type="text" name="name" id="name" placeholder="Name" required class="form-input form-inp">
                </div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">

                <div class="col-lg-6 col-md-6 col-sm-6 ">
                  <div class="form-group">
                    <input type="email" name="email" id="email" placeholder="Email" required class="form-input width-86">
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 ">
                  <div class="form-group">
                    <input type="number" name="mobile" id="mobile" placeholder="Mobile" required class="form-input width-86 only-no" minlength="10" maxlength="10">
                  </div>
                </div>
              </div>

              <div class="col-lg-12 padding-lr-zero">
                <div class="form-group">
                  <input type="text" name="message" id="message" placeholder="Your Message" required class="form-input">
                </div>
              </div>

              <div class="col-lg-12 padding-lr-zero">
                <div class="form-group">
                  <input type="submit" name="contact-submit" id="send-email" class="btn btn-primary" value="Send">
                </div>
              </div>

            </form>
          </div>

         

            






            




    </div> <!-- content ends -->

    <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
  
</div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.donut.js"></script>
  <script src="js/contact_us.js"></script>
  <script src="js/checker.js"></script>

</body>
</html>