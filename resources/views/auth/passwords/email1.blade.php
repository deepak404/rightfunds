<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{URL('css/bootstrap.min.css')}}">
  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>
  <script src="{{URL('js/jquery.donut.js')}}"></script>
  <script src="{{URL('js/register.js')}}"></script>
  <link rel="stylesheet" href="{{URL('css/login.css')}}">
  <link rel="stylesheet" href="{{URL('css/login-responsive.css')}}">

</head>
<body>




  
<div class = "container">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-div">
      <img class = "login-logo center-block" src="{{URL('icons/login_logo.png')}}">
    </div>


    <div class="content">
 
        <div id="enter_email_div" style="display: block">
              <div class = "row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <p class="login-info-text">Enter Your emails?</p>
                    <p class="login-sub-text">We will send you a link to reset your password.</p>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div id="trouble-text">
                      @if (session('status'))
                          <div class="">
                              <p class="text-center password_reset_success">{{ session('status') }}</p>
                          </div>
                      @endif
                      <form id="" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            

                            <div class="col-xs-12" id="reset-email-div">
                                <input id="email" type="email" class="reset-email" placeholder="Email" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block reset-email-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div id = "button-div" class = "text-center">
                          <button type="submit" class="btn btn-primary center-block" name="reset-btn" id="reset-btn" value="RESET">RESET</button>
                          <button type = "button" onclick="location.href='/login';" id = "go_back" class = "btn btn-primary">BACK</button>
                        </div>
                      </form>
                    </div>
                  </div>
              
              </div> 
              <div class="hr"></div>
            </div>
            

            <div id = "no_acct_div">             
              <div class="row">
                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <p class="login-info-text">Don't have an account ?</p>
                  <p class="login-sub-text">Create one now and start investing </p>
                 <div class="form-group">
                    <input type="button" class = " center-block btn btn-primary" onclick="location.href='/register';" id="signup-btn" value="SIGNUP">
                  </div>
                </div>
              </div>
            </div>

        <div class="login-footer">
          <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 footer-link padding-lr-zero">

              <ul class="footer-links list-inline">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Disclosures</a></li>
                <li><a href="#">Contact</a></li>
              </ul>
          </div>

          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 padding-lr-zero disc-div">
            <p id="disc-text">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p> 
          </div>

          <div class="col-lg-4 col-sm-3 col-md-3 col-xs-12 padding-lr-zero social-media">

              <ul class="social-links list-inline">
                <li class="soc-img"><a href="#"><img src="{{URL('icons/twitter.png')}}"></a></li>
                <li class="soc-img"><a href="#"><img src="{{URL('icons/facebook.png')}}"></a></li>
                <li class="soc-img"><a href="#"><img src="{{URL('icons/linked_in.png')}}"></a></li>
              </ul>

          </div>



          <div class=" col-lg-8 col-md-9 col-sm-9 col-xs-12 ">
            <p id="copyright-text" >&copy;2017, Rightfunds.com</p>
          </div>


        </div>

       

    </div> <!--Content Ends -->

</div>



  <script>
    
    /*$(document).ready(function(){

        $('#trbl-log').on('click',function(){
          $('#login_div').slideToggle(250,'swing');
          $('#trouble_login').slideToggle();
        });

        $('#back-login').on('click',function(){
          $('#trouble_login').slideToggle(250,'swing');
          $('#login_div').slideToggle();
          
        });

        $('#i_forgot,#i_want').on('click',function(){
          $('#trouble_login').slideToggle();
          $('#enter_email_div').slideToggle(250,'swing');
        });

        $('#go_back').on('click',function(){
          $('#login_div').slideToggle(250,'swing');
           $('#enter_email_div').slideToggle(250,'swing');
        });
    });*/

  </script>

</body>
</html>
