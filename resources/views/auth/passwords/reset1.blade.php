<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{URL('css/bootstrap.min.css')}}">
  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>
  <script src="{{URL('js/jquery.donut.js')}}"></script>
  <script src="{{URL('js/register.js')}}"></script>
  <link rel="stylesheet" href="{{URL('css/login.css')}}">
  <link rel="stylesheet" href="{{URL('css/login-responsive.css')}}">


</head>
<body>




  
<div class = "container">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-div">
      <img class = "login-logo center-block" src="{{URL('icons/login_logo.png')}}">
    </div>


    <div class="content">

        <div id = "login_div">
          <div class = "row">
            <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 form-div padding-lr-zero" id="login_div">
              <p class="login-info-text">Reset Password</p>
              <p class="login-sub-text">Reset your password to login to your account.</p>


              <form action="{{ url('/password/reset') }}" method="POST" role = "form" id="login_form">
                {{csrf_field()}}
                
                  <div class="form-group has-error {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" name="email" id = "email" required placeholder="Enter your emailss" value="{{$email or old('email')}}">
                        <span class="help-block help-center" id="email_error"></span>
                         @if ($errors->has('email'))
                              <span class="help-block reset-email-error">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                  </div>

                  <div class="form-group has-error {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" id = "password" required placeholder="Enter New Password">
                        <span class="help-block help-center" id="password_error"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                  </div>

                  <div class="form-group has-error {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" name="password_confirmation" id = "password" required placeholder="Confirm Password">
                        <span class="help-block help-center" id="password_error"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                  </div>


                  <div class="form-group">
                    <input type="submit" class = " center-block btn btn-primary" id="login-btn" value="RESET" id = "submit">
                  </div>

              </form>
            </div>
          </div> <!--Row ends -->
          <div class = "hr"></div>   
        </div>  <!--login_div ends -->

        

       

        <div class="login-footer">
          <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 footer-link padding-lr-zero">

              <ul class="footer-links list-inline">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Disclosures</a></li>
                <li><a href="#">Contact</a></li>
              </ul>
          </div>

          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 padding-lr-zero disc-div">
            <p id="disc-text">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p> 
          </div>

          <div class=" col-lg-7 col-md-9 col-sm-9 col-xs-12 ">
            <p id="copyright-text" >&copy;2017, Rightfunds.com</p>
          </div>

          <div class="col-lg-5 col-sm-3 col-md-3 col-xs-12 padding-lr-zero social-media">

              <!--<ul class="social-links list-inline">
                <li class="soc-img"><a href="#"><img src="icons/twitter.png"></a></li>
                <li class="soc-img"><a href="#"><img src="icons/facebook.png"></a></li>
                <li class="soc-img"><a href="#"><img src="icons/linked_in.png"></a></li>
              </ul>-->


              <div class="col-lg-4 col-sm-4 col-md-4 col-xs-2   padding-lr-zero">
                <a href="https://twitter.com/rightfundsindia"><img  class="center-block" src="/icons/twitter.png"></a>
              </div>

              <div class="col-lg-4 col-sm-4 col-md-4  col-xs-2 padding-lr-zero" >
               <a href="https://www.facebook.com/rightfunds/"><img  class="center-block" src="/icons/facebook.png"></a>
              </div>

              <div class="col-lg-4 col-sm-4 col-md-4  col-xs-2  padding-lr-zero">
                <a href="https://www.linkedin.com/company/rightfunds.com"><img class="center-block"  src="/icons/linked_in.png"></a>
              </div>
          </div>


        </div>

       

    </div> <!--Content Ends -->

</div>




</body>
</html>
