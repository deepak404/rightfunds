    <!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Portfolio Details</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="{{url('css/version2/footer.css')}}">
        <link rel="stylesheet" href="{{url('css/version2/register.css')}}">
        <link rel="stylesheet" href="{{url('css/version2/register-responsive.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/version2/font-and-global.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="{{url('css/version2/forgot-password.css')}}">

      <style type="text/css">

      </style>
    </head>
    <body>


        <section id="register-section">
            <div class="container box-shadow-all br" id="register-container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <img src="{{url('icons/nav-logo.png')}}" class="logo">
                        <div class = "col-lg-6 col-md-6 col-sm-6 border-right">
                            <p class="heading">Get your Free Account now.</p>
                            <p class="sub-heading">Invest in Best Equity,Debt,Tax saving mutual funds with rightfunds.com</p>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="footer-container">
                                <p class="sub-heading">Follow us on</p>
                                <ul class="list-inline">
                                    <li><a href="https://www.facebook.com/rightfunds/"><img src="{{url('icons/facebook-icon.png')}}" class="social-icon"></a></li>
                                    <li><a href="https://twitter.com/rightfundsindia"><img src="{{url('icons/twitter-icon.png')}}" class="social-icon"></a></li>
                                    <li><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('icons/linked-icon.png')}}" class="social-icon"></a></li>
                                </ul>
                                <ul class="list-inline">
                                    <li class="footer-links"><a target = "_blank" href="{{url('/about-us')}}">About</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/terms')}}">Terms of Use</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/contact-us')}}">Contact</a></li>
                                </ul>
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6" id="second-col">
                            <div class = "col-lg-6 col-md-6 col-sm-6 p-lr-zero">  
                                <p class="heading form-heading  active-form" id="login-form-head">Forgot Password</p>    
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero">
                            <p class="login-info-text">Enter Your email</p>
                            <p class="login-sub-text">We will send you a link to reset your password.</p>
                          </div>

                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  p-lr-zero">
                            <div id="trouble-text">
                              @if (session('status'))
                                  <div class="">
                                      <p class="text-center password_reset_success">{{ session('status') }}</p>
                                  </div>
                              @endif
                              <form id="" method="POST" action="{{ url('/password/email') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    

                                   <div class="col-xs-12 p-lr-zero" id="reset-email-div">
                                        <!-- <input id="email" type="email" class="reset-email input-field" placeholder="Email" name="email" value="{{ old('email') }}"> -->

                                        <div class="form-group">
                                            <input type="text" name = "email" id="email" class="input-field" value = "{{old('email')}}" required>
                                            <label>Email</label>
                                            <span class="text-danger"></span>
                                        </div>

                                        @if ($errors->has('email'))
                                            <span class="text-danger email-error">
                                                {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div id = "button-div" class = "text-center">
                                  <button type="submit" class="btn btn-primary" name="reset-btn" id="reset-btn" value="RESET">RESET</button>
                                  <button type = "button" onclick="location.href='/login';" id = "go_back" class = "btn btn-primary">BACK</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


     <script src="{{url('js/jquery.min.js')}}"></script>
     <script src="{{url('js/register.js')}}"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


     </script>
    </body>
</html>
