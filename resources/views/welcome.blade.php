<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> Buy Mutual Funds | Buy Mutual Funds Online | Rightfunds </title>
        <meta name="description" content="Buy top performing mutual funds in India with Rightfunds. Paperless Signup, Zero Fees, Tax Free investments. Invest in the best Equity, Debt and Tax Saving mutual funds online."
">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
       <!--  <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/footer.css"> -->
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css?v=1.5">
        <link rel="stylesheet" href="css/landing.css?v=1.5">
        <link rel="stylesheet" href="css/landing-responsive.css?v=1.5">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        <div id="loader" class="loader"></div>

        <section id="introduction-tab">

            <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
              <div class="container" id="navbar-container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{url('/home')}}"><img src="icons/nav-logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <!--  <ul class="nav navbar-nav">
                    <li class="active-menu"><a href="#">Home</a></li>
                    <li><a href="#">Account Statement</a></li>
                    <li><a href="#">Settings</a></li>
                    
                  </ul> -->
                  
                  <ul class="nav navbar-nav navbar-right" id="register-nav">
                    <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/faq')}}">FAQ</a></li>
                    <li><a href="https://www.sipcalculator.io" target="_blank">SIP Calculator</a></li>
                    <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>

            <div class="container" id="intro-container"> 
                <div class="row">   
                    <div class = "col-md-6 col-sm-12 p-lr-zero" id="intro-container">
                        <div class = "col-md-12 p-lr-zero" id="intro-text-container">  
                            <h1>Really Simple,</h1>
                            <h1>Automated Investing.</h1>
                            <p class="head-info">Rightfunds is an Investment Platform for the best Equity, Debt & Tax Saving Mutual Funds.</p>

                        </div>                        
                    </div>
                    <div class="col-md-6 col-sm-12" id="reg-container">
                        <div id="reg-form-holder" class="center-block">
                            <h3 class="text-center">Get you free account today !</h3>
                            <form id="register-form" action="{{ url('/register') }}" name="register-form" class="col-md-12">  
                                {{csrf_field()}}
                                <div class = "col-md-12 p-lr-zero">
                                    <div class="form-group">
                                        <input type="text" name = "name" id="name" class="input-field text-field" placeholder="Name as per PAN" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-md-12 p-lr-zero">
                                    <div class="form-group">
                                        <input type="text" name = "email" id="email" placeholder="Email" class="input-field" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-md-12 p-lr-zero">
                                    <div class="form-group">
                                        <input type="text" name = "mobile" id="mobile" placeholder="Mobile" class="input-field num-field" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-md-12 p-lr-zero">
                                    <div class="form-group">
                                        <input type="password" name = "password" id="password" class="input-field" placeholder="Password" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-md-12 p-lr-zero">
                                    <div class="form-group">
                                        <input type="password" name = "password-confirmation" id="password-confirmation" class="input-field" placeholder="Confirm Password" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-md-12 p-lr-zero">
                                    <div class="form-group">
                                        <input type="submit" name = "submit" id="submit" value="Register" class="btn center-block btn-primary register-btn">
                                    </div>
                                </div>

                            </form>

                            <form name="otp-ver" id="otp-ver" class="form-inline">
                              <div class="form-group otp-form-group">
                                <p class="text-center" id="otp-info-text">Via SMS for mobile number verification</p>
                                <div class="row otp-row">
                                  <div class = "col-lg-12 col-md-12 col-sm-12" id="otp-container">
                                      
                                        <input type="text" name="otp1" id="otp1" class="otp-field input-field" minlength="1" maxlength="1" required>
                                        <input type="text" name="otp2" id="otp2" class="otp-field input-field" minlength="1" maxlength="1" required>
                                        <input type="text" name="otp3" id="otp3" class="otp-field input-field" minlength="1" maxlength="1" required>
                                        <input type="text" name="otp4" id="otp4" class="otp-field input-field" minlength="1" maxlength="1" required>
                                  </div>
                                </div>
                              </div>
                              <div id="otp_error_msg"><span><span></div>
                              <div id="otp_success_msg"><span><span></div>
                              <div class="row">
                                <p id = "didn-t" class="text-center">Didn't receive the OTP ?</p>
                                <p><span id="resend">Resend OTP</span></p>
                              </div>

                              <input type="submit" name="verify" value="VERIFY" id = "verify-btn" class="btn btn-primary center-block">

                              <p id="back-to-s">Back to Sign up</p>
                            </form>
                        </div>
                    </div>  
                </div>  
            </div>  
            <img src="icons/plane.svg" id="plane">
        </section>   

        <section id="company-info-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class = "col-md-4 col-sm-4 p-l-zero">
                            <img src="icons/strongbox.svg" class="info-images center-block">
                            <p class="info-text text-center">Total assets under management</p>
                            <h2 class="info-numbers text-center">Rs.30,00,00,000+</h2>
                        </div>

                        <div class = "col-md-4 col-sm-4 p-l-zero">
                            <img src="icons/tax-return.svg" class="info-images center-block">
                            <p class="info-text text-center">Net Returns generated</p>
                            <h2 class="info-numbers text-center">Rs.5,00,00,000+</h2> 
                        </div>

                        <div class = "col-md-4 col-sm-4 p-l-zero">
                            <img src="icons/certificate.svg" class="info-images center-block">
                            <p class="info-text text-center">No of Schemes offered</p>
                            <h2 class="info-numbers text-center">100+</h2> 
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="articles"> 
            <div class="container"> 
                <div class="row">   
                    <h1 class="text-center">Articles to help you get started with investing in mutual funds.</h1>
                    <div class="col-md-12 col-sm-12">   
                        <div class="col-md-4 col-sm-4 article-parent">  
                            <div class="col-md-12 p-lr-zero box-shadow">
                                <div class="article-banner" id="article-1">
                                    <!-- <img src="icons/pexels-photo.jpg"> -->
                                </div> 
                                <div class="article-content">
                                    <h3>Why should I Invest my money ?</h3>
                                    <p class="article-intro">
                                        In order to understand why we should begin investing our money let us understand three key financial concepts that ..
                                    </p>

                                    <a class="article-link" href="https://www.rightfunds.com/blog/why_should_i_invest_my_money">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 article-parent">  
                            <div class="col-md-12 p-lr-zero box-shadow">
                                <div class="article-banner" id="article-2">
                                    <!-- <img src="icons/mutual-funds.jpg"> -->
                                </div> 
                                <div class="article-content">
                                    <h3>Introduction to Mutual Funds</h3>
                                    <p class="article-intro">
                                        A mutual fund is a professionally managed investment scheme run by an asset management company (AMC). It involves pooling..
                                    </p>

                                    <a class="article-link" href="https://www.rightfunds.com/blog/introduction_to_mutual_funds">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 article-parent">  
                            <div class="col-md-12 p-lr-zero box-shadow">
                                <div class="article-banner" id="article-3">
                                    <!-- <img src="icons/piggy-bank.jpg"> -->
                                </div> 
                                <div class="article-content">
                                    <h3>How do I save on taxes with Mutual funds?</h3>
                                    <p class="article-intro">
                                        Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place. Roughly 2% of the..
                                    </p>

                                    <a class="article-link" href="https://www.rightfunds.com/blog/how_do_i_save_on_taxes_with_mutual_funds">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>  
            </div>  
        </section>


        <section id="top-performing-mutual-fund">   
            <div class="container-fluid"> 
                <div class="row">   
                    <h2 class="text-center">Top Performing Mutual fund Schemes</h2>
                    <a href="javascript:void(0);" class="btnPrevious">Previous</a>
                    <a href="javascript:void(0);" class="btnNext">Next</a>
                    <div class="carousel">
                        <ul>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = " amc-container">
                                        <img src="icons/reliance.png" class="center-block " alt="buy mutual funds - Reliance">
                                        <h4 >Reliance Top 200 fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">12-06-2007</p>
                                            <p class="amc-info-text">S&P BSE 200</p>
                                            <p class="amc-info-text">Rs 3,606 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">18.1%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">14.0%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">17.4%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/kotak.png" class="center-block " alt="buy mutual funds - Kotak">
                                        <h4 >Kotak Select Focus fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">20-08-2009</p>
                                            <p class="amc-info-text">NIFTY 50</p>
                                            <p class="amc-info-text">Rs 9,867 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">20.1</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">18.5</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">21.0</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/L&T.png" class="center-block " alt="buy mutual funds - l&t">
                                        <h4 >L&T Midcap fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">23-07-2004</p>
                                            <p class="amc-info-text">NIFTY MIDCAP 100</p>
                                            <p class="amc-info-text">Rs 1,018 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">30.7</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">25.2</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">28.1</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/hdfc.png" class="center-block " alt="buy mutual funds - HDFC">
                                        <h4 >HDFC Tax Saver</h4>
                                        <p >Minimum Investment amount Rs.500</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">18-12-1995</p>
                                            <p class="amc-info-text">NIFTY 500</p>
                                            <p class="amc-info-text">Rs 6,113 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">19.6</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">11.9</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">16.8</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/icicipru.png" class="center-block " alt="buy mutual funds - ICICI">
                                        <h4 >ICICI Top 100 Fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">19-06-1998</p>
                                            <p class="amc-info-text">NIFTY 50</p>
                                            <p class="amc-info-text">Rs 2,173 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">17.8%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">12.4%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">16.7%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/tata.png" class="center-block " alt="buy mutual funds - Tata">
                                        <h4 >Tata Equity P/E fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">15-06-2004</p>
                                            <p class="amc-info-text">S&P BSE SENSEX</p>
                                            <p class="amc-info-text">Rs 1,447 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">29.3%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">22.1%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">23.1%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/Birla.png" class="center-block " alt="buy mutual funds - Birla">
                                        <h4 >Birla Tax Relief 96</h4>
                                        <p >Minimum Investment amount Rs.500</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">06-03-2008</p>
                                            <p class="amc-info-text">S&P BSE 200</p>
                                            <p class="amc-info-text">Rs 3,501 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">22.6%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">20.1%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">21.8%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/sundaram.png" class="center-block " alt="buy mutual funds - Sundaram">
                                        <h4 >Sundaram Rural India fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">19-04-2006</p>
                                            <p class="amc-info-text">S&P BSE 500</p>
                                            <p class="amc-info-text">Rs 1,679 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">24%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">21.8%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">21.9%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/reliance.png" class="center-block " alt="buy mutual funds - Sundaram">
                                        <h4 >Reliance Banking Fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">21-05-2003</p>
                                            <p class="amc-info-text">NIFTY BANK</p>
                                            <p class="amc-info-text">Rs 2,718 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">17.9%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">9.5%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">17.4%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/idfc.png" class="center-block " alt="buy mutual funds - Sundaram">
                                        <h4 >IDFC Infrastructure Fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">28-02-2011</p>
                                            <p class="amc-info-text">NIFTY INFRA</p>
                                            <p class="amc-info-text">Rs 578 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">25.9%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">13.9%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">17.0%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/axis.png" class="center-block " alt="buy mutual funds - Sundaram">
                                        <h4 >Axis Focus 25 Fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">29-01-2012</p>
                                            <p class="amc-info-text">NIFTY 50</p>
                                            <p class="amc-info-text">Rs 2192 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">23.5%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">10%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">16.4%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class = "text-center amc-parent-container">  
                                    <div class = "amc-container">
                                        <img src="icons/principal.png" class="center-block " alt="buy mutual funds - Sundaram">
                                        <h4 >Principal Blue chip fund</h4>
                                        <p >Minimum Investment amount Rs.5000</p>

                                        <div class = "scheme-info-header">  
                                            <p class="amc-info-text">Launch Date</p>
                                            <p class="amc-info-text">Benchmark</p>
                                            <p class="amc-info-text">Asset Size</p>
                                        </div> 
                                        <div class = "scheme-info-detail">  
                                            <p class="amc-info-text">20-10-2008</p>
                                            <p class="amc-info-text">NIFTY MIDCAP</p>
                                            <p class="amc-info-text">Rs 1258 Cr</p>
                                        </div>
                                        <div class = "return-block">
                                            <h4>Returns</h4>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">1 Year</p>
                                            <h3 class="green return-perc">22.9%</h3>
                                        </div>
                                        <div class = "return-perc-div">
                                            <p class="amc-year">3 Year</p>
                                            <h3 class="green return-perc">14.3%</h3>
                                        </div> 
                                        <div class = "return-perc-div">
                                            <p class="amc-year">5 Year</p>
                                            <h3 class="green return-perc">26.5%</h3>
                                        </div>   
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    

<!-- 
                    <div id="direction-holder">
                        <p class="text-center" id="toggle-card-parent"><span class="toggle-scheme" id="show-one"><i class="material-icons">keyboard_arrow_left</i></span><span class="toggle-scheme" id="show-two"><i class="material-icons">keyboard_arrow_right</i></span></p> 
                    </div>  -->
                                   
                    </div>  <!-- #performing funds container ends -->
                </div>  
            </div>  
        </section>

        <section id="why-choose-us">    
            <div class="container"> 
                <div class="row">   
                    <h2 class="text-center">Why Choose us to help you invest your money ?</h2>
                    <div class = "col-lg-12 col-md-12 col-sm-12 feature-container">   
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/zero-fee.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Zero Fees</h4>
                            <p class="feature-info text-center">No hidden costs. We are paid by the Asset Management Companies. You pay us nothing at all.</p>
                        </div> 
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/invest-online.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Invest Online</h4>
                            <p class="feature-info text-center">Make and monitor your investments in just a few clicks. No more paper work.</p>
                        </div> 
                         <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/custom-portfolio.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Customized Portfolio</h4>
                            <p class="feature-info text-center">Build and track your own Portfolios based on your desired level of risk.</p>
                        </div>  
                    </div>  

                    <div class = "col-lg-12 col-md-12 col-sm-12 feature-container">   
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/bank-grade.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Bank Grade Security</h4>
                            <p class="feature-info text-center">256 bit SSL Encryption. Your investments go directly from your bank to the mutual fund</p>
                        </div> 
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/tax-free.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Tax Free Returns</h4>
                            <p class="feature-info text-center">The returns made from Equity mutual funds are completely tax free after a year of staying invested.</p>
                        </div> 
                         <div class = "col-lg-4 col-md-4 col-sm-4">  
                            <img src="icons/secure.png" class="center-block choose-image">
                            <h4 class="text-center feature-heading">Secure Transaction</h4>
                            <p class="feature-info text-center">Orders routed through Bombay Stock Exchange in real time. Secure delivery and fulfillment.</p>
                        </div>  
                    </div>  

                </div>  
            </div>  
        </section>  

        <section id="invest-now-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 bg">
                        <p class="text-center" id="invest-now-text"><span>Take your first step towards financial independance!</span><span><a href="{{url('/register')}}" id="inv-link">Invest Now</a></span></p>
                    </div>
                </div>
            </div>
        </section>

        <!-- <section id="testimony">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="text-center">What are happy investors say ?</h2>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                          
                          <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                          </ol>

                          
                          <div class="carousel-inner col-md-12 col-xs-12">
                            <div class="item col-xs-12 col-md-12 active">
                              <div class=" col-md-4 col-xs-12">
                                    <div class="img-holder">
                                        <img class="img-circle" src="icons/profile_icon.jpg"></img>
                                        <div class="detail-holder">
                                            <h3>Naveen</h3>
                                            <h6>Chennai</h6>
                                        </div>  
                                    </div>
                                    
                              </div>
                              <div class="col-xs-12 col-md-8">
                                  <p class="testimony-text">Thank you team Rightfunds for such good customer care and loyalty! Time and patience taken by the team to help me invest my money in all the right channels is truly commendable.</p>
                              </div>
                            </div>

                            <div class="item col-xs-12 col-md-12">
                              <div class=" col-md-4 col-xs-12">
                                    <div class="img-holder">
                                        <img class="img-circle" src="icons/profile_icon.jpg"></img>
                                        <div class="detail-holder">
                                            <h3>Naveen</h3>
                                            <h6>Chennai</h6>
                                        </div>  
                                    </div>
                                    
                              </div>
                              <div class="col-xs-12 col-md-8">
                                  <p class="testimony-text">Thank you team Rightfunds for such good customer care and loyalty! Time and patience taken by the team to help me invest my money in all the right channels is truly commendable.</p>
                              </div>
                            </div>

                            <div class="item col-xs-12 col-md-12">
                              <div class=" col-md-4 col-xs-12">
                                    <div class="img-holder">
                                        <img class="img-circle" src="icons/profile_icon.jpg"></img>
                                        <div class="detail-holder">
                                            <h3>Naveen</h3>
                                            <h6>Chennai</h6>
                                        </div>  
                                    </div>
                                    
                              </div>
                              <div class="col-xs-12 col-md-8">
                                  <p class="testimony-text">Thank you team Rightfunds for such good customer care and loyalty! Time and patience taken by the team to help me invest my money in all the right channels is truly commendable.</p>
                              </div>
                            </div>
                          </div>

                          
                          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                    </div>     
                </div>
            </div>
        </section> -->

        <section id="our-partners">
            <div class="container"> 
                <div class="row">
                    <h2 class="text-center">Our Partners</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12 logo-container">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/franklin-mutual-funds.png" class="partners-logo img img-responsive" alt="Franklin mutual fund" id="franklin">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/dsp-mutual-funds.png" class="partners-logo img img-responsive" alt="dsp blackrock mutual fund" id="dsp">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/Birla-mutual-funds.png" class="partners-logo img img-responsive" alt="birla sunlife mutual fund" id="birla">
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/icici-mutual-funds.png" class="partners-logo img img-responsive" alt="icici prudential mutual fund" id="icici">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/kotak-mutual-funds.png" class="partners-logo img img-responsive" alt="kotak mutual fund" id="kotak">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/axis-mutual-funds.png" class="partners-logo img img-responsive" alt="axis mutual fund" id="axis">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12 logo-container">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/reliance-mutual-funds.png" class="partners-logo img img-responsive" alt="reliance mutual fund" id="reliance">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/sbi-mutual-funds.png" class="partners-logo img img-responsive" alt="sbi mutual fund" id="sbi">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/hdfc-mutual-funds.png" class="partners-logo img img-responsive" alt="hdfc mutual fund" id="hdfc">
                        </div>        
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/l&t-mutual-funds.png" class="partners-logo img img-responsive" alt="l&t mutual fund" id="lt">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/tata-mutual-funds.png" class="partners-logo img img-responsive" alt="tata mutual fund" id="tata">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/sundaram-mutual-funds.png" class="partners-logo img img-responsive" id="sundaram" alt="sundaram mutual fund" id="sundaram">
                        </div>
                      </div>                         
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12 logo-container">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/iifl.png" class="partners-logo img img-responsive" alt="iifl" id="iifl">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/principal.png" class="partners-logo img img-responsive" alt="principal mutual fund" id="principal">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/invesco.png" class="partners-logo img img-responsive" alt="invesco mutual fund" id="invesco">
                        </div>        
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding-lr-zero text-center margin-logo">
                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/boi.png" class="partners-logo img img-responsive" alt="boi mutual fund" id="boi">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/idfc.png" class="partners-logo img img-responsive" alt="idfc mutual fund" id="idfc">
                        </div>

                        <div class="col-xs-4 text-center">
                          <img src= "icons/mutual-funds-companies/uti.png" class="partners-logo img img-responsive" id="uti" alt="uti mutual fund" id="uti">
                        </div>
                      </div>                         
                    </div>
            </div>  
        </section>  

<section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="#">Privacy Policy</a></li>
                            <li class="footer-links"><a href="#">Terms of Use</a></li>
                            <li class="footer-links"><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;2017,Rightfunds.com</p>
                    </div> -->

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-4 col-md-4 col-sm-4">  
                            

<svg id="footer-logo" xmlns="http://www.w3.org/2000/svg" viewBox="-1266.246 2522.658 2463.121 512.001"><defs><style>.ab{fill:#fff;}</style></defs><g transform="translate(-1325 2022)"><g transform="translate(58.754 503.906)"><path class="ab" d="M-1442.34,1513.377s102.832-40.05,102.832-171.026c0-132.6-132.6-163.45-132.6-163.45H-1645.3V1289.31h148.838s51.418,9.743,51.418,57.37c0,65.488-53.582,66.571-53.582,66.571H-1645.3v113.117h73.607l108.244,160.744h146.131Z" transform="translate(1645.301 -1178.9)"></path><path class="ab" d="M-1377.861,1222.1v113.117H-1597.6c38.967-25.98,77.936-66.029,88.762-113.117Z" transform="translate(1855.764 -988.291)"></path><path class="ab" d="M-1286.395,1179v109.869h-221.9c-15.152-48.711-47.627-88.761-89.3-109.869Z" transform="translate(1855.764 -1178.459)"></path></g><g transform="translate(836.496 500.658)"><path class="ab" d="M-1398.408,1359.569l-35.721-64.405h-28.145v64.405H-1500.7V1178.8h84.43c37.889,0,61.16,24.354,61.16,58.453,0,31.933-20.566,49.251-39.512,53.582l40.594,68.734Zm3.787-122.316c0-15.155-11.9-24.355-27.059-24.355h-40.594v48.71h40.594C-1405.986,1261.607-1394.621,1252.407-1394.621,1237.253Z" transform="translate(1505.571 -1176.093)"></path><path class="ab" d="M-1468.5,1359.569V1178.8h38.426v180.77Z" transform="translate(1647.645 -1176.093)"></path><path class="ab" d="M-1456.4,1271.932c0-56.827,43.3-93.632,96.338-93.632,37.348,0,61.16,18.943,74.691,40.593l-31.934,17.318a51.071,51.071,0,0,0-42.758-23.813c-33.014,0-56.828,25.438-56.828,59.534s23.814,59.535,56.828,59.535c25.982,0,41.137-16.779,47.088-35.722h-56.287v-34.1h98.5c0,64.406-35.182,103.373-92.553,103.373C-1413.1,1365.023-1456.4,1328.76-1456.4,1271.932Z" transform="translate(1701.034 -1178.3)"></path><path class="ab" d="M-1294.861,1359.569v-76.312h-85.512v76.313H-1418.8V1178.8h38.428v70.9h85.512v-70.9h38.971v180.77Z" transform="translate(1866.936 -1176.093)"></path><path class="ab" d="M-1331.258,1359.569V1212.9H-1384.3v-34.1h252.211v34.1h-160.746v146.672Z" transform="translate(2019.157 -1176.093)"></path><path class="ab" d="M-1353.1,1359.569V1178.8h127.729v34.1h-89.3v37.886h87.68v34.1h-87.68v75.231H-1353.1Z" transform="translate(2156.822 -1176.093)"></path><path class="ab" d="M-1324.6,1287.146V1178.9h38.967v107.163c0,25.438,14.611,42.756,42.756,42.756,27.6,0,42.217-17.318,42.217-42.756V1178.9h39.51v108.245c0,44.922-26.52,75.771-81.725,75.771C-1298.082,1362.376-1324.6,1331.526-1324.6,1287.146Z" transform="translate(2282.57 -1175.653)"></path><path class="ab" d="M-1163.42,1359.569l-86.055-117.987v117.987H-1287.9V1178.8h39.508l83.891,113.657V1178.8h38.428v180.77Z" transform="translate(2444.501 -1176.093)"></path><path class="ab" d="M-1251.4,1359.569V1178.8h71.441c56.828,0,95.8,36.262,95.8,90.385,0,54.663-39.51,90.385-95.8,90.385Zm127.729-90.384c0-31.933-19.484-56.288-56.287-56.288h-33.016v113.116h33.016C-1144.776,1326.014-1123.671,1300.034-1123.671,1269.186Z" transform="translate(2605.548 -1176.093)"></path><path class="ab" d="M-1217.8,1336.339l21.109-30.309a77.962,77.962,0,0,0,57.369,24.895c21.65,0,31.391-9.741,31.391-20.023,0-31.392-103.914-9.743-103.914-77.4,0-30.308,25.979-55.2,68.736-55.2,28.686,0,52.5,8.659,70.359,25.437l-21.65,28.686c-14.611-13.531-34.1-20.025-52.5-20.025-16.236,0-25.437,7.036-25.437,17.861,0,28.685,103.914,9.2,103.914,76.854,0,33.015-23.812,57.911-72.523,57.911C-1176.125,1365.024-1201.021,1353.657-1217.8,1336.339Z" transform="translate(2753.8 -1178.301)"></path><path class="ab" d="M-1501.6,1286.791c0-11.908,9.74-22.191,21.648-22.191s21.648,10.283,21.648,22.191a21.711,21.711,0,0,1-21.648,21.648A21.711,21.711,0,0,1-1501.6,1286.791Z" transform="translate(1501.6 -797.521)"></path><path class="ab" d="M-1489.4,1331.933c0-55.746,42.215-93.633,96.336-93.633,39.512,0,62.785,21.648,75.232,44.381l-33.016,16.236c-7.576-14.613-23.812-26.52-42.217-26.52-33.014,0-56.826,25.438-56.826,59.535s23.813,59.535,56.826,59.535c18.4,0,34.641-11.908,42.217-26.521l33.016,16.236c-12.99,22.73-35.721,44.381-75.232,44.381C-1447.184,1425.024-1489.4,1387.138-1489.4,1331.933Z" transform="translate(1555.427 -913.564)"></path><path class="ab" d="M-1455,1331.933c0-54.123,40.051-93.633,94.715-93.633s94.172,38.967,94.172,93.633-39.51,93.633-94.172,93.633C-1415.49,1425.024-1455,1386.056-1455,1331.933Zm148.838,0c0-34.1-21.65-59.535-54.664-59.535-33.555,0-55.2,25.438-55.2,59.535,0,33.557,21.648,59.535,55.2,59.535C-1327.271,1390.927-1306.162,1365.489-1306.162,1331.933Z" transform="translate(1707.212 -913.564)"></path><path class="ab" d="M-1258.246,1419.57V1289.135l-50.875,130.436H-1325.9l-50.875-130.436V1419.57H-1415.2V1238.8h54.123l43.838,113.115,43.84-113.115h54.123v180.77Z" transform="translate(1882.82 -911.358)"></path></g></g></svg>
                            <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
                            <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                            <p class="footer-info">Company</p>
                            <ul class="footer-list">
                                <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                                <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                                <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                            <p class="footer-info">Support</p>
                            <ul class="footer-list">
                                <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                                <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                                <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                            </ul>
                        </div> 
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                            <p class="footer-info">Follow us</p>
                            <ul class="list-inline" id="social-parent">
                                <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook-footer-logo.png" class="footer-social"></a></li>
                                <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter-footer-logo.png" class="footer-social"></a></li>
                                <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linkedin-footer-logo.png" class="footer-social"></a></li>
                            </ul>
                        </div>       
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                        <p class="footer-info p-l-15">AMC Partners</p>
                        <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span> |
                        <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin Templeton Mutual Fund</span>.</p>
                        <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
                    </div>   
                </div>  
            </div>  
        </section>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <!-- <script src="js/version2/landing.js?v=1.1"></script> -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

     <script type="text/javascript">
         $(document).ready(function(){


            $(function(){
            var carousel = $('.carousel ul');
            var carouselChild = carousel.find('li');
            var clickCount = 0;
            
            itemWidth = carousel.find('li:first').width()+1; //Including margin

            //Set Carousel width so it won't wrap
            carousel.width(itemWidth*carouselChild.length);

            //Place the child elements to their original locations.
            refreshChildPosition();
            
            //Set the event handlers for buttons.
            $('.btnNext').click(function(){
                clickCount++;
                
                //Animate the slider to left as item width 
                carousel.finish().animate({
                    left : '-='+itemWidth
                },'350', function(){
                    //Find the first item and append it as the last item.
                    lastItem = carousel.find('li:first');
                    lastItem.remove().appendTo(carousel);
                    lastItem.css('left', ((carouselChild.length-1)*(itemWidth))+(clickCount*itemWidth));
                });
            });
            
            $('.btnPrevious').click(function(){
                clickCount--;
                //Find the first item and append it as the last item.
                lastItem = carousel.find('li:last');
                lastItem.remove().prependTo(carousel);

                lastItem.css('left', itemWidth*clickCount);             
                //Animate the slider to right as item width 
                carousel.finish().animate({
                    left: '+='+itemWidth
                },'350');
            });

            function refreshChildPosition(){
                carouselChild.each(function(){
                    $(this).css('left', itemWidth*carouselChild.index($(this)));
                });
            }
            
            function refreshChildPositionNext(){
                carouselChild.each(function(){
                    leftVal =  parseInt($(this).css('left'));
                });
            }
        });

            setInterval(function(){ 
                // console.log('hello');
               $(".btnNext").click();
            },4000)
         });
     </script>

     <script src="js/version2/loader.js?v=1.4"></script>
     <script src="js/version2/validation.js?v=1.4"></script>
     <script src="js/register.js?v=1.4"></script>
    </body>
</html>
