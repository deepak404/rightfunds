        <section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="#">Privacy Policy</a></li>
                            <li class="footer-links"><a href="#">Terms of Use</a></li>
                            <li class="footer-links"><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;2017,Rightfunds.com</p>
                    </div> -->

                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-4 col-md-4 col-sm-4">  
                            

<svg id="footer-logo" xmlns="http://www.w3.org/2000/svg" viewBox="-1266.246 2522.658 2463.121 512.001"><defs><style>.ab{fill:#fff;}</style></defs><g transform="translate(-1325 2022)"><g transform="translate(58.754 503.906)"><path class="ab" d="M-1442.34,1513.377s102.832-40.05,102.832-171.026c0-132.6-132.6-163.45-132.6-163.45H-1645.3V1289.31h148.838s51.418,9.743,51.418,57.37c0,65.488-53.582,66.571-53.582,66.571H-1645.3v113.117h73.607l108.244,160.744h146.131Z" transform="translate(1645.301 -1178.9)"/><path class="ab" d="M-1377.861,1222.1v113.117H-1597.6c38.967-25.98,77.936-66.029,88.762-113.117Z" transform="translate(1855.764 -988.291)"/><path class="ab" d="M-1286.395,1179v109.869h-221.9c-15.152-48.711-47.627-88.761-89.3-109.869Z" transform="translate(1855.764 -1178.459)"/></g><g transform="translate(836.496 500.658)"><path class="ab" d="M-1398.408,1359.569l-35.721-64.405h-28.145v64.405H-1500.7V1178.8h84.43c37.889,0,61.16,24.354,61.16,58.453,0,31.933-20.566,49.251-39.512,53.582l40.594,68.734Zm3.787-122.316c0-15.155-11.9-24.355-27.059-24.355h-40.594v48.71h40.594C-1405.986,1261.607-1394.621,1252.407-1394.621,1237.253Z" transform="translate(1505.571 -1176.093)"/><path class="ab" d="M-1468.5,1359.569V1178.8h38.426v180.77Z" transform="translate(1647.645 -1176.093)"/><path class="ab" d="M-1456.4,1271.932c0-56.827,43.3-93.632,96.338-93.632,37.348,0,61.16,18.943,74.691,40.593l-31.934,17.318a51.071,51.071,0,0,0-42.758-23.813c-33.014,0-56.828,25.438-56.828,59.534s23.814,59.535,56.828,59.535c25.982,0,41.137-16.779,47.088-35.722h-56.287v-34.1h98.5c0,64.406-35.182,103.373-92.553,103.373C-1413.1,1365.023-1456.4,1328.76-1456.4,1271.932Z" transform="translate(1701.034 -1178.3)"/><path class="ab" d="M-1294.861,1359.569v-76.312h-85.512v76.313H-1418.8V1178.8h38.428v70.9h85.512v-70.9h38.971v180.77Z" transform="translate(1866.936 -1176.093)"/><path class="ab" d="M-1331.258,1359.569V1212.9H-1384.3v-34.1h252.211v34.1h-160.746v146.672Z" transform="translate(2019.157 -1176.093)"/><path class="ab" d="M-1353.1,1359.569V1178.8h127.729v34.1h-89.3v37.886h87.68v34.1h-87.68v75.231H-1353.1Z" transform="translate(2156.822 -1176.093)"/><path class="ab" d="M-1324.6,1287.146V1178.9h38.967v107.163c0,25.438,14.611,42.756,42.756,42.756,27.6,0,42.217-17.318,42.217-42.756V1178.9h39.51v108.245c0,44.922-26.52,75.771-81.725,75.771C-1298.082,1362.376-1324.6,1331.526-1324.6,1287.146Z" transform="translate(2282.57 -1175.653)"/><path class="ab" d="M-1163.42,1359.569l-86.055-117.987v117.987H-1287.9V1178.8h39.508l83.891,113.657V1178.8h38.428v180.77Z" transform="translate(2444.501 -1176.093)"/><path class="ab" d="M-1251.4,1359.569V1178.8h71.441c56.828,0,95.8,36.262,95.8,90.385,0,54.663-39.51,90.385-95.8,90.385Zm127.729-90.384c0-31.933-19.484-56.288-56.287-56.288h-33.016v113.116h33.016C-1144.776,1326.014-1123.671,1300.034-1123.671,1269.186Z" transform="translate(2605.548 -1176.093)"/><path class="ab" d="M-1217.8,1336.339l21.109-30.309a77.962,77.962,0,0,0,57.369,24.895c21.65,0,31.391-9.741,31.391-20.023,0-31.392-103.914-9.743-103.914-77.4,0-30.308,25.979-55.2,68.736-55.2,28.686,0,52.5,8.659,70.359,25.437l-21.65,28.686c-14.611-13.531-34.1-20.025-52.5-20.025-16.236,0-25.437,7.036-25.437,17.861,0,28.685,103.914,9.2,103.914,76.854,0,33.015-23.812,57.911-72.523,57.911C-1176.125,1365.024-1201.021,1353.657-1217.8,1336.339Z" transform="translate(2753.8 -1178.301)"/><path class="ab" d="M-1501.6,1286.791c0-11.908,9.74-22.191,21.648-22.191s21.648,10.283,21.648,22.191a21.711,21.711,0,0,1-21.648,21.648A21.711,21.711,0,0,1-1501.6,1286.791Z" transform="translate(1501.6 -797.521)"/><path class="ab" d="M-1489.4,1331.933c0-55.746,42.215-93.633,96.336-93.633,39.512,0,62.785,21.648,75.232,44.381l-33.016,16.236c-7.576-14.613-23.812-26.52-42.217-26.52-33.014,0-56.826,25.438-56.826,59.535s23.813,59.535,56.826,59.535c18.4,0,34.641-11.908,42.217-26.521l33.016,16.236c-12.99,22.73-35.721,44.381-75.232,44.381C-1447.184,1425.024-1489.4,1387.138-1489.4,1331.933Z" transform="translate(1555.427 -913.564)"/><path class="ab" d="M-1455,1331.933c0-54.123,40.051-93.633,94.715-93.633s94.172,38.967,94.172,93.633-39.51,93.633-94.172,93.633C-1415.49,1425.024-1455,1386.056-1455,1331.933Zm148.838,0c0-34.1-21.65-59.535-54.664-59.535-33.555,0-55.2,25.438-55.2,59.535,0,33.557,21.648,59.535,55.2,59.535C-1327.271,1390.927-1306.162,1365.489-1306.162,1331.933Z" transform="translate(1707.212 -913.564)"/><path class="ab" d="M-1258.246,1419.57V1289.135l-50.875,130.436H-1325.9l-50.875-130.436V1419.57H-1415.2V1238.8h54.123l43.838,113.115,43.84-113.115h54.123v180.77Z" transform="translate(1882.82 -911.358)"/></g></g></svg>
                            <p id="reg-company">&copy;Prosperity Technology Private Limited,2017</p>
                            <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
                        </div>
                        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                            <p class="footer-info">Company</p>
                            <ul class="footer-list">
                                <li><a href="{{url('/about-us')}}">About us</a></li>
                                <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                                <li><a href="{{url('/media-kit')}}">Media and Press Kit</a></li>
                            </ul>
                        </div>
                        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                            <p class="footer-info">Support</p>
                            <ul class="footer-list">
                                <li><a href="{{url('/faq')}}">FAQ</a></li>
                                <li><a href="{{url('/terms')}}">Terms of Use</a></li>
                                <li><a href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                            </ul>
                        </div> 
                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                            <p class="footer-info">Follow us</p>
                            <ul class="list-inline" id="social-parent">
                                <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook-footer-logo.png" class="footer-social"></a></li>
                                <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter-footer-logo.png" class="footer-social"></a></li>
                                <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linkedin-footer-logo.png" class="footer-social"></a></li>
                            </ul>
                        </div>       
                    </div> 
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                        <p class="footer-info p-l-15">AMC Partners</p>
                        <p id="amc-names" class ="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span> |
                        <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin Templeton Mutual Fund</span>.</p>
                        <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
                    </div>   
                </div>  
            </div>  
        </section>