<?php
    $notifications = \Auth::user()->notifications()->orderBy('created_at','desc')->get();
?>
        <nav class="navbar navbar-default navbar-custom">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/home')}}"><img src="{{url('icons/nav-logo.svg')}}" class="nav-logo"></a>
            </div>

            

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="{{ (\Request::route()->getName() == 'homePage') ? 'active-menu' : '' }}"><a href="{{url('/home')}}">Home</a></li>
                <li class="{{ (\Request::route()->getName() == 'portfolioDetails') ? 'active-menu' : '' }}"><a href="{{url('/portfolio_details')}}">Account Statement</a></li>

                @if(Auth::user()->nachDetails->nach_status == 0)
                    <li class="{{ (\Request::route()->getName() == 'accountActivation') ? 'active-menu' : '' }}"><a href="{{url('/account_activation')}}">Activate Account</a></li>
                @else
                    <li class="{{ (\Request::route()->getName() == 'preference') ? 'active-menu' : '' }}"><a href="{{url('/preference')}}">Profile</a></li>
                @endif
                
              </ul>

              
              <ul class="nav navbar-nav navbar-right">

              @if(\Request::route()->getName() == 'fundSelector')
              @else
              <li><button class="btn btn-primary" onclick="location.href = '/fund_selector';" id="nav-invest-btn">Invest Now</button></li>
              @endif
                
                <li class="lg-logout"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="material-icons" id="nav-notification">notifications</i></a>
                    <ul class="dropdown-menu notify-drop box-shadow-all">
                        <div class="notify-drop-title">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom"><i class="fa fa-dot-circle-o"></i></a></div>
                            </div>
                        </div>
                        <!-- end notify title -->
                        <!-- notify content -->
                        <div class="drop-content">
                        
                            @foreach($notifications as $notification)

                            @if(count($notifications) == 0)
                            <li>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>You have no notifications right now.</p>
                                </div>
                            </li>
                            @else
                            <li>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>{{$notification['description']}}</p>
                                <p class="time">{{date('d/m/Y',strtotime($notification['date']))}}</p>
                                </div>
                            </li>
                            @endif
                            @endforeach
                            
                            <!-- <li>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>Your KYC Video is Verified.Kindly Send us the Physical Documents.</p>
                                <p class="time">31/09/2017</p>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>You can start investing in SIP once we received your mandate.</p>
                                <p class="time">31/09/2017</p>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>Your KYC forms are received Successfully.</p>
                                <p class="time">31/09/2017</p>
                                </div>
                            </li>
                            <li>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>Your Account is activated. Happy Investing.</p>
                                <p class="time">31/09/2017</p>
                                </div>
                            </li> -->
                            
                        </div>
                      </ul>
                </li>
                <li><p id="nav-initial">{{Auth::user()->name[0]}}</p></li>
                <li>
                    
                        <p class="nav-user-name">{{Auth::user()->name}}</p>
                        <p class="nav-user-details">{{Auth::user()->email}}</p>
                    
                </li>
                <li class="sm-logout"><a href="{{url('/logout')}}">Logout <i class="material-icons" id="logout-icon">exit_to_app</i></a></li>
                <li>
                <div class="dropdown lg-logout"><i class="material-icons dropdown-toggle" data-toggle = "dropdown" id="nav-dropdown">keyboard_arrow_down</i>
                  <ul class="dropdown-menu" id="logout-dropdown">
                    <li><a href="{{url('/logout')}}">Logout <i class="material-icons" id="logout-icon">exit_to_app</i></a></li>
                  </ul>
                </div></li>
                <!-- <div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
  <span class="caret"></span></button>
  
</div> -->
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

      @yield('content')


        
        
        <section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                            <li class="footer-links"><a href="{{url('/terms')}}">Terms of Use</a></li>
                            <li class="footer-links"><a href="{{url('/contact-us')}}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;2017,Rightfunds.com</p>
                    </div>
                </div>  
            </div>  
        </section>