<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">
    <meta property="og:image" content="img/article4.jpg" />
    <meta property="og:title" content="How to make returns like Warren Buffett ?" />

	  <title>How to make returns like Warren Buffett? | Buy Mutual funds Online | Rightfunds</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
    <section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
              
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse  navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right  navbar-menubar">
                    <li><a class="active_navbar_header   " href="{{url('/blog')}}">Home</a></li> 
                    <li><a class="active_navbar_header   " href=" https://www.rightfunds.com/">Rightfunds.com</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>

    <section id="">
      <div class="container container-mod">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12">
                <img class="article-img img img-responsive" src="{{url('img/article4.jpg')}}">
            
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 article-spacing ">
                  <h2 class="content-header">How to make returns like Warren Buffett?</h2>
               
              </div>

                <div class="content-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12   ">
                  <p class="article-content">
                    When we think of Warren Buffett one of the wealthiest and savviest investors the world has ever seen, what comes to mind is an investor who may possibly be generating returns well over a 100% each year to get that wealthy. 
                  </p>
            
                  <p class="article-content">
                    
                    The real figure however is not that merry. In fact it is not even close to 50%. Warren Buffett’s company Berkshire Hathway delivered 21.6% returns year on year for the last 50 years.

                  </p>
                  <p class="article-content">
                  <strong class="strong">How did he become a billionaire then?! One may ask.</strong>
                </p>
                  <p class="article-content ">
                  
                  Without further ado, Let’s unveil the secret. Warren Buffett stumbled upon a great tool when he was really young and used it well to have what he has now. This great tool is called "<strong class="strong">COMPOUND INTEREST</strong>" . Its universal and works for anyone who is patient.

                </p>
                  <p class="article-content">
                  <strong class="strong">So how can it help you make a fortune?</strong>
                </p>

                  <p class="article-content">
                    
                    Let’s assume you invest rupees 100,000 and make 20% returns each year. This will give you rupees 20,000 in the first year, bringing your total up to 120,000 rupees at the end of year one. Now what happens if we repeat this process over say 25 years? Let’s do a bit of math to find out. 

                  </p>

                  <p class="article-content">
                  <strong class="strong">Compound Interest Formula:</strong>
                </p>

                  <p class="article-content">

                    Net amount = Initial Investment x ( 1 + returns/100 )<sup>Investment Period</sup> = 100,000 x (1 + 20/100)<sup>25</sup> = 95,39,625.

                  </p>

                  <p class="article-content">
                 Your initial investment of rupees 100,000 has grown an astounding 9,539% to rupees 95.39 Lakhs at the end of 25 years!
                  </p>

                  <p class="article-content">
                  But we’re talking about Warren Buffett like returns, so let’s say you are patient and stay invested for 50 years just like him, What do you think the returns would be? Double? Not really.</p>

                <p class="article-content">
                  And at the end of 50 years Rupees 91 Crores!
                    </p>

                  <p class="article-content">
                  
                    Remember you started out with just 1 Lakh! That’s roughly investing 8,300 rupees per month for a just one year. 
                         </p>

                  <p class="article-content">
                  
                  Now, what is common between Warren Buffett and yourself? Yes, the Compound Interest formula itself! It does no partiality and is the same for the poor and wealthy. To make the best use of it what you need to do is disciplined investing. Being disciplined and staying invested for a long period of time is the key to amassing great wealth. This is exactly what Warren Buffett did. He started investing at the age of 11 and now is 84. You could guess what would be the result of 72 years of compound interest. 
  
                  </p>

                  <p class="article-content">
                  If you think making 20% returns year on year for a long period of time is impossible, think again! The mutual fund schemes listed below have given over 20% returns for the last 20 years (courtesy, NDTV Profit)

                  </p>

                  <p class="article-content">
                    <strong class="strong"> 1) HDFC Equity Fund:</strong>Launched on January 1, 1995. If you had started an SIP of Rs.
                    5,000 in this fund on April 1, 1998, your investment would have grown to Rs. 1.95
                    crore by April 1, 2017, which means a CAGR return of 23.56 per cent over 20 years.

                  </p>
                   <p class="article-content">
                    <strong class="strong">2) Reliance Growth Fund:</strong>  This fund has generated a CAGR return of 23.68 per cent
                    since its inception. Your Rs. 5,000 SIP in this fund, started on April 1, 1998 would
                    have helped you accumulate a corpus of Rs. 2.12 crore by April 1, 2017.
                  </p>
                  <p class="article-content">
                    <strong class="strong">3) Franklin India Prima Fund: </strong>It is one of the flagship fund of Franklin Templeton
                    India. If you had started an SIP of Rs. 5,000 in this fund on April 1, 1998, your
                    investment would have grown to Rs. 2.38 crore by April 1, 2017, which means a
                    CAGR return of 25.11 per cent over 20 years.
                  </p>
                  <p class="article-content">
                    <strong class="strong"> 4) Reliance Vision Fund:</strong> Launched on October 8, 1995, it has generated a CAGR
                    return of 19.92 per cent till now. Your Rs. 5,000 SIP in this fund, started on April 1,
                    1998, would have become Rs. 1.46 crore by April 1, 2017.
                  </p>
                  <p class="article-content">
                    <strong class="strong"> 5) HDFC Top 200 Fund:</strong> This flagship fund of HDFC Mutual Fund has generated a
                    CAGR return of 20.88 per cent since its inception in September 1996. If you would
                    have started an SIP of Rs. 5,000 in this fund on April 1, 1998, your investment would
                    have grown to Rs. 1.43 crore by April 1, 2017.
                  </p>
                  <div class="img-spacing">
                    <img src="{{url('img/Warren-buffett-illustration.svg')}} " class="img img-responsive">
                  </div>

                  <p class="article-content">
                    Begin investing today with Rightfunds.com and take your first step towards Financial Independence!
                  </p>
                  
                  <!-- <p class="copy-right">
                   © Prosperity Technology Private Limited, 2017.
                  </p> -->
                  
                  <div>

                  </div>

                </div>

                </div>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section id="recommended-article">
      <div class="container container-mod   ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head other-articles">More articles you might like:</h6>
                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                 
                  <h2 class="article-headtxt">How do I save on taxes with mutual funds ?</h2>
                  <p class="article-para">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                  <h2 class="article-headtxt">Introduction to Mutual funds.</h2>
                  <p class="article-para">
                    
                    It involves pooling the money of investors such us ourselves into a sizable corpus. This amount is then invested into various securities such as stocks, bonds, treasury bills etc.
                  </p>
                  <a href="{{url('/blog/introduction_to_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                 
                </div>
                <hr class="col-lg-12">

              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg ')}}" id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
          <div >
       <!--  <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div> -->
        </section>

  </body>
</html>