<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">

	  <title>Rightfunds | Blog</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
    <section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
              
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse  navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right  navbar-menubar">
                    <li><a class="active_navbar_header   " href="{{url('/blog')}}">Home</a></li> 
                    <li><a class="active_navbar_header   " href=" https://www.rightfunds.com/">Rightfunds.com</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>

    <section id="">
      <div class="container container-mod">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12">
                <img class="article-img img img-responsive" src="{{url('img/article5.jpg')}}">
            
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 article-spacing ">
                  <h2 class="content-header">Key highlights of Finance Minister Mr.Arun Jaitley's budget for the 2018/19 fiscal year.</h2>
               
              </div>

                <div class="content-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12   ">
                  <p class="article-content">
                    Here are the highlights of Finance Minister Arun Jaitley's budget for the 2018/19 fiscal year. 
                  </p>
            
                  <p class="article-content">
                    
                    1. Estimates 7.2 to 7.5 percent GDP growth in second half of current fiscal year .
                  </p>
                  <p class="article-content ">
                    2.Finance minister says "firmly on path to achieve 8% plus growth soon" .
                 </p> 
                  <p class="article-content ">   
                    3.Finance minister says will focus on strengthening rural, agriculture economy .
                  </p>
                  <p class="article-content ">
                    4. Minimum support price of all crops to be increased to atleast 1.5 times of production cost.
                  </p>
                  <p class="article-content ">

                    5.Reduced corporate tax by 25% extended to cos with turnover of Rs 2.5 to benefit small, micro and medium enterprises.
                  </p>
                  <p class="article-content ">
                    6. Rs 70 bn revenue forgone on account of lower corporate tax for Rs 2.5-bn turnover companies, says FM.
                  </p>
                  <p class="article-content ">
                    7.LTCG tax of 10% for investments over Rs 100k; STCG tax at 15%.
                  </p>
                  <p class="article-content ">
                    8.Huge increase in tax returns filed; 8.5 mn people filed returns in 2017-18, as against 6.6 mn in 2016-17.
                  </p>
                  <p class="article-content ">
                    9. Govt to provide free LPG connection to 80 million women under Ujjwala scheme, up from earlier target of 50 million.
                  </p>
                  <p class="article-content ">
                    10.President's emolument raised to Rs 500,000, Vice President's to Rs 400,000 and Governors' to Rs 350,000 per month.
                  </p>
                  <p class="article-content ">
                    11. National Insurance Co, Oriental Insurance Co and United Assurance Co to be merged into one entity and subsequently listed.
                  </p>
                  <p class="article-content ">
                    12. New law to provide automatic revision of emoluments indexed to inflation for MPs.
                  </p>
                  <p class="article-content ">
                    13. GST revenue for 2017-18 will be for 11 months; shortfall of non-tax revenue due to deferment of spectrum auction, says FM.
                  </p>
                  <p class="article-content ">
                    14. National Highways exceeding 9,000-km will be completed in 2018-19.
                  </p>
                  <p class="article-content ">
                    15.99 cities selected for smart cities project with an outlay of Rs 2.04 trn.
                  </p>
                  <p class="article-content ">
                    16.Govt to allocate Rs 7,140 crore for textiles sector in 2018-19.
                  </p>
                  <p class="article-content ">
                    17. Govt will monetise select central public sector enterprises using Infrastructure Investment Trusts.
                  </p>
                  <p class="article-content ">
                    18. Rs 800-bn disinvestment target for 2018-19, announces Jaitley.
                  </p>
                  <p class="article-content ">
                    19. NITI Aayog to establish a national programme for artificial intelligence.
                  </p>
                  <p class="article-content ">
                    20. SEBI may consider mandating large corporates to use bond market to finance one-fourth of their fund needs.
                  </p>
                  <p class="article-content ">
                    21. Govt to expand capacity of airports by five times to cater to one billion trips a year.
                  </p>
                  <p class="article-content ">
                    22. Employees PF Act to be amended to reduce contribution of women to 8% from 12% with no change in employer's contribution.
                  </p>
                  <p class="article-content ">
                    23. Govt will evolve a scheme to provide a unique ID to every enterprise on lines of Aadhaar.
                  </p>
                  <p class="article-content ">
                    24. Govt does not consider crypto-currency as legal; will take all measures against its illegal use.
                  </p>
                  <p class="article-content ">
                    25. 500,000 WiFi hotspots to provide broadband access to 50 mn rural people.
                  </p>
                  <p class="article-content ">
                    26. Govt to expand capacity of airports by five times to cater to one billion trips a year.
                  </p>
                  <p class="article-content ">
                    27. Regional air connectivity scheme shall connect 56 unserved airports and 31 unserved helipads.
                  </p>
                  <p class="article-content ">
                    28. When our govt took over India was considered one of the fragile five economies of the world; we have reversed it; India is today fastest growing economy, said FM
                  </p><p class="article-content ">
                    29.Introduction of GST has made indirect tax system simpler: FM.
                  </p><p class="article-content ">
                    30. Demonetisation has reduced cash in economy, promoted digital transactions, says FM.
                  </p><p class="article-content ">
                    31. India is today a $2.5 trillion economy and will become fifth largest economy in the world from the present seventh largest: FM.

                  </p>

                  <p class="article-content ">
                  
                    32.To provide Rs 500,000 per family annually for medical reimbursement under National Health Protection Scheme. Finance minister says the plan will protect 100 million poor families and will be world's largest health protection scheme.
                </p>
                  <p class="article-content">
                  33.To implement special schemes for governments around Delhi to address air pollution.
                </p>

                  <p class="article-content ">
                    
                    34.Removal of crop residue to be subsidised in order to tackle the problem of pollution due to burning of crop residue.

                  </p>

                  <p class="article-content ">
                      35.Exports to grow by 17% in 2017-18, says FM Jaitley. 
                  </p>
                  <p class="article-content ">
                      36. Cooking gas being given free to poor under PMUY, 40 mn unconnected being provided electricity connection, stent prices slashed.
                  </p>
                  <p class="article-content ">
                      37. India produced over 275 million tonnes of foodgrains and 300 million tonnes of fruits and vegetables in 2016-17, says FM.
                  </p>
                  <p class="article-content ">
                      38. Govt will ensure farmers get MSP if prices fall; Niti Aayog will discuss with state govts for mechanism to ensure farmers get better prices.
                  </p>
                  <p class="article-content ">
                      39. Rs 20 billion fund to be set up for upgrading rural agri markets.
                  </p>
                  <p class="article-content ">
                      40. Agriculture ministry will reorient ongoing programmes to promote cluster-based horticulture production.
                  </p>
                  <p class="article-content ">
                      41. Rs 160 bn to be spent on providing electricity connection to 40 mn poor households.
                  </p>
                  <p class="article-content ">
                      42. Two new schools of planning and architecture to be set up; 18 more in IITs and NIITs.
                  </p>
                  <p class="article-content ">
                      43. Govt to launch Prime Minister's Research Fellow Scheme which will identify 1000 B.Tech students to do Ph.D at IITs .
                  </p>
                  <p class="article-content ">
                      44. By 2022, every block with more than 50 percent ST population will have Ekalvya schools at par with Navodaya Vidyalayas.
                  </p>
                  <p class="article-content ">
                      45. Govt to substantially increase allocation under national livelihood mission to Rs 57.5 bn in next fiscal.
                  </p>
                  <p class="article-content ">
                      46. Target for loan disbursement under Mudra scheme set at Rs 3 trillion for next fiscal.
                  </p>
                  <p class="copy-right">
                   Credits: Business Standard, Budget 2018.
                  </p>

                  <hr class="endline breakline">
                  
                  
                  <div>

                  </div>

                </div>

                </div>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section id="recommended-article">
      <div class="container container-mod   ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head other-articles">More articles you might like:</h6>
                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                 
                  <h2 class="article-headtxt">How do I save on taxes with mutual funds ?</h2>
                  <p class="article-para">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                  <h2 class="article-headtxt">Introduction to Mutual funds.</h2>
                  <p class="article-para">
                    
                    It involves pooling the money of investors such us ourselves into a sizable corpus. This amount is then invested into various securities such as stocks, bonds, treasury bills etc.
                  </p>
                  <a href="{{url('/blog/introduction_to_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                 
                </div>
                <hr class="col-lg-12">

              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg')}} " id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
          <div >
       <!--  <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div> -->
        </section>

  </body>
</html>