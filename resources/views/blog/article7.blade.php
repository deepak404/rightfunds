<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">
	<meta property="og:image" content="img/article7.jpg" />
    <meta property="og:title" content="Benchmark Indices vs Mutual Funds" />

	<title>Benchmark Indices vs Mutual Funds | Rightfunds | Blog</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">

    <style type="text/css">
    	td>img{
    		width: 100% !important;
    		/*height: 150% !important;*/
    	}

    	span{
    		font-size: 18px !important;
    		line-height: 1.8 !important;
    	}

    	.g-comparison{
    		font-size: 20px !important;
    	}

    	.content-header{
    		font-family: 'Frank Ruhl Libre', serif;
    		margin-top: 20px;
    	}

    	center{
    		    margin-top: 20px;
    	}
    </style>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
  	<section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse  navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right  navbar-menubar">
                    <li><a class="active_navbar_header" href="{{url('/blog')}}">Home</a></li> 
                    <li><a class="active_navbar_header" href=" https://www.rightfunds.com/">Rightfunds.com</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>
  	<div class="container container-mod">

  		<div class="row">
  			<div class="col-xs-12 col-sm-12 col-md-12  col-lg-12">
                <img class="article-img img img-responsive" src="{{url('img/article7.jpg')}}">
          </div>
  		</div>
  		<span style="background-color: #ffffff;"><!-- NAME: MEMBER WELCOME --> <!-- [if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG></o:AllowPNG>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]--> <!--*|IF:MC_PREVIEW_TEXT|*--> <!-- [if !gte mso 9]><!----><!--<![endif]--> <!--*|END:IF|*--></span><center>

		<table style="width: 100%; border-collapse: collapse; margin-top: 20px; margin-left: auto; margin-right: auto;" border="0">
		<tbody>
		<tr style="height: 45px;">
		<td style="width: 100%; height: 45px;"><strong><span style="font-size: 16pt; color: #333;"><h2 class="content-header">Benchmark Indices vs Mutual Funds</h2></span></strong></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 30px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 117px;">
		<td style="width: 100%; line-height: 20px; height: 72px; word-spacing: 1px;"><span style="font-size: 12pt;"><span style="font-size: 12pt;">With key Indices such as the Nifty &amp; Sensex Surging to over 28% in 2017, It was undoubtedly a year of Equites. Retail investors who invested in a large manner into mutual funds were amply rewarded. Other asset classes remained lull with property prices ending flat or at a marginal increase of 2-3% in a few pockets, 10 Year G-secs yielding about 7.3% and a return of 6.9% &amp; -9.15% on Gold &amp; Silver.<br /></span></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 50px; height: 72px;" border="0">
		<tbody>
		<tr style="height: 72px;">
		<td style="width: 100%; line-height: 20px; height: 72px; word-spacing: 1px;"><span style="font-size: 12pt;">2018, However seems to have begun on a somewhat expected bearish note. As the market attempted to correct the run-up in valuations against actual corporate earnings amidst the emerging banking scams and geopolitical instability. A clue to how the markets may do in the future could lie in their present valuations, while past data indicates a relatively stable zone for Nifty is at a P/E between 21 &amp; 22 the present valuation at 25.35 indicates a furthercorrection could be imminent. This however should not deter long-term investors who&rsquo;ve placed their bets in the right mutual funds and stocks.<br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%; line-height: 20px; height: 72px; word-spacing: 1px;"><span style="font-size: 12pt;">While the Key indices did lose close to 10% in the last couple of months not All Mutual funds performed equally. Some performed significantly better than the others, indicating more robust investment strategies and better corporate governance. Complied together is a list of the best and worst performing mutual funds during the recent bear market. For new investors thinking of entering the markets it is a good idea to invest lump sum amounts in a diversified portfolio of balanced funds comprising of both equities &amp; debt instruments or adopting a systematic investment approach by breaking up equity investments into regular intervals.</span></td>
		</tr>
		</tbody>
		</table>
		<!-- Best Small Cap Starts-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Small Cap]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">BSE Small Cap</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">16994.36</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-9.2%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Best Performers - Small Cap</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Franklin India Smaller Companies Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">58.87</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.05%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">DSP Blackrock Small and Mid cap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">54.613</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-4.29%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Kotak Emerging Equity Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">38.992</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-4.41%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">L&amp;T Emerging Businesses Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">26.796</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">HDFC Small Cap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">44.23</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-3.34%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/c85a50bc9b" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- Best Small Cap Starts--> <!-- Worst Small Cap Starts-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Small Cap]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">BSE Small Cap</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">16994.36</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-9.2%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Worst Performers - Small Cap</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Sundaram S.M.I.L.E. Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">98.5058</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.58%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Reliance Mid &amp; Small Cap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">45.6637</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.21%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Aditya Birla Sun Life Small and Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">40.1155</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-7.4%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Aditya Birla Sun Life Pure Value Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">60.2499</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.35%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Sahara Star Value Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">24.4851</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-7.14%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison<br /><br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/60759bdd60" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- Worst Small Cap Starts--> <!-- Best Mid Cap Starts-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Mid Cap]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">BSE Mid Cap</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">15962.59</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.07%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Best Performers - Mid Cap</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">ICICI Prudential Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">98.49</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.46%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">SBI Magnum Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">79.2155</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.08%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">SBI Magnum Global Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">171.1187</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-4.8%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">L&amp;T Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">138.99</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-6.58%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Axis Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">33.38</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">0.21%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/8733950d46" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- Best Mid Cap ends--> <!-- Worst Mid Cap Starts-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Mid Cap]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">BSE Mid Cap</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">15962.59</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.07%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Worst Performers - Mid Cap</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Tata Midcap Growth Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">134.2859</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-7.55%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Reliance Mid &amp; Small cap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">45.6637</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.21%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Mirae asset Emeging bluechip Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">47.521</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-8.91%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">BNP Paribas Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32.588</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.93%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">IDBI Midcap Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">11.46</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-6.68%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison<br /><br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/4c5ebd19fc" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- worst Mid Cap ends--> <!-- Best large Cap Starts-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Large Cap]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">Sensex</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32968.68</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.33%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Best Performers - Large Cap</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Sundaram Select Focus Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">160.3813</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.82%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">SBI Blue Chip Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">37.2226</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.12%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">IDFC Focused Equity Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">38.3874</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-6.13%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">BOI AXA Equity Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">36.32</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-6.66%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Kotak Select Focus Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">31.817</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-6.61%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/62e4a9216a" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- Best Large Cap ends--> <!-- Worst large Cap Starts-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Large Cap]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">Sensex</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32968.68</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.33%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Worst Performers - Large Cap</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Reliance Vision Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">533.9554</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-13.55%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Franklin Blue Chip Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">433.9091</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">HDFC Top 200 Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">429.085</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-10.96%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Reliance Top 200 Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">31.0234</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.15%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">IDFC Equity Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">29.7223</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.03%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison<br /><br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/bcb4d5d5f5" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!-- Best Large Cap ends-->
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Sectoral]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">Sensex</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32968.68</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.33%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Best Performers - Sectoral</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Franklin India technology Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">140.3131</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-2.44%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">UTI transporation and Logistics fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">118.1763</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.31%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">SBI FMCG Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">115.1362</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-3.64%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">SBI Banking and Financial Services fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">14.735</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.45%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Tata India Consumer fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">16.9699</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-4.36%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/57e60f8f0a" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Sectoral]</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">Sensex</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32968.68</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.33%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Worst Performers - Sectoral</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">Reliance Diversified Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">110.5256</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.81%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">HDFC Infrastructure&nbsp; Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">18.636</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-16.13%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">DSP Blackrock Natural resources and New Energy Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">33.897</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-9.16%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Sundaram Financial Services oppurtunities Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">37.0709</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-11.08%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Franklin Build India Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">39.5105</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-10.32%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison<br /><br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/baa417e55b" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Balanced]<br /><span style="font-size: 12pt;">(&#x2243; 60% Equity, 40% Debt)</span></span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">Sensex</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32968.68</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.33%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Best Performers - Balanced</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">ICICI Prudential Balanced Advantage Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">33.11</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-1.08%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">L&amp;T Dynamic Equity Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">22.736</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-1.19%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">BOI AXA Equity Debt Rebalancer Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">14.7869</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-0.87%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Aditya Birla Sunlife Balanced Advantage Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">49.74</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-2.36%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">AXIS Dynamic Equity Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">10.35</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-1.43%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison<br /><br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/c18e80e6d7" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 50px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;"><span style="font-size: 14pt;">Index Vs Mutual Funds [Balanced]<br /></span></span><span style="font-size: 14pt;"><span style="font-size: 12pt;">(~- 60% Equity, 40% Debt)</span><br /></span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 14pt;">01.Feb.2018 - 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 14pt;">Benchmark Index</span></td>
		<td style="width: 50%; height: 17px; text-align: right;"><span style="font-size: 12pt;">Nav As on 28.Mar.2018</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ea107b; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; width: 60% !important; display: inline-block; font-size: 13pt;">Sensex</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">32968.68</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 14pt;">-8.33%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; margin-top: 20px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 50%; height: 17px;"><span style="font-size: 16pt;">Top 5 Worst Performers - Balanced</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #307fc3; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">ICICI Prudential Balanced Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">124.89</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.43%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<!-- Franklin smaller india companies ends here-->
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #926bab; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">TATA Balanced Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">201.3395</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.85%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #48b158; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 80% !important; display: inline-block;">UTI Balanced Fund</span><br /></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">165.0419</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-6.06%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #ef5c6e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">HDFC Prudence Fund</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">485.082</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-8.8%</span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><hr style="border: none; height: 1px; background-color: #a3a3a3; margin-top: 15px;" /></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse; margin-top: 30px; height: 42px;" border="0">
		<tbody>
		<tr style="height: 42px;">
		<td style="width: 50%; height: 42px; vertical-align: top; border-left: 5px solid #f4781e; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-left: 10px; word-break: break-word;"><span style="margin-left: 0px; font-size: 13pt; width: 60% !important; display: inline-block;">Reliance Regular Savings Fund - Balanced</span></td>
		<td style="width: 40%; height: 42px; vertical-align: top;">NAV<br /><br /><span style="font-size: 14pt;">53.335</span></td>
		<td style="width: 10%; height: 42px; vertical-align: top;">Returns<br /><br /><span style="font-size: 18.6667px;">-5.95%</span></td>
		</tr>
		</tbody>
		</table>
		<br /><br />
		<table style="width: 100%; margin-top: 15px; border-collapse: collapse;" border="0">
		<tbody>
		<tr style="height: 17px;">
		<td style="width: 100%; height: 17px;"><span style="font-size: 14pt;" class = "g-comparison"> Graphical Comparison<br /><br /></span></td>
		</tr>
		</tbody>
		</table>
		<table style="width: 100%; border-collapse: collapse;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;">
		<table style="border-collapse: collapse; width: 100%;" border="0">
		<tbody>
		<tr>
		<td style="width: 100%;"><img src="https://s3.amazonaws.com/cloudHQ_share/1e00081054" width="100%" height="100%" /></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<br /></center>
  	</div>

  	<section id="recommended-article">
      <div class="container container-mod ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head other-articles">More articles you might like:</h6>
                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                 
                  <h2 class="article-headtxt">How do I save on taxes with mutual funds ?</h2>
                  <p class="article-para">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                  <h2 class="article-headtxt">Why should I invest My money?</h2>
                  <p class="article-para">
                    In order to understand why we should begin investing our money let us understand three key financial concepts that will help us grow.
                  </p>
                  <a href="{{url('/blog/why_should_i_invest_my_money')}}" class="button button-spacing">Read more</a>
                  
                 
                </div>
                <hr class="col-lg-12">

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  	<section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg')}} " id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
          <div >
       <!--  <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div> -->
        </section>

  </body>
</html>


