<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">
    <meta property="og:image" content="img/article2.jpg" />
    <meta property="og:title" content="Introduction to Mutual Funds" />

	  <title>Introduction to Mutual Funds | Buy Mutual funds Online | Rightfunds</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
    <section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
              
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse  navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right  navbar-menubar">
                    <li><a class="active_navbar_header   " href="{{url('/blog')}}">Home</a></li> 
                    <li><a class="active_navbar_header   " href=" https://www.rightfunds.com/">Rightfunds.com</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>

    <section id="">
      <div class="container container-mod">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12 ">
                <img class="article-img img img-responsive" src="{{url('img/article2.jpg')}}">
            
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 article-spacing ">
                  <h2 class="content-header">Introduction to Mutual Funds</h2>
               
              </div>

                <div class="content-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12   ">
                  <p class="article-content">
                    A mutual fund is a professionally managed investment scheme run by an asset management company (AMC).
                  </p>
            
                  <p class="article-content">
                    
                    It involves pooling the money of investors such us ourselves into a sizable corpus. This amount is then invested into various securities such as stocks, bonds, treasury bills etc.

                  </p>

                  <p class="article-content ">
                  Each scheme has an investment objective such as capital appreciation, providing a regular income or capital preservation. The objective of the scheme governs the assets classes a fund manager invests in. For example, a mutual fund scheme with the objective of giving an investor capital appreciation on his money may choose to invest in the shares of numerous publically traded companies. When these companies grow, so does the Net Asset Value (NAV) of the scheme.
                </p>


                  <p class="article-content">
                    
                    Equity MFs invest in the numerous shares of publically traded companies on the stock market. They generate returns for the investor by means of capital appreciation and the dividends payed out. Though they are subject to market volatility, it has historically been proven that investing in these schemes over a long period of time (>5years) has given the investors maximum returns compared to any other investment alternative. So if you are planning to invest away your money for a long period of time equity based schemes may be your best bet.
                  </p>
                  <p class="article-content">
                    
                   The other category ‘debt’ works slightly differently, here the fund manager primarily invests in corporate bonds, government securities, Treasury bills & Money market instruments. In other words, returns are generated from the money lent to the government or corporate. Debt mutual funds are not susceptible to market fluctuations and instead depend upon the lending rate fixed by the central bank. If you’re looking to invest your money over a short period of time a debt mutual fund holding papers of a suitable maturity might be a good alternative to consider.
                  </p>

                  <p class="article-content">
                 While most of the mutual fund schemes fall under one of the above discussed categories, it takes deep understanding of the underlying mechanisms to fully appreciate the differences between each scheme.
                  </p>

                  <p class="article-content">
                  65% of India’s wealth is locked up in fixed deposits, gold and Real estate, mutual funds only account for 2.1% of the total investments. However, this is changing quickly with increasing awareness. Large inflows are seen into the mutual fund industry over the past couple of years.
                  </p>

                  
                  <p class="content-subhead">
                  Here are five reasons why you must invest in MF’s.
                </p>

                <p class="article-content">
                  1. Mutual funds returns have consistently outperformed their benchmarks and alternative asset classes by a significant margin. They are time tested with prolonged track records. Reliance Growth Fund, HDFC Equity Fund, Franklin India Prima Fund, Reliance Vision Fund, HDFC top 200 Fund etc. are funds that have delivered over 20% CAGR returns for a period of 20 years!
                  </p>

                  <p class="article-content">
                  2. Unlike returns from fixed deposits, gold or property, capital gains on equity based mutual funds are completely tax free after a year of staying invested. So if you do pay taxes on your investments you must definitely consider investing in equity based mutual funds.
                  </p>

                  <p class="article-content">
                  3. Taxes on Debt based mutual funds are calculated after accounting for inflation at a maximum of 20%, when the investor stays invested for a period of 3 years. Dividends payed out are completely tax free in the hands of the investor.  
                  </p>

                  <p class="article-content">
                  4. Mutual funds are a highly diversified investment vehicle, this drastically minimises the risk associated with market fluctuations unlike the case of investing directly into individual shares on the stock market. More over in the case of debt based funds, fund managers hold corporate papers of numerous companies with varying maturities this minimises any credit or interest rate risks involved.
                  </p>

                  <p class="article-content">
                    5. Mutual funds are a liquid asset class. Units of open-ended funds may be redeemed from the asset management company at any time. The money is credited to the investors account within a matter of 4 days from placing the withdrawal request. For close ended schemes, units could be bought or sold on the stock market when desired.
                  </p>
                  <p class="article-content">
                    Mutual fund provide diversification, professional management, economies of scale and tax efficiency. To consistently manage one’s portfolio, one needs a lot of time and skills. It is a job best left to a fund manager. So begin investing today!
                  </p>
                  <hr class="endline breakline">
                  <div>

                  </div>

                </div>

                </div>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section id="recommended-article">
      <div class="container container-mod   ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head ">More articles you might like:</h6>

                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                 
                 
                  <h2 class="article-headtxt">How do I save on taxes with mutual funds ?</h2>
                  <p class="article-para">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>

                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                  <h2 class="article-headtxt">How to make returns like Warren Buffett?</h2>
                  <p class="article-para">
                    
                    When we think of Warren Buffett one of the wealthiest and savviest investors the world has ever seen, what comes to mind is an investor who may possibly be generating returns well over a 100% each year to get that wealthy. 
                  </p>
                  <a href="{{url('/blog/how_to_make_returns_like_warren_buffet')}}" class="button button-spacing">Read more</a>
                  
                <hr class="col-lg-12">
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg')}} " id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
          <!-- <div >
        <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div> -->
        </section>

  </body>
</html>