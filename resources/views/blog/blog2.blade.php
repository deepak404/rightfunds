<!doctype html>
<html class="no-js" lang="" xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | How do I save on taxes with mutual funds ?</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="icons/piggy-bank.jpg" />
        <meta property="og:title" content="How do I save on taxes with mutual funds ?" />

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <!-- <link rel="stylesheet" href="css/index.css"> -->
        <link rel="stylesheet" href="css/footer.css">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css">
        <link rel="stylesheet" href="css/landing.css">
        <link rel="stylesheet" href="css/landing-responsive.css">
        <link rel="stylesheet" href="css/contact-us.css">
        <link rel="stylesheet" href="css/version2/blog.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-98301278-1', 'auto');
          ga('send', 'pageview');

        </script>
    </head>
    <body>

        

    <section id="blog2" class="blog-jumbo-section">
        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="icons/nav-logo.svg	" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/faq')}}">FAQ</a></li>
                <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick = "javascript:location.href = '/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </section>

    <section>
         <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-div">
                    <h1 class="blog-header">How do I save on taxes with mutual funds ?</h1>
                    <p class="content-text">Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at
                    the right place.</p>

                    <p class="content-text">
                      Roughly 2% of the Indians pay income tax because 95% of the households earn an income of
less than 2.5 Lakhs annually. We should for a moment take pride in the fact that we pay
taxes that contribute towards building the nation.
                    </p>

                    <p class="content-text">
                      While paying taxes is one of the ways to contribute towards the growth of the country, the
government of India also provides for its citizens alternative ways to do so incidentally while
saving one’s income from income tax. So let’s get started!
                    </p>

                    <p class="content-text">
                      First, it is important to assess whether you come under the existing taxable bracket
provided by the finance ministry.
                    </p>

                    <p class="blog-sub-headers">
                        Income Tax Slab Rates for FY 2017-18 (AY 2018-19)
                    </p>

                    <table class="table table-bordered">
                      <thead>
                        <th>Income Tax Slab</th>
                        <th>Income Tax Rate</th>
                        <th>Education CESS</th>
                        <th>Secondary and Higher Education CESS</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Annual Income up to Rs. 2,50,000</td>
                          <td>NIL</td>
                          <td>NIL</td>
                          <td>NIL</td>
                        </tr>

                        <tr>
                          <td>Annual Income between Rs. 2,50,001 - Rs. 5,00,000</td>
                          <td>5% of Income
exceeding Rs.
2,50,000</td>
                          <td>2% of income tax</td>
                          <td>1% of income tax</td>
                        </tr>

                        <tr>
                          <td>Annual Income between Rs. 5,00,001 - Rs. 10,00,000</td>
                          <td>20% of Income
exceeding Rs.
5,00,000</td>
                          <td>2% of income tax</td>
                          <td>1% of income tax</td>
                        </tr>

                        <tr>
                          <td>Annual Income above Rs. 10,00,000</td>
                          <td>30% of Income
exceeding Rs.
10,00,000</td>
                          <td>2% of income tax</td>
                          <td>1% of income tax</td>
                        </tr>
                      </tbody>
                    </table>

                    <p class="content-text">If you do not come under these tax slabs then you probably do not have to worry about
paying income tax, but if you do then it’s a good idea to acquaint yourself with section 80C.</p>

                    <p class="blog-sub-headers">Salient Features of Section 80C</p>

                    

                    <p class="content-text">1. Section 80C is one of the various sections that are eligible for income tax
deductions under Chapter VI-A of the Income Tax Act; it is also the most popular
section that is commonly used by salaried employees to save taxes on their
incomes</p>

                    <p class="content-text">
                      <strong>2. In a particular financial year, Section 80C allows a tax payer to deduct a
maximum of Rs. 1,50,000 from his or her total taxable income</strong>
                    </p>

                    <p class="content-text">
                      3. Both individual tax payers and HUF can claim deductions under this section.
                    </p>


                    <p class="blog-sub-headers">
                        Instruments To Save Taxes Under Section 80C:
                    </p>

                    <ul id="tax-saving-instruments" style="
    list-style-type: upper-roman;
">
                      <li>Employee Provident Fund(EPF)</li>
                      <li>Employee Provident Fund (EPF)</li>
                      <li>Voluntary Provident Fund (VPF)</li>
                      <li>Public Provident Fund (PPF)</li>
                      <li>Equity-Linked Savings Scheme (ELSS) [aka “tax saving mutual funds”]</li>
                      <li>National Savings Certificate (NSC)</li>
                      <li>National Pension Scheme (NPS)</li>
                      <li>Tax-Saving 5-Year Bank Fixed Deposits (FD) and Post Office Time Deposits</li>
                      <li>Unit Linked Insurance Plan (ULIP)</li>
                      <li>Senior Citizens Savings Scheme (SCSS)</li>
                      <li>Sukanya Samriddhi Yojana</li>
                      <li>Life Insurance Premium</li>
                      <li>Payment of the Principal Component of Home Loans</li>
                    </ul>


                    <p class="content-text"> By investing in the financial instruments listed above one is eligible to save up to
150,000 rupees in taxes every year. But with so many choices available how does one
choose? Let us look at the table below for a quick comparison.</p>

                    <table class="table-bordered table">
                      <thead>
                        <th>Investment Type</th>
                        <th>Lock In period</th>
                        <th>Assured Return</th>
                        <th>Returns Expected</th>
                        <th>Is Return Taxable?</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Tax Saving Mutual Funds</td>
                          <td>3 Years</td>
                          <td>Equity Market Related</td>
                          <td>12-16%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>EPF</td>
                          <td>5 Years;
Exceptions
Prevail</td>
                          <td>No</td>
                          <td>8.65%</td>
                          <td>No</td>
                        </tr>


                        <tr>
                          <td>Life Insurance</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>0.6%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>5 Year tax Saving FD</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>6-7.2%</td>
                          <td>Interest Component is Taxable</td>
                        </tr>

                        <tr>
                          <td>NSE</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>7.80%</td>
                          <td>Interest Component is Taxable</td>
                        </tr>

                        <tr>
                          <td>ULIP</td>
                          <td>5 Years</td>
                          <td>Equity Market Related</td>
                          <td>8-10%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>SCSS</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>8.30%</td>
                          <td>Interest Component is Taxable</td>
                        </tr>

                        <tr>
                          <td>PPF</td>
                          <td>15 Years</td>
                          <td>Yes</td>
                          <td>7.80%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>Sukanya
Samriddhi
Yojana</td>
                          <td>21 Years</td>
                          <td>Yes</td>
                          <td>8.30%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>NPS</td>
                          <td>Until Retirement</td>
                          <td>Equity Market Related</td>
                          <td>8-10%</td>
                          <td>Yes</td>
                        </tr>


                      </tbody>
                    </table>

                    <p class="content-text">From the table, if you do not want to have your money locked up for 5, 10 or 20 years in
order to save taxes, tax saving mutual funds may be your best bet!
They not only have the shortest lock-in period vis-á- vis other options but also boast of
impressive returns.</p>

                    <p class="content-text">Tax saving mutual funds essentially invest at least 65% of the investors wealth in equities.
This implies that the returns an investor makes are linked to the performance of the equity
market. While markets may be volatile in the short run, we know that historically tax saving
mutual funds can be extremely rewarding in the long run (over a period of 3 years) and have
the potential to generate terrific returns that can easily surpass inflation.</p>

                    <p class="content-text">With no upper limit on tax saving mutual funds, individuals can invest as much as they want
and for a substantial period, if long-term wealth creation is one of their primary goals. Even
if one is cash-strapped at the moment, he or she can start off with an investment amount as
low as Rs.1000. Investments can be made on a monthly basis in the form of a Systematic
Investment Plan or SIP to benefit from rupee cost averaging &amp; overcoming short term
volatility. Tax saving mutual funds are classified as Triple E (Exempt - Exempt - Exempt). This
implies that the principle amount, dividends payed out &amp; the accumulated returns are
completely tax-free, provided the investments are held for at least a year. For today’s
working professionals who seek lucrative investment instruments that do not burden them
with lengthy lock-in periods investing in tax saving mutual funds may be an ideal choice. At
Rightfunds we offer carefully handpicked schemes that have provided investors with
superior returns over a prolonged period of time. Sign up today and begin investing!</p>

                  </div>
            </div>
        </div>
    </section>

    <section id="next-article-2" class="next-article">
       <div class="container custom-container">
           <div class="row">
               <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="next-art-div">
                    <div class = "col-lg-9 col-md-9 col-sm-9 col-xs-9"><p class="next-article">Next Article</p>
                   <h1 class="next-art-name"><a href="{{url('/why_should_i_invest_my_money')}}">Why should I invest my money ?</a> </h1></div>
                    <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <span><i class="material-icons right-arrow">arrow_forward</i></span>
                    </div>
                   
               </div>
           </div>
       </div>
    </section>


@extends('layouts.outside-footer')

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $(document).scroll(function () {
          var nav = $(".navbar-fixed-top");
          var jumbo = $('.blog-jumbo-section');
          var visible_height = jumbo.height() - 50;
          //alert(visible_height);
          nav.toggleClass('scrolled', $(this).scrollTop() > (jumbo.height()-110));
        });
    });
  </script>


  </body>
  </html>
