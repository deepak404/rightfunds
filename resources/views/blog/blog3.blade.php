<!doctype html>
<html class="no-js" lang="" xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Introduction to Mutual Funds</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="icons/mutual-funds.jpg" />
        <meta property="og:title" content="Introduction to Mutual Funds" />

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <!-- <link rel="stylesheet" href="css/index.css"> -->
        <link rel="stylesheet" href="css/footer.css">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css">
        <link rel="stylesheet" href="css/landing.css">
        <link rel="stylesheet" href="css/landing-responsive.css">
        <link rel="stylesheet" href="css/contact-us.css">
        <link rel="stylesheet" href="css/version2/blog.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-98301278-1', 'auto');
          ga('send', 'pageview');

        </script>
    </head>
    <body>

        

    <section id="blog3" class="blog-jumbo-section">
        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="icons/nav-logo.svg" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/faq')}}">FAQ</a></li>
                <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick = "javascript:location.href = '/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </section>

    <section>
         <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-div">
                    <h1 class="blog-header">Introduction to Mutual Funds</h1>
                    <p class="content-text">A mutual fund is a professionally managed investment scheme run by an asset
                    management company (AMC).</p>

                    <p class="content-text">It involves pooling the money of investors such us ourselves into a sizable corpus.
This amount is then invested into various securities such as stocks, bonds,
treasury bills etc.</p>
                    <p class="content-text">Each scheme has an investment objective such as capital appreciation, providing a
regular income or capital preservation. The objective of the scheme governs the
assets classes a fund manager invests in. For example, a mutual fund scheme with
the objective of giving an investor capital appreciation on his money may choose
to invest in the shares of numerous publically traded companies. When these
companies grow, so does the Net Asset Value (NAV) of the scheme.</p>
                    <p class="content-text">As an investor you can buy ‘units’ of a mutual fund which basically represent your
share of holdings in a particular scheme. There are currently 2000+ mutual fund
schemes in India offered by over 40 asset management companies. They cater to
a variety of needs in order to generate value for an investor. Most mutual funds
schemes can simply be divided into equity based, debt based or a combination of
the two depending upon the asset classes they invest in.</p>
                    <p class="content-text">Equity MFs invest in the numerous shares of publically traded companies on the
stock market. They generate returns for the investor by means of capital
appreciation and the dividends payed out. Though they are subject to market
volatility, it has historically been proven that investing in these schemes over a
long period of time (&gt;5years) has given the investors maximum returns compared
to any other investment alternative. So if you are planning to invest away your
money for a long period of time equity based schemes may be your best bet.</p>
                    <p class="content-text">The other category ‘debt’ works slightly differently, here the fund manager
primarily invests in corporate bonds, government securities, Treasury bills &amp;
Money market instruments. In other words, returns are generated from the
money lent to the government or corporate. Debt mutual funds are not
susceptible to market fluctuations and instead depend upon the lending rate fixed
by the central bank. If you’re looking to invest your money over a short period of
time a debt mutual fund holding papers of a suitable maturity might be a good
alternative to consider.</p>
                    <p class="content-text">While most of the mutual fund schemes fall under one of the above discussed
categories, it takes deep understanding of the underlying mechanisms to fully
appreciate the differences between each scheme.</p>
                    <p class="content-text">65% of India’s wealth is locked up in fixed deposits, gold and Real estate, mutual
funds only account for 2.1% of the total investments. However, this is changing
quickly with increasing awareness. Large inflows are seen into the mutual fund
industry over the past couple of years.</p>

                    <p class="blog-sub-headers">
                        Here are five reasons why you must invest in MF’s.
                    </p>

                    <p class="content-text">1. Mutual funds returns have consistently outperformed their benchmarks and
alternative asset classes by a significant margin. They are time tested with
prolonged track records. Reliance Growth Fund, HDFC Equity Fund, Franklin India
Prima Fund, Reliance Vision Fund, HDFC top 200 Fund etc. are funds that have
delivered over 20% CAGR returns for a period of 20 years!</p>
                    <p class="content-text">2. Unlike returns from fixed deposits, gold or property, capital gains on equity
based mutual funds are completely tax free after a year of staying invested. So if
you do pay taxes on your investments you must definitely consider investing in
equity based mutual funds.</p>
                    <p class="content-text">3. Taxes on Debt based mutual funds are calculated after accounting for inflation
at a maximum of 20%, when the investor stays invested for a period of 3 years.
Dividends payed out are completely tax free in the hands of the investor.</p>
                    <p class="content-text">4. Mutual funds are a highly diversified investment vehicle, this drastically
minimises the risk associated with market fluctuations unlike the case of investing
directly into individual shares on the stock market. More over in the case of debt
based funds, fund managers hold corporate papers of numerous companies with
varying maturities this minimises any credit or interest rate risks involved.</p>
                    <p class="content-text">5. Mutual funds are a liquid asset class. Units of open-ended funds may be
redeemed from the asset management company at any time. The money is
credited to the investors account within a matter of 4 days from placing the
withdrawal request. For close ended schemes, units could be bought or sold on
the stock market when desired.</p>
                    <p class="content-text">Mutual fund provide diversification, professional management, economies of
scale and tax efficiency. To consistently manage one’s portfolio, one needs a lot of
time and skills. It is a job best left to a fund manager. So begin investing today!</p> 


                  </div>
            </div>
        </div>
    </section>

    <section id="next-article3" class="next-article">
       <div class="container custom-container">
           <div class="row">
               <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="next-art-div">
                    <div class = "col-lg-9 col-md-9 col-sm-9 col-xs-9"><p class="next-article">Next Article</p>
                   <h1 class="next-art-name"><a href="{{url('/how_do_i_save_on_taxes_with_mutual_funds')}}">How do i Save on taxes with Mutual Funds</a> </h1></div>
                    <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <span><i class="material-icons right-arrow">arrow_forward</i></span>
                    </div>
                   
               </div>
           </div>
       </div>
    </section>


@extends('layouts.outside-footer')

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $(document).scroll(function () {
          var nav = $(".navbar-fixed-top");
          var jumbo = $('.blog-jumbo-section');
          var visible_height = jumbo.height() - 50;
          //alert(visible_height);
          nav.toggleClass('scrolled', $(this).scrollTop() > (jumbo.height()-110));
        });
    });
  </script>


  </body>
  </html>
