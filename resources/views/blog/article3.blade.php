<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">
    <meta property="og:image" content="img/article3.jpg" />
    <meta property="og:title" content="How do I save on taxes with mutual funds ?" />

	  <title>How do I save on taxes with mutual funds ? | Buy Mutual funds Online | Rightfunds</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">
    <link href="{{url('css/article3.css')}}" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
              
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg ')}}" class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse  navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right  navbar-menubar">
                    <li><a class="active_navbar_header   " href="{{url('/blog')}}">Home</a></li> 
                    <li><a class="active_navbar_header   " href=" https://www.rightfunds.com/">Rightfunds.com</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  


    <section id="">
      <div class="container container-mod">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12">
                <img class="article-img img img-responsive" src="{{url('img/article3.jpg')}}">
            
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 article-spacing ">
              <h2 class="content-header">How do I save on taxes with mutual funds ?</h2>
                </div>
                  <div class="content-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12   ">
                    <p class="article-content">
                      Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                    </p>
                    
                    <p class="article-content">
                      
                      Roughly 2% of the Indians pay income tax because 95% of the households earn an income of less than 2.5 Lakhs annually. We should for a moment take pride in the fact that we pay taxes that contribute towards building the nation.

                    </p>

                    <p class="article-content content-spacing">
                    While paying taxes is one of the ways to contribute towards the growth of the country, the government of India also provides for its citizens alternative ways to do so incidentally while saving one’s income from income tax. So let’s get started!
                  </p>

                    <p class="article-content">
                      
                      First, it is important to assess whether you come under the existing taxable bracket provided by the finance ministry.
                    </p>

                     <p class="content-subhead">
                    Income Tax Slab Rates for FY 2017-18 (AY 2018-19)
                  </p>
                  
                    <img src="{{url('img/Income Tax Slab Rates.svg')}}" class="img-spacing padding-lr-zero">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-lr-zero cess">
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padding-l-zero">
                        <p class="content-subhead">Educational CESS</p>
                        <hr> 
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-lr-zero">
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                            <p class="incometax">Income tax</p>
                            <h1 class="tax-value">0%</h1>
                          </div>
                          <div class=" col-xs-8 col-sm-8 col-md-8 col-lg-8 horz-seperator">
                            <p>Annual Income (Rs.)</p>
                            <h1 class="income-value">Upto 2,50,000</h1>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 incometax-section2 padding-lr-zero">
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <p class="incometax">Income tax</p>
                            <h1 class="tax-value">2%</h1>
                          </div>
                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 horz-seperator padding-r-zero">
                            <p>Annual Income (Rs.)</p>
                            <h1 class="income-value">Rs. 2,50,001 - Rs. 5,00,000</h1>
                            <h1 class="income-value">Rs. 5,00,001 - Rs. 10,00,000</h1>
                            <h1 class="income-value">Above Rs. 10,00,000</h1>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padding-lr-zero">
                        <p class="content-subhead">Secondary and Higher Education CESS</p>
                        <hr> 
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-lr-zero">
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <p class="incometax">Income tax</p>
                            <h1 class="tax-value">0%</h1>
                          </div>
                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 horz-seperator">
                            <p>Annual Income (Rs.)</p>
                            <h1 class="income-value">Upto 2,50,000</h1>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 incometax-section2 padding-lr-zero">
                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                            <p class="incometax">Income tax</p>
                            <h1 class="tax-value">1%</h1>
                          </div>
                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 horz-seperator padding-r-zero">
                            <p>Annual Income (Rs.)</p>
                            <h1 class="income-value">Rs. 2,50,001 - Rs. 5,00,000</h1>
                            <h1 class="income-value">Rs. 5,00,001 - Rs. 10,00,000</h1>
                            <h1 class="income-value">Above Rs. 10,00,000</h1>
                          </div>
                        </div>
                      </div>
                    </div>
                        



                    <p class="article-content">
                      
                      If you do not come under these tax slabs then you probably do not have to worry about paying income tax, but if you do then it’s a good idea to acquaint yourself with section 80C.

                    </p>
                    <p class="content-subhead">
                    Salient Features of Section 80C:
                  </p>

                    <p class="article-content">
                    1. Section 80C is one of the various sections that are eligible for income tax deductions under Chapter VI-A of the Income Tax Act; it is also the most popular section that is commonly used by salaried employees to save taxes on their incomes.
                    </p>

                    <p class="article-content">
                    <strong class="strong">2. In a particular financial year, Section 80C allows a tax payer to deduct a maximum of Rs. 1,50,000 from his or her total taxable income.
                    </strong>
                    </p>

                    <p class="article-content">
                    3. Both individual tax payers and HUF can claim deductions under this section.
                    </p>
                    <p class="content-subhead">
                    Instruments To Save Taxes Under Section 80C:
                  </p>
                  <ul class="roman-list">
                    <li>Employee Provident Fund (EPF)</li>
                    <li>Voluntary Provident Fund (VPF)</li>
                    <li>Public Provident Fund (PPF)</li>
                    <li>Equity-Linked Savings Scheme (ELSS) [aka “tax saving mutual funds”]</li>
                    <li>National Savings Certificate (NSC)</li>
                    <li>National Pension Scheme (NPS)</li>
                    <li>Tax-Saving 5-Year Bank Fixed Deposits (FD) and Post Office Time Deposits</li>
                    <li>Unit Linked Insurance Plan (ULIP)</li>
                    <li>Senior Citizens Savings Scheme (SCSS)</li>
                    <li>Sukanya Samriddhi Yojana</li>
                    <li>Life Insurance Premium</li>
                    <li>Payment of the Principal Component of Home Loans</li>
                    

                  </ul>
                  <p class="article-content">
                    By investing in the financial instruments listed above one is eligible to save up to 1,50,000 rupees in taxes every year. But with so many choices available how does one choose? Let us look at the table below for a quick comparison.
                    </p>
                    <table class="table-bordered table">
                      <thead>
                        <tr><th>Investment Type</th>
                        <th>Lock In period</th>
                        <th>Assured Return</th>
                        <th>Returns Expected</th>
                        <th>Is Return Taxable?</th>
                      </tr></thead>
                      <tbody>
                        <tr>
                          <td>Tax Saving Mutual Funds</td>
                          <td>3 Years</td>
                          <td>Equity Market Related</td>
                          <td>12-16%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>EPF</td>
                          <td>5 Years;
Exceptions
Prevail</td>
                          <td>No</td>
                          <td>8.65%</td>
                          <td>No</td>
                        </tr>


                        <tr>
                          <td>Life Insurance</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>0.6%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>5 Year tax Saving FD</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>6-7.2%</td>
                          <td>Interest Component is Taxable</td>
                        </tr>

                        <tr>
                          <td>NSC</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>7.80%</td>
                          <td>Interest Component is Taxable</td>
                        </tr>

                        <tr>
                          <td>ULIP</td>
                          <td>5 Years</td>
                          <td>Equity Market Related</td>
                          <td>8-10%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>SCSS</td>
                          <td>5 Years</td>
                          <td>Yes</td>
                          <td>8.30%</td>
                          <td>Interest Component is Taxable</td>
                        </tr>

                        <tr>
                          <td>PPF</td>
                          <td>15 Years</td>
                          <td>Yes</td>
                          <td>7.80%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>Sukanya
                              Samriddhi
                            Yojana</td>
                          <td>21 Years</td>
                          <td>Yes</td>
                          <td>8.30%</td>
                          <td>No</td>
                        </tr>

                        <tr>
                          <td>NPS</td>
                          <td>Until Retirement</td>
                          <td>Equity Market Related</td>
                          <td>8-10%</td>
                          <td>Yes</td>
                        </tr>


                      </tbody>
                    </table>

                    <p class="article-content">
                    From the table, if you do not want to have your money locked up for 5, 10 or 20 years in
                    order to save taxes, tax saving mutual funds may be your best bet!
                    They not only have the shortest lock-in period vis-á- vis other options but also boast of
                    impressive returns.
                    </p>

                    <p class="article-content">
                      Tax saving mutual funds essentially invest at least 65% of the investors wealth in equities.
                      This implies that the returns an investor makes are linked to the performance of the equity
                      market. While markets may be volatile in the short run, we know that historically tax saving
                      mutual funds can be extremely rewarding in the long run (over a period of 3 years) and have
                      the potential to generate terrific returns that can easily surpass inflation.
                    </p>

                    <p class="article-content">
                    With no upper limit on tax saving mutual funds, individuals can invest as much as they want
                    and for a substantial period, if long-term wealth creation is one of their primary goals. Even
                    if one is cash-strapped at the moment, he or she can start off with an investment amount as
                    low as Rs.1000. Investments can be made on a monthly basis in the form of a Systematic
                    Investment Plan or SIP to benefit from rupee cost averaging &amp; overcoming short term
                    volatility. Tax saving mutual funds are classified as Triple E (Exempt - Exempt - Exempt). This
                    implies that the principle amount, dividends payed out &amp; the accumulated returns are
                    completely tax-free, provided the investments are held for at least a year. For today’s
                    working professionals who seek lucrative investment instruments that do not burden them
                    with lengthy lock-in periods investing in tax saving mutual funds may be an ideal choice. At
                    Rightfunds we offer carefully handpicked schemes that have provided investors with
                    superior returns over a prolonged period of time. Sign up today and begin investing!
                    </p>
                    <hr class="endline breakline">

                  <div>

                  </div>

                </div>

                </div>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section id="recommended-article">
      <div class="container container-mod   ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head other-articles">More articles you might like:</h6>
                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                
                  <h2 class="article-headtxt">How to make returns like Warren Buffett?</h2>
                  <p class="article-para">
                    
                    When we think of Warren Buffett one of the wealthiest and savviest investors the world has ever seen, what comes to mind is an investor who may possibly be generating returns well over a 100% each year to get that wealthy. 
                  </p>
                  <a href="{{url('/blog/how_to_make_returns_like_warren_buffet')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                 
                  <h2 class="article-headtxt">Introduction to Mutual Funds.</h2>
                  <p class="article-para">
                   It involves pooling the money of investors such us ourselves into a sizable corpus. This amount is then invested into various securities such as stocks, bonds, treasury bills etc.
                  </p>
                  <a href="{{url('/blog/introduction_to_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12">
                <div >
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg')}} " id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
          <!-- <div >
        <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div> -->
        </section>

  </body>
</html>