<!doctype html>
<html class="no-js" lang="" xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | How to make returns like Warren Buffet ?</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="icons/warren-buffet.png" />
        <meta property="og:title" content="How to make returns like Warren Buffet ?" />


        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <!-- <link rel="stylesheet" href="css/index.css"> -->
        <link rel="stylesheet" href="css/footer.css">

        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css">
        <link rel="stylesheet" href="css/landing.css">
        <link rel="stylesheet" href="css/landing-responsive.css">
        <link rel="stylesheet" href="css/contact-us.css">
        <link rel="stylesheet" href="css/version2/blog.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-98301278-1', 'auto');
          ga('send', 'pageview');

        </script>
    </head>
    <body>

        

    <section id="blog4" class="blog-jumbo-section">
        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
          <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="icons/nav-logo.svg " class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <!--  <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul> -->
              
              <ul class="nav navbar-nav navbar-right" id="register-nav">
                <li><a href="{{url('/faq')}}">FAQ</a></li>
                <li><a href="{{url('/login')}}" class="blue-text" id="login">Login</a></li>
                <li><button onclick = "javascript:location.href = '/register'" class="btn btn-primary" id="nav-register-btn">Register</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </section>

    <section>
         <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-div">
                    <h1 class="blog-header">How to make returns like warren buffet?</h1>
                    <p class="content-text">When we think of Warren buffet one of the wealthiest and savviest investors the world has ever seen, what comes to mind is an investor who may possibly be generating returns well over a 100% each year to get that wealthy.</p>

                    <p class="content-text">
                      The real figure however is not that merry. In fact it is not even close to 50%. Warren Buffet’s company Berkshire Hathway delivered 21.6% returns year on year for the last 50 years.
                    </p>

                    <p class="content-text">
                      How did he become a billionaire then?! One may ask.
                    </p>

                    <p class="content-text">
                      Without further ado, Let’s unveil the secret. Warren buffet stumbled upon a great tool when he was really young and used it well to have what he has now. This great tool is called “COMPOUND INTEREST”. Its universal and works for anyone who is patient.
                    </p>

                    <p class="blog-sub-headers">
                        So how can it help you make a fortune ?
                    </p>

                    <p class="content-text">ILet’s assume you invest rupees 100,000 and make 20% returns each year. This will give you rupees 20,000 in the first year, bringing your total up to 120,000 rupees at the end of year one. Now what happens if we repeat this process over say 25 years? Let’s do a bit of math to find out.</p>

                    <p class="blog-sub-headers">Compound Interest Formula:</p>

                    

                    <p class="content-text">Net amount = Initial Investment x ( 1 + returns/100 )<sup>Investment Period</sup></p>

                    <p class="content-text">
                      =  100,000 x (1 + 20/100)<sup>25</sup>
                    </p>

                    <p class="content-text">
                      =  95,39,625.
                    </p>

                    <p class="content-text">
                      Your initial investment of rupees 100,000 has grown an astounding 9,539% to rupees 95.39 Lakhs at the end of 25 years!
                    </p>

                     <p class="content-text">
                      But we’re talking about Warren Buffet like returns, so let’s say you are patient and stay invested for 50 years just like him, What do you think the returns would be? Double? Not really.
                    </p>

                    <p class="content-text">At the end of <strong>35 years</strong> you will have <strong>Rupees 5.9 Crores!</strong></p>

                    <p class="content-text">And at the end of <strong>50 years Rupees 91 Crores!</strong></p>

                    <p class="content-text">Remember you started out with just 1 Lakh! That’s roughly investing 8,300 rupees per month for a just one year.</p>


                    <p class="content-text"> Now, what is <strong>common between Warren Buffet and yourself?</strong> Yes, <strong>the Compound Interest formula itself!</strong> It does no partiality and is the same for the poor and wealthy. To make the best use of it what you need to do is disciplined investing. Being disciplined and staying invested for a long period of time is the key to amassing great wealth. This is exactly what Warren Buffet did. He started investing at the age of 11 and now is 84. You could guess what would be the result of 72 years of compound interest.</p>


                    

                    <p class="content-text">FIf you think making 20% returns year on year for a long period of time is impossible, think again! The mutual fund schemes listed below have given over 20% returns for the last 20 years (courtesy, NDTV Profit)</p>

                    <p class="content-text">1) <strong>HDFC Equity Fund:</strong> Launched on January 1, 1995. If you had started an SIP of Rs. 5,000 in this fund on April 1, 1998, your investment would have grown to Rs. 1.95 crore by April 1, 2017, which means a CAGR return of 23.56 per cent over 20 years.</p>

                    <p class="content-text">2) <strong>Reliance Growth Fund:</strong> This fund has generated a CAGR return of 23.68 per cent since its inception. Your Rs. 5,000 SIP in this fund, started on April 1, 1998 would have helped you accumulate a corpus of Rs. 2.12 crore by April 1, 2017.</p>

                    <p class="content-text">3) <strong>Franklin India Prima Fund:</strong> It is one of the flagship fund of Franklin Templeton India. If you had started an SIP of Rs. 5,000 in this fund on April 1, 1998, your investment would have grown to Rs. 2.38 crore by April 1, 2017, which means a CAGR return of 25.11 per cent over 20 years.</p>

                    <p class="content-text">4) <strong>Reliance Vision Fund:</strong> Launched on October 8, 1995, it has generated a CAGR return of 19.92 per cent till now. Your Rs. 5,000 SIP in this fund, started on April 1, 1998, would have become Rs. 1.46 crore by April 1, 2017.</p>

                    <p class="content-text">5) <strong>HDFC Top 200 Fund:</strong> This flagship fund of HDFC Mutual Fund has generated a CAGR return of 20.88 per cent since its inception in September 1996. If you would have started an SIP of Rs. 5,000 in this fund on April 1, 1998, your investment would have grown to Rs. 1.43 crore by April 1, 2017.</p>

                    <img style="margin: 40px auto;" src="icons/wb-quote.png" class="img img-responsive">


                    <p class="content-text">Begin investing today with Rightfunds.com and take your first step towards Financial Independence!</p>


                  </div>
            </div>
        </div>
    </section>

    <section id="next-article-2" class="next-article">
       <div class="container custom-container">
           <div class="row">
               <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="next-art-div">
                    <div class = "col-lg-9 col-md-9 col-sm-9 col-xs-9"><p class="next-article">Next Article</p>
                   <h1 class="next-art-name"><a href="{{url('/why_should_i_invest_my_money')}}">Why should I invest my money ?</a> </h1></div>
                    <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <span><i class="material-icons right-arrow">arrow_forward</i></span>
                    </div>
                   
               </div>
           </div>
       </div>
    </section>


@extends('layouts.outside-footer')

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $(document).scroll(function () {
          var nav = $(".navbar-fixed-top");
          var jumbo = $('.blog-jumbo-section');
          var visible_height = jumbo.height() - 50;
          //alert(visible_height);
          nav.toggleClass('scrolled', $(this).scrollTop() > (jumbo.height()-110));
        });
    });
  </script>


  </body>
  </html>
