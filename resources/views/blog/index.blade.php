<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">

	  <title>Rightfunds | Blog</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article-index.css')}}" rel="stylesheet">
            <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-98301278-1', 'auto');
          ga('send', 'pageview');

        </script>
  </head>

  <body class="body">
    <section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
              
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right navbar-menubar">
                    <li>
                      <a class="active_navbar_header padding-lr-zero" href=" https://www.rightfunds.com/">Rightfunds.com</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>

    <section id="cover-article">
      <div class="container  container-indexmod">
        <div class="row">
          <div class="article-section">
            <div class="main-article col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <h6 class="sub-head padding-lr-zero">Top Article</h6>
              <h1 class="main-title">How to make returns like Warren Buffett?</h1>
              <p class="main-para"> When we think of Warren Buffett one of the wealthiest and savviest investors the world has ever seen, what comes to mind is an investor who may possibly be generating returns well over a 100% each year to get that wealthy. 
              </p>
              <div>
                <a  class="button button-spacing" href="{{url('/blog/how_to_make_returns_like_warren_buffet')}}">Read more</a>
                <!-- <ul class="index-share pull-right list-inline">
                  <li> <i class="material-icons share-icon">share</i></li>
                  <li> <h4>share</h4></li>
                </ul> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div>

    <section >
      <div class="container-fluid">
        <div class="row">
          <div class="image-sizeing img-spacing col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
            <img class="main-img img img-responsive" src="{{url('img/topcover.jpg')}}">
            
          </div>

        </div>
      </div>
    </section>

    <section id="latest-article">
      <div class="container">
        <div class="row">
          <div class="latest-section col-lg-12 padding-lr-zero">
              <h6 class="sub-head latestpub-head ">Latest Publish</h6>
              
              

              <div class="latest-articles col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-lr-zero" >

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 article-card">
                  <img class="latest-img img img-responsive" src="{{url('img/article5.jpg')}}"">
                  <h2 class="article-headtxt article-headsize">Key highlights of FM budget for the 2018/19 fiscal year.</h2>
                  <p class="article-para article-parasize">
                    Estimates 7.2 to 7.5 percent GDP growth in second half of current fiscal year,Finance minister says "firmly on path to achieve 8% plus growth soon" 
                  </p>
                  <a href="{{url('/blog/budget_for_the_2018_19_fiscal_year')}}" class="button button-spacing readmore-btn">Read more</a>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 article-card">
                  <img class="latest-img img img-responsive" src="{{url('img/article6.jpg')}}"">
                  <h2 class="article-headtxt article-headsize">What is a Systematic Investment Plan?</h2>
                  <p class="article-para article-parasize">
                    An SIP is an investment methodology wherein a fixed amount of money is invested into a mutual fund scheme or any other asset class over regular intervals of time.
                  </p>
                  <a href="{{url('/blog/what_is_systematic_investment_plan')}}" class="button button-spacing readmore-btn">Read more</a>
                </div>

                
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 article-card">
                  <img class="latest-img img img-responsive" src="{{url('img/article3.jpg')}}">
                  <h2 class="article-headtxt article-headsize">How do I save on taxes with mutual funds?</h2>
                  <p class="article-para article-parasize">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place. 
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 article-card">
                  <img class="latest-img img img-responsive" src="{{url('img/article2.jpg')}}">
                  <h2 class="article-headtxt article-headsize">Introduction to Mutual Funds. </h2>
                  <p class="article-para article-parasize">
                   It involves pooling the money of investors such us ourselves into a sizable corpus. This amount is then invested into various securities such as stocks, bonds, treasury bills etc.
                  </p>
                  <a href="{{url('/blog/introduction_to_mutual_funds')}}" class="button button-spacing">Read more</a>
                </div>

                <div class=" col-xs-12   col-sm-4    col-md-4 col-lg-4 article-card">
                  <img class="latest-img img img-responsive" src="{{url('img/article1.jpg')}}">
                  <h2 class="article-headtxt article-headsize">Why should I invest My money? </h2>
                  <p class="article-para  article-parasize">
                    In order to understand why we should begin investing our money let us understand three key financial concepts that will help us grow.
                  </p>
                  <a href="{{url('/blog/why_should_i_invest_my_money')}}" class="button button-spacing">Read more</a>
                </div>

                <div class=" col-xs-12   col-sm-4    col-md-4 col-lg-4 article-card">
                  <img class="latest-img img img-responsive" src="{{url('img/article7.jpg')}}">
                  <h2 class="article-headtxt article-headsize">Benchmark Indices vs Mutual Funds </h2>
                  <p class="article-para  article-parasize">
                    With key Indices such as the Nifty &amp; Sensex Surging to over 28% in 2017, It was undoubtedly a year of Equites. Retail investors who invested in a large manner
                  </p>
                  <a href="{{url('/blog/benchmark_indices_vs_mutual_funds')}}" class="button button-spacing">Read more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg ')}}" id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
        </section>



  </body>
</html>