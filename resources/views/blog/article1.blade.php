<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">
    <meta property="og:image" content="img/article1.jpg" />
    <meta property="og:title" content="Why should I invest My money? " />

	  <title>Why should I invest My money? | Buy Mutual funds Online | Rightfunds</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
    <section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
              
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right navbar-menubar">
                    <li><a class="active_navbar_header " href="{{url('/blog')}}">Home</a></li> 

                    <li><a class="active_navbar_header   " href=" https://www.rightfunds.com/">Rightfunds.com</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>

    <section id="">
      <div class="container container-mod">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12 ">
                <img class="article-img img img-responsive" src="{{url('img/article1.jpg')}}">
            
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 article-spacing ">
              <h2 class="content-header">Why should I invest My money? </h2>
          </div>

                <div class="content-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12   ">
                  <p class="article-content">
                    In order to understand why we should begin investing our money let us understand three key financial concepts that will help us grow wealthy and understand money in relative terms.
                  </p>
                  <p class="content-subhead">
                    1.The concept of Inflation
                  </p>

                  <p class="article-content">
                    
                    Very simply, Inflation is the general increase in the price of goods and services in an economy. The result of inflation is a consequential decrease in purchasing power. Figuratively 1 lakh rupees in your bank account may only be ‘worth’ fifty thousand at the end of 10 years because of inflation. As things around you get expensive you are able to buy lesser and lesser of the same commodity.

                  </p>

                  <p class="article-rep article-content content-spacing">
                    The Graph below shows <strong class="strong"> the 'real value' of money adjusted for inflation over a period of 10 years.</STRONG>
                  </p>
                  <div class="img-spacing">
                    <img src="{{url('img/money stack.png')}} " class="img img-responsive">
                  </div>

                  <p class="article-content content-spacing">
                  From the graph it is evident that inflation erodes a significant proportion of an individual’s wealth over time. In order to protect our wealth from eroding away it is important to make the right investments. Good investments do not only protect you from inflation but also grow your wealth.
                </p>

                  <p class="content-subhead">
                    2.The concept of 'compounding opportunity'
                  </p>

                  <p class="article-content">
                    
                    Young professionals often delay investing their money. The general assumption is that a significant amount of wealth is needed to begin investing. At Rightfunds one may begin investing with just rupees 5,000 and watch their wealth grow. Regular monthly investments are a seamless antidote to volatile markets.

                  </p>
                  <p class="article-content">
                    
                    Compound interest is often called the 8th wonder of the world and for good reason. In compounding, the size of investment is of much lesser significance than the time period for which one invests their money. For those who appreciate a bit of math, Time is an exponential power, while the principle amount is linear.

                  </p>

                  <p class="article-content">
                  Let us assume you invest rupees 1 Lakh at 15% per annum when your 35. Upon your retirement at the age of 60 you decide to withdraw your investment. Using the compound interest formula your wealth has now grown 32 times to roughly 32.9 Lakhs, quite an amount right?
                  </p>

                  <p class="article-content">
                  Say you had begun investing ten years earlier at 25, any guesses to what the initial investment of 1 lakh would amount to?
                  </p>

                  <p class="article-content">
                  If you said 1.33 Crore, you guessed it right! Just adding 10 years to your investment has multiplied your returns over a 100 times. Now investing early does not sound like such a bad idea after all. The longer you wait more the ‘compounding opportunity’ you lose.
                  </p>
                  <p class="content-subhead">
                  3. Achieving Financial Independence
                </p>

                <p class="article-content">
                  Say you woke up one morning, and decided not to go to work anymore. You would now like to pursue a hobby that you’ve long dreamt of or maybe you’ve decided to work for yourself. Would the returns from your existing investments support your current life style? Savings may eventually run out. So unless you’ve developed a perennial source of income through investing your earnings wisely financial independence may remain a dream.
                  </p>

                  <p class="article-content">
                  So how does one achieve financial independence? To start off, you may want to put down a figure that is a fair estimate of how much money you would need a month to sustain your current lifestyle. Say it’s rupees 50,000. Now working backwards, if your investments yielded 15% returns per annum then you would need to have invested a total of 40 Lakh rupees to generate returns of 50,000 per month. [50,000 x 12 months / 15% ].
                  </p>

                  <p class="article-content">
                    While saving up 40 Lakh rupees may seem like a daunting task, investing just rupees 5000 a month from your salary in a systematic investment plan at the same rate would make you financially independent in a matter of 16 years!
                  </p>

                  <p class="article-content">
                  While there are no shortcuts to amassing wealth, investing wisely will put you on the right track to doing so. Begin investing today!
                  </p>
                  <hr class="endline breakline">
                  <div>

                  </div>

                </div>

                </div>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section id="recommended-article">
      <div class="container container-mod   ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head other-articles">More articles you might like:</h6>

                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                 
                  <h2 class="article-headtxt">Introduction to Mutual funds.</h2>
                  <p class="article-para">
                    
                    It involves pooling the money of investors such us ourselves into a sizable corpus. This amount is then invested into various securities such as stocks, bonds, treasury bills etc.
                  </p>
                  <a href="{{url('/blog/introduction_to_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                 
                  <h2 class="article-headtxt">How do I save on taxes with mutual funds ?</h2>
                  <p class="article-para">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 recommend-end">
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg')}} " id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us" class="aboutus">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us" class="contactus">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit" class="media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq" class="faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms" class="terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy" class="privacy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div> 
          <!-- <div >
        <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div>  -->
        </section>

  </body>
</html>