<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, minimum-scale=1">

	  <title>Rightfunds | Blog</title>
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/article.css')}}" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-114917491-1');
    </script>
  </head>

  <body class="body">
    <section id="navigationbar">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="nav-bar">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                  </button>
                  <a class="navbar-brand" href="{{url('/blog')}}">
                    <img src="{{url('img/nav-logo.svg')}} " class="nav-logo">
                  </a>
                </div>
                <div class="collapse navbar-collapse  navbar-menulist" id="myNavbar">
                  <ul class="nav navbar-nav navbar-header pull-right  navbar-menubar">
                    <li><a class="active_navbar_header" href="{{url('/blog')}}">Home</a></li> 
                    <li><a class="active_navbar_header" href=" https://www.rightfunds.com/">Rightfunds.com</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>       
        </div>
      </nav>  
    </section>

    <section id="">
      <div class="container container-mod">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12">
                <img class="article-img img img-responsive" src="{{url('img/article6.jpg')}}">
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 article-spacing ">
              <h2 class="content-header">What is a Systematic Investment Plan?</h2>
          </div>

                <div class="content-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12" id="article-para"  >
                  <p>
                    An SIP or a systematic investment plan is an investment methodology
                    wherein a fixed amount of money is invested into a mutual fund scheme or
                    any other asset class over regular intervals of time. It is usually done over a
                    period of many years. 
                  </p>
            
                  <p>
                    
                    Starting an SIP is beneficial in many ways. To begin with it does not require
                    an investor to commit a large sum of money upfront into an investment
                    product. It is ideal for investors who earn a regular income but do not have
                    a large corpus to invest. One may begin investing in a mutual fund SIP with
                    as little as rupees 1,000 each month. Upon the investors request the money
                    may be automatically debited from his or her bank account saving the
                    hassle of having to manually transfer money into the mutual fund scheme
                    each month.
                  </p>
                  <p>
                    The SIP approach to investing is especially useful when the markets are
                    volatile or overvalued thanks to a concept called <strong>'Rupee Cost Averaging’</strong>.
                 </p> 
                  <p >   
                    Say one was to invest a total of rupees 3,000 into a mutual fund scheme. In order to do so the investor had two choices, the first was to invest rupees 1000 each month for the next three months and the second was to invest the entire sum of rupees 3,000 in one go. What would be the impact of both these approaches on his profitability? Assuming he adopted the first method and the NAV of the fund in month one was rupees 200, he was able to purchase 5 units of the mutual fund scheme (1000/5).
                  </p>
                  <p>
                   The next month the NAV dropped to rupees 150, now, 6.6 units of the mutual fund scheme were bought (1000/150). The third month the NAV rose to rupees 225 enabling the investor to buy 4.4 units (1000/225). The investor would now own a total of 16 units of the mutual fund scheme at an average cost of rupees 187.5 per unit (3000/16). At the current market price of rupees 225, his investment would now be worth 225 x 16 = 3,600 rupees.
                  </p>
                  <p>
                    What if he had invested the entire amount of rupees 3000 in the first month at NAV 200? The investor would have obtained 3000/200 = 15 units at an average cost of rupees 200 per unit, and the market value of his investment at the end of the third month would only be 15 x 225 = 3,375 rupees.
                  </p>
                  <p>
                    By following a systematic investment plan of investing rupees 1000 each month theinvestor was able to lower his average cost of purchase and hence accrue more units.
                  </p>
                  <p>
                    This is rupee cost averaging, it is a seamless antidote to market volatility. In the long run rupee cost averaging coupled with compound interest can provide the investor great returns on his investments.
                  </p>
                  <p>
                    The success of an SIP is governed by the regularity of investments and the ability of an investor to stay invested for long periods of time. In other words, one needs to be a disciplined investor to reap benefits of a systematic investment plan. It is equally important to start early, an investment amount of rupees 1000 invested for 40 years at the rate of 15% CAGR would generate returns of rupees 3.1 crores. However, even if the investment is doubled to rupees 2000 and is invested for 20 years at 15% CAGR, then the future value would only be rupees 30.3 lakh.
                  </p>
                  <p>
                    If you are a young working professional and are unsure of how to begin investing your money, starting an SIP is probably your best bet. To understand why you should begin investing your money you can have a you can have a look at <a href="
                    https://www.rightfunds.com/blog/why_should_i_invest_my_money">why should i invest my money ?</a>
                  </p>
                  <p>
                    Use the <a href="https://www.sipcalculator.io">SIP Calculator</a> to estimate the returns you would generate on your monthly investments over a desired time horizon. Historically equity mutual funds Have generated 18 – 25% CAGR returns over a time horizon of at least five years. Given below is the list of top 5 mutual funds that have given over 20% returns for the last 20 years. To learn more about mutual funds, check out <a href="https://www.rightfunds.com/blog/introduction_to_mutual_funds" >Introduction to mutual funds.</a>
                  </p>
                  <p>
                    1)<strong> HDFC Equity Fund:</strong> Launched on January 1, 1995. Starting an SIP of Rs. 5,000 in this fund on April 1, 1998, your investment would have grown to Rs. 1.95 crore by April 1, 2017, which means a CAGR return of 23.56 per cent over 20 years.
                  </p>
                  <p>
                    2)<strong> Reliance Growth Fund:</strong> This fund has generated a CAGR return of 23.68 per cent since its inception. Your Rs. 5,000 SIP in this fund, started on April 1, 1998 would have helped you accumulate a corpus of Rs. 2.12 crore by April 1, 2017.
                  </p>
                  <p>
                    3) <strong>Franklin India Prima Fund:</strong> A flagship fund of Franklin Templeton India.
                      Starting an SIP of Rs. 5,000 in this fund on April 1, 1998, your investment
                      would have grown to Rs. 2.38 crore by April 1, 2017, giving a CAGR return
                      of 25.11 per cent over 20 years.
                  </p>
                  <p>
                    4) <strong>Reliance Vision Fund:</strong> Launched on October 8, 1995, it has generated a
                        CAGR return of 19.92 per cent till now. Your Rs. 5,000 SIP in this fund,
                        started on April 1,1998, would have become Rs. 1.46 crore by April 1, 2017.
                  </p>
                  <p>
                    5)<strong>HDFC Top 200 Fund:</strong> This flagship fund of HDFC Mutual Fund has
                        generated a CAGR return of 20.88 per cent since its inception in September
                        1996. If you would have started an SIP of Rs. 5,000 in this fund on April 1,
                        1998, your investment would have grown to Rs. 1.43 crore by April 1, 2017.
                  </p>
                  <hr class="endline breakline">
                </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>

    <section id="recommended-article">
      <div class="container container-mod ">
        <div class="row">
          <div class="latest-section col-lg-12   ">
              <h6 class="sub-head other-articles">More articles you might like:</h6>
                <div class="latest-articles col-lg-12 padding-lr-zero  " >
                 
                  <h2 class="article-headtxt">How do I save on taxes with mutual funds ?</h2>
                  <p class="article-para">
                   Paying a lot of taxes? Contemplating on how you could save more money? If so, you’re at the right place.
                  </p>
                  <a href="{{url('/blog/how_do_i_save_on_taxes_with_mutual_funds')}}" class="button button-spacing">Read more</a>
                  
                </div>
                <hr class="col-lg-12 sepration-line ">

                <div class="latest-articles col-lg-12  padding-lr-zero " >
                  <h2 class="article-headtxt">Why should I invest My money?</h2>
                  <p class="article-para">
                    In order to understand why we should begin investing our money let us understand three key financial concepts that will help us grow.
                  </p>
                  <a href="{{url('/blog/why_should_i_invest_my_money')}}" class="button button-spacing">Read more</a>
                  
                 
                </div>
                <hr class="col-lg-12">

              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <section id="footer-section">
      <div class="container"> 
        <div class="row">   
          <div class="padding-l-zero col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-4 col-md-4 col-sm-4 "> 
              <img src="{{url('img/footer-logo.svg')}} " id="footer-id" class="nav-logo"> 
              <p id="reg-company">©Prosperity Technology Private Limited,2017</p>
              <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
              <p class="footer-info">Company</p>
              <ul class="footer-list">
                  <li><a href="https://www.rightfunds.com/about-us">About us</a></li>
                  <li><a href="https://www.rightfunds.com/contact-us">Contact Us</a></li>
                  <li><a href="https://www.rightfunds.com/media-kit">Media and Press Kit</a></li>
              </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                  <p class="footer-info">Support</p>
                  <ul class="footer-list">
                      <li><a href="https://www.rightfunds.com/faq">FAQ</a></li>
                      <li><a href="https://www.rightfunds.com/terms">Terms of Use</a></li>
                      <li><a href="https://www.rightfunds.com/privacy_policy">Privacy Policy</a></li>
                  </ul>
              </div> 
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">  
                  <p class="footer-info">Follow us</p>
                  <ul class="list-inline" id="social-parent">
                      <li class="social-list"><a href="https://www.facebook.com/rightfunds/"><img src="{{url('img/facebook-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://twitter.com/rightfundsindia"><img src="{{url('img/twitter-logo.svg')}}" class="footer-social"></a></li>
                      <li class="social-list"><a href="https://www.linkedin.com/company/rightfunds.com"><img src="{{url('img/linkedin-logo.svg')}}" class="footer-social"></a></li>
                  </ul>
                </div>       
              </div> 
              <div class="padding-l-zero  col-lg-12 col-md-12 col-sm-12 col-xs-12" id="partners-container">   
                  <p class="footer-info p-l-15">AMC Partners</p>
                  <p id="amc-names" class="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&amp;T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                  <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                  <p class="footer-info p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><span id="arn">ARN : 116221</span></p>
              </div>   
            </div>  
          </div>  
          <div >
       <!--  <ul class="share">
          <li> <i class="material-icons share-icon">share</i></li>
          <li><a class="" href="#"><img src="img/facebook.svg" class="facebook"></a></li>
          <li><a class="" href="#"><img src="img/twitter.svg" class="twitter"></a></li>
          <li><a class="" href="#"><img src="img/linkedin.svg" class="linkedin"></a></li>
        </ul>  
      </div> -->
        </section>

  </body>
</html>