<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{URL('/css/admin/bootstrap.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-pending-orders.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/footer.css?v=1.1')}}">
    <link href="{{URL('../css/font-awesome.min.css" rel="stylesheet" type="text/css')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/extras.css?v=1.1')}}">

    <style type="text/css">
      th, td {
        text-align: center !important;
        font-size: 14px !important;
        width: 101px !important;
        color: gray;
        font-family: gotham_book;
      }

      .header-cont{
        margin-bottom: 20px;
      }

      #add_scheme_btn{
          font-size: 12px;
          background-color: #0091EA;
          border-color: #0091EA;
          padding: 8px 28px;
          border-radius: 3px;
          font-weight: lighter;
      }

      #top-nav{
        z-index: 10;
      }

      select{
        width: 100%;
        height: 35px;
        border-radius: 4px;
        border: 1px solid gainsboro;
      }

      .scheme_input_helper{
          text-align: left;
          margin-bottom: 0px;
          margin-top: 10px;
          font-size: 13px;
          color: gray;
          margin-bottom: 5px;
      }

      #submit_scheme{
        font-size: 14px;
        background-color: #0091EA;
        border-color: #0091EA;
        /* padding: 8px 28px; */
        border-radius: 3px;
        font-weight: lighter;
        margin: 10px auto;
      }

      .modal_header{
            font-size: 18px;
          margin-top: 15px;
      }

      #add_bse_scheme_code{
        margin-bottom: 20px;
      }

      #form_container{
        margin-bottom: 25px;
      }
    </style>
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="{{URL('../icons/logo.png')}}"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';"  id="logout-nav">LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">

    <div class = "row investment-summary">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-bar ">

              <div class="col-lg-12 col-md-12 col-sm-12  border-bot header-cont">
                <div class=" col-md-9 col-sm-9 col-lg-9">
                  <p id="scheme_header">Scheme details</p>
                </div> 

                <div class=" col-md-3 col-sm-3 col-lg-3">
                  <button type="button" id="add_scheme_btn" class="btn btn-primary">Add Scheme</button>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">

                <table class="table">
    <thead>
      <tr>
        <th>Scheme Name</th>
        <th>Scheme Code</th>
        <th>Scheme Type</th>
        <th>Scheme Priority</th>
        <th>BSE Scheme Code</th>
        <th>Scheme Status</th>
        <th></th>
      </tr>
    </thead>
    <tbody id="scheme_details">

    <?php //dd($schemes); ?>
      @foreach($schemes as $scheme)
        <tr>
          @if($scheme['scheme_priority'] < 0)
            <td>{{$scheme['scheme_name']}}</td>
            <td>{{$scheme['scheme_code']}}</td>
            <td>{{$scheme['scheme_type']}}</td>
            <td>{{$scheme['scheme_priority']}}</td>
            <td>{{$scheme['bse_scheme_code']}}</td>
            @if($scheme['scheme_status'] == 1)
            <td style="color: #0091EA;">Active</td>
            @elseif($scheme['scheme_status'] == 0)
            <td style="color: red;"">In-Active</td>
            @endif
            
          @else
            <td style="color: black;">{{$scheme['scheme_name']}}</td>
            <td style="color: black;">{{$scheme['scheme_code']}}</td>
            <td style="color: black;">{{$scheme['scheme_type']}}</td>
            <td style="color: black;">{{$scheme['scheme_priority']}}</td>
            <td style="color: black;">{{$scheme['bse_scheme_code']}}</td>
            @if($scheme['scheme_status'] == 1)
            <td style="color: #0091EA;">Active</td>
            @elseif($scheme['scheme_status'] == 0)
            <td style="color: red;">In-Active</td>
            @endif
          @endif
          <!--<td><i data-code="{{$scheme['scheme_code']}}" class="remove-scheme material-icons delete_scheme_icon">delete</i></td>-->
          <td><i data-code="{{$scheme['scheme_code']}}" class="edit-scheme material-icons edit_scheme_icon">mode_edit</i></td>
        </tr>
      @endforeach
    </tbody>
  </table>
  </div>
</div>                        
</div> <!--Search Bar Ends -->
</div>

<!--            <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about_us')}}">About</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <li><a href="{{URL('/disclosure')}}">Disclosures</a></li>
                    <li><a href="{{URL('/contact_us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="{{URL('../icons/twitter.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="{{URL('../icons/facebook.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="{{URL('../icons/linked_in.png')}}"></a>

                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
       -->

</div> <!--Content Ends -->

</div>

  <script src="{{URL('/js/jquery.min.js')}}"></script>
  <script src="{{URL('/js/bootstrap.min.js')}}"></script>
  <script src="{{URL('/js/scroller.js')}}"></script>
  <script src="{{URL('/js/smart_search.js')}}"></script>
  <script src="{{url('js/loader.js')}}"></script>
  <script src="{{url('js/manage_scheme.js')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>


<!-- To add new scheme-->
<div id="addSchemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <p class="text-center modal_header">Scheme Details</p>
      <div style="text-align:center;" id="add_scheme_status"></div>
      <div class="modal-body">

        <div class="row">
          <form id="add_scheme_form">
            <div class="col-lg-12 col-md-12 col-sm-12" id="form_container">

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme Name</p>
                <input type="text" name="add_scheme_name" class="scheme_input form-control center-block" id="add_scheme_name" required>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Fund Manager</p>
                <input type="text" name="add_fund_manager" class="scheme_input form-control center-block" id="add_fund_manager" required>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme Code</p>
                <input type="number" name="add_scheme_code" class="scheme_input form-control center-block" id="add_scheme_code" required>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Asset Size</p>
                <input type="text" name="add_asset_size" class="scheme_input form-control center-block" id="add_asset_size" required>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="add_scheme_type" class="scheme_select" name="add_scheme_type">
                  <option value="eq">Equity</option>
                  <option value="debt">Debt</option>
                  <option value="bal">Hybrid</option>
                  <option value="ts">ELSS</option>
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Fund type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="add_fund_type" class="scheme_select" name="add_fund_type">
                  <option value="ultrashortterm">Ultra Short Term</option>
                  <option value="creditopportunities">Credit Opportunities</option>
                  <option value="longterm">Long Term</option>
                  <option value="largecap">Large Cap</option>
                  <option value="diversified">Diversified</option>
                  <option value="hybrid">Hybrid</option>
                  <option value="smallmidcap">Small & Mid Cap</option>
                  <option value="sector">Sector</option>
                  <option value="liquid">Liquid</option>
                  <option value="floatingrate">Floating Rate</option>
                  <option value="gilt">Gilt</option>
                  <option value="accrual">Accrual</option>
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Benchmark</p>
                <input type="text" name="add_benchmark" class="scheme_input form-control center-block" id="add_benchmark" required>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme Priority</p>
                <input type="text" name="add_scheme_priority" class="scheme_input form-control center-block" id="add_scheme_priority" required>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Exit Load</p>
                <input type="text" name="add_exit_load" class="scheme_input form-control center-block" id="add_exit_load" required>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Launch Date</p>
                <input type="text" name="add_launch_date" class="scheme_input form-control center-block" id="add_launch_date" required>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">BSE Scheme Code</p>
                <input type="text" name="add_bse_scheme_code" class="scheme_input form-control center-block" id="add_bse_scheme_code" required>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Historic NAV</p>
                <input type="file" name="add_scheme_nav" class="scheme_input form-control center-block" id="add_scheme_nav" required>
              </div>


            </div>
            <button type="submit" class="btn btn-primary center-block" id="submit_scheme">Add Scheme</button>
          </form>
        </div>


      </div>
    </div>

  </div>
</div>

<!-- End of add scheme-->

<!-- To update scheme-->
<div id="updateSchemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <p class="text-center modal_header">Scheme Details</p>
      <div style="text-align:center;" id="update_scheme_status"></div>
      <div class="modal-body">

        <div class="row">
          <form id="update_scheme_form">
            <input type="hidden" id='old_scheme_code' value="">
            <div class="col-lg-12 col-md-12 col-sm-12" id="form_container">

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme Name</p>
                <input type="text" name="update_scheme_name" class="scheme_input form-control center-block" id="update_scheme_name" required>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Fund Manager</p>
                <input type="text" name="update_fund_manager" class="scheme_input form-control center-block" id="update_fund_manager" required>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme Code</p>
                <input type="number" name="update_scheme_code" class="scheme_input form-control center-block" id="update_scheme_code" required disabled>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Asset Size</p>
                <input type="text" name="update_asset_size" class="scheme_input form-control center-block" id="update_asset_size" required>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="update_scheme_type" class="scheme_select" name="update_scheme_type">
                  <option value="eq">Equity</option>
                  <option value="debt">Debt</option>
                  <option value="bal">Hybrid</option>
                  <option value="ts">ELSS</option>
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Fund type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="update_fund_type" class="scheme_select" name="update_fund_type">
                  <option value="ultrashortterm">Ultra Short Term</option>
                  <option value="creditopportunities">Credit Opportunities</option>
                  <option value="longterm">Long Term</option>
                  <option value="largecap">Large Cap</option>
                  <option value="diversified">Diversified</option>
                  <option value="hybrid">Hybrid</option>
                  <option value="smallmidcap">Small & Mid Cap</option>
                  <option value="sector">Sector</option>
                  <option value="liquid">Liquid</option>
                  <option value="floatingrate">Floating Rate</option>
                  <option value="gilt">Gilt</option>
                  <option value="accrual">Accrual</option>
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Benchmark</p>
                <input type="text" name="update_benchmark" class="scheme_input form-control center-block" id="update_benchmark" required>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme Priority</p>
                <input type="text" name="update_scheme_priority" class="scheme_input form-control center-block" id="update_scheme_priority" required>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Exit Load</p>
                <input type="text" name="update_exit_load" class="scheme_input form-control center-block" id="update_exit_load" required>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Bse Scheme Code</p>
                <input type="text" name="update_bse_scheme_code" class="scheme_input form-control center-block" id="update_bse_scheme_code" required >
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Launch Date</p>
                <input type="text" name="update_launch_date" class="scheme_input form-control center-block" id="update_launch_date" required>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme status</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="update_scheme_status" class="scheme_select" name="update_scheme_status">
                  <option value="1">Enable</option>
                  <option value="0">Disable</option>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-primary center-block" id="submit_scheme">Update Scheme</button>
          </form>
        </div>


      </div>
    </div>

  </div>
</div>

<!-- end of update scheme-->


<div id="userStatusModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Status</h4>
      </div>
      <div class="modal-body">
        <p id="addition_status" class="mont-reg text-center"></p>
      </div>
    </div>

  </div>
</div>


</body>
</html>
