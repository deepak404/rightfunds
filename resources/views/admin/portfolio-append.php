<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" id="export-row">

                  <div class="col-lg-10 col-md-10 col-sm-10">
                      <p class="hereshow">Show history for<span class="styled-select slate">
                            <select>
                              <option>Last 6 months</option>
                              <option>Last 1 year</option>
                              <option>Last 2 years</option>
                          
                            </select>
                          </span></p>
                   </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <button type="button" id="export-btn" class="btn btn-primary">EXPORT</button>
                  </div>

                </div><!-- export-row ends -->                
            </div><!-- Row ends -->








                      <form id = "units-held-form">
              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">EQUITY FUNDS</p>
                    <div class="table-wrapper" id="equity-table-wrapper">
                     <table class="table" id="equity-table">
                    
                        <thead>
                          <tr>
                            <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                            <th>Folio Number</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Units Held</th>
                            <th>Current Nav(Rs)</th>
                            <th>Current Value(Rs)</th>
                            <th>Net Returns(Rs)</th>
                            <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                            
                          </tr>
                        </thead> <button type="button" class="btn btn-primary" id="equity-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                  </button>
                        <tbody>
                          <tr>
                            <td>Kotak Mahindra mutual Funds  Mahindra mutual Funds  </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                            <td class="ltc-amount">2000</td>
                            
                            
                          </tr>
                          <tr>
                            <td>Kotak Mahindra mutual Funds </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><input type="number" name="" id = "inp-units-val" style = "display:none;"><p id = "p-unit-val">1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                             <td class="ltc-amount">2000</td>
                               
                          </tr>
                          <tr>
                            <td class="total  bt-green">TOTAL</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"></td>
                             <td class="total bt-green">45000</td>
                            <td class="total  bt-green">57000</td>
                            <td class="total  bt-green">57000</td>
                             <td class="total bt-green">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>
                              
                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>



              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">DEBT FUNDS</p>
                    <div class="table-wrapper" id="debt-table-wrapper">
                     <table class="table" id="equity-table">
                        <thead>
                          <tr>
                            <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                            <th>Folio Number</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Units Held</th>
                            <th>Current Nav(Rs)</th>
                            <th>Current Value(Rs)</th>
                            <th>Net Returns(Rs)</th>
                            <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="debt-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                              </button>
                        <tbody>
                          <tr>
                            <td>Kotak Mahindra mutual Funds  Mahindra mutual Funds  </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                             <td class="ltc-amount">2000</td>

                            
                          </tr>
                          <tr>
                            <td>Kotak Mahindra mutual Funds </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                             <td class="ltc-amount">2000</td>    
                          </tr>
                          <tr>
                            <td class="total bt-green ">TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                             <td class="total bt-green ">45000</td>
                            <td class="total bt-green ">57000</td>
                            <td class="total bt-green ">57000</td>
                             <td class="total bt-green ">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>    
                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>



                               <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">HYBRID FUNDS</p>
                    <div class="table-wrapper" id="hybrid-table-wrapper">
                     <table class="table" id="equity-table">
                        <thead>
                          <tr>
                            <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                            <th>Folio Number</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Units Held</th>
                            <th>Current Nav(Rs)</th>
                            <th>Current Value(Rs)</th>
                            <th>Net Returns(Rs)</th>
                            <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                            <th>Absolute Returns(Rs)</th>
                            <th>Annualised Returns(Rs)</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                            </button>
                        <tbody>
                          <tr>
                            <td>Kotak Mahindra mutual Funds  Mahindra mutual Funds  </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                            <td class="ltc-amount">2000</td>
                            <td>800</td>
                             <td>800</td>
                            
                          </tr>
                          <tr>
                            <td>Kotak Mahindra mutual Funds </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                            <td class="ltc-amount">2000</td>
                            <td>800</td>
                             <td>800</td>      
                          </tr>


        </form> <!-- Units held form -->
                         

                          <tr>
                             <td class="total bt-green ">TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                             <td class="total bt-green ">45000</td>
                            <td class="total bt-green ">57000</td>
                            <td class="total bt-green ">57000</td>
                             <td class="total bt-green ">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>
                            <td class="total bt-green ">800</td>
                             <td class="total bt-green ">800</td>   

                          </tr>

                          <tr>
                            <td class="empty"></td>
                            <td class="empty"></td>
                            <td class="empty"></td>
                             <td class="empty"></td>
                            <td class="empty"></td>
                            <td class="empty"></td>
                             <td class="empty"></td>
                            <td class="empty"></td>
                            <td class="empty"></td>
                             <td class="empty"></td>  
                             
                          </tr>


                           <tr>
                             <td class="total bt-green ">GRAND TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                             <td class="total bt-green ">45000</td>
                            <td class="total bt-green ">57000</td>
                            <td class="total bt-green ">57000</td>
                             <td class="total bt-green ">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>
                            <td class="total bt-green ">800</td>
                             <td class="total bt-green ">800</td>   

                          </tr>


                        </tbody>

                      </table>
                    </div>
              </div>






              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">HYBRID FUNDS</p>
                    <p class="subnote">No amount was invested between</p>
                    <p class="subnote" id="contact-sub">1 Jan 2016 to 1 April 2016 in this category</p>
              </div>