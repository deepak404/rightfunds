<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{url('/css/admin/admin-overview.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('/css/admin/admin-overview-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css?v=1.1')}}">
</head>
<body>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
                    @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>

<div class = "container">


  <div class="content">

     <!-- <div class = "row"> -->
          <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>

              </div> -->

              <!--  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">{{\Auth::user()->name}}</p>
                      <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                    </div>

              </div> -->

              

              

          </div> <!--Profile Bar Ends -->
    <!-- </div> -->


          <div class = "row investment-summary">

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">Investment Summary</p>
                <p class = "is-details" > <?php echo date('j M Y');?>.</p>
              </div>

              <div class = "row">

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row">

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <p class = "int-topic">Total Customers</p>
                        <span class = "amount">{{$overview_data['total_user_count']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <p class = "int-topic">Total assets under management</p>
                        <span class = "amount">{{$overview_data['total_amount_invested']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <p class = "int-topic">Total Scheme offered</p>
                        <span class = "amount">{{$overview_data['total_schemes_offered']}}</span>
                      </div>


                  </div> <!-- in-det-row ends --> 


                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row req-reb">

                      <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <p class = "int-topic">No of One time investments</p>
                        <span class = "amount">{{$overview_data['onetime_count']}}</span>
                      </div>

                      <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <p class = "int-topic">Total No of SIP investments</p>
                        <span class = "amount">{{$overview_data['sip_count']}}</span>
                      </div>



                          
                  </div> <!-- in-det-row ends --> 
              </div>

<!--               <div class="row hr">
                      
              </div>


              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block" style="display: none;">
                <p class = "is-head por-head">Portfolio Performance</p>
                
              </div>

              <div class = "row" style="display: none;">

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row port-det-row">

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Conservative</p>
                        <span class = "amount">{{$overview_data['debt_investment_percent']}}%</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Moderate</p>
                        <span class = "amount">{{$overview_data['balanced_investment_percent']}}%</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Aggressive</p>
                        <span class = "amount">{{$overview_data['equity_investment_percent']}}%</span>
                      </div>


                  </div> 
              </div> -->

              <div class="row hr">
                      
              </div><!-- HR ends -->


                <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                    
                    <p class = "is-head por-head" style="margin-top: 15px;">New Signup</p>
                
                </div>

              <div class = "row">

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row pen-det-row">

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Daily</p>
                        <span class = "amount">{{$overview_data['new_user_today']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Weekly</p>
                        <span class = "amount">{{$overview_data['new_user_week']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Monthly</p>
                        <span class = "amount">{{$overview_data['new_user_month']}}</span>
                      </div>


                  </div>

                </div>

                   

          </div>


        <div class = "row investment-summary">
          <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">Pending Transaction</p>
                                <p class = "is-details" ><?php echo date('j M Y');?>.</p>
              </div>

              <div class = "row">

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row pen-det-row">

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Activation Pending</p>
                        <span class = "amount">{{$overview_data['nach_pending']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Pending SIP transaction</p>
                        <span class = "amount">{{$overview_data['sip_this_week']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Pending Onetime transaction</p>
                        <span class = "amount">{{$overview_data['pending_onetime_count']}}</span>
                      </div>

                      <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <p class = "int-topic">SIP Transaction this week</p>
                        <span class = "amount">{{$overview_data['sip_this_week']}}</span>

                      </div>
                      <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <p class = "int-topic">Non KYC users</p>
                        <span class = "amount">{{$overview_data['kyc_pending']}}</span>
                      </div>


                  </div> <!-- in-det-row ends --> 
              </div>

          </div>


            <div class = "row investment-summary">

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">Demographics</p>
                <p class = "is-details" ><?php echo date('j M Y');?>.</p>
              </div>

              <div class = "row">

                 <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row dem-row">

                      <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <p class = "int-topic">Average Investment per customer</p>
                        <span class = "amount">{{$overview_data['average_investment_per_customer']}}</span>
                      </div>

                      <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <p class = "int-topic">Male/Female</p>
                        <span class = "amount">{{$overview_data['male_female_customer']}}</span>
                      </div>

                  </div> 

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row dem-row">

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic"> <1 lakh </p>
                        <span class = "amount">{{$overview_data['less_than_lakh']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">1-10 lakhs</p>
                        <span class = "amount">{{$overview_data['less_than_10lakh']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">> 10 lakh</p>
                        <span class = "amount">{{$overview_data['greater_than_10lakh']}}</span>
                      </div>


                  </div> <!-- in-det-row ends --> 

              </div>     

          </div>



         <!--  <div class = "row investment-summary">
              <div class = "row">


               <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head por-head" style="margin-top: 15px;">New Signup</p>
                
              </div>
              </div> 

              <div class = "row">

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row pen-det-row">

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Daily</p>
                        <span class = "amount">{{$overview_data['new_user_today']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Weekly</p>
                        <span class = "amount">{{$overview_data['new_user_week']}}</span>
                      </div>

                      <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p class = "int-topic">Monthly</p>
                        <span class = "amount">{{$overview_data['new_user_month']}}</span>
                      </div>


                  </div> <!-- in-det-row ends --> 
              </div>



          </div>

 -->
          


          <!-- <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about-us')}}">About us</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                    <!-- <li><a href="{{URL('/contact-us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="../icons/twitter.png"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="../icons/facebook.png"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="../icons/linked_in.png"></a>
                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div> -->
        <!-- </div> --> 
      

</div> <!--Content Ends -->

</div>



          <!-- Modal -->
<div id="addGoalModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="setGoalModalBody">

        <p class="text-center" id="set-g-track">Set goals to stay on track!</p>
         <p class="text-center" id="inv-worth">I would like to have investments worth</p>

        <div class="input-group input-group-md goal-amount-block center-block ">
                  <span class="input-group-addon " id="inv-rs">Rs</span>
                  <input type="number" class="form-control" placeholder="Your Amount" aria-describedby="basic-addon1" id="inv-amount" required="">
        </div>
        <button type="button" class="btn btn-primary center-block" id="set-g-btn">SET GOAL</button>

       

        

       


            
          

      </div>
    </div>
  </div>
</div>

  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>

</body>
</html>
