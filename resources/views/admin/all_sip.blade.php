<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> 


    <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{URL('/css/admin/admin-pending-orders.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/footer.css?v=1.1')}}">
    <link href="{{URL('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/pending_orders_1.1.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/suggestions.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/extras.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
    <style type="text/css">
      .pending-btn,.pending-btn:hover,.pending-btn:focus{
        background-color: #0091EA;
        border-color: #0091EA;
        float: right;
        margin-left: 10px;
        margin-top: 20px;
        margin-right: 0px;
        border-radius: 2px;
      }

      #pending-btn-container{
        float: right;
      }

      .confirm-upload-btn,.confirm-upload-btn:hover,.confirm-upload-btn:focus{
        background-color: #0091EA;
        border-color: #0091EA;
        margin-left: 10px;
        margin-top: 20px;
        margin-right: 0px;
        border-radius: 2px;
        width: 60px;
      }

      input[type=file]{
        display: none;
      }

      #pending-btn-container .material-icons{
        vertical-align: middle;
      }

      #top-nav{
        z-index: 10;
      }

      #confirm-allot{
        text-align: center;
        font-size: 16px;
        font-family: gotham_medium;
        color: #0091EA;
      }

      #confirm-upload-container{
        text-align: center;
      }

      .confirm-info{
        font-size: 16px;
        font-family: gotham_book;
      }

      .footer-row{
        margin: 20px auto;
      }
    </style>
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';">LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!-- <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li> -->
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
                    @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>
  
<div class = "container">
    <div class = "row investment-summary">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero ">
              
                  @foreach($sip_array as $sip)
                  <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero pending-details-container" data-invid = "">

                    <div class="col-lg-10 col-md-10 col-sm-10">
                      <p class="pend_inv_name">{{$sip['investor_name']}}</p>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2">
                      <i class="material-icons pull-right pending-order-details-cl" data-toggle = "collapse" data-target="#{{$sip['sip_id']}}">keyboard_arrow_down</i>
                    </div>

                    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero pend-det-info-cont">
                          <div class="col-lg-9 col-md-9 col-sm-9">
                              <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                                <p class="pending-det-info" id="pan_holder">PAN : 12346</p>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                                <p class="pending-det-info bold">Investment</p>
                              </div>
                              <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                                <p class="pending-det-info">COnservative</p>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3 padding-lr-zero">
                                <p class="pending-det-info">Monthly</p>
                              </div>
                            
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-3 bse-alert-holder">
                            <span class="bse-badge-success">BSE Pass</span>
                          </div>
                    </div> -->

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          
                          <p id="sip_placed">Tenure : <span id="sip_placed_date">{{$sip['tenure']}} Years</span></p>
                        
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                        <!-- <p id="sip_placed">Tenure : <span id="sip_placed_date">{{$sip['tenure']}} Years</span></p> -->
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4">
                        <p class="text-center pull-right" id="pending-inv-amount">Rs.{{$sip['sip_amount']}}</p>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 collapse pending-inv-details-holder" id="{{$sip['sip_id']}}">

                            @foreach($sip_details_array as $sip_details)

                                @foreach($sip_details as $sip_detail)
                                  @if($sip_detail['sip_id'] == $sip['sip_id'])
                                    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-top">
                                      <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero pending-details-scheme-container">
                                        
                                        
                                        
                                      </div>

                                      <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                                        <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                                          <p class="pending-details-scheme">
                                            {{$sip_detail['scheme_name']}}
                                          </p>
                                        </div>
                                         <div class="col-lg-4 col-md-4 col-sm-4 padding-r-zero">
                                          <p class="pull-right pending-amount-details">Rs.{{$sip_detail['inv_amount']}}</p>
                                        </div>
                                      </div>
                                    </div>
                                  @endif

                                @endforeach

                            @endforeach()
                  
                    </div><!-- One ends -->

                  </div> <!--Pending Details Container ends -->
                  @endforeach

                </div>
                </div>
              <!-- Pending Investments ends here -->

           <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about_us')}}">About</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <li><a href="{{URL('/disclosure')}}">Disclosures</a></li>
                    <li><a href="{{URL('/contact_us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="{{URL('/icons/twitter.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="{{URL('/icons/facebook.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="{{URL('/icons/linked_in.png')}}"></a>

                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
      

</div> <!--Content Ends -->

</div>



<div id="scheduleInvModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Update Info</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">

            <button type="button" onclick="javascript:location.href='/admin/pending_orders'" class="btn btn-primary text-center" id="notify-done">Done</button>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>

<div id="confirmAllotModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="confirm-upload-container">
            <p id="confirm-allot">Allotment Status</p>

            <p class="text-center confirm-info">Are you Sure want to upload the allotment Status ?</p>
            <button class="btn btn-primary confirm-upload-btn" value="yes" id="confirm-allot-upload">Yes</button>
            <button class="btn btn-primary confirm-upload-btn" value="no" id="cancel-allot-upload">No</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="confirmRedemptionModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="confirm-upload-container">
            <p id="confirm-allot">Redemption Status</p>

            <p class="text-center confirm-info">Are you Sure want to upload the redemption Status ?</p>
            <button class="btn btn-primary confirm-upload-btn" value="yes" id="confirm-redemption-upload">Yes</button>
            <button class="btn btn-primary confirm-upload-btn" value="no" id="cancel-redemption-upload">No</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



  <script src="{{URL('/js/jquery.min.js')}}"></script>
  <script src="{{URL('/js/bootstrap.min.js')}}"></script>
  <!--<script src="../js/index.js"></script>-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
  <script src="{{URL('/js/scroller.js')}}"></script>
  <script src="{{URL('/js/smart_search.js')}}"></script>
  <script src="{{url('js/loader.js')}}"></script>

  <script src="{{url('js/new_pending_order.js')}}"></script>


  <script>

  $(document).ready(function(){
      //alert($('.bse_date_selector:checkbox:checked').length);

      // $.each('.pending-inv-details-holder',function(){
      //   console.log($(this).find('.bse_date_selector:checkbox').length);
      // });

      $('.pending-inv-details-holder').each(function(){
        //console.log($(this).find('.bse_date_selector:checkbox:checked').length);

        var success_bse_length = $(this).find('.bse_date_selector[data-bstatus=0]:checkbox:checked').length;
        var failed_bse_length = $(this).find('.bse_date_selector[data-bstatus=1]:checkbox:checked').length;
        var available_bse_checkbox = $(this).find('.bse_date_selector:checkbox').length;
        

        if (success_bse_length == available_bse_checkbox) {
              $(this).parent().find('.pend-det-info-cont').find('.bse-alert-holder').append('<span class="bse-badge-success">BSE Pass</span>');
        }else if (failed_bse_length != 0 && success_bse_length != 0) {
              $(this).parent().find('.pend-det-info-cont').find('.bse-alert-holder').append('<span class="bse-badge-partial">BSE placed Partially</span>');
        }
        else{
          $(this).parent().find('.pend-det-info-cont').find('.bse-alert-holder').append('<span class="bse-badge-failure">BSE fail</span>');
        }
      })
  });

  </script>



</body>
</html>
