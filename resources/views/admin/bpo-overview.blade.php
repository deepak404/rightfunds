<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{url('/css/admin/admin-overview.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('/css/admin/admin-overview-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css?v=1.1')}}">
</head>
<body>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
          @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>

<div class = "container">
  <div class="content">
    <div class = "row investment-summary">
      <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
        <p class = "is-head">Investment Summary</p>
        <p class = "is-details" > <?php echo date('j M Y');?>.</p>
      </div>
      <div class = "row">

          <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row" style="margin-bottom: 35px !important;">

              <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <p class = "int-topic">Total Customers</p>
                <span class = "amount">{{$count}}</span>
              </div>

              <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <p class = "int-topic">Total assets under management</p>
                <span class = "amount">{{$total}}</span>
              </div>

          </div> <!-- in-det-row ends --> 
      </div>
<!--       <div class="row hr">  
      </div> -->
    </div>
  </div> <!--Content Ends -->
</div>


          <!-- Modal -->
<div id="addGoalModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="setGoalModalBody">

        <p class="text-center" id="set-g-track">Set goals to stay on track!</p>
         <p class="text-center" id="inv-worth">I would like to have investments worth</p>

        <div class="input-group input-group-md goal-amount-block center-block ">
                  <span class="input-group-addon " id="inv-rs">Rs</span>
                  <input type="number" class="form-control" placeholder="Your Amount" aria-describedby="basic-addon1" id="inv-amount" required="">
        </div>
        <button type="button" class="btn btn-primary center-block" id="set-g-btn">SET GOAL</button>

       

        

       


            
          

      </div>
    </div>
  </div>
</div>

  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>

</body>
</html>
