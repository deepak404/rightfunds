<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL('../css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-det.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-det-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/footer.css?v=1.1')}}">
    <link href="{{URL('/css/font-awesome.min.css" rel="stylesheet" type="text/css')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/suggestions.css?v=1.1')}}">
  <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>

        <a class="navbar-brand" href="/"><img src="{{URL('../icons/logo.png')}}"></a>

      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
                    @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
        </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <!-- <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>
              </div>
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
              </div>

          </div> --> <!--Profile Bar Ends -->
    <!-- </div> -->


     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 details-bar">

<!--               <div class="col-md-1 col-sm-1 col-xs-12">
                 <img  class = "img profile-image det-prof-image" src="{{URL('../icons/profile_icon.jpg')}}">
              </div> -->


              <div class="col-md-2 col-sm-2 col-xs-12" id="user-det">
                 <p class="det-per-name">{{$modify_order_details['name']}}</p>
                 <p class="det-per-mail">{{$modify_order_details['email']}}</p>
                 <p class="det-per-phone">{{$modify_order_details['mobile']}}</p>
              </div>


              <div class="col-md-2 col-sm-2 col-xs-4 text-center margin-10">
                 <p class="det-heading">Transaction type</p>
                 <p class="det-content">{{$modify_order_details['transaction_type']}}</p>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-4 text-center margin-10">
                 <p class="det-heading">Portfolio type</p>
                 <span class="badge center-block moderate-badge">{{$modify_order_details['portfolio_type']}}</span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-4 text-center margin-10">
                 <p class="det-heading">Category</p>
                 <p class="det-content">{{$modify_order_details['investment_type']}}</p>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-6 text-center margin-10">
                 <p class="det-heading">Date</p>
                 <p class="det-content">{{$modify_order_details['date']}}</p>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-6 text-center margin-30">
                 <div>
                   <p class="det-heading">Amount</p>
                 <p class="det-content det-amount">&#x20B9;{{$modify_order_details['amount']}}</p>
                 </div>
              </div>

              
          </div> <!--Profile Bar Ends -->
    </div>

    
  <div class = "row investment-summary">
    <div class="table-group row">
      <form id="modify_orders" method="post" action="/admin/modify_pending_orders" >
        <div class="col-xs-12">
          <input type='hidden' value="{{$modify_order_details['transaction_type']}}" id="transaction_type" name="transaction_type">
          {{csrf_field()}}
          @foreach($modify_order_details['portfolio_details'] as $scheme_type => $portfolio_detail)
            @if($scheme_type == 'eq')
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title fund-title">
                    EQUITY         
                  </h3>
                </div>
        
                <table class="table">
                  <tbody id = "equity-body">
                    @foreach($portfolio_detail as $portfolio)
                      <tr>
                        <td class = "fund-name col-xs-10">
                          <input type="hidden" name="portfolio_details[{{$portfolio['portfolio_id']}}][id]" value="{{$portfolio['portfolio_id']}}">
                          <div class = "col-xs-1 padding-lr-zero checkbox-div"><input name="portfolio_details[{{$portfolio['portfolio_id']}}][checked]" type="checkbox" id ="{{$portfolio['portfolio_id']}}_checkbox" checked>
                            <label for="{{$portfolio['portfolio_id']}}_checkbox"><span class="checkbox"><img src="{{URL('../icons/check_en.png')}}"></span></label>
                          </div>

                          <div class = "col-xs-11 fund-name-div"><span class = "span-fund-name">{{$portfolio['scheme_name']}}</span>
                          </div>
                              
                        </td>
                            
                        <td class = "fund-amount"><p class = "fund-amount-inner">Rs. {{$portfolio['amount']}}</p></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @elseif($scheme_type == 'debt')
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title fund-title"> DEBT </h4>
                </div>
          
                <table class="table">
                  <tbody id = "debt-body">
                    @foreach($portfolio_detail as $portfolio)
                      <tr>
                        <td class = "fund-name">
                          <input type="hidden" name="portfolio_details[{{$portfolio['portfolio_id']}}][id]" value="{{$portfolio['portfolio_id']}}">
                          <div class = "col-xs-1 padding-lr-zero checkbox-div"><input name="portfolio_details[{{$portfolio['portfolio_id']}}][checked]"type="checkbox" id = "{{$portfolio['portfolio_id']}}_checkbox" checked>
                            <label for="{{$portfolio['portfolio_id']}}_checkbox"><span class="checkbox"><img src="{{URL('../icons/check_en.png')}}"></span></label>
                          </div>

                          <div class = "col-xs-11 fund-name-div">
                            <span class = "span-fund-name">{{$portfolio['scheme_name']}}</span>
                          </div>
                        </td>
                      
                        <td class = "fund-amount"><p class = "fund-amount-inner">Rs. {{$portfolio['amount']}}</p></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @elseif($scheme_type == 'bal')
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title fund-title"> HYBRID </h4>
                </div>
          
                <table class="table">
                  <tbody id = "elss-body">
                    @foreach($portfolio_detail as $portfolio)
                      <tr>
                        <td class = "fund-name">
                          <input type="hidden" name="portfolio_details[{{$portfolio['portfolio_id']}}][id]" value="{{$portfolio['portfolio_id']}}">
                          <div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id ="{{$portfolio['portfolio_id']}}_checkbox" name="portfolio_details[{{$portfolio['portfolio_id']}}][checked]" checked>
                            <label for="{{$portfolio['portfolio_id']}}_checkbox"><span class="checkbox"><img src="{{URL('../icons/check_en.png')}}"></span></label>
                          </div>
                          <div class = "col-xs-11 fund-name-div">
                            <span class = "span-fund-name">{{$portfolio['scheme_name']}}</span>
                          </div>
                        </td>
                      
                        <td class = "fund-amount"><p class = "fund-amount-inner">Rs. {{$portfolio['amount']}}</p></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @elseif($scheme_type == 'ts')
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title fund-title"> ELSS </h4>
                </div>
          
                <table class="table">
                  <tbody id = "elss-body">
                    @foreach($portfolio_detail as $portfolio)
                      <tr>
                        <td class = "fund-name">
                          <input type="hidden" name="portfolio_details[{{$portfolio['portfolio_id']}}][id]" value="{{$portfolio['portfolio_id']}}">
                          <div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id ="{{$portfolio['portfolio_id']}}_checkbox" name="portfolio_details[{{$portfolio['portfolio_id']}}][checked]" checked>
                            <label for="{{$portfolio['portfolio_id']}}_checkbox"><span class="checkbox"><img src="{{URL('../icons/check_en.png')}}"></span></label>
                          </div>
                          <div class = "col-xs-11 fund-name-div">
                            <span class = "span-fund-name">{{$portfolio['scheme_name']}}</span>
                          </div>
                        </td>
                      
                        <td class = "fund-amount"><p class = "fund-amount-inner">Rs. {{$portfolio['amount']}}</p></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            @endif
          @endforeach
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title fund-title"> TOTAL
                <p style="float:right;" id="total-amt">Rs. {{$modify_order_details['amount']}}</p>
              </h4>
            </div>        
          </div>
        </div>
        <div class= "col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <input type = "submit" class = "btn btn-primary" id = "wd-btn" value="ORDER COMPLETE">
        </div>
      </form>
    </div>
  </div>
          <!-- <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="/">Home</a></li>
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/disclosure')}}">Disclosures</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{URL('../icons/twitter.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="{{URL('../icons/facebook.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">

                    <a href=""><img id = "" src="{{URL('../icons/linked_in.png')}}"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>


 -->
          </div>
      

</div> <!--Content Ends -->

</div>




  <script src="{{URL('/js/jquery.min.js')}}"></script>
  <script src="{{URL('/js/bootstrap.min.js')}}"></script>
  <script src="{{URL('/js/jquery.donut.js')}}"></script>
  <script src="{{URL('/js/index.js?v=1.1')}}"></script>
  <script src="{{URL('/js/modify_investment.js?v=1.1')}}"></script>
  <script src="{{URL('/js/admin_pending_orders.js')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{URL('/js/scroller.js?v=1.1')}}"></script>
    <script src="{{url('/js/loader.js?v=1.1')}}"></script>


  <script>
    $('.from-to').datepicker({ dateFormat: "dd-mm-yy",changeMonth: true, changeYear: true, yearRange: '2016:2050' });


    $('.checkbox-div input').on('change', function() {
        var parentDiv=$(this).parents(".checkbox-div:eq(0)");
        if($(this).is(":checked")){
          $(parentDiv).find("img").attr("src","{{URL('../icons/check_en.png')}}");
        }
        else{
        $(parentDiv).find("img").attr("src","{{URL('../icons/check_dis.png')}}");
        }
    });
  </script>

    <!-- Modal -->
<div id="cancelApproved" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Great !</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">
            <button type="button" onclick="javascript:location.href='/admin/order_history'" class="btn btn-primary text-center" id="notify-done">Done</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
