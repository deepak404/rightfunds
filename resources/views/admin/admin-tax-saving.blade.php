<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="{{URL('css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{URL('css/admin/admin-tax-saving.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/admin-tax-saving-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/footer.css?v=1.1')}}">
    <link href="{{URL('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{URL('/css/suggestions.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li>
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
        </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <!-- <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>


              </div>

              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                  <p class="profile-name">{{\Auth::user()->name}}</p>
                  <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                </div>
              </div>
               
          </div>  --><!--Profile Bar Ends -->
  <!--   </div> -->


     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-bar ">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-r-zero">
                          <input type="text" name="search-input" id="search-input" placeholder="Search with Name/Mobile/PAN ">
            </div>

            <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 trans-filter padding-lr-zero">

                  <span class = "last-t">Last Transaction</span>
                  <select class = "filter-search-date">
                    <option>1 Day</option>
                    <option><1 week</option>
                    <option><1 month</option>
                    <option><6 month</option>

                  </select>
                  

            </div>

<!--             <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-zero srch-btn-div">

                <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-lr-zero filter-btn-div">
                  <button type = "button" class = "search-filter-btn btn btn-primary">Newly Enrolled</button>
                </div>

                <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12  padding-lr-zero filter-btn-div">
                  <button type = "button" class = "search-filter-btn btn btn-primary">Rebalance Req.</button>
                </div>

            </div> -->
                        

          </div> <!--Search Bar Ends -->
     </div>





            <div id="user_performance_det">
            
            </div>

            <!--<div class = "row investment-summary">

              

             <div class = "row" id="user-det-row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                      <img class="img profile-image" src="../../icons/profile_icon.jpg">
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">Mr. {{\Auth::user()->name}}!</p>
                      <p class="invest-text" id="search-email">{{\Auth::user()->email}}</p>
                      <p id ="srch-user-no">{{\Auth::user()->mobile}}</p>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p class="srch-options"> Rebalancing Required</p>
                    <img src= "../../icons/rebal.png" class="center-block" id="rebal-icon">
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p class="srch-options">Portfolio type</p>
                    <span class="badge center-block moderate-badge">MODERATE</span>
                  </div>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Current Value</p>
                    <p class="return-det"><span class="rs-sym">&#x20B9;</span>60000</p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Total Investment</p>
                    <p class="return-det"><span class="rs-sym">&#x20B9;</span>50000</p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Net Profit</p>
                    <p class="return-det"><span class="rs-sym">&#x20B9;</span>10000</p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Return (XIRR)</p>
                    <p class="return-det">23%</p>
                  </div>
                </div>

                  

              </div>-->     

            </div>

        <div class = "row investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/portfolio_details/{{$id}}"  target="_blank"><li class="list-headings">Portfolio Details</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/inv_history/{{$id}}" target="_blank"><li class="list-headings ">Investment History</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/tax_saving/{{$id}}" target="_blank"><li class="list-headings active-page">Tax Saving Statements</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{ URL('admin/kyc') }}" target="_blank"><li class="list-headings no-b-right padding-small">KYC Details</li></a>
                    </div>                 
                  
               </ul>
             
             </div><!-- Heading ends -->

            
            <div class="row" id="export-row">
                <div class="col-lg-12 col-md-12 col-sm-12 statement-div">

                  <div class="col-lg-9 col-md-9 col-sm-9">
                      <p class="fin-text">Financial year <span class = "fin-year">
                            2015-2016
                          </span></p>
                   </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 btn-div">
                      <button type="button" id="upload-ts" class="btn btn-primary fin-btn upload-tax-btn">Upload</button>
                    <!--<button type="button" id="sende-btn" class="btn btn-primary fin-btn">Send Email</button>
                    <button type="button" id="savepdf-btn" class="btn btn-primary fin-btn">Save as PDF</button>-->
                  </div>

                </div><!-- statement-div ends --> 

                <div class="col-lg-12 col-md-12 col-sm-12 statement-div">

                  <div class="col-lg-9 col-md-9 col-sm-9">
                      <p class="fin-text">Financial year <span class = "fin-year">
                            2017-2018
                          </span></p>
                   </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 btn-div">
                    <button type="button" id="sende-btn" class="btn btn-primary fin-btn upload-tax-btn">Upload</button>
                    <!--<button type="button" id="savepdf-btn" class="btn btn-primary fin-btn">Save as PDF</button>-->
                  </div>

                </div><!-- statement-div ends -->                   
          </div><!-- export-row ends -->          
              

        </div><!--- Investment summary row ends-->



         


          
          





          <!-- <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li> -->
                <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                <!-- <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="../../icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="../../icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="../../icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>
 -->


          </div>
      

</div> <!--Content Ends -->

</div>




  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>
  <script src="{{URL('js/jquery.donut.js')}}"></script>
  <script src="{{URL('js/index.js?v=1.1')}}"></script>
  <script src="{{URL('js/checkbox.js?v=1.1')}}"></script>
  <script src="{{URL('js/scroller.js?v=1.1')}}"></script>
  <script src="{{URL('js/admin-portfolio.js?v=1.1')}}"></script>
  <script src="{{URL('js/jquery-ui.js')}}"></script>
  <script src="{{URL('js/smart_search.js?v=1.1')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
    <script src="{{url('js/loader.js?v=1.1')}}"></script>

</body>
</html>
