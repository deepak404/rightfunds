<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL('css/admin/bootstrap.min.css?v=1.1')}}">

    <link rel="stylesheet" href="{{URL('css/admin/admin-inv-history.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/admin-inv-history-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/loader.css?v=1.1')}}">
    <link href="{{URL('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

  
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="{{URL('icons/logo.png')}}"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
        <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
                    @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <!-- <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar ">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                  <p class="profile-name">Admin Dashboard</p>
                  <p class="invest-text">Get your latest account statement.</p>
                </div>
              </div> -->
              <!-- <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                  <p class="profile-name">{{\Auth::user()->name}}</p>
                  <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                </div>
              </div> -->

          <!-- </div> Profile Bar Ends  -->
     <!-- </div> -->


     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-bar ">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-r-zero">
              <input type="text" name="search-input" id="search-input" placeholder="Search with Name/Mobile/PAN ">
            </div>
<!-- 
            <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 trans-filter padding-lr-zero">

              <span class = "last-t">Last Transaction</span>
              <select class = "filter-search-date" id="transact_range">
                <option value="1">1 Day</option>
                <option value="2"><1 week</option>
                <option value="3"><1 month</option>
                <option value="4"><6 months</option>
              </select>
            </div>
 -->
            <!--<div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-zero srch-btn-div">

                <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-lr-zero filter-btn-div">
                  <button type = "button" class = "search-filter-btn btn btn-primary">Newly Enrolled</button>
                </div>

                <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12  padding-lr-zero filter-btn-div">
                  <button type = "button" class = "search-filter-btn btn btn-primary">Rebalance Req.</button>
                </div>

            </div>-->
                        

          </div> <!--Search Bar Ends -->
     </div>







          <div id="user_performance_det">
              <div class = "row investment-summary" id="user-det-row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">{{$userName}}</p>
                      <p class="invest-text" id="search-email">{{$userEmail}}</p>
                      <p id ="srch-user-no">{{$userMobile}}</p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Total Investment</p>
                    <p class="return-det">Rs.<span id="amount_invested">{{$investment_status['amount_invested']}}</span></p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Current Value</p>
                    <p class="return-det">Rs.<span id="current_value">{{$investment_status['current_value']}}</span></p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Net Profit</p>
                    <p class="return-det">Rs.<span id="profit_loss">{{$investment_status['profit_loss_amount']}}</span></p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Return (XIRR)</p>
                    <p class="return-det"><span id="xirr_return">{{$investment_status['xirr']}}</span>%</p>
                  </div>
                </div>

          </div>

        <div class = "row investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/portfolio_details/{{$id}}" id="inv_port" target="_blank"><li class="list-headings ">Portfolio Details</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/inv_history/{{$id}}"  id="inv_his" target="_blank"><li class="list-headings active-page">Investment History</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/tax_saving/{{$id}}" id="inv_ts" target="_blank"><li class="list-headings ">Tax Saving Statements</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{ URL('admin/kyc') }}" target="_blank"><li class="list-headings no-b-right padding-small">KYC Details</li></a>
                    </div>                   
                  
               </ul>
             
             </div><!-- Heading ends -->

            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" id="export-row">

                  <!--<div class="col-lg-10 col-md-10 col-sm-10">
                      <p class="hereshow">Show history for<span class="styled-select slate">
                            <select>
                              <option>Last 6 months</option>
                              <option>Last 1 year</option>
                              <option>Last 2 years</option>
                          
                            </select>
                          </span></p>
                   </div>-->

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <form id="export_data" action="/admin/get_ind_user_investment_performance" method="post" target="_blank">
                      {{csrf_field()}}
                      <input type="hidden" name="user_id" id="user_id"/>
                      <input type="hidden" name="transaction_range" id="transaction_range"/>
                      <input type="hidden" name="response_type" id="response_type" value="docmument" />
                      <input type="submit" id="export-btn" class="btn btn-primary" value="EXPORT" />
                    </form>
                  </div>

                </div><!-- export-row ends -->                
              </div><!-- Row ends -->
              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">

              <p class="funds-name">EQUITY FUNDS</p>
                    <div class="table-wrapper" id="equity-table-wrapper">
                     <table class="table" id="equity-table">
                    
                        <thead>
                          <tr>
                            <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                            <th>Type of Investment</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Transaction Status</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="equity-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                         @if(array_key_exists("eq", $investment_summary))
                        <tbody id="equity_body">
                          @foreach($investment_summary['eq'] as $eq)
                          <tr>
                            <td>{{$eq['date']}}</td>
                            <td>{{$eq['investment_type']}}</td>
                            <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$eq['inv_amount']}}</div></td>
                            <td>{{$eq['investment_status']}}</td>
                          </tr>
                          @endforeach
                          <tr>
                            <td class="total  bt-green">TOTAL AMOUNT INVESTED</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading"></div></td>
                            <td class="total  bt-green"></td>
                          </tr>
                        </tbody>
                        @else
                        <tbody id="equity_body"><tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr></tbody>
                        @endif
                      </table>

                    </div>
              </div>


              
              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">DEBT FUNDS</p>
                    <div class="table-wrapper" id="debt-table-wrapper">
                     <table class="table" id="equity-table">
                         <thead>
                          <tr>
                            <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                            <th>Type of Investment</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Transaction Status</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="debt-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    @if(array_key_exists("debt", $investment_summary))
                        <tbody id="debt_body">
                          @foreach($investment_summary['debt'] as $debt)
                          <tr>
                            <td>{{$debt['date']}}</td>
                            <td>{{$debt['investment_type']}}</td>
                            <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$debt['inv_amount']}}</div></td>
                            <td>{{$debt['investment_status']}}</td>
                          </tr>
                          @endforeach
                          <tr>
                            <td class="total  bt-green">TOTAL AMOUNT INVESTED</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading"></div></td>
                            <td class="total  bt-green"></td>
                          </tr>
                        </tbody>
                        @else
                       <tbody id="debt_body"><tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr></tbody>
                        @endif
                      </table>
                    </div>
              </div>

              

               <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">BALANCED FUNDS</p>
                    <div class="table-wrapper" id="hybrid-table-wrapper">
                     <table class="table" id="equity-table">
                        <thead>
                          <tr>
                            <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                            <th>Type of Investment</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Transaction Status</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                      @if(array_key_exists("bal", $investment_summary))
                        <tbody id="balanced_body">
                          @foreach($investment_summary['bal'] as $bal)
                          <tr>
                            <td>{{$bal['date']}}</td>
                            <td>{{$bal['investment_type']}}</td>
                            <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$bal['inv_amount']}}</div></td>
                            <td>{{$bal['investment_status']}}</td>
                          </tr>
                          @endforeach
                          <tr>
                            <td class="total  bt-green">TOTAL AMOUNT INVESTED</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading"></div></td>
                            <td class="total  bt-green"></td>
                          </tr>
                        </tbody>
                        @else
                       <tbody id="balanced_body"><tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr></tbody>
                        @endif
                      </table>
                    </div>
              </div>


              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">TAX SAVER</p>
                    <div class="table-wrapper" id="hybrid-table-wrapper">
                     <table class="table" id="equity-table">
                        <thead>
                          <tr>
                            <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                            <th>Type of Investment</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Transaction Status</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                        </button>
                    @if(array_key_exists("ts", $investment_summary))
                        <table class="table" id="elss_body">
                          @foreach($investment_summary['ts'] as $ts)
                          <tr>
                            <td>{{$ts['date']}}</td>
                            <td>{{$ts['investment_type']}}</td>
                            <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$ts['inv_amount']}}</div></td>
                            <td>{{$ts['investment_status']}}</td>
                          </tr>
                          @endforeach
                          <tr>
                            <td class="total  bt-green">TOTAL AMOUNT INVESTED</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading"></div></td>
                            <td class="total  bt-green"></td>
                          </tr>
                        </tbody>
                        @else
                       <tbody id="elss_body"><tr><td colspan="4"><p class="subnote">No amount was invested between</p><p class="subnote" id="contact-sub">the selected period in this category</p></td></tr></tbody>
                      @endif

                      </table>
                    </div>
                    <!--<p class="subnote">No amount was invested between</p>
                    <p class="subnote" id="contact-sub">1 Jan 2016 to 1 April 2016 in this category</p>-->
              </div>


              

        </div><!--- Investment summary row ends-->


          <!-- <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <!<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                   <!--  <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{URL('icons/twitter.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="{{URL('icons/facebook.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{URL('icons/linked_in.png')}}"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>
 -->


          </div>
      

</div> <!--Content Ends -->

</div>




  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>
  <script src="{{URL('js/jquery.donut.js')}}"></script>
  <!--<script src="{{URL('js/index.js')}}"></script>-->
  <script src="{{URL('js/checkbox.js?v=1.1')}}"></script>
  <script src="{{URL('js/scroller.js?v=1.1')}}"></script>
  <script src="{{URL('js/jquery-ui.js')}}"></script>
  <script src="{{URL('js/smart_search.js?v=1.1')}}"></script>
  <script src="{{URL('js/admin_inv_history.js?v=1.1')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>

</body>
</html>
