<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="../css/bootstrap.min.css">

    <link rel="stylesheet" href="../css/admin/admin-amc.css?v=1.1">
    <link rel="stylesheet" href="../css/admin/admin-amc-responsive.css?v=1.1">
    <link rel="stylesheet" href="../css/footer.css?v=1.1">
</head>
<body>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
        <li class="active menu-text "><a href="{{URL('/admin/overview')}}">Overview</a></li>
        <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
        <li class="menu-text menu-text-active"><a href="{{URL('#')}}">AMC Performance</a></li>
        <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li>
        <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
        <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
        <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar ">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>

              </div>
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                    
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                </div>

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                  <p class="profile-name">{{\Auth::user()->name}}</p>
                  <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                </div>

              </div>

          </div> <!--Profile Bar Ends -->
     </div>


     



            <div class = "row investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="portfolio_details.html"><li class="list-headings active-page">Conservative</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="inv_history.html"><li class="list-headings">Moderate</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="tax_saving.html"><li class="list-headings">Aggressive</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="withdraw_funds.html"><li class="list-headings no-b-right ">Fund Analysis</li></a>
                    </div>                 
                  
               </ul>
             
             </div><!-- Heading ends -->

             <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                <p id="summary" class="text-center">SUMMARY</p>

                <div class="table-wrapper" id="summary-wrapper">
                  <table id="summary-table" class="table">
                    <thead>
                      <tr>
                        <th class="th-scheme-name">Scheme Name</th>
                        <th><a href = "#" id = "daily" class="daily">Daily</a></th>
                        <th><a href = "#" id = "1week" class="other-heading">1 Week</a></th>
                        <th><a href = "#" id = "1month" class="other-heading">1 Month</a></th>
                        <th><a href = "#" id = "3month" class="other-heading">3 Months</a></th>
                        <th><a href = "#" id = "6month" class="other-heading">6 Months</a></th>
                        <th><a href = "#" id = "1year" class="other-heading">1 Year</a></th>
                        <th><a href = "#" id = "3year" class="other-heading">3 Years</a></th>
                        <th><a href = "#" id = "5year" class="other-heading">5 Years</a></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><div class="col-xs-12">Portfolio Performance</div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                      </tr>

                      <tr>
                        <td><div class="col-xs-12">
                              <select class="fund-select-1">
                                <option>Birld Sun life fund</option>
                                <option>Reliance life fund</option>
                                <option>ICICI Prudent Fund</option>
                              </select>
                            </div>
                        </td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                      </tr>


                       <tr>
                        <td><div class="col-xs-12">
                              <select class="fund-select-1">
                                <option>Birld Sun life fund</option>
                                <option>Reliance life fund</option>
                                <option>ICICI Prudent Fund</option>
                              </select>
                            </div>
                        </td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- Table Wrapper ends -->
             </div>

             <div class="hr"></div>
             <div class="row" id="graph-div">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div id="inner-graph"></div>
                </div>
             </div><!-- Graph div ends-->


             <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                <p id="summary" class="text-center">DEBT</p>

                <div class="table-wrapper" id="debt-wrapper">
                  <table id="summary-table" class="table">
                    <thead>
                      <tr>
                        <th class="th-scheme-name">Scheme Name</th>
                        <th class="other-heading">%</th>
                        <th class="daily">Daily</th>
                        <th class="other-heading">1 Week</th>
                        <th class="other-heading">1 Month</th>
                        <th class="other-heading">3 Month</th>
                        <th class="other-heading">6 Months</th>
                        <th class="other-heading">1 Year</th>
                        <th class="other-heading">3 Years</th>
                        <th class="other-heading">5 Years</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id = "checkbox1">
                      <label for="checkbox1"><span class="checkbox"><img src="../icons/check_dis.png"></span></label>
                    </div>
                    <div class="col-xs-11"><p class="debt-scheme-name">Birls SL frontline fund</p></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>

                      </tr>

                      <tr>
                        <td>
                        <div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id = "checkbox2">
                            <label for="checkbox2"><span class="checkbox"><img src="../icons/check_dis.png"></span></label>
                        </div>
                        <div class="col-xs-11">
                              <p class="debt-scheme-name">Birla Sunlife Equity</p>
                        </div>
                        </td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                      </tr>


                       <tr>
                        <td>
                        <div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id = "checkbox3">
                          <label for="checkbox3"><span class="checkbox"><img src="../icons/check_dis.png"></span></label>
                         </div>
                        <div class="col-xs-11">
                              <p class="debt-scheme-name">Kotak Select Focus fund Regular</p>
                            </div>
                        </td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                      </tr>


                      <tr id="overall-per">
                        <td><div class="col-xs-12">
                              <p class="debt-over-per">Overall Performance</p>
                            </div>
                        </td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="pos-per">0.1%</span></div></td>
                        <td><div class="col-xs-10"><span class="neg-per">0.1%</span></div></td>
                      </tr>                      
                    </tbody>                    
                  </table>
                 
                </div><!-- Table Wrapper ends -->
                 <button type="button" class="btn btn-primary center-block" id="update-sche-btn">UPDATE</button>
             </div>

            </div><!--- Investment summary row ends-->



         


          
          





          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                    <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="../icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="../icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="../icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>



          <!-- Modal -->
<div id="addGoalModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="setGoalModalBody">

        <p class="text-center" id="set-g-track">Set goals to stay on track!</p>
         <p class="text-center" id="inv-worth">I would like to have investments worth</p>

        <div class="input-group input-group-md goal-amount-block center-block ">
                  <span class="input-group-addon " id="inv-rs">Rs</span>
                  <input type="number" class="form-control" placeholder="Your Amount" aria-describedby="basic-addon1" id="inv-amount" required="">
        </div>
        <button type="button" class="btn btn-primary center-block" id="set-g-btn">SET GOAL</button>

       

        

       


            
          

      </div>
    </div>
  </div>
</div>

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/jquery.donut.js"></script>
  <script src="../js/index.js?v=1.1"></script>
  <script src="../js/checkbox.js?v=1.1"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

   <script>
     google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCurveTypes);

function drawCurveTypes() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Portfolio Performance');
      data.addColumn('number', 'Birla Sunlife');

      data.addRows([ // Pattern format (X,Dogs,Cats)
        [0, 0, 0],    [1, 10, 5],   [2, 23, 15],  [3, 17, 9],   [4, 18, 10],  [5, 9, 5],
        [6, 11, 3],   [7, 27, 19],  [8, 33, 25],  [9, 40, 32],  [10, 32, 24], [11, 35, 27],
        [12, 30, 22], [13, 40, 32], [14, 42, 34], [15, 47, 39], [16, 44, 36], [17, 48, 40],
        [18, 52, 44], [19, 54, 46], [20, 42, 34], [21, 55, 47], [22, 56, 48], [23, 57, 49],
        [24, 60, 52], [25, 50, 42], [26, 52, 44], [27, 51, 43], [28, 49, 41], [29, 53, 45],
        [30, 55, 47], [31, 60, 52], [32, 61, 53], [33, 59, 51], [34, 62, 54], [35, 65, 57],
        [36, 62, 54], [37, 58, 50], [38, 55, 47], [39, 61, 53], [40, 64, 56], [41, 65, 57],
        [42, 63, 55], [43, 66, 58], [44, 67, 59], [45, 69, 61], [46, 69, 61], [47, 70, 62],
        [48, 72, 64], [49, 68, 60], [50, 66, 58], [51, 65, 57], [52, 67, 59], [53, 70, 62],
        [54, 71, 63], [55, 72, 64], [56, 73, 65], [57, 75, 67], [58, 70, 62], [59, 68, 60],
        [60, 64, 56], [61, 60, 52], [62, 65, 57], [63, 67, 59], [64, 68, 60], [65, 69, 61],
        [66, 70, 62], [67, 72, 64], [68, 75, 67], [69, 150, 72]
      ]);

      var w = window.innerWidth;
      var h = window.innerHeight;

      console.log(w,h);


      if (w > 1200) {
        var chart_height = 350 ;
        var chart_width = 750;
      }

      if (w >= 992 && w <= 1200) {
        var chart_height = 400 ;
        var chart_width = 800;
      }

       if (w >= 768 && w <= 992) {
        var chart_height = 400 ;
        var chart_width = 700;
      }

      if (w >= 500 && w <= 768) {
        var chart_height = 400 ;
        var chart_width = 450;
      }

      if (w >= 350 && w <= 500) {
        var chart_height = 300 ;
        var chart_width = 350;
      }

      if (w >= 300 && w <= 350) {
        var chart_height = 250 ;
        var chart_width = 270;
      }



      var options = {
        hAxis: {
          title: 'Performance',

        },
        vAxis: {
          title: 'Growth'
        },
        series: {
          1: {curveType: 'function'}
        },

        height: chart_height
        ,width: chart_width


      };

      var chart = new google.visualization.LineChart(document.getElementById('inner-graph'));
      chart.draw(data, options);
    }
   </script>

</body>
</html>
