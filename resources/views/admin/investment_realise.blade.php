<!-- Investment Summary Email starts -->
<html>
  <head>

    <meta property="og:title" content="*|MC:SUBJECT|*">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet">
    <title>*|MC:SUBJECT|*</title>
    
  <style type="text/css">
		#outlook a{
			padding:0;
		}
		body{
			width:100% !important;
		}
		body{
			-webkit-text-size-adjust:none;
		}
		body{
			box-shadow:10px 10px grey;
			margin:0;
			padding:0;
		}
		img{
			border:none;
			font-size:14px;
			font-weight:bold;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
			text-transform:capitalize;
		}
		#backgroundTable{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		.box{
			margin:-100px;
			width:100%;
			background-color:#388ccc;
		}
	/*
	@tab Page
	@block background color
	@tip Set the background color for your email. You may want to choose one that matches your company's branding.
	@theme page
	*/
		body,.backgroundTable{
			/*@tab Page
@block background color
@tip Set the background color for your email. You may want to choose one that matches your company's branding.
@theme page*/box-shadow:10px 10px grey;
			width:500px;
			/*@editable*/background-color:#F7f7f7;
		}

		#templateContainer{
			margin-top:50%;
			border:1px solid #DDDDDD;
		}

		h1,.h1{
			/*@editable*/color:#202020;
			display:block;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:34px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			margin-bottom:10px;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@block heading 2
	@tip Set the styling for all second-level headings in your emails.
	@theme heading2
	*/
		h2,.h2{
			/*@editable*/color:#202020;
			display:block;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:30px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			margin-bottom:10px;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@block heading 3
	@tip Set the styling for all third-level headings in your emails.
	@theme heading3
	*/
		h3,.h3{
			/*@editable*/color:#202020;
			display:block;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:26px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			margin-bottom:10px;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@block heading 4
	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	@theme heading4
	*/
		h4,.h4{
			/*@editable*/color:#202020;
			display:block;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:22px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			margin-bottom:10px;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@block preheader style
	@tip Set the background color for your email's preheader area.
	@theme page
	*/
		#templatePreheader{
			/*@editable*/background-color:#FAFAFA;
			margin-top:50%;
		}
	/*
	@tab Header
	@block preheader text
	@tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
	*/
		.preheaderContent div{
			/*@editable*/color:#505050;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:10px;
			/*@editable*/line-height:100%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@block preheader link
	@tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
	*/
		.preheaderContent div a:link,.preheaderContent div a:visited{
			/*@editable*/color:#336699;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		.preheaderContent div img{
			height:auto;
			max-width:600px;
		}
	/*
	@tab Header
	@block header style
	@tip Set the background color and border for your email's header area.
	@theme header
	*/
		#templateHeader{
			/*@editable*/background-color:#FFFFFF;
			/*@editable*/border-bottom:0;
		}
	/*
	@tab Header
	@block header text
	@tip Set the styling for your email's header text. Choose a size and color that is easy to read.
	*/
		.headerContent{
			/*@editable*/color:#202020;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:34px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			/*@editable*/padding:0;
			/*@editable*/text-align:center;
			/*@editable*/vertical-align:middle;
			border-top:6px solid red;
		}
	/*
	@tab Header
	@block header link
	@tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
	*/
		.headerContent a:link,.headerContent a:visited{
			/*@editable*/color:#336699;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		#headerImage{
			height:auto;
			max-width:800px !important;
		}
	/*
	@tab Body
	@block body style
	@tip Set the background color for your email's body area.
	*/
		#templateContainer,.bodyContent{
			/*@tab Body
@block body style
@tip Set the background color for your email's body area.*/;
			border-radius:0px;
			/*@editable*/background-color:#fff;
			    border: none;
		}
	/*
	@tab Body
	@block body text
	@tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
	@theme main
	*/
		.bodyContent div{
			/*@editable*/color:#505050;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:14px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@block body link
	@tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
	*/
		.bodyContent div a:link,.bodyContent div a:visited{
			/*@editable*/color:#336699;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		.bodyContent img{
			display:inline;
			margin-bottom:10px;
			border-radius:1px;
		}
	/*
	@tab Footer
	@block footer style
	@tip Set the background color and top border for your email's footer area.
	@theme footer
	*/
		#templateFooter{
			/*@tab Footer
@block footer style
@tip Set the background color and top border for your email's footer area.
@theme footer*/margin-top:1%;
			/*@editable*/background-color:#FDFDFD;
			/*@editable*/border-top:0;
		}
	/*
	@tab Footer
	@block footer text
	@tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
	@theme footer
	*/
		.footerContent div{
			/*@editable*/color:#707070;
			/*@editable*/font-family: 'Open Sans', sans-serif;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:125%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Footer
	@block footer link
	@tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
	*/
		.footerContent div a:link,.footerContent div a:visited{
			/*@editable*/color:#336699;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		.footerContent img{
			display:inline;
		}
	/*
	@tab Footer
	@block social bar style
	@tip Set the background color and border for your email's footer social bar.
	*/
		#social{
			/*@editable*/background-color:#FAFAFA;
			/*@editable*/border:1px solid #F5F5F5;
		}
	/*
	@tab Footer
	@block social bar style
	@tip Set the background color and border for your email's footer social bar.
	*/
		#social div{
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@block utility bar style
	@tip Set the background color and border for your email's footer utility bar.
	*/
		.table{
			/*@tab Footer
@block utility bar style
@tip Set the background color and border for your email's footer utility bar.*/box-shadow:10px 10px grey;
		}
		#utility{
			/*@editable*/background-color:#F7f7f7;
			/*@editable*/border-top:1px solid #F5F5F5;
		}
	/*
	@tab Footer
	@block utility bar style
	@tip Set the background color and border for your email's footer utility bar.
	*/
		#utility div{
			/*@editable*/text-align:center;
		}
		#monkeyRewards img{
			max-width:160px;
		}
		.title1{
			margin-top:10px;
			font-size:26px;
			color:#388ccc;
		}

		pre{
			font-family:'Open Sans',sans-serif;
		}


		table {
    font-family: 'Open Sans', sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: white;
}

.pending{
color : #ec972a!important;
}

.failure{
color : #f33!important;
}

.success{
color :#00ab3e!important;
}


</style></head>
  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <br>
        
        <table border="0" cellpadding="15" cellspacing="10" width="700" id="templateBody">
          <tr>
            <td valign="top" class="bodyContent">
              
              
              <table border="0" cellpadding="10" cellspacing="0" width="100%">
                <tr>
                  <td valign="top" style="border:none !important;">
                    <div mc:edit="std_content00">
                      <img src="http://www.rightfunds.com/mail_logo.png" alt="logo">

                      <br>
                      Hello {{$user_name}},<br>
                      <br>
                      
                     Here is the Summary of your {{$investment_type}} made on <strong>{{$investment_date}}</strong> of <strong>Rs.{{$amount_invested}}.</strong><br>
                      <br>


                    <table>
						  <tr>
						    <th>Scheme Name</th>
						    <th>Amount</th>
						    <th>Status</th>
						  </tr>

						@foreach($investment_details as $investment_detail)
							@if($investment_detail['status'] == '0')
								<tr>
									<td>{{$investment_detail['scheme_name']}}</td>
									<td>{{$investment_detail['scheme_amount']}}</td>
									<td class="pending"><strong>Pending</strong></td>
								</tr>
							@elseif($investment_detail['status'] == '1')
								<tr >
									<td>{{$investment_detail['scheme_name']}}</td>
									<td>{{$investment_detail['scheme_amount']}}</td>
									<td class="success"><strong>Success</strong></td>
								</tr>
							@elseif($investment_detail['status'] == '2')
								<tr>
									<td>{{$investment_detail['scheme_name']}}</td>
									<td>{{$investment_detail['scheme_amount']}}</td>
									<td class="failure"><strong>Failure</strong></td>
								</tr>
							@endif
						@endforeach

						  
					</table><br>
                      
                      

                     If you face any other problem kindly write to  contact@rightfunds.com<br>
                     Or Just Call - <strong>88258 88200</strong> <br><br>

                     Regards,<br>
                     Rightfunds team<br>

                        
                          
                          
                        </div>
                      </td>
                    </tr>
                  </table>
                 
                </td>
              </tr>
            </table>
            
            <br>
      </body>
</html>

<!-- Investment Summary Email ends -->

