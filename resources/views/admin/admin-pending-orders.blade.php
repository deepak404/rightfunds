<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="/css/hello.css?v=1.1">

    <link rel="stylesheet" href="{{URL('/css/admin/admin-pending-orders.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/footer.css?v=1.1')}}">
    <link href="{{URL('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/suggestions.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/extras.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
        </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <!-- <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>


              </div> -->
<!-- 
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                  <p class="profile-name">{{\Auth::user()->name}}</p>
                  <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                </div>
              </div> -->
               <!--<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">Mr. Sandeep!</p>
                      <p class="invest-text" id="admin-email">sandeep@gmail.com</p>
                    </div>

              </div>-->

              

              

          <!-- </div> Profile Bar Ends  -->
    <!-- </div> -->


    <div class = "row investment-summary">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-r-zero">
              <input type="text" name="search-input" id="search-input" placeholder="Search with Name/Mobile/PAN ">
            </div>

            <form id="export_data" action="/admin/get_user_pending_orders" method="post" target="_blank">
              {{csrf_field()}}
              <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 trans-filter padding-lr-zero">
                <span id="export-text">Export Date</span>
                <sapn><input type="hidden" name="user_id" id="user_id" value=""/></sapn>
                <span><input type="text" name="from_exp" name="from_exp" placeholder="From" class="from-to"></span>
                <span><input type="text" name="to_exp" name="to_exp" placeholder="To" class="from-to"></span>
              </div>

              <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-12 padding-lr-zero srch-btn-div">
                <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-lr-zero filter-btn-div">
                  <input type = "submit" class = "btn btn-primary" id="export-btn" value="EXPORT" />
                </div>
              </div>
            </form>
                        
          </div> <!--Search Bar Ends -->
     </div>

          



            <div class = "row investment-summary">

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  
                </div>
              </div>
              

              <div class = "row">
                <form id="nach-update" method = "post">
                 <div class="col-lg-12 col-md-12 col-sm-12">
                   <div id="nach-table">
                     <table>
                      <thead>

                         <tr class="heading">
                           <th><div class="col-xs-12">Customer Details</div></th>
                           <th><div class="col-xs-12">Transaction Type</div></th>
                           <th><div class="col-xs-12">Portfolio Type</div></th>
                           <th><div class="col-xs-12">Category</div></th>
                           <th><div class="col-xs-12">Date</div></th>
                           <th><div class="col-xs-12">Amount</div></th>
                           <th><div class="col-xs-12">Details</div></th>                         
                          </tr>

                      </thead><button type="button" class="btn btn-primary" id="nach-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
</button>

                       <tbody>
                          @foreach($investment_details as $investment_detail)
                            <tr class="table-row">
                              <td><div class="col-xs-12"><p class="cus-name">{{$investment_detail['user_name']}}</p><p class="cus-phone">{{$investment_detail['contact_no']}}</p></div></td>
                              <td>{{$investment_detail['investment_or_withdraw']}}</td>
                              <td>{{$investment_detail['investment_type']}}</td>
                              <td>{{$investment_detail['portfolio_type']}}</td>
                              <td>date</td>
                              <td>{{$investment_detail['amount']}}</td>
                              @if($investment_detail['investment_or_withdraw'] == 'Investment') 
                                <td><a href="/admin/pending_details/investment/id/{{$investment_detail['investment_id']}}"><img src="../icons/pending-det.png" class = "pend-img"></a></td>
                              @else
                                <td><a href="/admin/pending_details/withdraw/id/{{$investment_detail['investment_id']}}"><img src="../icons/pending-det.png" class = "pend-img"></a></td>
                              @endif
                            </tr>
                          @endforeach
                       </tbody>


                     </table>                     
                   </div>
                   <!--<button type="button" class="btn btn-primary center-block" id="update-nach">UPDATE</button>-->
                 </div>
                </form>
                  

              </div>     

          </div>

           <!-- <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about-us')}}">About us</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li> -->
                    <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                    <!-- <li><a href="{{URL('/contact-us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="{{URL('/icons/twitter.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="{{URL('/icons/facebook.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="{{URL('/icons/linked_in.png')}}"></a>

                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
       -->

</div> <!--Content Ends -->

</div>




  <script src="{{URL('/js/jquery.min.js')}}"></script>
  <script src="{{URL('/js/bootstrap.min.js')}}"></script>
  <!--<script src="../js/index.js"></script>-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
  <script src="{{URL('/js/scroller.js')}}"></script>
  <script src="{{URL('/js/smart_search.js')}}"></script>
    <script src="{{url('js/loader.js')}}"></script>


  <script>
    $('.from-to').datepicker({ dateFormat: "dd-mm-yy",changeMonth: true, changeYear: true, yearRange: '2016:2050' });
  </script>



</body>
</html>
