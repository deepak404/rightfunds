<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{url('css/admin/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('css/admin/admin-kyc.css?v=1.2')}}">
  <link rel="stylesheet" href="{{url('css/admin/admin-kyc-responsive.css?v=1.2')}}">
  <link rel="stylesheet" href="{{url('css/footer.css?v=1.2')}}">
  <link rel="stylesheet" href="{{url('css/suggestions.css?v=1.2')}}">
  <link rel="stylesheet" href="{{url('css/loader.css?v=1.2')}}">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <style type="text/css">
    .download-files {
      display: block;
      font-size: 12px;
      text-decoration: none;
    }
  </style>
</head>

<body>

<div class="loader" id="loader"></div>

<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="../icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
           <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
                    @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
        </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">
      <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-bar ">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 search-spacing">
              <input type="search" autocomplete = "off" name="search-input" id="search-input" placeholder="Search with Name/Mobile/PAN "><i class="material-icons search-icon">search</i>
            </div>
          </div> <!--Search Bar Ends -->
      </div>

      <div id="user_performance_det">
        <div class = "row investment-summary" id="user-det-row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block "> 
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
              <p class="profile-name">Mr. Sandeep</p>
              <p class="invest-text" id="search-email">sandeep@gmail.com</p>
              <p id ="srch-user-no">8807600463</p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <p class="srch-options"> Rebalancing Required</p>
              <img src= "../icons/rebal.png" class="center-block" id="rebal-icon">
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="port-result-det">Total Investment</p>
              <p class="return-det"><span>Rs.</span><span id="amount_invested"></span></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="port-result-det">Current Value</p>
              <p class="return-det"><span >Rs.</span><span id="current_value"></span></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="port-result-det">Net Profit</p>
              <p class="return-det"><span    >Rs.</span><span id="profit_loss"></span></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="port-result-det">Return (XIRR)</p>
              <p class="return-det"><span id="xirr_return"></span>%</p>
            </div>
          </div>
        </div>
      </div>

      <div class = "row investment-summary" id="kyc_details">
       <div class="heading">
         <ul class="top-links list-inline panel-nav nav-tabs">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
              <a href="#" id="kyc-port" target="_blank"><li class="list-headings" >Portfolio Details</li></a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
              <a href="#" id="kyc-inv" target="_blank"><li class="list-headings ">Investment History</li></a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
              <a href="#" id="kyc-tax" target="_blank"><li class="list-headings ">Tax Saving Statements</li></a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
              <a href="{{ URL('admin/kyc') }}"><li class="list-headings no-b-right active-page padding-small">KYC Details</li></a>
            </div>                
         </ul>
       </div><!-- Heading ends -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12" id="">
            <div class="col-lg-12 col-md-12 col-sm-12 info-div">

              @if(Auth::user()->email == "tmaster@rightfunds.com")
                <p class="det-header">Personal Details<span class="pull-right " id="tab"> <a target="_blank" href="" id="super-id"><i class="material-icons"  id="tab-icon">open_in_new</i></a></p>
              @else
               <p class="det-header">Personal Details<span class="pull-right" id="tab"></p>
              @endif
             
              <form action="#" id="personal-form">
                  <input type="hidden" name="user_id" id="personal-form-id">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 info-div">
                 <div class="col-lg-4 col-md-4 col-sm-4  col-xs-12">
                   <p class="form-helper">Name as per PAN</p>
                   <input class="srch-show-inp" type="text" name="srch-per-name" id="srch-per-name">
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-4  col-xs-12">
                   <p class="form-helper">Date of Birth</p>
                   <input class="srch-show-inp" type="text" name="srch-per-dob" id="srch-per-dob">
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                   <p class="form-helper">Mobile Number</p>
                   <input class="srch-show-inp" type="text" name="srch-per-mob" id="srch-per-mob">
                 </div>
                </div>
                <!-- INfo div ends -->
                <div class="col-lg-12 col-md-12 col-sm-12 info-div">
                 <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Occupation</p>
                   <input class="srch-show-inp" type="text" name="srch-per-occ" id="srch-per-occ">
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-4  col-xs-12">
                   <p class="form-helper">PAN</p>
                   <input class="srch-show-inp" type="text" name="srch-per-pan" id="srch-per-pan">
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-4  col-xs-12">
                   <p class="form-helper">AADHAR</p>
                   <input class="srch-show-inp" type="text" name="srch-per-aadhar" id="srch-per-aadhar">
                 </div>
                </div>
                <!-- INfo div ends -->
                <div class="col-lg-12 col-md-12 col-sm-12 info-div">
                <div class="col-lg-4 col-md-4 col-sm-4 ">
                  <p class="form-helper">Place of Birth</p>
                  <input class="srch-show-inp" type="text" name="srch-per-pob" id="srch-per-pob">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 ">
                  <p class="form-helper">Nationality</p>
                  <input class="srch-show-inp" type="text" name="srch-per-nat" id="srch-per-nat">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 ">
                  <p class="form-helper">Sex</p>
                  <div class="sex-radio-divs">
                    <div class=" male-radio-div padding-lr-zero radio-inline">
                      <input type="radio" name="sex" id="male" value="male">
                      <label for="male"><span class="radio">Male</span></label>
                    </div>
                    <div class=" female-radio-div padding-lr-zero radio-inline">
                      <input type="radio" name="sex" id="female" value="female">
                      <label for="female"><span class="radio">Female</span></label>
                    </div>
                  </div>
                </div>
                </div><!-- INfo div ends -->
                <div class="col-lg-12 col-md-12 col-sm-12 info-div">
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Tax Status</p>
                   <input class="srch-show-inp" type="text" name="srch-per-ts" id="srch-per-ts">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Income Range</p>
                   <input class="srch-show-inp" type="text" name="srch-per-inc" id="srch-per-inc">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Marital Status</p>
                   <div class="sex-radio-divs">
                      <div class=" male-radio-div padding-lr-zero radio-inline">
                        <input type="radio" name="mar-status" id="single" value="single">
                        <label for="single"><span class="radio">Single</span></label>
                      </div>
                      <div class=" female-radio-div married-radio-div padding-lr-zero radio-inline">
                        <input type="radio" name="mar-status" id="married" value="married">
                        <label for="married"><span class="radio">Married</span></label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Mother's Name</p>
                   <input class="srch-show-inp" type="text" name="srch-per-mn" id="srch-per-mn">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Father/Spouse Name</p>
                   <input class="srch-show-inp" type="text" name="srch-per-fsn" id="srch-per-fsn">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Politically Associated</p>
                   <div class="sex-radio-divs">
                    <div class=" male-radio-div padding-lr-zero radio-inline">
                      <input type="radio" name="pol-status" id="pa-yes" value="pol-yes">
                      <label for="pa-yes"><span class="radio">Yes</span></label>
                    </div>
                      <div class=" female-radio-div married-radio-div pol-radio-div padding-lr-zero radio-inline">
                        <input type="radio" name="pol-status" id="pa-no" value="pol-no">
                        <label for="pa-no"><span class="radio">No</span></label>
                      </div>
                  </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 save-spacing">
                    <input type="submit" name="" value="Save" class="btn btn-primary save-button" id="personal-btn">
                  </div>
                </div><!-- INfo div ends -->
              </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 info-div">
              <div class="hr"></div>
                <p class="det-header">Customer Address</p>
                <form id="address-form">
                  <input type="hidden" name="user_id" id="address-form-id">
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">Address</p>
                   <textarea id="srch-per-addr" rows="4" name="srch-per-addr"></textarea>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">City</p>
                   <input class="srch-show-inp" type="text" name="srch-per-city" id="srch-per-city">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 ">
                   <p class="form-helper">PIN</p>
                   <input class="srch-show-inp" type="text" name="srch-per-pin" id="srch-per-pin">
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 save-spacing">
                    <input type="submit" name="" value="Save" class="btn btn-primary save-button">
                  </div>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 info-div">
              <div class="hr"></div>
                <p class="det-header">Nominee</p>
                <form id="nominee-form">
                  <!--Nominee 1 Starts -->
                  <input type="hidden" name="user_id" id="nominee-form-id">
                  <div class=" col-lg-12 col-md-12 col-sm-12 info-div nominee-div">
                   <div class="col-lg-3 col-md-3 col-sm-3 ">
                     <p class="form-helper">Name</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom1-name" id="srch-per-nom1-name">
                   </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 ">
                     <p class="form-helper">Date of Birth</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom1-dob" id="srch-per-nom1-dob">
                   </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 ">
                     <p class="form-helper">Relationship</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom1-rel" id="srch-per-nom1-rel">
                   </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 ">
                     <p class="form-helper">Percentage</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom1-per" id="srch-per-nom1-per">
                   </div>
                  </div>
                  <!-- Nominee 1 ends -->
                  <!--Nominee 2 Starts -->
                  <div class=" col-lg-12 col-md-12 col-sm-12 info-div nominee-div">
                   <div class="col-lg-3 col-md-4 col-sm-3 ">
                     <p class="form-helper">Name</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom2-name" id="srch-per-nom2-name">
                   </div>
                   <div class="col-lg-3 col-md-4 col-sm-3 ">
                     <p class="form-helper">Date of Birth</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom2-dob" id="srch-per-nom2-dob">
                   </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 ">
                     <p class="form-helper">Relationship</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom2-rel" id="srch-per-nom2-rel">
                   </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 ">
                     <p class="form-helper">Percentage</p>
                     <input class="srch-show-inp" type="text" name="srch-per-nom2-per" id="srch-per-nom2-per">
                   </div>
                  </div>
                  <!-- Nominee 2 ends -->
                  <!--Nominee 3 Starts -->
                  <div class=" col-lg-12 col-md-12 col-sm-12 info-div nominee-div">
                  <div class="col-lg-3 col-md-3 col-sm-3 ">
                   <p class="form-helper">Name</p>
                   <input class="srch-show-inp" type="text" name="srch-per-nom3-name" id="srch-per-nom3-name">
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <p class="form-helper">Date of Birth</p>
                    <input class="srch-show-inp" type="text" name="srch-per-nom3-dob" id="srch-per-nom3-dob">
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <p class="form-helper">Relationship</p>
                    <input class="srch-show-inp" type="text" name="srch-per-nom3-rel" id="srch-per-nom3-rel">
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <p class="form-helper">Percentage</p>
                    <input class="srch-show-inp" type="text" name="srch-per-nom3-per" id="srch-per-nom3-per">
                  </div>
                  </div>
                  <!-- Nominee 3 ends -->
                  <div class="col-lg-12 col-md-12 col-sm-12 save-spacing">
                    <input type="submit" name="" value="Save" class="btn btn-primary save-button">
                  </div>
                </form>   
            </div><!-- INfo div ends -->
          </div>
        </div>
        <div class="row bank-det-row">
          <div class=" col-lg-12 col-md-12 col-sm-12 info-div ">
            <div class="hr"></div>
            <p class="det-header">Bank Details</p>
            <form id="bank-form">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <p class="det-sub-header">Primary Bank Account</p>
                <input type="hidden" name="user_id" id="bank-form-id">
                <div class="row bank-details-row">
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">Bank Account No</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank1-accno" id="srch-per-bank1-accno">
                  </div>
                  <div class="col-xs-5 padding-l-zero">
                    <p class="form-helper">IFSC Code</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank1-ifsc" id="srch-per-bank1-ifsc">
                  </div>
                  <!-- <span class="pull-right"><i class="material-icons">autorenew</i></span> -->
                </div> <!-- Bank account NO and IFSC code parent row ends -->
                <div class="row bank-details-row">
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">Bank Name</p>
                    <select class="srch-show-inp srch-show-sm-inp" id="srch-per-bank1-name" name="srch-per-bank1-name">

                     </select>  
                  </div>
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">Branch Name</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-branch1-name" id="srch-per-branch1-name">
                  </div>
                </div> <!-- Bank name and Branch Name parent row ends -->
                <div class="row bank-details-row">
                  <div class="col-xs-12 padding-l-zero">
                    <p class="form-helper">Address</p>
                    <textarea id="bank1-addr" name="bank1-addr" rows="5"></textarea>
                  </div>
                </div> <!-- Address and Pin parent row ends -->
                <div class="row bank-details-row">
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">City</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank1-city" id="srch-per-bank1-city">
                  </div>
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">PIN Code</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank1-pin" id="srch-per-bank1-pin">
                  </div>
                </div> <!-- Address and Pin parent row ends -->       
                <div class="row bank-details-row">
                  <div class="col-xs-12 padding-l-zero">
                    <p class="form-helper">Account type</p>
                    <div class="col-xs-6  checkbox-div"><input type="checkbox" name="savings" id="savings">
                      <label for="savings"><span class="checkbox"><img src="../icons/check_dis.png"></span></label><span class="acc-type-name">Savings</span>
                    </div>
                    <div class="col-xs-6  checkbox-div"><input type="checkbox" name="current" id="current">
                      <label for="current"><span class="checkbox"><img src="../icons/check_dis.png"></span></label><span class="acc-type-name">Current</span>
                    </div>
                  </div>
                </div> <!-- Address and Pin parent row ends -->
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <p class="det-sub-header">Secondary Bank Account</p>
                <div class="row bank-details-row">
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">Bank Account No</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank2-accno" id="srch-per-bank2-accno">
                  </div>
                  <div class="col-xs-5 padding-l-zero">
                    <p class="form-helper">IFSC Code</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank2-ifsc" id="srch-per-bank2-ifsc">
                  </div>
                </div> <!-- Bank account NO and IFSC code parent row ends -->
                <div class="row bank-details-row">
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">Bank Name</p>
                    <select class="srch-show-inp srch-show-sm-inp" id="srch-per-bank2-name" name="srch-per-bank2-name">

                     </select> 
                  </div>
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">Branch Name</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-branch2-name" id="srch-per-branch2-name">
                  </div>
                </div> <!-- Bank name and Branch Name parent row ends -->
                <div class="row bank-details-row">
                  <div class="col-xs-12 padding-l-zero">
                    <p class="form-helper">Address</p>
                    <textarea id="bank2-addr" name="bank2-addr" rows="5"></textarea>
                  </div>
                </div> <!-- Address and Pin parent row ends -->
                <div class="row bank-details-row">
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">City</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank2-city" id="srch-per-bank2-city">
                  </div>
                  <div class="col-xs-6 padding-l-zero">
                    <p class="form-helper">PIN Code</p>
                    <input class="srch-show-inp srch-show-sm-inp" type="text" name="srch-per-bank2-pin" id="srch-per-bank2-pin">
                  </div>
                </div> <!-- Address and Pin parent row ends -->       
                <div class="row bank-details-row">
                  <div class="col-xs-12 padding-l-zero">
                    <p class="form-helper">Account type</p>
                    <div class="col-xs-6  checkbox-div">
                      <input type="checkbox" name="savings2" id="savings2" checked>
                      <label for="savings2"><span class="checkbox"><img src="../icons/check_dis.png"></span></label><span class="acc-type-name">Savings</span>
                    </div>
                    <div class="col-xs-6  checkbox-div">
                      <input type="checkbox" name="current2" id="current2">
                      <label for="current2"><span class="checkbox"><img src="../icons/check_dis.png"></span></label><span class="acc-type-name">Current</span>
                    </div>
                  </div>
                </div> <!-- Address and Pin parent row ends -->                 
              </div>
              <div class = "row bank-det-row">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" id="tax-res-col">
                  <span class="tax-res">Tax Residence other than India</span>
                  <div class="sex-radio-divs" id="tax-res-div">
                    <div class=" male-radio-div padding-lr-zero radio-inline">
                      <input type="radio" name="tr-o-ind" id="tr-yes" value="tr-yes">
                      <label for="tr-yes"><span class="radio">Yes</span></label>
                    </div>
                    <div class=" female-radio-div tax-radio-div pol-radio-div padding-lr-zero radio-inline">
                      <input type="radio" name="tr-o-ind" id="tr-no" value="tr-no">
                      <label for="tr-no"><span class="radio">No</span></label>
                    </div>
                  </div>
                </div><!-- Tax Res col ends -->
              </div>
              <div class = "row bank-det-row">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 info-div">
                  <div class = "col-lg-4 col-md-4 col-sm-4">
                    <p class="form-helper">Bank Name</p>
                      <input class="srch-show-inp" type="text" name="srch-per-bank2-pin" id="srch-per-bank2-pin">
                  </div>
                  <div class = "col-lg-4 col-md-4 col-sm-4">
                    <p class="form-helper">Tax Register</p>
                      <input class="srch-show-inp" type="text" name="srch-per-tax-reg" id="srch-per-tax-reg">
                  </div>
                  <div class = "col-lg-4 col-md-4 col-sm-4">
                    <p class="form-helper">Citizenship</p>
                      <input class="srch-show-inp" type="text" name="srch-per-citizenship" id="srch-per-citizenship">
                  </div>
                </div> 
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 save-spacing">
               <input type="submit" name="" value="Save" class="btn btn-primary save-button">
              </div>
            </form>
          </div><!-- INfo div ends -->
        </div>
        <div class="row bank-det-row">
          <div class=" col-lg-12 col-md-12 col-sm-12 info-div ">
            <div class = "hr"></div>
            <p class="det-header">KYC Documents</p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero" id="srch-img-div">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <p class="form-helper srch-file-helper">PAN</p>
                <img src="../icons/pic_upload.png" class="srch-result-img" id="pan-img">
                <form class = "file-forms" id="form-pan">
                <input type="hidden" name="user_id" id="pan_id" value="">
                <input class="upload-file" type="file" name="pan" id="pan" accept="image/*">
                <label for="pan">
                  <a id="upload-pan" class="upload-files" >Upload</a>
                </label>
                </form>
                <a href="#" id="download-pan" class="download-files" download>Download</a>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <p class="form-helper srch-file-helper">Address Proof </p>
                <img src="../icons/pic_upload.png" class="srch-result-img" id="addr-pr-front">
                <form class = "file-forms" id="form-apf">
                <input type="hidden" name="user_id" id="apf_id" value="">
                <input class="upload-file" type="file" name="apf" id="apf" accept="image/*">
                <label for="apf">
                <a id="upload-apf" class="upload-files">Upload</a>
                </label>
                </form>
                <a href="#" id="download-apf" class="download-files" download>Download</a>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <p class="form-helper srch-file-helper">Video Entire</p>
                <img src="../icons/pic_upload.png" class="srch-result-img" id="ekyc-video">
                <form class = "file-forms" id="form-video">
                  <input type="hidden" name="user_id" id="video_id" value="">
                  <input class="upload-file" type="file" name="video" id="video" accept="video/*">
                  <label for="video">
                    <a id="upload-video" class="upload-files">Upload</a>
                  </label>
                </form>
                <a href="#" id="download-video" class="download-files" download>Download</a>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <p class="form-helper srch-file-helper">Cancelled Cheque/</p>
                <img src="../icons/pic_upload.png" class="srch-result-img" id="cc-img">
                <form class = "file-forms" id="form-cc">
                  <input type="hidden" name="user_id" id="cc_id" value="">
                  <input class="upload-file" type="file" name="cc" id="cc" accept="image/*">
                  <label for="cc">
                    <a id="upload-cc" class="upload-files">Upload</a>
                  </label>
                </form>
                <a href="#" id="download-cc" class="download-files" download>Download</a>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <p class="form-helper srch-file-helper">Mandate </p>
                <img src="../icons/pic_upload.png" class="srch-result-img" id="mandate-img">
                <form class = "file-forms" id="form-mandate">
                  <input type="hidden" name="user_id" id="mandate_id" value="">
                  <input class="upload-file" type="file" name="mandate" id="mandate" accept="image/*">
                  <label for="mandate">
                    <a id="upload-mandate" class="upload-files">Upload</a>
                  </label>
                </form>
                <a href="#" id="download-mandate" class="download-files" download>Download</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row bank-det-row">
          <div class=" col-lg-12 col-md-12 col-sm-12 info-div ">
            <div class = "hr"></div>
            <p class="det-header">Download Documents</p>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="down-doc-div">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="form-helper srch-file-helper">KYC Form</p>
              <img src="../icons/pic_upload.png" class="srch-result-img" id="kyc-form">
              <form class = "file-forms" id="form-ekyc">
                <input type="hidden" name="user_id" id="ekyc_id" value="">
                <input class="upload-file" type="file" name="ekyc" id="ekyc" accept="image/*">
                <label for="ekyc">
                  <a id="upload-ekyc" class="upload-files">Upload</a>
                </label>
              </form>
              <a href="#" id="download-ekyc" class="download-files" download>Download</a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="form-helper srch-file-helper">Acct. Opening Form</p>
              <img src="../icons/pic_upload.png" class="srch-result-img" id="acc-op-form">
              <form class = "file-forms" id="form-aof">
                <input type="hidden" name="user_id" id="aof_id" value="">
                <input class="upload-file" type="file" name="aof" id="aof" accept="image/*">
                <label for="aof">
                  <a id="upload-aof" class="upload-files">Upload</a>
                </label>
              </form>
              <a href="#" id="download-aof" class="download-files" download>Download</a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <p class="form-helper srch-file-helper">Filled Mandate</p>
              <img src="../icons/pic_upload.png" class="srch-result-img" id="mandate-form">
              <form class = "file-forms" id="form-mnt">
                <input type="hidden" name="user_id" id="mnt_id" value="">
                <input class="upload-file" type="file" name="mnt" id="mnt" accept="image/*">
                <label for="mnt">
                  <a id="upload-mnt" class="upload-files">Upload</a>
                </label>
              </form>
              <a href="#" id="download-mnt" class="download-files" download>Download</a>
            </div>
          </div>
        </div>
      </div>
    </div><!--- Investment summary row ends-->
</div> <!--Content Ends -->

<div id="file-upload-modal" class="modal fade">
  <div class="modal-dialog" id="uploadModal">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <p class="text-center blue modal-header-text">Upload Confirmation</p>
      </div>
      <div class="modal-body" id="fileUploadModal">
        <p class="text-center info-text">Please click confirm button to upload the file?</p>
      </div>
      <button type="button" class="btn btn-primary center-block" id="confirm-btn">Confirm</button>
    </div>
  </div>
</div>

<div id="response-modal" class="modal fade">
  <div class="modal-dialog" id="save-div">
    <div class="modal-content">
<!--       <div class="modal-header">
        <button class="close" type="button" data-dismiss="modal">&times;</button>
        <p class="text-center info-text"></p>
      </div> -->
      <div class="modal-body">
        <p class="text-center info-text" id="save-p">Your data has been saved Successfully.</p>
      </div>
      <button class="btn btn-primary center-block" id="save-btn" data-dismiss="modal">Okey</button>
    </div>
  </div>
</div>

<div id="video-modal" class="modal fade">
  <div class="modal-dialog" id="videoModal">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <p class="text-center blue modal-header-text">ekyc Video</p>
      </div>
      <div class="modal-body" id="videoBodyModel">
        <video controls="controls" autoplay="" name="media" style="height: 284px;">
          <source src="#" type="video/mp4" id="ekyc_video">
        </video>
      </div>
      <button type="button" class="btn btn-primary center-block" id="video-btn" style="margin-bottom: 10px;background-color: #0091ea;border-color: #0091ea;">Done</button>
    </div>
  </div>
</div>


  
  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>
  <script src="{{URL('js/jquery.donut.js')}}"></script>
  <!--<script src="{{URL('js/index.js')}}"></script>-->
  <script src="{{URL('js/checkbox.js?v=1.2')}}"></script>
  <script src="{{URL('js/smart_search.js?v=1.2')}}"></script>
  <script src="{{URL('js/admin-kyc.js?v=1.4')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
  <script src="{{url('js/loader.js?v=1.2')}}"></script>




</body>
</html>
