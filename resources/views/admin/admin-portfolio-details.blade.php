<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{URL('css/admin/bootstrap.min.css?v=1.1')}}">

    <link rel="stylesheet" href="{{URL('css/admin/admin-portfolio-details.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/admin-inv-history-responsive.css?v=1.1')}}">

    <link href="{{URL('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css?v=1.1">
    <link rel="stylesheet" href="{{URL('css/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/suggestions.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/admin/extras.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('css/footer.css?v=1.1')}}">

    <link rel="stylesheet" href="{{URL('css/loader.css?v=1.1')}}">
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="{{URL('icons/logo.png')}}"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
                    @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
        </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <!-- <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>


              </div> -->

<!--               <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                  <p class="profile-name">{{\Auth::user()->name}}</p>
                  <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                </div>
              </div> -->
               <!--<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">Mr. Sandeep!</p>
                      <p class="invest-text" id="admin-email">sandeep@gmail.com</p>
                    </div>

              </div>-->

              

              

          <!-- </div> Profile Bar Ends  -->
    <!-- </div> -->


     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-bar ">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-r-zero">
              <input type="text" name="search-input" id="search-input" placeholder="Search with Name/Mobile/PAN ">
            </div>

<!--             <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 trans-filter padding-lr-zero">

              <span class = "last-t">Last Transaction</span>
              <select class = "filter-search-date" id="transact_range">
                <option value="1">1 Day</option>
                <option value="2"><1 week</option>
                <option value="3"><1 month</option>
                <option value="4"><6 months</option>
              </select>
            </div> -->

            <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-zero srch-btn-div">

                <!--<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-lr-zero filter-btn-div">
                  <button type = "button" class = "search-filter-btn btn btn-primary">Newly Enrolled</button>
                </div>

                <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12  padding-lr-zero filter-btn-div">
                  <button type = "button" class = "search-filter-btn btn btn-primary">Rebalance Req.</button>
                </div>-->

            </div>
                        

          </div> <!--Search Bar Ends -->
     </div>







            <div id="user_performance_det">
              <div class = "row investment-summary" id="user-det-row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">{{$userName}}</p>
                      <p class="invest-text" id="search-email">{{$userEmail}}</p>
                      <p id ="srch-user-no">{{$userMobile}}</p>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    
                  </div>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Total Investment</p>
                    <p class="return-det">Rs.<span id="amount_invested">{{$portfolio_total}}</span></p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Current Value</p>
                    <p class="return-det">Rs.<span id="current_value">{{$current_value_total}}</span></p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Net Profit</p>
                    <p class="return-det">Rs.<span id="profit_loss">{{$nr_total}}</span></p>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <p class="port-result-det">Return (XIRR)</p>
                    <p class="return-det"><span id="xirr_return">{{$investment_status['xirr']}}</span>%</p>
                  </div>
                </div>

                  

            </div>
        <div class = "row investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/portfolio_details/{{$id}}" id="inv_port" target="_blank"><li class="list-headings active-page">Portfolio Details</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/inv_history/{{$id}}" id="inv_his" target="_blank"><li class="list-headings ">Investment History</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="/admin/tax_saving/{{$id}}" id="inv_ts" target="_blank"><li class="list-headings ">Tax Saving Statements</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{ URL('admin/kyc') }}" target="_blank"><li class="list-headings no-b-right padding-small">KYC Details</li></a>
                    </div>                 
                  
               </ul>
             
             </div><!-- Heading ends -->

            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" id="export-row">

                  <!--<div class="col-lg-7 col-md-7 col-sm-7">
                      <p class="hereshow">Show history for<span class="styled-select slate">
                            <select>
                              <option>Last 6 months</option>
                              <option>Last 1 year</option>
                              <option>Last 2 years</option>
                          
                            </select>
                          </span></p>
                   </div>-->

                  <div class="col-lg-3 col-md-3 col-sm-3">
                    <button type="button" id="wd-btn" class="btn btn-primary">ADD WITHDRAW</button>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3">
                    <button type="button" id="inv-btn" class="btn btn-primary">ADD INVESTMENT</button>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <form id="export_data" action="/admin/get_ind_user_portfolio_performance" method="post" target="_blank">
                      {{csrf_field()}}
                      <input type="hidden" name="user_id" id="user_id" value="{{$id}}" />
                      <!-- <input type="hidden" name="transaction_range" id="transaction_range"/> -->
                      <input type="hidden" name="response_type" id="response_type" value="docmument" />
                      <input type="submit" id="export-btn" class="btn btn-primary" value="EXPORT" />
                    </form>
                  </div>

                </div><!-- export-row ends -->
            </div><!-- Row ends -->


          <!--<form id = "units-held-form"> -->
              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">PORTFOLIO DETAILS</p>
                    <div class="table-wrapper" id="equity-table-wrapper">
                     <table class="table" id="equity-table">
                    
                        <thead>
                          <tr>
                            <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                            <th>Class</th>
                            <th>Folio Number</th>
                            <th>DOI/DOW</th>
                            <th>Amount invested(Rs.)</th>
                            <th>NAV</th>
                            <th>Units Held</th>
                            <th>Current Nav(Rs)</th>
                            <th>Current Value(Rs)</th>
                            <th>Net Returns(Rs)</th>
                            <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                            <th>Inv/WD</th>
                            <th>Edit</th>
                            
                          </tr>
                        </thead> 
                        <button type="button" class="btn btn-primary" id="equity-scroll">
                          <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </button>
                        <tbody id="portfolio-summary-details">
                          @foreach($pass_portfolio as $data)
                            <tr id="{{$data['id']}}">
                              <td>{{$data['fund_name']}}</td>
                              <td>{{$data['type']}}</td>
                              <td>{{$data['folio_number']}}</td>
                              <td>{{$data['date']}}</td>
                              <td>{{$data['amount']}}</td>
                              <td>{{$data['inv_nav']}}</td>
                              <td><div class="units-held-p"><p>{{$data['units_held']}}</p></div></td>
                              <td>{{$data['current_nav']}}</td>
                              <td>{{$data['current_value']}}</td>
                              <td>{{$data['net_returns']}}</td>
                              <td class="ltc-amount">{{$data['ltcg']}}</td>
                              <td>{{$data['transaction_type']}}</td>
                              <td><a data-id = "{{$data['id']}}" class="edit-portfolio"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                            </tr>
                          @endforeach
                          <tr>
                            <td class="total bt-green ">TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green ">{{$portfolio_total}}</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green ">{{$current_value_total}}</td>
                            <td class="total bt-green ">{{$nr_total}}</td>
                            <td class="total  bt-green ltc-amount">{{$ltcg_total}}</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>



              <!--<div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">DEBT FUNDS</p>
                    <div class="table-wrapper" id="debt-table-wrapper">
                     <table class="table" id="equity-table">
                        <thead>
                          <tr>
                            <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                            <th>Folio Number</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Units Held</th>
                            <th>Current Nav(Rs)</th>
                            <th>Current Value(Rs)</th>
                            <th>Net Returns(Rs)</th>
                            <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="debt-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                              </button>
                        <tbody>
                          <tr>
                            <td>Kotak Mahindra mutual Funds  Mahindra mutual Funds  </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                             <td class="ltc-amount">2000</td>

                            
                          </tr>
                          <tr>
                            <td>Kotak Mahindra mutual Funds </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                             <td class="ltc-amount">2000</td>    
                          </tr>
                          <tr>
                            <td class="total bt-green ">TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                             <td class="total bt-green ">45000</td>
                            <td class="total bt-green ">57000</td>
                            <td class="total bt-green ">57000</td>
                             <td class="total bt-green ">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>    
                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>-->



<!--            <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">HYBRID FUNDS</p>
                    <div class="table-wrapper" id="hybrid-table-wrapper">
                     <table class="table" id="equity-table">
                        <thead>
                          <tr>
                            <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                            <th>Folio Number</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Units Held</th>
                            <th>Current Nav(Rs)</th>
                            <th>Current Value(Rs)</th>
                            <th>Net Returns(Rs)</th>
                            <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                            <th>Absolute Returns(Rs)</th>
                            <th>Annualised Returns(Rs)</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                        </button>
                        <tbody>
                          <tr>
                            <td>Kotak Mahindra mutual Funds  Mahindra mutual Funds  </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                            <td class="ltc-amount">2000</td>
                            <td>800</td>
                             <td>800</td>
                            
                          </tr>
                          <tr>
                            <td>Kotak Mahindra mutual Funds </td>
                            <td>A178B4</td>
                            <td>15000</td>
                             <td><div class="units-held-p"><p>1163.5</p></div></td>
                            <td>45689</td>
                            <td>17000</td>
                             <td>17000</td>
                            <td class="ltc-amount">2000</td>
                            <td>800</td>
                             <td>800</td>      
                          </tr>


        </form> 
                         

                          <tr>
                             <td class="total bt-green ">TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                             <td class="total bt-green ">45000</td>
                            <td class="total bt-green ">57000</td>
                            <td class="total bt-green ">57000</td>
                             <td class="total bt-green ">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>
                            <td class="total bt-green ">800</td>
                             <td class="total bt-green ">800</td>   

                          </tr>

                          <tr>
                            <td class="empty"></td>
                            <td class="empty"></td>
                            <td class="empty"></td>
                             <td class="empty"></td>
                            <td class="empty"></td>
                            <td class="empty"></td>
                             <td class="empty"></td>
                            <td class="empty"></td>
                            <td class="empty"></td>
                             <td class="empty"></td>  
                             
                          </tr>


                           <tr>
                             <td class="total bt-green ">GRAND TOTAL</td>
                            <td class="total bt-green "></td>
                            <td class="total bt-green "></td>
                             <td class="total bt-green ">45000</td>
                            <td class="total bt-green ">57000</td>
                            <td class="total bt-green ">57000</td>
                             <td class="total bt-green ">12000</td>
                            <td class="total  bt-green ltc-amount">800</td>
                            <td class="total bt-green ">800</td>
                             <td class="total bt-green ">800</td>   

                          </tr>


                        </tbody>

                      </table>
                    </div>-->
              </div>





<!--
              <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
              <p class="funds-name">HYBRID FUNDS</p>
                    <p class="subnote">No amount was invested between</p>
                    <p class="subnote" id="contact-sub">1 Jan 2016 to 1 April 2016 in this category</p>
              </div>-->

              

        </div><!--- Investment summary row ends-->



         


          
          





                    <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about-us')}}">About us</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                    <li><a href="{{URL('/contact-us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="{{URL('/icons/twitter.png')}}"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="{{URL('/icons/facebook.png')}}"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="{{URL('/icons/linked_in.png')}}"></a>
                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
      

</div> <!--Content Ends -->

</div>


<div id="editPortfolio" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Portfolio</h4>
      </div>
      <div class="modal-body">
        
          <div class="row">
          <form id="update-portfolio-form" name="update-portfolio-form" action="#" method="post">
            <input type="hidden" name="p_id" id="p_id" value="">
          <div class="col-lg-12">
            <div class="col-lg-6">
              <p class="form-helper">Fund Name</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-fund" id="port-edit-fund" required>
              </div>
            </div>
            <div class="col-lg-6">
              <p class="form-helper">Folio Number</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-folio" id="port-edit-folio" required>
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="col-lg-6">
              <p class="form-helper">Transaction type</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-pt" id="port-edit-pt" required>
              </div>
            </div>
            <div class="col-lg-6">
              <p class="form-helper">DOI/DOW</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-date" id="port-edit-date" required>
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="col-lg-6">
              <p class="form-helper">Amount Invested</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-ai" id="port-edit-ai" required>
              </div>
            </div>
            <div class="col-lg-6">
              <p class="form-helper">NAV</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-nav" id="port-edit-nav" required>
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="col-lg-6">
              <p class="form-helper">Units Held</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-uh" id="port-edit-uh" required>
              </div>
            </div>
            <div class="col-lg-6">
              <p class="form-helper">Current Nav</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-c-nav" id="port-edit-c-nav" required>
              </div>
            </div>
          </div>


          <div class="col-lg-12">
            <div class="col-lg-6">
              <p class="form-helper">Current Value</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-cv" id="port-edit-cv" required>
              </div>
            </div>
            <div class="col-lg-6">
              <p class="form-helper">Net Returns</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-nr" id="port-edit-nr" required>
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="col-lg-6">
              <p class="form-helper">Eligible as LTCG</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-ltcg" id="port-edit-ltcg" required>
              </div>
            </div>
            <div class="col-lg-6">
              <p class="form-helper">Portfolio Type</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="port-edit-ptype" id="port-edit-ptype" required>
              </div>
            </div>
          </div>

          <input type="submit" class="btn btn-primary center-block" id="update-portfolio" name="UPDATE" value="UPDATE">

         </form>
        </div>
       
      </div>
      
    </div>

  </div>
</div>


<div id="wdModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Manual withdraw</h4>
      </div>
      <div class="modal-body">

        <form id="manual-withdraw">


          <input type="hidden" name="user_id" id="user_id" value="{$id}"/>
          <div class="col-xs-12">
            <p class="form-helper">Scheme</p>
            <div class="form-group">
              <select name="withdraw_scheme_code" id="withdraw_scheme_code" class="signup-f-input">
                <option value=''>Select</option>
              </select>
            </div>
          </div>

          <div class="col-xs-12">
            <p class="form-helper">Date</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="withdraw_date" id="withdraw_date" required>
              </div>
          </div>

          <div class="col-xs-12">
            <p class="form-helper">Amount</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="withdraw_amount" id="withdraw_amount" required>
              </div>
          </div>

          <div class="col-xs-12">
            <p class="form-helper">NAV</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="withdraw_nav" id="withdraw_nav" required>
              </div>
          </div>
          <input type="submit" name="submit-wd" id="wd-submit" class="center-block btn btn-primary" value="ADD">


        </form>
      </div>
    </div>
  </div>
</div>




<div id="invModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Manual Investment</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          
<form id="manual-investment" class="col-xs-12 col-md-12 col-sm-12" name="manual-investment">

              <div class = "col-lg-12 col-md-12 col-sm-12">
            <input type="hidden" name="user_id" id="inv_user_id" value="{{$id}}"/>
          <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <p class="form-helper">Investment Amount</p>
            <div class="form-group">
              <input class="signup-f-input" type="text" name="inv_amount" id="inv_amount" required>
            </div>
          </div>

          <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <p class="form-helper">Investment Type</p>
              <div class="form-group">
                <select class="add-inv-select" name="investment-type" id="inv-type">
                  <option value="onetime">Onetime</option>
                  <option value="monthly">SIP</option>
                </select>
              </div>
          </div>

          <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <p class="form-helper">Portfolio Type</p>
              <div class="form-group">
                <select class="add-inv-select" name="portfolio-type" id="portfolio-type">
                  <option value="Conservative">Conservative</option>
                  <option value="Moderate">Moderate</option>
                  <option value="Aggressive">Aggressive</option>
                  <option value="Tax Saver">Tax Saver</option>
                </select>
              </div>
          </div>

          <div class="col-xs-3">
            <p class="form-helper">Investment Date</p>
              <div class="form-group">
                <input class="signup-f-input" type="text" name="inv-date" id="inv-date" required>
              </div>
          </div>

          <div class="col-xs-3">
            <p class="form-helper">Payment type</p>
              <div class="form-group">
                <select class="add-inv-select" name="payment-type" id="payment-type">
                  <option value="1">NEFT</option>
                  <option value="2">Mandate</option>
                </select>
              </div>
          </div>

          <div class = "col-lg-12 col-md-12 col-sm-12" id="inv-portfolio-div">
            <a href="#" class="btn btn-primary" id="add-portfolio">ADD PORTFOLIO</a>


            <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero portfolio-holder">
              
            </div>

          </div>
          

          <div class = "col-lg-12 col-md-12 col-sm-12">
            <input type="submit" name="inv-submit" id="inv-submit" class="center-block btn btn-primary" />
          </div>

          </div>
        </form>
          
        </div>
      </div>
    </div>
  </div>
</div>


<div id="showInfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Update Info</h4>
      </div>
      <div class="modal-body">

        <p id="update-info">Succesfully updated the portfolio</p>
        <button type="button" class="btn btn-primary center-block" data-dismiss = "modal">Done</button>
      </div>
    </div>
  </div>
</div>

  <script src="{{URL('js/jquery.min.js')}}"></script>
  <script src="{{URL('js/bootstrap.min.js')}}"></script>
  <script src="{{URL('js/jquery.donut.js')}}"></script>
  <script src="{{URL('js/checkbox.js?v=1.1')}}"></script>
  <script src="{{URL('js/scroller.js?v=1.1')}}"></script>
  <script src="{{URL('js/admin-portfolio.js?v=1.3')}}"></script>
  <script src="{{URL('js/jquery-ui.js?v=1.1')}}"></script>
  <script src="{{URL('js/smart_search.js?v=1.1')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
    <script src="{{url('js/loader.js?v=1.1')}}"></script>

</body>
</html>
