<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL('/css/admin/bootstrap.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-nach.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-nach-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/footer.css?v=1.1')}}">
    <link href="{{URL('/css/font-awesome.min.css" rel="stylesheet')}}" type="text/css">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css?v=1.1')}}">
  <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="{{URL('../icons/logo.png')}}"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" id="logout-nav" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li>
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
          <li class="menu-text menu-text-active" ><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block ">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">Admin Dashboard</p>
                      <p class="invest-text">Get your latest account statement.</p>
                    </div>

              </div>

               <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 center-block ">
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <img  class = "img profile-image" src="../icons/profile_icon.jpg">
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 user-content">
                      <p class="profile-name">{{\Auth::user()->name}}</p>
                      <p class="invest-text" id="admin-email">{{\Auth::user()->email}}</p>
                    </div>

              </div>

              

              

          </div> <!--Profile Bar Ends -->
    </div>


          



            <div class = "row investment-summary">

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-lg-6 col-md-6 col-sm-6" id="top-div">
                    <span class="span-show">Filter By</span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="nach-btn" data-toggle="dropdown">All
                       <i class="fa fa-chevron-down" aria-hidden="true"></i></button>
                        <ul class="dropdown-menu">
                          <li><a href="{{URL('/admin/nach/')}}">ALL</a></li>
                          <li><a href="{{URL('/admin/nach/kyc')}}">KYC COMPLETED</a></li>
                          <li><a href="{{URL('/admin/nach/mandate_sent')}}">MANDATE SENT</a></li>
                          <li><a href="{{URL('/admin/nach/mandate_received')}}">MANDATE RECEIVED</a></li>
                          <li><a href="{{URL('/admin/nach/tpsl')}}">SEND TO TPSL</a></li>
                          <li><a href="{{URL('/admin/nach/cleared')}}">CLEARED</a></li>
                          <li><a href="{{URL('/admin/nach/disapproved')}}">DISAPPROVED</a></li>
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="hr"></div>

              <div class = "row">
                <form id="nach-update" name="nach-update" method="post" action="{{URL('admin/save_nach')}}">
                {{ csrf_field() }}
                 <div class="col-lg-12 col-md-12 col-sm-12">
                   <div id="nach-table">
                     <table>
                      <thead>
                         <tr class="heading">
                         <th><div class="col-xs-12">Sl.No</div></th>
                         <th><div class="col-xs-12">CUSTOMER NAME</div></th>
                         <th><div class="col-xs-12">PAN NUMBER</div></th>
                         <th><div class="col-xs-12">PAN VER</div></th>
                         <th><div class="col-xs-12">KYC COMPLETED</div></th>
                         <th><div class="col-xs-12">MANDATE SENT</div></th>
                         <th><div class="col-xs-12">MANDATE RECEIVED</div></th>
                         <!--<th><div class="col-xs-12">SEND TO TPSL</div></th>-->
                         <th><div class="col-xs-12">CLEARED</div></th>
                         <th><div class="col-xs-12">DISAPPROVED</div></th>
                       </tr>
                      </thead><button type="button" class="btn btn-primary" id="nach-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i></button>

                       <tbody id="nach_body">
                          @foreach($pending_nach_details as $pending_nach_detail)
                            <tr class="table-row">
                              <td><div>{{$pending_nach_detail['si_no']}}<input type="hidden" value="{{$pending_nach_detail['user_id']}}" id="nachdetails[{{$pending_nach_detail['user_id']}}][user_id]"></div></td>
                              <td>{{$pending_nach_detail['name']}}</td>
                              <td>{{$pending_nach_detail['user_pan']}}</td>

                              @if($pending_nach_detail['pan_date'] =='')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][pan_completed]"  class="pan-completed" value="{{$pending_nach_detail['pan_date']}}"></td>
                              @else
                                <td><input type="text" class="pan-completed" value="{{$pending_nach_detail['pan_date']}}" disabled></td>
                              @endif

                              @if($pending_nach_detail['kyc_date'] =='')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][kyc_completed]"  class="kyc-completed" value="{{$pending_nach_detail['kyc_date']}}"></td>
                              @else
                                <td><input type="text" class="kyc-completed" value="{{$pending_nach_detail['kyc_date']}}" disabled></td>
                              @endif

                              @if($pending_nach_detail['mandate_sent_date'] == '')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][mandate_sent]" class="mandate-sent" value="{{$pending_nach_detail['mandate_sent_date']}}"></td>
                              @else
                                <td><input type="text" class="mandate-sent" value="{{$pending_nach_detail['mandate_sent_date']}}" disabled></td>
                              @endif

                              @if($pending_nach_detail['mandate_returned_date'] == '')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][mandate_rec]" class="mandate-rec" value="{{$pending_nach_detail['mandate_returned_date']}}"></td>
                              @else
                                <td><input type="text" class="mandate-rec" value="{{$pending_nach_detail['mandate_returned_date']}}" disabled></td>
                              @endif

                              <!--@if($pending_nach_detail['tpsl_date'] == '')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][tpsl_send]" class="tpsl-send" value="{{$pending_nach_detail['tpsl_date']}}"></td>
                              @else
                                <td><input type="text" class="tpsl-send" value="{{$pending_nach_detail['tpsl_date']}}" disabled></td>
                              @endif-->

                              @if($pending_nach_detail['cleared_date'] == '')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][cleared]" class="cleared" value="{{$pending_nach_detail['cleared_date']}}"></td>
                              @else
                                <td><input type="text" class="cleared" value="{{$pending_nach_detail['cleared_date']}}" disabled></td>
                              @endif   

                              @if($pending_nach_detail['dissaproved_date'] == '')
                                <td><input type="text" name="nachdetails[{{$pending_nach_detail['user_id']}}][disapp]" class="disapp" value="{{$pending_nach_detail['dissaproved_date']}}"></td>
                              @else
                                <td><input type="text" class="disapp" value="{{$pending_nach_detail['dissaproved_date']}}" disabled></td>
                              @endif

                            </tr>
                          @endforeach
                       </tbody>
                     </table>                     
                   </div>
                   <input type="submit" class="btn btn-primary center-block" id="update-nach" value="Update">
                 </div>
                </form>
              </div>     

          </div>



          


          
          





          <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about-us')}}">About us</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                    <li><a href="{{URL('/contact-us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src=" {{url('icons/twitter.png')}}"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src=" {{url('icons/facebook.png')}}"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src=" {{url('icons/linked_in.png')}}"></a>
                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
      

</div> <!--Content Ends -->

</div>



          <!-- Modal -->
<div id="addGoalModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="setGoalModalBody">

        <p class="text-center" id="set-g-track">Set goals to stay on track!</p>
         <p class="text-center" id="inv-worth">I would like to have investments worth</p>

        <div class="input-group input-group-md goal-amount-block center-block ">
                  <span class="input-group-addon " id="inv-rs">Rs</span>
                  <input type="number" class="form-control" placeholder="Your Amount" aria-describedby="basic-addon1" id="inv-amount" required="">
        </div>
        <button type="button" class="btn btn-primary center-block" id="set-g-btn">SET GOAL</button>

       

        

       


            
          

      </div>
    </div>
  </div>
</div>

<div id="nachSaveModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Great !</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">
            <button type="button" data-dismiss = "modal" class="btn btn-primary text-center" id="notify-done">DONE</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <script src="{{URL('/js/jquery.min.js')}}"></script>
  <script src="{{URL('/js/bootstrap.min.js')}}"></script>
  <script src="{{URL('/js/nach_details.js?v=1.1')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{URL('/js/scroller.js?v=1.1')}}"></script>
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="{{url('js/loader.js?v=1.1')}}"></script>

  <script>
    $('.pan-completed,.kyc-completed,.mandate-sent,.mandate-rec,.tpsl-send, .cleared, .disapp').datepicker({ dateFormat: "dd-mm-yy",changeMonth: true, changeYear: true, yearRange: '2016:2050' });

    $(document).ready(function(){


    });
  </script>

</body>
</html>
