<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{url('/css/admin/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{URL('/css/admin/admin-pending-orders.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/admin-po-responsive.css?v=1.1')}}">

    <link href="{{URL('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{URL('/css/admin/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/admin/datepicker.css?v=1.1')}}">

    <link rel="stylesheet" type="text/css" href="{{URL('/css/admin/nach1.css?v=1.1')}}">
    
    <link rel="stylesheet" href="{{URL('/css/admin/extras.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{URL('/css/loader.css?v=1.1')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>

  <div class="loader" id="loader"></div>


<nav class="navbar" id="top-nav">

    <div class="">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="/"><img src="/icons/logo.png"></a>
      </div>


    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';" id="logout-nav">LOG OUT</button>
      </ul>
    
      <div class="row" >
        <ul class="nav navbar-nav" id="second-nav">
          <li class="menu-text"><a href="{{URL('/admin/overview')}}">Overview</a></li>
          <li class="menu-text"><a href="{{URL('/admin/kyc')}}">Customer Support</a></li>
          <!--<li class="menu-text"><a href="{{URL('#')}}">AMC Performance</a></li>-->
          <!-- <li class="menu-text"><a href="{{URL('#')}}">Commission Status</a></li> -->
          <li class="menu-text"><a href="{{URL('/admin/sip_orders')}}">SIP Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/pending_orders')}}">Pending Orders</a></li>
          <li class="menu-text"><a href="{{URL('/admin/order_history')}}">Order History</a></li>
          @if(\Auth::id() != 145)
          <li class="menu-text"><a href="{{URL('/admin/manage_schemes')}}">Schemes</a></li>
          @endif
          <li class="menu-text menu-text-active"><a href="{{URL('/admin/nach')}}">NACH Status</a></li>
      </ul>
      </div>
    </div>
    </div>
  
</nav>


  
<div class = "container">

    <div class = "row investment-summary">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero ">

              <div class="col-lg-12 col-md-12 col-sm-12  border-bot header-cont">
                <div class=" col-md-4 col-sm-4 col-lg-4 border-right padding-lr-zero">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="material-icons">search</i></span>
                      <input type="text" class="form-control" placeholder="Search" id="search_nach_user">
                  </div>
                </div> 

                <div class=" col-md-8 col-sm-8 col-lg-8">
                  <?php if (\Auth::id() == 145): ?>
                    <div class="pull-right">
                      <span class="filter-span">Filter :</span>
                      <select class="filter-select">
                       <option value="0">All</option>
                       <option value="1">Prospect</option>
                       <option value="2">Follow Up</option>
                       <option value="3">Not Intrested</option>
                       <option value="4">No Response</option>
                      </select>
                    </div>
                  <?php endif ?>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="col-lg-4 col-md-4 col-sm-4 border-right padding-lr-zero" id="nach_user_sidebar">

                  <?php if (\Auth::id() == 145): ?>
                  @foreach($nach_details as $nach)
                      <div class="col-lg-12 col-md-12 col-sm-12 border-bot nach_user_container" data-id="{{$nach['user_id']}}">
                        <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                          <p class="nach_user_name">{{$nach['user_name']}}</p>
                          <p class="nach_user_pan">{{$nach['user_pan']}}</p>
                          <p class="nach_user_pan">{{$nach['user_mobile']}}</p>
                          <p class="nach_user_pan">{{$nach['user_email']}}</p>
                        </div>
                      </div> <!--NACH user container ends -->
                  @endforeach
                    <?php else: ?>
                  @foreach($nach_details as $nach)
                      <div class="col-lg-12 col-md-12 col-sm-12 border-bot nach_user_container" data-id="{{$nach['user_id']}}">
                        <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                        <?php if ($nach['priority'] == 1): ?>
                          <i class="material-icons pull-right priority-star user-imp" data-id="{{$nach['user_id']}}">star</i>
                          <?php else: ?>
                          <i class="material-icons pull-right priority-star" data-id="{{$nach['user_id']}}">star</i>
                        <?php endif ?>
                          <p class="nach_user_name">{{$nach['user_name']}}</p>
                          <p class="nach_user_pan">{{$nach['user_pan']}}</p>
                          <p class="nach_user_pan">{{$nach['user_mobile']}}</p>
                          <p class="nach_user_pan">{{$nach['user_email']}}</p>
                        </div>
                        <!-- <div class="col-lg-5 col-md-5 col-sm-5 padding-lr-zero">
                          <p class="badge nach-info-badge">MANDATE</p>
                        </div> -->
                      </div> <!--NACH user container ends -->
                  @endforeach
                  <?php endif ?>
                </div><!-- Sidebar ends-->


                  <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero" id="nach_content_bar" data-user = "">
                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <p id="active_user_name" class="user-nach-details">NAME</p>
                        <p id="active_user_pan" class="user-nach-details">PAN</p>
                        <p id="active_user_mobile" class="user-nach-details"></p>
                        <p id="active_user_email" class="user-nach-details"></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero" id="status-div">
                        <select class="pull-right" id="status-change">
                         <option value="0" disabled selected>Status</option>
                         <option value="1">Prospect</option>
                         <option value="2">Follow Up</option>
                         <option value="3">Not Intrested</option>
                         <option value="4">No Response</option>
                        </select>
                      </div>
                    </div>

                  <form>
                    <div class="col-lg-12 col-md-12 col-sm-12 catagory" >
                       <i class="material-icons catagory-checkicon active-checkicon" id="mandate-id">check_circle</i> <h4 class="catagory-name">Mandate</h4>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">Mandate Soft Copy </span><i class="material-icons open_tab" id="mandate_soft">open_in_new</i>
                        <p class="other_active_sub_details">Status Update on <span id="mandatesoft_date">3-8-2017</span></p>
                        <p id="mandate-update"></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="mandatesoft_status">
                         <option value="1">Approved</option>
                         <option value="2">Rejected</option>
                       </select>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">Mandate Hard Copy </span><i class="material-icons open_tab" id="mandate_soft">open_in_new</i>
                        <p class="other_active_sub_details">Status Update on <span id="mandatehard_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="mandatehard_status">
                         <option value="1">Received</option>
                         <option value="2">Not Received</option>
                       </select>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">Mandate Status(BSE)</span>
                        <p class="other_active_sub_details">Status Update on <span id="bsemandate_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="bsemandate_status">
                         <option value="1">Approved</option>
                         <option value="2">Pending</option>
                         <option value="3">Rejected</option>
                       </select>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 catagory" >
                       <i class="material-icons catagory-checkicon active-checkicon" id="bse-id">check_circle</i> <h4 class="catagory-name">BSE</h4>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">BSE Client</span>
                        <p class="other_active_sub_details">Status Update on <span id="bseclient_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="bseclient_status">
                         <option value="1">Added</option>
                         <option value="2">Not Added</option>
                       </select>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">AOF uploaded in BSE</span>
                        <p class="other_active_sub_details">Status Update on <span id="aof_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="aof_uploaded">
                         <option value="1">Uploaded</option>
                         <option value="2">Not Uploaded</option>
                       </select>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">AOF Status BSE</span>
                        <p class="other_active_sub_details">Status Update on <span id="bseaof_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="bseaof_status">
                         <option value="1">Approved</option>
                         <option value="2">Rejected</option>
                       </select>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 catagory" >
                       <i class="material-icons catagory-checkicon" id="cams-id">check_circle</i> <h4 class="catagory-name">CAMS</h4>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">PAN Status</span>
                        <p class="other_active_sub_details">Status Update on <span id="pan_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="pan_status">
                         <option value="1">Verified</option>
                         <option value="2">Not Verified</option>
                       </select>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">EKYC video</span> <i class="material-icons open_tab" id="play_ekyc">open_in_new</i>
                        <p class="other_active_sub_details">Status Update on <span id="kycvideo_date">3-8-2017</span></p>
                        <p id="ekyc-video-update"></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="kycvideo_status">
                         <option value="1">Approved</option>
                         <option value="2">Rejected</option>
                       </select>
                      </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">KYC Form</span>
                        <p class="other_active_sub_details">Status Update on <span id="kyc_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="kyc_status">
                         <option value="1">Received</option>
                         <option value="2">Not Received</option>
                       </select>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">FATCA Status</span>
                        <p class="other_active_sub_details">Status Update on <span id="fatca_date">3-8-2017</span></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="fatca_status">
                         <option value="1">Added</option>
                         <option value="2">Not Added</option>
                       </select>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 catagory" >
                       <i class="material-icons catagory-checkicon" id="bank-id">check_circle</i> <h4 class="catagory-name">Bank Documents</h4>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <div class="col-lg-8 col-md-8 col-sm-8 padding-lr-zero">
                        <span class="other_active_details">Cancelled Cheque / Passbook</span><i class="material-icons open_tab" id="show_cheque">open_in_new</i>
                        <p class="other_active_sub_details">Status Update on <span id="cc_date">3-8-2017</span></p>
                        <p id="cc-update"></p>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 padding-lr-zero">
                       <select class = "nach-select" id="cc_status">
                         <option value="1">Approved</option>
                         <option value="2">Rejected</option>
                       </select>
                      </div>
                    </div>


                     
                    <div class="col-lg-12 col-md-12 col-sm-12 border-bot" id="active-user-up-cont">
                      <p class="other_active_details">Mandate Link</p>
                      <form>
                        <input type="text" class="input-mod" id="mandate_link" placeholder="Paste mandate link here" required>
                        <input type="button" name="" id="add_btn" value="Add Link" class="btn btn-primary pull-right"  data-userid="">
                    </form>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 ">                   

                      <input type="button" name="" value="Complete" class="btn btn-primary complete-btn" align="center" id="complete-btn-id" data-userid="">
                 </div>

                </form>


                </div>
                  
                </div>
              </div>



              
                        
          </div> <!--Search Bar Ends -->
     </div>

<!-- 

           <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                    
                    <li><a href="{{URL('/about_us')}}">About</a></li>
                    <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                    <li><a href="{{URL('/disclosure')}}">Disclosures</a></li>
                    <li><a href="{{URL('/contact_us')}}">Contact</a></li>
                   </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="{{URL('/icons/twitter.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="{{URL('/icons/facebook.png')}}"></a>

                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="{{URL('/icons/linked_in.png')}}"></a>

                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>

              </div>
        </div>
       -->

</div> <!--Content Ends -->

</div>



<div id="nach_popup" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="inner_modal_body">
              
              <video controls="true">
                <source src="{{'../ekycvideo/sample_video.mp4'}}" type="video/mp4">
              </video>

             
            
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">

            <button type="button" class="btn btn-primary text-center" data-dismiss = "modal" id="notify-done">Done</button>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>




  <script src="{{URL('/js/jquery.min.js')}}"></script>
  <script src="{{URL('/js/bootstrap.min.js')}}"></script>
  <!--<script src="../js/index.js"></script>-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
  <script src="{{URL('/js/scroller.js?v=1.2')}}"></script>
  <script src="{{URL('/js/smart_search.js?v=1.2')}}"></script>
  <script src="{{url('js/loader.js?v=1.2')}}"></script>
  <script src="{{url('js/new-nach.js?v=1.3')}}"></script>


  <script>
    $('.from-to').datepicker({ dateFormat: "dd-mm-yy",changeMonth: true, changeYear: true, yearRange: '2016:2050' });
  </script>

</body>
</html>
