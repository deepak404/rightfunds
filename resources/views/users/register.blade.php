<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign Up | Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/jquery.donut.js?v=1.1')}}"></script>
    <script src="{{url('js/register.js?v=1.1')}}"></script>
    <script src="{{url('js/loader.js?v=1.1')}}"></script>
    <link rel="stylesheet" href="{{url('css/login.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/login-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/register-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/loader.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">

      <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
<body>



<div class="loader" id="loader"></div>
  
<div class = "container">



    
    <div class="logo-wrapper">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-div">
              <img class = "login-logo center-block" src="icons/login_logo.png">
      </div>
    </div>

    <div class="content">
        <div class=" row login-div">
          <p class="login-info-text">Register for an account</p>
          <p class="login-sub-text">Begin Investing to achieve your goals!</p>

          <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 form-div padding-lr-zero">
            <form action="{{ url('/register') }}" name="register-form" id="register-form" method="POST">
            {{csrf_field()}}
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <!--<p id="acc-type">Type of Account</p>-->


           <!-- <div class="radio-divs">

                    <div class = " ind-radio-div padding-lr-zero radio-inline">
                      <input type="radio"  name="acc_type" id="individual" value = "0" >
                      <label for="individual"><span class="radio">INDIVIDUAL</span></label>
                    </div>

                    <div class = " cor-radio-div padding-lr-zero radio-inline">
                      <input type="radio" name="acc_type" id="corporate" value = "1">
                      <label for= "corporate"><span class="radio">CORPORATE</span></label>
                    </div>
              
            </div>-->

            <input type="hidden" name="p_check" value="0">

              <div class="form-group has-error">
                <input type="text" class="form-inp" name="name" id = "name" required placeholder="Name">
                        <span class="help-block help-center" id="name_error"></span>
              </div>

              

<!--               <div class="form-group has-error">
                <input type="text" class="" name="pan" id = "pan" required placeholder="PAN Number" minlength="10" maxlength="10">
                  <span class="help-block help-center" id="pan_error"></span>
              </div> -->

              <div class="form-group has-error">
                <input type="email" name="email" id = "email" required placeholder="Email">
                <span class="help-block help-center" id="email_error"></span> 
              </div>

              <div class="form-group has-error">
                <input type="password" name="password" id = "password" required placeholder="Password">
                      <span class="help-block help-center" id="password_error"></span>
              </div><!--<a href="#"><p id="trbl-log">Trouble logging in ?</p></a>-->


              <div class="form-group has-error">
                <input type="password" name="password_confirmation" id = "password" required placeholder="Retype Password">
                      <span class="help-block help-center" id="password_confirmation_error"></span>
              </div>

              <div class="form-group has-error">
                <input type="text" name="mobile" id = "mobile" required placeholder="Mobile" minlength="10" maxlength="10">
                      <span class="help-block help-center" id="mobile_error"></span>
              </div>

              <div class="form-group">
                <input type="submit" class = "center-block btn btn-primary" id="signup-btn" value="SIGN UP">
              </div>

              <div class="hr">
            
              </div>

            </form>
          </div>

           <div class="row">
                <p class="login-info-text">Already have an account ?</p>
                <p class="login-sub-text">Just Log in  </p>

                 <div class="form-group">
                <input type="button" class = " center-block btn btn-primary" onclick="location.href='/login';"  id="login-btn" value="LOG IN">
                </div>


           </div>
        </div>

        <div class="login-footer">
          <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 footer-link padding-lr-zero">

              <ul class="footer-links list-inline">
               <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>
          </div>

          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 padding-lr-zero disc-div">
            <p id="disc-text">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p> 
          </div>

          <div class="col-lg-4 col-sm-3 col-md-3 col-xs-12 padding-lr-zero social-media">

              <ul class="social-links list-inline">
                <li class="soc-img"><a href="#"><img src="icons/twitter.png"></a></li>
                <li class="soc-img"><a href="#"><img src="icons/facebook.png"></a></li>
                <li class="soc-img"><a href="#"><img src="icons/linked_in.png"></a></li>
              </ul>

          </div>



          <div class=" col-lg-8 col-md-9 col-sm-9 col-xs-12 ">
            <p id="copyright-text" >&copy;2017, Rightfunds.com</p>
          </div>


        </div>

       

    </div> <!--Content Ends -->

</div>

<script>

</script>


<!-- Modal -->
<div id="otpModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>      
      </div>
      <div class="modal-body otp-modal-body">
            
        <p id="we-have">We have sent you an access code</p>
        <p id="sms"> via SMS for mobile number verification</p>

        <form name="otp-ver" id="otp-ver" class="form-inline">
          <div class="form-group otp-form-group">
            <div class="row otp-row">
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="otp1" id="otp1" class="otp-field" minlength="1" maxlength="1" required>
              </div>
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="otp2" id="otp2" class="otp-field" minlength="1" maxlength="1" required>
              </div>
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="otp3" id="otp3" class="otp-field" minlength="1" maxlength="1" required>
              </div>
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="otp4" id="otp4" class="otp-field" minlength="1" maxlength="1" required>
              </div>    
            </div>
          </div>
          <div id="otp_error_msg"><span><span></div>
          <div id="otp_success_msg"><span><span></div>
          <div class="row">
            <p id = "didn-t">Didn't receive the OTP ? <span id="resend">Resend OTP</span></p>
          </div>

          <input type="submit" name="verify" value="VERIFY" id = "verify-btn" class="btn btn-primary center-block">
        </form>
      </div>
    </div>
 </div>

<div id="registerFailModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p>Oops !</p>
            <p id="error-msg"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">
            <button type="button" onclick="javascript:location.href='/#'" class="btn btn-primary text-center" id="error-done">DONE</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
