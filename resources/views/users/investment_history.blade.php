<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details.css?v=1.1')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('css/jqueryui.css')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.css?v=1.1')}}">
    <link href="{{url('css/font-awesome.min.css?v=1.1')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css/inv-history.css?v=1.1')}}" rel="stylesheet" type="text/css">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>


<nav class="navbar">
  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/"><img src="icons/logo.png"></a>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="menu-text"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text menu-text-active"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    </div>
  </div>  
    
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Account Summary</p>
                      <p class="invest-text">Get your latest account statements</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="{{url('icons/zero_fee.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="{{url('icons/auto_inv.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="{{url('icons/tax_free.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>


          


          <div class = "row panel investment-summary">
             <div class="heading">
               <ul class="top-links list-inline panel-nav nav-tabs">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                    <a href="{{URL('/portfolio_details')}}"><li class="list-headings">Portfolio Details</li></a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                    <a href="{{URL('/investment_history')}}"><li class="list-headings active-page">Investment History</li></a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                    <a href="{{URL('/tax_saving')}}"><li class="list-headings">Tax Saving Statements</li></a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                    <a href="{{URL('/withdraw_funds')}}"><li class="list-headings no-b-right">Withdraw Funds</li></a>
                  </div>                 
               </ul>             
             </div><!-- Heading ends -->



              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" id="export-row">
                  <form id="investment_history_form" name="investment_history_form"  method="post" action="/get_investment_history" >
                    <div class="col-lg-10 col-md-10 col-sm-10">
                  
                      <p class="hereshow">Show history for<span class="styled-select slate">
                        
                        {{csrf_field()}}
                          <select id="investment_history_period" name="investment_histroy_period">
                            <option value="6">Last 6 months</option>
                            <option value="1">Last 1 year</option>
                            <option value="2">Last 2 years</option>
                          </select>
                          <input type="hidden" value="document" id="response_type" name="response_type">
                        
                      </span></p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                      <input type="submit" href="/get_user_portfolio_document" target="_blank" id="export-btn" class="btn btn-primary" value="EXPORT"/>
                    </div>
                  </form>
                </div><!-- export-row ends -->                
              </div><!-- Row ends -->

              

                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                  <p class="funds-name">EQUITY FUNDS</p>
                    <div class="table-wrapper" id="equity-table-wrapper">
                     
                        @if(array_key_exists('eq',$investment_details))
                            <table class="table" id="equity-table">
                              <thead>
                                <tr class="backg">
                                  <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                                  <th>Type of Investment</th>
                                  <th>Amount invested(Rs.)</th>
                                  <th>Transaction Status</th>
                                </tr>
                              </thead>
                              <button type="button" class="btn btn-primary" id="equity-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i></button>

                              <tbody id="equity_investment_history">
                                @foreach($investment_details['eq'] as $investment)
                                  @if(is_array($investment))
                                    <tr>
                                      <td>{{$investment['investment_date']}}</td>
                                      <td>{{$investment['investment_type']}}</td>
                                      <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment['investment_amount']}}</div><br/><span class="investment-data" onclick="get_investment_details({{$investment['investment_id']}})">View Details</span></td>
                                       <td>{{$investment['investment_status']}}</td>
                                    </tr>
                                  @endif
                                @endforeach
                                <tr>
                                  <td class="total  bt-green">TOTAL AMOUNT</td>
                                  <td class="total  bt-green"></td>
                                  <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment_details['eq']['total_investment']}}</div></td>
                                  <td class="total  bt-green"></td>    
                                </tr>
                              </tbody>
                          
                        
                        @else
                          <p class="text-center no_inv_port">No Investments in this portfolio for the selected period.</p>
                        
                        @endif
                          


                      </table>
                    </div>
                  </div>



                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                  <p class="funds-name">DEBT FUNDS</p>
                    <div class="table-wrapper" id="debt-table-wrapper">
                     @if(array_key_exists('debt',$investment_details))
                      <table class="table" id="equity-table">
                        <thead>
                          <tr class="backg">
                            <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                            <th>Type of Investment</th>
                            <th>Amount invested(Rs.)</th>
                            <th>Transaction Status</th>
                          </tr>
                        </thead><button type="button" class="btn btn-primary" id="debt-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>
                          <tbody id="debt_investment_history">
                            @foreach($investment_details['debt'] as $investment)
                              @if(is_array($investment))
                                <tr>
                                  <td>{{$investment['investment_date']}}</td>
                                  <td>{{$investment['investment_type']}}</td>
                                  <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment['investment_amount']}}</div><br/><span class="investment-data" onclick="get_investment_details({{$investment['investment_id']}})">View Details</span></td>
                                   <td>{{$investment['investment_status']}}</td>
                                </tr>
                              @endif
                            @endforeach
                            <tr>
                              <td class="total  bt-green">TOTAL AMOUNT</td>
                              <td class="total  bt-green"></td>
                              <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment_details['debt']['total_investment']}}</div></td>
                              <td class="total  bt-green"></td>
                                 
                            </tr>
                          </tbody>
                        @else
                          <p class="text-center no_inv_port">No Investments in this portfolio for the selected period.</p>
                        @endif
                      </table>
                    </div>
                  </div>



                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                  <p class="funds-name">BALANCED FUNDS</p>
                    <div class="table-wrapper" id="hybrid-table-wrapper">
                     
                        @if(array_key_exists('bal',$investment_details))
                        
                         <table class="table" id="equity-table">
                          <thead>
                            <tr class="backg">
                              <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                              <th>Type of Investment</th>
                              <th>Amount invested(Rs.)</th>
                              <th>Transaction Status</th>
                            </tr>
                          </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>

                        <tbody id="bal_investment_history">
                          @foreach($investment_details['bal'] as $investment)
                            @if(is_array($investment))
                              <tr>
                                <td>{{$investment['investment_date']}}</td>
                                <td>{{$investment['investment_type']}}</td>
                                <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment['investment_amount']}}</div><br/><span class="investment-data" onclick="get_investment_details({{$investment['investment_id']}})">View Details</span></td>
                                 <td>{{$investment['investment_status']}}</td>
                              </tr>
                            @endif
                          @endforeach
                        <tr>
                            <td class="total  bt-green">TOTAL AMOUNT</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment_details['bal']['total_investment']}}</div></td>
                            <td class="total  bt-green"></td>
                               
                          </tr>
                        </tbody>
                        
                        @else
                          <p class="text-center no_inv_port">No Investments in this portfolio for the selected period.</p>
                        
                        @endif
                          


                      </table>
                    </div>
                  </div>


                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                  <p class="funds-name">ELSS FUNDS</p>
                    <div class="table-wrapper" id="elss-table-wrapper">
                     
                        @if(array_key_exists('ts',$investment_details))
                         <table class="table" id="equity-table">
                          <thead>
                            <tr class="backg">
                              <th><div class ="col-xs-8" id="mutual">Date Invested</div></th>
                              <th>Type of Investment</th>
                              <th>Amount invested(Rs.)</th>
                              <th>Transaction Status</th>
                            </tr>
                          </thead><button type="button" class="btn btn-primary" id="elss-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>

                        <tbody id="elss_investment_history">
                          @foreach($investment_details['ts'] as $investment)
                            @if(is_array($investment))
                              <tr>
                                <td>{{$investment['investment_date']}}</td>
                                <td>{{$investment['investment_type']}}</td>
                                <td><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment['investment_amount']}}</div><br/><span class="investment-data" onclick="get_investment_details({{$investment['investment_id']}})">View Details</span></td>
                                 <td>{{$investment['investment_status']}}</td>
                              </tr>
                            @endif
                          @endforeach
                        <tr>
                            <td class="total  bt-green">TOTAL AMOUNT</td>
                            <td class="total  bt-green"></td>
                            <td class="total  bt-green"><div class="col-xs-8 padding-lr-zero" id="amount-heading">{{$investment_details['ts']['total_investment']}}</div></td>
                            <td class="total  bt-green"></td>
                               
                          </tr>
                        </tbody>
                        
                        
                        @else
                          <p class="text-center no_inv_port">No Investments in this portfolio for the selected period.</p>
                        
                        @endif
                          


                      </table>
                    </div>
                  </div>

            </div> <!-- investment-summary ends -->
    


          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>

<!-- Modal -->
<div id="invDetailsModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>        
      </div>
      <div class="modal-body" id="invDetailsModalBody">


      <div class="row">
        <div class="col-xs-12 padding-lr-zero">
          <table class="table table-striped">
            <thead>
              <tr>
                <th><p class="text-center gotham_book header_table">Scheme Name</p></th>
                <th><p class="text-center gotham_book header_table">Date of Investment</p></th>
                <th><p class="text-center gotham_book header_table">Amount Invested</p></th>
                <th><p class="text-center gotham_book header_table">Purchase NAV</p></th>
              </tr>
            </thead>
            <tbody id="pending-inv-body">
            </tbody>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <p id="modal-disc">Disclosure : We encourage our investors to acquaint themselves with all the information provided by the AMC, before taking investment decision</p>
        </div>
      </div>

       </div>
    </div>
  </div>
</div>

  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <script src="{{url('js/investment_history.js')}}"></script>
  <script src="{{url('js/scroller.js')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
    function get_investment_details(id){var dataString='investment_id='+id;$.ajax({type:"POST",url:"/get_investment_details",headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},cache:!1,processData:!1,data:dataString,success:function(data){var investment_data='';$('#pending-inv-body').html('');$.each(data.response,function(key,value){investment_data+='<tr><td><p class="text-center table_content_modal">'+value.scheme_name+'</p></td><td><p class="text-center gotham_book table_content_modal">'+value.investment_date+'</p></td><td><p class="text-center gotham_book table_content_modal">'+value.amount_invested+'</p></td><td><p class="text-center gotham_book table_content_modal">'+value.purchase_nav+'</p></td></tr>'});$('#pending-inv-body').html(investment_data);if(investment_data!=''){$('#invDetailsModal').modal('show')}},error:function(xhr,status,error){},})}
  </script>

</body>
</html>
