<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login | Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/register.js')}}"></script>
    <script src="{{url('js/loader.js')}}"></script>
    <link rel="stylesheet" href="{{url('css/login.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/login-responsive.css')}}">
    <link rel="stylesheet" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/loader.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>



<div class="loader" id="loader"></div>

<div class = "container">

    <div class="logo-wrapper">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-div">
              <a href="{{URL('/')}}"><img class = "login-logo center-block" src="icons/login_logo.png"></a>
      </div>
    </div>


    <div class="content">

        <div id = "login_div">
          <div class = "row">
            <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 form-div padding-lr-zero" id="login_div">
              <p class="login-info-text">Log into your account</p>
              <p class="login-sub-text">Begin Investing to achieve your goals!</p>


              <form action="{{ url('/login') }}" method="POST" role = "form" id="login_form">
                {{csrf_field()}}
                
                  <div class="form-group has-error">
                    <input type="email" name="email" id = "email" required placeholder="Enter your email">
                        <span class="help-block help-center" id="email_error"></span>
                  </div>

                  <div class="form-group has-error">
                    <input type="password" name="password" id = "password" required placeholder="Password">
                        <span class="help-block help-center" id="password_error"></span>
                  </div><a href="#"><p id="trbl-log">Trouble logging in ?</p></a>

                  <div class="form-group">
                    <input type="submit" class = " center-block btn btn-primary" id="login-btn" value="LOG IN" id = "submit">
                  </div>

              </form>
            </div>
          </div> <!--Row ends -->
          <div class = "hr"></div>   
        </div>  <!--login_div ends -->

        

        <div id="trouble_login">
              <div class = "row" id = "trouble_login_row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <p class="login-info-text">Trouble Logging in ?</p>
                    <p class="login-sub-text">To start investing and achieve your life goals</p>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div id="trouble-text">
                      <p class="text-center"><span><a style = "display: inline;" class="password_opt" id = "i_forgot" href="{{URL('/password/reset')}}">I forgot my Password</a></span></p>
                      <!--<a class="password_opt" id = "i_want" href="{{URL('/password/reset')}}">I want to create a new Password.</a>-->
                      <p class="text-center"><span><a style = "display: inline;" class="password_opt" href = "{{URL('/contact-us')}}">Contact Us</a></span></p>
                    </div>
                    <input type="button" class="btn btn-primary center-block" name="back-login" id="back-login" value="BACK">
                  </div>

              </div> 
              <div class="hr"></div>
            
        </div>   
        

        <div id="enter_email_div">
              <div class = "row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <p class="login-info-text">Enter Your email?</p>
                    <p class="login-sub-text">We will send recovery email to your registered email address.</p>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div id="trouble-text">
                      <form id="" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            

                            <div class="col-xs-12" id="reset-email-div">
                                <input id="email" type="email" class="reset-email" placeholder="Email" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div id = "button-div" class = "text-center">
                          <button type="submit" class="btn btn-primary center-block" name="reset-btn" id="reset-btn" value="RESET">RESET</button>
                          <button type = "button" id = "go_back" class = "btn btn-primary">BACK</button>
                        </div>
                      </form>
                    </div>
                  </div>
              
              </div> 
              <div class="hr"></div>
            </div>
            

            <div id = "no_acct_div">             
              <div class="row">
                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <p class="login-info-text">Don't have an account ?</p>
                  <p class="login-sub-text">Create one now and start investing today. </p>
                 <div class="form-group">
                    <input type="button" class = " center-block btn btn-primary" onclick="location.href='/register';" id="signup-btn" value="SIGNUP">
                  </div>
                </div>
              </div>
            </div>

        <div class="login-footer">
          <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 footer-link padding-lr-zero">

              <ul class="footer-links list-inline">
                <li><a href="{{URL('/')}}">Home</a></li>
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <!--<li><a href="{{URL('#')}}">Blog</a></li>-->
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <!--<li><a href="{{URL('/disclosure')}}">Disclosures</a></li>-->
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>
          </div>

          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 padding-lr-zero disc-div">
            <p id="disc-text">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p> 
          </div>

         

          <div class="col-lg-5 col-sm-3 col-md-3 col-xs-12 padding-lr-zero social-media">

              <!--<ul class="social-links list-inline">
                <li class="soc-img"><a href="#"><img src="icons/twitter.png"></a></li>
                <li class="soc-img"><a href="#"><img src="icons/facebook.png"></a></li>
                <li class="soc-img"><a href="#"><img src="icons/linked_in.png"></a></li>
              </ul>-->


              <div class="col-lg-4 col-sm-4 col-md-4 col-xs-2   padding-lr-zero">
                <a href="https://twitter.com/rightfundsindia"><img  class="center-block footer-soc-img" src="icons/twitter.png"></a>
              </div>

              <div class="col-lg-4 col-sm-4 col-md-4  col-xs-2 padding-lr-zero" >
               <a href="https://www.facebook.com/rightfunds/"><img  class="center-block footer-soc-img" src="icons/facebook.png"></a>
              </div>

              <div class="col-lg-4 col-sm-4 col-md-4  col-xs-2  padding-lr-zero" style="margin-left: 0%;">
                <a href="https://www.linkedin.com/company/rightfunds.com"><img class="center-block footer-soc-img"  src="icons/linked_in.png"></a>
              </div>
          </div>

           <div class=" col-lg-7 col-md-9 col-sm-9 col-xs-12 ">
            <p id="copyright-text" >&copy;2017, Rightfunds.com</p>
          </div>



          


        </div>

       

    </div> <!--Content Ends -->

</div>

<!-- Modal -->
<div id="otpModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>      
      </div>
      <div class="modal-body otp-modal-body">
            
        <p id="we-have">We have sent you an accesss code</p>
        <p id="sms"> via SMS for mobile number verification</p>

        <form name="otp-ver" id="otp-ver" class="form-inline">
          <div class="form-group otp-form-group">
            <div class="row otp-row">
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="1" id="otp1" class="otp-field" minlength="1" maxlength="1" required>
              </div>
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="2" id="otp2" class="otp-field" minlength="1" maxlength="1" required>
              </div>
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="3" id="otp3" class="otp-field" minlength="1" maxlength="1" required>
              </div>
              <div class="col-xs-2 padding-lr-zero">
                <input type="text" name="4" id="otp4" class="otp-field" minlength="1" maxlength="1" required>
              </div>    
            </div>
          </div>
          <div id="otp_error_msg"></div>
          <div id="otp_success_msg"></div>
          <div class="row">
            <p id = "didn-t">Didn't receive the OTP ? <span id="resend">Resend OTP</span></p>
          </div>

          <input type="submit" name="verify" id="verify-btn" value="VERIFY" class="btn btn-primary verify-btn center-block">
        </form>
      </div>
    </div>
  </div>


  <script>
    


  </script>

</body>
</html>
