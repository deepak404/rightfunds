    <!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Login</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/register.css?v=1.1">
        <link rel="stylesheet" href="css/version2/register-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/modal.css?v=1.1">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>

      <style type="text/css">

      </style>
    </head>
    <body>
    <div id="loader" class="loader"></div>

        <section id="register-section">
            <div class="container box-shadow-all br " id="register-container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <a href="{{url('/')}}"><img src="icons/nav-logo.svg" class="logo"></a>
                        <div class = "col-lg-6 col-md-6 col-sm-6 border-right">
                            <p class="heading">Get your Free Account now.</p>
                            <p class="sub-heading">Invest in Best Equity, Debt, Tax saving mutual funds with rightfunds.com</p>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="footer-container">
                                <p class="sub-heading">Follow us on</p>
                                <ul class="list-inline">
                                    <li><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linked-icon.png" class="social-icon"></a></li>
                                </ul>
                                <ul class="list-inline">
                                    <li class="footer-links"><a target = "_blank" href="{{url('/about-us')}}">About</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/terms')}}">Terms of Use</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/contact-us')}}">Contact</a></li>
                                </ul>
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6" id="second-col">
                            <div class = "col-lg-6 col-md-6 col-sm-6 p-lr-zero">  
                                <p class="heading form-heading  active-form" id="login-form-head">Login</p>    
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="form-container">
                                <form id="login-form">  
                                    
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="text" name = "email" id="email" class="input-field" required>
                                            <label>Email</label>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="password" name = "password" id="password" class="input-field" required>
                                            <label>Password</label>
                                            <span class="text-danger"></span>
                                            <a href = "{{url('/password/reset')}}" class="pull-right blue-text" id="forgot-password">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <p id="login-error" class = "text-danger"></p>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="submit" name = "submit" id="submit" value="Login" class="btn btn-primary register-btn">
                                            <span class="blue-text">Don't have an account? <a id = "login-link" href="{{url('/register')}}"> Register</a></span>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="right-footer-container">
                                <p class="sub-heading">Follow us on</p>
                                <ul class="list-inline">
                                    <li><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linked-icon.png" class="social-icon"></a></li>
                                </ul>
                                <ul class="list-inline">
                                    <li class="footer-links"><a target = "_blank" href="{{url('/about-us')}}">About</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/terms')}}">Terms of Use</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/contact-us')}}">Contact</a></li>
                                </ul>
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


     <script src="js/jquery.min.js"></script>
     <script src="js/register.js?v=1.1"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="js/version2/loader.js?v=1.1"></script>
     <script src="js/version2/validation.js?v=1.1"></script>

     </script>
    </body>
</html>
