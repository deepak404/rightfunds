<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Withdraw Details</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        
        <link rel="stylesheet" href="css/version2/acc-statement.css?v=1.1">
        <link rel="stylesheet" href="css/version2/modal.css?v=1.1">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/pd-responsive.css?v=1.1">


        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="css/version2/withdraw-details.css?v=1.1">
      <link rel="stylesheet" href="css/version2/withdraw-responsive.css?v=1.1">
      <link rel="stylesheet" href="{{url('css/version2/navbar-responsive.css?v=1.1')}}">

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
      
    </head>
    <body>

    <div id="loader" class="loader"></div>

    @extends('layouts.navbar')
    @section('content')

        <section id="acc-statement-header">
            <div class="container">
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline text-center fl" id="acc-statement-header-ul">
                            <li><a href="{{url('/portfolio_details')}}">Portfolio Details</a></li>
                            <li><a href="{{url('/investment_history')}}">Investment History</a>
                            <li><a href="{{url('/tax_saving')}}">Tax Saving Statement</a>
                            <li class="active-acc-header"><a href="{{url('/withdraw_funds')}}">Withdraw Funds</a>
                        </ul>
                    </div>
                </div>  
            </div><!-- container ends -->
        </section>
        <?php $port_count = count($portfolio_details); ?>
        @if($port_count > 0)
        <section id="withdraw-info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                    
                        
                            <p class="withdraw-imp-info">We strongly recommend that you keep all the fund in your portfolio for at least a year before withdrawing, for the following reasons:</p>
                            <ul id="withdraw-info-list">
                                <li> Different funds perform differently under the given market conditions. It is important to maintain a diversified portfolio at all times in order to minimise risk.</li>
                                <li>Staying invested in equity funds for at least 1 year makes their return count as Long term capital gains. LTCG is taxed at 10%.</li>
                                <li>[In the case of Debt funds, returns are considered as LTCG after staying invested for at least a period of 3 years. Following this return are taxed lower at 20% after accounting for inflation.]</li>
                                <li>Staying invested for a year prevents any exit loads(if applicable) from being applied to the amount you withdraw. Exit load is usually 1% of the amount withdrawn.</li>
                            </ul>
                    </div>
                </div>
            </div>
        </section>
        @endif
                        
        <section id="withdraw-section">
            <div class="container">
                <div class="row">
                    <form id="withdrawal-form">
                        <div class="col-lg-12 col-md-12 col-sm-12 ">

                        
                        @if($port_count > 0)

                            <div class="col-lg-9 col-md-9 col-sm-9">
                                
                                <div class="col-lg-12 col-md-12 col-sm-12 br border-all p-lr-zero">
                                    @foreach($portfolio_details as $scheme_type => $scheme_details)
                                    <?php //dd(count($scheme_details)) ?>
                                    @if(is_array($scheme_details) && $scheme_type == 'eq')
                                    <p class="table-container-header">Equity</p>
                                    <div class="acc-table-wrapper">
                                        <table class="table withdraw-table">
                                            <tbody>
                                                @foreach($scheme_details as $scheme_detail)
                                                <tr class="border-bot">
                                                    <td>
                                                        <p class="withdraw-fund-name">{{$scheme_detail['scheme_name']}}</p>
                                                        <p class="current-invested">Current Value - Rs. <span id="{{$scheme_detail['scheme_code']}}">{{$scheme_detail['current_value']}}</span></p>
                                                        @if(array_key_exists($scheme_detail['scheme_code'],$pending_withdraws))
                                                        <p class="pending-withdraw">Pending Withdraw - Rs. {{$pending_withdraws[$scheme_detail['scheme_code']]['amount']}}</p>
                                                        @endif
                                                    </td>
                                                    <td>

                                                        <input type="text"  name="{{$scheme_detail['scheme_code']}}" id="{{$scheme_detail['scheme_code']}}" class="input-field custom-amount num-field" placeholder="Enter Amount">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="full-amount" data-scheme="{{$scheme_detail['scheme_code']}}" id="checkbox-{{$scheme_detail['scheme_code']}}" name="">
                                                        <label for="checkbox-{{$scheme_detail['scheme_code']}}"><i class="material-icons">check_box_outline_blank</i></label>
                                                        <span>Full Amount</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table> <!-- Equity Withdraw Table ends -->
                                    </div>
                                    @elseif(is_array($scheme_details) && $scheme_type == 'bal')
                                    <p class="table-container-header">Balanced</p>
                                    <div class="acc-table-wrapper">
                                        <table class="table withdraw-table" >
                                            <tbody>
                                                @foreach($scheme_details as $scheme_detail)
                                                <tr class="border-bot">
                                                    <td>
                                                        <p class="withdraw-fund-name">{{$scheme_detail['scheme_name']}}</p>
                                                        <p class="current-invested">Current Value - Rs. <span id="{{$scheme_detail['scheme_code']}}">{{$scheme_detail['current_value']}}</span></p>
                                                         @if(array_key_exists($scheme_detail['scheme_code'],$pending_withdraws))
                                                        <p class="pending-withdraw">Pending Withdraw - Rs. {{$pending_withdraws[$scheme_detail['scheme_code']]['amount']}}</p>
                                                        @endif


                                                    </td>
                                                    <td>

                                                        <input type="text"  name="{{$scheme_detail['scheme_code']}}" id="{{$scheme_detail['scheme_code']}}" class="input-field custom-amount num-field" placeholder="Enter Amount">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="full-amount" data-scheme="{{$scheme_detail['scheme_code']}}" id="checkbox-{{$scheme_detail['scheme_code']}}" name="">
                                                        <label for="checkbox-{{$scheme_detail['scheme_code']}}"><i class="material-icons">check_box_outline_blank</i></label>
                                                        <span>Full Amount</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table> <!-- Balanced Withdraw Table ends -->
                                    </div>
                                    @elseif(is_array($scheme_details) && $scheme_type == 'debt')
                                    <p class="table-container-header">Debt</p>
                                    <div class="acc-table-wrapper">
                                        <table class="table withdraw-table" >
                                            <tbody>
                                                @foreach($scheme_details as $scheme_detail)
                                                <tr class="border-bot">
                                                    <td>
                                                        <p class="withdraw-fund-name">{{$scheme_detail['scheme_name']}}</p>
                                                        <p class="current-invested">Current Value - Rs. <span id="{{$scheme_detail['scheme_code']}}">{{$scheme_detail['current_value']}}</span></p>
                                                         @if(array_key_exists($scheme_detail['scheme_code'],$pending_withdraws))
                                                        <p class="pending-withdraw">Pending Withdraw - Rs. {{$pending_withdraws[$scheme_detail['scheme_code']]['amount']}}</p>
                                                        @endif


                                                    </td>
                                                    <td>

                                                        <input type="text"  name="{{$scheme_detail['scheme_code']}}" id="{{$scheme_detail['scheme_code']}}" class="input-field custom-amount num-field" placeholder="Enter Amount">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="full-amount" data-scheme="{{$scheme_detail['scheme_code']}}" id="checkbox-{{$scheme_detail['scheme_code']}}" name="">
                                                        <label for="checkbox-{{$scheme_detail['scheme_code']}}"><i class="material-icons">check_box_outline_blank</i></label>
                                                        <span>Full Amount</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table> <!-- Debt Withdraw Table ends -->
                                    </div>
                                    @elseif(is_array($scheme_details) && $scheme_type == 'ts')
                                    <p class="table-container-header">Debt</p>
                                    <div class="acc-table-wrapper">
                                        <table class="table withdraw-table" >
                                            <tbody>
                                                @foreach($scheme_details as $scheme_detail)
                                                <tr class="border-bot">
                                                    <td>
                                                        <p class="withdraw-fund-name">{{$scheme_detail['scheme_name']}}</p>
                                                        <p class="current-invested">Current Value - Rs. <span id="{{$scheme_detail['scheme_code']}}">{{$scheme_detail['current_value']}}</span></p>
                                                         @if(array_key_exists($scheme_detail['scheme_code'],$pending_withdraws))
                                                        <p class="pending-withdraw">Pending Withdraw - Rs. {{$pending_withdraws[$scheme_detail['scheme_code']]['amount']}}</p>
                                                        @endif


                                                    </td>
                                                    <td>

                                                        <input type="text"  name="{{$scheme_detail['scheme_code']}}" id="{{$scheme_detail['scheme_code']}}" class="input-field custom-amount num-field" placeholder="Enter Amount">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" class="full-amount" data-scheme="{{$scheme_detail['scheme_code']}}" id="checkbox-{{$scheme_detail['scheme_code']}}" name="">
                                                        <label for="checkbox-{{$scheme_detail['scheme_code']}}"><i class="material-icons">check_box_outline_blank</i></label>
                                                        <span>Full Amount</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table> <!-- Debt Withdraw Table ends -->
                                    </div>
                                    @endif
                                    @endforeach

                                </div> <!-- col-lg-12 col-md-12 col-sm-12 -->
                            </div> <!-- col-lg-9 col-md-9 col-sm-9 --> 
                            <div class="col-lg-3 col-md-3 col-sm-3 br">
                                <div class="col-lg-12 col-md-12 col-sm-12 br border-all p-lr-zero box-shadow-all" id="withdraw-summary">
                                    <p class="border-bot summary-header">Withdraw Summary</p>
                                    <div id="withdraw-summary-details" class="col-lg-12 col-sm-12 col-md-12">
                                        
                                        <p class="cont-header" id="total-amount-text">Total Amount Withdrawal</p>
                                        <p class="withdraw-info-amount">Rs.<span>0</span></p>
                                        <input type="submit" class="btn btn-primary" id ="withdraw-btn" value="Withdraw" name="">
                                    </div>
                                </div>
                            </div>
                            @else
                            <h4 id="no-investment" class="text-center">No Investments yet to withdraw.</h4>
                            @endif
                        </div>
                    </form>
                </div> <!-- Row ends -->
            </div>
        </section>


        @endsection()

        <!-- Modal -->
        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header"></h4>
              </div>
              <div class="modal-body" id="modal-body">
                
              </div>
            </div>

          </div>
        </div>

        <script type="text/javascript" src="js/version2/withdraw-funds.js?v=1.1"></script>
        <script type="text/javascript" src="js/version2/validation.js?v=1.1"></script>
        <script src="js/version2/loader.js?v=1.1"></script>
        <script type="text/javascript">

            function redirect(){
                window.location.href = '/';
            }

        </script>
    </body>
</html>
