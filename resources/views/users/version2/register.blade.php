    <!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Registration</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/register.css?v=1.1">
        <link rel="stylesheet" href="css/version2/register1-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/modal.css?v=1.1">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <style type="text/css">
            #register-form{
                display: block;
            }
      </style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

        <div id="loader" class="loader"></div>
        <section id="register-section">
            <div class="container box-shadow-all br" id="register-container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <a href="{{url('/')}}"><img src="icons/nav-logo.svg" class="logo"></a>
                        <div class = "col-lg-6 col-md-6 col-sm-6 border-right">
                            <p class="heading">Get your Free Account now.</p>
                            <p class="sub-heading">Invest in Best Equity, Debt, Tax saving mutual funds with rightfunds.com</p>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="footer-container">
                                <p class="sub-heading">Follow us on</p>
                                <ul class="list-inline">
                                    <li><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linked-icon.png" class="social-icon"></a></li>
                                </ul>
                                <ul class="list-inline">
                                    <li class="footer-links"><a target = "_blank" href="{{url('/about-us')}}">About</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/terms')}}">Terms of Use</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/contact-us')}}">Contact</a></li>
                                </ul>
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6" id="second-col">
                            <div class = "col-lg-6 col-md-6 col-sm-6 p-lr-zero">
                                <p class="heading form-heading blue-text" id="reg-form-head">Register</p>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="form-container">
                                <form action="{{ url('/register') }}" name="register-form" id="register-form" method="POST">  
                                {{csrf_field()}}
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="text" name = "name" id="name" class="input-field text-field" required>
                                            <label>Full Name</label>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="text" name = "email" id="email" class="input-field" required>
                                            <label>Email</label>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="text" name = "mobile" id="mobile" class="input-field num-field" required>
                                            <label>Mobile</label>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="password" name = "password" id="password" class="input-field" required>
                                            <label>Password</label>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="password" name = "password-confirmation" id="password-confirmation" class="input-field" required>
                                            <label>Confirm Password</label>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="submit" name = "submit" id="submit" value="Register" class="btn btn-primary register-btn">

                                            <span class="blue-text">Already have an account? <a id = "login-link" href="{{url('/login')}}"> Login</a></span>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="right-footer-container">
                                <p class="sub-heading">Follow us on</p>
                                <ul class="list-inline">
                                    <li><a href="https://www.facebook.com/rightfunds/"><img src="icons/facebook-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://twitter.com/rightfundsindia"><img src="icons/twitter-icon.png" class="social-icon"></a></li>
                                    <li><a href="https://www.linkedin.com/company/rightfunds.com"><img src="icons/linked-icon.png" class="social-icon"></a></li>
                                </ul>
                                <ul class="list-inline">
                                    <li class="footer-links"><a target = "_blank" href="{{url('/about-us')}}">About</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/terms')}}">Terms of Use</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/contact-us')}}">Contact</a></li>
                                </ul>
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!--         <section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="#">Privacy Policy</a></li>
                            <li class="footer-links"><a href="#">Terms of Use</a></li>
                            <li class="footer-links"><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;2017,Rightfunds.com</p>
                    </div>
                </div>  
            </div>  
        </section> -->

     <script src="js/jquery.min.js"></script>
     <script src="js/register.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="js/version2/loader.js"></script>
     <script src="js/version2/validation.js"></script>

     </script>



     <!-- Modal -->
<div id="otpModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body otp-modal-body">
            
        <p id="we-have" class="text-center">We have sent you an access code</p>
        <p id="sms" class="text-center"> via SMS for mobile number verification</p>

        <form name="otp-ver" id="otp-ver" class="form-inline">
          <div class="form-group otp-form-group">
            <div class="row otp-row">
              <div class = "col-lg-12 col-md-12 col-sm-12" id="otp-container">
                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                    <input type="text" name="otp1" id="otp1" class="otp-field input-field" minlength="1" maxlength="1" required>
                  </div>
                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                    <input type="text" name="otp2" id="otp2" class="otp-field input-field" minlength="1" maxlength="1" required>
                  </div>
                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                    <input type="text" name="otp3" id="otp3" class="otp-field input-field" minlength="1" maxlength="1" required>
                  </div>
                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                    <input type="text" name="otp4" id="otp4" class="otp-field input-field" minlength="1" maxlength="1" required>
                  </div>    
              </div>
            </div>
          </div>
          <div id="otp_error_msg"><span><span></div>
          <div id="otp_success_msg"><span><span></div>
          <div class="row">
            <p id = "didn-t" class="text-center">Didn't receive the OTP ? <span id="resend">Resend OTP</span></p>
          </div>

          <input type="submit" name="verify" value="VERIFY" id = "verify-btn" class="btn btn-primary center-block">
        </form>
      </div>
    </div>
 </div>
    </body>
</html>
