<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Settings</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">


        <link rel="stylesheet" href="css/version2/settings.css?v=1.1">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/jqueryui.css?v=1.1">
        <link rel="stylesheet" href="css/version2/modal.css?v=1.1">

        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/version2/navbar-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/activate-acc-responsive.css?v=1.1">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>

    <body>
    <div class="loader" id="loader"></div>
    
        
        @extends('layouts.navbar')
        @section('content')

        <section id="settings">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 p-r-35 p-l-zero" id="sidebar-container">
                            <div class="col-lg-12 col-md-12 col-sm-12 border-all br p-lr-zero" id="settings-sidebar">
                                <ul id="settings-nav">
                                    <li class="border-bottom active-settings-nav"><a href="#">Personal Details</a></li>
                                    <li class="border-bottom inactive-sidemenu"><a href="#">Change Password</a></li>
                                    <li class="border-bottom inactive-sidemenu"><a href="#">Bank Details</a></li>
                                    <li class="inactive-sidemenu"><a href="#">Nominee Details</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 border-all br p-lr-zero">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bottom header-pad">
                                <p class="section-header pl-30">Personal Details 
                                   <!--  -->
                                    
                                </p>
                                <p id="bank-details-info">To Edit Bank Details Please Contact <span class="blue-text">88258 88200</span> or <span class="blue-text">contact@rightfunds.com</span></p>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="personal-details-tab">
                                <form id="personal-details-form" class="settings-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "name" id="name" value="{{\Auth::user()->name}}" class="input-field text-field" required>
                                                <label>First Name</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "email" id="email" value="{{\Auth::user()->email}}" class="input-field" required>
                                                <label>E-mail ID</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "mobile" id="mobile" value="{{\Auth::user()->mobile}}" class="input-field num-field" required>
                                                <label>Mobile Number</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" value="{{\Auth::user()->personalDetails->address}}" class="input-field" required>
                                                <label>Communication Address</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pincode" id="pin" value="{{\Auth::user()->personalDetails->pincode}}" class="input-field num-field" required>
                                                <label>PIN Code</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "aadhar" id="aadhar" value="{{\Auth::user()->personalDetails->aadhar}}" class="input-field num-field" required>
                                                <label>Aadhar Number</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>

                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- Personal Details Tab ends -->


                             <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="bank-details-tab">
                                <form id="bank-details-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-name" id="acc-name" value = "{{ \Auth::user()->bankDetails->first()->acc_name}}" class="input-field text-field" disabled>
                                                <label>Account Holder Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-no" id="acc-no" value = "{{ \Auth::user()->bankDetails->first()->acc_no}}" class="input-field num-field" disabled>

                                                <label>Account Number</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "ifsc-code" id="ifsc-code" value = "{{ \Auth::user()->bankDetails->first()->ifsc_code}}" class="input-field" disabled>
                                                <label>IFSC Code</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address"  class="input-field" value="Savings" disabled>
                                                <label>Account Type</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE"> -->
                                </form>
                            </div><!-- bank Details Tab ends -->

                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="change-password-tab">
                                <form id="change-password-form" class="settings-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "current-password" id="current-password" class="input-field" required>
                                                <label>Current Password</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "new-password" id="new-password" class="input-field" required>
                                                <label>New Password</label>
                                            </div>
                                            <span class="text-danger" style="display: none;" id="no-match"></span>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "confirm-password" id="confirm-password" class="input-field" required>
                                                <label>Re-Enter Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- bank Details Tab ends -->

                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="nominee-details-tab">
                                <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                    
                                        @foreach(\Auth::user()->nomineeDetails as $nominee)
                                        @if($nominee->nomi_type === "1")
                                            <form class="nominee-details-form" data-id="1">
                                            <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom1-name" id="nom1-name" value="{{ $nominee->nomi_name }}" class="input-field text-field" required >
                                                        <label>Nominee Name</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom1-relationship" id="nom1-relationship" value="{{ $nominee->nomi_relationship }}" class="input-field text-field" required>
                                                        <label>Relationship</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">

                                                        @if($nominee->nomi_dob == '')
                                                            <input type="text" name = "nom1-dob" id="nom1-dob" value="" class="input-field" required>

                                                            
                                                        @else
                                                        <input type="text" name = "nom1-dob" id="nom1-dob" value="{{ date('d-m-Y', strtotime($nominee->nomi_dob))}}" class="input-field" required>
                                                        @endif
                                                        
                                                        <label>Date of Birth</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom1-address" id="nom1-address" value="{{ $nominee->nomi_addr }}" class="input-field" required>
                                                        <label>Communication Address</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                               <!--  <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom1-pin" id="nom1-pin" class="input-field" required>
                                                        <label>Pincode</label>
                                                    </div>
                                                </div> -->
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom1-perc" id="nom1-perc" value="{{ $nominee->nomi_holding }}" class="input-field num-field" required>
                                                        <label>Percentage Holding</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>

<!--                                             <div class = "col-lg-12 col-md-12 col-sm-1 p-tb-20 checkbox-row" >
                                                 <input type="checkbox" class="full-amount" id="nom1-copy-addr" name="">
                                                 <label for="nom1-copy-addr" id="nom-label"><i class="material-icons">check_box_outline_blank</i></label>
                                             </div> -->

                                            <input type="submit" class = "blue-btn btn-primary btn" id="submit-nominee" name="" value="SAVE">
                                            </form>
                                        @endif
                                        @if($nominee->nomi_type === "2")
                                            <form class="nominee-details-form" data-id="2">
                                            <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom2-name" id="nom2-name" value="{{ $nominee->nomi_name }}" class="input-field text-field" required >
                                                        <label>Nominee Name</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom2-relationship" id="nom2-relationship" value="{{ $nominee->nomi_relationship }}" class="input-field text-field" required>
                                                        <label>Relationship</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        @if($nominee->nomi_dob == '')
                                                            <input type="text" name = "nom2-dob" id="nom2-dob" value="" class="input-field" required>
                                                        @else
                                                        <input type="text" name = "nom2-dob" id="nom2-dob" value="{{ date('d-m-Y', strtotime($nominee->nomi_dob))}}" class="input-field" required>
                                                        @endif
                                                        <label>Date of Birth</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom2-address" id="nom2-address" value="{{ $nominee->nomi_addr }}" class="input-field" required>
                                                        <label>Communication Address</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                               <!--  <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom1-pin" id="nom1-pin" class="input-field" required>
                                                        <label>Pincode</label>
                                                    </div>
                                                </div> -->
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" name = "nom2-perc" id="nom2-perc" value="{{ $nominee->nomi_holding }}" class="input-field num-field" required>
                                                        <label>Percentage Holding</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>

                                             <!-- <div class = "col-lg-12 col-md-12 col-sm-12 p-tb-20 checkbox-row">
                                                 <input type="checkbox" class="full-amount" id="nom2-copy-addr" name="checkbox1">
                                                 <label for="nom2-copy-addr" id="nom-label"><i class="material-icons">check_box_outline_blank</i></label>
                                                 <span>Copy your Address.</span>
                                             </div>  -->

                                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                                 <input type="submit" class = "blue-btn btn-primary btn" id="submit-nominee" name="" value="SAVE">
                                            </div>
                                            </form>
                                        @endif
                                        @endforeach
                                    
                                </div>
                            </div><!-- Nominee Details Tab ends -->

                        </div> 
                    </div>     
                </div>
            </div>
        </section>

        @endsection

        <!-- Modal -->
        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="modal-header"></h4>
              </div>
              <div class="modal-body" id="modal-body">
                
              </div>
            </div>

          </div>
        </div>

     <script type="text/javascript">
        var nom_count = "{{$nom_count}}";
        //console.log(parseInt(nom_count));
     </script>
     <script src="js/jquery.min.js?v=1.1"></script>
     <script src="js/version2/validation.js?v=1.1"></script>
     <script src="js/version2/loader.js?v=1.1"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script type="text/javascript" src ="js/version2/account-settings.js?v=1.1"></script>

     <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

</html>
