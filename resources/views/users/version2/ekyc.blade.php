<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | EKYC</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">
        <link rel="stylesheet" href="css/jqueryui.css?v=1.1">
        <link rel="stylesheet" href="css/version2/navbar-responsive.css?v=1.1">

        
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> --> 
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="css/version2/ekyc.css?v=1.1">
        <link rel="stylesheet" type="text/css" href="css/version2/modal.css?v=1.1">
        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>


    </head>
    <body>

    @extends('layouts.navbar')
    @section('content')

        <section id="ekyc-info">
            <div class="container box-shadow-all br" id="ekyc-container">
                <p id="back" class="text-left"><span class="text-left" id="go-back"><i class="material-icons">keyboard_arrow_left</i>Back</span></p>
                <div class = "col-lg-12 col-md-12 col-sm-12" id="part1">
                    <div class="row">
                        <div class = "col-lg-12 col-md-12 col-sm-12">
                            <div id="header-container">
                                <p class="text-center" id="ekyc-header">E-KYC Verification</p>
                                <p class="text-center ekyc-sub-header">The Verification process will be done via your webcam.</p>
                                <p class="text-center ekyc-sub-header">Please Keep these documents ready.</p>
                            </div>
                        </div>
                        <div class = "col-lg-12 col-md-12 col-sm-12" id="card-container">
                            <div id="pan-holder" class="card-holder">
                                <img src="icons/pancard.png" class="center-block">
                                <p class="text-center card-header">Pan Card</p>
                                <p class="text-center card-info">Issued by Income Tax Department</p>
                            </div>
                            <div id="poa-holder" class="card-holder">
                                <img src="icons/poa.png" class="center-block">
                                <p class="text-center card-header">Proof of Address</p>
                                <p class="text-center card-info">Passport,Driving Licence or Aadhar</p>
                            </div>
                            <a id="sample-video">Watch Sample Video</a>
                            <a href="#" id="ekyc-start" class="btn btn-primary">Get Started<i class="material-icons">videocam</i></a>
                        </div>
                    </div>
                </div>
                <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="part2">
                        <p class="text-center" id="ekyc-part2-header">E-KYC Verification</p>
                        <p class="text-center" id="ekyc-info-text">Documents to be shown - PAN Card and Proof of Address.</p>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero"> 
                          <video id="preview"></video>

                        </div>
                        

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="cam-button">  
                                                  
                            <button id="stop" disabled class="btn btn-black is-black">Preview<i class="material-icons">history</i></button>
                            <button id="record" class="btn btn-black is-black">Record<i class="material-icons">videocam</i></button>
                            <button id="stopV" class="btn btn-black is-black">Stop</button>
                            <button id="submit" disabled class="btn btn-black is-black">Submit<i class="material-icons">file_upload</i></button>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="timer-holder">
                            <span id="timer" class="pull-left">20</span>
                        </div>


                </div>
                <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="part3">
                        <p class="text-center" id="ekyc-part2-header">Sample Video</p>
                        <p class="text-center" id="ekyc-info-text">Documents to be shown - PAN Card and Proof of Address.</p>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero"> 
                            <video controls="true" id="sample-video">
                              <source src="{{'assets/sample_video/sample_video.mp4'}}" type="video/mp4">
                            </video>
                        </div>
                </div>
            </div>
        </section>

        @endsection

                <!-- Modal -->
        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header">Modal Header</h4>
              </div>
              <div class="modal-body" id="modal-body">
                
              </div>
            </div>

          </div>
        </div>

     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="js/jquery.donut.js"></script>

     <script type="text/javascript">
         $(document).ready(function(){
            $('#ekyc-start').on('click',function(){
                      !window.stream && navigator.getUserMedia({
                          audio: !0,
                          video: !0,
                      }, function(stream) {
                          window.stream = stream;
                          onstream()
                      }, function(error) {});
                      window.stream && onstream();

                function onstream() {
                    preview.src = window.URL.createObjectURL(stream);
                    preview.play();
                    preview.muted = !0;
                    recordAudio = RecordRTC(stream, {
                        type: 'audio',
                        recorderType: StereoAudioRecorder,
                        bufferSize: 16384,
                        onAudioProcessStarted: function() {
                        }
                    });
                }

                $('#part1').hide("slide", { direction: "down" }, 50,function(){
                    //$('#part1').children().fadeOut();
                    $('#part2').show('slide',{direction: "down"},150,function(){
                        $('#ekyc-container').css({'width':'600px','padding':'0px'});
                    });    
                });
                
            });

            $('#go-back').on('click',function(){
                // window.stream.stop();
                if ($('#part2').is(':visible')) {
                    
                    var css = {'width':'1170px','padding':'0px'};
                    slideAnimation('#part2','#part1',200,150,css)
                }
                if ($('#part3').is(':visible')) {
                    
                    var css = {'width':'1170px','padding':'0px'};
                    slideAnimation('#part3','#part1',200,150,css)
                }
                $('#ekyc-container').css({'width':'1170px','padding':'0px'});
            });

            $('#sample-video').on('click',function(){
                var css = {'width':'600px','padding':'0px'};
                slideAnimation('#part1','#part3',50,150,css)
            });

            function slideAnimation(hideElement,showElement,hideDur,showDur,css){
                $(hideElement).hide("slide", { direction: "down" }, hideDur,function(){
                    $(showElement).show('slide',{direction: "down"},showDur,function(){
                        $('#ekyc-container').css(css);
                    });    
                });
            }
         });
     </script>
     <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script>
  <script src="js/version2/loader.js?v=1.1"></script>
  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>
  <script src="{{url('js/version2/video_record.js?v=1.1')}}"></script>
  <script src="{{url('js/ckyc.js?v=1.1')}}"></script>
  <script type="text/javascript">
      function redirectHome(){
        window.location.href = "/account_activation";
      }
  </script>
    </body>
</html>
