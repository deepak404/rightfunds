<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Fund Selector</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{url('css/version2/footer.css?v=1.1')}}">
        <link rel="stylesheet" href="{{url('css/version2/jqueryui.css?v=1.1')}}">
        <link rel="stylesheet" href="{{url('css/version2/modal.css?v=1.1')}}">
        <link rel="stylesheet" href="{{url('css/version2/navbar-responsive.css?v=1.1')}}">

        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/version2/font-and-global.css?v=1.1')}}">
        <link rel="stylesheet" href="{{url('css/version2/fund-selector.css?v=1.1')}}">
        <link rel="stylesheet" href="{{url('css/version2/fund-selector-responsive.css?v=1.1')}}">
        <link rel="stylesheet" href="{{url('css/version2/notifications.css?v=1.1')}}">
        <!-- <link rel="stylesheet" href="css/loader.css"> -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
      <!-- <link rel="stylesheet" href="css/version2/withdraw-details.css"> -->
    </head>
    <body>
    <div class="loader" id="loader"></div>

    @extends('layouts.navbar')
    @section('content')

        <form id="invest-form">
        {{csrf_field()}}
            <section id="inv-details-section">
                <div class="container box-shadow-all br">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-md-12 col-xs-12" id="section-container">
                            <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12" id="left-fund-container">
                                
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero border-right">
                                        <input type = "text" placeholder = "" id = "inv-amount" class = "input-field num-field form-num" required>
                                        <label>Investment Amount</label>
                                        <p id="minimum-text">Minimum Investment :<span id="minimum-amount"> 5000</span></p>

                                        <select id="port-type" style="display: none;">
                                            <option value="Conservative">Conservative</option>
                                            <option value="Moderate">Moderate</option>
                                            <option value="Aggressive">Aggressive</option>
                                            <option value="Tax Saver">Tax Saver</option>
                                            <option value="own">Custom</option>
                                        </select>
                                        <div class="dropdown">
                                          <button class="btn btn-primary dropdown-toggle portfolio-menu" type="button" data-toggle="dropdown">Select Portfolio<span><i id="port-type-icon" class="material-icons">keyboard_arrow_down</i></span>
                                          </button>
                                          <ul class="dropdown-menu">
                                            <li><a href="#" class="port-select">Conservative</a></li>
                                            <li><a href="#" class="port-select">Moderate</a></li>
                                            <li><a href="#" class="port-select">Aggressive</a></li>
                                            <li><a href="#" class="port-select">Tax Saver</a></li>
                                            <li><a href="#" class="port-select">Custom</a></li>
                                          </ul>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero">
                                    <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                                        <input type="radio" id="onetime" name="investment-type" value="onetime">
                                        <label for="onetime"><i class="material-icons">radio_button_unchecked</i></label>
                                    </div>
                                    <div class = "col-lg-10 col-md-10 col-sm-10 col-xs-10 p-l-zero">
                                        <p class="inv-type-text">Invest Now </p>
                                        <input type="text" name="future" id="one-sel-date" placeholder="Select Date">
                                    </div>                                
                                </div> <!-- form-group ends -->
                                <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero">
                                    <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                                        <input type="radio" id="future" name="investment-type" value="future">
                                        <label for="future"><i class="material-icons">radio_button_unchecked</i></label>
                                    </div>
                                    <div class = "col-lg-10 col-md-10 col-sm-10 col-xs-10 p-l-zero">
                                        <p class="inv-type-text">Schedule a Future Investment</p>
                                        <input type="text" name="future" id="sche-sel-date" placeholder="Select Date">
                                    </div>                                
                                </div> -->
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero">
                                    <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                                        <input type="radio" id="monthly" name="investment-type" value="monthly">
                                        <label for="monthly"><i class="material-icons">radio_button_unchecked</i></label>
                                    </div>
                                    <div class = "col-lg-10 col-md-10 col-sm-10 col-xs-10 p-l-zero">
                                        <p class="inv-type-text">Systematic investment plan (SIP)</p>
                                        <input type="text" name="future" id="sip-date" placeholder="Select Date">
                                       

                                        <div class="dropdown" id="sip-date-dropdown">
                                          <button class="btn btn-primary dropdown-toggle sip-date-menu" type="button" data-toggle="dropdown">Every Month<span><i id="sip-type-icon">keyboard_arrow_down</i></span>
                                          </button>
                                          <ul class="dropdown-menu">
                                            <li><a href="#" class="sip-date-select">5th</a></li>
                                            <li><a href="#" class="sip-date-select">10th</a></li>
                                            <li><a href="#" class="sip-date-select">15th</a></li>
                                            <li><a href="#" class="sip-date-select">20th</a></li>
                                            <li><a href="#" class="sip-date-select">25th</a></li>
                                          </ul>
                                        </div>

                                         <span id="to">for</span>

                                        <select id="sip-duration" style="display: none;">
                                          <option value="1">1 Year</option>
                                          <option value="3">3 Years</option>
                                          <option value="5">5 Years</option>
                                          <option value="10">10 Years</option>
                                          <option value="20">20 Years</option>
                                          <option value="25">25 Years</option>
                                          <option value="30">20 Years</option>
                                        </select>

                                        <div class="dropdown" id="sip-duration-dropdown">
                                          <button class="btn btn-primary dropdown-toggle sip-duration-menu" type="button" data-toggle="dropdown">Duration<span><i id="sip-type-icon">keyboard_arrow_down</i></span>
                                          </button>
                                          <ul class="dropdown-menu">
                                            <li><a href="#" class="sip-dur-select">1 Year</a></li>
                                            <li><a href="#" class="sip-dur-select">3 Years</a></li>
                                            <li><a href="#" class="sip-dur-select">5 Years</a></li>
                                            <li><a href="#" class="sip-dur-select">10 Years</a></li>
                                            <li><a href="#" class="sip-dur-select">20 Years</a></li>
                                            <li><a href="#" class="sip-dur-select">25 Years</a></li>
                                            <li><a href="#" class="sip-dur-select">30 Years</a></li>
                                          </ul>
                                        </div>
                                    </div>                                
                                </div> <!-- form-group ends -->
                            </div>   
                        </div>
                    </div>
                </div>
            </section>


            <section id="fund-details-section">
                <div class="container box-shadow-all br">
                    <div class="row">
                        <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero p-30 border-bottom">
                                <p id="portfolio-header">Select Portfolio</p>
                                <p id="portfolio-text"></p>
                            </div>


                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero funds-container border-bottom" id="eq-funds-container">
                                <p class="text-center fund-heading"><i class="material-icons arrow-down fund-type-arrow" id="eq-arrow" data-toggle = "collapse" href="#eq-collapse-container" >keyboard_arrow_down</i><span>Equity</span></p>
                            <!-- </div> -->


                                

                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero" id="eq-collapse-container">


                            @foreach($schemes as $scheme)
                                @if($scheme['scheme_type'] == 'eq')
                                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 scheme-container border-bottom" id="{{$scheme['scheme_code']}}">
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8 pl-10">
                                        <p class="inv-scheme-name">{{$scheme['scheme_name']}}</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <p class="inv-scheme-amount pull-right"><span></span></p>
                                    </div>
                                </div>
                                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero scheme-details-container">
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom">
                                        <p class="inner-header   ">Fund Details</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2  col-xs-6 p-lr-zero   ">
                                            <p class="detail-header">Fund Manager</p>
                                            <p class = "detail-content" id="fund-manager-name-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-6 p-lr-zero   ">
                                            <p class="detail-header">Type</p>
                                            <p class = "detail-content" id="fund-type-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-6 p-lr-zero   ">
                                            <p class="detail-header">Exit Load</p>
                                            <p class = "detail-content" id="exit-load-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-6 p-lr-zero   ">
                                            <p class="detail-header">Benchmark</p>
                                            <p class = "detail-content" id="benchmark-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-6 p-lr-zero   ">
                                            <p class="detail-header">Asset Size(Cr)</p>
                                            <p class = "detail-content" id="asset-size-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-6 p-lr-zero   ">
                                            <p class="detail-header">Launch Date</p>
                                            <p class = "detail-content" id="launch-date-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot active-duration-holder">
                                       <div class="col-lg-2 col-md-2 col-sm-2  w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center active-duration" data-scode = "{{$scheme['scheme_code']}}">3 Month</p>
                                      </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">6 Months</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Year</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">3 Years</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">5 Years</p>
                                       </div>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                                       <div id="{{$scheme['scheme_code']}}-graph-container">
                                         
                                       </div>
                                     </div>
                                     <div class = "col-lg-12 col-md-12 col-sm-12 col-xs 12 p-lr-30  ">
                                        <p class="nav-det-container">
                                            <span class="inner-header">Nav</span>
                                            <span class="scheme-nav" id="scheme-nav-{{$scheme['scheme_code']}}"></span>
                                            <span class="green scheme-change"></span>
                                            <span class="scheme-perc green"></span>
                                        </p>
                                        <a class="sid-a-tag" href="/sid/{{$scheme['scheme_code']}}.pdf" download>
                                            <i class="material-icons" id="sid-icon">file_download</i>
                                            <span id="sid-span">SID</span>
                                        </a>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom border-top">
                                        <p class="inner-header   ">Absolute Returns</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-4 p-lr-zero   ">
                                            <p class="detail-header">1 Month</p>
                                            <p class = "return-content" id="one-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-4 p-lr-zero   ">
                                            <p class="detail-header">3 Month</p>
                                            <p class = "return-content" id="three-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-4 p-lr-zero   ">
                                            <p class="detail-header">6 Month</p>
                                            <p class = "return-content" id="six-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-4 p-lr-zero   ">
                                            <p class="detail-header">1 Year</p>
                                            <p class = "return-content" id="one-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-4 p-lr-zero   ">
                                            <p class="detail-header">3 Year</p>
                                            <p class = "return-content" id="three-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-4 p-lr-zero   ">
                                            <p class="detail-header">5 Year</p>
                                            <p class = "return-content" id="five-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div> <!-- equity collapse container ends -->
                                
                            </div> <!-- Equity funds container ends -->

                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero funds-container border-bottom" id="bal-funds-container">
                                <p class="text-center fund-heading"><i class="material-icons arrow-down fund-type-arrow" id="bal-arrow" data-toggle = "collapse" href="#bal-collapse-container" >keyboard_arrow_down</i><span>Balanced</span></p>


                                

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero collapse" id="bal-collapse-container">
                            @foreach($schemes as $scheme)
                                @if($scheme['scheme_type'] == 'bal')
                                <div class = "col-lg-12 col-md-12 col-sm-12 scheme-container border-bottom" id="{{$scheme['scheme_code']}}">
                                    <div class = "col-lg-8 col-md-8 col-sm-8 pl-10">
                                        <p class="inv-scheme-name">{{$scheme['scheme_name']}}</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4">
                                        <p class="inv-scheme-amount pull-right"><span></span></p>
                                    </div>
                                </div>
                                <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero scheme-details-container">
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 border-bottom">
                                        <p class="inner-header">Fund Details</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Fund Manager</p>
                                            <p class = "detail-content" id="fund-manager-name-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Type</p>
                                            <p class = "detail-content" id="fund-type-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Exit Load</p>
                                            <p class = "detail-content" id="exit-load-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Benchmark</p>
                                            <p class = "detail-content" id="benchmark-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Asset Size(Cr)</p>
                                            <p class = "detail-content" id="asset-size-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Launch Date</p>
                                            <p class = "detail-content" id="launch-date-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bot active-duration-holder">
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center active-duration" data-scode = "{{$scheme['scheme_code']}}">3 Month</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">6 Months</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Year</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">3 Years</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">5 Years</p>
                                       </div>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                                       <div id="{{$scheme['scheme_code']}}-graph-container">
                                         
                                       </div>
                                     </div>
                                     <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 ">
                                        <p class="nav-det-container">
                                            <span class="inner-header">Nav</span>
                                            <span class="scheme-nav" id="scheme-nav-{{$scheme['scheme_code']}}"></span>
                                            <span class="green scheme-change"></span>
                                            <span class="scheme-perc green"></span>
                                        </p>
                                        <a class="sid-a-tag" href="/sid/{{$scheme['scheme_code']}}.pdf" download>
                                            <i class="material-icons" id="sid-icon">file_download</i>
                                            <span id="sid-span">SID</span>
                                        </a>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 border-bottom border-top">
                                        <p class="inner-header">Absolute Returns</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">1 Month</p>
                                            <p class = "return-content" id="one-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">3 Month</p>
                                            <p class = "return-content" id="three-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">6 Month</p>
                                            <p class = "return-content" id="six-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">1 Year</p>
                                            <p class = "return-content" id="one-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">3 Year</p>
                                            <p class = "return-content" id="three-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">5 Year</p>
                                            <p class = "return-content" id="five-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div> <!-- bal collapse container ends -->

                            </div> <!-- Balanced funds container ends -->


                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero funds-container border-bottom" id="debt-funds-container">

                                <p class="text-center fund-heading"><i class="material-icons arrow-down fund-type-arrow" id="debt-arrow" data-toggle = "collapse" href="#debt-collapse-container" >keyboard_arrow_down</i><span>Debt</span></p>


                                

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero collapse" id="debt-collapse-container">
                            @foreach($schemes as $scheme)
                                @if($scheme['scheme_type'] == 'debt')
                                <div class = "col-lg-12 col-md-12 col-sm-12 scheme-container border-bottom" id="{{$scheme['scheme_code']}}">
                                    <div class = "col-lg-8 col-md-8 col-sm-8 pl-10">
                                        <p class="inv-scheme-name">{{$scheme['scheme_name']}}</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4">
                                        <p class="inv-scheme-amount pull-right"><span></span></p>
                                    </div>
                                </div>
                                <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero scheme-details-container">
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 border-bottom">
                                        <p class="inner-header">Fund Details</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Fund Manager</p>
                                            <p class = "detail-content" id="fund-manager-name-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Type</p>
                                            <p class = "detail-content" id="fund-type-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Exit Load</p>
                                            <p class = "detail-content" id="exit-load-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Benchmark</p>
                                            <p class = "detail-content" id="benchmark-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Asset Size(Cr)</p>
                                            <p class = "detail-content" id="asset-size-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Launch Date</p>
                                            <p class = "detail-content" id="launch-date-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bot active-duration-holder">
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center active-duration" data-scode = "{{$scheme['scheme_code']}}">3 Month</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">6 Months</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Year</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">3 Years</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">5 Years</p>
                                       </div>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                                       <div id="{{$scheme['scheme_code']}}-graph-container">
                                         
                                       </div>
                                     </div>
                                     <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 ">
                                        <p class="nav-det-container">
                                            <span class="inner-header">Nav</span>
                                            <span class="scheme-nav" id="scheme-nav-{{$scheme['scheme_code']}}"></span>
                                            <span class="green scheme-change"></span>
                                            <span class="scheme-perc green"></span>
                                        </p>
                                        <a class="sid-a-tag" href="/sid/{{$scheme['scheme_code']}}.pdf" download>
                                            <i class="material-icons" id="sid-icon">file_download</i>
                                            <span id="sid-span">SID</span>
                                        </a>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 border-bottom border-top">
                                        <p class="inner-header">Absolute Returns</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">1 Month</p>
                                            <p class = "return-content" id="one-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">3 Month</p>
                                            <p class = "return-content" id="three-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">6 Month</p>
                                            <p class = "return-content" id="six-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">1 Year</p>
                                            <p class = "return-content" id="one-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">3 Year</p>
                                            <p class = "return-content" id="three-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">5 Year</p>
                                            <p class = "return-content" id="five-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div> <!-- debt collapse container ends -->
                                

                            </div> <!-- debt funds container ends -->

                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero funds-container border-bottom" id="ts-funds-container">
                                <p class="text-center fund-heading"><i class="material-icons arrow-down fund-type-arrow" id="ts-arrow" data-toggle = "collapse" href="#ts-collapse-container" >keyboard_arrow_down</i><span>Tax Saver</span></p>
                                
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero collapse" id="ts-collapse-container">
                            @foreach($schemes as $scheme)
                                @if($scheme['scheme_type'] == 'ts')
                                <div class = "col-lg-12 col-md-12 col-sm-12 scheme-container border-bottom" id="{{$scheme['scheme_code']}}">
                                    <div class = "col-lg-8 col-md-8 col-sm-8 pl-10">
                                        <p class="inv-scheme-name">{{$scheme['scheme_name']}}</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4">
                                        <p class="inv-scheme-amount pull-right"><span></span></p>
                                    </div>
                                </div>
                                <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero scheme-details-container">
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 border-bottom">
                                        <p class="inner-header">Fund Details</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Fund Manager</p>
                                            <p class = "detail-content" id="fund-manager-name-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Type</p>
                                            <p class = "detail-content" id="fund-type-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Exit Load</p>
                                            <p class = "detail-content" id="exit-load-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Benchmark</p>
                                            <p class = "detail-content" id="benchmark-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Asset Size(Cr)</p>
                                            <p class = "detail-content" id="asset-size-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">Launch Date</p>
                                            <p class = "detail-content" id="launch-date-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bot active-duration-holder">
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center active-duration" data-scode = "{{$scheme['scheme_code']}}">3 Month</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">6 Months</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Year</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">3 Years</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">5 Years</p>
                                       </div>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                                       <div id="{{$scheme['scheme_code']}}-graph-container">
                                         
                                       </div>
                                     </div>
                                     <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 ">
                                        <p class="nav-det-container">
                                            <span class="inner-header">Nav</span>
                                            <span class="scheme-nav" id="scheme-nav-{{$scheme['scheme_code']}}"></span>
                                            <span class="green scheme-change"></span>
                                            <span class="scheme-perc green"></span>
                                        </p>
                                        <a class="sid-a-tag" href="/sid/{{$scheme['scheme_code']}}.pdf" download>
                                            <i class="material-icons" id="sid-icon">file_download</i>
                                            <span id="sid-span">SID</span>
                                        </a>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-30 border-bottom border-top">
                                        <p class="inner-header">Absolute Returns</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">1 Month</p>
                                            <p class = "return-content" id="one-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">3 Month</p>
                                            <p class = "return-content" id="three-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">6 Month</p>
                                            <p class = "return-content" id="six-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">1 Year</p>
                                            <p class = "return-content" id="one-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">3 Year</p>
                                            <p class = "return-content" id="three-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                            <p class="detail-header">5 Year</p>
                                            <p class = "return-content" id="five-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div> <!-- ts collapse container -->
                                

                                
                                <!-- <div class = "col-lg-12 col-md-12 col-sm-12 scheme-container border-bottom p-lr-zero">
                                    <div class = "col-lg-8 col-md-8 col-sm-8">
                                        <p class="inv-scheme-name">Reliance Regular Savings Fund - Balanced Option  - Direct Plan Growth Plan</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4">
                                        <p class="inv-scheme-amount pull-right"><span>Rs. </span>5000000</p>
                                    </div>
                                </div>     -->
                            </div> <!-- Tax Saver funds container ends -->


                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 total-container border-bottom p-lr-zero">
                                <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8 p-lr-zero">
                                    <p class="total-text blue">Total Investment Amount</p>
                                </div>
                                <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4 p-lr-zero">
                                    <p class="inv-scheme-amount pull-right" id="total-inv-amount">Rs. <span></span></p>
                                </div>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 total-container border-bottom p-lr-zero">
                                <input type="submit" name="submit" value="Invest Now" id = "invest-btn" class="btn btn-primary center-block">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>

        @endsection()



        <!-- Modal -->
        <div id="infoModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header"></h4>
              </div>
              <div class="modal-body" id="modal-body">
                
              </div>
            </div>

          </div>
        </div>

        <!-- Modal -->
        <div id="showInfo" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header">Payment Link Info</h4>
              </div>
              <div class="modal-body" id="modal-body">
                <p class = "text-center general-info">Payment Link will be sent to your email. Kindly make the payment with in 24 Hours.</p>
                <button type = "button" class = "btn btn-primary center-block popup-btn" id="mandate-pay" data-dismiss="modal">Okay</button>
              </div>
            </div>

          </div>
        </div>


        @if(isset($updated_portfolio))
        <!-- Modal -->
        <div id="showPaymentInfo" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close close-pn">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header">Payment Summary</h4>
              </div>
              <div class="modal-body" id="modal-body">
                <table class="table payment-table">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Scheme Name</th>
                            <th>Payment Status</th>
                            <th>Amount</th>   
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 1; 
                            $scheme_name = App\SchemeDetails::pluck('scheme_name','scheme_code')->toArray();
                        ?>
                        @foreach($updated_portfolio as $portfolio)
                        <?php $name = $scheme_name[$portfolio['scheme_code']] ?>
                        <tr>
                            <td><p>{{$count}}.</p></td>
                            <td><p class="scheme-name">{{$name}}</p></td>
                            @if($portfolio['bse_payment_status'] == "apr")
                            <td><p class="payment-status">Payment Approved</p></td>
                            @elseif($portfolio['bse_payment_status'] == "rej")
                            <td><p class="payment-status">Payment Rejected</p></td>
                            @elseif($portfolio['bse_payment_status'] == "afc")
                            <td><p class="payment-status">Awaiting Funds Confirmation</p></td>
                            @elseif($portfolio['bse_payment_status'] == "pni")
                            <td><p class="payment-status">Payment Not Initiated Yet.</p></td>
                            @else
                            <td><p class="payment-status">NA</p></td>
                            @endif
                            
                            <td><p class="order-amount">{{$portfolio['amount_invested']}}</p></td>
                        </tr>
                        <?php  $count++; ?>
                        @endforeach
                    </tbody>
                </table>
                <button type = "button" class = "btn btn-primary center-block popup-btn close-pn" id="close-pn">Okay</button>
              </div>
            </div>

          </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#showPaymentInfo').modal('show');
            });
        </script>
        @endif

        
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js?v=1.1"></script>
        <script type="text/javascript" src = "js/version2/fund-selector.js?v=1.3"></script>
        <script type="text/javascript" src = "js/version2/fund-graph.js?v=1.1"></script>
        <script type="text/javascript" src = "js/version2/fund-date.js?v=1.2"></script>
        <script type="text/javascript" src = "js/version2/custom-amount.js?v=1.1"></script>
        <script type="text/javascript" src = "js/version2/validation.js?v=1.1"></script>
        <script type="text/javascript" src = "js/version2/loader.js?v=1.1"></script>
        <script type="text/javascript" src = "js/jquery-ui.js?v=1.1"></script>

        <script type="text/javascript">

            function redirect(id){
                // window.location.href = '/make_payment/'+id+'';
                if (id != null) {
                        window.open('https://www.rightfunds.com/make_payment/'+id+'','_blank');    
                }else{
                    alert('Sorry. Something went wrong');
                }
                
            }

            function redirectHome(){
                window.location.href = '/home';
            }

            function infoPopUp(){
              $('#showInfo').modal('show');
            }

            function getMandatelink(){
            $('#infoPopUp').modal('hide');
            $.ajax({
              type: "GET",
              url: "/get_mandate_link",
              async:false,
              success: function(data){
                if (data.msg == 1) {
                    window.location.replace("http://www.rightfunds.com");
                    var win = window.open(data.mandate_url, '_blank');
                    win.focus();
                }else if (data.msg == 2){
                  infoPopUp()
                }
              },
              error: function(xhr,status,error){
                alert(error);
              }
            });
          }

        </script>

    </body>
</html>
