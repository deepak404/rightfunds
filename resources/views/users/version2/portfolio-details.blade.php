<?php 
        $fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
 ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Portfolio Details</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/version2/portfolio-details.css?v=1.1">
        <link rel="stylesheet" href="css/version2/acc-statement.css?v=1.1">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/navbar-responsive.css?v=1.1">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <script src="js/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="css/version2/pd-responsive.css?v=1.1">
        <link rel="stylesheet" href="{{url('css/version2/navbar-responsive.css?v=1.1')}}">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

    @extends('layouts.navbar')
    @section('content')

        @if(\Auth::user()->nachDetails->nach_status == 0)
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" id="alert-holder">
                        <p id="inactive-parent">
                            <span id="inactive-info"><i class="material-icons">error</i></span>
                            <span>Please Activate your account to start investing</span>
                            <a href="{{url('/account_activation')}}" class="pull-right"><i class="material-icons">arrow_forward</i></a></p>
                    </div>
                </div>
            </div>
        </section>
        @endif

        <section id="acc-statement-header">
            <div class="container">
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline text-center fl" id="acc-statement-header-ul">
                            <li class="active-acc-header"><a href="{{url('/portfolio_details')}}">Portfolio Details</a></li>
                            <li><a href="{{url('/investment_history')}}">Investment History</a>
                            <li><a href="{{url('/tax_saving')}}">Tax Saving Statement</a>
                            <li><a href="{{url('/withdraw_funds')}}">Withdraw Funds</a>
                        </ul>
                    </div>
                </div>  
            </div><!-- container ends -->
        </section>

        <section id="acct-info-section">    
            <div class="container"> 
                <div class="row">  

                    @if(count($portfolio_details) == 0)
                    @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="acct-info-container">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-6 p-lr-zero">
                            <p id="acct-info-statement">Here's how your statement looks as of today, <?php echo date('j M Y');?></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 p-lr-zero">
                            <a href="/get_user_portfolio_document" class="btn btn-primary" id="export-portfolio-btn">Export</a>
                        </div>
                    </div>
                    @endif

                    
                </div>  
            </div>  
        </section>

        <section  class="portfolio-table-section">
            <div class="container table-container" id="equity-funds-table-container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-border p-lr-zero">
                        <p class="table-container-header">Equity Funds</p>

                        @if(array_key_exists('eq',$portfolio_details))
                        <div class="acc-table-wrapper">
                            <table class="table ">
                                <thead>
                                  <tr>
                                    <th class="fund-name-header"><p class="table-header">Mutual Fund Scheme</p></th>
                                    <th class="folio-number-header"><p class="table-header">Folio Number</p></th>
                                    <th><p class="table-header">Amount Invested(Rs.)</p></th>
                                    <th><p class="table-header">Units Held</p></th>
                                    <th><p class="table-header">Current NAV(Rs.)</p></th>
                                    <th><p class="table-header">Current Value(Rs.)</p></th>
                                    <th><p class="table-header">Net Returns</p></th>
                                    <th><p class="table-header">Eligible as LTCG(Rs.)</p></th>
                                    <th><p class="table-header">Absolute Returns</p></th>
                                  </tr>
                                </thead>
                                <tbody>


                                  @foreach($portfolio_details['eq'] as $investment)
                                  @if(is_array($investment))
                                  <tr class="p-tb-five table-row">
                                    <td class="fund-name-content"><p class="table-content">{{$investment['scheme_name']}}</p></td>
                                    <td><p class="table-content">{{$investment['folio_number']}}</p></td>
                                    <td><p class="table-content">
                                        <?php 
                                            $whole = floor($investment['amount_invested']);
                                            $fraction = $investment['amount_invested'] - $whole;
                                            if ($fraction != 0.0 || $fraction != 0) {
                                                echo $fmt->format($investment['amount_invested']);
                                            }else {
                                                echo $fmt->format($investment['amount_invested']).".00";
                                            }
                                         ?>
                                        </p></td>
                                    <td><p class="table-content">{{$investment['units_held']}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['nav'])}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['current_value'])}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['net_return'])}}</p></td>
                                    <td><p class="table-content">{{$investment['ltcg']}}</p></td>
                                    <td><p class="table-content">{{$investment['xirr']}}</p></td>
                                  </tr>
                                  @endif
                                  @endforeach
                                  <tr class="p-tb-five  table-row">
                                    <td class="fund-name-content"><p class="total total-content-data">TOTAL</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">
                                        <?php
                                            $whole = floor($portfolio_details['eq']['total_amount_invested']);
                                            $fraction = $portfolio_details['eq']['total_amount_invested'] - $whole;
                                            if ($fraction != 0.0 || $fraction != 0) {
                                                echo $fmt->format($portfolio_details['eq']['total_amount_invested']);
                                            }else {
                                              echo $fmt->format($portfolio_details['eq']['total_amount_invested']).".00";
                                            }  
                                        ?>
                                        </p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['eq']['total_units_held']}}</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['eq']['total_current_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['eq']['total_net_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['eq']['total_ltcg']}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['eq']['total_xirr']}}</p></td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="no-port">No Investments in this portfolio yet</p>
                        @endif
                    </div>
                </div>  
            </div>  
        </section>  



        <section  class="portfolio-table-section">
            <div class="container table-container" id="debt-funds-table-container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-border p-lr-zero">
                        <p class="table-container-header">Debt Funds</p>
                        @if(array_key_exists('debt',$portfolio_details))
                        <div class="acc-table-wrapper">
                            <table class="table ">
                                <thead>
                                  <tr>
                                    <th class="fund-name-header"><p class="table-header">Mutual Fund Scheme</p></th>
                                    <th class="folio-number-header"><p class="table-header">Folio Number</p></th>
                                    <th><p class="table-header">Amount Invested(Rs.)</p></th>
                                    <th><p class="table-header">Units Held</p></th>
                                    <th><p class="table-header">Current NAV(Rs.)</p></th>
                                    <th><p class="table-header">Current Value(Rs.)</p></th>
                                    <th><p class="table-header">Net Returns</p></th>
                                    <th><p class="table-header">Eligible as LTCG(Rs.)</p></th>
                                    <th><p class="table-header">Absolute Returns</p></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($portfolio_details['debt'] as $investment)
                                  @if(is_array($investment))
                                  <tr class="p-tb-five table-row">
                                    <td class="fund-name-content"><p class="table-content">{{$investment['scheme_name']}}</p></td>
                                    <td><p class="table-content">{{$investment['folio_number']}}</p></td>
                                    <td><p class="table-content">
                                        <?php 
                                            $whole = floor($investment['amount_invested']);
                                            $fraction = $investment['amount_invested'] - $whole;
                                            if ($fraction != 0.0 || $fraction != 0) {
                                                echo $fmt->format($investment['amount_invested']);
                                            }else{
                                                echo $fmt->format($investment['amount_invested']).".00";
                                            }
                                         ?>
                                         </p></td>
                                    <td><p class="table-content">{{$investment['units_held']}}</p></td>
                                    <td><p class="table-content">{{$investment['nav']}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['current_value'])}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['net_return'])}}</p></td>
                                    <td><p class="table-content">{{$investment['ltcg']}}</p></td>
                                    <td><p class="table-content">{{$investment['xirr']}}</p></td>
                                  </tr>
                                  @endif
                                  @endforeach
                                  <tr class="p-tb-five  table-row" style="color: #0091EA;">
                                    <td class="fund-name-content"><p class="total total-content-data">TOTAL</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">
                                        <?php 
                                            $whole = floor($portfolio_details['debt']['total_amount_invested']);
                                            $fraction = $portfolio_details['debt']['total_amount_invested'] - $whole;
                                            if ($fraction != 0.0 || $fraction != 0) {
                                                echo $fmt->format($portfolio_details['debt']['total_amount_invested']);
                                            }else{
                                                echo $fmt->format($portfolio_details['debt']['total_amount_invested']).".00";
                                            }
                                         ?> 
                                     </p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['debt']['total_units_held']}}</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['debt']['total_current_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['debt']['total_net_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['debt']['total_ltcg']}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['debt']['total_xirr']}}</p></td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="no-port">No Investments in this portfolio yet</p>
                        @endif
                    </div>
                </div>  
            </div>  
        </section>  


        <section  class="portfolio-table-section">
            <div class="container table-container" id="balanced-funds-table-container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-border p-lr-zero">
                        <p class="table-container-header">Balanced Funds</p>
                        @if(array_key_exists('bal',$portfolio_details))
                        <div class="acc-table-wrapper">
                            <table class="table ">
                                <thead>
                                  <tr>
                                    <th class="fund-name-header"><p class="table-header">Mutual Fund Scheme</p></th>
                                    <th class="folio-number-header"><p class="table-header">Folio Number</p></th>
                                    <th><p class="table-header">Amount Invested(Rs.)</p></th>
                                    <th><p class="table-header">Units Held</p></th>
                                    <th><p class="table-header">Current NAV(Rs.)</p></th>
                                    <th><p class="table-header">Current Value(Rs.)</p></th>
                                    <th><p class="table-header">Net Returns</p></th>
                                    <th><p class="table-header">Eligible as LTCG(Rs.)</p></th>
                                    <th><p class="table-header">Absolute Returns</p></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($portfolio_details['bal'] as $investment)
                                  @if(is_array($investment))
                                  <tr class="p-tb-five table-row">
                                    <td class="fund-name-content"><p class="table-content">{{$investment['scheme_name']}}</p></td>
                                    <td><p class="table-content">{{$investment['folio_number']}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['amount_invested'])}}.00</p></td>
                                    <td><p class="table-content">{{$investment['units_held']}}</p></td>
                                    <td><p class="table-content">{{$investment['nav']}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['current_value'])}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['net_return'])}}</p></td>
                                    <td><p class="table-content">{{$investment['ltcg']}}</p></td>
                                    <td><p class="table-content">{{$investment['xirr']}}</p></td>
                                  </tr>
                                  @endif
                                  @endforeach
                                  <tr class="p-tb-five  table-row">
                                    <td class="fund-name-content"><p class="total total-content-data">TOTAL</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">
                                        <?php 
                                            $whole = floor($portfolio_details['bal']['total_amount_invested']);
                                            $fraction = $portfolio_details['bal']['total_amount_invested'] - $whole;
                                            if ($fraction != 0.0 || $fraction != 0) {
                                                echo $fmt->format($portfolio_details['bal']['total_amount_invested']);
                                            }else{
                                                echo $fmt->format($portfolio_details['bal']['total_amount_invested']).".00";
                                            }
                                         ?>
                                         </p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['bal']['total_units_held']}}</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['bal']['total_current_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['bal']['total_net_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['bal']['total_ltcg']}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['bal']['total_xirr']}}</p></td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="no-port">No Investments in this portfolio yet</p>
                        @endif
                    </div>
                </div>  
            </div>  
        </section>  


        <section  class="portfolio-table-section">
            <div class="container table-container" id="elss-funds-table-container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-border p-lr-zero">
                        <p class="table-container-header">ELSS Funds</p>
                        @if(array_key_exists('ts',$portfolio_details))
                        <div class="acc-table-wrapper">
                            <table class="table ">
                                <thead>
                                  <tr>
                                    <th class="fund-name-header"><p class="table-header">Mutual Fund Scheme</p></th>
                                    <th class="folio-number-header"><p class="table-header">Folio Number</p></th>
                                    <th><p class="table-header">Amount Invested(Rs.)</p></th>
                                    <th><p class="table-header">Units Held</p></th>
                                    <th><p class="table-header">Current NAV(Rs.)</p></th>
                                    <th><p class="table-header">Current Value(Rs.)</p></th>
                                    <th><p class="table-header">Net Returns</p></th>
                                    <th><p class="table-header">Eligible as LTCG(Rs.)</p></th>
                                    <th><p class="table-header">Absolute Returns</p></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($portfolio_details['ts'] as $investment)
                                  @if(is_array($investment))
                                  <tr class="p-tb-five table-row">
                                    <td class="fund-name-content"><p class="table-content">{{$investment['scheme_name']}}</p></td>
                                    <td><p class="table-content">{{$investment['folio_number']}}</p></td>
                                    <td><p class="table-content">
                                        <?php 
                                            $whole = floor($investment['amount_invested']);
                                            $fraction = $investment['amount_invested'] - $whole;
                                            if ($fraction != 0.0 || $fraction != 0) {
                                                 echo $fmt->format($investment['amount_invested']);
                                            }else{
                                                echo $fmt->format($investment['amount_invested']).".00";
                                            }
                                        ?>
                                        </p></td>
                                    <td><p class="table-content">{{$investment['units_held']}}</p></td>
                                    <td><p class="table-content">{{$investment['nav']}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['current_value'])}}</p></td>
                                    <td><p class="table-content">{{$fmt->format($investment['net_return'])}}</p></td>
                                    <td><p class="table-content">{{$investment['ltcg']}}</p></td>
                                    <td><p class="table-content">{{$investment['xirr']}}</p></td>
                                  </tr>
                                  @endif
                                  @endforeach
                                  <tr class="p-tb-five  table-row">
                                    <td class="fund-name-content"><p class="total total-content-data">TOTAL</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">
                                        <?php 
                                            $whole = floor($portfolio_details['ts']['total_amount_invested']);
                                            $fraction = $portfolio_details['ts']['total_amount_invested'] - $whole;
                                            if ($fraction == 0.0 || $fraction == 0) {
                                                echo $fmt->format($portfolio_details['ts']['total_amount_invested']);
                                            }else{
                                                echo $fmt->format($portfolio_details['ts']['total_amount_invested']).".00";
                                            }
                                         ?>
                                         </p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['ts']['total_units_held']}}</p></td>
                                    <td><p class="table-content total-content-data"></p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['ts']['total_current_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$fmt->format($portfolio_details['ts']['total_net_value'])}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['ts']['total_ltcg']}}</p></td>
                                    <td><p class="table-content total-content-data">{{$portfolio_details['ts']['total_xirr']}}</p></td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="no-port">No Investments in this portfolio yet</p>
                        @endif
                    </div>
                </div>  
            </div>  
        </section>  


        @endsection



    </body>
</html>
