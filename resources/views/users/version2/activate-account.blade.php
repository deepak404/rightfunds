<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Activate Account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/version2/activate-account.css?v=1.1">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/jqueryui.css?v=1.1">
        <link rel="stylesheet" href="css/version2/modal.css?v=1.1">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">
        <link rel="stylesheet" href="css/version2/navbar-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/activate-acc-responsive.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="css/version2/activate-account.css?v=1.1">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>



    @extends('layouts.navbar')
    @section('content')

        <section id="settings">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 p-r-35 p-l-zero" id="sidebar-container">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 border-all br p-lr-zero" id="settings-sidebar">
                                <ul id="settings-nav">
                                    <li class="border-bottom active-settings-nav activate-acc-menu">
                                        @if(\Auth::user()->p_check == "1")
                                        <span><i class="material-icons activate-tick">done</i></span>
                                        @else
                                        <span><i class="material-icons non-active-tick">done</i></span>
                                        @endif
                                        <a href="#" class="blue-text">Personal Details</a>
                                    </li>
                                    <?php $cc_status = \Auth::user()->nachDetails->cc_status ;
                                          $mandate_status = \Auth::user()->nachDetails->mandatesoft_status;
                                    ?>
                                    @if($cc_status == 2 || $mandate_status == 2)
                                    <li class="border-bottom inactive-sidemenu activate-acc-menu">
                                        <span><i class="material-icons non-active-tick">done</i></span>
                                        <a href="#">Bank Details</a>
                                    </li>
                                    @elseif($cc_status == 1 && $mandate_status == 1)
                                    <li class="border-bottom inactive-sidemenu activate-acc-menu">
                                        @if(\Auth::user()->b_check == 1)
                                        <span><i class="material-icons activate-tick">done</i></span>
                                        @else
                                        <span><i class="material-icons non-active-tick">done</i></span>
                                        @endif
                                        <a href="#">Bank Details</a>
                                    </li>
                                    @else
                                    <li class="border-bottom inactive-sidemenu activate-acc-menu">
                                        <span><i class="material-icons non-active-tick">done</i></span>
                                        <a href="#">Bank Details</a>
                                    </li>
                                    @endif
                                    

                                    <?php 
                                        $video_link = \Auth::user()->ekyc->video_link;
                                         $ekycvideo_status = \Auth::user()->nachDetails->kycvideo_status
                                     ?>

                                    @if(\Auth::user()->nachDetails->pan_status == NULL)
                                        <?php // The below code is show if the pan is not verified by the admin yet.?>
                                        <li class="inactive-sidemenu inactive-menu">
                                            <span><i class="material-icons non-active-tick">done</i></span>
                                            <a href="#">EKYC and Documents</a>
                                            <p href="#" id="kyc-info-text">Will be notified once your PAN is verified</a>
                                        </li>
                                    @elseif(\Auth::user()->nachDetails->pan_status == 1)
                                        <?php // The below code is show if the User is already a KYC Compliant.?>
                                        <li class="inactive-sidemenu inactive-menu">
                                            <span><i class="material-icons activate-tick">done</i></span>
                                            <a href="#">EKYC and Documents</a>
                                            <p href="#" id="kyc-info-text">You are a KYC Compliant</a>
                                        </li>
                                    @elseif(\Auth::user()->nachDetails->pan_status == 2)
                                        <?php // The below code is show if the pan is not KYC verified?>
                                        @if($video_link == '' && $ekycvideo_status == '')
                                        <li class="inactive-sidemenu activate-acc-menu">
                                            <span><i class="material-icons non-active-tick">done</i></span>
                                            <a href="#">EKYC and Documents</a>
                                            <p class="text-left blue-text" id="kyc-info-text">You are not a KYC Compliant. Start your EKYC</p>
                                        </li>
                                        @elseif($video_link != '' && $ekycvideo_status == '1')
                                        <li class="inactive-sidemenu activate-acc-menu">
                                            <span><i class="material-icons activate-tick">done</i></span>
                                            <a href="#">EKYC and Documents</a>
                                            <p class="text-left blue-text" id="kyc-info-text">Your KYC video is verified. Download the KYC Form.</p>
                                        </li>
                                        @elseif($video_link != '' && $ekycvideo_status == '2')
                                        <li class="inactive-sidemenu activate-acc-menu">
                                            <span><i class="material-icons non-active-tick">done</i></span>
                                            <a href="#">EKYC and Documents</a>
                                            <p class="text-left blue-text" id="kyc-info-text">Your KYC Video is rejected. Start your EKYC</p>
                                        </li>
                                        @elseif($video_link != '' && $ekycvideo_status == '')
                                        <li class="inactive-sidemenu activate-acc-menu">
                                            <span><i class="material-icons activate-tick">done</i></span>
                                            <a href="#">EKYC and Documents</a>
                                            <p href="#" id="kyc-info-text">Your Video is yet to be verified.</a>
                                        </li>
                                        @endif
                                    
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 border-all br p-lr-zero">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bottom header-pad">
                                <p class="section-header pl-30">Personal Details <button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button></p>
                                <!-- <p id="bank-details-info">To Edit Bank Details Please Contact <span class="blue-text">88258 88200</span> or <span class="blue-text">contact@rightfunds.com</span></p> -->
                            </div>
                            @if(\Auth::user()->p_check == "1")
                            @foreach($personal_details as $personal_detail)
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="personal-details-tab">
                                <form id="personal-details-form" class="acc-activate-forms">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "name" id="name" class="input-field text-field" value="{{$personal_detail['f_name']}}">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Name</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pan" id="pan" class="input-field" value="{{$personal_detail['pan']}}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">PAN</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "dob" id="dob" class="input-field" value="{{$personal_detail['dob']}}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Date of Birth</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "fathers-name" id="fathers-name" class="input-field text-field" value="{{$personal_detail['f_s_name']}}">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Father's/Spouse Name</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "mothers-name" id="mothers-name" class="input-field text-field" value="{{$personal_detail['mothers_name']}}">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Mother's Name</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <select name="occupation" id="occupation">
                                                    @if($personal_detail['occ_type'] == 'service')
                                                    <option value="service" selected>Service</option> 
                                                    @else
                                                    <option value="service">Service</option> 
                                                    @endif
                                                    @if($personal_detail['occ_type'] == 'others')
                                                    <option value="others" selected>Others</option>
                                                    @else
                                                    <option value="others">Others</option>
                                                    @endif                                            
                                                    @if($personal_detail['occ_type'] == 'private')
                                                    <option value="private" selected>Private Sector</option>
                                                    @else
                                                    <option value="private">Private Sector</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'professional')
                                                    <option value="professional" selected>Professional</option>
                                                    @else
                                                    <option value="professional">Professional</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'business')
                                                    <option value="business" selected>Business</option>
                                                    @else
                                                    <option value="business">Business</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'public')
                                                    <option value="public" selected>Public Sector</option>
                                                    @else
                                                    <option value="public">Public Sector</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'government')
                                                    <option value="government" selected>Government Sector</option>
                                                    @else
                                                    <option value="government">Government Sector</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'self')
                                                    <option value="self" selected>Self Employed</option>
                                                    @else
                                                    <option value="self">Self Employed</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'retired')
                                                    <option value="retired" selected>Retired</option>
                                                    @else
                                                    <option value="retired">Retired</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'housewife')
                                                    <option value="housewife" selected>Housewife</option>
                                                    @else
                                                    <option value="housewife">Housewife</option>
                                                    @endif                                                    
                                                    @if($personal_detail['occ_type'] == 'student')
                                                    <option value="student" selected>Student</option>
                                                    @else
                                                    <option value="student">Student</option>
                                                    @endif

                                                    @if($personal_detail['occ_type'] == 'not-categorized')
                                                    <option value="not-categorized" selected>Not Categorized</option>
                                                    @else
                                                    <option value="not-categorized">Not Categorized</option>
                                                    @endif
                                                </select>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Sex</p>
                                            <div class="sex-radio-divs">
                                              <div class = " male-radio-div padding-lr-zero radio-inline">

                                              @if($personal_detail['sex'] == "male")
                                                <input type="radio"  name="gender" id="male" value = "male" checked>
                                              @else
                                                <input type="radio"  name="gender" id="male" value = "male">
                                              @endif
                                                <label for="male" class="radio-label"><span class="radio left-label-span">Male</span></label>
                                              </div>

                                              <div class = " female-radio-div padding-lr-zero radio-inline">
                                              @if($personal_detail['sex'] == "female")
                                                <input type="radio"  name="gender" id="female" value = "female" checked>
                                              @else
                                                <input type="radio"  name="gender" id="female" value = "female">
                                              @endif
                                                <label for= "female" class="radio-label"><span class="radio right-label-span">Female</span></label>
                                              </div>                              
                                            </div> 
                                            <span class="text-danger error"></span>                               
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Residence</p>
                                            <div class="resident-radio-divs">
                                              <div class = " indian-radio-div padding-lr-zero radio-inline">
                                              @if($personal_detail['resident'] == "indian")
                                                <input type="radio"  name="resident" id="indian" value = "indian" checked>
                                              @else
                                                <input type="radio"  name="resident" id="indian" value = "indian" >
                                              @endif
                                                <label for="indian" class="radio-label"><span class="radio left-label-span">Indian</span></label>
                                              </div>

                                              <div class = " nri-radio-div padding-lr-zero radio-inline">
                                              @if($personal_detail['resident'] == "nri")
                                                <input type="radio" name="resident" id="nri" value = "nri" checked>
                                              @else
                                                <input type="radio" name="resident" id="nri" value = "nri">
                                              @endif
                                                
                                                <label for= "nri" class="radio-label"><span class="radio right-label-span">NRI</span></label>
                                              </div>                              
                                            </div>
                                            <span class="text-danger error"></span>    
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Marital Status</p>
                                            <div class="marital-radio-divs">
                                              <div class = " single-radio-div padding-lr-zero radio-inline">
                                              @if($personal_detail['mar_status'] == "single")
                                                <input type="radio"  name="marital-status" id="single" value = "single" checked>
                                              @else
                                                <input type="radio"  name="marital-status" id="single" value = "single" >
                                              @endif
                                                <label for="single" class="radio-label"><span class="radio left-label-span">Single</span></label>
                                              </div>

                                              <div class = " married-radio-div padding-lr-zero radio-inline">
                                              @if($personal_detail['mar_status'] == "married")
                                                <input type="radio"  name="marital-status" id="married" value = "married" checked>
                                              @else
                                                <input type="radio"  name="marital-status" id="married" value = "married" >
                                              @endif
                                                <label for= "married" class="radio-label"><span class="radio right-label-span">Married</span></label>
                                              </div>                              
                                            </div>
                                            <span class="text-danger error"></span>    
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" class="input-field" value="{{$personal_detail['address']}}" >
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Communication Address</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pincode" id="pincode" class="input-field num-field" value="{{$personal_detail['pincode']}}" >
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Pin Code</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nationality" id="nationality" value="Indian" class="input-field text-field" >
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Nationality</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "birth-city" id="birth-city" class="input-field text-field" value="{{$personal_detail['b_city']}}" >
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Birth City</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "aadhar" id="aadhar" class="input-field num-field" value="{{$personal_detail['aadhar']}}" >
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Aadhar Number</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="NEXT">
<!--                                     <input type="button" class="blue-btn btn-primary btn" id="edit-personal" value="EDIT" name="">
                                    <input type="button" class="blue-btn btn-primary btn" id="next-btn" value="NEXT" name=""> -->
                                </form>
                            </div><!-- Personal Details Tab ends -->
                            @endforeach
                            @else
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="personal-details-tab">
                                <form id="personal-details-form" class="acc-activate-forms">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "name" id="name" class="input-field text-field" value="{{$name}}">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Full Name</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pan" id="pan" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">PAN</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "dob" id="dob" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Date of Birth</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "fathers-name" id="fathers-name" class="input-field text-field">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Father's/Spouse Name</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "mothers-name" id="mothers-name" class="input-field text-field">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Mother's Name</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                                <select name="occupation" id="occupation">
                                                    <option value="service">Service</option>
                                                    <option value="others">Others</option>
                                                    <option value="private">Private Sector</option>
                                                    <option value="professional">Professional</option>
                                                    <option value="business">Business</option>
                                                    <option value="public">Public Sector</option>
                                                    <option value="government">Government Sector</option>
                                                    <option value="self">Self Employed</option>
                                                    <option value="retired">Retired</option>
                                                    <option value="housewife">Housewife</option>
                                                    <option value="student">Student</option>
                                                    <option value="not-categorized">Not Categorized</option>
                                                </select>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <span class="text-danger error"></span>
                                                <!-- <label class="input-label">Occupation Type</label> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Sex</p>
                                            <div class="sex-radio-divs">
                                              <div class = " male-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="gender" id="male" value = "male">
                                                <label for="male" class="radio-label"><span class="radio left-label-span">Male</span></label>
                                              </div>

                                              <div class = " female-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="gender" id="female" value = "female">
                                                <label for= "female" class="radio-label"><span class="radio right-label-span">Female</span></label>
                                              </div>                              
                                            </div> 
                                            <span class="text-danger error"></span>                               
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Residence</p>
                                            <div class="resident-radio-divs">
                                              <div class = " indian-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="resident" id="indian" value = "indian">
                                                <label for="indian" class="radio-label"><span class="radio left-label-span">Indian</span></label>
                                              </div>

                                              <div class = " nri-radio-div padding-lr-zero radio-inline">
                                                <input type="radio" name="resident" id="nri" value = "nri">
                                                <label for= "nri" class="radio-label"><span class="radio right-label-span">NRI</span></label>
                                              </div>                              
                                            </div>
                                            <span class="text-danger error"></span>    
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Marital Status</p>
                                            <div class="marital-radio-divs">
                                              <div class = " single-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="marital-status" id="single" value = "single">
                                                <label for="single" class="radio-label"><span class="radio left-label-span">Single</span></label>
                                              </div>

                                              <div class = " married-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="marital-status" id="married" value = "married" >
                                                <label for= "married" class="radio-label"><span class="radio right-label-span">Married</span></label>
                                              </div>                              
                                            </div>
                                            <span class="text-danger error"></span>    
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" class="input-field">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Communication Address</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pincode" id="pincode" class="input-field num-field">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Pin Code</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nationality" id="nationality" value="indian" class="input-field text-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Nationality</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "birth-city" id="birth-city" class="input-field text-field">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Birth City</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "aadhar" id="aadhar" class="input-field num-field" value="">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Aadhar Number</label>
                                                <span class="text-danger error"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="NEXT">
                                </form>
                            </div><!-- Personal Details Tab ends -->
                            @endif


                            @if(\Auth::user()->b_check == "1")
                            @foreach($bank_details as $bank_detail)
                                <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="bank-details-tab">
                                    <form id="bank-details-form" class="acc-activate-forms">
                                        <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">

                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <div class="form-group">
                                                     <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                                   <select  style = "display: none;" name="bank-name" id="bank-name">
                                                    <?php 
                                                        $bank_name_selected = "";
                                                     ?>
                                                        @foreach($bank_info as $bank)
                                                        <?php if ($bank_details[0]['bank_id'] == $bank['bank_id']): ?>
                                                            <option value="{{$bank['bank_id']}}" selected>{{$bank['bank_name']}}</option>
                                                            <?php $bank_name_selected = $bank['bank_name'] ?>
                                                            <?php else: ?>
                                                            <option value="{{$bank['bank_id']}}">{{$bank['bank_name']}}</option>
                                                            
                                                        <?php endif ?>
                                                        @endforeach
                                                    </select>
                                                    <!--<span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <span class="error text-danger"></span>
                                                    <label class="input-label">Occupation Type</label> -->
                                                    <div class="dropdown">
                                                      <button id="bank-name-btn" class="btn btn-primary dropdown-toggle bank-menu" type="button" data-toggle="dropdown"><span>{{$bank_name_selected}}</span><span><i id="acc-type-icon" class="material-icons">keyboard_arrow_down</i></span>
                                                      </button>
                                                      <ul class="dropdown-menu " id="bank-dropdown">
                                                        @foreach($bank_info as $bank)
                                                        <li><a href="#" class="bank-select" data-bankid = "{{$bank['bank_id']}}">{{$bank['bank_name']}}</a></li>
                                                        
                                                        @endforeach
                                                      </ul>
                                                    </div>
                                                    <span class="error text-danger"></span>

                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <input type="text" name = "acc-name" id="acc-name" class="input-field text-field" value="{{$bank_detail['acc_name']}}" required>
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label class="input-label">Account Holder Name</label>
                                                    <span class="error text-danger"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <input type="text" name = "acc-no" id="acc-no" class="input-field" value="{{$bank_detail['acc_no']}}">
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label class="input-label">Account Number</label>
                                                    <span class="error text-danger"></span>
                                                </div>
                                            </div>

                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <input type="text" name = "ifsc-code" id="ifsc-code" class="input-field" value="{{$bank_detail['ifsc_code']}}">
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label class="input-label">IFSC Code</label>
                                                    <span class="error text-danger"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                                    <select name="acc-type" id="acc-type">
                                                        <option value="0">Savings</option>
                                                        <option value="1">Current</option>
                                                    </select>
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <span class="error text-danger"></span>
                                                    <!-- <label class="input-label">Occupation Type</label> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                            <p class="sub-tab-heading">Documents Upload</p>

                                            <?php 
                                                $cc_link = \Auth::user()->ekyc->cc_link;
                                                $mandate_link = \Auth::user()->ekyc->mandate_link;
                                                $cc_status = \Auth::user()->nachDetails->cc_status;
                                                $mandate_status = \Auth::user()->nachDetails->mandatesoft_status
                                            ?>

                                            <?php
                                            //below condition applies if mandate or cc is not uploaded to rejected.
                                             ?>

<!-- mandate and cc link status -->
											@if($cc_link == '' || $cc_status == '2')
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">  
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Cancelled Cheque or Passbook Front Page</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <input type="file" name="cancelled-cheque" id="cancelled-cheque">
                                                    <label for="cancelled-cheque">
                                                        <a class="btn btn-primary bank-label">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   
                                                <p class="download-form">
                                                <a href="/files/samples/cc&passbook_sample.jpg" class="pull-right" download>Sample</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div>
                                            <span class="error text-danger"></span>
                                        </div>
                                        @elseif($cc_link != '' && $cc_status == '')
										<div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Cancelled Cheque or Passbook Front Page</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   
                                                <p class="download-form">
                                                <a href="/files/samples/cc&passbook_sample.jpg" class="pull-right" download>Sample</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div>  
                                            <span class="error text-danger"></span>
                                            <p>Already Uploaded</p>
                                        </div>
                                        @elseif($cc_link != '' && $cc_status == '1')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Cancelled Cheque or Passbook Front Page</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   
                                                <p class="download-form">
                                                <a href="/files/samples/cc&passbook_sample.jpg" class="pull-right" download>Sample</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div>  
                                            <span class="error text-danger"></span>
                                            <p style="color: #04D298;">Verified successfully</p>
                                        </div>
                                        @endif

                                        @if($mandate_link == '' || $mandate_status == '2')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Mandate</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <input type="file" name="mandate" id="mandate">
                                                    <label for="mandate">
                                                        <a class="btn btn-primary bank-label">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   <p class="download-form"><a href="{{url('/assets/Bank Mandate.pdf')}}" download="">Download Form</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div> 
                                            <span class="error text-danger"></span>
                                        </div>
                                        @elseif($mandate_link != '' && $mandate_status == '')
										<div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Mandate</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   <p class="download-form"><a href="{{url('/assets/Bank Mandate.pdf')}}" download="">Download Form</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div> 
                                            <span class="error text-danger"></span>
                                            <p>Already Uploaded</p>
                                        </div>
                                        @elseif($mandate_link != '' && $mandate_status == '1')
										<div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Mandate</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   <p class="download-form"><a href="{{url('/assets/Bank Mandate.pdf')}}" download="">Download Form</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div> 
                                            <span class="error text-danger"></span>
                                            <p style="color: #04D298;">Verified Successfully</p>
                                        </div>                     
                                        @endif
                                    </div>
                                        <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                        @if(\Auth::user()->nachDetails->pan_status == '2')
                                        <input type="button" class="blue-btn btn-primary btn" id="next-kyc" name="" value="NEXT">
                                        @else
                                        <input type="button" class="blue-btn btn-primary btn" id="next-kyc" name="" value="NEXT" style="display: none;">
                                        @endif 
                                </form>                 
                                @if($mandate_link != '' && $cc_link != '')
					                @if($mandate_status == '1' && $cc_status == '1')
                                    <p class="green m-l-30">Mandate and Cancelled Cheque/Passbook Approved.</p>
                                    @elseif($mandate_status == '2' || $cc_status == '2')
                                    <p class="red m-t-20 m-l-30">Mandate and Cancelled Cheque/Passbook Rejected. Kindly Upload it again.</p>
                                    @elseif($mandate_status == '' || $cc_status == '')
                                    <p class="blue m-t-20 m-l-30">Documents under verification it may take upto 24hrs.</p>
                                    @endif
                                @endif

                                </div><!-- bank Details Tab ends --> 
                            @endforeach
                            @else
                            @endif


                             <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="bank-details-tab">
                                <form id="bank-details-form" class="acc-activate-forms">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                 <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                               <select  style = "display: none;" name="bank-name" id="bank-name">
                                                    
                                                    @foreach($bank_info as $bank)
                                                    <option value="{{$bank['bank_id']}}">{{$bank['bank_name']}}</option>
                                                    
                                                    @endforeach
                                                </select>
                                                <!--<span class="highlight"></span>
                                                <span class="bar"></span>
                                                <span class="error text-danger"></span>
                                                <label class="input-label">Occupation Type</label> -->
                                                <div class="dropdown"  >
                                                  <button id="bank-name-btn" class="btn btn-primary dropdown-toggle bank-menu" type="button" data-toggle="dropdown"><span>Bank Name</span><span><i id="acc-type-icon" class="material-icons">keyboard_arrow_down</i></span>
                                                  </button>
                                                  <ul class="dropdown-menu" id="bank-dropdown">
                                                    @foreach($bank_info as $bank)
                                                    <li><a href="#" class="bank-select" data-bankid = "{{$bank['bank_id']}}">{{$bank['bank_name']}}</a></li>
                                                    
                                                    @endforeach
                                                  </ul>
                                                </div>
                                                <span class="error text-danger"></span>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-name" id="acc-name" class="input-field text-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Account Holder Name</label>
                                                <span class="error text-danger"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-no" id="acc-no" class="input-field ">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Account Number</label>
                                                <span class="error text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">   
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "ifsc-code" id="ifsc-code" class="input-field">
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">IFSC Code</label>
                                                <span class="error text-danger"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                               <select  style = "display: none;" name="acc-type" id="acc-type">
                                                    <option value="1">Savings</option>
                                                    <option value="0">Current</option>
                                                </select>
                                                <!--<span class="highlight"></span>
                                                <span class="bar"></span>
                                                <span class="error text-danger"></span>
                                                <label class="input-label">Occupation Type</label> -->
                                                <div class="dropdown">
                                                  <button class="btn btn-primary dropdown-toggle occupation-menu" type="button" data-toggle="dropdown">Account Type<span><i id="acc-type-icon" class="material-icons">keyboard_arrow_down</i></span>
                                                  </button>
                                                  <ul class="dropdown-menu" id="acctype-dropdown">
                                                    <li><a href="#" class="acc-select">Savings</a></li>
                                                    <li><a href="#" class="acc-select">Current</a></li>
                                                  </ul>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <p class="sub-tab-heading">Documents Upload</p>
                                            <?php 
                                                $cc_link = \Auth::user()->ekyc->cc_link;
                                                $mandate_link = \Auth::user()->ekyc->mandate_link;
                                                $cc_status = \Auth::user()->nachDetails->cc_status;
                                                $mandate_status = \Auth::user()->nachDetails->mandatesoft_status
                                            ?>

                                            <?php
                                            //below condition applies if mandate or cc is not uploaded to rejected.
                                             ?>

<!-- mandate and cc link status -->
                                            @if($cc_link == '' || $cc_status == '2')
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">  
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Cancelled Cheque or Passbook Front Page</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <input type="file" name="cancelled-cheque" id="cancelled-cheque">
                                                    <label for="cancelled-cheque">
                                                        <a class="btn btn-primary bank-label">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   
                                                <p class="download-form">
                                                <a href="/files/samples/cc&passbook_sample.jpg" class="pull-right" download>Sample</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div>
                                            <span class="error text-danger"></span>
                                        </div>
                                        @elseif($cc_link != '' && $cc_status == '')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Cancelled Cheque or Passbook Front Page</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   
                                                <p class="download-form">
                                                <a href="/files/samples/cc&passbook_sample.jpg" class="pull-right" download>Sample</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div>  
                                            <span class="error text-danger"></span>
                                            <p>Already Uploaded</p>
                                        </div>
                                        @elseif($cc_link != '' && $cc_status == '1')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Cancelled Cheque or Passbook Front Page</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   
                                                <p class="download-form">
                                                <a href="/files/samples/cc&passbook_sample.jpg" class="pull-right" download>Sample</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div>  
                                            <span class="error text-danger"></span>
                                            <p style="color: #04D298;">Verified successfully</p>
                                        </div>
                                        @endif

                                        @if($mandate_link == '' || $mandate_status == '2')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Mandate</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <input type="file" name="mandate" id="mandate">
                                                    <label for="mandate">
                                                        <a class="btn btn-primary bank-label">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   <p class="download-form"><a href="{{url('/assets/Bank Mandate.pdf')}}" download="">Download Form</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div> 
                                            <span class="error text-danger"></span>
                                        </div>
                                        @elseif($mandate_link != '' && $mandate_status == '')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Mandate</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   <p class="download-form"><a href="{{url('/assets/Bank Mandate.pdf')}}" download="">Download Form</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div> 
                                            <span class="error text-danger"></span>
                                            <p>Already Uploaded</p>
                                        </div>
                                        @elseif($mandate_link != '' && $mandate_status == '1')
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class = "col-lg-12 col-md-12 col-sm-12 file-container">   
                                                <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                                                    <p class="form-helper">Mandate</p> 
                                                </div>
                                                <div class = "col-lg-5 col-md-5 col-sm-5 text-center p-lr-zero"> 
                                                    <label>
                                                        <a class="btn btn-primary bank-label gray-btn">
                                                            Upload
                                                        </a>
                                                    </label>
                                                </div>
                                                <div class = "col-lg-12 col-md-12 col-sm-12">   <p class="download-form"><a href="{{url('/assets/Bank Mandate.pdf')}}" download="">Download Form</a>
                                                </p>
                                                    <p class="upload-filename"></p>
                                                </div>
                                            </div> 
                                            <span class="error text-danger"></span>
                                            <p style="color: #04D298;">Verified Successfully</p>
                                        </div>                     
                                        @endif
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                    @if(\Auth::user()->nachDetails->pan_status == '2')
                                    <input type="button" class="blue-btn btn-primary btn" id="next-kyc" name="" value="NEXT"">
                                    @else
                                    <input type="button" class="blue-btn btn-primary btn" id="next-kyc" name="" value="NEXT"" style="display: none;">
                                    @endif
                                </form>

                                @if($mandate_status == '1' && $cc_status == '1')
                                <p class="blue m-l-30">Mandate and Cancelled Cheque/Passbook Approved.</p>
                                @elseif($mandate_status == '2' || $cc_status == '2')
                                <p class="red m-l-30">Mandate and Cancelled Cheque/Passbook Rejected. Kindly Upload it again.</p>
                                @elseif($mandate_status == '' || $cc_status == '')
                                <p class="m-l-30"><!-- Mandate and Cancelled Cheque/Passbook are yet to be verified --></p>
                                @endif
                            </div><!-- bank Details Tab ends -->

                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="nominee-details-tab">
                                <form id="nominee-details-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-name" id="nom1-name" class="input-field text-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Nominee Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "nom1-relationship" id="nom1-relationship" class="input-field text-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Relationship</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-dob" id="nom1-dob" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Date of Birth</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-address" id="nom1-address" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Communication Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-pin" id="nom1-pin" class="input-field num-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Pincode</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-perc" id="nom1-perc" class="input-field num-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Percentage Holding</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- Nominee Details Tab ends -->


                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="ekyc-tab">
                             <?php $video_link = \Auth::user()->ekyc->video_link; $ekyc_status = \Auth::user()->nachDetails->kycvideo_status?>
                                <form id="ekyc-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <p>Start EKYC</p>
                                                @if($video_link != '' && $ekyc_status == '')
                                                
                                                @elseif($video_link == '' && $ekyc_status == '')
                                                <a href="{{url('/ekyc')}}" class="btn btn-primary ekyc-btn">EKYC</a>
                                                @elseif($video_link != '' && $ekyc_status == '2')
                                                <a href="{{url('/ekyc')}}" class="btn btn-primary ekyc-btn">EKYC</a>
                                                @elseif($video_link != '' && $ekyc_status == '1')
                                                <!-- <p class="green m-l-30">EKYC Video Approved.</p> -->
                                                <a href="#" class="btn btn-primary gray-btn">EKYC</a>
                                                @endif
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                               <p id="kyc-info-header"><span>KYC Form</span> <!-- <span class="pull-right blue-text">View Sample</span> --></p>
                                               <div class="kyc-form-container">
                                                  <a href="{{url('assets/kyc.pdf')}}" download>
                                                       <img src="icons/upload_icon.png">
                                                       <span>Download form</span>
                                                  </a>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                @if($video_link != '' && $ekyc_status == '')
                                <p class="blue m-l-30">EKYC video has been uploaded and yet to be verified</p>
                                @elseif($video_link != '' && $ekyc_status == '2')
                                <p class="red m-l-30">EKYC Video Rejected. Kindly Do it again.</p>
                                @elseif($video_link != '' && $ekyc_status == '1')
                                <p class="green m-l-30">EKYC Video Approved.</p>
                                @endif

                                <p id="why-ekyc-header">Why EKYC ?</p>
                                <p class="why-ekyc-content">SEBI (Securities Exchange Board of India) requires all investors to complete an 'In Person Verification' process before allowing them to invest in mutual funds. Rightfunds being an online platform achieves this by an E-KYC video. This allows us to on board investors from all over the country without having to meet each one personally. We give data privacy paramount importance and do not share it with third parties.</p>
                            </div> <!-- ekyc tab ends -->

                        </div> 
                    </div>     
                </div>
            </div>
        </section>

        @endsection


        <!-- Modal -->
        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title text-center" id="modal-header"></h4>
              </div>
              <div class="modal-body" id="modal-body">
                
              </div>
            </div>

          </div>
        </div>


     <script src="js/jquery.min.js"></script>
     <script src="js/version2/activate-account.js?v=1.2"></script>
     <script src="js/version2/validation.js?v=1.1"></script>
     <script src="js/version2/loader.js?v=1.1"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="js/jquery.donut.js?v=1.1"></script>
     <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script>
    </body>
</html>
