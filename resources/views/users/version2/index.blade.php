<?php
    $fmt = new \NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="css/version2/index-responsive.css?v=1.1">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
         <link rel="stylesheet" href="css/version2/index.css?v=1.1">
         <link rel="stylesheet" href="css/version2/loader.css?v=1.1">


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="{{url('css/version2/navbar-responsive.css?v=1.1')}}">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>
    <div class="loader" id="loader"></div>
        @extends('layouts.navbar')
        @section('content')

        @if(\Auth::user()->nachDetails->nach_status == 0)
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" id="alert-holder">
                        <p id="inactive-parent">
                            <span id="inactive-info"><i class="material-icons">error</i></span>
                            <span>Please Activate your account to start investing</span>
                            <a href="{{url('/account_activation')}}" class="pull-right"><i class="material-icons">arrow_forward</i></a></p>
                    </div>
                </div>
            </div>
        </section>
        @endif

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bot header-pad"  id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30">Investment Summary</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <p class="section-header-info">Snapshot of your Investment as of Today, <span class="today-date"><?php echo date('j M Y');?>.</span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container inv-detailsleft">
                                   <p class="cont-header">Current Value</p>
                                    @if(array_key_exists('total_current_value', $user_investment_details))
                                    <p class="inv-sum-amount"><span>Rs. </span><span id="current-value">{{$fmt->format($user_investment_details['total_current_value'])}}</span></p>
                                    @else
                                    <p class="inv-sum-amount">Rs.0</p>
                                    @endif
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container inv-detailsright">
                                   <p class="cont-header">Total Investment</p>
                                    @if(array_key_exists('total_amount_invested', $user_investment_details))
                                    <p class="inv-sum-amount"><span>Rs. </span>
                                        <?php
                                            $whole = floor($user_investment_details['total_amount_invested']);
                                            $fraction = $user_investment_details['total_amount_invested'] - $whole;

                                            if ($fraction != 0.0 || $fraction != 0) {
                                              if (\Auth::id() == 130) {
                                                echo $fmt->format($user_investment_details['total_amount_invested']+50000);
                                              }else{
                                                echo $fmt->format($user_investment_details['total_amount_invested']);
                                              }
                                            }else{
                                              if (\Auth::id() == 130) {
                                                echo $fmt->format($user_investment_details['total_amount_invested']+50000).".00";
                                              }else {
                                                echo $fmt->format($user_investment_details['total_amount_invested']).".00";
                                              }
                                            }
                                         ?>
                                        </p>
                                    @else
                                    <p class="inv-sum-amount">Rs.0</p>
                                    @endif
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container inv-detailsleft">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    @if(array_key_exists('total_net_profit', $user_investment_details))
                                    <?php if (\Auth::id() == 130): ?>
                                      <p class="inv-sum-amount"><span>Rs. </span>{{$fmt->format($user_investment_details['total_net_profit']-50000)}}</p>
                                      <?php else: ?>
                                        <p class="inv-sum-amount"><span>Rs. </span>{{$fmt->format($user_investment_details['total_net_profit'])}}</p>
                                    <?php endif; ?>
                                    @else
                                    <p class="inv-sum-amount">Rs.0</p>
                                    @endif
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container inv-detailsright" id="returns-holder">
                                    <span class="cont-header">Return</span>
                                    <span id="growth-type">XIRR</span>
                                    @if(array_key_exists('total_profit_percent', $user_investment_details))
                                    <p class="inv-sum-amount" id="avg-returns">{{$user_investment_details['total_profit_percent']}}%</p>
                                    @else
                                    <p class="inv-sum-amount" id="avg-returns">0%</p>
                                    @endif
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>

        <section id="goal-benchmark">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 p-lr-zero br box-shadow-sub" id="goal-div">
                            <div class="col-lg-12 col-md-12 col-sm-12 container-card br p-lr-zero">
                                <p class="header-pad border-bot section-header pl-30" id="goal-header">Goal <span class="pull-right" id="goal-info">i</span></p>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero">

                                    <?php


                                        $goal_amount = $user_investment_details['goal_amount'];
                                        $achieved_goal = $user_investment_details['total_current_value'];

                                        if ($goal_amount == 0) {
                                            $remaining_goal = 100;
                                        }else{
                                            $remaining_goal = $goal_amount - $achieved_goal;
                                        }

                                        if ($goal_amount != 0) {
                                            $away_percent = (($goal_amount - $achieved_goal)/$goal_amount) * 100;
                                            $goal_string = "You are only ".round($away_percent,1)."% away from your Goal";
                                        }else{
                                            $goal_string = "You haven't set your goal yet.";
                                        }


                                     ?>

                                    <input type="hidden" id="remaining-goal" value="{{$remaining_goal}}">
                                    @if($user_investment_details['goal_percent'] < 10)
                                        <p id="goal-perc">{{$user_investment_details['goal_percent']}}%</p>
                                    @elseif($user_investment_details['goal_percent'] < 100)
                                        <p id="goal-perc">{{$user_investment_details['goal_percent']}}%</p>
                                    @else
                                        <p id="goal-perc">{{$user_investment_details['goal_percent']}}%</p>
                                    @endif
                                    <canvas id="goal-chart" height="200" width="160" class="donut-chart">
                                      <div data-value="{{$user_investment_details['total_current_value']}}" id="goal-reached-val"></div>
                                      <div data-value="{{$remaining_goal}}" id="remaining-goal-val"></div>
                                    </canvas>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero" id="goal-chart-div">
                                    <p class="text-center imp-content" id="goal-away-text">{{$goal_string}}</p>
                                    <p class="text-center sub-cont-header">Goal Amount</p>
                                    <p class="text-center sub-cont-amount">Rs.<span id= "current-goal-amount">
                                        <?php
                                            if ( strpos($user_investment_details['goal_amount'], '.' ) === true) {
                                            echo $fmt->format($user_investment_details['goal_amount']);
                                            }else{
                                            echo $fmt->format($user_investment_details['goal_amount']).".00";
                                            }
                                         ?></span></p>
                                    <a class="text-center center-block blue-text" id="edit-goal">Edit Goal</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 " id="key-benchmark">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 border-all box-shadow-sub br container-card  p-lr-zero">
                                <p class="section-header header-pad border-bot pl-30">Key Benchmarks <span id="bse-date">As of <span><?php echo date('j M Y',strtotime("-1 days"));?>.</span></span><span class="pull-right" id="bse-info">i</span></p>

                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero benchmark-holder">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero pl-30">
                                        <p class="cont-header mb-0">BSE SENSEX</p>
                                        <p class="inner-number-value">{{$fmt->format($Sensex['close_val'])}}</p>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero pr-30">
                                        <p class="cont-header text-right mb-0">One Day Change</p>
                                        <p class="inner-number-value text-right">{{$Sensex['diff_val']}}
                                            <span class="#">
                                                @if($Sensex['diff_val'] > 0)
                                                <i class="material-icons text-green bse-material">arrow_drop_up</i>
                                                @else
                                                <i class="material-icons text-red bse-material">arrow_drop_down</i>
                                                @endif
                                            </span>
                                            <span id="one-day-bse">{{$Sensex['avg_val']}}%</span>
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero benchmark-holder">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero pl-30">
                                        <p class="cont-header mb-0">BSE Small Cap</p>
                                        <p class="inner-number-value">{{$fmt->format($Smallcap['close_val'])}}</p>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero pr-30">
                                        <p class="cont-header text-right mb-0">One Day Change</p>
                                        <p class="inner-number-value text-right">{{$Smallcap['diff_val']}}
                                            <span class="#">
                                                @if($Smallcap['diff_val'] > 0)
                                                <i class="material-icons text-green bse-material">arrow_drop_up</i>
                                                @else
                                                <i class="material-icons text-red bse-material">arrow_drop_down</i>
                                                @endif
                                            </span>
                                            <span id="one-day-bse">{{$Smallcap['avg_val']}}%</span>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-shadow-sub p-lr-zero">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-card br p-lr-zero" id="add-inv-card">
                                <p class="text-center">Add more Funds to your Portfolio of Investments</p>
                                <a href="{{URL('fund_selector')}}" id="invest-btn" class="center-block">Invest Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow-sub p-lr-zero border-all br">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot header-pad" id="portfolio-header-cont">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30">Portfolio Composition</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <p class="section-header-info">How your Portfolio looks as of Today, <span class="today-date"><?php echo date('j M Y');?></span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 xs-widthres  border-xs-right border-right portfolio-comp-container portfolio-big-cont border-xs-bottom">
                                    @if(Auth::user()->p_check == 1 && $user_investment_details['eq']['total_amount_invested'] > 0)
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Equity</p>
                                                <p class="portfolio-split contenthead-left" id="equity-split">                                                                       {{$user_investment_details['eq']['profit_percent']+$user_investment_details['eq']['investment_percent']}}%
                                                </p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="equity-returns">{{$user_investment_details['eq']['total_profit_percent']}}%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="equity-cv">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format($user_investment_details['eq']['total_current_value'])}}</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="equity-ti">
                                                    <span>Rs. </span>
                                                    <span>
                                                        <?php
                                                            $whole = floor($user_investment_details['eq']['total_amount_invested']);
                                                            $fraction = $user_investment_details['eq']['total_amount_invested'] - $whole;

                                                            if ($fraction != 0.0 || $fraction != 0) {
                                                                echo $fmt->format($user_investment_details['eq']['total_amount_invested']);
                                                            }else{
                                                               echo $fmt->format($user_investment_details['eq']['total_amount_invested']).".00";
                                                            }
                                                         ?>

                                                    </span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="equity-np">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format(round(($user_investment_details['eq']['total_current_value']) - ($user_investment_details['eq']['total_amount_invested']),2))}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @else

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Equity</p>
                                                <p class="portfolio-split contenthead-left" id="equity-split">0%</p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="equity-returns">{{$user_investment_details['eq']['total_profit_percent']}}%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="equity-cv">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="equity-ti">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="equity-np">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>


                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  xs-widthres  border-xs-right border-right portfolio-comp-container portfolio-big-cont border-xs-bottom" id="debt-comp-container">
                                    @if(Auth::user()->p_check == 1 && $user_investment_details['debt']['total_amount_invested'] > 0)
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Debt</p>
                                                <p class="portfolio-split contenthead-left" id="debt-split">                                                                       {{$user_investment_details['debt']['profit_percent']+$user_investment_details['debt']['investment_percent']}}%
                                                </p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="debt-returns">{{$user_investment_details['debt']['total_profit_percent']}}%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="debt-cv">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format($user_investment_details['debt']['total_current_value'])}}</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="debt-ti">
                                                    <span>Rs. </span>
                                                    <span>
                                                        <?php
                                                            $whole = floor($user_investment_details['debt']['total_amount_invested']);
                                                            $fraction = $user_investment_details['debt']['total_amount_invested'] - $whole;
                                                            if ($fraction != 0.0 || $fraction != 0) {
                                                                echo $fmt->format($user_investment_details['debt']['total_amount_invested']);
                                                            }else{
                                                                echo $fmt->format($user_investment_details['debt']['total_amount_invested']).".00";
                                                            }
                                                         ?>
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center contenthead-left">Net Profit</p>
                                                <p class="portfolio-amount-value text-center contenthead-left" id="debt-np">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format(round(($user_investment_details['debt']['total_current_value']) - ($user_investment_details['debt']['total_amount_invested']),2))}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @else

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Debt</p>
                                                <p class="portfolio-split contenthead-left" id="debt-split">0%</p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="debt-returns">0%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="debt-cv">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="debt-ti">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="debt-np">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>


                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  xs-widthres  border-xs-right border-right portfolio-comp-container portfolio-big-cont" >
                                    @if(Auth::user()->p_check == 1 && $user_investment_details['bal']['total_amount_invested'] > 0)
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Balanced</p>
                                                <p class="portfolio-split contenthead-left" id="bal-split">                                                                       {{$user_investment_details['bal']['profit_percent']+$user_investment_details['bal']['investment_percent']}}%
                                                </p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="bal-returns">{{$user_investment_details['bal']['total_profit_percent']}}%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="bal-cv">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format($user_investment_details['bal']['total_current_value'])}}</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="bal-ti">
                                                    <span>Rs. </span>
                                                    <span>
                                                        <?php
                                                            $whole = floor($user_investment_details['bal']['total_amount_invested']);
                                                            $fraction = $user_investment_details['bal']['total_amount_invested'] - $whole;
                                                            if ($fraction != 0.0 || $fraction != 0) {
                                                                echo $fmt->format($user_investment_details['bal']['total_amount_invested']);
                                                            }else{
                                                                echo $fmt->format($user_investment_details['bal']['total_amount_invested']).".00";
                                                            }
                                                         ?>
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="bal-np">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format(round(($user_investment_details['bal']['total_current_value']) - ($user_investment_details['bal']['total_amount_invested']),2))}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @else

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Balanced</p>
                                                <p class="portfolio-split contenthead-left" id="bal-split">0%</p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="bal-returns">0%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="bal-cv">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="bal-ti">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="bal-np">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  xs-widthres  border-xs-right  portfolio-big-cont" id="portfolio-comp-container">
                                    @if(Auth::user()->p_check == 1 && $user_investment_details['ts']['total_amount_invested'] > 0)
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Tax Saver</p>
                                                <p class="portfolio-split contenthead-left" id="ts-split">                                                                       {{$user_investment_details['ts']['profit_percent']+$user_investment_details['ts']['investment_percent']}}%
                                                </p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="ts-returns">{{$user_investment_details['ts']['total_profit_percent']}}%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12   p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="ts-cv">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format($user_investment_details['ts']['total_current_value'])}}</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="ts-ti">
                                                    <span>Rs. </span>
                                                    <span>
                                                        <?php
                                                            $whole = floor($user_investment_details['ts']);
                                                            $fraction = $user_investment_details['ts']['total_amount_invested'] - $whole;
                                                            if ($fraction != 0.0 || $fraction != 0) {
                                                                echo $fmt->format($user_investment_details['ts']['total_amount_invested']);
                                                            }else{
                                                                echo $fmt->format($user_investment_details['ts']['total_amount_invested']).".00";
                                                            }
                                                         ?>
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="ts-np">
                                                    <span>Rs. </span>
                                                    <span>{{$fmt->format(round(($user_investment_details['ts']['total_current_value']) - ($user_investment_details['ts']['total_amount_invested']),2))}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @else

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero p-b-20">
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header mb-0 contenthead-left">Tax Saver</p>
                                                <p class="portfolio-split contenthead-left" id="ts-split">0%</p>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-md-6 col-xs-6 p-lr-zero">
                                                <p class="cont-header text-right mb-0">Returns</p>
                                                <p class="portfolio-returns text-green text-right" id="ts-returns">0%
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-top p-t-20 border-top portfolio-details-container">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Current Value</p>
                                                <p class="portfolio-amount-value text-center" id="ts-cv">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Total Investment</p>
                                                <p class="portfolio-amount-value text-center" id="ts-ti">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 portfolio-details-holder">
                                                <p class="portfolio-amount-info cont-header text-center">Net Profit</p>
                                                <p class="portfolio-amount-value text-center" id="ts-np">
                                                    <span>Rs. </span>
                                                    <span>0</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>
        @endsection

        <div id="InfoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title text-center" id="modal-header">
                    Why should you set a goal ?
                </h4>
              </div>
              <div class="modal-body" id="modal-body">
                <p class="goal-info text-center">
                    Setting a goal helps you stay on track! Investors who set measurable goals are more likely to surpass them than those who don't. Goal tracker helps you monitor your progress over time.
                </p>

                <button class="btn btn-primary center-block" id="okay-goal" data-dismiss="modal">Okay</button>
              </div>
            </div>

          </div>
        </div>


        <div id="bseInfoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title text-center" id="modal-header">
                    What are key Benchmarks ?
                </h4>
              </div>
              <div class="modal-body" id="modal-body">
                <p class="goal-info text-center">
                    Key benchmarks can be considered a representation of the overall market conditions. Your returns on equity mutual funds are likely to mirror / out perform the returns given by the benchmark. The BSE Sensex comprises of the stocks of 30 of the largest companies by market capitalisation. The BSE Small Cap represents companies in the bottom 15% of the total BSE market cap.
                </p>

                <button class="btn btn-primary center-block" id="okay-goal" data-dismiss="modal">Okay</button>
              </div>
            </div>

          </div>
        </div>



        <div id="generalInfoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title text-center" id="modal-header">
                </h4>
              </div>
              <div class="modal-body" id="modal-body">
                <p class="goal-info text-center" id="general-info">

                </p>

                <button class="btn btn-primary center-block" id="okay-goal" data-dismiss="modal">Okay</button>
              </div>
            </div>

          </div>
        </div>


     <script src="js/jquery.min.js"></script>
     <script src="js/version2/index.js?v=1.1"></script>
     <script src="{{url('js/version2/loader.js?v=1.1')}}"></script>
     <script src="{{url('js/version2/jquery.donut.js?v=1.1')}}"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

     <script type="text/javascript">
         $(document).ready(function(){
                $('#goal-info').on('click',function(){
                    $('#InfoModal').modal('show');
                });

                $('#bse-info').on('click',function(){
                    $('#bseInfoModal').modal('show');
                });


              var curr_val,amt_inv,eq_amt_inv,eq_curr_val,debt_amt_inv,debt_curr_val,bal_amt_inv,bal_curr_val,ts_amt_inv,ts_curr_val = 0;
                var xirr_avg_return = xirr_eq_return = xirr_debt_return = xirr_bal_return = xirr_ts_return = 0;

            $('#growth-type').on('click',function(){
                var ret_type = $(this).text();
                console.log(ret_type);
                xirr_avg_return = parseFloat({{$user_investment_details['total_profit_percent']}});//console.log(xirr_avg_return);
                xirr_eq_return = parseFloat({{$user_investment_details['eq']['total_profit_percent']}});
                xirr_debt_return = parseFloat({{$user_investment_details['debt']['total_profit_percent']}});
                xirr_bal_return = parseFloat({{$user_investment_details['bal']['total_profit_percent']}});
                xirr_ts_return = parseFloat({{$user_investment_details['ts']['total_profit_percent']}});

                console.log(xirr_eq_return);
                console.log(xirr_debt_return);

                curr_val = parseFloat({{$user_investment_details['total_current_value']}});
                amt_inv = parseFloat({{$user_investment_details['total_amount_invested']}});

                eq_amt_inv = parseFloat({{$user_investment_details['eq']['total_amount_invested']}});
                eq_curr_val = parseFloat({{$user_investment_details['eq']['total_current_value']}});

                debt_amt_inv = parseFloat({{$user_investment_details['debt']['total_amount_invested']}});
                debt_curr_val = parseFloat({{$user_investment_details['debt']['total_current_value']}});

                bal_amt_inv = parseFloat({{$user_investment_details['bal']['total_amount_invested']}});
                bal_curr_val = parseFloat({{$user_investment_details['bal']['total_current_value']}});

                ts_amt_inv = parseFloat({{$user_investment_details['ts']['total_amount_invested']}});
                ts_curr_val = parseFloat({{$user_investment_details['ts']['total_current_value']}});

                if (ret_type == 'XIRR') { //Calculate Absolute Returns

                    $(this).text('ABS');
                  var average_return = 0;
                  if(amt_inv > 0 ) {

                    if (curr_val == 0 || amt_inv == 0) {
                        average_return = 0;
                    }
                    average_return = parseFloat((((curr_val - amt_inv)/amt_inv) * 100)).toFixed(2);
                  }
                  //console.log("average return is" +average_return);
                  if (eq_amt_inv > 0) {

                    var eq_return = parseFloat((((eq_curr_val - eq_amt_inv)/eq_amt_inv) * 100)).toFixed(2);
                  }else{
                    var eq_return = 0;
                  }

                  if (debt_amt_inv > 0) {

                    var debt_return = parseFloat((((debt_curr_val - debt_amt_inv)/debt_amt_inv) * 100)).toFixed(2);
                  }else{
                     var debt_return = 0;
                  }

                  if (bal_amt_inv > 0) {
                    var bal_return = parseFloat((((bal_curr_val - bal_amt_inv)/bal_amt_inv) * 100)).toFixed(2);
                  }else{
                    var bal_return = 0;
                  }

                  if (ts_amt_inv > 0) {
                    var ts_return = parseFloat((((ts_curr_val - ts_amt_inv)/ts_amt_inv) * 100)).toFixed(2);
                  }else{
                    var ts_return = 0;
                  }

                  $('#equity-returns').text(eq_return+'%');
                  $('#debt-returns').text(debt_return+'%');
                  $('#bal-returns').text(bal_return+'%');
                  $('#ts-returns').text(ts_return+'%');
                  $('#avg-returns').text(average_return+'%');

                }else{ // Calculate XIRR Returns
                    $(this).text('XIRR');

                    $('#equity-returns').text(xirr_eq_return+'%');
                    $('#debt-returns').text(xirr_debt_return+'%');
                    $('#bal-returns').text(xirr_bal_return+'%');
                    $('#ts-returns').text(xirr_ts_return+'%');
                    $('#avg-returns').text(xirr_avg_return+'%');

                }
            });
        });

     </script>
    </body>
</html>
