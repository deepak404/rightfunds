<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Tax Saving Statements</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/version2/tax-saving.css?v=1.1">
        <link rel="stylesheet" href="css/version2/acc-statement.css?v=1.1">
        <link rel="stylesheet" href="css/version2/footer.css?v=1.1">
        <link rel="stylesheet" href="css/version2/pd-responsive.css?v=1.1">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->

        <script
              src="https://code.jquery.com/jquery-3.2.1.js"
              integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
              crossorigin="anonymous"></script>


        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/version2/font-and-global.css?v=1.1">
        <link rel="stylesheet" href="css/version2/notifications.css?v=1.1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="{{url('css/version2/navbar-responsive.css?v=1.1')}}">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-114917491-1');
</script>
    </head>
    <body>

    @extends('layouts.navbar')
    @section('content')

        <section id="acc-statement-header">
            <div class="container">
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline text-center fl" id="acc-statement-header-ul">
                            <li><a href="{{url('/portfolio_details')}}">Portfolio Details</a></li>
                            <li><a href="{{url('/investment_history')}}">Investment History</a>
                            <li class="active-acc-header"><a href="{{url('/tax_saving')}}">Tax Saving Statement</a>
                            <li><a href="{{url('/withdraw_funds')}}">Withdraw Funds</a>
                        </ul>
                    </div>
                </div>  
            </div><!-- container ends -->
        </section>

        <section id="no-tss">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4 class="text-center" id="no-tss">Tax Saving Statement is not available yet.</h4>
                    </div>
                </div>
            </div>
        </section>

<!--         <section  class="portfolio-table-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <p class="tax-statement-heading">Financial Year<span class="tax-financial-year">04-09-2016 - 04-09-2017</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-email"><i class="material-icons">mail_outline</i></span><span>Send E-mail</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-download"><i class="material-icons">file_download</i></span><span>Download</span></p>
                        </div>
                    </div>
                </div>  
            </div>  
        </section>

        <section  class="portfolio-table-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <p class="tax-statement-heading">Financial Year<span class="tax-financial-year">04-09-2016 - 04-09-2017</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-email"><i class="material-icons">mail_outline</i></span><span>Send E-mail</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-download"><i class="material-icons">file_download</i></span><span>Download</span></p>
                        </div>
                    </div>
                </div>  
            </div>  
        </section> 

        <section  class="portfolio-table-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <p class="tax-statement-heading">Financial Year<span class="tax-financial-year">04-09-2016 - 04-09-2017</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-email"><i class="material-icons">mail_outline</i></span><span>Send E-mail</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-download"><i class="material-icons">file_download</i></span><span>Download</span></p>
                        </div>
                    </div>
                </div>  
            </div>  
        </section>

        <section  class="portfolio-table-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <p class="tax-statement-heading">Financial Year<span class="tax-financial-year">04-09-2016 - 04-09-2017</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-email"><i class="material-icons">mail_outline</i></span><span>Send E-mail</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-download"><i class="material-icons">file_download</i></span><span>Download</span></p>
                        </div>
                    </div>
                </div>  
            </div>  
        </section>

        <section  class="portfolio-table-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <p class="tax-statement-heading">Financial Year<span class="tax-financial-year">04-09-2016 - 04-09-2017</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-email"><i class="material-icons">mail_outline</i></span><span>Send E-mail</span></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="tax-action-holder"><span class="tax-download"><i class="material-icons">file_download</i></span><span>Download</span></p>
                        </div>
                    </div>
                </div>  
            </div>  
        </section> -->


        @endsection

    <script>
        $(document).ready(function(){
            $(document).on('click','.material-icons',function(){
                if ($(this).text() == 'keyboard_arrow_down') {
                    console.log($(this).text());
                    $(this).text('keyboard_arrow_up');
                }else if($(this).text() == 'keyboard_arrow_up'){

                    $(this).text('keyboard_arrow_down');               
                }                  
            });
        });
    </script>

    </body>
</html>
