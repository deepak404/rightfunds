<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details.css?v=1.1')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('css/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.css?v=1.1')}}">
    <link href="{{url('css/font-awesome.min.css?v=1.1')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>


<nav class="navbar">
  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/home"><img src="icons/logo.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="menu-text"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text menu-text-active"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    </div>
  </div>  
    
  
</nav>
  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Account Summary</p>
                      <p class="invest-text">Get your latest account statements</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="{{url('icons/zero_fee.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="{{url('icons/auto_inv.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="{{url('icons/tax_free.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>


          


          <div class = "row panel investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/portfolio_details')}}"><li class="list-headings active-page">Portfolio Details</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/investment_history')}}"><li class="list-headings">Investment History</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/tax_saving')}}"><li class="list-headings">Tax Saving Statements</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/withdraw_funds')}}"><li class="list-headings no-b-right">Withdraw Funds</li></a>
                    </div>                 
                  
               </ul>
             
             </div><!-- Heading ends -->



              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" id="export-row">

                  <div class="col-lg-10 col-md-10 col-sm-10">
                      <p class="hereshow">Here's how your statements look as of today,<span id="port-det-date"><?php echo date('j M Y');?></span></p>
                   </div>

                  <div class="col-lg-2 col-md-2 col-sm-2">
                    <a id="export-btn" href="/get_user_portfolio_document" target="_blank" class="btn btn-primary">EXPORT</a>
                  </div>

                </div><!-- export-row ends -->                
              </div><!-- Row ends -->


                    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                      <p class="funds-name">EQUITY FUNDS</p>
                      <div class="table-wrapper" id="equity-table-wrapper">

                      @if(array_key_exists('eq',$portfolio_details))
                        <table class="table" id="equity-table">
                        
                          <thead>
                            <tr>
                              <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                              <th>Folio Number</th>
                              <th>Amount invested(Rs.)</th>
                              <th>Units Held</th>
                              <th>Current Nav(Rs)</th>
                              <th>Current Value(Rs)</th>
                              <th>Net Returns(Rs)</th>
                              <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                              <th>Annualised Returns(Rs)</th>
                            </tr>
                          </thead> <button type="button" class="btn btn-primary" id="equity-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>
                          <tbody>
                            @foreach($portfolio_details['eq'] as $investment)
                              @if(is_array($investment))
                              <tr>
                                <td>{{$investment['scheme_name']}}</td>
                                <td>{{$investment['folio_number']}}</td>
                                <td>{{$investment['amount_invested']}}</td>
                                <td>{{$investment['units_held']}}</td>
                                <td>{{$investment['nav']}}</td>
                                <td>{{$investment['current_value']}}</td>
                                <td>{{$investment['net_return']}}</td>
                                <td class="ltc-amount">{{$investment['ltcg']}}</td>
                                <td>{{$investment['xirr']}}</td>
                              </tr>
                              @endif
                            @endforeach

                            <tr>
                              <td class="total bt-green ">TOTAL</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['eq']['total_amount_invested']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['eq']['total_units_held']}}</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['eq']['total_current_value']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['eq']['total_net_value']}}</td>
                              <td class="total  bt-green ltc-amount">{{$portfolio_details['eq']['total_ltcg']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['eq']['total_xirr']}}</td>      
                            </tr>
                          </tbody>
                        </table>

                      @else

                      <p class="text-center no_port_details">No Investments in this Portfolio yet.</p>

                      @endif

                      </div>
                    </div>


                  
                    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                      <p class="funds-name">DEBT FUNDS</p>
                      <div class="table-wrapper" id="debt-table-wrapper">

                      @if(array_key_exists('debt',$portfolio_details))
                        <table class="table" id="equity-table">
                          <thead>
                            <tr>
                              <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                              <th>Folio Number</th>
                              <th>Amount invested(Rs.)</th>
                              <th>Units Held</th>
                              <th>Current Nav(Rs)</th>
                              <th>Current Value(Rs)</th>
                              <th>Net Returns(Rs)</th>
                              <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                              <th>Annualised Returns(Rs)</th>
                            </tr>
                          </thead><button type="button" class="btn btn-primary" id="debt-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>
                          <tbody>
                            @foreach($portfolio_details['debt'] as $investment)
                              @if(is_array($investment))
                              <tr>
                                <td>{{$investment['scheme_name']}}</td>
                                <td>{{$investment['folio_number']}}</td>
                                <td>{{$investment['amount_invested']}}</td>
                                <td>{{$investment['units_held']}}</td>
                                <td>{{$investment['nav']}}</td>
                                <td>{{$investment['current_value']}}</td>
                                <td>{{$investment['net_return']}}</td>
                                <td class="ltc-amount">{{$investment['ltcg']}}</td>
                                <td>{{$investment['xirr']}}</td>    
                              </tr>
                              @endif
                            @endforeach

                            <tr>
                              <td class="total bt-green ">TOTAL</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['debt']['total_amount_invested']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['debt']['total_units_held']}}</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['debt']['total_current_value']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['debt']['total_net_value']}}</td>
                              <td class="total  bt-green ltc-amount">{{$portfolio_details['debt']['total_ltcg']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['debt']['total_xirr']}}</td>      
                            </tr>
                          </tbody>
                        </table>
                      @else
                        <p class="text-center no_port_details">No Investments in this Portfolio yet.</p>
                      @endif
                      </div>
                    </div>


                 
                    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                      <p class="funds-name">BALANCED FUNDS</p>
                      <div class="table-wrapper" id="hybrid-table-wrapper">
                      @if(array_key_exists('bal',$portfolio_details))
                       <table class="table" id="equity-table">
                          <thead>
                            <tr>
                              <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                              <th>Folio Number</th>
                              <th>Amount invested(Rs.)</th>
                              <th>Units Held</th>
                              <th>Current Nav(Rs)</th>
                              <th>Current Value(Rs)</th>
                              <th>Net Returns(Rs)</th>
                              <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                              <th>Annualised Returns(Rs)</th>
                            </tr>
                          </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>
                          <tbody>
                             @foreach($portfolio_details['bal'] as $investment)
                              @if(is_array($investment))
                              <tr>
                                <td>{{$investment['scheme_name']}}</td>
                                <td>{{$investment['folio_number']}}</td>
                                <td>{{$investment['amount_invested']}}</td>
                                <td>{{$investment['units_held']}}</td>
                                <td>{{$investment['nav']}}</td>
                                <td>{{$investment['current_value']}}</td>
                                <td>{{$investment['net_return']}}</td>
                                <td class="ltc-amount">{{$investment['ltcg']}}</td>
                                <td>{{$investment['xirr']}}</td>    
                              </tr>
                              @endif
                            @endforeach

                            <tr>
                              <td class="total bt-green ">TOTAL</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['bal']['total_amount_invested']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['bal']['total_units_held']}}</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['bal']['total_current_value']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['bal']['total_net_value']}}</td>
                              <td class="total  bt-green ltc-amount">{{$portfolio_details['bal']['total_ltcg']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['bal']['total_xirr']}}</td>      
                            </tr>
                            <tr>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>  
                            </tr>
                          </tbody>
                        </table>
                        @else
                          <p class="text-center no_port_details">No Investments in this Portfolio yet.</p>
                        @endif

                      </div>
                    </div>


                     <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="equity-div">
                      <p class="funds-name">ELSS FUNDS</p>
                      <div class="table-wrapper" id="elss-table-wrapper">
                      @if(array_key_exists('ts',$portfolio_details))
                       <table class="table" id="equity-table">
                          <thead>
                            <tr>
                              <th><div class ="col-xs-12" id="mutual">Mutual Fund scheme</div></th>
                              <th>Folio Number</th>
                              <th>Amount invested(Rs.)</th>
                              <th>Units Held</th>
                              <th>Current Nav(Rs)</th>
                              <th>Current Value(Rs)</th>
                              <th>Net Returns(Rs)</th>
                              <th><div class="col-xs-12" id="eligible">Eligible as LTCG(Rs)</div></th>
                              <th>Annualised Returns(Rs)</th>
                            </tr>
                          </thead><button type="button" class="btn btn-primary" id="hybrid-scroll"><i class="fa fa-angle-right" aria-hidden="true"></i>
                          </button>
                          <tbody>
                             @foreach($portfolio_details['ts'] as $investment)
                              @if(is_array($investment))
                              <tr>
                                <td>{{$investment['scheme_name']}}</td>
                                <td>{{$investment['folio_number']}}</td>
                                <td>{{$investment['amount_invested']}}</td>
                                <td>{{$investment['units_held']}}</td>
                                <td>{{$investment['nav']}}</td>
                                <td>{{$investment['current_value']}}</td>
                                <td>{{$investment['net_return']}}</td>
                                <td class="ltc-amount">{{$investment['ltcg']}}</td>
                                <td>{{$investment['xirr']}}</td>    
                              </tr>
                              @endif
                            @endforeach

                            <tr>
                              <td class="total bt-green ">TOTAL</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['ts']['total_amount_invested']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['ts']['total_units_held']}}</td>
                              <td class="total bt-green "></td>
                              <td class="total bt-green ">{{$portfolio_details['ts']['total_current_value']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['ts']['total_net_value']}}</td>
                              <td class="total  bt-green ltc-amount">{{$portfolio_details['ts']['total_ltcg']}}</td>
                              <td class="total bt-green ">{{$portfolio_details['ts']['total_xirr']}}</td>      
                            </tr>
                            <tr>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>
                              <td class="empty"></td>  
                            </tr>
                          </tbody>
                        </table>
                        @else
                          <p class="text-center no_port_details">No Investments in this Portfolio yet.</p>
                        @endif

                      </div>
                    </div>

              <div class="col-xs-12">
                <p class="subnote">These statements may be verified directly against those received from the mutual fund company</p>
                <p class="subnote" id="contact-sub">Kindly<a href="{{URL('/contact-us')}}"><span id="span-contact"> contact us</span></a> in case of any discrepency</p>
              </div>

              






          </div> <!-- investment-summary ends -->
    


          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>










  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/scroller.js?v=1.1')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
  


</script>

</body>
</html>
