<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.donut.js"></script>
  <script src="js/register.js"></script>
  <link rel="stylesheet" href="css/login.css?v=1.1">
  <link rel="stylesheet" href="css/login-responsive.css?v=1.1">
  <link rel="stylesheet" href="css/register-responsive.css?v=1.1">
  <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
</head>
<body>




  
<div class = "container">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-div">
      <img class = "login-logo center-block" src="icons/login_logo.png">
    </div>


    <!-- Modal -->
<div id="otpModal"  role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        
      </div>
      <div class="modal-body otp-modal-body">
            
          <p id="we-have">We have sent you an accesss code</p>
          <p id="sms"> via SMS for mobile number verification</p>

          <form name="otp-ver" id="otp-ver" class="form-inline">
            <div class="form-group otp-form-group">

              <div class="row otp-row">
                      
                <div class="col-xs-2 padding-lr-zero">
                  <input type="text" name="1" id="otp1" class="otp-field" minlength="1" maxlength="1" required>
                </div>
                <div class="col-xs-2 padding-lr-zero">
                  <input type="text" name="2" id="otp2" class="otp-field" minlength="1" maxlength="1" required>
                </div>
                <div class="col-xs-2 padding-lr-zero">
                  <input type="text" name="3" id="otp3" class="otp-field" minlength="1" maxlength="1" required>
                </div>
                <div class="col-xs-2 padding-lr-zero">
                  <input type="text" name="4" id="otp4" class="otp-field" minlength="1" maxlength="1" required>
                </div>    
              </div>   
            </div>
            <div id="otp_error_msg"></div>
            <div id="otp_success_msg"></div>
            <div class="row">
              <p id = "didn-t">Didn't receive the OTP ? <span id="resend">Resend OTP</span></p>
            </div>

            <input type="submit" name="verify" value="VERIFY" class="btn btn-primary verify-btn center-block">
          </form>

  </div>
</div>


</div>




</body>
</html>
