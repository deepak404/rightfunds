<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/preference.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/preference-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/loader.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.css?v=1.1')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('css/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>

<div class="loader" id="loader"></div>


<nav class="navbar">
  
  <div class="container">
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/"><img src="icons/logo.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="menu-text"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text menu-text-active"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';">LOG OUT</button>
      </ul>
    </div>
  </div>  
    
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Preference</p>
                      <p class="invest-text">Modify your Personal Details</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="icons/zero_fee.png">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="icons/auto_inv.png">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="icons/tax_free.png">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>


          


    

          <div class = "row investment-summary">
            <form id="pers-det-form" name="pers-det-form" enctype="multipart/form-data" method="post" action="/save_user_details" >
              {{csrf_field()}}
              <input type="hidden" value="{{\Auth::user()->acc_type}}" name="acctype" id="acctype">
              <div class="row" id="personal-det-row">
               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 ">
                 <p class="tab-heading">Personal Details</p>
                 <p class="tab-heading-sub">We use this information to create the account and process your investment.</p>
               </div>

               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" >

                   <div class = "col-xs-6">
                    <button type="button" class="btn btn-primary per-cancel-btn" id = "per-cancel-btn">CANCEL</button>
                   </div>

                   <div class = "col-xs-6 pad-r-z">
                    <button type="button" class="btn btn-primary per-edit-btn" id = "per-edit-btn">EDIT</button>
                     <!--<button type="button" class="btn btn-primary per-edit-btn" id = "bank-save-btn">SAVE</button>-->
                     <input type="submit" class="btn btn-primary per-edit-btn" id= "per-save-btn" value="SAVE"/>
                   </div>
               </div>
              </div>

              <div class="row">
              <!--<form id="pers-det-form" name="pers-det-form" >-->
                <div class="col-lg-12 col-md-12 col-sm-12 pad-20">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                      <div class="wrap">
                        @if(\Auth::user()->acc_type === "0")

                          @if(\Auth::user()->personalDetails->profile_img != '')
                          <img src="{{url('assets/profile_images/'.\Auth::user()->personalDetails->profile_img)}}" class="img-circle" id="per-det-pic">
                          @elseif(\Auth::user()->personalDetails->profile_img == '')
                            @if(\Auth::user()->personalDetails->sex == 'male')
                            <img src="{{url('assets/profile_images/male.png')}}" class="img-circle" id="per-det-pic">
                            @elseif(\Auth::user()->personalDetails->sex == 'female')
                            <img src="{{url('assets/profile_images/female.png')}}" class="img-circle" id="per-det-pic">
                            @endif
                          @endif

                        @elseif(\Auth::user()->acc_type === "1")

                          @if(\Auth::user()->corporateDetails->profile_img != '')
                          <img src="{{url('assets/profile_images/'.\Auth::user()->corporateDetails->profile_img)}}" class="img-circle" id="per-det-pic">
                          @elseif(\Auth::user()->corporateDetails->profile_img == '')
                          <img src="{{url('assets/profile_images/profile_pic.jpg')}}" class="img-circle" id="per-det-pic">
                          @endif

                        @endif
                        <a href="#" id="upload_pic_icon">
                          <span id="upload-pre-pic">
                            <label for="avatar" id="avatar_label">
                              <img src="icons/pre_pro_edit.png" style="cursor: pointer;">
                            </label> 
                          </span>
                        </a>
                      </div>
                      <p id="img-error">Cannot Update the image Now</p>
                      <!--<form id="change_pic_form" id="change_pic_form" method="post" action="/save_user_image" >
                          {{csrf_field()}}
                        <input type="file" name="avatar" id="avatar">
                        <input type="submit" value="Save Image">
                      </form>-->
                      <input type="file" name="avatar" id="avatar">
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">


                    
                          <p class = "form-helper">Name</p>
                          <div class = "form-group">
                            <input class = "signup-f-input" type = "text" name = "name" id = "name" value="{{\Auth::user()->name}}" required disabled>
                              <span class="help-block help-center errors" id="name-error"></span>
                          </div>

                        <!--<p class = "form-helper">Password</p>
                        <div class = "form-group">
                          <input class = "signup-f-input" type = "text" name = "per-pass" id = "per-pass" value="" required disabled>
                        </div>-->

                        <p class = "form-helper">Mobile Number</p>
                        <div class="form-group">
                          <div class="input-group" id="per-phone-group">
                            <span class="input-group-addon" id="basic-addon1">+91</span>
                            <input type="number" id = "mobile" name = "mobile" class="form-control" aria-describedby="basic-addon1" value="{{\Auth::user()->mobile}}" required disabled>
                              <span class="help-block help-center errors" id="mobile-error"></span>
                          </div>
                        </div>

                        <p class = "form-helper">Communication Address</p>
                  
                        <div class = "form-group">
                        @if(\Auth::user()->acc_type === "0")
                          <textarea id="address" cols="27" rows="4" name = "address" disabled>{{ \Auth::user()->personalDetails->address}}</textarea>
                        @elseif(\Auth::user()->acc_type === "1")
                          <textarea id="address" cols="27" rows="4" name = "address" disabled>{{ \Auth::user()->corporateDetails->address}}</textarea>
                        @endif
                            <span class="help-block help-center errors" id="addr-error"></span>
                        </div>


                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <p class = "form-helper">Email ID</p>
                      <div class = "form-group">
                        <input class = "signup-f-input" type = "text" name = "email" value="{{\Auth::user()->email}}" id = "email" required disabled>
                        
                          <span class="help-block help-center errors" id="email-error"></span>
                        
                      </div>
                      
                      <p class = "form-helper">Pincode</p>
                      <div class = "form-group">
                        @if(\Auth::user()->acc_type === "0")
                          <input class = "signup-f-input" type = "text" name = "pincode" id = "pincode" value="{{ \Auth::user()->personalDetails->pincode}}" required disabled>
                        @elseif(\Auth::user()->acc_type === "1")
                          <input class = "signup-f-input" type = "text" name = "pincode" id = "pincode" value="{{ \Auth::user()->corporateDetails->pincode}}" required disabled>
                        @endif
                        <span class="help-block help-center errors" id="pincode-error"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </form>

                          <div class="hr"></div>

            <form id="password-det-form" name="password-det-form" method="post" action="/save_bank_details" >
          {{csrf_field()}}
          
            <div class = "row pad-15" id="bank-det-row" >
                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6" >
                   <p class="tab-heading">Change Password</p>
                   <p class="tab-heading-sub">Change your password regularly to keep your account secure.</p>
                 </div>

                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                   <div class = "col-xs-6">
                    <button type="button" class="btn btn-primary per-cancel-btn" id = "password-cancel-btn">CANCEL</button>
                   </div>

                   <div class = "col-xs-6 pad-r-z">
                    <button type="button" class="btn btn-primary per-edit-btn" id = "password-edit-btn">EDIT</button>
                     <!--<button type="button" class="btn btn-primary per-edit-btn" id = "bank-save-btn">SAVE</button>-->
                     <input type="submit" class="btn btn-primary per-edit-btn" id= "password-save-btn" value="SAVE"/>
                   </div>                  
                 </div>
            </div>

               <div class="row" id="password-det-cont">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-l-zero">

                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res">
                        <p class = "form-helper">Current Password</p>
                        <div class = "form-group">
                         
                            <input class = "signup-f-input" type = "password" name = "current_password" id = "current_password"  required disabled>
                            <span class="help-block help-center errors" id="current_password_error"></span>
                        </div>                       
                   </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res">
                        <p class = "form-helper">New Password</p>
                        <div class = "form-group">
                          
                            <input class = "signup-f-input" type = "password" name = "new_password" id = "new_password" required disabled>
                            <span class="help-block help-center errors" id="new_password_error"></span>
                          
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res">
                        <p class = "form-helper">Confirm Password</p>
                        <div class = "form-group">
                          
                            <input class = "signup-f-input" type = "password" name = "confirm_password" id = "confirm_password" required disabled>
                            <span class="help-block help-center errors" id="confirm_password_error"></span>
                          
                        </div>
                    </div>


                 </div>

               </div>
            </form>
          

          <div class="hr"></div>

          <form id="bank-det-form" name="bank-det-form" method="post" action="/save_bank_details" >
          {{csrf_field()}}
          <input type="hidden" value="{{\Auth::user()->acc_type}}" name="acctype" id="acctype">
            <div class = "row pad-15" id="bank-det-row" >
                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6" >
                   <p class="tab-heading">Bank Details</p>
                   <p class="tab-heading-sub">We use this information to invest/redeem money from your bank account.</p>
                 </div>


                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                   <div class = "col-xs-6">
                    <button type="button" class="btn btn-primary per-cancel-btn" id = "bank-cancel-btn">CANCEL</button>
                   </div>

                   <div class = "col-xs-6 pad-r-z">
                    <button type="button" class="btn btn-primary per-edit-btn" id = "bank-edit-btn">EDIT</button>
                     <!--<button type="button" class="btn btn-primary per-edit-btn" id = "bank-save-btn">SAVE</button>-->
                     <input type="submit" class="btn btn-primary per-edit-btn" id= "bank-save-btn" value="SAVE"/>
                   </div>                  
                 </div>
            </div>

               <div class="row" id="bank-det-cont">
                 <div class="col-lg-12 col-md-12 col-sm-12 padding-l-zero">

                   <div class="col-lg-4 col-md-4 col-sm-4">
                        <p class = "form-helper">Name</p>
                        <div class = "form-group">
                          @if(\Auth::user()->acc_type === "0")
                          <input class = "signup-f-input" type = "text" name = "bank_name" id = "bank_name" value="{{ \Auth::user()->bankDetails->where('bank_type',1)->first()->acc_name}}" required disabled>
                          @elseif(\Auth::user()->acc_type === "1")
                            <input class = "signup-f-input" type = "text" name = "bank_name" id = "bank_name" value="{{ \Auth::user()->bankDetails->where('bank_type',1)->first()->acc_name}}" required disabled>
                          @endif
                          <span class="help-block help-center errors" id="bank_name_error"></span>
                          
                        </div>

                        <p class = "form-helper">IFSC Code</p>
                        <div class = "form-group">
                          @if(\Auth::user()->acc_type === "0")
                          <input class = "signup-f-input" type = "text" name = "bank_ifsc" id = "bank-ifsc" value="{{ \Auth::user()->bankDetails->where('bank_type',1)->first()->ifsc_code}}" required disabled>
                          @elseif(\Auth::user()->acc_type === "1")
                            <input class = "signup-f-input" type = "text" name = "bank_ifsc" id = "bank_ifsc" value="{{ \Auth::user()->bankDetails->where('bank_type',1)->first()->ifsc_code}}" required disabled>
                          @endif
                          <span class="help-block help-center errors" id="bank_ifsc_error"></span>
                        </div>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <p class = "form-helper">Bank Account Number</p>
                        <div class = "form-group">
                           @if(\Auth::user()->acc_type === "0")
                            <input class = "signup-f-input" type = "text" name = "bank_number" id = "bank_number" value="{{ \Auth::user()->bankDetails->where('bank_type',1)->first()->acc_no}}" required disabled>
                          @elseif(\Auth::user()->acc_type === "1")
                            <input class = "signup-f-input" type = "text" name = "bank_number" id = "bank_number" value="{{ \Auth::user()->bankDetails->where('bank_type',1)->first()->acc_no}}" required disabled>
                          @endif
                            <span class="help-block help-center errors" id="bank_number_error"></span>
                          
                        </div>

                        <p class = "form-helper">Account type</p>
                       <div class="styled-select slate" >
                            <select name="bank_account_type" id="bank_account_type" disabled>
                              <?php 
                                $acctype = 'savings';
                                $acc_type_options = array(
                                  'current' => 'Current',
                                  'savings'=> 'Savings',
                                  'NRE' => 'NRE'
                                );
                                if(\Auth::user()->acc_type === "0") {
                                  $acctype = \Auth::user()->bankDetails->where('bank_type',1)->first()->acc_type;
                                } elseif(\Auth::user()->acc_type === "1"){
                                  $acctype = \Auth::user()->bankDetails->where('bank_type',1)->first()->acc_type;
                                }
                              ?>
                              @foreach($acc_type_options as $key => $value)
                                @if($acctype == $key)
                                  <option value="{{$key}}" selected>{{$value}}</option>
                                @elseif($acctype != $key)
                                  <option value="{{$key}}" >{{$value}}</option>
                                @endif
                              @endforeach
                                  <!--<option value="current">Current Account</option>-->
                            </select>
                            <span class="help-block help-center errors" id="bank_type_error"></span>
                          </div>
                    </div>

                 </div>

               </div>
            </form>


          

          <div class="hr"></div>
        @foreach(\Auth::user()->nomineeDetails as $nominee)
          @if($nominee->nomi_type === "1")
            <form id="nominee1-det-form" name="nominee1-det-form" method="post" action="/save_nominee1_details" >
              {{csrf_field()}}
              <div class="row" id="nominee-det-row"> 
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                 <p class="tab-heading">Nominee Details</p>
                 <p class="tab-heading-sub">Add/remove a nominee for your investment.</p>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                   <div class = "col-xs-6">
                    <button type="button" class="btn btn-primary per-cancel-btn" id = "nominee1-cancel-btn">CANCEL</button>
                   </div>

                   <div class = "col-xs-6 pad-r-z">
                    <button type="button" class="btn btn-primary per-edit-btn" id = "nominee1-edit-btn">EDIT</button>
                     <!--<button type="button" class="btn btn-primary per-edit-btn" id = "bank-save-btn">SAVE</button>-->
                     <input type="submit" class="btn btn-primary per-edit-btn" id= "nominee1-save-btn" value="SAVE"/>
                   </div>                  
                 </div>

              </div><!--Nominee Det row ends  -->

              <div class="row " id="nominee1-det-cont" >
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="nominee-1">
                    <p class = "nominee">Nominee 1</p>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res">
                      <p class = "form-helper">Name</p>
                      <div class = "form-group">
                        <input class = "signup-f-input" type = "text" name = "nominee1_name" id = "nominee1_name" value="{{ $nominee->nomi_name }}" required disabled>
                          <span class="help-block help-center errors" id="nominee1_name_error"></span>
                      </div>

                      <p class = "form-helper">Address</p>
                      <div class = "form-group">
                        <textarea id="nominee1_address" cols="27" rows="4" name = "nominee1_address" required disabled>{{ $nominee->nomi_addr }}</textarea>
                        <span class="help-block help-center errors" id="nominee1_address_error"></span>
                      </div>
                    </div> <!-- Name and address Div ends(col-lg-4)-->


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res"><!-- col-lg-4 starts-->
                      <p class = "form-helper">Relationship</p>
                      <div class = "form-group"><!--Relationship div starts -->
                        <input class = "signup-f-input" type = "text" name = "nominee1_relationship" id = "nominee1_relationship" value="{{ $nominee->nomi_relationship}}" required disabled>
                        <span class="help-block help-center errors" id="nominee1_rel_error"></span>
                      </div><!--Relationship div ends -->

                      <p class = "form-helper">Percentage of Holding</p>
                      <div class="input-group perc-group">
                        <input type="number" class="form-control" aria-describedby="basic-addon2" name="nominee1_percent" id="nominee1_percent" value="{{ $nominee->nomi_holding}}" required disabled>
                        <span class="input-group-addon" id="basic-addon2">%</span>
                      </div>
                      <span class="help-block help-center errors" id="nominee1_holding_error"></span>
                    </div><!-- col-lg-4 ends-->
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res"><!-- DOB STARTS-->
                      <p class = "form-helper">Date of birth</p>
                      <div class = "form-group">

                      @if($nominee->nomi_dob == '')
                        <input class = "signup-f-input" type = "text" name = "nominee1_dob" id = "nominee1_dob" value=" " required disabled>
                      @else
                        <input class = "signup-f-input" type = "text" name = "nominee1_dob" id = "nominee1_dob" value="{{ date('d-m-Y', strtotime($nominee->nomi_dob))}}" required disabled>
                      @endif
                        
                        <span class="help-block help-center errors" id="nominee1_dob_error"></span>
                      </div>                        
                    </div> <!-- DOB ENDS-->
                  </div>
                </div>

                <div class="col-lg-12 col-xs-12 col-md-12">
                   <div class = "col-lg-12 col-md-12">
                        <div class = "col-lg-8 col-md-8 col-sm-8">
                            <div class = "col-xs-12 padding-lr-zero checkbox-div"><input type="checkbox" id = "copy1-addr" disabled>
                                <label for="copy1-addr"><span class="checkbox"><img src="icons/check_dis.png"></span></label><span class = "copy-inv-addr">Copy address of the investor</span>
                             </div>
                        </div>
                   </div>
                </div>
              </div> <!-- Nominee det content row ends -->
            </form>
            <div class="hr"></div>
          @elseif($nominee->nomi_type === "2")
          <form id="nominee2-det-form" name="nominee2-det-form" method="post" action="/save_nominee2_details" >
            {{csrf_field()}}
             <div class="row" id="nominee2-det-row">

                    <div class="col-lg-12 col-md-12 col-sm-12">
                      
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                    </div>

                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                   <div class = "col-xs-6">
                    <button type="button" class="btn btn-primary per-cancel-btn" id = "nominee2-cancel-btn">CANCEL</button>
                   </div>

                   <div class = "col-xs-6 pad-r-z">
                    <button type="button" class="btn btn-primary per-edit-btn" id = "nominee2-edit-btn">EDIT</button>
                     <!--<button type="button" class="btn btn-primary per-edit-btn" id = "bank-save-btn">SAVE</button>-->
                     <input type="submit" class="btn btn-primary per-edit-btn" id= "nominee2-save-btn" value="SAVE"/>
                   </div>                  
                 </div>
                </div>

              </div><!--Nominee Det row ends  -->


            <div class="row " id="nominee2-det-cont" >

            <div class="col-lg-12 col-sm-12 col-xs-12">
              <div class="col-lg-12 col-md-12 col-sm-12" id="nominee-2">
                <p class = "nominee">Nominee 2</p>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res">
                
                  <p class = "form-helper">Name</p>
                  <div class = "form-group">
                    <input class = "signup-f-input" type = "text" name = "nominee2_name" id = "nominee2_name" value="{{ $nominee->nomi_name }}" required disabled>
                    <span class="help-block help-center errors" id="nominee2_name_error"></span>
                  </div>

                  <p class = "form-helper">Address</p>
                  <div class = "form-group">
                   <textarea id="nominee2_address" cols="27" rows="4" name = "nominee2_address" disabled>{{ $nominee->nomi_addr }}</textarea>
                   <span class="help-block help-center errors" id="nominee2_address_error"></span>
                  </div>

                </div> <!-- Name and address Div ends(col-lg-4)-->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res"><!-- col-lg-4 starts-->

                  <p class = "form-helper">Relationship</p>
                  <div class = "form-group"><!--Relationship div starts -->
                    <input class = "signup-f-input" type = "text" name = "nominee2_relationship" id = "nominee2_relationship" value="{{ $nominee->nomi_relationship}}" required disabled>
                    <span class="help-block help-center errors" id="nominee2_rel_error"></span>
                  </div><!--Relationship div ends -->

                  <p class = "form-helper">Percentage of Holding</p>
                  <div class="input-group perc-group">
                    <input type="number" class="form-control" aria-describedby="basic-addon2" name="nominee2_percent" id="nominee2_percent" value="{{ $nominee->nomi_holding}}" disabled>
                    <span class="input-group-addon" id="basic-addon2">%</span>
                  </div>
                  <span class="help-block help-center errors" id="nominee2_holding_error"></span>
                </div><!-- col-lg-4 ends-->

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res"><!-- DOB STARTS-->
                  <p class = "form-helper">Date of birth</p>
                  <div class = "form-group">
                    @if($nominee->nomi_dob == '')
                        <input class = "signup-f-input" type = "text" name = "nominee2_dob" id = "nominee2_dob" value=" " required disabled>
                      @else
                        <input class = "signup-f-input" type = "text" name = "nominee2_dob" id = "nominee2_dob" value="{{ date('d-m-Y', strtotime($nominee->nomi_dob))}}" required disabled>
                      @endif
                    <span class="help-block help-center errors" id="nominee2_dob_error"></span>
                  </div>                        
                </div> <!-- DOB ENDS-->
              </div><!-- nominee-2 ends -->
            </div>    
            <div class="col-lg-12 col-xs-12 col-md-12">
              <div class = "col-lg-12 col-md-12 col-sm-12">
                <div class = "col-lg-8 col-md-8 col-sm-8">
                  <div class = "col-xs-12 padding-lr-zero checkbox-div"><input type="checkbox" id = "copy2-addr" disabled>
                      <label for="copy2-addr"><span class="checkbox"><img src="icons/check_dis.png"></span></label><span class = "copy-inv-addr">Copy address of the investor</span>
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- Nominee det content row ends -->
        </form>
        <div class="hr"></div>
        @elseif($nominee->nomi_type === "3")
            <form id="nominee3-det-form" name="nominee3-det-form" method="post" action="/save_nominee3_details" > 
            {{csrf_field()}}
              <div class="row" id="nominee-det-row">
                <div class="col-lg-12 col-xs-12 col-md-12">

                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                  </div>

                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                   <div class = "col-xs-6">
                    <button type="button" class="btn btn-primary per-cancel-btn" id = "nominee3-cancel-btn">CANCEL</button>
                   </div>

                   <div class = "col-xs-6 pad-r-z">
                    <button type="button" class="btn btn-primary per-edit-btn" id = "nominee3-edit-btn">EDIT</button>
                     <!--<button type="button" class="btn btn-primary per-edit-btn" id = "bank-save-btn">SAVE</button>-->
                     <input type="submit" class="btn btn-primary per-edit-btn" id= "nominee3-save-btn" value="SAVE"/>
                   </div>                  
                 </div>

               </div>
              </div><!--Nominee Det row ends  -->

              <div class="row" id="nominee3-det-cont" >
                <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="col-lg-12 col-md-12 col-sm-12" id="nominee-3">
                  <p class = "nominee">Nominee 3</p>

                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res">

                    <p class = "form-helper">Name</p>
                    <div class = "form-group">
                      <input class = "signup-f-input" type = "text" name = "nominee3_name" id = "nominee3_name"  value="{{ $nominee->nomi_name }}" required disabled>
                      <span class="help-block help-center errors" id="nominee3_name_error"></span>
                    </div>

                    <p class = "form-helper">Address</p>
                    <div class = "form-group">
                     <textarea id="nominee3_address" cols="27" rows="4" name = "nominee3_address" disabled>{{ $nominee->nomi_addr }}</textarea>
                      <span class="help-block help-center errors" id="nominee3_address_error"></span>
                    </div>

                  </div> <!-- Name and address Div ends(col-lg-4)-->


                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res"><!-- col-lg-4 starts-->

                    <p class = "form-helper">Relationship</p>
                    <div class = "form-group"><!--Relationship div starts -->
                      <input class = "signup-f-input" type = "text" name = "nominee3_relationship" id = "nominee3_relationship" value="{{ $nominee->nomi_relationship}}" required disabled>
                      <span class="help-block help-center errors" id="nominee3_rel_error"></span>
                    </div><!--Relationship div ends -->

                    <p class = "form-helper">Percentage of Holding</p>
                    <div class="input-group perc-group">
                      <input type="number" class="form-control" aria-describedby="basic-addon2" name="nominee3_percent" id="nominee3_percent" value="{{ $nominee->nomi_holding}}" disabled>
                      <span class="input-group-addon" id="basic-addon2">%</span>
                    </div>
                    <span class="help-block help-center errors" id="nominee3_holding_error"></span>

                  </div><!-- col-lg-4 ends-->

                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad_res"><!-- DOB STARTS-->
                    <p class = "form-helper">Date of birth</p>
                    <div class = "form-group">
                      @if($nominee->nomi_dob == '')
                        <input class = "signup-f-input" type = "text" name = "nominee3_dob" id = "nominee3_dob" value=" " required disabled>
                      @else
                        <input class = "signup-f-input" type = "text" name = "nominee3_dob" id = "nominee3_dob" value="{{ date('d-m-Y', strtotime($nominee->nomi_dob))}}" required disabled>
                      @endif
                      <span class="help-block help-center errors" id="nominee3_dob_error"></span>
                    </div>                        
                  </div> <!-- DOB ENDS-->
                    

                </div><!-- nominee-3 ends -->
                <div class = "col-lg-12 col-md-12 col-sm-12">
                  <div class = "col-lg-8 col-md-8 col-sm-8">
                    <div class = "col-xs-12 padding-lr-zero checkbox-div"><input type="checkbox" id = "copy3-addr" disabled>
                      <label for="copy3-addr" disabled><span class="checkbox"><img src="icons/check_dis.png"></span></label><span class = "copy-inv-addr">Copy address of the investor</span>
                    </div>
                  </div>
                </div>
              </div>
              </div> <!-- Nominee det content row ends -->
            </form>
            @endif
            @endforeach












          </div> 
    </div>


          <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
                   <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
                  </ul>

                    <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="icons/twitter.png"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="icons/facebook.png"></a>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="icons/linked_in.png"></a>
                    </div>

                    <div class="row padding-lr-zero footer-comp-div">
                      <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                    </div>

                </div>
            </div>



          </div>
      

</div> <!--Content Ends -->

</div>

    <!-- Modal -->
<div id="preferenceInfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Great !</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">
            <button type="button" data-dismiss = "modal" class="btn btn-primary text-center" id="notify-done">DONE</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <script src="{{url('js/preference.js?v=1.1')}}"></script>
  <script src="{{url('js/checkbox.js?v=1.1')}}"></script>
  <script src="{{url('js/loader.js?v=1.1')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</body>
</html>
