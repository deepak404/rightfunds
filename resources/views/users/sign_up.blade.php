<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="css/bootstrap.min.css">
  
    <link rel="stylesheet" href="css/sign-up.css">
    
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/sign-up-responsive.css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
    <link rel="stylesheet" href="css/jqueryui.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <link rel="stylesheet" href="{{url('css/error.css')}}">

</head>
<body>


  <nav class="navbar">
  
    
    <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle btn btn-primary back-btn">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </button>
            <a class="navbar-brand" href="/"><img src="icons/login_logo.png"></a>
          </div><!--Navbar header ends -->

          <div class="collapse navbar-collapse" id="myNavbar">         
            <ul class="nav navbar-nav navbar-right navbar-btn ">
              <button type="button" class="btn btn-primary nav-back-btn" onclick="location.href='/login';"><i class="fa fa-chevron-left" aria-hidden="true"></i>
      &nbsp; BACK</button>
            </ul>
          </div>
    </div>
    
    
  
</nav>


<div class="container">

    <div class="content">


        <div class=" row profile-bar">
            <p class="is-head" id="acct_acti">Account Activation</p>
            <p class="is-details" id="acct_deta">Activate your account to start investing</p>
        </div>
        
        <div class="row  profile-bar account-act-war">

          <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <p class="is-head">Sign up with Rightfunds</p>
                <p class="is-details">Begin investing and achieve your goals!</p>
              
          </div> -->

          <!--<div class = "row adv-row">

              <div class = "col-lg-12 col-md-12 col-sm-12 signup-adv-div">

                  <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                      <img src="icons/zero_fee.png" class = "center-block">
                      <p class = "adv-head">Free Account</p>
                      <p class = "adv-exp">It is and always will be free to open a Rightfunds account. No fees. No hidden costs. No subscription charges.</p>
                  </div>

                  <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                    <img src="icons/security.png" class = "center-block">
                    <p class = "adv-head">Bank Grade Security</p>
                    <p class = "adv-exp">We employ bank grade security and practise offline data storage to ensure your information is safe at all times.</p>

                  </div>

                  <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                    <img src="icons/privacy.png" class = "center-block">
                    <p class = "adv-head">Complete Privacy</p>
                    <p class = "adv-exp">We will never share any of your information with third parties. Your privacy is of utmost importance to us.</p>

                  </div>
              </div> <!-- signup-adv-div ends -->
          <!--</div><!-- adv-row -->
          <form action="{{url('/personal_details')}}" id = "signup_personal" method="POST" role = "form">
          <div class = "row form-row">

            <div class = "col-lg-4 col-sm-4 col-md-4 per-det-div">
              <p class = "per-det">Personal Details</p>
              <p id = "per-det-info">We use this information to create your account and process your investment.</p>
            </div><!--per-det-div ends -->

            <div class = "col-lg-7 col-md-7 col-sm-7 col-xs-12 padding-lr-zero form-div">
                 
                 <div class = "row">
                    <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                      <p class = "form-helper">Full Name</p>
                      <div class = "form-group">
                        <input class = "signup-f-input form-inp" type = "text" name = "f-full-name" id = "full_name" value="Naveen" required>
                        <span class="help-block help-center" id="name_error"></span>
                      </div>
                    </div>


                    <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                      <p class = "form-helper">PAN Number</p>

                      <div class = "form-group">
                        <input class = "signup-f-input" type = "text" name = "pan_number" id = "pan_number" value="BFRPN4910B" required minlength="10" maxlength="10">
                        <span class="help-block help-center" id="pan_error"></span>
                      </div>

                    </div>

                 </div><!-- row ends -->

                 <div class ="row row-two">

                    <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <p class = "form-helper">Date of Birth</p>
                      <div class = "form-group">
                        <input class = "signup-f-input form-inp" type = "text" name = "f-s-dob" id = "dob" value="12-06-1995" required>
                        <span class="help-block help-center" id="dob_error"></span>
                      </div>
                    </div>


                    <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <p class = "form-helper">Sex</p>
                      <div class="sex-radio-divs">

                        <div class = " male-radio-div padding-lr-zero radio-inline">
                          <input type="radio"  name="sex" id="male" value = "male" checked>
                          <label for="male"><span class="radio">Male</span></label>
                        </div>

                        <div class = " female-radio-div padding-lr-zero radio-inline">
                          <input type="radio" name="sex" id="female" value = "female">
                          <label for= "female"><span class="radio">Female</span></label>
                        </div>
                        <span class="help-block help-center" id="sex_error"></span>
                      </div>
                      <p id = "sel_sex" style="display: none;">Select your Gender</p>
                    </div>


                 </div><!-- row ends -->

                 
                 <div class ="row">

                    <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <p class = "form-helper">Occupation Type</p>

                              
                              <select id="occupation" name="occupation">

                                <option>Service</option>
                                <option>Others</option>
                                <option>Private Sector</option>
                                <option>Professional</option>
                                <option>Business</option>
                                <option>Public Sector</option>
                                <option>Government Sector</option>
                                <option>Self Employed</option>
                                <option>Retired</option>
                                <option>Housewife</option>
                                <option>Student</option>
                                <option>Not Categorized</option>

                              </select>
                              <span class="help-block help-center" id="employment_error"></span>
                    </div>

                    <div class = "col-lg-6 col-md-6 col-sm-6">
                      <p class = "form-helper">Resident</p>
                      <div class="resident-radio-divs">

                        <div class = " res-radio-div padding-lr-zero radio-inline">
                          <input type="radio"  name="resident" id="res-ind" value = "res-ind" checked>
                          <label for="res-ind"><span class="radio">Indian</span></label>
                        </div>

                        <div class = " nonres-radio-div padding-lr-zero radio-inline">
                          <input type="radio" name="resident" id="non-res" value = "non-res">
                          <label for= "non-res"><span class="radio">NRI</span></label>
                        </div>
                        <span class="help-block help-center" id="resident_error"></span>
                      </div>
                      <p id = "sel_res" style="display: none;">Select your resident</p>
                    </div>


                    <div class = "row row-three">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                              <p class = "form-helper">City of Birth</p>
                              <div class = "form-group">
                                <input class = "signup-f-input form-inp" type = "text" name = "birth_city" value="chennai" id = "birth_city" required>
                                <span class="help-block help-center" id="city_error"></span>
                              </div>
                            </div>



                            <div class = "col-lg-6 col-md-6 col-sm-6">
                              <p class = "form-helper">Marital Status</p>
                              <div class="marital-radio-divs">

                                <div class = " single-radio-div padding-lr-zero radio-inline">
                                  <input type="radio"  name="marital_status" id="single" value = "single" checked >
                                  <label for="single"><span class="radio">Single</span></label>
                                </div>

                                <div class = " married-radio-div padding-lr-zero radio-inline">
                                  <input type="radio" name="marital_status" id="married" value = "married">
                                  <label for= "married"><span class="radio">Married</span></label>
                                </div>
                                <span class="help-block help-center" id="status_error"></span>
                              </div>

                              <p id = "sel_mar" style="display: none;">Select your Marital Status</p>
                            </div>

                            
                        </div>
                    </div><!-- row ends -->

                    <div class = "row">                    
                        <div class="col-lg-12 col-md-12 col-sm-12">

                            <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                              <p class = "form-helper">Father/Spouse Full Name as per PAN</p>
                              <div class = "form-group">
                                <input class = "signup-f-input form-inp" type = "text" name = "f-s-name" value = "Maran" id = "father_name" required>
                                <span class="help-block help-center" id="fname_error"></span>
                              </div>
                            </div>

                            <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <p class = "form-helper">Mother's Full Name as per PAN</p>
                              <input class = "signup-f-input form-inp" type = "text" name = "f-m-name" value="Nagamani" id = "mother_name" required>
                              <span class="help-block help-center" id="mname_error"></span>
                            </div>
                        </div>
                    </div><!-- row ends -->

                    <div class = "row">                    
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                          <p class = "form-helper">PIN</p>
                          <div class = "form-group">
                            <input class = "signup-f-input pincode" type = "text" name = "f-s-pin" value="600049" id = "pincode" required minlength="6" maxlength="6">
                            <span class="help-block help-center" id="pincode_error"></span>
                          </div>
                        </div>

                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <p class = "form-helper">Nationality</p>
                          <div class = "form-group">
                  
                            <select name="f-s-nat" id="nationality" class="signup-f-input" required>
                              <option value="indian">Indian</option>
                              <option value="afghan">Afghan</option>
                              <option value="albanian">Albanian</option>
                              <option value="algerian">Algerian</option>
                              <option value="american">American</option>
                              <option value="andorran">Andorran</option>
                              <option value="angolan">Angolan</option>
                              <option value="antiguans">Antiguans</option>
                              <option value="argentinean">Argentinean</option>
                              <option value="armenian">Armenian</option>
                              <option value="australian">Australian</option>
                              <option value="austrian">Austrian</option>
                              <option value="azerbaijani">Azerbaijani</option>
                              <option value="bahamian">Bahamian</option>
                              <option value="bahraini">Bahraini</option>
                              <option value="bangladeshi">Bangladeshi</option>
                              <option value="barbadian">Barbadian</option>
                              <option value="barbudans">Barbudans</option>
                              <option value="batswana">Batswana</option>
                              <option value="belarusian">Belarusian</option>
                              <option value="belgian">Belgian</option>
                              <option value="belizean">Belizean</option>
                              <option value="beninese">Beninese</option>
                              <option value="bhutanese">Bhutanese</option>
                              <option value="bolivian">Bolivian</option>
                              <option value="bosnian">Bosnian</option>
                              <option value="brazilian">Brazilian</option>
                              <option value="british">British</option>
                              <option value="bruneian">Bruneian</option>
                              <option value="bulgarian">Bulgarian</option>
                              <option value="burkinabe">Burkinabe</option>
                              <option value="burmese">Burmese</option>
                              <option value="burundian">Burundian</option>
                              <option value="cambodian">Cambodian</option>
                              <option value="cameroonian">Cameroonian</option>
                              <option value="canadian">Canadian</option>
                              <option value="cape verdean">Cape Verdean</option>
                              <option value="central african">Central African</option>
                              <option value="chadian">Chadian</option>
                              <option value="chilean">Chilean</option>
                              <option value="chinese">Chinese</option>
                              <option value="colombian">Colombian</option>
                              <option value="comoran">Comoran</option>
                              <option value="congolese">Congolese</option>
                              <option value="costa rican">Costa Rican</option>
                              <option value="croatian">Croatian</option>
                              <option value="cuban">Cuban</option>
                              <option value="cypriot">Cypriot</option>
                              <option value="czech">Czech</option>
                              <option value="danish">Danish</option>
                              <option value="djibouti">Djibouti</option>
                              <option value="dominican">Dominican</option>
                              <option value="dutch">Dutch</option>
                              <option value="east timorese">East Timorese</option>
                              <option value="ecuadorean">Ecuadorean</option>
                              <option value="egyptian">Egyptian</option>
                              <option value="emirian">Emirian</option>
                              <option value="equatorial guinean">Equatorial Guinean</option>
                              <option value="eritrean">Eritrean</option>
                              <option value="estonian">Estonian</option>
                              <option value="ethiopian">Ethiopian</option>
                              <option value="fijian">Fijian</option>
                              <option value="filipino">Filipino</option>
                              <option value="finnish">Finnish</option>
                              <option value="french">French</option>
                              <option value="gabonese">Gabonese</option>
                              <option value="gambian">Gambian</option>
                              <option value="georgian">Georgian</option>
                              <option value="german">German</option>
                              <option value="ghanaian">Ghanaian</option>
                              <option value="greek">Greek</option>
                              <option value="grenadian">Grenadian</option>
                              <option value="guatemalan">Guatemalan</option>
                              <option value="guinea-bissauan">Guinea-Bissauan</option>
                              <option value="guinean">Guinean</option>
                              <option value="guyanese">Guyanese</option>
                              <option value="haitian">Haitian</option>
                              <option value="herzegovinian">Herzegovinian</option>
                              <option value="honduran">Honduran</option>
                              <option value="hungarian">Hungarian</option>
                              <option value="icelander">Icelander</option>
                              <option value="indian">Indian</option>
                              <option value="indonesian">Indonesian</option>
                              <option value="iranian">Iranian</option>
                              <option value="iraqi">Iraqi</option>
                              <option value="irish">Irish</option>
                              <option value="israeli">Israeli</option>
                              <option value="italian">Italian</option>
                              <option value="ivorian">Ivorian</option>
                              <option value="jamaican">Jamaican</option>
                              <option value="japanese">Japanese</option>
                              <option value="jordanian">Jordanian</option>
                              <option value="kazakhstani">Kazakhstani</option>
                              <option value="kenyan">Kenyan</option>
                              <option value="kittian and nevisian">Kittian and Nevisian</option>
                              <option value="kuwaiti">Kuwaiti</option>
                              <option value="kyrgyz">Kyrgyz</option>
                              <option value="laotian">Laotian</option>
                              <option value="latvian">Latvian</option>
                              <option value="lebanese">Lebanese</option>
                              <option value="liberian">Liberian</option>
                              <option value="libyan">Libyan</option>
                              <option value="liechtensteiner">Liechtensteiner</option>
                              <option value="lithuanian">Lithuanian</option>
                              <option value="luxembourger">Luxembourger</option>
                              <option value="macedonian">Macedonian</option>
                              <option value="malagasy">Malagasy</option>
                              <option value="malawian">Malawian</option>
                              <option value="malaysian">Malaysian</option>
                              <option value="maldivan">Maldivan</option>
                              <option value="malian">Malian</option>
                              <option value="maltese">Maltese</option>
                              <option value="marshallese">Marshallese</option>
                              <option value="mauritanian">Mauritanian</option>
                              <option value="mauritian">Mauritian</option>
                              <option value="mexican">Mexican</option>
                              <option value="micronesian">Micronesian</option>
                              <option value="moldovan">Moldovan</option>
                              <option value="monacan">Monacan</option>
                              <option value="mongolian">Mongolian</option>
                              <option value="moroccan">Moroccan</option>
                              <option value="mosotho">Mosotho</option>
                              <option value="motswana">Motswana</option>
                              <option value="mozambican">Mozambican</option>
                              <option value="namibian">Namibian</option>
                              <option value="nauruan">Nauruan</option>
                              <option value="nepalese">Nepalese</option>
                              <option value="new zealander">New Zealander</option>
                              <option value="ni-vanuatu">Ni-Vanuatu</option>
                              <option value="nicaraguan">Nicaraguan</option>
                              <option value="nigerien">Nigerien</option>
                              <option value="north korean">North Korean</option>
                              <option value="northern irish">Northern Irish</option>
                              <option value="norwegian">Norwegian</option>
                              <option value="omani">Omani</option>
                              <option value="pakistani">Pakistani</option>
                              <option value="palauan">Palauan</option>
                              <option value="panamanian">Panamanian</option>
                              <option value="papua new guinean">Papua New Guinean</option>
                              <option value="paraguayan">Paraguayan</option>
                              <option value="peruvian">Peruvian</option>
                              <option value="polish">Polish</option>
                              <option value="portuguese">Portuguese</option>
                              <option value="qatari">Qatari</option>
                              <option value="romanian">Romanian</option>
                              <option value="russian">Russian</option>
                              <option value="rwandan">Rwandan</option>
                              <option value="saint lucian">Saint Lucian</option>
                              <option value="salvadoran">Salvadoran</option>
                              <option value="samoan">Samoan</option>
                              <option value="san marinese">San Marinese</option>
                              <option value="sao tomean">Sao Tomean</option>
                              <option value="saudi">Saudi</option>
                              <option value="scottish">Scottish</option>
                              <option value="senegalese">Senegalese</option>
                              <option value="serbian">Serbian</option>
                              <option value="seychellois">Seychellois</option>
                              <option value="sierra leonean">Sierra Leonean</option>
                              <option value="singaporean">Singaporean</option>
                              <option value="slovakian">Slovakian</option>
                              <option value="slovenian">Slovenian</option>
                              <option value="solomon islander">Solomon Islander</option>
                              <option value="somali">Somali</option>
                              <option value="south african">South African</option>
                              <option value="south korean">South Korean</option>
                              <option value="spanish">Spanish</option>
                              <option value="sri lankan">Sri Lankan</option>
                              <option value="sudanese">Sudanese</option>
                              <option value="surinamer">Surinamer</option>
                              <option value="swazi">Swazi</option>
                              <option value="swedish">Swedish</option>
                              <option value="swiss">Swiss</option>
                              <option value="syrian">Syrian</option>
                              <option value="taiwanese">Taiwanese</option>
                              <option value="tajik">Tajik</option>
                              <option value="tanzanian">Tanzanian</option>
                              <option value="thai">Thai</option>
                              <option value="togolese">Togolese</option>
                              <option value="tongan">Tongan</option>
                              <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                              <option value="tunisian">Tunisian</option>
                              <option value="turkish">Turkish</option>
                              <option value="tuvaluan">Tuvaluan</option>
                              <option value="ugandan">Ugandan</option>
                              <option value="ukrainian">Ukrainian</option>
                              <option value="uruguayan">Uruguayan</option>
                              <option value="uzbekistani">Uzbekistani</option>
                              <option value="venezuelan">Venezuelan</option>
                              <option value="vietnamese">Vietnamese</option>
                              <option value="welsh">Welsh</option>
                              <option value="yemenite">Yemenite</option>
                              <option value="zambian">Zambian</option>
                              <option value="zimbabwean">Zimbabwean</option>
                            </select>
                            <span class="help-block help-center" id="nationality_error"></span>
                          </div>
                        </div>
                      </div>
                    </div><!-- row ends -->


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                              <p class = "form-helper" id="non-res-text">Address</p>

                              <div class = "form-group">
                                <textarea class="signup-f-input" id="address" required>Villivakkam</textarea>
                                <span class="help-block help-center" id="address_error"></span>
                              </div>
                            </div>
                        </div>
                    </div><!-- row ends -->


                 </div>
           </div> <!-- form-div ends -->
        </div> <!--Profile bar ends -->
          
           <div class="hr"></div>

            <div class = "row form-row">

                <div class = "col-lg-4 col-md-4 col-sm-4 per-det-div">
                      <p class = "per-det">Bank Details</p>
                      <p id = "per-det-info">Add the bank account from which you would be making the investment. Your withdrawals would be deposited to the same account.</p>
                </div> <!--Per-det-div ends -->

                <div class = "col-lg-7 col-md-7 col-sm-7 padding-lr-zero form-div">

                      <div class = "row">
                        <div class = "col-lg-6 col-md-6 col-sm-6">
                          <p class = "form-helper">Account Holder Name</p>
                          <div class = "form-group">
                            <input class = "signup-f-input form-inp" type = "text" name = "f-s-acc-name" value="Naveen Kumar" id = "acc_name" required>
                            <span class="help-block help-center" id="accname_error"></span>
                          </div>
                        </div>

                        <div class = "col-lg-6 col-md-6 col-sm-6">
                          
                        </div>
                      </div><!-- row ends -->

                       <div class = "row">


                        <div class="col-lg-12 col-md-12 col-sm-12 padding-l-zero">
                            <div class = "col-lg-6 col-md-6 col-sm-6">
                                <p class = "form-helper">Cancelled Cheque or Bank Passbook front Page</p>                           
                            </div>


                            <div class = "col-lg-6 col-md-6 col-sm-6">
                                <p class = "form-helper">Mandate Upload</p>                            
                            </div>

                        </div>   

                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 box-shadow upload-container">
                               <div class="col-lg-10 col-md-10 col-sm-10 padding-lr-zero">
                                   <div class="info-group">
                                        <p class="form-helper pm0 upload-cont-text">Upload</p>
                                        <p class="form-helper upload-cont-text">type of image Jpeg/PNG</p>
                                   </div>
                               </div>

                               <div class="col-lg-2 col-md-2 col-sm-2">
                                   <label for="cancelcheck_file">
                                       <img src="icons/upload_icon.png" class="upload_icon">
                                   </label>
                                   <input type="file" name="cancelcheck_file" id="cancelcheck_file" style="display: none;">
                               </div>
                           </div>
                           <span class="help-block help-center activation-file-error" id="cancelcheck_error"></span>
                        </div>

                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 box-shadow upload-container">
                               <div class="col-lg-10 col-md-10 col-sm-10 padding-lr-zero">
                                   <div class="info-group">
                                        <p class="form-helper pm0 upload-cont-text">Upload</p>
                                        <p class="form-helper upload-cont-text">type of image Jpeg/PNG</p>
                                   </div>
                               </div>

                               <div class="col-lg-2 col-md-2 col-sm-2">
                                   <label for="mandate_file">
                                       <img src="icons/upload_icon.png" class="upload_icon">
                                   </label>
                                   <input type="file" name="mandate_file" id="mandate_file" style="display: none;">
                               </div>
                           </div>
                           <span class="help-block help-center activation-file-error" id="mandate_error"></span>
                        </div>
                       </div><!-- row ends -->

                </div>

            </div><!-- Second row ends -->
            <div class="hr"></div>

            @if(\Auth::user()->p_check == 1) 
              <div class = "row form-row">

                <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12 per-det-div">
                  <p class = "per-det">EKYC</p>
                  <p id = "per-det-info">According to SEBI regulation you need to go through KYC Verfication Process</p>
                </div>

                <div class = "col-lg-7 col-md-7 col-sm-7 col-xs-12 padding-lr-zero form-div">
                  <div class = "row">
                           <div class="col-lg-6 col-md-6 col-sm-6">
                               <p class="ekyc-text">Your KYC is not completed.</p>
                               <p class="ekyc-text">Complete your E-KYC</p>
                                <a href="/ekyc" class="btn btn-primary" id="ekyc-btn">EKYC</a>
                           </div>

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <p class="kyc-info-text">KYC Form Download</p>
                               <div class="col-lg-12 col-md-12 col-sm-12 box-shadow upload-container">
                                  <div class="col-lg-10 col-md-10 col-sm-10 padding-lr-zero">
                                    <div class="info-group">
                                      <p class="form-helper pm0 upload-cont-text">Upload</p>
                                      <p class="form-helper upload-cont-text">type of image Jpeg/PNG</p>
                                    </div>
                                  </div>

                                  <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label for="mandate_file">
                                      <img src="icons/upload_icon.png" class="upload_icon">
                                    </label>
                                    <input type="file" name="kyc_file" id="kyc_file" style="display: none;">
                                  </div>
                               </div>
                               <span class="help-block help-center" id="kyc_error"></span>
                           </div>
                  </div>
                </div>
              </div>
              <div class="hr"></div>
            @endif

            

            <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12">
                <input type="submit" name="create-account" class = "btn btn-primary center-block create-acc-btn" value="CREATE ACCOUNT">
             </div>
            </div>

            </form> 
            




    </div> <!-- content ends -->

    <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about_us')}}">About</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact_us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
  
</div>


      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="js/activation.js"></script>

      <script>
        /*$('.signup-f-input').keypress(function(key) {
            if((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45)) return false;
         });

        /*$('#f-s-pin').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');

        if($(this.val()))
        });*/
      </script>
      
  

</body>
</html>