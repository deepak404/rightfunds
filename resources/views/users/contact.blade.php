<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{url('css/sign-up.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/sign-up-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/loader.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>

  <div class="loader" id="loader"></div>

    <nav class="navbar">
  
    <div class="navbar-header">
      <button type="button" class="navbar-toggle btn btn-primary back-btn">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>

      </button>
      <a class="navbar-brand" href="/"><img src="{{url('icons/login_logo.png')}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     
      <ul class="nav navbar-nav navbar-right navbar-btn ">
        <button type="button" class="btn btn-primary nav-back-btn" onclick="location.href='http://rightfunds.com';"><i class="fa fa-chevron-left" aria-hidden="true"></i>
&nbsp; BACK</button>
      </ul>
    </div>
  
</nav>

<div class="container">

    <div class="content">
        
        <div class="row  profile-bar">

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <p class="is-head">Contact Us</p>
                <p class="is-details">To start investing and achieve your life goals</p>
              
          </div> 

          <div class = "row adv-row">

              <div class = "col-lg-12 col-md-12 col-sm-12 contact-us-div">

              <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                  <img src="{{url('icons/loc.png')}}" class = "center-block">
                  <p class = "adv-head">Location</p>
                  <p class = "adv-exp contact-exp">B073, Commanders Court </p>
                  <p class = "adv-exp contact-exp"> Ethiraj Salai, Egmore,</p>
                  <p class = "adv-exp contact-exp"> Chennai - 600008</p>
              </div>

              <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                <img src="{{url('icons/phone.png')}}" class = "center-block">
                <p class = "adv-head">Phone</p>
                <p class = "adv-exp contact-exp">We are only a Phone call away</p>
                <p class="cont-phone">8807600421</p>

              </div>

              <div class = "col-lg-4 col-md-4 col-sm-4 signup-adv-text">
                <img src="{{url('icons/live_chat.png')}}" class = "center-block">
                <p class = "adv-head">Email</p>
                <p class = "adv-exp contact-exp">Drop a mail</p>
                 <a id="live-chat"><p class="cont-phone">contact@rightfunds.com</p></a>

              </div>

          </div> <!-- signup-adv-div ends -->

          </div>

         

            






            




    </div> <!-- content ends -->

    <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{url('icons/twitter.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="{{url('icons/facebook.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{url('icons/linked_in.png')}}"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
  
</div>

  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js?v=1.1'')}}"></script>
  <script src="{{url('js/jquery.donut.js?v=1.1'')}}"></script>
  <script src="{{url('js/checker.js?v=1.1'')}}"></script>

</body>
</html>