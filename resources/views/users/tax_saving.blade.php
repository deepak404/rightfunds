<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="css/bootstrap.min.css?v=1.1">
    <link rel="stylesheet" href="css/portfolio-details.css?v=1.1">
    <link rel="stylesheet" href="css/footer.css?v=1.1">
    <link rel="stylesheet" href="css/portfolio-details-responsive.css?v=1.1">
    <link rel="stylesheet" href="css/version2/pd-responsive.css?v=1.1">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link rel="stylesheet" href="css/datepicker.css?v=1.1">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/tax_saving.css?v=1.1" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>


<nav class="navbar">
  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/"><img src="icons/logo.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active menu-text"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text menu-text-active"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    </div>
  </div>  
    
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Account Summary</p>
                      <p class="invest-text">Get your latest account statements</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="icons/zero_fee.png">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="icons/auto_inv.png">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="icons/tax_free.png">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>


          


          <div class = "row panel investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/portfolio_details')}}"><li class="list-headings">Portfolio Details</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/investment_history')}}"><li class="list-headings">Investment History</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/tax_saving')}}"><li class="list-headings active-page">Tax Saving Statements.</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/withdraw_funds')}}"><li class="list-headings no-b-right">Withdraw Funds</li></a>
                    </div>                 
                  
               </ul>
             
             </div><!-- Heading ends -->



              <div class="row" id="export-row">

                <div class="col-lg-12 col-md-12 col-sm-12 statement-div">

                  <p class="fin-text text-center">No tax saving statements yet</p>
                  <!--<div class="col-lg-9 col-md-9 col-sm-9">
                      <p class="fin-text">Financial year <span class = "fin-year">
                            2015-2016
                          </span></p>
                   </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 btn-div">
                    
                    <button type="button" id="savepdf-btn" class="btn btn-primary fin-btn">DOWNLOAD</button>
                  </div>-->

                </div><!-- statement-div ends --> 

                <!--<div class="col-lg-12 col-md-12 col-sm-12 statement-div">

                  <div class="col-lg-9 col-md-9 col-sm-9">
                      <p class="fin-text">Financial year <span class = "fin-year">
                            2017-2018
                          </span></p>
                   </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 btn-div">
                    
                    <button type="button" id="savepdf-btn" class="btn btn-primary fin-btn">DOWNLOAD</button>
                  </div>

                </div><!-- statement-div ends -->                   
              </div><!-- export-row ends -->

              

              


               



                



              






          </div> <!-- investment-summary ends -->
    


          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>










  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fund_selector_donut.js"></script>
  <script src="js/fund_selector.js"></script>
  <script src="js/withdraw_funds.js"></script>
  <script src="js/preference.js"></script>
  <script src="js/scroller.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>



</script>

</body>
</html>
