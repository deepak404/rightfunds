<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fund_selector1.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" href="css/fund_selector_responsive.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link rel="stylesheet" href="css/datepicker.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body>


    <nav class="navbar">
  
    <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="/"><img src="icons/logo.png"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="active menu-text menu-text-active"><a href="#">Home</a></li>
            <li class="menu-text"><a href="#">Preference</a></li>
            <li class="menu-text"><a href="#">Account Statement</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right navbar-btn">
            <button type="button" class="btn btn-primary nav-logout-btn">LOG OUT</button>
          </ul>
        </div>
    </div>
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Fund Selector</p>
                      <p class="invest-text">Build Robust investment Portfolios</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="icons/zero_fee.png">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="icons/auto_inv.png">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="icons/tax_free.png">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>

         <form name="fund_sel_form" id="fund_sel_form">
                      {{csrf_field()}}


          <div class = "row investment-summary">

         


              
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p class="is-head">Investment Type</p>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 inv-amount-container">
                  <p class="inv-cont-text">How much do you wish to invest?</p>

                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Rs.</span>
                    <input type="text" name="inv-amount" id="inv-amount">
                  </div>

                  
                  <p class="inv-cont-text">Min:5000</p>

                  <select id="port_type">
                      <!-- This method is nice because it doesn't require extra div tags, but it also doesn't retain the style across all browsers. -->
                      <option value="Conservative">Conservative</option>
                      <option value="Moderate">Moderate</option>
                      <option value="Aggressive">Aggressive</option>
                      <option value="Tax Saver">Tax Saver</option>
                      <option value="own">Custom</option>
                  </select>

                </div> <!-- Inv amount Container ends -->

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">

                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 padding-lr-zero">
                           <div class = " onetime-radio-div padding-lr-zero ">
                              <input type="radio"  name="inv_type" id="onetime" value = "onetime" >
                              <label for="onetime"><span class="radio"><img src="icons/r_dis.png" id="one_img"></span></label>
                           </div>
                        </div>
                        <div class="col-lg-10 col-sm-10 col-md-10 col-xs-10 padding-lr-zero">                          
                            <div class="invtype-cont">
                              <p class="no-b-margin portfolio-name">Invest now - One Time</p>
                            <!--<p class="no-b-margin risk-type">12 Jul 16</p>-->
                              <input type="text" name="future" id="one-sel-date" placeholder="Select Date">
                            </div>                         
                        </div>

                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 padding-lr-zero">

                           <div class = " future-radio-div padding-lr-zero">
                              <input type="radio"  name="inv_type" id="future" value = "future" >
                              <label for="future"><span class="radio" id="span_future_img"><img src="{{url('icons/r_dis.png')}}" id="future_img"></span></label>
                            </div>
                        </div>
                        <div class="col-lg-10 col-sm-10 col-md-10 col-xs-10 padding-lr-zero">
                          
                            <div class="invtype-cont">
                              <p class="no-b-margin portfolio-name">Schedule a Future Investment</p>
                            <input type="text" name="future" id="sche-sel-date" placeholder="Select Date">
                            </div>
                          
                        </div>

                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 padding-lr-zero">
                           <div class = " future-radio-div padding-lr-zero">
                              <input type="radio"  name="inv_type" id="monthly" value = "monthly" >
                              <label for="monthly"><span class="radio" id="span_future_img"><img src="{{url('icons/r_dis.png')}}" id="monthly_img"></span></label>
                            </div>
                        </div>
                        <div class="col-lg-10 col-sm-10 col-md-10 col-xs-10 padding-lr-zero">
                          
                            <div class="invtype-cont">
                              <p class="no-b-margin portfolio-name">Monthly Investment Plan</p>
                              <input type="text" name="future" id="sip-date" placeholder="Select Date"><span id="to">for</span>
                              <select id="sip-duration">
                                <option value="1">1 Year</option>
                                <option value="3">3 Years</option>
                                <option value="5">5 Years</option>
                                <option value="10">10 Years</option>
                                <option value="20">20 Years</option>
                              </select>
                            </div>
                          
                        </div>

                  </div>                  
                </div> <!-- col-lg-6 ends -->

              </div>

              

          </div>


          <div class = "row investment-summary">

              

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">Recommended Portfolio</p>
                <p class="text-center" id="portfolio-heading">Conservative</p>
                <p id="recom-spl">Conservative investing is an investing strategy that seeks to preserve an investment portfolio's value by investing in lower risk securities such as fixed-income and money market securities, and often blue-chip or large-cap equities.</p>
               
              </div>

              



            <div class="table-group row">
              <div class=" col-lg-12 col-md-12 col-xs-12 equity-container" >


                <div class="fund-title-container">
                  <p class="fund-title">EQUITY</p>
                </div>

                <div id="eq-details-container" data-cname = "equity" >
                  <div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >
                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">
                     <i class="material-icons arrow-down" data-toggle = "collapse" href="#one" >keyboard_arrow_down</i>
                   </div>
                   <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">
                     <p class = "span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</p>
                     <p class = "currently-inv"> Currently invested <span class = "curr-span-amt">Rs. 50,000</span></p>
                   </div>
                   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">
                     <p class = "fund-amount-inner">Rs. 50,00,000</p>
                   </div>
                </div>



                <div id="one" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">

                       <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Fund Manager</p>
                               <p class="fund-details-content">Mr. Navaneetha Krishnan</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Benchmark</p>
                               <p class="fund-details-content">S&P BSE</p>
                             </div>
                           </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Launch</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Investment</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                            </div>


                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Exit Load</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Asset Size</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                           </div>
                       </div> <!-- Fund Detail container closes-->

                       <div class="col-lg-12 col-md-12 col-sm-12">
                         <p id="nav_container"><span>NAV - </span><span id="scheme_nav">3000.0000</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">
                           <p class="graph-duration text-center active-duration border-right">3 Months</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">
                           <p class="graph-duration text-center border-right">6 Months</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">
                           <p class="graph-duration text-center border-right">1 Year</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">
                           <p class="graph-duration text-center border-right">3 Years</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero w-20">
                           <p class="graph-duration text-center border-right">5 Years</p>
                         </div>
                       </div>

                      

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                         <div id="graph-container">
                           
                         </div>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 border-top">
                         <p id="fund-ret-header">Fund Returns</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div id="border-blah">
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Day</p>
                             <p class="return-perc neg-ret">5.6%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Day</p>
                             <p class="return-perc pos-ret">0.2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Month</p>
                             <p class="return-perc pos-ret">0.9%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">3 Months</p>
                             <p class="return-perc pos-ret">2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 year</p>
                             <p class="return-perc pos-ret">16%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Years</p>
                             <p class="return-perc pos-ret">22%</p>
                           </div>
                         </div>
                       </div>



                </div> <!--ID one Ends -->
                </div>
              </div>




              <div class=" col-lg-12 col-md-12 col-xs-12 debt-container" >


                <div class="fund-title-container">
                  <p class="fund-title">DEBT</p>
                </div>

                <div id="debt-details-container" data-cname = "debt">
                  <div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >
                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">
                     <i class="material-icons arrow-down" data-toggle = "collapse" href="#two" >keyboard_arrow_down</i>
                   </div>
                   <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">
                     <p class = "span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</p>
                     <p class = "currently-inv"> Currently invested <span class = "curr-span-amt">Rs. 50,000</span></p>
                   </div>
                   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">
                     <p class = "fund-amount-inner">Rs. 50,00,000</p>
                   </div>
                </div>



                <div id="two" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">

                       <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Fund Manager</p>
                               <p class="fund-details-content">Mr. Navaneetha Krishnan</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Benchmark</p>
                               <p class="fund-details-content">S&P BSE</p>
                             </div>
                           </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Launch</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Investment</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                            </div>


                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Exit Load</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Asset Size</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                           </div>
                       </div> <!-- Fund Detail container closes-->

                       <div class="col-lg-12 col-md-12 col-sm-12">
                         <p id="nav_container"><span>NAV - </span><span id="scheme_nav">3000.0000</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center active-duration border-right">1 Day</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">5 Day</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">1 Month</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">3 Months</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">1 year</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center">5 Years</p>
                         </div>
                       </div>

                      

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                         <div id="debt-graph-container">
                           
                         </div>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 border-top">
                         <p id="fund-ret-header">Fund Returns</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div id="border-blah">
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Day</p>
                             <p class="return-perc neg-ret">5.6%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Day</p>
                             <p class="return-perc pos-ret">0.2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Month</p>
                             <p class="return-perc pos-ret">0.9%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">3 Months</p>
                             <p class="return-perc pos-ret">2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 year</p>
                             <p class="return-perc pos-ret">16%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Years</p>
                             <p class="return-perc pos-ret">22%</p>
                           </div>
                         </div>
                       </div>



                </div> <!--ID one Ends -->
                </div>
              </div>



              <div class=" col-lg-12 col-md-12 col-xs-12 bal-container" >


                <div class="fund-title-container">
                  <p class="fund-title">BALANCED</p>
                </div>


                <div id="bal-details-container" data-cname = "bal">
                  <div id="scheme-code-container" class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                  <div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >
                     <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">
                       <i class="material-icons arrow-down" data-toggle = "collapse" href="#three" >keyboard_arrow_down</i>
                     </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">
                       <p class = "span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</p>
                       <p class = "currently-inv"> Currently invested <span class = "curr-span-amt">Rs. 50,000</span></p>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">
                       <p class = "fund-amount-inner">Rs. 50,00,000</p>
                     </div>
                  </div>



                    <div id="three" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">

                       <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Fund Manager</p>
                               <p class="fund-details-content">Mr. Navaneetha Krishnan</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Benchmark</p>
                               <p class="fund-details-content">S&P BSE</p>
                             </div>
                           </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Launch</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Investment</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                            </div>


                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Exit Load</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Asset Size</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                           </div>
                       </div> <!-- Fund Detail container closes-->

                       <div class="col-lg-12 col-md-12 col-sm-12">
                         <p id="nav_container"><span>NAV - </span><span id="scheme_nav">3000.0000</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center active-duration border-right">1 Day</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">5 Day</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">1 Month</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">3 Months</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">1 year</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center">5 Years</p>
                         </div>
                       </div>

                      

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                         <div id="bal-graph-container">
                           
                         </div>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 border-top">
                         <p id="fund-ret-header">Fund Returns</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div id="border-blah">
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Day</p>
                             <p class="return-perc neg-ret">5.6%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Day</p>
                             <p class="return-perc pos-ret">0.2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Month</p>
                             <p class="return-perc pos-ret">0.9%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">3 Months</p>
                             <p class="return-perc pos-ret">2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 year</p>
                             <p class="return-perc pos-ret">16%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Years</p>
                             <p class="return-perc pos-ret">22%</p>
                           </div>
                         </div>
                       </div>



                   </div> <!--ID one Ends -->
                </div>
                </div>

                
              </div>



            <div class=" col-lg-12 col-md-12 col-xs-12 taxsaver-container" >


                <div class="fund-title-container">
                  <p class="fund-title">TAX SAVER</p>
                </div>

                <div id="ts-details-container" data-cname = "taxsaver">
                  <div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >
                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">
                     <i class="material-icons arrow-down" data-toggle = "collapse" href="#four" >keyboard_arrow_down</i>
                   </div>
                   <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">
                     <p class = "span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</p>
                     <p class = "currently-inv"> Currently invested <span class = "curr-span-amt">Rs. 50,000</span></p>
                   </div>
                   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">
                     <p class = "fund-amount-inner">Rs. 50,00,000</p>
                   </div>
                </div>



                <div id="four" class="collapse col-lg-12 col-sm-12 col-md-12 padding-lr-zero fund-details-container">

                       <div class="col-lg-12 col-sm-12 col-md-12 padding-lr-zero" id="fund_details_container">

                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Fund Manager</p>
                               <p class="fund-details-content">Mr. Navaneetha Krishnan</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Benchmark</p>
                               <p class="fund-details-content">S&P BSE</p>
                             </div>
                           </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Launch</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Investment</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                            </div>


                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-lr-zero fund-det-container">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Exit Load</p>
                               <p class="fund-details-content">12-Apr-2017</p>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-lr-zero text-center">
                               <p class="fund-details-heading">Asset Size</p>
                               <p class="fund-details-content">Growth</p>
                             </div>
                           </div>
                       </div> <!-- Fund Detail container closes-->

                       <div class="col-lg-12 col-md-12 col-sm-12">
                         <p id="nav_container"><span>NAV - </span><span id="scheme_nav">3000.0000</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center active-duration border-right">1 Day</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">5 Day</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">1 Month</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">3 Months</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center border-right">1 year</p>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                           <p class="graph-duration text-center">5 Years</p>
                         </div>
                       </div>

                      

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
                         <div id="ts-graph-container">
                           
                         </div>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 border-top">
                         <p id="fund-ret-header">Fund Returns</span></p>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero border-bot">
                         <div id="border-blah">
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Day</p>
                             <p class="return-perc neg-ret">5.6%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Day</p>
                             <p class="return-perc pos-ret">0.2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 Month</p>
                             <p class="return-perc pos-ret">0.9%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">3 Months</p>
                             <p class="return-perc pos-ret">2%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">1 year</p>
                             <p class="return-perc pos-ret">16%</p>
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 padding-lr-zero">
                             <p class="graph-duration text-center">5 Years</p>
                             <p class="return-perc pos-ret">22%</p>
                           </div>
                         </div>
                       </div>



                </div> <!--ID one Ends -->
                </div>


                <div class=" col-lg-12 col-md-12 col-xs-12 padding-lr-zero" >


                    <div class="fund-title-container">
                      <p class="fund-title">TOTAL AMOUNT</p>
                    </div>

                    <div>
                      <div class="col-lg-12 col-sm-12 col-md-12 scheme-container padding-lr-zero" >
                       <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 padding-r-zero">
                         
                       </div>
                       <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 padding-lr-zero">
                         <p class = "span-fund-name">TOTAL AMOUNT TO BE INVESTED</p>
                       </div>
                       <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-lr-zero fund-amount-container">
                         <p class = "fund-amount-inner">Rs. <span id="total-amt"></span></p>
                       </div>
                    </div>
                </div>


                </div>
              </div>

              </div> <!-- col-xs-12 ends -->

                    <div class = "col-lg-12 col-md-12 col-sm-12">
                         <input type="submit" class="btn btn-primary center-block" id="invest-now-btn" value="INVEST">
                    </div>

            </div> <!-- table-group ends -->



          </form>

          </div>

          

          



          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Disclosures</a></li>
                <li><a href="#">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>

    <!-- Modal -->
<div id="invDetailsModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="invDetailsModalBody">

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Date of Investment</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">18 Jul 2017</span></div>
        </div>

         <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Status</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Inprocess</span></div>
        </div>

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Portfolio Type</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Custom</span></div>
        </div>

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Additional Investment</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Rs. 20000</span></div>
        </div>

        <div class="row">
          <button type = "button" class = "btn btn-primary save-now-btn" >SAVE</button>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <a class="cancel-inv-link"><span class = cancel-inv><img src="icons/no_tick.png"><span id="cancel-inv-txt">Cancel Investment</span></span></a>
            <p id="modal-disc">Disclosure : We encourage our investors to acquaint themselves with all the information provided by the AMC, before taking investment decision</p>
          </div>
        </div>


            
          

      </div>
    </div>
  </div>
</div>



    <!-- Modal -->
<div id="cancelInvModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body cancel-inv-body">

        <p id="as-can-inv">Are you sure about cancelling your investment ?</p>
        <p id="perc-goal-rem">You have 32% of your goal Remaining</p>

        <div id="progress-div">

          <span id="prog-zero">0</span><progress class="amount-progress" value="60000" max="120000">70 %</progress><span id="prog-no">5,00,000</span>
        </div>
        <button type="button" class="btn btn-primary yes-p-btn">Yes, Please Cancel Investment </button><br>
        <button type="button" class="btn btn-primary no-p-btn">No, I will stay Invested </button>

            
          

      </div>
    </div>
  </div>
</div>



<div id="invSummaryModal" class="modal fade" role="dialog">
  <div class="modal-dialog inv-summary-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body inv-summary-body">

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Date of Investment</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">18 Jul 2017</span></div>
        </div>

         <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Status</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Inprocess</span></div>
        </div>

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Portfolio Type</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Custom</span></div>
        </div>

          <div class="row">
                    <div class="hr">
          
        </div>
          </div>

        <p id="inv-summary">Investment Summary</p>
        <div class="row">
<table class="table">
              <thead class="table-head modal-table-head">
                <tr>
                  <th id="modal-equity-text">EQUITY</th>
                  <th id="eq-show-less"> </th>
                  
                </tr>
              </thead>
              <tbody id = "equity-body">
                <tr>

                  <td class = "modal-fund-name">

                    <div class = "col-xs-1 padding-lr-zero checkbox-div"></div>

                    <div class = "col-xs-11 fund-name-div"><span class = "modal-span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</span></div>
                    
                    </td>
                  
                  <td class = "fund-amount"><p class = "span-fund-amount-inner">Rs. 50,00,000</p></td>
                </tr>

                <tr>

                  <td class = "modal-fund-name">

                    <div class = "col-xs-1 padding-lr-zero checkbox-div"></div>

                    <div class = "col-xs-11 fund-name-div"><span class = "modal-span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</span></div>
                    
                    </td>
                  
                  <td class = "fund-amount"><p class = "span-fund-amount-inner">Rs. 50,00,000</p></td>
                </tr>
                </tbody>



                <thead class="table-head modal-table-head">
                <tr>
                  <th id="modal-elss-text">ELSS</th>
                  <th id="eq-show-less"> </th>
                  
                </tr>
              </thead>
              <tbody id = "equity-body">
                <tr>

                  <td class = "modal-fund-name">

                    <div class = "col-xs-1 padding-lr-zero checkbox-div"></div>

                    <div class = "col-xs-11 fund-name-div"><span class = "modal-span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</span></div>
                    
                    </td>
                  
                  <td class = "fund-amount"><p class = "span-fund-amount-inner">Rs. 50,00,000</p></td>
                </tr>

                <tr>

                  <td class = "modal-fund-name">

                    <div class = "col-xs-1 padding-lr-zero checkbox-div"></div>

                    <div class = "col-xs-11 fund-name-div"><span class = "modal-span-fund-name">HDFC DYNAMIC FUND HDFC DYNAMIC FUND HDFC DYNAMIC</span></div>
                    
                    </td>
                  
                  <td class = "fund-amount"><p class = "span-fund-amount-inner">Rs. 50,00,000</p></td>
                </tr>
                </tbody>



                <thead class="table-head modal-table-head">
                <tr>
                  <th id="modal-equity-text">TOTAL</th>
                  <th id="span-fund-amount-total">Rs 50,00,000</th>
                  
                </tr>
              </thead>
               
              </table>
        </div>


        <div class="row">
          <button type = "button" class = "btn btn-primary save-now-btn" >SAVE</button>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <a class="cancel-inv-link"><span class = cancel-inv><img src="icons/no_tick.png"><span id="cancel-inv-txt">Cancel Investment</span></span></a>
            <p id="modal-disc">Disclosure : We encourage our investors to acquaint themselves with all the information provided by the AMC, before taking investment decision</p>
          </div>
        </div>


            
          

      </div>
    </div>
  </div>
</div>



    <!-- Modal -->
<div id="scheduleInvModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Great !</p>
            <p id="you-have">you have successfully scheduled an investment of <span class="blue">45000</span> in <span class="blue">conservative</span> portfolio on <span class="blue">17 Jul 2017</span></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">
          <p class="text-center" id="notify-via">Notify Via</p>
          <input type="hidden" name="investment_id" id="investment_id">
          <div class="col-xs-6 text-center">

            <div class = " own-radio-div padding-lr-zero radio-inline">
                  <input type="radio"  name="notify-mode" id="email" value = "email" >
                  <label for="email"><span class="notify-radio" id="own-span"><img src="icons/r_dis.png" id="email-img"></span><span class="comm-mode">Email</span></label>
            </div>
          </div>

          <div class="col-xs-6 text-center">

            <div class = " own-radio-div padding-lr-zero radio-inline">
                  <input type="radio"  name="notify-mode" id="phone" value = "phone" >
                  <label for="phone"><span class="notify-radio" id="own-span"><img src="icons/r_dis.png" id="phone-img"></span><span class="comm-mode">SMS</span></label>
            </div>
          </div>
          <p class="text-center" id="once-amt">Once amount has been invested</p>
          <button type="button" class="btn btn-primary text-center" id="notify-done">DONE</button>
        </div>
        </div>



            
          

      </div>
    </div>
  </div>
</div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fund_selector_donut.js"></script>
  <script src="js/fund_selector1.js"></script>
  <script src="js/jquery-ui.js"></script>

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


     <script>
     google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCurveTypes);

function drawCurveTypes() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Gold Performance');
     

      data.addRows([ // Pattern format (X,Scheme Performance)
        [0, 0],    [1, 10],   [2, 23],  [3, 17,], [4, 18],  [5, 9],
        [6, 11],   [7, 27],  [8, 33],  [9, 40], [10, 32], [11, 35],
        [12, 30], [13, 40], [14, 42], [15, 47], [16, 44], [17, 48],
        [18, 52], [19, 54], [20, 42], [21, 55], [22, 56], [23, 57],
        [24, 60], [25, 50], [26, 52], [27, 51], [28, 49], [29, 53],
        [30, 55], [31, 60], [32, 61], [33, 59], [34, 62], [35, 65],
        [36, 62], [37, 58], [38, 55], [39, 61], [40, 64], [41, 65],
        [42, 63], [43, 66], [44, 67], [45, 69], [46, 69], [47, 70],
        [48, 72], [49, 68], [50, 66], [51, 65], [52, 67], [53, 70],
        [54, 71], [55, 72], [56, 73], [57, 75], [58, 70], [59, 68],
        [60, 64], [61, 60], [62, 65], [63, 67], [64, 68], [65, 69],
        [66, 70], [67, 72], [68, 75], [69, 150]
      ]);




      var options = {
        hAxis: {
          title: '',
          gridlines: {
            color: 'transparent'
          },
          baselineColor: '#fff',
         gridlineColor: '#fff', 


        },
        vAxis: {
          title: '',
          baselineColor: '#d7d7d7',
          gridlineColor: '#d7d7d7',

              
        },
        series: {
          1: {curveType: 'function'}
        },

        /*height: chart_height
        ,width: chart_width*/

        legend : 'none',
        
        colors : ['#0091EA'],
        width : 850,
        height : 300,



      };

      var gold_chart = new google.visualization.LineChart(document.getElementById('graph-container'));
      var ts_chart = new google.visualization.LineChart(document.getElementById('ts-graph-container'));
      var debt_chart = new google.visualization.LineChart(document.getElementById('debt-graph-container'));
      var bal_chart = new google.visualization.LineChart(document.getElementById('bal-graph-container'));
      
      gold_chart.draw(data, options);
      ts_chart.draw(data, options);
      debt_chart.draw(data, options);
      bal_chart.draw(data, options);


    }
   </script>


   <script>
     google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCurveTypes);

function drawCurveTypes() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Silver Performance');
     

      data.addRows([ // Pattern format (X,Scheme Performance)
        [0, 0],    [1, 10],   [2, 23],  [3, 17,], [4, 18],  [5, 9],
        [6, 11],   [7, 27],  [8, 33],  [9, 40], [10, 32], [11, 35],
        [12, 30], [13, 40], [14, 42], [15, 47], [16, 44], [17, 48],
        [18, 52], [19, 54], [20, 42], [21, 55], [22, 56], [23, 57],
        [24, 60], [25, 50], [26, 52], [27, 51], [28, 49], [29, 53],
        [30, 55], [31, 60], [32, 61], [33, 59], [34, 62], [35, 65],
        [36, 62], [37, 58], [38, 55], [39, 61], [40, 64], [41, 65],
        [42, 63], [43, 66], [44, 67], [45, 69], [46, 69], [47, 70],
        [48, 72], [49, 68], [50, 66], [51, 65], [52, 67], [53, 70],
        [54, 71], [55, 72], [56, 73], [57, 75], [58, 70], [59, 68],
        [60, 64], [61, 60], [62, 65], [63, 67], [64, 68], [65, 69],
        [66, 70], [67, 72], [68, 75], [69, 150]
      ]);




      var options = {
        hAxis: {
          title: '',
          gridlines: {
            color: 'transparent'
          },
          baselineColor: '#fff',
         gridlineColor: '#fff', 


        },
        vAxis: {
          title: '',
          baselineColor: '#d7d7d7',
          gridlineColor: '#d7d7d7',

              
        },
        series: {
          1: {curveType: 'function'}
        },

        /*height: chart_height
        ,width: chart_width*/

        legend : 'none',
     

        colors : ['#0091EA'],


      };

      var silver_chart = new google.visualization.LineChart(document.getElementById('silver-chart'));
      silver_chart.draw(data, options);

      

    }



    $(document).ready(function(){

      $(document).on('click','.arrow-down',function(){
        if ($(this).hasClass('collapsed')) {
          $(this).text('keyboard_arrow_down');
        }
        else{
          $(this).text('keyboard_arrow_up');
        }
      })
    })
   </script>


</body>
</html>
