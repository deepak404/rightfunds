<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/fund_selector.css')}}">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" href="{{url('css/fund_selector_responsive.css')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('css/jqueryui.css')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.css')}}">
    <link rel="stylesheet" href="{{url('css/loader.css')}}">
    <link rel="stylesheet" href="{{url('css/error.css')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>

<div class="loader" id="loader"></div>

<nav class="navbar">
  
    <div class="container">
      <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/home"><img src="{{url('icons/logo.png')}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active menu-text menu-text-active"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';">LOG OUT</button>
      </ul>
    </div>
    </div>
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Fund Selector</p>
                      <p class="invest-text">Build Robust Investment Portfolios</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="{{url('icons/zero_fee.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="{{url('icons/auto_inv.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="{{url('icons/tax_free.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>

    <div id = "alert-div">
      <p class = "text-center gotham_book" id = "mini-div">Minimum Investment Amount should be 5000 for this Portfolio</p>
    </div>


          <div class = "row investment-summary">

          <form name="fund_sel_form" id="fund_sel_form">
            
          

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <img src="{{url('icons/1.png')}}" class="center-block one" >
               
              </div>

              

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">How much do you wish to invest?</p>
               
              </div>

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 ">

                <div class="input-group input-group-lg inv-amount-block center-amount-block ">
                  <span class="input-group-addon " id="inv-rs">Rs.</span>
                  <input type="text" class="form-control inv-amount" placeholder="" aria-describedby="basic-addon1" id = "inv-amount" maxlength="7">
                </div>
                <p id="amount-required" class="text-center">Amount is required before selecting the investment type</p>
                <p id="min-val-text">Minimum Investment amount : <span id="min-val">5,000</span></p>
               
              </div>

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12  padding-lr-zero">

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-zero inv-type-div">

                   <div class = " onetime-radio-div padding-lr-zero radio-inline">
                      <input type="radio"  name="inv_type" id="onetime" value = "onetime" >
                      <label for="onetime"><span class="radio"><img src="{{url('icons/r_dis.png')}}" id="one_img"></span></label>
                    </div>

                  <p class="invest-plan-text">Invest now- One Time</p>
                  <p class="invest-plan-date" id="onetime-date"><span class = "plan-date"><input type="text" name="future" id="one-sel-date" placeholder="Select Date" disabled></span>
                  @if($ot_count > 0)
                  <img src="{{('icons/i_icon.png')}}" class="i-icon" id="onetime-inv-icon" data-value = "0">
                  @else
                  @endif
                  </p>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-zero inv-type-div">

                    <div class = " future-radio-div padding-lr-zero radio-inline">
                      <input type="radio"  name="inv_type" id="future" value = "future" >
                      <label for="future"><span class="radio" id="span_future_img"><img src="{{url('icons/r_dis.png')}}" id="future_img"></span></label>
                    </div>

                  <p class="invest-plan-text">Schedule a Future investment </p>
                  <p class="invest-plan-date" id="schedule-date"><span class = "plan-date"><input type="text" name="future" id="sche-sel-date" placeholder="Select Date" disabled></span>
                  @if($f_count > 0)
                  <img src="{{url('icons/i_icon.png')}}" class="i-icon" id="fut-icon" data-value = "1">
                  @else
                  @endif
                  </p>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-lr-zero inv-type-div">

                   <div class = " monthly-radio-div padding-lr-zero radio-inline">
                      <input type="radio"  name="inv_type" id="monthly" value = "monthly" >
                      <label for="monthly"><span class="radio"><img src="{{url('icons/r_dis.png')}}" id="monthly_img"></span></label>
                    </div>
                  
                  <p class="invest-plan-text">Monthly investment Plan</p>
                  <p class="invest-plan-date" id="monthly-date"><span class = "plan-date">


                  <input type="text" name="sip-date" id="sip-date" placeholder="Select Date"><span id="to">for</span><!--<input type="text" name="future" id="month-end-date" placeholder="Select Date" disabled>-->
                  <select id="sip-duration">
                    <option value="1">1 Year</option>
                    <option value="3">3 Years</option>
                    <option value="5">5 Years</option>
                    <option value="10">10 Years</option>
                    <option value="20">20 Years</option>
                  </select></span>
                  @if($sip_count > 0)
                  <img src="{{url('icons/i_icon.png')}}" class="i-icon" id = "monthly-icon" data-value = "2"></p>
                  @else
                  @endif
                </div>
               
              </div>


              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">Portfolio type: <span id="portfol-name"></span></p>
                 <p id="amount-required-port" class="text-center">Amount is required before selecting the investment type</p>
               
              </div>

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 portfolio-type-div">



                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 conser-port-div padding-lr-zero">
                      <p class="port-text" id = "conser-text">Conservative</p>
                      <img src="{{url('icons/conser_off.png')}}" id = "conser-image">
                      <p class = "port-below-text" >LOW RISK</p>
                       <div class = " lowrisk-radio-div padding-lr-zero radio-inline">
                      <input type="radio"  name="port_type" id="low-risk" value = "low-risk" >
                      <label for="low-risk"><span class="port-radio"><img src="{{url('icons/r_dis.png')}}" id="low-img"></span></label>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mod-port-div padding-lr-zero">
                      <p class="port-text" id = "moder-text">Moderate</p>
                      <img src="{{url('icons/mod_off.png')}}" id ="moder-image">
                      <p class = "port-below-text" >MODERATE RISK</p>
                       <div class = " modrisk-radio-div padding-lr-zero radio-inline">
                      <input type="radio"  name="port_type" id="moderate-risk" value = "moderate-risk" >
                      <label for="moderate-risk"><span class="port-radio"><img src="{{url('icons/r_dis.png')}}" id="mod-img"></span></label>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 aggr-port-div padding-lr-zero">
                      <p class="port-text" id = "aggr-text">Aggressive</p>
                      <img src="{{url('icons/aggr_off.png')}}" id = "aggr-image">
                      <p class = "port-below-text" >HIGH RISK</p>

                      <div class = " highrisk-radio-div padding-lr-zero radio-inline">
                        <input type="radio"  name="port_type" id="high-risk" value = "high-risk" >
                        <label for="high-risk"><span class="port-radio"><img src="{{url('icons/r_dis.png')}}" id="high-img"></span></label>
                      </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 elss-port-div padding-lr-zero">
                      <p class="port-text" id = "elss-text">ELSS</p>
                      <img src="{{url('icons/ts_off.png')}}" id = "elss-image">
                      <p class = "port-below-text" >TAX SAVER</p>

                       <div class = " elss-radio-div padding-lr-zero radio-inline">
                          <input type="radio"  name="port_type" id="elss" value = "elss" >
                          <label for="elss"><span class="port-radio" id="own-span"><img src="{{url('icons/r_dis.png')}}" id="elss-img"></span></label>
                       </div>



                    </div>


               
              </div>

              <div class="col-lg-12 col-sm-12 col-md-12">
                <div id="custom-div">
                  <div class = " own-radio-div padding-lr-zero radio-inline">
                        <input type="radio"  name="port_type" id="own" value = "own" >
                        <label for="own"><span class="port-radio" id="own-span"><img src="{{url('icons/r_dis.png')}}" id="own-img"></span></label>
                  </div>
                  <span class="text-center gotham_book" id="custom-port-text">Build Your Custom Portfolio</span>
                </div>
              </div>


              


              

          </div>


          <div class = "row investment-summary">

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <img src="{{url('icons/2.png')}}" class="center-block one" >              
              </div>

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head" id="reco-port">Recommended Portfolio</p>
                <p id="recom-spl">This is the recommended split-up for your current portfolio type.</p>
               
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 port-comp-data padding-lr-zero">


                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 equity-div grad-border" id="equity-div">
                      
                      <canvas id="equity-chart" height="200" width="200" class="donut-chart">
                          <div data-value="100">EQUITY</div>
                      </canvas><div class="canvas-in-div"><p class="port-name">EQUITY</p>
                        <p class="port-perc" id="eq-perc">0%</p></div>

                      
                  </div>

                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 debt-div grad-border" id="debt-div">
                    <canvas id="debt-chart" height="200" width="200" class="donut-chart">
                          <div data-value="100">DEBT</div>
                      </canvas><div class="canvas-in-div"><p class="port-name">DEBT</p>
                        <p class="port-perc" id="debt-perc">0%</p></div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hybrid-div grad-border" id="hybrid-div">
                    <canvas id="gold-chart" height="200" width="200" class="donut-chart">
                          <div data-value="100">BALANCED</div>

                      </canvas><div class="canvas-in-div"><p class="port-name">BALANCED</p>
                        <p class="port-perc" id="bal-perc">0%</p></div>
                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 elss-div grad-border" id="elss-div">
                    <canvas id="elss-chart" height="200" width="200" class="donut-chart">
                          <div data-value="100">ELSS</div>

                      </canvas><div class="canvas-in-div"><p class="port-name">ELSS</p>
                        <p class="port-perc" id="elss-perc">0%</p></div>
                  </div>
             
          </div>



  <div class="table-group row">
    <div class="col-xs-12">


    <div class="panel-group" id="accordion">
      
            <div class="panel panel-default" id="equity-panel">
        <div class="panel-heading">
        <h3 class="panel-title fund-title">
          EQUITY
          <a data-toggle="collapse" id="equity-sm" href="#collapse1" data-parent ="#accordion" style="float:right;">Show More</a>
        </h3>
        </div>
        <div id="collapse1" class="panel-collapse collapse-in">
         <table class="table">
            <tbody id = "equity-body">

                <p class="text-center empty-sugg">Add Amount and select portfolio type for fund Suggestions</p>


              </tbody>
          </table>
        </div>
      </div>
    


    
      <div class="panel panel-default" id="debt-panel">
        <div class="panel-heading">
        <h4 class="panel-title fund-title">
          DEBT
          <a data-toggle="collapse" href="#collapse2" data-parent = "#accordion" id="debt-sm" style="float:right;">Show More</a>
        </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
        <table class="table">
            <tbody id = "debt-body">

              <p class="text-center empty-sugg">Add Amount and select portfolio type for fund Suggestions</p>

              </tbody>
          </table>
        </div>
      </div>
      


      
      <div class="panel panel-default" id="bal-panel">
        <div class="panel-heading">
        <h4 class="panel-title fund-title">
          BALANCED
          <a data-toggle="collapse" href="#collapse3" data-parent="#accordion" style="float:right;" id="hybrid-sm">Show More</a>
        </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse">
        <table class="table table-bordered">
            <tbody id = "bal-body">

                <p class="text-center empty-sugg">Add Amount and select portfolio type for fund Suggestions</p>

              </tbody>
          </table>
        </div>
      </div>


       <div class="panel panel-default" id="elss-panel">
        <div class="panel-heading">
        <h4 class="panel-title fund-title">
          EQUITY LINKED SAVINGS SCHEME
          <a data-toggle="collapse" href="#collapse4" data-parent="#accordion" style="float:right;" id="elss-sm">Show More</a>
        </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse">
        <table class="table table-bordered">
            <tbody id = "elss-body">

                <p class="text-center empty-sugg">Add Amount and select portfolio type for fund Suggestions</p>

              </tbody>
          </table>
        </div>
      </div>
      

      <div class="panel panel-default" id="total_amount_panel">
        <div class="panel-heading">
        <h4 class="panel-title fund-title">
          TOTAL AMOUNT TO BE INVESTED
          <span style="float:right;">Rs. <p style="float:right;" id="total-amt">0</p></span>
        </h4>
        </div>
        
      </div>
    </div>



      </div>
    </div>

          <div class = "col-lg-12 col-md-12 col-sm-12" id="portfolio_done_div">
                
                <p id="built-port">I have built my Portfolio</p><button class = "btn btn-primary inv-now-btn" id="invest-now-button" type="submit">INVEST NOW</button>

          </div>

  </div>



          </form>

          </div>

          

          



          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{url('icons/twitter.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="{{url('icons/facebook.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{url('icons/linked_in.png')}}"></a>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>

    <!-- Modal -->
<div id="invDetailsModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="invDetailsModalBody">


      <div class="row">
        <div class="col-xs-12 padding-lr-zero">
          <table class="table table-striped">
            <thead>
              <tr>
                <th><p class="text-center gotham_book header_table">Date of Investment</p></th>
                <th><p class="text-center gotham_book header_table">Status</p></th>
                <th><p class="text-center gotham_book header_table">Portfolio Type</p></th>
                <th><p class="text-center gotham_book header_table">Investment Amount</p></th>
              </tr>
            </thead>
            <tbody id="pending-inv-body">


            </tbody>
          </table>
        </div>
      </div>


        <div class="row">
          <div class="col-lg-12">
            <!--<a class="cancel-inv-link"><span class = cancel-inv><img src="icons/no_tick.png"><span id="cancel-inv-txt">Cancel Investment</span></span></a>-->
            <p id="modal-disc">Disclosure : We encourage our investors to acquaint themselves with all the information provided by the AMC, before taking investment decision</p>
          </div>
        </div>


       </div>
    </div>
  </div>
</div>



    <!-- Modal -->
<div id="cancelInvModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body cancel-inv-body">

        <p id="as-can-inv">Are you sure about cancelling your investment ?</p>
        <p id="perc-goal-rem">You have 32% of your goal Remaining</p>

        <div id="progress-div">

          <span id="prog-zero">0</span><progress class="amount-progress" value="60000" max="120000">70 %</progress><span id="prog-no">5,00,000</span>
        </div>
        <button type="button" class="btn btn-primary yes-p-btn">Yes, Please Cancel Investment </button><br>
        <button type="button" class="btn btn-primary no-p-btn">No, I will stay Invested </button>

            
          

      </div>
    </div>
  </div>
</div>



<div id="invSummaryModal" class="modal fade" role="dialog">
  <div class="modal-dialog inv-summary-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body inv-summary-body">

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Date of Investment</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">18 Jul 2017</span></div>
        </div>

         <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Status</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Inprocess</span></div>
        </div>

        <div class ="row inv-details-row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 xs-pad-zero"><span class = "inv-info-text">Portfolio Type</span></div>
          <div class = "col-lg-1 col-md-1 col-sm-1 col-xs-1 xs-pad-zero"><span class = "col">:</span></div>
          <div class = "col-lg-5 col-md-5 col-sm-5 col-xs-5 xs-pad-zero"><span class = "inv-date">Custom</span></div>
        </div>

          <div class="row">
                    <div class="hr">
          
        </div>
          </div>

        <p id="inv-summary">Investment Summary</p>
        <div class="row">
          <table class="table">
              <thead class="table-head modal-table-head">
                <tr>
                  <th id="modal-equity-text">EQUITY</th>
                  <th id="eq-show-less"> </th>
                  
                </tr>
              </thead>
              <tbody id = "equity-body">
               
              </tbody>



                <thead class="table-head modal-table-head">
                <tr>
                  <th id="modal-elss-text">ELSS</th>
                  <th id="eq-show-less"> </th>
                  
                </tr>
              </thead>
              <tbody id = "equity-body">
                
              </tbody>

              <thead class="table-head modal-table-head">
              </thead>
               
              </table>
        </div>


        <div class="row">
          <button type = "button" class = "btn btn-primary save-now-btn" >SAVE</button>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <a class="cancel-inv-link"><span class = cancel-inv><img src="icons/no_tick.png"><span id="cancel-inv-txt">Cancel Investment</span></span></a>
            <p id="modal-disc">Disclosure : We encourage our investors to acquaint themselves with all the information provided by the AMC, before taking investment decision</p>
          </div>
        </div>


            
          

      </div>
    </div>
  </div>
</div>



    <!-- Modal -->
<div id="scheduleInvModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Great !</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div" style="display: none;">
          <p class="text-center" id="notify-via">Notify Via</p>

          <div class="col-xs-6 text-center">
            <input type="hidden" id="investment_id" value="47">
            <div class = " own-radio-div padding-lr-zero radio-inline">
              <input type="radio"  name="notify-mode" id="email" value ="email" >
              <label for="email"><span class="notify-radio" id="own-span"><img src="icons/r_dis.png" id="email-img"></span><span class="comm-mode">Email</span></label>
            </div>
          </div>

          <div class="col-xs-6 text-center">

            <div class = " own-radio-div padding-lr-zero radio-inline">
                  <input type="radio"  name="notify-mode" id="phone" value = "phone" >
                  <label for="phone"><span class="notify-radio" id="own-span"><img src="icons/r_dis.png" id="phone-img"></span><span class="comm-mode">SMS</span></label>
            </div>
          </div>
            <p class="text-center" id="once-amt">Once amount has been invested</p>
            
          </div><button type="button" class="btn btn-primary text-center" id="notify-done">DONE</button>
        </div>
      </div>
    </div>
  </div>
</div>


<!--Dummy user modal -->

<!-- Modal -->
<div id="nonKycModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center non-kyc" style="color: #0091EA;">User verification pending.</h4>
      </div>
      <div class="modal-body">
        <p class="text-center" id = "non_ver_pan"></p>
        <p class="text-center" style="padding: 0px 0px 40px 0px;" id="begin_inv">Your can begin investing <span style="color: #0091EA;">once your registration process is complete</span></p>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="helloModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="port_modal_name" style="color: #0091EA;"></h4>
      </div>
      <div class="modal-body">
        <p id="port_explanation"></p>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="schemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fund-name"><span class="fund-span" id="fund_name">L&T Ultra Short term</span></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 padding-lr-zero">
            <table class="table-striped">
              <thead>
                
              </thead>
              <tbody>
                <tr>
                  <td class="table-first">Fund Manager</td>
                  <td class="gotham_book table-sec" id="fund_manager">Anil shah</td>
                  <td class="table-first">Asset Size(Rs.Cr)</td>
                  <td class="gotham_book">Rs.<span id="asset_size">5000</span></td>
                </tr>

                <tr>
                  <td class="table-first">Entry Load</td>
                  <td class="gotham_book table-sec" id="entry_load">NA</td>
                  <td class="table-first">Exit Load</td>
                  <td class="gotham_book" id="exit_load">1.00%</td>
                </tr>

                <tr>
                  <td class="table-first">Benchmark</td>
                  <td class="gotham_book table-sec" id="benchmark">S&P BSE 200</td>
                  <td class="table-first">Launch Date</td>
                  <td class="gotham_book" id="launch_date">12-Apr-2017</td>
                </tr>

                <tr>
                  <td class="table-first">Investment Plan</td>
                  <td class="gotham_book table-sec" id="investment_plan">Growth</td>
                  <td class="table-first">Fund Type</td>
                  <td class="gotham_book" id="fund_type">Open ended</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12" id="graph-container">
            <div class="panel" id="scheme_panel">
              <div class="panel-head">
              <input type="hidden" id="duration_time" value="3">
              <input type="hidden" id="scheme_id" >
                <ul class="duration-list">
                  <li><a class="duration-time gotham_book" data-value='3'>3 Months</a></li>
                  <li><a class="duration-time gotham_book" data-value='6'>6 Months</a></li>
                  <li><a class="duration-time gotham_book" data-value='12'>1 Year</a></li>
                  <li><a class="duration-time gotham_book" data-value='36'>3 Years</a></li>
                  <li style="border-right: none;"><a class="duration-time gotham_book" data-value='60'>5 Years</a></li>
                </ul>
              </div>
              <div class="panel-body">

                <div id="graph-div">
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">

        <div class="row">
          <div class="col-xs-12">
            <div class="col-xs-10">
              <p id="disclosure" class="gotham_book">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns. </p>
            </div>
            <div class="col-xs-2">
               <a id="sid" class="btn btn-default" download>SID <span><i class="fa fa-download" aria-hidden="true" ></i>
</span></a>
      
            </div>
          </div>
        </div>
       
    </div>

  </div>
</div>



  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <script src="{{url('js/jquery.fund_selector_donut.js')}}"></script>
  <script src="{{url('js/fund_selector_d.js')}}"></script>
  <script src="{{url('js/jquery-ui.js')}}"></script>
  <script src="{{url('js/loader.js')}}"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


  <script>
    
  </script>

</body>
</html>
