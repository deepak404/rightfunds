<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details.css?v=1.1')}}">
    <link rel="stylesheet" href="{{'css/footer.css?v=1.1'}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{url('css/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.css?v=1.1')}}">
    <link href="{{url('css/font-awesome.min.css?v=1.1')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css/tax_saving.css?v=1.1')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{url('css/fund_selector.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/fund_selector_responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>


<nav class="navbar">
  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/"><img src="icons/logo.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active menu-text"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text menu-text-active"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    </div>
  </div>  
    
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Account Summary</p>
                      <p class="invest-text">Get your latest account statements</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="{{url('icons/zero_fee.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="{{url('icons/auto_inv.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="{{url('icons/tax_free.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>


          </div> <!--Profile Bar Ends -->
    </div>


          


          <div class = "row investment-summary">

             <div class="heading">
               
               <ul class="top-links list-inline panel-nav nav-tabs">

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/portfolio_details')}}"><li class="list-headings">Portfolio Details</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/investment_history')}}"><li class="list-headings">Investment History</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href="{{URL('/tax_saving')}}"><li class="list-headings">Tax Saving Statements</li></a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 padding-lr-zero">
                      <a href={{URL('/withdraw_funds')}}"><li class="list-headings no-b-right  active-page">Withdraw Funds</li></a>
                    </div>                 
                  
               </ul>
             
             </div><!-- Heading ends -->


             <div class = "row" id = "withdraw-info">

                    <div class = "col-lg-12 col-md-12 col-sm-12">

                         <p class = "withdraw-info-text" id = "w-s-text">We strongly recommend that you keep all the fund in your portfolio for atleast a year before withdrawing, for the following reasons:</p>
                          <ul class = "withdraw-ul">
                              <li>Different funds perform differently under the given market conditions. It is important to maintain a diversified portfolio at all times in order to minimise risk.</li>

                              <li>Staying invested in equity funds for atleast 1 year makes their return count as Long term capital gains. LTCG are completely tax free.</li>

                              <li>[In the case of Debt funds, returns are considered as LTCG after staying invested for atleast a period of 3 years. Following this return are taxed lower at 20% after accounting for inflation.]</li>

                              <li>Staying invested for a year prevents any exit loads(if applicable) from being applied to the amount you withdraw. Exit load is usually 1% of the amount withdrawn.</li>
                          </ul>
                    </div>

             </div>



  <div class="table-group row" id="wihtdraw_ids">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="col-xs-12">
      @foreach($portfolio_details as $scheme_type => $scheme_details)
        @if(is_array($scheme_details) && $scheme_type == 'eq')
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title fund-title">
                EQUITY
                <a data-toggle="collapse" id="equity-sm" href="#collapse1" style="float:right;">Show More</a>
              </h3>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
             <table class="table">
                <tbody id = "equity-body">
                  @foreach($scheme_details as $scheme_detail)
                    <tr>
                      <td class = "fund-name">
                        <div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id = "{{$scheme_detail['scheme_code']}}_checkbox"  value="{{$scheme_detail['scheme_code']}}">
                          <label for="{{$scheme_detail['scheme_code']}}_checkbox"><span class="checkbox"><img src="icons/check_dis.png"></span></label>
                        </div>
                        <div class = "col-xs-11 fund-name-div"><span class = "span-fund-name">{{$scheme_detail['scheme_name']}}</span><p class = "currently-inv"> Current Value <span class = "curr-span-amt" >Rs. <span id="{{$scheme_detail['scheme_code']}}_current_value"> {{$scheme_detail['current_value']}}</span></span></p></div>
                      </td>
                      <td class = "fund-amount"><input type="number" name="{{$scheme_detail['scheme_code']}}" class="custom-amount" id="{{$scheme_detail['scheme_code']}}" placeholder = "Rs." ></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @elseif(is_array($scheme_details) && $scheme_type == 'debt')
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title fund-title">
                DEBT
                <a data-toggle="collapse" href="#collapse2"  id="debt-sm" style="float:right;">Show More</a>
              </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
              <table class="table">
                <tbody id ="debt_body">
                  @foreach($scheme_details as $scheme_detail)
                    <tr>
                      <td class = "fund-name"><div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id ="{{$scheme_detail['scheme_code']}}_checkbox" value="{{$scheme_detail['scheme_code']}}">
                        <label for="{{$scheme_detail['scheme_code']}}_checkbox"><span class="checkbox"><img src="icons/check_dis.png"></span></label></div>
                      <div class = "col-xs-11 fund-name-div"><span class = "span-fund-name">{{$scheme_detail['scheme_name']}}</span><p class = "currently-inv"> Current Value<span class = "curr-span-amt" >Rs. <span id="{{$scheme_detail['scheme_code']}}_current_value"> {{$scheme_detail['current_value']}}</span></span></p></div></td>
                      <td class = "fund-amount"><input type="number" name="{{$scheme_detail['scheme_code']}}" class="custom-amount" id="{{$scheme_detail['scheme_code']}}"  placeholder = "Rs." ></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @elseif(is_array($scheme_details) && $scheme_type == 'bal')
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title fund-title">
                HYBRID
                <a data-toggle="collapse" href="#collapse3" style="float:right;" id="hybrid-sm">Show More</a>
              </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
              <table class="table table-bordered">
                <tbody id = "elss-body">
                  @foreach($scheme_details as $scheme_detail)
                    <tr>
                      <td class = "fund-name"><div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id ="{{$scheme_detail['scheme_code']}}_checkbox" value="{{$scheme_detail['scheme_code']}}">
                        <label for="{{$scheme_detail['scheme_code']}}_checkbox"><span class="checkbox"><img src="icons/check_dis.png"></span></label></div>
                        <div class = "col-xs-11 fund-name-div"><span class = "span-fund-name">{{$scheme_detail['scheme_name']}}</span><p class = "currently-inv"> Current Value <span class = "curr-span-amt" >Rs. <span id="{{$scheme_detail['scheme_code']}}_current_value"> {{$scheme_detail['current_value']}}</span></span></p></div></td>
                      <td class = "fund-amount"><input type="number" name="{{$scheme_detail['scheme_code']}}" class="custom-amount" id="{{$scheme_detail['scheme_code']}}" placeholder = "Rs." ></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @elseif(is_array($scheme_details) && $scheme_type == 'ts')
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title fund-title">
                ELSS
                <a data-toggle="collapse" href="#collapse4" style="float:right;" id="elss-sm">Show More</a>
              </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
              <table class="table table-bordered">
                <tbody id = "elss-body">
                  @foreach($scheme_details as $scheme_detail)
                    <tr>
                      <td class = "fund-name"><div class = "col-xs-1 padding-lr-zero checkbox-div"><input type="checkbox" id ="{{$scheme_detail['scheme_code']}}_checkbox" value="{{$scheme_detail['scheme_code']}}">
                        <label for="{{$scheme_detail['scheme_code']}}_checkbox"><span class="checkbox"><img src="icons/check_dis.png"></span></label></div>
                        <div class = "col-xs-11 fund-name-div"><span class = "span-fund-name">{{$scheme_detail['scheme_name']}}</span><p class = "currently-inv"> Current Value <span class = "curr-span-amt" >Rs. <span id="{{$scheme_detail['scheme_code']}}_current_value"> {{$scheme_detail['current_value']}}</span></span></p></div></td>
                      <td class = "fund-amount"><input type="number" name="{{$scheme_detail['scheme_code']}}" class="custom-amount" id="{{$scheme_detail['scheme_code']}}" placeholder = "Rs." ></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @endif
      @endforeach

      @if(array_key_exists('total_current_value',$portfolio_details))
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title fund-title">
              TOTAL AMOUNT
                <p style="float:right;">Rs. <span id="total-amt">{{$portfolio_details['total_current_value']}}</span></p>
            </h4>
          </div>        
        </div>
      @endif
    </div>
    @if(array_key_exists('total_current_value',$portfolio_details))
      <div class= "col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button type = "button" class = "btn btn-primary" id = "wd-btn">WITHDRAW</button>
      </div>
    @endif
  </div>
</div> <!-- investment-summary ends -->
    


          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>

    <!-- Modal -->
<div id="scheduleInvModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p id="great">Great !</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>

        <div class="row">
          <div class="col-xs-12" id="notify-opt-div">

            <button type="button" onclick="javascript:location.href='/#'" class="btn btn-primary text-center" id="notify-done">DONE</button>
          </div>
        </div>



            
          

      </div>
    </div>
  </div>
</div>
  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <script src="{{url('js/jquery.fund_selector_donut.js')}}"></script>
  <script src="{{url('js/withdraw_funds.js?v=1.1')}}"></script>
  <script src="{{url('js/scroller.js?v=1.1')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>



</script>

</body>
</html>
