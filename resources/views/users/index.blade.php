<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="{{url('css/bootstrap.min.css?v=1.1')}}">

    <link rel="stylesheet" href="{{url('css/index.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/index-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>


<!-- <nav class="navbar">
  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <a class="navbar-brand" href="#"><img src="{{url('icons/logo.png')}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active menu-text menu-text-active"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
        <li class="menu-text"><a href="{{URL('/account_activation')}}">Activate Account</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="location.href ='/logout';" >LOG OUT</button>
      </ul>
    </div>
  </div>  
    
  
</nav> -->

@extends('layouts.navbar')
  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 padding-r-zero col-xs-12">
                    @if(Auth::user()->p_check == 1)
                      @if(\Auth::user()->personalDetails->profile_img == "")
                        @if(\Auth::user()->personalDetails->sex == "male")
                        <img  class = "img profile-image" src="icons/pro_male.png">
                        @elseif(\Auth::user()->personalDetails->sex == "female")
                        <img  class = "img profile-image" src="icons/pro_female.png">
                        @endif
                      @else
                        <img  class = "img profile-image" src="assets/profile_images/{{\Auth::user()->personalDetails->profile_img}}">
                      @endif
                    @else
                      <img  class = "img profile-image" src="icons/pro_male.png">
                    @endif


                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 profile-content">
                      <p class="profile-name">{{Auth::user()->name}}</p>
                      <!--<p class="invest-text">Investing should be easy, let's get started.</p>-->
                      <p class="invest-text">{{\Auth::user()->email}}</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="{{url('icons/zero_fee.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="{{url('icons/auto_inv.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="{{url('icons/tax_free.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>


          </div> <!--Profile Bar Ends -->
    </div>

    @if(\Auth::user()->r_check == '1')

    @else


      @if(\Auth::user()->p_check == '1')
        <div class="row investment-summary" id="reg_bar">
          <div class="col-lg-12 col-md-12 col-sm-12">
            @if($mandate_cleared != '')
              <a id="remove_reg_bar">&times</a>
            @else
              <a id="remove_reg_bar">&times</a>
            @endif
            <div class="stepwizard">
              <div class="stepwizard-row">
                <div class="stepwizard-step">
                  <p>PAN Verified</p>
                  @if($pan_verified != '')
                    <img  class = "tick_image" src="{{url('icons/tick_index.png')}}" />
                    <p>Pan Verified.</p>
                  @else
                    <img  class = "tick_image" src="icons/untick_index.png" />
                    <p>Pan will be verified in one day.</p>
                  @endif
                </div>
                <div class="stepwizard-step">
                  <p>KYC</p>
                  @if($kyc_check == '')                
                    @if($video_link == '')
                      @if($pan_verified == '')
                        <img  class = "tick_image" src="{{url('icons/untick_index.png')}}" />
                        <p>KYC not verfied yet.</p>
                      @else 
                        <img  class = "tick_image" src="{{url('icons/untick_index.png')}}" />
                        <p>KYC not verfied yet. Begin your E-KYC <a href="{{url('/ekyc')}}" id="ekyc_here">here.</a></p>
                      @endif
                    @elseif($video_link != '')
                      <img  class = "tick_image" src="{{url('icons/untick_index.png')}}" />
                      <p><a href="{{URL('/assets/documents/mandate&kyc.pdf')}}"  target="_blank" class="download_forms">Download Documents</a></p>
                    @endif
                  @elseif($kyc_check != '')
                    <img  class = "tick_image" src="{{url('icons/tick_index.png')}}" />
                    <p>E-Kyc Video Complete</p>
                    @if($mandate_received != '')
                    @else
                      <p><a href="{{URL('/assets/documents/mandate.pdf')}}" target="_blank" class="download_forms">Download Mandate</a></p>
                    @endif
                  @endif
                </div>
                <div class="stepwizard-step">
                  <p>Mandate Received</p>
                  @if($mandate_received != '')
                    <img  class = "tick_image" src="{{url('icons/tick_index.png')}}" />
                  @else
                    <img  class = "tick_image" src="{{url('icons/untick_index.png')}}" />
                  <!--<p><a href="#" id="download_mandate">Download Mandate</a></p>-->
                  @endif
                </div>
                <div class="stepwizard-step">
                  <p>Mandate Registered</p>
                  @if($mandate_cleared != '')
                   <img  class = "tick_image" src="{{url('icons/tick_index.png')}}" />
                    <p>Start Investing.</p>
                  @else
                    <img  class = "tick_image" src="{{url('icons/untick_index.png')}}" />
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      @endif
    @endif




          <!-- CONTENT SHOULD COME HERE -->

              <div class = "row investment-summary">

              <div class = "col-lg-12 col-sm-12 col-md-12 col-xs-12 center-block">
                <p class = "is-head">Investment Summary</p>
                <p class = "is-details" >Snapshot of your investments as of today, <?php echo date('j M Y');?>.</p>
              </div>

              <div class = "row">

                  <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 in-det-row">

                      <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <p class = "int-topic">Current value</p>
                        <?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>
                          @if(array_key_exists('total_current_value', $user_investment_details))
                            <span class = "rs">Rs. </span><span class = "amount" id="total_current_value">{{$fmt->format($user_investment_details['total_current_value'])}}</span>
                          @else
                            <span class = "rs">Rs. </span><span class = "amount" id="total_current_value">0</span>
                          @endif
                      </div>

                      <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <p class = "int-topic">Total Investments</p>
                         @if(array_key_exists('total_amount_invested', $user_investment_details))
                            <span class = "rs">Rs.</span><span class = "amount" >{{$fmt->format($user_investment_details['total_amount_invested'])}}</span>
                          @else
                            <span class = "rs">Rs.</span><span class = "amount"> 0 </span>
                          @endif
                      </div>

                      <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <p class = "int-topic">Net Profit/Loss</p>
                          @if(array_key_exists('total_net_profit', $user_investment_details))
                            <span class = "rs">Rs.</span><span class = "amount">{{$fmt->format($user_investment_details['total_net_profit'])}}</span>
                          @else
                            <span class = "rs">Rs.</span><span class = "amount"> 0 </span>
                          @endif
                      </div>

                      <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <p class = "int-topic">Returns</p>
                          @if(array_key_exists('total_profit_percent', $user_investment_details))
                            <span class = "rs"></span><span class = "amount" id="average_return">{{$fmt->format($user_investment_details['total_profit_percent'])}}%</span>
                          @else
                            <span class = "rs"></span><span class = "amount" id="average_return"> 0% </span>
                          @endif
                          <p id="growth_type">XIRR</p>
                      </div>

                      @if(Auth::user()->p_check == 1) 
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <a id="add-goal-text-link">Add goal</a>
                        </div>
                      @endif
                  </div> <!-- in-det-row ends --> 
              </div>

               <div class="row hr" id="abv-x">
                      
              </div><!-- HR ends -->

              @if(Auth::user()->p_check == 1)
                <div class = "row" id="goal-row">
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="x-button">
                      <p id="close-inv-btn">&times</p>
                    </div>
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 goal-ach-row" id="goal-ach-div">

                        <div class ="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding: 0px;">
                          <p class ="int-topic">Goal Achieved</p>
                          <span class ="amount" id="goal_achieved_percent">{{$user_investment_details['goal_percent']}}%</span>
                        </div>

                        <div class = "col-lg-7 col-md-7 col-sm-7 col-xs-12 scale-row padding-lr-zero" id="goal_holder">
                          
                            <div class="col-xs-12 padding-lr-zero" id="goal_progress">
                              @if(array_key_exists('total_amount_invested',$user_investment_details))
                                <progress class="amount-progress" value="{{$user_investment_details['total_current_value']}}" max="{{$user_investment_details['goal_amount']}}">70 %</progress>
                              @else 
                                <progress class="amount-progress" value="0" max="{{$user_investment_details['goal_amount']}}">70 %</progress>
                              @endif
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 padding-lr-zero">
                          <div id="goal-amt-inn-div">
                              <span id="goal-amount">Rs. <span id="target_goal_amount">{{$fmt->format($user_investment_details['goal_amount'])}}</span></span>
                              <a id="edit-goal">Edit Goal</a>
                          </div>
                        </div>
                      
                    </div> <!-- goal-ach-row ends --> 
                </div>
              @endif

              <!-- pull-right goal-amount-div -->

              <div class="row">
                 
              </div>

              <div class="row hr">
                      
              </div><!-- HR ends -->


              <div class="row">
                  <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12" id="ach_away">
                    @if(Auth::user()->p_check == 1)
                      @if($user_investment_details['goal_percent'] == 100)


                      <div class="col-lg-12 col-md-12 col-sm-12"><p id="goal-away-text" class="text-center">You have achieved your goal. Click here to change your goal.<button type="button" class = "btn btn-primary" id="invest-now-btn"  onclick="javascript:location.href='/fund_selector'">INVEST MORE &nbsp;></button></p></div>


                        <!--<div class="col-lg-9 col-md-9 col-sm-9"><span id="goal-away-text" class="text-center">You have achieved your goal. Click here to change your goal.</span></div> 


                        <div class="col-lg-2 col-md-2 col-sm-2 padding-l-zero" ><button type="button" class = "btn btn-primary" id="change_goal">CHANGE GOAL &nbsp;></button></div>
                        <!--<span id="goal-away-text">You have achieved your goal. Click here to change your goal</span><button type="button" class = "btn btn-primary" id="change_goal">CHANGE GOAL &nbsp;></button>-->

                      @elseif($user_investment_details['goal_percent'] > 0)

                      <div class="col-lg-12 col-md-12 col-sm-12"><p id="goal-away-text" class="text-center">You are only {{100 - $user_investment_details['goal_percent']}}% away from achieving your Goal.<button type="button" class = "btn btn-primary" id="invest-now-btn"  onclick="javascript:location.href='/fund_selector'">INVEST NOW &nbsp;></button></p></div>


                        <!--<div class="col-lg-9 col-md-9 col-sm-9"><span id="goal-away-text" class="text-center">You are only {{100 - $user_investment_details['goal_percent']}}% away from achieving your Goal.</span></div> 


                        <div class="col-lg-2 col-md-2 col-sm-2 padding-l-zero" ><button type="button" class = "btn btn-primary" id="change_goal">CHANGE GOAL &nbsp;></button></div>-->

                        <!--<span id="goal-away-text">You are only {{100 - $user_investment_details['goal_percent']}}% away from achieving your Goal.</span><button type="button" class = "btn btn-primary" id="invest-now-btn"  onclick="javascript:location.href='/fund_selector'">INVEST NOW &nbsp;></button>-->
                      @else

                        <div class="col-lg-12 col-md-12 col-sm-12"><p id="goal-away-text" class="text-center">You have no Investments currently. <button type="button" class = "btn btn-primary" id="invest-now-btn"  onclick="javascript:location.href='/fund_selector'">INVEST NOW &nbsp;></button></p></div> 


                        <!--<div class="col-lg-2 col-md-2 col-sm-2 padding-l-zero" ></div>-->

                        <!--<span id="goal-away-text"></span><button type="button" class = "btn btn-primary" id="invest-now-btn"  onclick="javascript:location.href='/fund_selector'">INVEST NOW &nbsp;></button>-->
                      @endif
                    @else
                      <div class="col-lg-12 col-md-12 col-sm-12"><p id="goal-away-text" class="text-center">You have no Investments currently. <button type="button" class = "btn btn-primary" id="invest-now-btn"  onclick="javascript:location.href='/fund_selector'">INVEST NOW &nbsp;></button></p></div>
                    @endif
                  </div>
                
              </div>



          </div>

          <div class="row port-comp">
              <div class = "col-lg-12  col-md-12 col-sm-12 col-xs-12 " id="portfo-head">
                  <p class="is-head">Portfolio Composition</p>
                  <p class = "is-details" id="inv-today">How your portfolio as of today, <?php echo date('j M Y');?>.</p>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 port-comp-data padding-lr-zero">


                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 equity-div grad-border" id="equity-div">
                      @if(Auth::user()->p_check == 1 && $user_investment_details['eq']['total_amount_invested'] > 0)
                        <canvas id="equity-chart" height="200" width="180" class="donut-chart">
                          <div data-value="{{$user_investment_details['eq']['investment_percent']}}"></div>
                          <div data-value="{{$user_investment_details['eq']['profit_percent']}}"></div>
                          <div data-value="{{$user_investment_details['eq']['total_investment_percent']}}"></div>
                        </canvas>
                        <div class="canvas-in-div">
                          <p class="port-name">EQUITY</p>
                          <p class="port-perc">{{$user_investment_details['eq']['profit_percent']+$user_investment_details['eq']['investment_percent']}}%</p>
                        </div>

                        <div class="port-comp-details">
                          <p class="port-comp-text">Amt. Invested</p>
                          <p class="port-comp-amount">Rs. {{$fmt->format($user_investment_details['eq']['total_amount_invested'])}}</p>

                          <p class="port-comp-text">Curr. Value</p>
                          <p class="port-comp-amount">Rs. {{$fmt->format($user_investment_details['eq']['total_current_value'])}}</p>

                          <p class="port-comp-text">Net Gain/Loss</p>
                          <p class="port-comp-amount-dis" >Rs. {{$fmt->format($user_investment_details['eq']['total_net_profit'])}}</p>
                          @if($user_investment_details['eq']['total_profit_percent'] < 0)
                            <span class="gl-neg-perc">(<span id="eq_ng_return">{{$user_investment_details['eq']['total_profit_percent']}}</span>%)</span>
                          @else
                            <span class="gl-perc">(<span id="eq_return">{{$user_investment_details['eq']['total_profit_percent']}}</span>%)</span>
                          @endif
                        </div>
                      @else
                        <canvas id="equity-chart" height="200" width="180" class="donut-chart">
                          <div data-value="0"></div>
                          <div data-value="0"></div>
                          <div data-value="10"></div>
                        </canvas>
                        <div class="canvas-in-div">
                          <p class="port-name">EQUITY</p>
                          <p class="port-perc">0%</p>
                        </div>

                        <div class="port-comp-details">
                          <p class="port-comp-text">Amt. Invested</p>
                          <p class="port-comp-amount">Rs. 0</p>

                          <p class="port-comp-text">Curr. Value</p>
                          <p class="port-comp-amount">Rs. 0</p>

                          <p class="port-comp-text">Net Gain/Loss</p>
                          <p class="port-comp-amount-dis" >Rs. 0</p><span class="gl-perc">(0%)</span>
                        </div>
                      @endif
                  </div>


                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 debt-div grad-border" id="debt-div">
                    @if(Auth::user()->p_check == 1 && $user_investment_details['debt']['total_amount_invested'] > 0)
                      <canvas id="debt-chart" height="200" width="180" class="donut-chart">
                        <div data-value="{{$user_investment_details['debt']['investment_percent']}}"></div>
                          <div data-value="{{$user_investment_details['debt']['profit_percent']}}"></div>
                          <div data-value="{{$user_investment_details['debt']['total_investment_percent']}}"></div>
                      </canvas><div class="canvas-in-div"><p class="port-name">DEBT</p>
                      <p class="port-perc">{{$user_investment_details['debt']['profit_percent']+$user_investment_details['debt']['investment_percent']}}%</p></div>

                      <div class="port-comp-details">
                        <p class="port-comp-text">Amt. Invested</p>
                        <p class="port-comp-amount">Rs. {{$fmt->format($user_investment_details['debt']['total_amount_invested'])}}</p>

                        <p class="port-comp-text">Curr. Value</p>
                        <p class="port-comp-amount">Rs. {{$fmt->format($user_investment_details['debt']['total_current_value'])}}</p>

                        <p class="port-comp-text">Net Gain/Loss</p>
                        <p class="port-comp-amount-dis" >Rs. {{$fmt->format($user_investment_details['debt']['total_net_profit'])}}</p>
                        @if($user_investment_details['debt']['total_profit_percent'] < 0)
                          <span class="gl-neg-perc">(<span id="debt_ng_return">{{$user_investment_details['debt']['total_profit_percent']}}</span>%)</span>
                        @else
                          <span class="gl-perc">(<span id="debt_return">{{$user_investment_details['debt']['total_profit_percent']}}</span>%)</span>
                        @endif
                      </div>
                    @else
                      <canvas id="debt-chart" height="200" width="180" class="donut-chart">
                        <div data-value="0"></div>
                        <div data-value="0"></div>
                        <div data-value="10"></div>
                      </canvas><div class="canvas-in-div"><p class="port-name">DEBT</p>
                      <p class="port-perc">0%</p></div>

                      <div class="port-comp-details">
                        <p class="port-comp-text">Amt. Invested</p>
                        <p class="port-comp-amount">Rs. 0</p>

                        <p class="port-comp-text">Curr. Value</p>
                        <p class="port-comp-amount">Rs. 0</p>

                        <p class="port-comp-text">Net Gain/Loss</p>
                        <p class="port-comp-amount-dis" >Rs. 0</p><span class="gl-perc">(0%)</span>
                      </div>
                    @endif
                  </div>


                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hybrid-div grad-border" id="hybrid-div">
                    @if(Auth::user()->p_check == 1 && $user_investment_details['bal']['total_amount_invested'] > 0)
                      <canvas id="bal-chart" height="200" width="180" class="donut-chart">
                           <div data-value="{{$user_investment_details['bal']['investment_percent']}}"></div>
                          <div data-value="{{$user_investment_details['bal']['profit_percent']}}"></div>
                          <div data-value="{{$user_investment_details['bal']['total_investment_percent']}}"></div>
                      </canvas><div class="canvas-in-div"><p class="port-name">BALANCED</p>
                      <p class="port-perc">{{$user_investment_details['bal']['profit_percent']+$user_investment_details['bal']['investment_percent']}}%</p></div>

                      <div class="port-comp-details">
                        <p class="port-comp-text">Amt. Invested</p>
                        <p class="port-comp-amount">Rs. {{$fmt->format($user_investment_details['bal']['total_amount_invested'])}}</p>

                        <p class="port-comp-text">Curr. Value</p>
                        <p class="port-comp-amount">Rs. {{$fmt->format($user_investment_details['bal']['total_current_value'])}}</p>

                        <p class="port-comp-text">Net Gain/Loss</p>
                        <p class="port-comp-amount-dis" >Rs. {{$fmt->format($user_investment_details['bal']['total_net_profit'])}}</p>
                        @if($user_investment_details['bal']['total_profit_percent'] < 0)
                          <span class="gl-neg-perc">(<span id="bal_ng_return">{{$user_investment_details['bal']['total_profit_percent']}}</span>%)</span>
                        @else
                          <span class="gl-perc">(<span id="bal_return">{{$user_investment_details['bal']['total_profit_percent']}}</span>%)</span>
                        @endif
                      </div>
                    @else
                      <canvas id="bal-chart" height="200" width="180" class="donut-chart">
                        <div data-value="0"></div>
                        <div data-value="0"></div>
                        <div data-value="10"></div>
                      </canvas><div class="canvas-in-div"><p class="port-name">BALANCED</p>
                      <p class="port-perc">0%</p></div>

                      <div class="port-comp-details">
                        <p class="port-comp-text">Amt. Invested</p>
                        <p class="port-comp-amount">Rs. 0</p>

                        <p class="port-comp-text">Curr. Value</p>
                        <p class="port-comp-amount">Rs. 0</p>

                        <p class="port-comp-text">Net Gain/Loss</p>
                        <p class="port-comp-amount-dis" >Rs. 0</p><span class="gl-perc">(0%)</span>
                      </div>
                    @endif
                  </div>


                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 elss-div grad-border" id="elss-div">
                    @if(Auth::user()->p_check == 1 && $user_investment_details['ts']['total_amount_invested'] > 0 )
                      <canvas id="elss-chart" height="200" width="180" class="donut-chart">
                           <div data-value="{{$user_investment_details['ts']['investment_percent']}}"></div>
                          <div data-value="{{$user_investment_details['ts']['profit_percent']}}"></div>
                          <div data-value="{{$user_investment_details['ts']['total_investment_percent']}}"></div>
                      </canvas><div class="canvas-in-div"><p class="port-name">ELSS</p>
                      <p class="port-perc">{{$user_investment_details['ts']['profit_percent']+$user_investment_details['ts']['investment_percent']}}%</p></div>

                      <div class="port-comp-details">
                        <p class="port-comp-text">Amt. Invested</p>
                        <p class="port-comp-amount">Rs. {{number_format($user_investment_details['ts']['total_amount_invested'],2,'.',',')}}</p>

                        <p class="port-comp-text">Curr. Value</p>
                        <p class="port-comp-amount">Rs. {{number_format($user_investment_details['ts']['total_current_value'],2,'.',',')}}</p>

                        <p class="port-comp-text">Net Gain/Loss</p>
                        <p class="port-comp-amount-dis" >Rs. {{number_format($user_investment_details['ts']['total_net_profit'],2,'.',',')}}</p>
                        @if($user_investment_details['ts']['total_profit_percent'] < 0)
                          <span class="gl-neg-perc">(<span id="ts_ng_return">{{$user_investment_details['ts']['total_profit_percent']}}</span>%)</span>
                        @else
                          <span class="gl-perc">(<span id="ts_return">{{$user_investment_details['ts']['total_profit_percent']}}</span>%)</span>
                        @endif
                      </div>
                    @else
                      <canvas id="elss-chart" height="200" width="180" class="donut-chart">
                        <div data-value="0"></div>
                        <div data-value="0"></div>
                        <div data-value="10"></div>
                      </canvas><div class="canvas-in-div"><p class="port-name">ELSS</p>
                      <p class="port-perc">0%</p></div>

                      <div class="port-comp-details">
                        <p class="port-comp-text">Amt. Invested</p>
                        <p class="port-comp-amount">Rs. 0</p>

                        <p class="port-comp-text">Curr. Value</p>
                        <p class="port-comp-amount">Rs. 0</p>

                        <p class="port-comp-text">Net Gain/Loss</p>
                        <p class="port-comp-amount-dis" >Rs. 0</p><span class="gl-perc">(0%)</span>
                      </div>
                    @endif
                  </div>

              </div>



              <div class="hr">

              </div>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="view-portfolio">

                 <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 doing-great">
                     <p class="is-details"  id="meet-det-text">View detailed portfolio of fund held. <button type="button" class = "btn btn-primary" id="port-btn" onclick="javascript:location.href='/portfolio_details'">VIEW PORTFOLIO &nbsp;></button></p>
                     
                  </div>

                 <!--<div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-l-zero">-->
                   
                 </div>

                </div>
              </div>
            
             

          </div>

           <!-- CONTENT ENDS HERE -->



          <div class="row footer-row ">
              
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                
               <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <a href="https://twitter.com/rightfundsindia"><img  class = "social_icons" id = "" src="icons/twitter.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <a href="https://www.facebook.com/rightfunds/"><img  class = "social_icons" id = "fb-icon" src="icons/facebook.png"></a>
                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <a href="https://www.linkedin.com/company/rightfunds.com"><img  class = "social_icons" id = "" src="icons/linked_in.png"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>
              </div>



          </div>
</div> <!--Content Ends -->

</div>



          <!-- Modal -->
<div id="addGoalModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="invDetModDialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="setGoalModalBody">

        <p class="text-center" id="set-g-track">Set goals to stay on track!</p>
         <p class="text-center" id="inv-worth">I would like to have investments worth</p>

        <div class="input-group input-group-md goal-amount-block center-block ">
                  <span class="input-group-addon " id="inv-rs">Rs.</span>
                  <input type="number" class="form-control"  aria-describedby="basic-addon1" id="goal_amount" required="">
        </div>
        <button type="button" class="btn btn-primary center-block" id="set-g-btn">SET GOAL</button>
      </div>
    </div>
  </div>
</div>



  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <script src="{{url('js/jquery.donut.js')}}"></script>
  
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
<script>
$(document).ready(function(){
  var target_goal = parseInt($('#target_goal_amount').html());
  if(target_goal > 0) {
    $('#add-goal-text-link').css({display:'none'});
    $('#goal-row').css({display:'block'});
  } else {
    $('#add-goal-text-link').css({display:'inline'});
  }
$('#remove_reg_bar').on('click',function(){
  var no = "{{Auth::user()->id}}";
  
  removeRegBar(no);
});

  

  var curr_val,amt_inv,eq_amt_inv,eq_curr_val,debt_amt_inv,debt_curr_val,bal_amt_inv,bal_curr_val,ts_amt_inv,ts_curr_val = 0;
  var xirr_avg_return = xirr_eq_return = xirr_debt_return = xirr_bal_return = xirr_ts_return = 0;
  
  @if(Auth::user()->p_check == 1) {
    xirr_avg_return = parseFloat({{$user_investment_details['total_profit_percent']}});//console.log(xirr_avg_return);
    xirr_eq_return = parseFloat({{$user_investment_details['eq']['total_profit_percent']}});
    xirr_debt_return = parseFloat({{$user_investment_details['debt']['total_profit_percent']}});
    xirr_bal_return = parseFloat({{$user_investment_details['bal']['total_profit_percent']}});
    xirr_ts_return = parseFloat({{$user_investment_details['ts']['total_profit_percent']}});
  @endif




  @if(Auth::user()->p_check == 1) {
    if (("{{$user_investment_details['total_current_value']}}") && ("{{$user_investment_details['total_amount_invested']}}"))
    {
      curr_val = parseFloat({{$user_investment_details['total_current_value']}});
      amt_inv = parseFloat({{$user_investment_details['total_amount_invested']}});
    }/* else {
    curr_val = 0;
    amt_inv = 0;
  }*/
  @endif

  @if(Auth::user()->p_check == 1) {
    if (Auth::user()->p_check == 1 && ("{{$user_investment_details['eq']['total_amount_invested']}}") && ("{{$user_investment_details['eq']['total_current_value']}}")) 
    {
      eq_amt_inv = parseFloat({{$user_investment_details['eq']['total_amount_invested']}});
      eq_curr_val = parseFloat({{$user_investment_details['eq']['total_current_value']}});
    }/* else {
      eq_amt_inv  = 0;
      eq_curr_val = 0; 
    }*/
  @endif

  @if(Auth::user()->p_check == 1) {
    if (Auth::user()->p_check == 1 && ("{{$user_investment_details['debt']['total_amount_invested']}}") && ("{{$user_investment_details['debt']['total_current_value']}}")) 
    {

      debt_amt_inv = parseFloat({{$user_investment_details['debt']['total_amount_invested']}});
      debt_curr_val = parseFloat({{$user_investment_details['debt']['total_current_value']}});
    }/* else {
      debt_amt_inv  = 0;
      debt_curr_val = 0; 
    }*/
  @endif

  @if(Auth::user()->p_check == 1) {
    if (Auth::user()->p_check == 1 && ("{{$user_investment_details['bal']['total_amount_invested']}}") && ("{{$user_investment_details['bal']['total_current_value']}}")) 
    {
      bal_amt_inv = parseFloat({{$user_investment_details['bal']['total_amount_invested']}});
      bal_curr_val = parseFloat({{$user_investment_details['bal']['total_current_value']}});
    }/* else {
      bal_amt_inv  = 0;
      bal_curr_val = 0; 
    }*/
  @endif

  @if(Auth::user()->p_check == 1) {
    if(Auth::user()->p_check == 1 && ("{{$user_investment_details['ts']['total_amount_invested']}}") && ("{{$user_investment_details['ts']['total_current_value']}}")) 
    {
      ts_amt_inv = parseFloat({{$user_investment_details['ts']['total_amount_invested']}});
      ts_curr_val = parseFloat({{$user_investment_details['ts']['total_current_value']}});
    }/* else {
      ts_amt_inv  = 0;
      ts_curr_val = 0; 
    }*/
  @endif




  $('#growth_type').on('click',function(){

    if ($(this).text() == "XIRR") {
      $(this).html('ABSOLUTE');
      var average_return = 0;
      if(amt_inv > 0 ) {
        average_return = parseFloat((((curr_val - amt_inv)/amt_inv) * 100)).toFixed(2);
      }
      //console.log("average return is" +average_return);

      var eq_return = parseFloat((((eq_curr_val - eq_amt_inv)/eq_amt_inv) * 100)).toFixed(2);
      //console.log("Equity return is" + (eq_return * 100));

      var debt_return = parseFloat((((debt_curr_val - debt_amt_inv)/debt_amt_inv) * 100)).toFixed(2);
      //console.log("Equity return is" + (debt_return * 100));

      var bal_return = parseFloat((((bal_curr_val - bal_amt_inv)/bal_amt_inv) * 100)).toFixed(2);
      //console.log("Equity return is" + (bal_return * 100));

      var ts_return = parseFloat((((ts_curr_val - ts_amt_inv)/ts_amt_inv) * 100)).toFixed(2);

      $('#average_return').text(average_return+"%");
      $('#eq_return').text(eq_return);
      $('#debt_return').text(debt_return);
      $('#bal_return').text(bal_return);
      $('#ts_return').text(ts_return);
    } else if ($(this).text() == "ABSOLUTE"){
      $(this).html('XIRR');
      $('#average_return').text(xirr_avg_return+"%");
      $('#eq_return').text(xirr_eq_return);
      $('#debt_return').text(xirr_debt_return);
      $('#bal_return').text(xirr_bal_return);
      $('#ts_return').text(xirr_ts_return);
    }
  });
});




</script>
<script src="{{url('js/index.js?v=1.1')}}"></script>

</body>
</html>
