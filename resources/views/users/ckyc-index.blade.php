<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rightfunds</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{url('css/ckyc-index.css?v=1.1')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/footer.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/portfolio-details-responsive.css?v=1.1')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css?v=1.1">
    <link rel="stylesheet" href="{{url('css/jqueryui.css?v=1.1')}}">
    <link rel="stylesheet" href="{{url('css/datepicker.css?v=1.1')}}">
	<link rel="stylesheet" href="{{url('css/loader.css?v=1.1')}}">
    <link href="{{url('css/font-awesome.min.css?v=1.1')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('css/error.css?v=1.1')}}">

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98301278-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>

<div class="loader" id="loader"></div>
<nav class="navbar">
  
    <div class="container">
      <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/"><img src="{{url('icons/logo.png')}}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active menu-text menu-text-active"><a href="{{URL('/home')}}">Home</a></li>
        <li class="menu-text"><a href="{{URL('/preference')}}">Preference</a></li>
        <li class="menu-text"><a href="{{URL('/portfolio_details')}}">Account Statement</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right navbar-btn">
        <button type="button" class="btn btn-primary nav-logout-btn" onclick="javascript:location.href='{{url()->previous()}}'">BACK</button>
      </ul>
    </div>
    </div>
  
</nav>

  
<div class = "container">


  <div class="content">

     <div class = "row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-bar">

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-block ">
                    
                    

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fs-content">
                      <p class="profile-name">Complete online KYC</p>
                      <p class="invest-text">Upload your documents to complete KYC</p>
                    </div>

              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id="zero-fee">
                  <img src="{{url('icons/zero_fee.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Zero Fees</span>
                     <span class="highlight-text highlight-bot-text">For Everything</span>
                  </div>
                   
                  
                
              </div>

              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "automated">
                  <img src="{{url('icons/auto_inv.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Automated</span>
                     <span class="highlight-text highlight-bot-text">Investment</span>
                  </div>
                
              </div>


              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding-r-zero zero-fee" id = "tax-free">
                  <img src="{{url('icons/tax_free.png')}}">
                  <div class="info-container">
                     <span class="highlight-text">Tax Free</span>
                     <span class="highlight-text highlight-bot-text">Return</span>
                  </div>
                
              </div>

          </div> <!--Profile Bar Ends -->
    </div>


          


          <div class = "row panel investment-summary">

              <div class="row" id="ckyc-intro">

                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" id="ckyc-header-div">
                <p class="verify-header text-center">E-KYC Verification</p>
                <p class="verify-sub text-center">The verification process will be done via your webcam. Please keep these documents ready.</p>
                </div> <!--CKYC Header div ends -->


              

              <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" id="ckyc-content-div">




                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-ten">

                      <div class="col-xs-12" id="pancard-card">
                        <img src="{{url('icons/pan_card_s.png')}}" class="center-block" id="pan-card-img">
                        <div class="col-xs-12 card-text-div">
                            <p class="text-center card-header">PAN Card</p>
                            <p class="text-center card-sub">Issued by the income tax department.</p>
                        </div>
                      </div>

                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-ten">

                      <div class="col-xs-12" id="pancard-card">
                        <img src="{{url('icons/poa.png')}}" class="center-block" id="pan-card-img">
                        <div class="col-xs-12 card-text-div">
                            <p class="text-center card-header">Proof of Address</p>
                            <p class="text-center card-sub">like passport, Driving license or Aadhar</p>
                        </div>
                      </div>

                  </div>



                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-ten">

                      <div class="col-xs-12" id="pancard-card">
                        <img src="{{url('icons/sign_icon.png')}}" class="center-block" id="pan-card-img">
                        <div class="col-xs-12 card-text-div">
                            <p class="text-center card-header">Signature</p>
                            <p class="text-center card-sub">Sign on a piece of white paper with a blue pen</p>
                        </div>
                      </div>

                  </div>

              </div> <!--CKYC Content div ends -->

              <div class="col-lg-12 col-md-12 col-sm-12 text-center check-agree">
                

                 <span class="blue-kyc" id="watch_sample">Watch Sample Video </span>

                
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 text-center get-start-div">
                <button type="button" class="btn btn-primary" id="get-started-btn">GET STARTED&nbsp;&gt;</button>
              </div>


              </div><!-- CKYC INTRO ROW ENDS-->


               <div class="row" id="selfie">

                <!--<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" id="ckyc-header-div">
                    <p class="verify-header text-center">Let's take a Selfie</p>
                    <p class="verify-sub text-center">Make sure your face fits within the dotted line.</p>
                </div> <!--CKYC Header div ends -->

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 

                  <button type="button" class="btn btn-primary" id="back-video-btn">BACK</button>
                  <p class="verify-header text-center">E-KYC Verification</p>
                  <p class="verify-sub text-center" id="please-keep">Please Keep your PAN card, Proof of Address and Signature(Sign on a white paper with a blue pen) ready.</p>




                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 

                  <video id="preview" controls></video>

                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="cam-button"> 

                  <button id="record" class="btn btn-primary">RECORD</button>
                  <button id="stop" disabled class="btn btn-primary">PREVIEW</button>
                  <!--<button id="retake" disabled class="btn btn-primary">RETAKE</button>-->
                  <button id="submit" disabled class="btn btn-primary">SUBMIT</button>
                  
                </div>


              </div><!-- Selfie ROW ENDS-->
          </div> <!-- investment-summary ends -->



    


          <div class="row footer-row ">
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-l-zero" id="footer-links">
               <ul class="footer-links list-inline">
                <li><a href="{{URL('/about-us')}}">About us</a></li>
                <li><a href="{{URL('/privacy_policy')}}">Privacy Policy</a></li>
                <li><a href="{{URL('/terms')}}">Terms of Use</a></li>
                <li><a href="{{URL('/contact-us')}}">Contact</a></li>
              </ul>

                <p id="footer-disc">Disclaimer: Mutual fund investments are subject to market risk, Please read all scheme related documents carefully. Past performance is not an indicator of future returns.</p>
              </div>
              <div class="col-lg-1">
                
              </div>

              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 social-media padding-r-zero">
                  
                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{url('icons/twitter.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "fb-icon" src="{{url('icons/facebook.png')}}"></a>
                  </div>


                  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-4">
                    <a href=""><img id = "" src="{{url('icons/linked_in.png')}}"></a>
                  </div>

                  <div class="row padding-lr-zero footer-comp-div">
                    <p id="footer-comp">&copy;2017, Rightfunds.com</p>
                  </div>

              </div>



          </div>
      

</div> <!--Content Ends -->

</div>


<!-- Modal -->
<div id="watchSampleModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Sample Video</h4>
      </div>
      <div class="modal-body">
        <video controls="true">
          <source src="{{'assets/sample_video/sample_video.mp4'}}" type="video/mp4">
        </video>
      </div>
    </div>

  </div>
</div>

    <!-- Modal -->
<div id="ekycvideostatusModal" class="modal fade" role="dialog">
  <div class="modal-dialog  schedule-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body schedule-inv-body">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gotham_book">
            <p id="great" class="gotham_book">Great !</p>
            <p id="you-have"></p>
          </div>
        </div>

        <div class="row hr"></div>
        <button type="button" class="btn btn-primary text-center" id="notify-done">DONE</button>
      </div>
    </div>
  </div>
</div>

<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="{{url('js/ckyc.js')}}"></script>
<script src="{{url('js/loader.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>

<script>

</script>
</body>
</html>
